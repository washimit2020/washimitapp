<?php

namespace App\Http\Controllers\Places;

use App\__ModelsFestival\Festival;
use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\FollowStoreRequest;
use App\ManyToPlacesModels\Follower;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class FollowController extends Controller
{
    private $place = null;

    /**
     * to make a follow
     *
     * @param FollowStoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FollowStoreRequest $request)
    {

        $this->get_place_instance();


        $msg = ['followed' => $this->create_follower_instance($request)];

        //make notification

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, $msg));
    }

    /**
     * to get whom the current user is following
     *
     * @param $type Constants
     * @return \Illuminate\Http\JsonResponse
     */
    public function following($type)
    {

        if ($type == Constants::_TYPE_FESTIVAL) {
            $festivals = Festival::whereHas("followedBy", function ($query) {
                $query->where([
                    ['user_id', auth()->user()->id],
                    ['type_id', Constants::_TYPE_FOLLOW_FESTIVAL],
                    ['status_id', Constants::_STATUS_ACTIVE],
                ])->whereHas('user', function ($query) {
                    check_if_blocked($query);

                });
            })->paginate(Constants::PAGINATION);
            return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['festivals' => $festivals]));

        } else {
            $users = User::whereHas("followedBy", function ($query) {
                $query->where([
                    ['user_id', auth()->user()->id],
                    ['type_id', Constants::_TYPE_FOLLOW_USER],
                    ['status_id', Constants::_STATUS_ACTIVE],
                ])->whereHas('user', function ($query) {
                    check_if_blocked($query);

                });;

            })->where('type_id', $type)->paginate(Constants::PAGINATION);
            return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['users' => $users]));
        }


//        $following = Follower::where([
//                ['user_id', auth()->user()->id],
//                ['type_id', $follow_type],
//                ['status_id', Constants::_STATUS_ACTIVE],
//            ]
//        )->with(['place.social_media'])->whereHas('place', function ($query) use ($type) {
//
//            $query->where([
//                ['type_id', $type],
//                ['status_id', Constants::_STATUS_ACTIVE],
//
//            ]);
//
//        })->paginate(Constants::PAGINATION);

//        ,'place.attachments'=>function($query) use ($type){
//
//        $args = [Constants::_TYPE_AVATAR_IMAGE,Constants::_TYPE_COVER_IMAGE];
//
//        if ($type != Constants::_TYPE_FESTIVAL ){
//            //validate the festival here
//        }
//
//        $query->whereIn('type_id',$args)->with('type');


//        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['following' => $following]));
    }

    /**
     * to get who follows the current user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function followed_by()
    {
        $users = User::whereHas("following", function ($query) {
            $query->where([
                ['place_id', auth()->user()->id],
                ['type_id', Constants::_TYPE_FOLLOW_USER],
                ['status_id', Constants::_STATUS_ACTIVE],
            ])->whereHas('user',function ($query){
                check_if_blocked($query);
            });


        })->paginate(Constants::PAGINATION);
        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['users' => $users]));

    }

    /**
     * to determine the place
     */
    protected function get_place_instance()
    {
        switch (request()->follow_type) {
            case Constants::_TYPE_FOLLOW_USER:
                $this->place = User::find(request()->user_id);
                break;
            case Constants::_TYPE_FOLLOW_FESTIVAL:
                $this->place = Festival::find(request()->festival_id);
                break;
        }
    }

    /**
     * to store/update it in the db
     *
     * @param FollowStoreRequest $request
     * @return bool
     */
    protected function create_follower_instance(FollowStoreRequest $request)
    {
        $follow = auth()->user()->following()->where([
            ['place_id', $this->place->id],
            ['type_id', $request->follow_type],
        ])->first();

        if ($request->follow_type == Constants::_TYPE_FOLLOW_USER) {
            abort_if_blocked($this->place);
        } else {
            abort_if_blocked($this->place->user);
        }

        if (empty($follow)) {
            $follow = new Follower();
            $follow->user_id = auth()->user()->id;
            $follow->place_id = $this->place->id;
            $follow->type_id = $request->follow_type;
            $follow->save();
        } else {
            if ($follow->status_id == Constants::_STATUS_ACTIVE) {
                $follow->status_id = Constants::_STATUS_DELETED;
                $follow->update();
                return false;
            } else {
                $follow->status_id = Constants::_STATUS_ACTIVE;
            }
            $follow->update();
        }
        return true;

    }
}
