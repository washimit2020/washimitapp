package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.washimit.it.ryadah.Fragment.profile.EditProduactFragment;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.activity.SelectProductToShaerInShowrOrBranch;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.model.Location;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.product.ShowSharedProduct;
import com.washimit.it.ryadah.model.product.StoreSharedProduct;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_PROG = 0;
    private final int USER = 1;
    private final int PRODUCT = 2;
    private final int REQUSTES_STORE = 3;
    private final int REQUSTES_SHOW = 4;
    private final int FESTIVAL = 5;

    List<?> list;
    Activity context;
    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading, selectProduct;
    OnLoadMoreListener loadMoreListener;
    ApiRequest request;
    Gson gson;
    HashMap<String, String> map;


    public MyProfileAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        map = new HashMap<String, String>();
        request = new ApiRequest(context);
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            Log.d("ok", "ok");
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder instanceof UserHolder) {

            User user = (User) list.get(i);

            ((UserHolder) viewHolder).item_user_my_profile_follow.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(user.getFollowed())));

            ((UserHolder) viewHolder).item_user_my_profile_location.setText(user.getSector_name() + " - " + user.getStreet_name());
            ((UserHolder) viewHolder).item_user_my_profile_name.setText(user.getName());
            ((UserHolder) viewHolder).item_user_my_profile_rating.setRating((float) user.getRating());
            Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into((((UserHolder) viewHolder).item_user_my_profile_img));

        } else if (viewHolder instanceof ProductHolder) {
            final Product product = (Product) list.get(i);
            ((ProductHolder) viewHolder).item_product_my_profile_product_name.setText(product.getName());
            ((ProductHolder) viewHolder).item_product_my_profile_product_price.setText(product.getPrice() + "");
            ((ProductHolder) viewHolder).item_product_my_profile_rating.setRating((float) product.getRating());
            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_profile_picture)
                    .into(((ProductHolder) viewHolder).item_product_my_profile_img);

            if (AapHalper.isHide(product.getStatus_id())) {
                ((ProductHolder) viewHolder).hide.setVisibility(View.VISIBLE);

            } else {
                ((ProductHolder) viewHolder).hide.setVisibility(View.GONE);

            }
            ((ProductHolder) viewHolder).item_product_my_profile_select.setVisibility(product.isSelected() ? View.VISIBLE : View.GONE);

            //         ((ProductHolder) viewHolder).


        } else if (viewHolder instanceof ProductRequstsShowHolder) {

            ShowSharedProduct showSharedProduct = (ShowSharedProduct) list.get(i);
            ((ProductRequstsShowHolder) viewHolder).item_rqustes_show_my_profile_product_name.setText(showSharedProduct.getProduct().getName());
            ((ProductRequstsShowHolder) viewHolder).item_rqustes_show_my_profile_user_name.setText(showSharedProduct.getProduct().getUser().getName());
            Picasso.with(context).load(Constant._PRODUCT_URL + showSharedProduct.getProduct().getId() + "/" + 0).placeholder(R.drawable.ic_profile_picture).fit().centerCrop().into(((ProductRequstsShowHolder) viewHolder).item_rqustes_show_my_profile_img);

            if (showSharedProduct.getStatus_id() == 1) {
                ((ProductRequstsShowHolder) viewHolder).acceptedOrRegect.setText(R.string.accepted);
                ((ProductRequstsShowHolder) viewHolder).acceptedOrRegect.setVisibility(View.VISIBLE);
                ((ProductRequstsShowHolder) viewHolder).linearLayout2.setVisibility(View.GONE);


            } else if (showSharedProduct.getStatus_id() == 5) {
                ((ProductRequstsShowHolder) viewHolder).acceptedOrRegect.setText(R.string.declined);
                ((ProductRequstsShowHolder) viewHolder).acceptedOrRegect.setVisibility(View.VISIBLE);
                ((ProductRequstsShowHolder) viewHolder).linearLayout2.setVisibility(View.GONE);

            } else if (showSharedProduct.getStatus_id() == 2) {
                ((ProductRequstsShowHolder) viewHolder).acceptedOrRegect.setVisibility(View.GONE);
                ((ProductRequstsShowHolder) viewHolder).linearLayout2.setVisibility(View.VISIBLE);
            }


        } else if (viewHolder instanceof ProductRequstsStoreHolder) {
            StoreSharedProduct storeSharedProduct = (StoreSharedProduct) list.get(i);

            ((ProductRequstsStoreHolder) viewHolder).item_rqustes_store_my_profile_name.setText(storeSharedProduct.getProduct().getName());
            ((ProductRequstsStoreHolder) viewHolder).item_rqustes_store_my_profile_number_of_eaters.setText(storeSharedProduct.getProduct().getNumber_of_eaters() + "");
            ((ProductRequstsStoreHolder) viewHolder).item_rqustes_store_my_profile_price.setText(storeSharedProduct.getProduct().getPrice() + "");
            Picasso.with(context).load(Constant._PRODUCT_URL + storeSharedProduct.getProduct().getId() + "/" + 0).placeholder(R.drawable.ic_profile_picture).fit().centerCrop()
                    .into(((ProductRequstsStoreHolder) viewHolder).item_rqustes_store_my_profile_img);
            ((ProductRequstsStoreHolder) viewHolder).item_rqustes_store_my_profile_status.setText(AapHalper.getStatus(storeSharedProduct.getStatus_id()));


        } else if (viewHolder instanceof FestivalHolder) {
            Festivals festivals = (Festivals) list.get(i);

            ((FestivalHolder) viewHolder).item_rqustes_festival_name.setText(festivals.getName());
            ((FestivalHolder) viewHolder).item_rqustes_festival_info.setText(festivals.getInfo());
            Picasso.with(context).load(Constant._FESTIVAL_URL + festivals.getId() + "/" + 0).placeholder(R.drawable.ic_profile_picture).fit().centerCrop()
                    .into(((FestivalHolder) viewHolder).item_rqustes_festival_img);
            ((FestivalHolder) viewHolder).item_rqustes_festival_status.setText(AapHalper.getStatus(festivals.getStatus_id()));


        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == USER) {
            return new UserHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_my_profile, viewGroup, false));

        }
        if (i == FESTIVAL) {
            return new FestivalHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rqustes_festival_my_profile, viewGroup, false));

        } else if (i == PRODUCT) {
            return new ProductHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product_my_profile, viewGroup, false));


        } else if (i == REQUSTES_SHOW) {
            return new ProductRequstsShowHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rqustes_show_my_profile, viewGroup, false));


        } else if (i == REQUSTES_STORE) {
            return new ProductRequstsStoreHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_rqustes_store_my_profile, viewGroup, false));


        } else if (i == VIEW_PROG) {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));

        }

        return null;
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) instanceof Product) {

            return PRODUCT;
        } else if (list.get(position) instanceof StoreSharedProduct) {

            return REQUSTES_STORE;
        } else if (list.get(position) instanceof ShowSharedProduct) {

            return REQUSTES_SHOW;
        } else if (list.get(position) instanceof User) {
            return USER;
        } else if (list.get(position) instanceof Festivals) {
            return FESTIVAL;
        } else if (list.get(position) == null) {

            return VIEW_PROG;
        }
        return position;
    }


    public class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_user_my_profile_img)
        ImageView item_user_my_profile_img;
        @BindView(R.id.item_user_my_profile_follow)
        ImageView item_user_my_profile_follow;
        @BindView(R.id.item_user_my_profile_map_location)
        ImageView item_user_my_profile_map_location;

        @BindView(R.id.item_user_my_profile_name)
        TextView item_user_my_profile_name;
        @BindView(R.id.item_user_my_profile_location)
        TextView item_user_my_profile_location;

        @BindView(R.id.item_user_my_profile_rating)
        RatingBar item_user_my_profile_rating;


        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_user_my_profile_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());
                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    cancelFollow(Constant._TYPE_FOLLOW_USER, user.getId());
                }
            });
            item_user_my_profile_map_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());
                    Intent intent = new Intent(context, MapsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("location", gson.toJson(new Location(new LatLng(user.getLat(), user.getLng()), user.getType().getId()), Location.class));
                    context.startActivity(intent);
                }
            });

        }
    }

    public class FestivalHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_rqustes_festival_img)
        ImageView item_rqustes_festival_img;
        @BindView(R.id.item_rqustes_festival_info)
        TextView item_rqustes_festival_info;
        @BindView(R.id.item_rqustes_festival_name)
        TextView item_rqustes_festival_name;
        @BindView(R.id.item_rqustes_festival_status)
        TextView item_rqustes_festival_status;

        public FestivalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public class ProductHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_product_my_profile_select)
        RelativeLayout item_product_my_profile_select;
        @BindView(R.id.hide)
        TextViewCast hide;
        @BindView(R.id.item_product_my_profile_img)
        ImageView item_product_my_profile_img;
        @BindView(R.id.item_product_my_profile_product_name)
        TextViewCast item_product_my_profile_product_name;
        @BindView(R.id.item_product_my_profile_product_price)
        TextViewCast item_product_my_profile_product_price;
        @BindView(R.id.item_product_my_profile_rating)
        RatingBar item_product_my_profile_rating;
        @BindView(R.id.item_product_my_profile_user_option)
        ImageView item_product_my_profile_user_option;
        @BindView(R.id.my_product)
        LinearLayout my_product;

        public ProductHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());
                    if (selectProduct) {
                        if (!AapHalper.isHide(product.getStatus_id())) {
                            product.setSelected(!product.isSelected());
                            item_product_my_profile_select.setVisibility(product.isSelected() ? View.VISIBLE : View.GONE);
                        } else {
                            Toast.makeText(context, "هذا النتج مخفي لا يمكن مشاركته", Toast.LENGTH_LONG).show();
                        }

                    } else {
                        Intent intent = new Intent(context, ProductDetailsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("data", gson.toJson(list.get(getAdapterPosition())));
                        context.startActivity(intent);
                    }
                }
            });

            item_product_my_profile_user_option.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Product product = (Product) list.get(getAdapterPosition());

                    AapHalper.CreateDialog(R.layout.dialog_product_option, context, new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            LinearLayout dialog_product_option_sher, dialog_product_option_edit, dialog_product_option_delete, dialog_product_option_show_or_hiden;
                            dialog_product_option_sher = (LinearLayout) dialog.findViewById(R.id.dialog_product_option_sher);
                            dialog_product_option_edit = (LinearLayout) dialog.findViewById(R.id.dialog_product_option_edit);
                            dialog_product_option_delete = (LinearLayout) dialog.findViewById(R.id.dialog_product_option_delete);
                            dialog_product_option_show_or_hiden = (LinearLayout) dialog.findViewById(R.id.dialog_product_option_show_or_hiden);

                            dialog_product_option_sher.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    AapHalper.sher(context, item_product_my_profile_img, product.getName());
                                    dialog.dismiss();
                                }
                            });

                            dialog_product_option_edit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("product", gson.toJson(product));
                                    EditProduactFragment fragment = new EditProduactFragment();
                                    fragment.setArguments(bundle);
                                    ((MainActivity) context).openFragment(fragment);
                                    dialog.dismiss();

                                }
                            });

                            dialog_product_option_delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    list.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    notifyItemRangeChanged(getAdapterPosition(), list.size() - 1);

                                    deleteProduct(product.getId());
                                    dialog.dismiss();
                                }
                            });

                            dialog_product_option_show_or_hiden.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (AapHalper.isHide(product.getStatus_id())) {
                                        hide.setVisibility(View.GONE);
                                        product.setStatus_id(Constant._STATUS_ACTIVE);

                                    } else {
                                        if (product.isSelected()) {
                                            product.setSelected(!product.isSelected());
                                            item_product_my_profile_select.setVisibility(product.isSelected() ? View.VISIBLE : View.GONE);

                                        }
                                        hide.setVisibility(View.VISIBLE);
                                        product.setStatus_id(Constant._STATUS_HIDDEN);
                                    }


                                    hide(product.getId());
                                    dialog.dismiss();
                                }


                            });


                        }
                    });

                }
            });
        }
    }

    public class ProductRequstsStoreHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_rqustes_store_my_profile_img)
        ImageView item_rqustes_store_my_profile_img;
        @BindView(R.id.item_rqustes_store_my_profile_name)
        TextView item_rqustes_store_my_profile_name;
        @BindView(R.id.item_rqustes_store_my_profile_price)
        TextView item_rqustes_store_my_profile_price;
        @BindView(R.id.item_rqustes_store_my_profile_number_of_eaters)
        TextView item_rqustes_store_my_profile_number_of_eaters;
        @BindView(R.id.item_rqustes_store_my_profile_status)
        TextView item_rqustes_store_my_profile_status;

        public ProductRequstsStoreHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ProductRequstsShowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.acceptedOrRegect)
        TextViewCast acceptedOrRegect;
        @BindView(R.id.linearLayout2)
        LinearLayout linearLayout2;
        @BindView(R.id.item_rqustes_show_my_profile_img)
        ImageView item_rqustes_show_my_profile_img;
        @BindView(R.id.item_rqustes_show_my_profile_user_name)
        TextView item_rqustes_show_my_profile_user_name;
        @BindView(R.id.item_rqustes_show_my_profile_product_name)
        TextView item_rqustes_show_my_profile_product_name;
        @BindView(R.id.item_rqustes_show_my_profile_reject)
        Button item_rqustes_show_my_profile_reject;
        @BindView(R.id.item_rqustes_show_my_profile_accept)
        Button item_rqustes_show_my_profile_accept;

        public ProductRequstsShowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_rqustes_show_my_profile_reject.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ShowSharedProduct product = (ShowSharedProduct) list.get(getAdapterPosition());
                    linearLayout2.setVisibility(View.GONE);
                    acceptedOrRegect.setVisibility(View.VISIBLE);
                    acceptedOrRegect.setText(R.string.declined);
                    acceptedOrRegect.setTextColor(context.getResources().getColor(R.color.app_color));
                    acceptReject(product.getId(), Constant._STATUS_DECLINED);
                }
            });
            item_rqustes_show_my_profile_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ShowSharedProduct product = (ShowSharedProduct) list.get(getAdapterPosition());
                    linearLayout2.setVisibility(View.GONE);
                    acceptedOrRegect.setVisibility(View.VISIBLE);
                    acceptedOrRegect.setText(R.string.accepted);
                    acceptedOrRegect.setTextColor(context.getResources().getColor(R.color.black));
                    acceptReject(product.getId(), Constant._STATUS_ACTIVE);
                }
            });
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }

    private void cancelFollow(int followType, int userId) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));
        parts.add(new StringPart("user_id", userId + ""));
        AapHalper.userAction("follow", parts, context, null);
    }

    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setSelectProduct(boolean selectProduct) {
        if (!selectProduct) {
            for (int i = 0; i < list.size(); i++) {
                Product product = (Product) list.get(i);
                product.setSelected(false);

            }
            notifyDataSetChanged();

        }
        this.selectProduct = selectProduct;
    }

    public void shaerSelect(String type) {
        ArrayList<Product> products = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Product product = (Product) list.get(i);
            if (product.isSelected()) {
                products.add(product);
            }

        }
        if (products.size() > 0) {
            Intent intent = new Intent(context, SelectProductToShaerInShowrOrBranch.class);
            intent.putExtra("products", gson.toJson(products));
            intent.putExtra("type", type);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } else {
            Toast.makeText(context, "يجب اختيار منتج على الاقل !", Toast.LENGTH_LONG).show();

        }

    }

    private void acceptReject(int productId, int statusId) {
        map.clear();
        map.put("status_id", statusId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/share/" + productId + "/update", map, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {

                Log.d("share", result);

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("share", errorMessage);
            }
        });

    }

    public void deleteProduct(int ProductId) {
        request.MultipartRequest(Request.Method.POST, false, "product/" + ProductId + "/delete", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("deleteProduct", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("deleteProduct", errorMessage);

            }
        });

    }

    private void openActivity(Class activity, String data) {


    }

    public void hide(int ProductId) {
        request.MultipartRequest(Request.Method.POST, false, "product/" + ProductId + "/hide", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("hideProduct", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("hideProduct", errorMessage);

            }
        });

    }

}