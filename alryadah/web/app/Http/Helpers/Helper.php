<?php

function check_status($data, \Illuminate\Database\Eloquent\Model $model)
{

    if (auth('api')->check()) {
        $data ['user_id'] = auth('api')->user()->id;
        $status = $model::where($data)->first();
    }

    if (empty($status)) {
        $status = false;
    } else {
        $status = true;
    }

    return $status;
}

function add_or_remove($data, \Illuminate\Database\Eloquent\Model $model)
{
    $save = true;
    try {
        make_like_notification($model::create($data), $data);
    } catch (\Illuminate\Database\QueryException $exception) {
        $save = false;

        $model::where([[$data]])->delete();
    }

    return $save;
}

function get_user_obj($user = null)
{
    if (!empty($user)) {
        return $user->with(["role", "attachment" => function ($query) {
            $query->with('type')->get();
        }])->find($user->id);
    }
    return function ($query) {
        $query->with(["role", "attachment" => function ($query) {
            $query->with('type')->get();
        }]);
    };
}

function get_news_obj()
{
    $relations = ['views', 'likes', 'comments'];
    $counts = ['views', 'likes', 'comments'];
    $relations["user"] = get_user_obj();
    $relations["attachment"] = function ($query) {
        $query->with('type')->get();
    };

    return \App\News::with($relations)->withCount($counts);
}

function get_comment_obj()
{
    $relations = ['user', 'replies'];
    $counts = ['likes', 'replies'];
    $relations["user"] = get_user_obj();
    $relations["replies"] = function ($query){
        $query->with(['user' => get_user_obj()])->paginate(10);
    };

    return \App\Comment::with($relations)
        ->withCount($counts);
}



function make_like_notification(\Illuminate\Database\Eloquent\Model $model, $data)
{
    $user = get_user_obj($model->user);

    if ($model instanceof \App\NewsLike) {
        $news = get_news_obj()->findOrFail($data['news_id']);
        if ($news->user_id != $model->user_id)
            $news->user->notify(new \App\Notifications\NewsLiked($news, $user));
    }
    if ($model instanceof \App\CommentLike) {
        $comment = get_comment_obj()->findOrFail($data['comment_id']);
        if ($comment->user_id != $model->user_id)
            $comment->user->notify(new \App\Notifications\CommentLiked($comment, $user));
    }

    if ($model instanceof \App\ReplyLike) {
        $reply = \App\Reply::findOrFail($data['reply_id']);
        $comment = get_comment_obj()->findOrFail($reply->comment_id);
        if ($reply->user_id != $model->user_id)
            $reply->user->notify(new \App\Notifications\CommentLiked($comment, $user));
    }
//    elseif ($model instanceof \App\ReplyLike){
//        $news = $model->reply()->withCount(['views', 'likes', 'comments'])->findOrFail($model->news->id);
//        if ($news->user != $model->user)
//            $news->user->notify(new \App\Notifications\NewsLiked($news,$user));
//    }
}

function get_translation_for_notification($notifications)
{
    foreach ($notifications as $notification) {
        $notification->msg = trans('notification.news.liked', translate_notification($notification->type, $notification->data));
    }
}

function translate_notification($type, $data)
{
    $trans_col["name"] = "{$data["user"]["first_name"]} {$data["user"]["last_name"]}";

    switch ($type) {
        case \App\Notifications\NewsLiked::class:
            $trans_col["news"] = str_limit($data["news"]["title"], 50);
            break;
        case \App\Notifications\CommentLiked::class:
            $trans_col["comment"] = str_limit($data["comment"]["content"], 50);
            break;
    }

    return $trans_col;
}

