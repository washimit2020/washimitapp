package com.washimit.questionandanswer.questionandanswer.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.interfac.deletnotecallback;
import com.washimit.questionandanswer.questionandanswer.interfac.pageIdCallback;
import com.washimit.questionandanswer.questionandanswer.modle.Sections;
import com.washimit.questionandanswer.questionandanswer.modle.Comment;
import com.washimit.questionandanswer.questionandanswer.modle.wishiListed;

import java.util.List;

/**
 * Created by washm on 4/4/17.
 */

public class UserActionAdapter extends RecyclerView.Adapter {

    pageIdCallback collback;
    deletnotecallback deletnotecallback;
    List<?> list;
    Activity context;


    public UserActionAdapter(Activity context, List<Object> list) {
        this.list = list;
        this.context = context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        if (viewType == 0) {
            holder = new SectionHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_action_item, parent, false));
        }
        if (viewType == 1) {
            holder = new WishilistedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_action_item, parent, false));
        } else if (viewType == 2) {
            holder = new commentHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_action_item, parent, false));
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof WishilistedHolder) {
            final wishiListed get = (wishiListed) list.get(position);

            ((WishilistedHolder) holder).title.setText(get.title);
            ((WishilistedHolder) holder).pageNumber.setText(String.valueOf(get.id));

            ((WishilistedHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.id);

                }
            });


        }
        if (holder instanceof SectionHolder) {
            final Sections get = (Sections) list.get(position);

            ((SectionHolder) holder).title.setText(get.title);
            ((SectionHolder) holder).pageNumber.setText(String.valueOf(get.id));

            ((SectionHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (get.question != null) {
                        collback.pageId(get.question.id);
                    } else {
                        Toast.makeText(context, "لا يوجد اسئلة لهذا الفصل", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else if (holder instanceof commentHolder) {

            final Comment get = (Comment) list.get(position);

            ((commentHolder) holder).title.setText(get.body);
            ((commentHolder) holder).pageNumber.setText(String.valueOf(get.question_id));

            ((commentHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.question_id);

                }
            });
            ((commentHolder) holder).delet_note.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());
                    deletnotecallback.deletNote(get.id);
                }
            });

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(0) instanceof Sections) {

            return 0;
        }
        if (list.get(0) instanceof wishiListed) {

            return 1;
        } else if (list.get(0) instanceof Comment) {

            return 2;
        }
        return super.getItemViewType(position);
    }

    public void onpageIdCollback(pageIdCallback collback) {
        this.collback = collback;
    }

    public void onpageNotDeletCollback(deletnotecallback deletnotecallback) {
        this.deletnotecallback = deletnotecallback;
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    public class WishilistedHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;
        ImageView delet_note;

        public WishilistedHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);
            delet_note = (ImageView) itemView.findViewById(R.id.delet_note);

            delet_note.setVisibility(View.GONE);
        }

    }

    public class commentHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;
        ImageView delet_note;

        public commentHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);
            delet_note = (ImageView) itemView.findViewById(R.id.delet_note);
            delet_note.setVisibility(View.VISIBLE);

        }


    }


    public class SectionHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;
        ImageView delet_note;

        public SectionHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);
            delet_note = (ImageView) itemView.findViewById(R.id.delet_note);

            delet_note.setVisibility(View.GONE);
        }

    }


}
