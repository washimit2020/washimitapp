//
//  Menu_TableViewCell.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

class Menu_TableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
   
    var menu:MenuItem!{
        didSet{
            self.title.text = menu.title
            self.icon.image = menu.image?.withRenderingMode(.alwaysTemplate)
            self.icon.tintColor = .tintColor
        }
    }
    
}
