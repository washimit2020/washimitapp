<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestivalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festivals', function (Blueprint $table) {
            $table->increments('id');
            $table->string("name");
            $table->string("organization");
            $table->string("source");
            $table->string("lat")->nullable();
            $table->string("lng")->nullable();
            $table->string("street_name");
            $table->string("sector_name");
            $table->string("organizer_name");
            $table->string("organizer_phone_number");

            $table->date("start_in");
            $table->time("from");
            $table->time("to");
            $table->text("info");
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('social_media_id')->nullable();
            $table->unsignedInteger('status_id')->default(\App\Http\Constants::_STATUS_INACTIVE);

            $table->decimal('rating', 2,1)->default(0);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('social_media_id')->references('id')->on('social_media');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festivals');
    }
}
