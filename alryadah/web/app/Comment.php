<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends NewsWithUser
{
    public $incrementing = true;
    /**
     * @var array primary key
     */
    protected $primaryKey = "id";

    /**
     * @var array fillable key
     */
    protected $fillable = [
        "news_id", "user_id", "content"
    ];

    /**
     * has many likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes(){
        return $this->hasMany('App\CommentLike','comment_id');
    }

    /**
     * has many replies
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies(){
        return $this->hasMany('App\Reply','comment_id');
    }


}
