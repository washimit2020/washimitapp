<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;

class NewsSubTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_sub_types')->insert([
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_INTERNATIONAL
            ],
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_INTERNATIONAL
            ],
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_INTERNATIONAL
            ],
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_ALRADYH_FEMALES
            ],
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_ALRADYH_FEMALES
            ],
            [
                "name_en" => str_random(15),
                "name_ar" => str_random(15),
                "news_main_type_id" => Constants::NEWS_MAIN_TYPE_ALRADYH_NETWORK
            ],

        ]);
    }
}
