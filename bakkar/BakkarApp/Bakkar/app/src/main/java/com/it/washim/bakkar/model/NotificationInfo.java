package com.it.washim.bakkar.model;

/**
 * Created by mohammad on 11/20/2017.
 */

public class NotificationInfo {
    public String title;
    public String body;
    public String sound;
    public String badge;
}
