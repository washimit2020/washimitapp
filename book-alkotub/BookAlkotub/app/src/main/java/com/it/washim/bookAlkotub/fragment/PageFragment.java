package com.it.washim.bookAlkotub.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.it.washim.bookAlkotub.activity.ShaerActivity;
import com.it.washim.bookAlkotub.coustumView.EditTextCursorWatcher;
import com.it.washim.bookAlkotub.R;
import com.it.washim.bookAlkotub.coustumView.TextViewCustem;
import com.it.washim.bookAlkotub.helper.ColorId;
import com.it.washim.bookAlkotub.helper.CustomTypefaceSpan;
import com.it.washim.bookAlkotub.helper.DataBHelper;
import com.it.washim.bookAlkotub.helper.PreferenceClass;
import com.it.washim.bookAlkotub.interfac.changefont;
import com.it.washim.bookAlkotub.model.highlight;
import com.it.washim.bookAlkotub.interfac.onselectChangecallback;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;


public class PageFragment extends Fragment implements View.OnClickListener, changefont {

    //TODO: Rename and change types of parameters
    private String titel;
    private String bady, refS;
    private String img;
    private int isWishilisted, isFullLine;
    private String comment;
    TextViewCustem ref, title;
    private int id;
    private EditTextCursorWatcher textselect;
    ImageView image, sher;
    RelativeLayout relative;
    private FloatingActionButton actionButtonble, actionButtonred, actionButtonyallo, actionButtonsher, actionButtonAddComment;
    LinearLayout layout;
    RelativeLayout page;
    DataBHelper helper;
    List<highlight> highlights;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    PreferenceClass preferenceClass;
    ScrollView scrollView;
    Drawable drawable;
    View refLine,not_full_ref_line;

    public PageFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PageFragment newInstance(String titel, String bady, int id, String img, int isWishilisted, String comment, String ref, int isFullLine) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putString("titel", titel);
        args.putString("bady", bady);
        args.putString("ref", ref);
        args.putString("img", img);
        args.putInt("id", id);
        args.putInt("isFullLine", isFullLine);
        args.putInt("isWishilisted", isWishilisted);
        args.putString("comment", comment);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            titel = getArguments().getString("titel");
            refS = getArguments().getString("ref");
            bady = getArguments().getString("bady");
            img = getArguments().getString("img");
            id = getArguments().getInt("id");
            isFullLine = getArguments().getInt("isFullLine");
            isWishilisted = getArguments().getInt("isWishilisted");
            comment = getArguments().getString("comment");
        }
        sharedPreferences = getActivity().getSharedPreferences("myPraf", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_page, container, false);
        preferenceClass = new PreferenceClass(getContext());
        helper = new DataBHelper(getActivity());
        relative = (RelativeLayout) view.findViewById(R.id.relative);
        image = (ImageView) view.findViewById(R.id.image);
        sher = (ImageView) view.findViewById(R.id.sher);
        textselect = (EditTextCursorWatcher) view.findViewById(R.id.bady);
        ref = (TextViewCustem) view.findViewById(R.id.ref);
        title = (TextViewCustem) view.findViewById(R.id.title);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        actionButtonble = (FloatingActionButton) view.findViewById(R.id.actionButtonwhiht);
        actionButtonred = (FloatingActionButton) view.findViewById(R.id.actionButtonred);
        actionButtonyallo = (FloatingActionButton) view.findViewById(R.id.actionButtonyallo);
        actionButtonsher = (FloatingActionButton) view.findViewById(R.id.actionButtonsher);
        layout = (LinearLayout) view.findViewById(R.id.selcter);
        page = (RelativeLayout) view.findViewById(R.id.page);
        refLine = (View) view.findViewById(R.id.ref_line);
        not_full_ref_line = (View) view.findViewById(R.id.not_full_ref_line);
        actionButtonAddComment = (FloatingActionButton) view.findViewById(R.id.add_comment);

        ScrollView scrollView = (ScrollView) view.findViewById(R.id.ScrollView);
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        actionButtonred.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.red)));
        actionButtonyallo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yaloo)));
        actionButtonble.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

        if (comment != null) {
            actionButtonAddComment.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
        } else {
            actionButtonAddComment.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));

        }
        pageSeting();

        layout.setVisibility(View.GONE);
        textselect.setTextIsSelectable(true);
        textselect.setCursorVisible(false);
        textselect.setShowSoftInputOnFocus(false);

        actionButtonble.setOnClickListener(this);
        actionButtonred.setOnClickListener(this);
        actionButtonyallo.setOnClickListener(this);
        actionButtonsher.setOnClickListener(this);
        actionButtonAddComment.setOnClickListener(this);
        sher.setOnClickListener(this);


        textselect.selectChange(new onselectChangecallback() {
            @Override
            public void selectChange(int selStart, int selEnd) {

                if (selStart != selEnd) {

                    layout.setVisibility(View.VISIBLE);
                } else {
                    layout.setVisibility(View.GONE);
                }


                // textselect.getText().setSpan(new UnderlineSpan(), selStart, selEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                // textselect.getText().setSpan(new ForegroundColorSpan(Color.BLUE), selStart, selEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            }
        });
        Resources res = getResources();
        String mDrawableName = img;


        if (img != null) {
            Log.d("mDrawableName", mDrawableName);
            int resID = res.getIdentifier(mDrawableName, "drawable", "com.it.washim.bookAlkotub");

            try {
                drawable = res.getDrawable(resID, null);
                image.setImageDrawable(drawable);
            } catch (Exception e) {

            }

        } else {
            image.setVisibility(View.GONE);
        }
        //replace("--(.+?)--","--");

        replace(new String[]{"--(.+?)--", "__(.+?)__", "sm(.+?)sm", "=(.+?)="}, new String[]{"--", "__", "sm", "="});

        if (titel != null) {
            title.setText(titel);
        }

        //    textselect.setText(bady);
        if (refS != null) {
            ref.setText(refS);
            if (isFullLine == 0) {
                not_full_ref_line.setVisibility(View.VISIBLE);

            }else {
                refLine.setVisibility(View.VISIBLE);

            }

        } else {
            refLine.setVisibility(View.GONE);
        }

        highlights = helper.getListhighlightList(id);


        for (int i = 0; i < highlights.size(); i++) {
            int color;
            if (highlights.get(i).getColor_hex() == 1) {
                color = Color.TRANSPARENT;
            } else if (highlights.get(i).getColor_hex() == 2) {
                color = Color.YELLOW;
            } else {
                color = Color.RED;
            }
            textselect.getText().setSpan(new BackgroundColorSpan(color), highlights.get(i).getStart(), highlights.get(i).getEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }


//        relative.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (actionButtonAddComment.getVisibility() == View.VISIBLE) {
//                    actionButtonAddComment.setVisibility(View.GONE);
//                    sher.setVisibility(View.GONE);
//                } else {
//                    actionButtonAddComment.setVisibility(View.VISIBLE);
//                    sher.setVisibility(View.VISIBLE);
//                }
//
//            }
//        });


        return view;
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.actionButtonwhiht: {
                // textselect.getText().setSpan(new StyleSpan(Typeface.BOLD), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                 textselect.getText().setSpan(new ForegroundColorSpan(Color.BLUE), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textselect.getText().setSpan(new BackgroundColorSpan(Color.TRANSPARENT), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                helper.insertHighlight(textselect.getSelectionStart(), textselect.getSelectionEnd(), id, ColorId.WHITE);

                layout.setVisibility(View.GONE);
                textselect.clearFocus();

                break;
            }
            case R.id.actionButtonred: {
                textselect.getText().setSpan(new BackgroundColorSpan(Color.RED), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                helper.insertHighlight(textselect.getSelectionStart(), textselect.getSelectionEnd(), id, ColorId.RED);
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }
            case R.id.actionButtonyallo: {
                textselect.getText().setSpan(new BackgroundColorSpan(Color.YELLOW), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                helper.insertHighlight(textselect.getSelectionStart(), textselect.getSelectionEnd(), id, ColorId.YELLOW);
                // textselect.getText().setSpan(new StyleSpan(Typeface.ITALIC), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }
            case R.id.actionButtonsher: {
                String x = "";
                for (int i = textselect.getSelectionStart(); i < textselect.getSelectionEnd(); i++) {
                    x = x + textselect.getText().toString().charAt(i);
                }
                sherText(x);
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }
            case R.id.add_comment: {
                dilogComment(id);
                break;
            }
            case R.id.sher: {
                if (img != null) {
                    Intent intent = new Intent(getActivity(), ShaerActivity.class);
                    intent.putExtra("img", img);
                    intent.putExtra("text", bady);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                } else {
                    sherText(bady);
                }
                break;
            }

        }
    }

    @Override
    public void changefont() {


        pageSeting();


    }

    private void dilogComment(int pageId) {
        Button add_comment, cancel;
        final EditTextCursorWatcher comment_text;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilog_comment);
        add_comment = (Button) dialog.findViewById(R.id.add_comment);
        cancel = (Button) dialog.findViewById(R.id.cancel);
        comment_text = (EditTextCursorWatcher) dialog.findViewById(R.id.comment_text);

        if (comment != null) {
            comment_text.setText(comment);
            add_comment.setText("تعديل");
        }

        add_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (comment_text.getText().toString().equalsIgnoreCase("")) {
                    if (helper.insertComment(id, null)) {
                        comment = null;
                        actionButtonAddComment.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimaryDark)));
                    }
                } else if (helper.insertComment(id, comment_text.getText().toString())) {
                    comment = comment_text.getText().toString();
                    actionButtonAddComment.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yaloo)));
                }
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(comment_text.getWindowToken(), 0);

                dialog.cancel();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(comment_text.getWindowToken(), 0);
                dialog.cancel();

            }
        });

        dialog.show();
    }

    private void sherText(String bady) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "تطبيق كتاب الحج");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, bady);
        startActivity(Intent.createChooser(sharingIntent, "مشاركة النص"));
    }

    private void pageSeting() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "LotusLinotype-Light.otf");
        ref.setTypeface(tf, 1);
        textselect.setTextSize(preferenceClass.gettextSize());
        title.setTextSize(preferenceClass.gettextSize());
        ref.setTextSize(preferenceClass.gettextSize());
        if (preferenceClass.getscrenMode()) {
            page.setBackgroundColor(getResources().getColor(R.color.white));
            textselect.setTextColor(getResources().getColor(R.color.black));
            title.setTextColor(getResources().getColor(R.color.black));
            ref.setTextColor(getResources().getColor(R.color.black));

            sher.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_share));
        } else {
            page.setBackgroundColor(getResources().getColor(R.color.black));
            textselect.setTextColor(getResources().getColor(R.color.white));
            ref.setTextColor(getResources().getColor(R.color.white));
            title.setTextColor(getResources().getColor(R.color.white));

            sher.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_share_white));
        }

    }

    public void replace(String[] p, String[] r) {

        SpannableStringBuilder SS = null;
        String body = bady;

        final Pattern pattern1 = Pattern.compile(p[0]);
        final Matcher matcher1 = pattern1.matcher(body);

        final Pattern pattern2 = Pattern.compile(p[1]);
        final Matcher matcher2 = pattern2.matcher(body);


        final Pattern pattern3 = Pattern.compile(p[2]);
        final Matcher matcher3 = pattern3.matcher(body);

        final Pattern pattern4 = Pattern.compile(p[3]);
        final Matcher matcher4 = pattern4.matcher(body);

        int x = 0;

        body = body.replaceAll(r[0], "  ");
        body = body.replaceAll(r[1], "  ");
        body = body.replaceAll(r[2], "  ");
        body = body.replaceAll(r[3], " ");
        Matcher matcher = null;
        for (int i = 0; i < p.length; i++) {

            if (i == 0) {
                matcher = matcher1;

            } else if (i == 1) {
                matcher = matcher2;

            } else if (i == 2) {
                matcher = matcher3;

            }
            else if (i == 3) {
                Log.d("matcher4", matcher4 + "");
                matcher = matcher4;

            }
            try {

                if (SS == null) {
                    SS = new SpannableStringBuilder(body);
                }

                while (matcher.find()) {

                    if (i == 0) {
                        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "uthmani.otf");
                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    } else if (i == 1) {
                        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "LotusLinotype-Light.otf");
                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }
                    else if (i == 3) {
                        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "LotusLinotype-Bold.otf");
                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }

//                    x=x+4;
                }

            } catch (Exception e) {

                textselect.setText(bady);
                break;
            }

        }

        textselect.setText(SS);


    }


}
