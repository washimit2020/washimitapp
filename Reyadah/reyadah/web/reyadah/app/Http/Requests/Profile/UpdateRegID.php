<?php

namespace App\Http\Requests\Profile;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRegID extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password'=>'required|max:'.Constants::MAX_STRING_LENGTH,
            'reg_id'=>'required|max:1000',
        ];
    }
}
