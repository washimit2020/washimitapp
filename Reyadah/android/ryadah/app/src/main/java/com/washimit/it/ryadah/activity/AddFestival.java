package com.washimit.it.ryadah.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.registrationActivity.ImageAdd;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;

import org.json.JSONException;

import java.io.File;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddFestival extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.activity_add_festival_name)
    EditTextView activity_add_festival_name;
    @BindView(R.id.activity_add_festival_description)
    EditTextView activity_add_festival_description;
    @BindView(R.id.activity_add_festival_organisers)
    EditTextView activity_add_festival_organisers;
    @BindView(R.id.activity_add_festival_source)
    EditTextView activity_add_festival_source;

    @BindView(R.id.activity_add_festival_street_name)
    EditTextView activity_add_festival_street_name;
    @BindView(R.id.activity_add_festival_sector_name)
    EditTextView activity_add_festival_sector_name;
    @BindView(R.id.activity_add_festival_manger_name)
    EditTextView activity_add_festival_manger_name;
    @BindView(R.id.activity_add_festival_manger_phone)
    EditTextView activity_add_festival_manger_phone;

    @BindView(R.id.activity_add_festival_start_in)
    TextViewCast activity_add_festival_start_in;
    @BindView(R.id.activity_add_festival_from)
    TextViewCast activity_add_festival_from;
    @BindView(R.id.activity_add_festival_to)
    TextViewCast activity_add_festival_to;

    @BindView(R.id.activity_add_festival_location)
    ButtonView activity_add_festival_location;

    @BindView(R.id.festival_add_img)
    TextViewCast festival_add_img;

    @BindView(R.id.add_festival)
    ImageView add_festival;

    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.instgram)
    ImageView instgram;
    @BindView(R.id.snapchat)
    ImageView snapchat;
    @BindView(R.id.twitter)
    ImageView twitter;
    @BindView(R.id.whatsapp)
    ImageView whatsapp;
    @BindView(R.id.back)
    ImageView back;
    Bundle bundle;

    LatLng lng;
    ArrayList<Part> parts;
    ArrayList<File> files;
    Calendar calendar;
    ApiRequest request;
    SimpleDateFormat dateformatter, timeformatter;
    Intent intent;
    String facebook_ = "", instagram_ = "", twitter_ = "", snapchat_ = "", whatsapp_ = "";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant._TYPE_MAP) {

            if (data != null) {
                lng = (LatLng) data.getExtras().get("latLng");
                Log.d("LatLng", lng.latitude + " " + lng.longitude);
                parts.add(new StringPart("lat", lng.latitude + ""));
                parts.add(new StringPart("lng", lng.longitude + ""));
            } else {

            }


        } else if (requestCode == Constant._TYPE_PICTURE) {
            files.clear();
            if (data != null) {
                if (data.getExtras().get("caver1") != null) {
                    files.add(new File(data.getExtras().getString("caver1") + ""));
                    Log.d("caver1", data.getExtras().get("caver1") + "");
                }
                if (data.getExtras().get("caver2") != null) {
                    files.add(new File(data.getExtras().getString("caver2") + ""));
                    Log.d("caver2", data.getExtras().get("caver2") + "");
                }
                if (data.getExtras().get("caver3") != null) {
                    files.add(new File(data.getExtras().getString("caver3") + ""));
                    Log.d("caver3", data.getExtras().get("caver3") + "");
                }


            }


        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_festival);
        ButterKnife.bind(this);
        request = new ApiRequest(this);
        calendar = Calendar.getInstance();
        intent = new Intent(AddFestival.this, ImageAdd.class);


        parts = new ArrayList<>();
        files = new ArrayList<>();
        activity_add_festival_location.setOnClickListener(this);

        activity_add_festival_start_in.setOnClickListener(this);
        activity_add_festival_from.setOnClickListener(this);
        activity_add_festival_to.setOnClickListener(this);


        timeformatter = new SimpleDateFormat("H:mm");
        dateformatter = new SimpleDateFormat("yyyy-MM-dd");


        add_festival.setOnClickListener(this);
        festival_add_img.setOnClickListener(this);
        facebook.setOnClickListener(this);
        instgram.setOnClickListener(this);
        snapchat.setOnClickListener(this);
        back.setOnClickListener(this);
        twitter.setOnClickListener(this);
        whatsapp.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_add_festival_location:


                openActivity(MapsActivity.class, Constant._TYPE_MAP);


                break;
            case R.id.back:
                finish();
                break;

            case R.id.festival_add_img:

                intent.putExtra("requestType", Constant._TYPE_ADD_PRODUCT);
                startActivityForResult(intent, Constant._TYPE_PICTURE);
                break;
            case R.id.add_festival:


                if (validation()) {


                    requstFestivals();

                }

                break;
            case R.id.activity_add_festival_start_in:

                showDialog(999);

                break;
            case R.id.activity_add_festival_from:


                showDialog(998);


                break;
            case R.id.activity_add_festival_to:

                if (!activity_add_festival_from.getText().toString().equalsIgnoreCase("") && activity_add_festival_from.getText().toString().length() <= 5) {
                    showDialog(997);

                } else {
                    Toast.makeText(getApplicationContext(), "يجب اختيار موعد اانطلاق اولاَ", Toast.LENGTH_LONG).show();
                }


                break;


            case R.id.facebook:


                openDialog(0);


                break;

            case R.id.twitter:


                openDialog(1);


                break;

            case R.id.instgram:


                openDialog(2);


                break;

            case R.id.snapchat:


                openDialog(3);


                break;

            case R.id.whatsapp:


                openDialog(4);


                break;


        }
    }

    public void openActivity(Class activity, int requstCode) {

        Intent intent = new Intent(AddFestival.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, requstCode);

    }

    private void requstFestivals() {
        parts.add(new StringPart("name", activity_add_festival_name.getText().toString()));
        parts.add(new StringPart("street_name", activity_add_festival_street_name.getText().toString()));
        parts.add(new StringPart("sector_name", activity_add_festival_sector_name.getText().toString()));
        parts.add(new StringPart("organizer_name", activity_add_festival_manger_name.getText().toString()));
        parts.add(new StringPart("organization", activity_add_festival_organisers.getText().toString()));
        parts.add(new StringPart("source", activity_add_festival_source.getText().toString()));
        parts.add(new StringPart("organizer_phone_number", activity_add_festival_manger_phone.getText().toString()));
        parts.add(new StringPart("info", activity_add_festival_description.getText().toString()));
        parts.add(new StringPart("start_in", activity_add_festival_start_in.getText().toString()));
        parts.add(new StringPart("to", activity_add_festival_to.getText().toString()));
        parts.add(new StringPart("from", activity_add_festival_from.getText().toString()));
        parts.add(new StringPart("facebook", facebook_));
        parts.add(new StringPart("twitter", twitter_));
        parts.add(new StringPart("instagram", instagram_));
        parts.add(new StringPart("snapchat", snapchat_));
        parts.add(new StringPart("whatsapp", whatsapp_));
        for (int i = 0; i < files.size(); i++) {
            parts.add(new FilePart("images[" + i + "]", files.get(i)));
        }

        request.MultipartRequest("POST", "festivals", "جاري طلب انشاء مهرجان", parts, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("festivals", result);
                finish();
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("festivals", errorMessage);
                Toast.makeText(getApplicationContext(), "توجد مشكلة ما يرجا المحاولة فيما بعد!", Toast.LENGTH_LONG).show();

            }
        });

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 999) {
            return new DatePickerDialog(this, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    Log.d("date", getDate(year, month, dayOfMonth) + "");
                    if (year >= calendar.get(Calendar.YEAR) && month >= calendar.get(Calendar.MONTH) && dayOfMonth >= calendar.get(Calendar.DAY_OF_MONTH)) {
                        activity_add_festival_start_in.setText(getDate(year, month, dayOfMonth));
                    } else {
                        activity_add_festival_start_in.setText("يجب اختيار تاريخ صحيح");
                    }
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        } else if (id == 998) {
            return new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Log.d("time", getTime(hourOfDay, minute) + "");
                    Log.d("time", hourOfDay + "" + minute);
                    activity_add_festival_from.setText(getTime(hourOfDay, minute));
                    activity_add_festival_to.setText("");
                }
            }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);

        } else if (id == 997) {
            return new TimePickerDialog(this, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    Log.d("time", getTime(hourOfDay, minute) + "");

                    Date d1 = null;
                    Date d2 = null;

                    try {
                        d1 = timeformatter.parse(activity_add_festival_from.getText().toString());
                        d2 = timeformatter.parse(hourOfDay + ":" + minute);

                        //in milliseconds
                        long diff = d2.getTime() - d1.getTime();

                        if (diff > 0) {
                            activity_add_festival_to.setText(getTime(hourOfDay, minute));
                        } else {
                            activity_add_festival_to.setText("يجب اختيار وقت بعد موعد فتح المهرجان");
                        }

                    } catch (Exception e) {
                        // TODO: handle exception
                    }


                }
            }, calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);
        }
        return null;

    }


    private String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        return timeformatter.format(tme);
    }

    private String getDate(int year, int month, int dayOfMonth) {
        Date date = new Date(year - 1900, month, dayOfMonth);
        return dateformatter.format(date);
    }


    public boolean validation() {


        if (activity_add_festival_name.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_name.setError(getResources().getString(R.string.fill_name));
            activity_add_festival_name.requestFocus();
            return false;
        }
        if (activity_add_festival_description.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_description.setError(getResources().getString(R.string.fill_desc));
            activity_add_festival_description.requestFocus();

            return false;
        }

        if (activity_add_festival_organisers.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_organisers.setError(getResources().getString(R.string.fill_organisers));
            activity_add_festival_organisers.requestFocus();
            return false;
        }

        if (activity_add_festival_source.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_source.setError(getResources().getString(R.string.fill_street));
            activity_add_festival_source.requestFocus();

            return false;
        }
        if (activity_add_festival_start_in.getText().toString().equalsIgnoreCase("") && activity_add_festival_start_in.getText().toString().length() > 10) {
            activity_add_festival_start_in.setText(getResources().getString(R.string.fill_date));
            activity_add_festival_start_in.requestFocus();
            return false;
        }
        if (activity_add_festival_from.getText().toString().equalsIgnoreCase("") && activity_add_festival_from.getText().toString().length() > 10) {
            activity_add_festival_from.setText(getResources().getString(R.string.fill_date));
            activity_add_festival_from.requestFocus();
            return false;
        }

        if (activity_add_festival_to.getText().toString().equalsIgnoreCase("") && activity_add_festival_to.getText().toString().length() > 10) {
            activity_add_festival_to.setText(getResources().getString(R.string.fill_date));
            activity_add_festival_to.requestFocus();
            return false;
        }

        if (activity_add_festival_street_name.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_street_name.setError(getResources().getString(R.string.fill_street));
            activity_add_festival_street_name.requestFocus();
            return false;
        }
        if (activity_add_festival_sector_name.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_sector_name.setError(getResources().getString(R.string.fill_sector));
            activity_add_festival_sector_name.requestFocus();
            return false;
        }

        if (activity_add_festival_manger_name.getText().toString().equalsIgnoreCase("")) {
            activity_add_festival_manger_name.setError(getResources().getString(R.string.fill_name));
            activity_add_festival_manger_name.requestFocus();
            return false;
        }

        if (!(Patterns.PHONE.matcher(activity_add_festival_manger_phone.getText().toString()).matches() && activity_add_festival_manger_phone.getText().toString().length() >= 10)) {
            activity_add_festival_manger_phone.setError(getResources().getString(R.string.fill_mobile_number));
            activity_add_festival_manger_phone.requestFocus();
            return false;
        }

        return true;
    }

    private void openDialog(final int x) {
        AapHalper.CreateDialog(R.layout.dialog_social_media, this, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final EditTextView text = (EditTextView) dialog.findViewById(R.id.dialog_social_media_text);
                ButtonView ok = (ButtonView) dialog.findViewById(R.id.dialog_social_media_ok);
                ButtonView cancel = (ButtonView) dialog.findViewById(R.id.dialog_social_media_cancel);


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        switch (x) {

                            case 0:
                                facebook_ = text.getText().toString();
                                break;
                            case 1:
                                twitter_ = text.getText().toString();
                                break;
                            case 2:
                                instagram_ = text.getText().toString();
                                break;
                            case 3:
                                snapchat_ = text.getText().toString();
                                break;

                            case 4:
                                whatsapp_ = text.getText().toString();
                                break;
                        }

                        dialog.dismiss();

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

            }
        });


    }
}
