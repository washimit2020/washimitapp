<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        \Illuminate\Support\Facades\DB::table('pages')->insert([
                [
                    'title' => "pages.articles",
                ],
                [
                    'title' => "pages.books",
                ],
                [
                    'title' => "pages.audio",
                ],
                [
                    'title' => "pages.agents",
                ],
            ]

        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
