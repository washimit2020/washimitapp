package com.it.washim.bakkar.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.MainActivity;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.model.NavItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 8/13/2017.
 */

public class NavBarAdapter extends RecyclerView.Adapter {
    Context context;
    ArrayList<NavItem> list;

    public NavBarAdapter(Context context, ArrayList<NavItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NavBarViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nav_bar, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof NavBarViewHolder) {
            final NavItem navItem = list.get(position);
            ((NavBarViewHolder) holder).name.setText(navItem.name);
            ((NavBarViewHolder) holder).image.setImageDrawable(context.getResources().getDrawable(navItem.img));
            ((NavBarViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).openPage(navItem.id);
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class NavBarViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        MyTextView name;
        @BindView(R.id.image)
        ImageView image;

        public NavBarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
           // itemView.setBackground(context.getResources().getDrawable(R.drawable.slider_item_backgrond));
        }
    }
}
