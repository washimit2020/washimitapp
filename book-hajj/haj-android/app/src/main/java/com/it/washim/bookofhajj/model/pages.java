package com.it.washim.bookofhajj.model;

/**
 * Created by washm on 4/2/17.
 */

public class pages {
    private int id;
    private String titel;
    private String bady;
    private int isWishilisted;
    private String comment;
    private int isFullLine;
    private int isRead;
    private String references;

    //  private List<images> images;
    private String images;


    public pages(int id, String titel, String bady, int isWishilisted, String comment,String references, int isFullLine, int isRead, String images) {
        this.id = id;
        this.titel = titel;
        this.bady = bady;
        this.isFullLine = isFullLine;
        this.isWishilisted = isWishilisted;
        this.comment = comment;
        this.images = images;
        this.isRead = isRead;
        this.references=references;
    }

    public int getIsFullLine() {
        return isFullLine;
    }

    public void setIsFullLine(int isFullLine) {
        this.isFullLine = isFullLine;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public String getReferences() {
        return references;
    }

    public void setReferences(String references) {
        this.references = references;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBady() {
        return bady;
    }

    public void setBady(String bady) {
        this.bady = bady;
    }

    public int getIsWishilisted() {
        return isWishilisted;
    }

    public void setIsWishilisted(int isWishilisted) {
        this.isWishilisted = isWishilisted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }
}