package com.washimit.it.ryadah.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.FestivalDetailsActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.activity.PlaceDetailsActivity;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.model.Location;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammed Ali on 04/30/2017.
 */

public class SharchAdapter extends RecyclerView.Adapter {

    private final int VIEW_PROG = 0;
    public final int PRODUCT = 1;
    public final int USER = 2;
    public final int FESTIVAL = 3;


    List<?> list;
    Activity context;
    Gson gson;

    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;
    int numberOfColmen;
    Intent intent;

    public void setNumberOfColmen(int numberOfColmen) {
        this.numberOfColmen = numberOfColmen;
    }

    public SharchAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

            Log.d("ok", "ok");
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        } else if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    if (url != null) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {
                            // End has been reached
                            // Do something
                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {
                                loadMoreListener.onLoadMore();
                            }
                            isLoading = true;
                        }
                    }
                }
            });
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == PRODUCT) {
            return new ProductHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_sharch, parent, false));

        } else if (viewType == USER) {
            return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_shearch, parent, false));

        } else if (viewType == FESTIVAL) {
            return new FestivalHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_festival_shearch, parent, false));

        } else {

            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_load_more, parent, false));

        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ProductHolder) {
            Product product = (Product) list.get(position);
            ((ProductHolder) holder).item_product_sharch_product_name.setText(product.getName());
            ((ProductHolder) holder).item_product_sharch_product_price.setText(product.getPrice() + "");
            ((ProductHolder) holder).item_product_sharch_user_name.setText(product.getUser().getName());
            ((ProductHolder) holder).item_product_sharch_user_icon_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(product.getUser().getType().getId())));
            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_store).into(((ProductHolder) holder).item_product_sharch_img);
            Picasso.with(context).load(Constant._AVATAR_URL + product.getUser_id()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((ProductHolder) holder).item_product_sharch_user_avatar);

        } else if (holder instanceof UserHolder) {
            final User user = (User) list.get(position);
            ((UserHolder) holder).item_place_shearch_location.setText(user.getSector_name() + " - " + user.getStreet_name());
            ((UserHolder) holder).item_place_shearch_name.setText(user.getName());
            ((UserHolder) holder).item_place_shearch_rating.setRating((float) user.getRating());
            Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((UserHolder) holder).item_place_shearch_img);
            ((UserHolder) holder).item_place_shearch_follow.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(user.getFollowed())));


            ((UserHolder) holder).item_place_shearch_map_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(context, MapsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("location", gson.toJson(new Location(new LatLng(user.getLat(), user.getLng()), user.getType().getId()), Location.class));
                    context.startActivity(intent);

                }
            });


        } else if (holder instanceof FestivalHolder) {
            final Festivals festival = (Festivals) list.get(position);


            ((FestivalHolder) holder).item_festival_shearch_name.setText(festival.getName());

            ((FestivalHolder) holder).item_festival_shearch_location.setText(festival.getStreet_name() + "-" + festival.getSector_name());
            Picasso.with(context).load(Constant._FESTIVAL_URL + festival.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((FestivalHolder) holder).item_festival_shearch_img);


            ((FestivalHolder) holder).item_festival_shearch_map_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(context, MapsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    intent.putExtra("location", gson.toJson(new Location(new LatLng(festival.getLat(), festival.getLng()), festival.getUser().getType().getId()), Location.class));
                    context.startActivity(intent);


                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) == null) {
            return VIEW_PROG;
        }
        if (list.get(position) instanceof Product) {
            return PRODUCT;


        } else if (list.get(position) instanceof User) {
            return USER;

        } else if (list.get(position) instanceof Festivals) {
            return FESTIVAL;


        }

        return super.getItemViewType(position);
    }


    public class ProductHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_product_sharch_img)
        ImageView item_product_sharch_img;
        @BindView(R.id.item_product_sharch_user_icon_type)
        ImageView item_product_sharch_user_icon_type;
        @BindView(R.id.item_product_sharch_user_avatar)
        ImageView item_product_sharch_user_avatar;
        @BindView(R.id.item_product_sharch_product_name)
        TextView item_product_sharch_product_name;
        @BindView(R.id.item_product_sharch_product_price)
        TextView item_product_sharch_product_price;
        @BindView(R.id.item_product_sharch_user_name)
        TextView item_product_sharch_user_name;

        public ProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_product_sharch_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());
                    openActivity(ProductDetailsActivity.class, product.getUser().getType().getId(), gson.toJson(product));

                }
            });
        }
    }

    public class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_place_shearch_img)
        ImageView item_place_shearch_img;
        @BindView(R.id.item_place_shearch_follow)
        ImageView item_place_shearch_follow;
        @BindView(R.id.item_place_shearch_map_location)
        ImageView item_place_shearch_map_location;

        @BindView(R.id.item_place_shearch_name)
        TextView item_place_shearch_name;
        @BindView(R.id.item_place_shearch_location)
        TextView item_place_shearch_location;

        @BindView(R.id.item_place_shearch_rating)
        RatingBar item_place_shearch_rating;


        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_place_shearch_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());
                    if (user.getFollowed() == 0) {
                        item_place_shearch_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unfollow));
                        user.setFollowed(1);
                        user.setFollowed_by_count(user.getFollowed_by_count() + 1);

                    } else {
                        item_place_shearch_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow));
                        user.setFollowed(0);
                        user.setFollowed_by_count(user.getFollowed_by_count() - 1);

                    }
                    follow(Constant._TYPE_FOLLOW_USER, user.getId());


                }
            });


            item_place_shearch_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());
                    openActivity(PlaceDetailsActivity.class, user.getType().getId(), gson.toJson(user));

                }
            });

        }
    }

    public class FestivalHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_festival_shearch_img)
        ImageView item_festival_shearch_img;
        @BindView(R.id.item_festival_shearch_map_location)
        ImageView item_festival_shearch_map_location;
        @BindView(R.id.item_festival_shearch_name)
        TextView item_festival_shearch_name;
        @BindView(R.id.item_festival_shearch_location)
        TextView item_festival_shearch_location;

        public FestivalHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openActivity(FestivalDetailsActivity.class, Constant._TYPE_FESTIVAL, gson.toJson(list.get(getAdapterPosition())));

                }
            });
        }


    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }


    private void follow(int followType, int userId) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));
        parts.add(new StringPart("user_id", userId + ""));
        AapHalper.userAction("follow", parts, context, null);
    }

    private void openActivity(Class activity, int userType, String data) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        intent.putExtra("data", data);
        context.startActivity(intent);

    }

    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
