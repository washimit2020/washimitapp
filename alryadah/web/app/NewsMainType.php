<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsMainType extends Type
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'id' ,'name_ar','name_en','is_active'
    ];

    public function sub(){
        return $this->hasMany('App\NewsSubType','news_main_type_id');
    }
}
