package com.it.washim.wojooohapp.activitys;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.bumptech.glide.Glide;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.BookAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookDetailsActivity extends BaseActivity {
    Post post;

    @BindView(R.id.book_name)
    MyTextView book_name;
    @BindView(R.id.book_categre)
    MyTextView book_categre;
    @BindView(R.id.book_user_name)
    MyTextView book_user_name;
    @BindView(R.id.book_print_number)
    MyTextView book_print_number;
    @BindView(R.id.book_page_number)
    MyTextView book_page_number;
    @BindView(R.id.book_page_size)
    MyTextView book_page_size;
    @BindView(R.id.book_last_publish)
    MyTextView book_last_publish;
    @BindView(R.id.book_prise_dollar)
    MyTextView book_prise_dollar;
    @BindView(R.id.book_prise_ksa)
    MyTextView book_prise_ksa;
    @BindView(R.id.book_download_number)
    MyTextView book_download_number;
    @BindView(R.id.book_content)
    MyTextView book_content;
    @BindView(R.id.book_download)
    MyButtonView book_download;
    @BindView(R.id.book_image)
    ImageView book_image;
    @BindView(R.id.book_rating)
    RatingBar book_rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            post = getGson().fromJson(getIntent().getExtras().getString("book"), Post.class);

        }

        if (post != null) {
            if (post.attachments.size() > 0) {
                Glide.with(this)
                        .load(Constant.IMAGE_URL + post.attachments.get(0).id)
                        .into(book_image);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    book_image .setImageDrawable(getResources().getDrawable(R.drawable.defult,null));
                }else {
                    book_image .setImageDrawable(getResources().getDrawable(R.drawable.defult));

                }
            }
            book_name.setText(post.title);
            book_categre.setText(post.categories.get(0).title);
            book_user_name.setText(post.author);
            book_print_number.setText("الطبعة: " + post.post_meta.book.publish_count);
            book_page_number.setText("عدد الصفحات: " + post.post_meta.book.pages_count);
            book_page_size.setText("القطع: " + post.post_meta.book.page_size);
            book_last_publish.setText("أخر طبعة: " + post.post_meta.book.last_publish);
            book_prise_dollar.setText("US " + Integer.parseInt(post.post_meta.book.price) * 0.266);
            book_prise_ksa.setText("KS " + post.post_meta.book.price);
            book_download_number.setText(post.post_meta.book.download_count + "");
            book_content.setText(post.content);
            book_rating.setRating(Float.parseFloat(post.post_meta.book.rating));


//             book_download;

        }
    }

}
