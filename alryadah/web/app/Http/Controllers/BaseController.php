<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Constants;
use App\UserRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BaseController extends Controller
{
    function __construct()
    {
        App::setLocale("ar");
    }

}
