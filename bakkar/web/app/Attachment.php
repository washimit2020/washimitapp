<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $fillable = ["path", "mime", "extension", "post_id",'attachment_type_id'];
    protected $hidden =["path","post_id"];


    public function post()
    {
        return $this->belongsTo(Post::class,'post_id');
    }
}
