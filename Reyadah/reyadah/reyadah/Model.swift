//
//  Model.swift
//  reyadah
//
//  Created by WASHM on 4/18/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import SwiftyJSON

protocol ModelProtocol {
    mutating func map()
}

class Model: ModelProtocol {
   
    
    func map() {
        preconditionFailure("This method must be overridden")
    }
    
    
    var json:JSON!
  
    init(_ json:JSON ,beginWith:String? = nil) {
        
        
        if beginWith == nil || beginWith!.isEmpty{
            self.json = json
        }else{
            guard let msg = json["msg"].dictionaryValue[beginWith!] else{

                return
            }
            self.json = msg
        }
        map()
    }
  
    
}

