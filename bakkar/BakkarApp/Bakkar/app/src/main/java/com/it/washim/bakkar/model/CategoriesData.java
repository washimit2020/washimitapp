package com.it.washim.bakkar.model;

/**
 * Created by mohammad on 10/16/2017.
 */

public class CategoriesData {
    public int id;
    public String name;
    @Override
    public String toString() {
        return name;
    }
}
