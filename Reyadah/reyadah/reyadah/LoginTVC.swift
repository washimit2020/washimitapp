//
//  LoginTVC.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import SwiftyJSON
import GoogleSignIn
import FBSDKLoginKit
import TwitterKit

class LoginTVC: FormsTVC {
    
    //    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var facebook: AspectFitButton!
    @IBOutlet weak var google: AspectFitButton!
    @IBOutlet weak var twitter: AspectFitButton!
    
    let forgotPassword = "forgot-password"
    let register = "register"
    var user_type:Int!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fields = [(email, emailError),(password,passwordError)]
        
        validator.registerField(email, errorLabel: emailError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        
        // You can now pass in regex and length parameters through overloaded contructors
        validator.registerField(password, errorLabel: passwordError, rules: [RequiredRule(message: Constants.requiredErrorMsg), MinLengthRule(length: 6, message: Constants.minLengthErrorMsg)])
        
        postValues["type_id"] = "\(UserDefaultsHelper.get_current_user_type())"
        
        action = login(_:)
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        google.addTarget(self, action: #selector(googleSignIn), for: .touchUpInside)
        facebook.addTarget(self, action: #selector(handelCostumeFBLogin), for: .touchUpInside)
        twitter.addTarget(self, action: #selector(twitterSignIn), for: .touchUpInside)
    }
    
    
    
    
    func googleSignIn(_ sender:AspectFitButton)  {
        sender.isEnabled = false
        GIDSignIn.sharedInstance().signIn()
    }
    
    func twitterSignIn(_ sender:AspectFitButton){
        sender.isEnabled = false
        
        Twitter.sharedInstance().logIn { (session, error) in
            if let error = error{
                print(error.localizedDescription)
                sender.isEnabled = true
                return
            }
            guard let session = session else{
                print("ha6'a 7ywan btmnyak")
                sender.isEnabled = true

                return
            }
            self.didLoginWithTwitter(session: session)
        }
           
    }
    func didLoginWithTwitter(session:TWTRSession){
        let client = TWTRAPIClient.withCurrentUser()

        client.requestEmail { email, error in
            if (email != nil) {
                self.handelSocialLogin(token: session.userID, name: session.userName, email: email!)
            } else {
                print("error: \(error!.localizedDescription)");
                self.twitter.isEnabled = true
                return
                
            }
        }
    }
    
    func handelCostumeFBLogin(_ sender:AspectFitButton) {
        sender.isEnabled = false
        
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
            if let err = error{
                print(err.localizedDescription)
                sender.isEnabled = true
                return
            }
            
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email, name"]).start {
                (connection , result , error) in
                if let err = error{
                    print(err.localizedDescription)
                    sender.isEnabled = true

                    return
                }
                guard let result = result as? [String:String] , let id = result["id"],let name = result["name"],let email = result["email"] else{
                    sender.isEnabled = true

                    return
                }
                
                self.handelSocialLogin(token: id, name: name , email: email)
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? ForgotPasswordTVC {
            destination.apiRequest = request
        }else if let destination = segue.destination as? RegisterTVC{
            destination.postValues = postValues
        }
        
    }
    
    @IBAction func login(_ sender: Any) {
        
        change_to_old_status()
        validator.validate(self)
    }
    
    override func validationSuccessful() {
        postValues["username"] = email.text
        postValues["password"] = password.text
        
        request.make_request(parameters: postValues,
                             apiImages: nil, link: RequestLinks.login.rawValue,
                             method: .post,
                             dialogMsg: "جاري تسجيل الدخول",
                             completion: loginHandler)
    }
    
    
    

    
}

extension LoginTVC:GIDSignInUIDelegate,GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error{
           google.isEnabled = true

            print(error.localizedDescription)
            return
        }
        
        handelSocialLogin(token: user.userID, name: user.profile.name, email: user.profile.email)
    }
    
     func loginHandler(_ response:DataRequest?)->Void{
        
        guard let response = response else {
            self.request.handle_error_dialog(nil)
            return
        }
        response.responseJSON(completionHandler: {
            response in
            
            if response.response?.statusCode == 422{
                self.request.handle_error_dialog(JSON(response.data!)["username"].stringValue)
            }
            else if response.response?.statusCode == 200{
                
                UserDefaultsHelper.store_user_data(response.data!)
                
                self.request.costumeAlert.dismiss(animated: true, completion:{
                    
                    UserDefaultsHelper.store_is_first()
                    make_sidebar()

                })
            }else{
                self.request.handle_error_dialog(nil)
            }
        })
        
    }
    
    func handelSocialLogin(token:String,name:String,email:String){
        
        UserDefaultsHelper.store_social_token(token: token)
   
        self.google.isEnabled = true
        self.facebook.isEnabled = true
        self.twitter.isEnabled = true
        
        postValues = [
            "email":email,
            "token":token,
            "name":name,
            "type_id":"\(UserDefaultsHelper.get_current_user_type())",
            "reg_id":"dsadas",
            "login_type":"\(Constants.SOCIAL_REGISTER)",
        ]
        
        request.make_request(parameters: postValues, apiImages: nil, link: RequestLinks.register.rawValue, method: .post, dialogMsg: "جاري محاولة تسجيل الدخول", viewController: nil, completion: {
            $0?.responseJSON{
                response in
                if response.response?.statusCode == Constants.RESPONSE_CODE_UNPROCESSABLE{
                    self.request.costumeAlert.dismiss(animated: true, completion: {
                        self.performSegue(withIdentifier: self.register, sender: nil)
                    })
                }else{
                    UserDefaultsHelper.store_user_data(response.data!)
                    UserDefaultsHelper.store_is_first()

                    make_sidebar()

                }
            }
        })
    }
}




