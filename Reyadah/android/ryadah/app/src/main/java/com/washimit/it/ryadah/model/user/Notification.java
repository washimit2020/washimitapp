package com.washimit.it.ryadah.model.user;

import com.washimit.it.ryadah.model.details.Comment;

/**
 * Created by mohammad on 7/10/2017.
 */

public class Notification {
    public String msg;
    public Comment comment;
    public int id;
    public String type;
    public String title;

}
