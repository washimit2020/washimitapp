package com.washimit.it.ryadah.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Product;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by washm on 4/29/17.
 */

public class ExplorerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_PROG = 0;
    private final int PRODUCT = 1;

    private int usersType;
    List<Product> list;
    Activity context;
    Gson gson;
    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;

    public ExplorerAdapter(List<Product> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {
                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                        Log.d("notNul", totalItemCount +"<="+(lastVisibleItem + 3)+"yas"+isLoading);


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {
                            Log.d("notNul", totalItemCount +"<="+(lastVisibleItem + 3)+"yas");
                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {
                                Log.d("loadMoreListener", "yas");

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder instanceof ProductViewHolder) {
            Product product = list.get(i);
            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_store).into(((ProductViewHolder) viewHolder).item_explorer_img);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == VIEW_PROG) {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));

        } else {

        }
        return new ProductViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_explorer, viewGroup, false));
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) == null) {
            return VIEW_PROG;
        }
        return PRODUCT;
    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_explorer_img)
        ImageView item_explorer_img;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("data", gson.toJson(list.get(getAdapterPosition())));
                    context.startActivity(intent);
                }
            });
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }

    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }


}