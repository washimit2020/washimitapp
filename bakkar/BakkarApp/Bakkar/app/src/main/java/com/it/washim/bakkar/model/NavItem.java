package com.it.washim.bakkar.model;

/**
 * Created by mohammad on 8/13/2017.
 */

public class NavItem {
    public String name;
    public int img;
    public int id;

    public NavItem(String name, int img, int id) {
        this.name = name;
        this.img = img;
        this.id = id;
    }
}
