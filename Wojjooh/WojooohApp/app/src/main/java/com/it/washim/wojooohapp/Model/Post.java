package com.it.washim.wojooohapp.Model;

import java.util.ArrayList;

/**
 * Created by mohammad on 9/19/2017.
 */

public class Post {
public int id;
public String title;
public String content;
public int page_id;
public int parent_id;
public ArrayList<Categorie> categories;
public PostMeta post_meta;
public ArrayList<Post> children;
public ArrayList<Attachment> attachments;
}
