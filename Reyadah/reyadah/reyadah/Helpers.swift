//
//  Helpers.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

func get_font_name()->String{
    return "TheSansArab"
}

func change_ui(){
    
    UIBarButtonItem.appearance()
        .setTitleTextAttributes(
            [NSFontAttributeName: UIFont(name: get_font_name()+"-Plain", size: 10)!,
             NSForegroundColorAttributeName:Constants.themeColor],
            for: UIControlState.normal)
    UITabBarItem.appearance()
        .setTitleTextAttributes(
            [NSFontAttributeName: UIFont(name: get_font_name()+"-Plain", size: 9)!,
             NSForegroundColorAttributeName:Constants.themeColor],
            for: UIControlState.normal)
    
    UITextField.appearance().substituteFontName = get_font_name()
    UILabel.appearance().substituteFontName = get_font_name()
    UITextView.appearance().substituteFontName = get_font_name()
    
}

func make_materialCell(_ cell:UICollectionViewCell){
    cell.backgroundColor = UIColor.white
    cell.layer.masksToBounds = false;
    cell.layer.borderColor = UIColor.white.cgColor;
    cell.layer.cornerRadius = 3
    cell.layer.borderWidth = 7.0;
    cell.layer.shadowOpacity = 0.75;
    cell.layer.shadowRadius = 5.0;
    cell.layer.shadowOffset = CGSize.zero;
    cell.layer.shadowPath = UIBezierPath(rect: cell.bounds).cgPath;
    cell.layer.shouldRasterize = false;
    
    
    //    UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 5, options: [],
    //                   animations: {
    //                    cell.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)},
    //                   completion: { finished in
    //                    UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 5, options: .curveEaseIn,
    //                                   animations: {
    //                                    cell.transform = CGAffineTransform(scaleX: 1, y: 1)
    //                    },completion: nil)})
    
}

func make_loading_dialog(_ dialogMsg:String)->LoadingDialog_ViewController{
    let costumeAlert = LoadingDialog_ViewController(nibName: "LoadingDialog_ViewController", bundle: nil)
    costumeAlert.msg = dialogMsg
    return costumeAlert
}

func get_product_placeholder()->UIImage{
    return UIImage(named:"defaultImage")!
}

func add_grey_border(layer:CALayer){
    layer.borderColor = UIColor.lightGray.cgColor
    layer.borderWidth = 1
    layer.cornerRadius = 5
}


func get_user_avatar_link(id:Int?)->URL?{
    guard let id = id else {
        return nil
    }
    
    let link = Constants.host+Constants.GET_USER_AVATAR+"\(id)"
    let avatar = URL(string: link)
    return avatar
}

func get_cover_links(id:Int?,url:String = Constants.GET_USER_COVER)->([URL?]){
    
    guard let id = id else {
        return []
    }
    
    let link = Constants.host+url+"\(id)/"
    var links = [URL?]()
    for i in 0...2{
        let cover = URL(string: link+"\(i)")
        links.append(cover)
    }
    return links
}

func make_slider_images(id:Int?,placeholder:String? = nil , url:String = Constants.GET_USER_COVER)->[KingfisherSource?]{
    guard let id = id else {
        return []
    }
    
    let links = get_cover_links(id: id,url: url)
    var fisher = [KingfisherSource?]()
    var image:UIImage?
    
    if let placeholder = placeholder{
        image = UIImage(named:placeholder)
    }
    
    for i in 0...2{
        fisher.append(KingfisherSource(url: links[i]!,placeholder: image))
    }
    
    return fisher
}

func make_location_text(_ user:User)->String{
    return user.street_name! + " - " + user.sector_name!
}

func make_status_text(_ status:UILabel, _ id:Int){
    switch id {
    case Constants.STATUS_ACTIVE:
        status.text = "مقبول"
        status.textColor = Constants.themeColor
        
    case Constants.STATUS_DECLINED:
        status.text = "مرفوض"
        status.textColor = UIColor.red
    default:
        status.text = "قيد الدراسة"
        status.textColor = UIColor.gray
    }
}


func check_user_follow_status(user:User)->UIImage{
    var image = "ic_follow"
    
    if user.followed! {
        image = "ic_unfollow"
    }
    
    return UIImage(named:image)!
}

func change_tint_of_button(button:UIButton , checked:Bool){
    if checked {
        button.tintColor = Constants.themeColor
    }else{
        button.tintColor = UIColor.gray
    }
    
}

func set_price(_ price:String)->String!{
    return "\(price) ريال"
}
func set_likes(_ likes: String)-> String{
    return likes + " likes"
}

func shareProductToMedia(image:UIImage,name:String)->UIActivityViewController{
let msg = "شاركني هذه التجربمة اللعينة"
    
    let activityViewController:UIActivityViewController = UIActivityViewController(activityItems:  [image, msg,name], applicationActivities: nil)
    activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]

    return activityViewController
}


func perform_locations_segue(ProductOrUser model:Any? = nil,ViewController vc:UIViewController ,_ apiRequest:ApiRequest){
    let locations = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.locations.rawValue) as! Locations_ViewController
    locations.apiRequest = apiRequest
    if model == nil{
        let branch = UserDefaultsHelper.get_user_data() as? Branch
        locations.branch = branch
    }
    else if model is Product{
        locations.product = model as? Product
    }else{
        locations.branch = model as? Branch
    }
    
    vc.navigationController?.pushViewController(locations, animated: true)
}
