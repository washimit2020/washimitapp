//
//  Search_TableViewController.swift
//  al-haj
//
//  Created by WASHM on 7/26/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite

class Search_TableViewController: BasePageGetter_TableViewController {
    
    @IBOutlet weak var searchText: C_UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    
    override func viewDidLoad() {
        searchText.delegate = self
        searchIcon.tintColor = #colorLiteral(red: 0.8553363681, green: 0.8105258346, blue: 0.7671664357, alpha: 1)
        searchIcon.image = searchIcon.image?.withRenderingMode(.alwaysTemplate)
        super.viewDidLoad()
        title = " "
        self.pages = Utils.shared.pages
        tableView.reloadData()
        tableView.tableHeaderView = nil
    }
    @IBOutlet weak var search: UIView!
    
    //    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    //        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PageTitle_TableViewCell
    //        cell.page = pages[indexPath.row]
    //        return cell
    //    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return search
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return search.frame.height
    }
  
}

extension Search_TableViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        offset = 0
        has_next = true
        self.pages = Utils.shared.pages.filter({ (page) -> Bool in
            var like = false
            if let title = page.title{
                like = title.contains(searchText.text!)
            }
            if let body = page.body{
                like = like || body.string.contains(searchText.text!)
            }
            return like
        })

       tableView.reloadData()
        
        
        return true
    }
}
