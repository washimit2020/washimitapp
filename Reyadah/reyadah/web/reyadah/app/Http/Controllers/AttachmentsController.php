<?php

namespace App\Http\Controllers;


use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\Attachment;
use App\Http\Constants;
use App\User;
use Illuminate\Support\Facades\Storage;

/**
 * a class for the attachments
 *
 * Class AttachmentsController
 * @package App\Http\Controllers
 */
class AttachmentsController extends Controller
{
    /**
     * to ge the avatar of the user
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function avatar(User $user)
    {
        list($file, $mime) = $this->get_attachment_instance($user, Constants::_TYPE_AVATAR_IMAGE);

        return response()->make($file, 200, ['content-type' => $mime]);
    }

    /**
     * to get the user cover image
     *
     * @param User $user
     * @param int $num
     * @return \Illuminate\Http\Response
     */
    public function cover(User $user, $num)
    {
        list($file, $mime) = $this->get_attachment_instance($user, Constants::_TYPE_COVER_IMAGE, $num);

        return response()->make($file, 200, ['content-type' => $mime]);
    }

    /**
     * get the product image
     *
     * @param Product $product
     * @param $num
     * @return \Illuminate\Http\Response
     */
    public function product(Product $product, $num)
    {
        list($file, $mime) = $this->get_attachment_instance($product, Constants::_TYPE_PRODUCT_ATTACHMENT, $num);

        return response()->make($file, 200, ['content-type' => $mime]);
    }

    /**
     * get the festival image
     *
     * @param Festival $festival
     * @param $num
     * @return \Illuminate\Http\Response
     */
    public function festival(Festival $festival, $num)
    {
        list($file, $mime) = $this->get_attachment_instance($festival, Constants::_TYPE_FESTIVAL_ATTACHMENT, $num);

        return response()->make($file, 200, ['content-type' => $mime]);
    }
    /**
     * to get the instance and get the file
     *
     * @param $related
     * @param $type
     * @param null $num
     * @return array
     */
    private function get_attachment_instance($related, $type, $num = null)
    {
        $attachment = Attachment::where(
            [
                ['related_id', $related->id],
                ['type_id', $type]
            ]
        );

        if (isset($num)) {
            $attachment = $attachment->skip($num);
        }
        $attachment = $attachment->first();


        return $this->get_attachment_data($this->get_default_path($type),$attachment);
    }

    /**
     * to make the attachment default path
     *
     * @param $type
     * @return string
     */
    private function get_default_path($type){
        switch ($type){
            case Constants::_TYPE_PRODUCT_ATTACHMENT:
                $path = Constants::DEFAULT_PRODUCT;
                break;
            case Constants::_TYPE_COVER_IMAGE:
                $path = Constants::DEFAULT_COVER;
                break;
            case Constants::_TYPE_AVATAR_IMAGE:
                $path = Constants::DEFAULT_AVATAR;
                break;
            default:
                $path = Constants::DEFAULT_COVER;
        }
        return $path;
    }

    /**
     * to get the attachment data
     *
     * @param $default_path
     * @param Attachment|null $attachment
     * @return array
     */
    private function get_attachment_data($default_path, Attachment $attachment = null)
    {
        if (isset($attachment->path) && Storage::exists($attachment->path)) {

            $file = Storage::get($attachment->path);
            $mime = $attachment->mime_type;
        } else {
            $file = Storage::get($default_path);
            $mime = Storage::mimeType($default_path);
        }
        return [$file, $mime];
    }
}
