<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\Wishlist\Store;
use App\Wishlist;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class WishlistController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wishlist = Wishlist::with(['news' => function ($query) {
            $query->with(["sub_type", "main_type", "status", "user" => get_user_obj(), 'attachment' => function ($query) {
                $query->with('type')->get();
            }])->withCount(['views', 'likes', 'comments'])->get();
        }])->where('user_id', request()->user()->id)
            ->paginate(10);

        return response()->json(ResponseHelper::successResponse(["wishlist" => $wishlist]));
    }

    public function store(Store $request)
    {
        $data = [
            "news_id" => $request->news_id,
            "user_id" => $request->user()->id,
        ];

        $save = add_or_remove($data, new Wishlist());

        return response()->json(ResponseHelper::successResponse(["wishlisted" => $save]));
    }

}
