package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 1/16/17.
 */

public class wishlist {
    String created_at;
    String updated_at;
    news news;

    public wishlist(String created_at, String updated_at, User user, news news) {
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.news = news;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }



    public news getNews() {
        return news;
    }

    public void setNews(news news) {
        this.news = news;
    }
}
