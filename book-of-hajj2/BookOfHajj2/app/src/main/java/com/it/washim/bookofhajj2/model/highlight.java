package com.it.washim.bookofhajj2.model;

/**
 * Created by washm on 4/2/17.
 */

public class highlight {

    int start;
    int end;
    int color_hex;

    public highlight(int start, int end, int color_hex) {
        this.start = start;
        this.end = end;
        this.color_hex = color_hex;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public int getColor_hex() {
        return color_hex;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public void setColor_hex(int color_hex) {
        this.color_hex = color_hex;
    }
}
