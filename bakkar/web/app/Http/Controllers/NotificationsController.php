<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 6/18/17
 * Time: 12:20 PM
 */

namespace App\Http\Controllers;


use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Users_Wishlist;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    public function read(Request $request)
    {
//        $this->validate($request,[
//            'notification_id'=>'required|exists:posts,id'
//        ]);

        $request->user()->unread_notifications()->chunk(100,function ($notifications){
            foreach ($notifications as $notification){
                $notification->read();
            }
        });

        return response()->json(["notification" => "done"]);
    }


}