package com.it.washim.wojooohapp.Model;

/**
 * Created by mohammad on 9/19/2017.
 */

public class Book {
    public double rating;
    public String author;
    public int page_size;
    public String last_publish;
    public int pages_count;
    public int publish_count;
    public int price;
}
