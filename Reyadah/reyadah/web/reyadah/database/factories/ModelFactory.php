<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'phone_number' => $faker->unique()->phoneNumber,
        'password' => $password ?: $password = bcrypt('secret'),
        'info' => $faker->text,
        'street_name' => $faker->streetName,
        'sector_name' => $faker->streetAddress,
        'lat' => $faker->latitude,
        'lng' => $faker->longitude,
        'reg_id' => str_random(30),
        'api_token' => str_random(60),
        'type_id' => random_int(\App\Http\Constants::_TYPE_STORE,\App\Http\Constants::_TYPE_COSTUMER),
        'status_id' => \App\Http\Constants::_STATUS_ACTIVE,
        'social_media_id'  => function () {
        return factory(\App\SocialMedia::class)->create()->id;
    },
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\SocialMedia::class, function ($faker) {
    return [
        'facebook' => $faker->title,
        'twitter' => $faker->title,
        'instagram' => $faker->title,
        'whatsapp' => $faker->title,
        'snapchat' => $faker->title,
    ];
});

