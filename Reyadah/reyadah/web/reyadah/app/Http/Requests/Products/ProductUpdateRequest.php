<?php

namespace App\Http\Requests\Products;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required|max:'.Constants::MAX_STRING_LENGTH,
            "number_of_eaters" => 'required|numeric|max:10|min:1',
            "ingredients" => 'required|max:1000',
            "price" => 'required|numeric|max:999|min:0.5',
        ];
    }
}
