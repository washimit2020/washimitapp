package com.it.washim.wojooohapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Model.HomeItem;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.activitys.HomeActivity;
import com.it.washim.wojooohapp.activitys.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/13/2017.
 */

public class HomeAdapter extends RecyclerView.Adapter {
    private static final int HOME_ITEM = 1;
    private static final int HOME_ITEM_TO_ROW = 2;
    Context context;
    ArrayList<HomeItem> list;

    public HomeAdapter(Context context, ArrayList<HomeItem> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == HOME_ITEM) {
            return new HomeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home, parent, false));

        } else {
            return new HomeTowRowViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_home_for_tow_row, parent, false));


        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        HomeItem item = list.get(position);
        if (holder instanceof HomeViewHolder) {
            ((HomeViewHolder) holder).name.setText(item.name);


        } else {
            ((HomeTowRowViewHolder) holder).name.setText(item.name);

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position).type == HOME_ITEM ? HOME_ITEM : HOME_ITEM_TO_ROW;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.home_item)
        CardView item;
        @BindView(R.id.home_name)
        MyTextView name;

        public HomeViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivity) context).openActivity(context, MainActivity.class, "page_type", list.get(getAdapterPosition()).action);
                }
            });
        }
    }

    public class HomeTowRowViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.home_item)
        CardView item;
        @BindView(R.id.home_name)
        MyTextView name;


        public HomeTowRowViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivity) context).openActivity(context, MainActivity.class, "page_type", list.get(getAdapterPosition()).action);
                }
            });
        }
    }
}
