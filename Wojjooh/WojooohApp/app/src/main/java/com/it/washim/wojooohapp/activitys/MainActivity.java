package com.it.washim.wojooohapp.activitys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.fragment.BookFragment;
import com.it.washim.wojooohapp.fragment.ExpandableFragment;
import com.it.washim.wojooohapp.fragment.RequestBookFragment;
import com.it.washim.wojooohapp.fragment.RequestToPublishBookFragment;
import com.it.washim.wojooohapp.fragment.SalePointsFragment;
import com.it.washim.wojooohapp.fragment.SearchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.slide_menu)
    ImageView slideMenu;
    @BindView(R.id.page_name)
    MyTextView pageName;
    private Bundle bundle;
    private Bundle bundleFragment;
    SalePointsFragment salePointsFragment = new SalePointsFragment();
    BookFragment bookFragment = new BookFragment();
    SearchFragment searchFragment = new SearchFragment();
    RequestBookFragment requestBookFragment = new RequestBookFragment();
    RequestToPublishBookFragment requestToPublishBookFragment = new RequestToPublishBookFragment();
    ExpandableFragment expandableFragment = new ExpandableFragment();

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        bundleFragment = new Bundle();
        bundle = getIntent().getExtras();

        slideMenu.setOnClickListener(this);
        back.setOnClickListener(this);

        if (bundle != null) {

            selectFragment(bundle.getInt("page_type"));

        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }


    private void openFragment(Fragment fragment) {
        fragment.setArguments(bundleFragment);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, fragment).commit();
    }

    public void selectFragment(int type) {
        switch (type) {

            case Constant.PAGES_SELLING_PLACES:
             //   openFragment(salePointsFragment);
                openFragment(searchFragment);
                pageName.setText("منافذ التوزيع");
                break;
            case Constant.PAGES_BOOK:
                openFragment(bookFragment);
                pageName.setText("قائمة الكتب");

                break;

            case Constant.PAGES_BUY_REQUEST:
                openFragment(requestBookFragment);
                pageName.setText("طلب شراء");
                break;

            case Constant.PAGES_PUBLISH_REQUEST:
                openFragment(requestToPublishBookFragment);
                pageName.setText("طلب نشر");
                break;
            case Constant.PAGES_SERVICES:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SERVICES);
                pageName.setText("الخدمات الفنية");
                openFragment(expandableFragment);
                break;

            case Constant.PAGES_BOOKS_SHOWROOM:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_BOOKS_SHOWROOM);
                pageName.setText("معارض الكتب");
                openFragment(expandableFragment);
                break;
            case Constant.PAGES_SPONSOR:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SPONSOR);
                pageName.setText("وكلاء التوزيع");
                openFragment(expandableFragment);
                break;

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.slide_menu:
                if (!drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.openDrawer(GravityCompat.END);
                }
                break;

        }
    }
}
