//
//  AddLocation_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/17/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class AddLocation_ViewController: UIViewController {
    
    @IBOutlet weak var map: MKMapView!
    var resultSearchController:UISearchController? = nil
    
    var userLocationCallBack:((_ annotation:MKAnnotation?)->Void)?
    
    let manager = CLLocationManager()
    let identifier = "CustomAnnotation"
    
    lazy var annot = MKPointAnnotation()
    
    var current:CLLocationCoordinate2D?
    var resizedImage:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        manager.requestWhenInUseAuthorization()
        map.delegate = self
        map.showsUserLocation = false
        
        let button = MKUserTrackingBarButtonItem(mapView: map)
        
        navigationItem.rightBarButtonItem = button
        
        
        if CLLocationManager.locationServicesEnabled(){
            manager.delegate = self
            manager.desiredAccuracy = kCLLocationAccuracyBest
            manager.startUpdatingLocation()
        }
        
        let pinImage = UIImage(named: "ic_add_location")
        self.resizedImage = pinImage
        set_up_searchbar()

        if current != nil {
            annot.coordinate = current!
            map.removeAnnotations(map.annotations)
            map.showsUserLocation = false
            map.region = .init(center: current!, span: .init(latitudeDelta: 0.75, longitudeDelta: 0.75))
            map.addAnnotation(annot)

        }
        
        
//        let size = CGSize(width: 25, height: 25)
//        UIGraphicsBeginImageContext(size)
//        pinImage!.draw(in: CGRect(x:0, y:0, width: size.width, height: size.height))
//        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
        
    }
    
    
    @IBAction func addPin(_ sender: Any) {
        let coordination = map.convert((sender as AnyObject).location(in: map), toCoordinateFrom: map)
        
        annot.coordinate = coordination;
        map.removeAnnotations(map.annotations)
        map.showsUserLocation = false
        map.addAnnotation(annot)
        
    }
    
    private func set_up_searchbar(){
        
        let locationSearchTable = LocationSearch_TableViewController(nibName: "LocationSearch_TableViewController", bundle: nil)
        resultSearchController = UISearchController(searchResultsController: locationSearchTable)
        resultSearchController?.searchResultsUpdater = locationSearchTable
        locationSearchTable.map = map
        
        
        let searchBar = resultSearchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "Search for places"
        navigationItem.titleView = resultSearchController?.searchBar
        resultSearchController?.hidesNavigationBarDuringPresentation = false
        resultSearchController?.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        userLocationCallBack!(map.annotations.last)        
    }
    deinit {
        let overlays = map.overlays
        map.removeOverlays(overlays)
        map.delegate = nil
        map.removeFromSuperview()
        map = nil
    }
}
extension AddLocation_ViewController:CLLocationManagerDelegate,MKMapViewDelegate{
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        
            annotationView!.image = resizedImage   // go ahead and use forced unwrapping and you'll be notified if it can't be found; alternatively, use `guard` statement to accomplish the same thing and show a custom error message
        } else {
            annotationView!.annotation = annotation
        }
        
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        map.removeAnnotations(map.annotations)
    }
    
}


