package com.washimit.it.ryadah.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.WishlistedAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 */
public class WishlistedFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {


    @BindView(R.id.linearLayout_no_internt)
    LinearLayout linearLayout_no_internt;
    @BindView(R.id.retry)
    ButtonView retry;

    @BindView(R.id.recycler_wishlisted)
    RecyclerView recycler_wishlisted;
    @BindView(R.id.swipe_refresh_wishlisted)
    SwipeRefreshLayout swipe_refresh_wishlisted;

    @BindView(R.id.no_wishlist)
    TextViewCast no_wishlist;
    ArrayList<Product> list;
    WishlistedAdapter adapter;
    ApiRequest request;
    Gson gson;
    String url;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_wishlisted, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        request = new ApiRequest(getActivity());
        gson = new Gson();
        recycler_wishlisted.setHasFixedSize(true);

        recycler_wishlisted.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        adapter = new WishlistedAdapter(list, getActivity(), recycler_wishlisted);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                list.add(null);
                adapter.notifyItemInserted(list.size() - 1);
                getNext(url);
            }
        });

        recycler_wishlisted.setAdapter(adapter);
        swipe_refresh_wishlisted.setOnRefreshListener(this);


        swipe_refresh_wishlisted.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh_wishlisted.setRefreshing(true);
                getWishlist();
            }
        });

        retry.setOnClickListener(this);

        return view;
    }


    private void getWishlist() {
        request.MultipartRequest(Request.Method.GET, false, "product/wishlist", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                list.clear();
                JSONObject object;
                JSONObject msg;
                JSONObject wishlisted;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");

                wishlisted = msg.getJSONObject("wishlisted");
                url = wishlisted.getString("next_page_url");
                adapter.setUrl(url);

                list.addAll((Collection<? extends Product>) gson.fromJson(wishlisted.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));

                if (list.size() == 0) {
                    recycler_wishlisted.setVisibility(View.GONE);
                    no_wishlist.setVisibility(View.VISIBLE);
                } else {
                    recycler_wishlisted.setVisibility(View.VISIBLE);
                    no_wishlist.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();

                swipe_refresh_wishlisted.setRefreshing(false);
                Log.d("wishlist", result);
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                recycler_wishlisted.setVisibility(View.GONE);
                swipe_refresh_wishlisted.setRefreshing(false);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                Log.d("wishlist", errorMessage);
            }
        });


    }

    private void getNext(String urls) {
        request.MultipartRequest(Request.Method.GET, true, urls, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size() - 1);
                JSONObject object;
                JSONObject msg;
                JSONObject wishlisted;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");

                wishlisted = msg.getJSONObject("wishlisted");
                url = wishlisted.getString("next_page_url");
                adapter.setUrl(url);
                list.addAll((Collection<? extends Product>) gson.fromJson(wishlisted.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));

                if (list.size() == 0) {
                    recycler_wishlisted.setVisibility(View.GONE);
                    no_wishlist.setVisibility(View.VISIBLE);
                } else {
                    recycler_wishlisted.setVisibility(View.VISIBLE);
                    no_wishlist.setVisibility(View.GONE);
                }
                adapter.notifyDataSetChanged();

                swipe_refresh_wishlisted.setRefreshing(false);
                Log.d("wishlist", result);
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                swipe_refresh_wishlisted.setRefreshing(false);
                recycler_wishlisted.setVisibility(View.GONE);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                Log.d("wishlist", errorMessage);
            }
        });


    }

    @Override
    public void onRefresh() {
        getWishlist();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry:
                swipe_refresh_wishlisted.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                recycler_wishlisted.setVisibility(View.VISIBLE);
                getWishlist();
                break;
        }
    }
}
