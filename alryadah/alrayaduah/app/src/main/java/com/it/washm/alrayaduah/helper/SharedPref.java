package com.it.washm.alrayaduah.helper;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by washm on 1/9/17.
 */

public class SharedPref {
    public  Context context;
    public  static SharedPreferences.Editor editor;
    public  static SharedPreferences prefs ;
    String restoredText ;

    /**
     *
     * @param context
     * @param prefName
     */
    public  SharedPref(Context context, String prefName){
         editor = context.getSharedPreferences(prefName, MODE_PRIVATE).edit();
         prefs=context.getSharedPreferences(prefName, MODE_PRIVATE);
    }

    /**
     *
     * @param kye name of pref
     * @param value value of pref
     */
    public void setShared(String kye ,String value){
        editor.putString(kye,value);
        editor.commit();


    }


    /**
     *
     * @param kye name of pref
     * @return value of pref
     */
    public String gitShared(String kye){

        return  prefs.getString(kye, "no data");
    }

    /**
     *
     * @param kye
     * @return
     */
    public String gitSharedNek(String kye){

        return  prefs.getString(kye, null);
    }

    public void clearRef(){
        editor.clear().commit();
    }

}
