//
//  ProfileUsers_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ProfileUsers_CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var unfollow: UIButton!
    @IBOutlet weak var locationButton: UIButton!
    override func awakeFromNib() {
        
        setup_cosmos(cosmos: ratingStar)
    }
    var user:User?{
        didSet{
            updateView()
        }
    }
    
    func updateView() {
        guard let user = user else {
            return
        }
        
        username.text = user.name
        avatar.set_avatar_image(user_id: user.id)
        locationText.text = make_location_text(user)
        ratingStar.rating = user.rating
        
    }
}
