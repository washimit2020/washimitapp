<?php

namespace App\Http\Requests\Shows;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class StoreBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'street_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'sector_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'lat' => 'required_with:lng|max:'.Constants::MAX_STRING_LENGTH,
            'lng' => 'required_with:lat|max:'.Constants::MAX_STRING_LENGTH,
        ];
    }
}
