//
//  FontsExtensions.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import SlideMenuControllerSwift
import ImageSlideshow

extension UILabel {
    public var substituteFontName : String {
        get {
            return self.font.fontName;
        }
        set {
            let fontNameToTest = self.font.fontName.lowercased();
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font.pointSize)
        }
    }
}

extension UITextView {
    public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
        
    }
    
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = Constants.themeColor.cgColor
        self.tintColor = Constants.themeColor
        self.textColor = Constants.themeColor
        
        self.layer.masksToBounds = true
    }
    
}

extension UIPickerView{
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.layer.borderWidth = 0.5
        self.layer.borderColor = Constants.themeColor.cgColor
        self.tintColor = Constants.themeColor
        
        self.layer.masksToBounds = true
    }
}

extension UITextField {
    public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            let fontNameToTest = self.font?.fontName.lowercased() ?? "";
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font?.pointSize ?? 17)
        }
    }
    override open func draw(_ rect: CGRect) {
        super.draw(rect)
        self.layer.borderWidth = 0.5
        self.layer.borderColor = Constants.themeColor.cgColor
        self.tintColor = Constants.themeColor
        self.textColor = Constants.themeColor
        
        self.layer.masksToBounds = true
    }
}


extension UITextView: Validatable {
    public var validationText: String {
        return text ?? ""
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in PNG format
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the PNG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    var png: Data? { return UIImagePNGRepresentation(self) }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
   
}



extension UIImageView{
    
    public func set_product_image(product_id:Int ){
        self.kf.indicatorType = .activity
        
        let link = get_cover_links(id: product_id, url: Constants.GET_PRODUCT_IMAGE)[0]
        self.kf.setImage(with: link, placeholder: get_product_placeholder(), options:  [.transition(.fade(0.5))], progressBlock: nil, completionHandler: nil)
    }
    
    public func set_avatar_image(user_id:Int){
        self.kf.indicatorType = .activity
        
        let link = get_user_avatar_link(id: user_id)
        self.kf.setImage(with: link, placeholder: get_product_placeholder(), options:  [.transition(.fade(0.5))], progressBlock: nil, completionHandler: nil)
    }
    
    func setUserIcon(user:User){
        var index  = user.type.id - 1
        if index > 2 || index < 0 {
            index = 2
        }
        self.image = UIImage(named:Constants.icon_name[index])
    }
}


extension SlideMenuController{
    public convenience init(_ storyboard:UIStoryboard) {
        self.init()
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "main") as! MainPage_ViewController
        
        if let is_first = UserDefaultsHelper.is_first(){
            for i in mainViewController.firstTimeLogin.indices{
                mainViewController.firstTimeLogin[i] = is_first
            }
            UserDefaultsHelper.remove_is_first()
        }

        let nvc =  UINavigationController(rootViewController: mainViewController)
        nvc.navigationBar.tintColor = Constants.themeColor
        nvc.navigationBar.isTranslucent = false
        nvc.navigationBar.backgroundColor = UIColor.white
        
        self.mainViewController = nvc
        self.rightViewController = SlideMenu_TableViewController(nibName: "SlideMenu_TableViewController", bundle: nil)
        initView()
    }
}


func setup_cosmos(cosmos:CosmosView){
    cosmos.updateOnTouch = false
    cosmos.rating = 0
    cosmos.emptyBorderColor = Constants.themeColor
    cosmos.filledBorderColor = Constants.themeColor
    cosmos.filledColor = Constants.themeColor
    cosmos.emptyColor = UIColor.lightGray
}

func setup_slider(slider:ImageSlideshow){
    
    slider.backgroundColor = UIColor.white
    slider.slideshowInterval = 5.0
    slider.pageControlPosition = PageControlPosition.underScrollView
    slider.pageControl.currentPageIndicatorTintColor = Constants.themeColor
    slider.pageControl.pageIndicatorTintColor = Constants.fadedThemeColor
    slider.contentScaleMode = .scaleAspectFill
}




func make_sidebar(window:UIWindow? = nil){
    let slideMenuController = SlideMenuController(UIStoryboard(name: "Main", bundle: nil))
    slideMenuController.automaticallyAdjustsScrollViewInsets = true

    var current:UIWindow!
    
    if  let window = window {
        current = window
    }else if let window = UIApplication.shared.keyWindow{
        current = window
    }else{
        return
    }
    current.rootViewController = slideMenuController
    current.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1.0)
    current.makeKeyAndVisible()
   
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
    
}



func makeDetailsTitle(_ viewcontroller:UIViewController , title:String, image:String?){
    
    let myView: UIView = UIView(frame: CGRect(x:200, y:0, width:150, height:70))
    let label: UILabel = UILabel(frame: CGRect(x:0, y:0, width:150, height:70))
    
    label.text = title
    label.textAlignment = .center
    label.textColor = Constants.themeColor
    label.textAlignment = .center
    label.font = UIFont(name: get_font_name(), size: 15)!
    label.numberOfLines = 2
    
    
    
    myView.addSubview(label)
    
    myView.backgroundColor = UIColor.clear
    myView.autoresizesSubviews = true
    myView.contentMode = .center
    
    if let name = image{
        let image: UIImage = UIImage(named: name)!
        let myImageView: UIImageView = UIImageView(image: image)
        myImageView.frame = CGRect(x:myView.frame.width, y:24, width:20, height:20)
        myImageView.contentMode = .scaleAspectFill
        myView.addSubview(myImageView)
        
    }
    viewcontroller.navigationItem.titleView = myView
}


func move_to_details(viewController: UIViewController, cell:HomePlaces_CollectionViewCell){
    let details =  Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
    details.model = cell.user
    viewController.navigationController?.pushViewController(details, animated: true)
}

