package com.it.washim.bakkar.fragment;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.google.gson.Gson;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.ApiRequest;
import com.it.washim.bakkar.halper.SharedPref;
import com.it.washim.bakkar.interfase.OnDialogCollBack;

/**
 * Created by mohammad on 8/7/2017.
 */

public class BaseFragment extends Fragment {
    private ProgressDialog dialog;
    private Dialog alertDialog;
    private Gson gson;

    public void showDialog(String msg) {
        if (dialog == null) dialog = new ProgressDialog(getActivity());
        dialog.setTitle("بكار");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(msg);

        if (!dialog.isShowing()) dialog.show();

    }

    public void showAlertDialog(String msg, final OnDialogCollBack collBack) {
        if (dialog == null) alertDialog = new Dialog(getActivity());
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.setContentView(R.layout.dialog_login);

        MyTextView text = (MyTextView) alertDialog.findViewById(R.id.msg);
        text.setText(msg);

        alertDialog.findViewById(R.id.log_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collBack.login();
            }
        });
        alertDialog.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collBack.hide();
                alertDialog.dismiss();
            }
        });


        if (!alertDialog.isShowing()) alertDialog.show();
    }

    public void hideDialog() {
        if (dialog.isShowing()) dialog.dismiss();

    }

    public Gson gson() {
        if (gson == null) gson = new Gson();
        return gson;
    }

    public ApiRequest request() {
        return ApiRequest.getInstance(getActivity());
    }

    public SharedPref shardPref() {

        return SharedPref.getSharedInstance(getActivity());

    }
}
