package com.it.washim.bakkar.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.it.washim.bakkar.activity.BookDetailsActivity;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.model.Posts;
import com.it.washim.bakkar.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/27/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter {

    private final int SEARCH_ITEM = 0;
    private final int PROGRIS_ITEM = 1;
    Context context;
    ArrayList<Posts> list;
    boolean loading = false;
    String url;
    OnLoadMorCollBack onLoadMoreCollBack;


    public SearchAdapter(Context context, ArrayList<Posts> list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (url != "null") {

                        if (layoutManager.findLastVisibleItemPosition() + 3 >= layoutManager.getItemCount()) {

                            if (!loading) {
                                if (onLoadMoreCollBack != null)
                                    onLoadMoreCollBack.onLoad();
                                loading = true;


                            }


                        }


                    }

                }
            });

        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false));

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SearchViewHolder) {
            final Posts post = list.get(position);
            ((SearchViewHolder) holder).title.setText(post.title);

            ((SearchViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (post.page_id == Constant.PAGES_VIDEO) {

                        Intent applicationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(post.link.media_url.replace("https", "youtube")));
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse(post.link.media_url));
                        try {
                            context.startActivity(applicationIntent);
                        } catch (ActivityNotFoundException ex) {
                            context.startActivity(browserIntent);
                        }

                    } else {

                        context.startActivity(new Intent(context, BookDetailsActivity.class)
                                .putExtra("rated", post.rated)
                                .putExtra("book_id", post.id)
                                .putExtra("image_id", post.attachments.size() > 0 ? post.attachments.get(0).id : null)
                                .putExtra("book_description", post.description)
                                .putExtra("book_title", post.title)
                                .putExtra("is_wishlisted", post.is_wishlisted)
                                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                    }
                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? PROGRIS_ITEM : SEARCH_ITEM;
    }


    public void setOnLoadMoreLesnar(OnLoadMorCollBack onLoadMoreCollBack) {
        this.onLoadMoreCollBack = onLoadMoreCollBack;
    }

    public void isLoading() {
        loading = false;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblListItem)
        MyTextView title;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {


        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }

}
