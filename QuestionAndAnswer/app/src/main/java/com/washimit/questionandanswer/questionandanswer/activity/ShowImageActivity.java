package com.washimit.questionandanswer.questionandanswer.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.TouchImageView;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;

public class ShowImageActivity extends AppCompatActivity {


    String img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        if (getIntent().getExtras() != null) {
            img = getIntent().getExtras().getString("img");
        }
        TouchImageView imageView = (TouchImageView) findViewById(R.id.imageView);

        ImageView back = (ImageView) findViewById(R.id.back);

        //imageView.setImageDrawable(getResources().getDrawable(R.drawable.cover_2));

        Picasso.with(getApplicationContext()).load(img).into(imageView);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
     }
