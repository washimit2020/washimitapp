<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:32 AM
 */

namespace App\Http;


class Constants
{
    const PAGES_ARTICLES = 1;
    const PAGES_BOOKS = 2;
    const PAGES_AUDIO = 3;
    const PAGES_AGENTS = 4;

    const ATTACHMENT_BOOK = 1;
    const ATTACHMENT_COVER = 2;
    const Attachment_AUDIO = 3;

    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_COSTUMER = 2;



    const FREE_FILTER = 1;
    const NEWEST_FILTER = 2;


    const CAN_ADMIN = 'admin';
}