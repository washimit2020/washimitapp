//
//  Profile_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/7/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

enum profile_ids : String {
    case wishlist = "wishlist"
    case product  = "profileProduct"
    case place  = "profilePlace"
    case share = "share"
}


class Profile_CollectionViewController: ItemsLoader_CollectionViewController {
    
    var cell_id = "wishlist"
    var following = Constants.TYPE_STORE
    var user:User!
    
    var itemSize:CGSize?
    let layout = UICollectionViewFlowLayout()
    var dialog:SortFilter_ViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Register cell classes
//        self.delegate = self
        self.collectionView!.register(UINib(nibName: "WishlistCell_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: profile_ids.wishlist.rawValue)
        self.collectionView!.register(UINib(nibName: "ProfileProduct_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: profile_ids.product.rawValue)
        self.collectionView!.register(UINib(nibName: "ProfileUsers_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: profile_ids.place.rawValue)
        
        self.collectionView!.register(UINib(nibName: "ShareProduct_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: profile_ids.share.rawValue)
        self.collectionView!.register(UINib(nibName:"SharedSection_CollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "header")
        
        self.emptyLabel?.frame.origin.y = 0
        
        self.collectionView?.contentInset = UIEdgeInsetsMake(-60, 0, 0, 0)
        self.automaticallyAdjustsScrollViewInsets = false
        self.collectionView?.allowsSelection = false
        // Do any additional setup after loading the view.
     
    
        
//        setup_cell_size()
    }
    
    
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let item = items[indexPath.row] as! JSON
//        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: cell_id, for: indexPath)
//        // Configure the cell
//        switch cell_id {
//        case profile_ids.wishlist.rawValue:
//            let product = cell as! WishlistCell_CollectionViewCell
//            product.product =  Product(item)
//            let tap = UITapGestureRecognizer(target: self, action: #selector(removeFromWishlist(_:)))
//            product.wishlist.addGestureRecognizer(tap)
//            cell = product
//        case profile_ids.product.rawValue:
//            let product = cell as! ProfileProduct_CollectionViewCell
//            product.product =  Product(item)
//            product.indexPath = indexPath
//            product.delegate = self
//            //            product.edit.tag = indexPath.row
//            //            product.edit.addTarget(self, action: #selector(show_dialog(_:)), for: .touchUpInside)
//            product.apiRequest = apiRequest
//            cell = product
//            
//            cell.layer.borderColor = Constants.themeColor.cgColor
//            cell.layer.borderWidth = 0.5
//            
//        case profile_ids.place.rawValue:
//            let place = cell as! ProfileUsers_CollectionViewCell
//            place.user = User(item )
//            place.followButton.addTarget(nil, action: #selector(removeFollowed(_:)), for: .touchUpInside)
//            cell = place
//        default:
//            let shared = cell as! ShareProduct_CollectionViewCell
//            shared.shared = Shared(item)
//            
//            cell = shared
//        }
//        
//        return cell
//    }
    
//    override func numberOfSections(in collectionView: UICollectionView) -> Int {
//        if cell_id == profile_ids.share.rawValue{
//            if Gate.is_show(){
//                return 2
//            }
//        }
//        
//        return 1
//        
//    }
    
      
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        return cell.product?.status_id == Constants.STATUS_ACTIVE
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        return cell.product?.status_id == Constants.STATUS_ACTIVE
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        if collectionView.allowsMultipleSelection{
            return false
        }
        return cell_id == profile_ids.product.rawValue
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        if  cell_id != profile_ids.product.rawValue{
            return false
        }
        if collectionView.allowsMultipleSelection{
            return false
        }
        
        if action == #selector(copy(_:)) || action == #selector(cut(_:)) || action == #selector(paste(_:)){
            return false
        }
        
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        //required always
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return itemSize!
    }
    
//    func setup_cell_size(){
//        layout.minimumInteritemSpacing = 0
//        layout.minimumLineSpacing = 0
//        
//        beginWithModel = "wishlisted"
//        link = RequestLinks.wishlist.rawValue
//        dataModelChanger = nil
//        
//        collectionView?.allowsMultipleSelection = false
//        
//        switch cell_id {
//        case profile_ids.wishlist.rawValue:
//            layout.minimumInteritemSpacing = 8
//            layout.minimumLineSpacing = 8
//            itemSize =  CGSize(width: (collectionView!.frame.width - 14) / 2, height: 160)
//        case profile_ids.product.rawValue:
//            beginWithModel = "products"
//            
//            itemSize =  CGSize(width: (collectionView!.frame.width ) / 3, height: 200)
//            dataModelChanger = self
//            link = RequestLinks.user_details.rawValue + "\(user.id!)"
//            
//            
//        case profile_ids.place.rawValue:
//            beginWithModel = "users"
//            link = RequestLinks.following.rawValue + "\(following)"
//            itemSize =  CGSize(width: (collectionView!.frame.width ) , height: 90)
//            
//        default:
//            beginWithModel = "shared"
//            link = RequestLinks.shareProduct.rawValue
//            itemSize =  CGSize(width: (collectionView!.frame.width ) , height: 90)
//        }
//        
//        collectionView?.collectionViewLayout = layout
//        
//        items.removeAll()
//        collectionView?.reloadData()
//        
//        clearData()
//    }
    
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        switch kind {
//        case UICollectionElementKindSectionHeader:
//            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! SharedSection_CollectionReusableView
//            if indexPath.section == 0{
//                header.text = "الطلبات المعلقة"
//            }else{
//                 header.text = "الطلبات الحالية"
//            }
//        default:
//            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
//        }
//    }

    
}

//extension Profile_CollectionViewController:ItemsLoaderDelegate,DataModelChanger,ProfileProductDelegate{
//    
//    
//    func dataFiller(data: [JSON]) {
//        for item in data{
//            self.items.append(item)
//        }
//    }
//    
//    func change(_ json: JSON) {
//        let user = User(json,beginWith: "user")
//        dataModel = user.dataModel
//        self.user = user
//    }
//    
//    func removeFromWishlist(_ sender:UITapGestureRecognizer){
//        
//        let point = sender.location(in: collectionView)
//        let indexPath =  collectionView!.indexPathForItem(at: point)!
//        let cell = collectionView?.cellForItem(at: indexPath) as! WishlistCell_CollectionViewCell
//        removieItem(index: indexPath)
//        apiRequest.call_wishlist_like_api(link: RequestLinks.wishlist.rawValue, product: cell.product!)
//    }
//    
//    func removeFollowed(_ sender:AspectFitButton){
//        sender.isEnabled = false
//        
//        let point = sender.convert(CGPoint.zero, to: collectionView)
//        let indexPath =  collectionView!.indexPathForItem(at: point)!
//        let cell = collectionView!.cellForItem(at: indexPath) as! ProfileUsers_CollectionViewCell
//        
//        removieItem(index: indexPath)
//        apiRequest.call_follow_unfollow(user_id: cell.user!.id)
//    }
//    
//    func removieItem(index:IndexPath){
//        items.remove(at: index.row)
//        collectionView?.performBatchUpdates({
//            self.collectionView?.deleteItems(at: [index])
//        }, completion: {
//            
//            if self.items.count == 0 {
//                self.emptyLabel?.isHidden = !$0
//            }
//        })
//    }
//    
//    func didPerformAction(indexPath: IndexPath, action: ProductActions) {
//        let cell = collectionView?.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
//        
//        switch action {
//        case ProductActions.delete:
//            removieItem(index: indexPath)
//            break
//        case ProductActions.hide:
//            collectionView!.reloadItems(at: [indexPath])
//            break
//        case ProductActions.edit:
//            let add_product = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.add_product.rawValue) as! AddProduct_TableViewController
//            
//            add_product.product = cell.product
//            selectedIndexPath = indexPath
//            self.navigationController?.pushViewController(add_product, animated: true)
//            
//            break
//        default:
//            let items:Array = [cell.productImage!.image!,cell.product!.name!] as [Any]
//            let activityViewController = UIActivityViewController(activityItems: items, applicationActivities: nil)
//            
//            present(activityViewController, animated: true, completion: {})
//            
//            break
//            
//        }
//        
//    }
//    
//    
//    
//    /*
//     func show_dialog(_ sender:AspectFitButton){
//     var dialog  = self.dialog
//     
//     selectedIndexPath = IndexPath(row: sender.tag, section: 0)
//     
//     
//     if let current = dialog{
//     dialog = current
//     }else{
//     dialog = SortFilter_ViewController(nibName: "SortFilter_ViewController", bundle: nil)
//     dialog?.items = [
//     ["name":"مشاركة" , "value":"1","type":"1"],
//     ["name":"تعديل","value":"2","type":"1"],
//     ["name":"حذف","value":"3","type":"delete"],
//     ["name":"اظهار/اخفاء","value":"4","type":"hide"]
//     ]
//     dialog?.modalTransitionStyle = .crossDissolve
//     dialog?.modalPresentationStyle = .overCurrentContext
//     dialog?.delegate = self
//     }
//     
//     self.present(dialog!, animated: true, completion: nil)
//     
//     }
//     */
//    /*
//     func selected_item(value: String, type: String) {
//     let cell = collectionView?.cellForItem(at: selectedIndexPath!) as! ProfileProduct_CollectionViewCell
//     let link = "/\(cell.product!.id!)/"
//     var status:String?
//     switch value {
//     case "1":break
//     case "2":break
//     default:
//     status = type
//     }
//     if let status = status{
//     if value == "3" {
//     removieItem(index: selectedIndexPath!)
//     }else{
//     if cell.product?.status_id == Constants.STATUS_HIDDEN{
//     cell.product?.status_id = Constants.STATUS_ACTIVE
//     }else{
//     cell.product?.status_id = Constants.STATUS_ACTIVE
//     }
//     
//     collectionView?.reloadItems(at: [selectedIndexPath!])
//     }
//     apiRequest.make_request(link: RequestLinks.product.rawValue + link + status, method: .post, completion: nil, dialogMsg: nil)
//     }
//     selectedIndexPath = nil
//     }
//     
//     */
//}
//
//

