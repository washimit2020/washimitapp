package com.it.washmit.bookgoodmorning.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.it.washmit.bookgoodmorning.R;


public class ConnectFragment extends Fragment implements View.OnClickListener {

    LinearLayout twitter, instagram, email, facebook;

    public ConnectFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect, container, false);
        twitter = (LinearLayout) view.findViewById(R.id.twitter);
        instagram = (LinearLayout) view.findViewById(R.id.instagram);
        email = (LinearLayout) view.findViewById(R.id.email);
        facebook = (LinearLayout) view.findViewById(R.id.facebook);


        twitter.setOnClickListener(this);
        instagram.setOnClickListener(this);
        email.setOnClickListener(this);
        facebook.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.twitter:
                open("https://twitter.com/nedaa99", "twitter://user?screen_name=nedaa99");

                break;

            case R.id.instagram:
                ///    open("https://www.instagram.com/drbakkar/", "instagram://user?username=drbakkar");

                break;

            case R.id.email:
                //     open("https://www.instagram.com/drbakkar/", "instagram://user?username=drbakkar");

                break;

            case R.id.facebook:

                //   open("https://www.facebook.com/DrBakkar1/", "fb://profile/263445073677828");

                break;

        }


    }


    private void open(String url, String urlApp) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp)));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}
