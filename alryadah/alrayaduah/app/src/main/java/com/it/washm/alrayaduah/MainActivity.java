package com.it.washm.alrayaduah;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.adapter.CustomDrawerAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.fragment.AboutAppFragment;
import com.it.washm.alrayaduah.fragment.MainNewsFragment;
import com.it.washm.alrayaduah.fragment.MyAccountFragment;
import com.it.washm.alrayaduah.fragment.SearchFragment;
import com.it.washm.alrayaduah.fragment.SettingFragment;
import com.it.washm.alrayaduah.fragment.SubNewsFragment;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.model.MyNews;
import com.it.washm.alrayaduah.model.NewsType;
import com.it.washm.alrayaduah.registration.LogInActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.drawer_lst_activity_main)
    ListView DrawerlistView;
    @BindView(R.id.multiple_actions)
    FloatingActionsMenu multiple_actions;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.slide_menu)
    ImageButton slide_menu;
    @BindView(R.id.search)
    ImageButton search;
    @BindView(R.id.minNews)
    RelativeLayout minNews;
    @BindView(R.id.myAccount)
    RelativeLayout myAccount;
    @BindView(R.id.myNews)
    RelativeLayout myNews;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.addNews)
    FloatingActionButton addNews;
    @BindView(R.id.myWishlist)
    FloatingActionButton wishList;
    @BindView(R.id.main)
    TextView main;
    @BindView(R.id.my_news)
    TextView my_news;
    @BindView(R.id.my_account_text)
    TextView my_account_text;


    private FragmentManager fragmentManager;
    private List<NewsType> DrawerList;
    private List<MyNews> NewsList;
    CustomDrawerAdapter customDrawerAdapter;
    Requests requests;
    public static String TAG = "Main_Activity";
    MainNewsFragment mainNewsFragment;
    SettingFragment settingFragment;
    AboutAppFragment aboutAppFragment;
    MyAccountFragment myAccountFragment;
    SubNewsFragment subNewsFragment;
    SearchFragment searchFragment;
    Gson gson;
    Intent intent;
    int lastPosition;
    boolean IsMain = false;
    SharedPref pref;
    ImageView setting, about;
    String[] Suggestions = {"علي", "محمد", "الزعبي"};
    CustamViewFont viewFont;

    public  static  boolean activity=false;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        viewFont = new CustamViewFont(MainActivity.this, "Cairo-Bold.ttf");


        activity=true;

        String token = FirebaseInstanceId.getInstance().getToken();

        // Log and toast
        String msg = getString(R.string.msg_token_fmt, token);
        Log.d(TAG, msg);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        setting = (ImageView) header.findViewById(R.id.setting);
        about = (ImageView) header.findViewById(R.id.about);


        mainNewsFragment = new MainNewsFragment();

        fragmentManager = getSupportFragmentManager();
        pref = new SharedPref(getApplicationContext(), "MyPref");

        if (!pref.gitShared("role_id").equalsIgnoreCase("2")) {
            addNews.setVisibility(View.GONE);
            myNews.setVisibility(View.GONE);
        }


        if (!AppHelper.IsLogIn(this)) {
            AppHelper.showDialog(this,
                    "انت تتصفح في وضع عدم تسجيل الدخول للتتمكن من الاستفاده من جميع الخصاصئص في التطبيق قم بتسجيل الدخول", "تسجيل الدخول", "", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getApplicationContext(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
        }




//      searchView.setCursorDrawable(R.drawable.customDrawerAdaptertom_cursor);
        searchView.setEllipsize(true);

        // searchView.setSuggestions(Suggestions);

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchFragment = SearchFragment.newInstance(query);
                //fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, searchFragment).commit();
                searchView.hideKeyboard(searchView);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {

                searchFragment = SearchFragment.newInstance("");
                //fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, searchFragment).commit();
                multiple_actions.setVisibility(View.GONE);

            }

            @Override
            public void onSearchViewClosed() {


                IsMain = true;

                fragmentManager.beginTransaction().replace(R.id.content_main, mainNewsFragment).commit();

                multiple_actions.setVisibility(View.VISIBLE);


            }
        });
        gson = new Gson();

        addNews.setSize(FloatingActionButton.SIZE_MINI);
        addNews.setIcon(R.drawable.ic_add_white_24dp);

        wishList.setSize(FloatingActionButton.SIZE_MINI);
        wishList.setIcon(R.drawable.ic_wish_list);


        DrawerList = new ArrayList<>();
        NewsList = new ArrayList<>();

        requests = new Requests(getApplicationContext(), MainActivity.this);

        customDrawerAdapter = new CustomDrawerAdapter(this, R.layout.drawer_item, DrawerList, viewFont);

        DrawerlistView.setAdapter(customDrawerAdapter);


        // fragmentManager = getSupportFragmentManager();
        IsMain = true;
        fragmentManager.beginTransaction().replace(R.id.content_main, mainNewsFragment).commit();


        getDrawerlItem();
        getMyNewsStatuses();

        addNews.setOnClickListener(this);
        wishList.setOnClickListener(this);
        slide_menu.setOnClickListener(this);
        minNews.setOnClickListener(this);
        myNews.setOnClickListener(this);
        myAccount.setOnClickListener(this);
        search.setOnClickListener(this);
        setting.setOnClickListener(this);
        about.setOnClickListener(this);
        DrawerlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IsMain = false;
                lastPosition = position;
                subNewsFragment = new SubNewsFragment(DrawerList.get(position));
                // fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, subNewsFragment).commit();
                drawer.closeDrawer(Gravity.RIGHT);
                multiple_actions.collapseImmediately();
            }
        });


        setFont();

    }

    public void setFont() {
        viewFont.setfont(main);
        viewFont.setfont(my_news);
        viewFont.setfont(my_account_text);
//        viewFont.setfont(addNews.);
//        viewFont.setfont(wishList.);
//        viewFont.setfont(forget_the_password,"Cairo-Regular.ttf");
//        viewFont.setfont(new_account,"Cairo-Bold.ttf");
//        viewFont.setfont(or_reg,"Cairo-Regular.ttf");
//        viewFont.setfont(skip,"Cairo-Bold.ttf");
    }

    @Override
    public void onStart() {
        super.onStart();
        activity = true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        activity = false;

    }

    @Override
    public void onBackPressed() {
        multiple_actions.collapseImmediately();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (searchView.isSearchOpen()) {
           // searchView.showSearch(false);

            searchView.closeSearch();
            return;
        }
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
            return;
        } else if (IsMain) {
            super.onBackPressed();
        } else {
            IsMain = true;
            multiple_actions.setVisibility(View.VISIBLE);
            fragmentManager.beginTransaction().replace(R.id.content_main, mainNewsFragment).commit();

        }
    }


    public void getMyNewsStatuses() {

        requests.StringRequest(Request.Method.GET, "news-statuses", "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                NewsList.clear();
                JSONObject object = null;
                JSONObject message = null;
                JSONArray news_statuses = null;
                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        news_statuses = message.getJSONArray("news_statuses");

                        NewsList.addAll((Collection<? extends MyNews>) gson.fromJson(news_statuses.toString(), new TypeToken<List<MyNews>>() {
                        }.getType()));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }


    private void getDrawerlItem() {

        requests.StringRequest(Request.Method.GET, "news-types", "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d(TAG, result);
                JSONObject object = null;
                JSONObject message = null;
                JSONArray news_main_types = null;
                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        news_main_types = message.getJSONArray("news_main_types");

                        DrawerList.addAll((Collection<? extends NewsType>) gson.fromJson(news_main_types.toString(), new TypeToken<List<NewsType>>() {}.getType()));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                customDrawerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about:
                IsMain = false;
                aboutAppFragment = new AboutAppFragment();
                fragmentManager.beginTransaction().replace(R.id.content_main, aboutAppFragment).commit();
                drawer.closeDrawer(Gravity.RIGHT);
                multiple_actions.collapseImmediately();
                break;

            case R.id.setting:
                IsMain = false;
                settingFragment = new SettingFragment();

                // fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, settingFragment).commit();
                drawer.closeDrawer(Gravity.RIGHT);
                multiple_actions.collapseImmediately();
                break;
            case R.id.addNews:
                intent = new Intent(getApplicationContext(), AddNewsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                multiple_actions.collapseImmediately();
                break;
            case R.id.myWishlist:
                if (AppHelper.IsLogIn(this)) {
                    intent = new Intent(getApplicationContext(), WishlistActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    multiple_actions.collapseImmediately();
                } else {
                    AppHelper.showDialog(this, "يجب تسجل الدخول حتي تتمكن من الوصول لقائمة مفضلتك", "تسجيل الدخول", "", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getApplicationContext(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
                }
                break;


            case R.id.search:
                searchView.showSearch(true);
                break;

            case R.id.slide_menu:
                if (drawer.isDrawerOpen(Gravity.RIGHT)) {
                    drawer.closeDrawer(Gravity.RIGHT);
                    multiple_actions.collapseImmediately();

                } else {
                    if (NewsList.size() == 0) {
                        getMyNewsStatuses();
                    }

                    if (DrawerList.size() == 0) {
                        getDrawerlItem();
                    } else {
                        DrawerlistView.setSelection(lastPosition);
                    }

                    drawer.openDrawer(Gravity.RIGHT);
                    multiple_actions.collapseImmediately();

                }
                break;


            case R.id.minNews:
                multiple_actions.setVisibility(View.VISIBLE);
                IsMain = true;
                mainNewsFragment = new MainNewsFragment();
                // fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.content_main, mainNewsFragment).commit();
                drawer.closeDrawer(Gravity.RIGHT);
                multiple_actions.collapseImmediately();
                break;


            case R.id.myNews:
                IsMain = false;
                if (AppHelper.isOnline(this)) {
                    subNewsFragment = new SubNewsFragment(NewsList);
                    // fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.content_main, subNewsFragment).commit();

                    drawer.closeDrawer(Gravity.RIGHT);
                    multiple_actions.collapseImmediately();
                } else {
                    getMyNewsStatuses();
                    Toast.makeText(getApplicationContext(), "تحقق من اتصال الانترنت", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.myAccount:


                if (AppHelper.IsLogIn(this)) {
                    if (AppHelper.isOnline(this)) {
                        IsMain = false;
                        myAccountFragment = new MyAccountFragment();

                        // fragmentManager = getSupportFragmentManager();

                        fragmentManager.beginTransaction().replace(R.id.content_main, myAccountFragment).commit();
                        drawer.closeDrawer(Gravity.RIGHT);
                        multiple_actions.setVisibility(View.GONE);
                    } else {
                        Toast.makeText(getApplicationContext(), "تحقق من اتصال الانترنت", Toast.LENGTH_LONG).show();

                    }
                } else {
                    drawer.closeDrawer(Gravity.RIGHT);
                    AppHelper.showDialog(this, "يجب تسجل الدخول حتي تتمكن من الوصول لمعلومات حسابك", "تسجيل الدخول", "", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getApplicationContext(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
                }
                break;

        }

    }
}
