package com.it.washim.bookrmamdan.activity;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.it.washim.bookrmamdan.R;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                //      mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
            }

              public void onFinish() {
                startActivity(new Intent(SplashScreen.this, HomeActivity.class));
                finish();
            }
        }.start();

    }
}
