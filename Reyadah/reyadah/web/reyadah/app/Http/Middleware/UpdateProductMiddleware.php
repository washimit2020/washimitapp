<?php

namespace App\Http\Middleware;

use App\__ModelsProducts\Product;
use App\Http\Constants;
use Closure;

class UpdateProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->product)) {
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }
        if ($request->product instanceof Product) {
            if ($request->product->status_id == Constants::_STATUS_DELETED){
                abort(Constants::RESPONSE_CODE_FORBIDDEN);

            }
            if ($request->product->user_id != $request->user()->id) {
                abort(Constants::RESPONSE_CODE_FORBIDDEN);
            }
        } else {
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }

        return $next($request);
    }
}
