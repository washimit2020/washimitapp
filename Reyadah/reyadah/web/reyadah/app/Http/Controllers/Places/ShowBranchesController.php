<?php

namespace App\Http\Controllers\Places;

use App\Http\Constants;
use App\Http\Requests\Shows\StoreBranchRequest;
use App\ManyToPlacesModels\ShowBranch;
use App\User;
use App\Http\Controllers\Controller;

class ShowBranchesController extends Controller
{

    private $attributes = ['name', 'lat', 'lng', 'street_name', 'sector_name'];

    /**
     * Display a listing of the resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {

        if (empty($user->type_id)) {
            $user = auth()->user();
        }
        check_if_blocked($user);

        $branches = $user->branch()->where('status_id', Constants::_STATUS_ACTIVE)->orderBy('name')->get();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['branches' => $branches]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreBranchRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchRequest $request)
    {
        $request = $request->only($this->attributes);

        $request['user_id'] = auth()->user()->id;

        $branch = ShowBranch::create($request);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, $branch));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreBranchRequest $request
     * @param  \App\ManyToPlacesModels\ShowBranch $showBranch
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBranchRequest $request, ShowBranch $showBranch)
    {
        $this->authorize('update', $showBranch);

        $showBranch->update($request->only($this->attributes));

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['branch' => $showBranch]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ManyToPlacesModels\ShowBranch $showBranch
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShowBranch $showBranch)
    {
        $this->authorize('update', $showBranch);
        $showBranch->status_id = Constants::_STATUS_DELETED;
        $showBranch->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['branch' => 'deleted']));

    }
}
