//
//  DataModel.swift
//  reyadah
//
//  Created by WASHM on 4/19/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import SwiftyJSON

class DataModel: Model {
    
    
    var total:Int!
    var per_page:Int!
    var current_page:Int!
    var last_page:Int!
    var next_page_url:String!
    var prev_page_url:String!
    var from:Int!
    var to:Int!
    var data:[JSON]!

    
    override func map() {
        
        total = json["total"].intValue
        per_page = json["per_page"].intValue
        current_page = json["current_page"].intValue
        last_page = json["last_page"].intValue
        next_page_url = json["next_page_url"].string
        prev_page_url = json["prev_page_url"].string
        from = json["from"].intValue
        to = json["to"].intValue
        data = json["data"].arrayValue
    }
}
