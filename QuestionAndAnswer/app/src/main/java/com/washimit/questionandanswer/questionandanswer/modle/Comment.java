package com.washimit.questionandanswer.questionandanswer.modle;

/**
 * Created by washm on 4/3/17.
 */

public class Comment {
    public int id;
    public int user_id;
    public int question_id;
    public String body;
    public String created_at;
    public String updated_at;
}
