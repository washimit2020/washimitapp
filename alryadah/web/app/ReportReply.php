<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportReply extends ReplyWithUser
{
    public $incrementing = true;

    /**
     * @var array primary key
     */
    protected $primaryKey = "id";


    /**
     * @var array fillable key
     */
    protected $fillable = [
        "reply_id", "user_id", "content"
    ];

}
