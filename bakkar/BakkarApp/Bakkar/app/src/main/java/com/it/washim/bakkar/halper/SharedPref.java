package com.it.washim.bakkar.halper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.it.washim.bakkar.model.User;

/**
 * Created by mohammad on 8/8/2017.
 */

public class SharedPref {
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;
    private static SharedPref pref = new SharedPref();
    private static Gson gson = new Gson();

    public static SharedPref getSharedInstance(Context context) {
        if (preferences == null) {
            preferences = context.getSharedPreferences("My_Pref", Context.MODE_PRIVATE);
            editor = preferences.edit();
        }
        return pref;
    }


    public void setBadger(int badger) {

        editor.putInt("badger", badger);
        editor.commit();

    }

    public int getBadger() {

        return preferences.getInt("badger", 0);

    }

    public void setUser(String user) {

        editor.putString("user", user);
        editor.commit();

    }

    public User getUser() {

        return gson.fromJson(preferences.getString("user", null), User.class);

    }

    public void clear() {

        editor.clear().commit();

    }


    public void setFontType(String fontType) {

        editor.putString("fontType", fontType);
        editor.commit();

    }

    public String getFontType() {

        return preferences.getString("fontType", "TheSansArab-Plain_0.ttf");

    }

    public void SetTextSize(int textSize) {

        editor.putInt("textSize", textSize);
        editor.commit();

    }

    public int getTextSize() {

        return preferences.getInt("textSize", 14);

    }

    public void SetScreenMode(boolean screenMode) {

        editor.putBoolean("screenMode", screenMode);
        editor.commit();

    }

    public boolean getScreenMode() {

        return preferences.getBoolean("screenMode", true);

    }


}
