<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ["title", "description", "link", "rating", "price", "published_at", "page_id", "parent_id"];

    protected $casts = [
        "link"=>"json"
    ];

    protected $appends =["is_wishlisted","rated"];

    protected $with = ["attachments","children","categories"];

    public function page()
    {
        return $this->belongsTo(Page::class,'page_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class,'parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class,'parent_id');
    }

    public function attachments(){
        return $this->hasMany(Attachment::class,'post_id');
    }

    public function comments()
    {
        return $this->hasMany(Users_Comment::class,'post_id');
    }

    public function wishlist()
    {
        return $this->hasMany(Users_Wishlist::class,'post_id');
    }

    public function rates()
    {
        return $this->hasMany(Posts_Rating::class,'post_id');
    }

    public function getIsWishlistedAttribute()
    {
        if (app('auth')->check()){
            return $this->wishlist()->where('user_id', app('auth')->user()->id)->count()>0;
        }
        return false;
    }

    public function getRatedAttribute()
    {
        if (app('auth')->check()){
            $rate =  $this->rates()->where('user_id', app('auth')->user()->id)->first();
            return isset($rate)?$rate->rating:0;
        }
        return 0;
    }

	public function categories()
	{
		return $this->belongsToMany(Category::class);
	}
}