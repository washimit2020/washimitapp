<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Constants;
use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignUpRequest;
use App\Http\Requests\UpdateProfile;
use App\Notifications\NewsLiked;
use App\User;
use App\UserAttachment;
use \Illuminate\Support\Facades\Auth as Authonticate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class Auth extends BaseController
{

    public function login(LoginRequest $request)
    {
        if (Authonticate::attempt(["email" => $request->email, "password" => $request->password])) {
            auth()->user()->reg_id = $request->reg_id;
            return ResponseHelper::userAuthnticationResponse(Authonticate::user());
        }
        return ResponseHelper::failedResponse(trans('auth.failed'));
    }

    public function sign_up(SignUpRequest $request)
    {
        $data = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "nick_name" => $request->nick_name,
            "reg_id" => $request->reg_id,
            "preferred_lang" => $request->preferred_lang,
            "password" => bcrypt($request->password),
            'user_role_id' => Constants::USER_ROLE_COSTUMER,
            'email' => $request->email,
            'gender' => $request->gender,
        ];

        $user = User::create($data);

        if ($request->hasFile("profile_image")) {
            $path = 'users/' . $user->id . "/profile_image";
            Storage::deleteDirectory($path);
            $file = $request->file('profile_image');
            $file_data = [
                'user_id' => $user->id,
                'path' => $file->store($path),
                'mime_type' => $file->getMimeType(),
                'user_attachment_type_id' => Constants::USER_ATTACHMENT_PROFILE_IMAGE
            ];
            UserAttachment::create($file_data);
        }

        if ($request->wantsJson()) {
            return ResponseHelper::userAuthnticationResponse($user);
        }

        return true;
    }

    public function profile()
    {
        $user = request()->user()->with(["role", "attachment" => function ($query) {
            $query->with('type')->get();
        }])->find(request()->user()->id);
        return response()->json(ResponseHelper::successResponse(['user' => $user]));
    }

    public function update_profile(UpdateProfile $request)
    {

        abort_unless(Hash::check($request->password, $request->user()->password), 401);
        $data = [
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "nick_name" => $request->nick_name,
            "reg_id" => $request->reg_id,
            "preferred_lang" => $request->preferred_lang,
            "password" => $request->user()->password
        ];



        $request->user()->update($data);
        if ($request->has('new_password')) {
            $request->user()->password  =    bcrypt($request->new_password);
            $request->user()->update();
        }

        if ($request->hasFile("profile_image")) {
            $path = 'users/' . $request->user()->id . "/profile_image";
            Storage::deleteDirectory($path);
            $file = $request->file('profile_image');
            $file_data = [
                'user_id' => $request->user()->id,
                'user_attachment_type_id' => Constants::USER_ATTACHMENT_PROFILE_IMAGE
            ];
            UserAttachment::updateOrCreate(
                $file_data,
                ['path' => $file->store($path)]
            );
        }

        return ResponseHelper::userAuthnticationResponse($request->user());
    }

    public function get_news($type)
    {
        if (!in_array($type, [Constants::NEWS_STATUS_PENDING, Constants::NEWS_STATUS_APPROVED, Constants::NEWS_STATUS_DISAPPROVED])) {
            abort(404);
        }

        $news = auth()->user()->news()->where('news_status_id', $type)->with(['user'=>get_user_obj(),'attachment' => function ($query) {
            $query->with('type')->get();
        }])->withCount(['views', 'likes', 'comments'])->paginate(10);

        return response()->json(ResponseHelper::successResponse(["news" => $news]));

    }

    public function get_notifications(){
        $notifications = auth()->user()->notifications()->paginate(10);
        get_translation_for_notification($notifications);
        return response()->json(ResponseHelper::successResponse(["notifications" => $notifications,'unread'=>auth()->user()->unreadNotifications->count()]));
    }

    public function read_notification($id){
        auth()->user()->notifications()->findOrFail($id)->markAsRead();
        return response()->json(ResponseHelper::successResponse(['notification'=>"read"]));

    }

}
