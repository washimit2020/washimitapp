<?php

namespace App\Http\Controllers\Places;

use App\__ModelsFestival\Festival;
use App\Attachment;
use App\Http\Constants;
use App\Http\Requests\Festivals\StoreRequest;
use App\SocialMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FestivalsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $festival = Festival::where('status_id',Constants::_STATUS_ACTIVE)->whereHas('user',function ($query){
            check_if_blocked($query);
        })->latest('rating')->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS,['festivals'=>$festival]));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $social_media = SocialMedia::create($request->only('facebook','twitter','snapchat','instagram','whatsapp'))->id;
        $data = $request->all();

        $data ["social_media_id"] = $social_media;
        $festival = auth()->user()->festivals()->create($data);

        $this->create_festival_attachments($request,$festival);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS,['festival'=>$festival]));

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function user_festivals(){
        $festivals = auth()->user()->festivals()->where('status_id','<>',Constants::_STATUS_DELETED)->latest()->orderBy('status_id')->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS,['festivals'=>$festivals]));

    }



    private function create_festival_attachments(StoreRequest $request,Festival $festival){
        $data ['type_id'] = Constants::_TYPE_FESTIVAL_ATTACHMENT;
        $data ['related_id'] = $festival->id;
        $user = auth()->user();

        $dir = "users/{$user->id}/festival/{$festival->id}";

        if ($request->hasFile('license')) {
            $file = $request->file('license');
            $data ['type_id'] = Constants::_TYPE_FESTIVAL_LICENSE;

            $data['path'] = $file->storeAs($dir, 'license');
            $data['mime_type'] = $file->getMimeType();
            $data['extension'] = $file->extension();
            Attachment::create($data);
        }

        if ($request->hasFile('images') && is_array($request->file('images'))) {
            foreach ($request->file('images') as $file) {
                $data['path'] = $file->storeAs($dir, time() . "_" . str_random());
                $data['mime_type'] = $file->getMimeType();
                $data['extension'] = $file->extension();
                Attachment::create($data);
            }
        }
    }
}
