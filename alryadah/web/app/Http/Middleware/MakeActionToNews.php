<?php

namespace App\Http\Middleware;

use App\Http\Helpers\Constants;
use App\News;
use Closure;

class MakeActionToNews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $news = News::find($request->news_id);
        if (empty($news)){
            $news = News::findOrFail($request->id);
        }
        if($news->news_status_id != Constants::NEWS_STATUS_APPROVED){
            abort(403);
        }
        return $next($request);
    }
}
