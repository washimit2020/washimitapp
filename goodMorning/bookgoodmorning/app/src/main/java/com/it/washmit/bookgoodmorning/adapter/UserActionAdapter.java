package com.it.washmit.bookgoodmorning.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.interfac.deletnotecallback;
import com.it.washmit.bookgoodmorning.interfac.pageIdCallback;
import com.it.washmit.bookgoodmorning.model.comment;
import com.it.washmit.bookgoodmorning.model.wishiListed;

import java.util.List;

/**
 * Created by washm on 4/4/17.
 */

public class UserActionAdapter extends RecyclerView.Adapter {

    pageIdCallback collback;
    deletnotecallback deletnotecallback;
    List<?> list;
    Activity context;


    public UserActionAdapter(Activity context, List<?> list) {
        this.list = list;
        this.context = context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder = null;
        if (viewType == 1) {
            holder = new WishilistedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_action_item, parent, false));
        } else if (viewType == 2) {
            holder = new commentHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.user_action_item, parent, false));
        }


        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof WishilistedHolder) {
            final wishiListed get = (wishiListed) list.get(position);

            ((WishilistedHolder) holder).title.setText(get.getTitel());
            ((WishilistedHolder) holder).pageNumber.setText(String.valueOf(get.getId()));

            ((WishilistedHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.getId());

                }
            });


        } else if (holder instanceof commentHolder) {

            final comment get = (comment) list.get(position);

            ((commentHolder) holder).title.setText(get.getComment());
            ((commentHolder) holder).pageNumber.setText(String.valueOf(get.getId()));

            ((commentHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.getId());

                }
            });
            ((commentHolder) holder).delet_note.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, list.size());
                    deletnotecallback.deletNote(get.getId());
                }
            });


        }


    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(0) instanceof wishiListed) {

            return 1;
        } else if (list.get(0) instanceof comment) {

            return 2;
        }
        return super.getItemViewType(position);
    }

    public void onpageIdCollback(pageIdCallback collback) {
        this.collback = collback;
    }

    public void onpageNotDeletCollback(deletnotecallback deletnotecallback) {
        this.deletnotecallback = deletnotecallback;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishilistedHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;
        ImageView delet_note;

        public WishilistedHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);
            delet_note = (ImageView) itemView.findViewById(R.id.delet_note);

            delet_note.setVisibility(View.GONE);
        }

    }

    public class commentHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;
        ImageView delet_note;

        public commentHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);
            delet_note = (ImageView) itemView.findViewById(R.id.delet_note);
            delet_note.setVisibility(View.VISIBLE);

        }


    }


}
