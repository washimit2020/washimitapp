<?php

namespace App\Notifications;

use App\Channels\FCM;
use App\Channels\FCM_Message;
use App\News;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewsLiked extends Notification
{
    use Queueable;

    protected $news;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * NewsLiked constructor.
     * @param News $news
     * @param User $user
     */

    public function __construct(News $news ,User $user)
    {
        $this->user = $user;
        $this->news = $news;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FCM::class,'database'];
    }

    public function toFCM($notifiable)
    {
        $fcm = new FCM_Message();


        $fcm->data= ['data'=>[
            'id'=>random_int(0,9999999),
            'user'=>$this->user,
            'news'=>$this->news,
            'type'=>self::class,
        ]
        ];
        $fcm->data["data"]["msg"] = trans('notification.news.liked',translate_notification(self::class,$fcm->data["data"]));

        return $fcm;
    }
// in your notification
    public function toDatabase($notifiable)
    {
        return [
            'user'=>$this->user,
            'news'=>$this->news
        ];
    }
}
