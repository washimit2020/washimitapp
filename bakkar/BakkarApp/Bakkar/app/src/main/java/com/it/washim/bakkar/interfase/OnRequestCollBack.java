package com.it.washim.bakkar.interfase;

import com.android.volley.VolleyError;

import org.json.JSONException;

/**
 * Created by mohammad on 8/7/2017.
 */

public interface OnRequestCollBack {
    void onResponse(String response) throws JSONException;

    void onErrorResponse(VolleyError error);

}
