package com.washimit.it.ryadah.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Block;
import com.washimit.it.ryadah.model.Branche;
import com.washimit.it.ryadah.model.Location;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by washm on 4/29/17.
 */

public class UserInfoPageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final int FOLLOW = 1;
    public final int HEADER = 2;
    public final int BLOCK = 3;
    public final int TITEL = 4;
    public final int BRANCHE = 5;
    private final int VIEW_PROG = 0;


    private int usersType;
    List<?> list;
    Activity context;
    private View defaultHeader;
    Gson gson;
    SharedPref pref;
    ApiRequest request;
    OnLoadMoreListener loadMoreListener;
    boolean isLoading;
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private String urls;
    HashMap<String, String> map;

    public UserInfoPageAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        pref = new SharedPref(context);
        request = new ApiRequest(context);
        map = new HashMap<String, String>();


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    if (urls != null) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (loadMoreListener != null && !urls.equalsIgnoreCase("null")) {
                                loadMoreListener.onLoadMore();
                            }
                            isLoading = true;
                        }
                    }
                }
            });
        }
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder instanceof HeaderHolder) {
            User user = (User) list.get(i);
            if (user.getType().getId() != Constant._TYPE_COSTUMER) {
                ((HeaderHolder) viewHolder).fragment_my_profile_user_img_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(user.getType().getId())));
                ((HeaderHolder) viewHolder).fragment_my_profile_follow_number.setText(user.getFollowed_by_count() + " " + "متابع");

            }
            ((HeaderHolder) viewHolder).fragment_my_profile_user_name.setText(user.getName());
            ((HeaderHolder) viewHolder).fragment_my_profile_user_info.setText(user.getInfo());
            Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().skipMemoryCache().placeholder(R.drawable.ic_profile_picture).into(((HeaderHolder) viewHolder).fragment_my_profile_user_profile_page);
            if (user.getId() == pref.getUser().getId()) {
                ((HeaderHolder) viewHolder).fragment_my_profile_user_profile_block.setVisibility(View.GONE);

            } else {
                ((HeaderHolder) viewHolder).fragment_my_profile_user_profile_block.setVisibility(View.VISIBLE);

            }
            ((HeaderHolder) viewHolder).fragment_my_profile_user_profile_info.setText(user.getInfo());

        } else if (viewHolder instanceof TitelHolder) {

            ((TitelHolder) viewHolder).titel_text.setText((CharSequence) list.get(i));
        } else if (viewHolder instanceof BrancheHolder) {

            Branche branche = (Branche) list.get(i);


            ((BrancheHolder) viewHolder).item_branche_show_location.setText(branche.getSector_name() + " - " + branche.getStreet_name());

            ((BrancheHolder) viewHolder).item_branche_show_name.setText(branche.getName());

            Picasso.with(context).load(Constant._AVATAR_URL + branche.getUser_id()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((BrancheHolder) viewHolder).item_branche_show_img);

        } else if (viewHolder instanceof BlockHolder) {
            Block block = (Block) list.get(i);
            ((BlockHolder) viewHolder).item_block_user_name.setText(block.getName());
            ((BlockHolder) viewHolder).item_block_user_about.setText(block.getInfo());

            Picasso.with(context).load(Constant._AVATAR_URL + block.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((BlockHolder) viewHolder).item_block_user_avatar);
        } else if (viewHolder instanceof FollowHolder) {
            User user = (User) list.get(i);
            ((FollowHolder) viewHolder).item_user_follower_user_name.setText(user.getName());
            ((FollowHolder) viewHolder).item_user_follower_user_info.setText(user.getInfo());
            Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((FollowHolder) viewHolder).item_user_follower_user_img);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        if (i == HEADER && defaultHeader != null) {
            return new HeaderHolder(defaultHeader);
        }

        if (i == TITEL) {
            return new TitelHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_titel, viewGroup, false));
        }
        if (i == VIEW_PROG) {
            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));
        }
        if (i == BLOCK) {
            return new BlockHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_block, viewGroup, false));
        }
        if (i == BRANCHE) {
            return new BrancheHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_branche_show, viewGroup, false));
        }
        if (i == FOLLOW) {

            return new FollowHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_follower, viewGroup, false));
        }
        return null;
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && defaultHeader != null) {
            return HEADER;
        } else if (list.get(position) == null) {
            return VIEW_PROG;
        } else if (list.get(position) instanceof String) {
            return TITEL;
        } else if (list.get(position) instanceof Branche) {
            return BRANCHE;
        } else if (list.get(position) instanceof User) {
            return FOLLOW;
        } else if (list.get(position) instanceof Block) {
            return BLOCK;
        }
        return 0;

    }


    public class HeaderHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.fragment_my_profile_user_profile_info)
        TextViewCast fragment_my_profile_user_profile_info;

        @BindView(R.id.fragment_my_profile_user_profile_facebook)
        ImageView fragment_my_profile_user_profile_facebook;


        @BindView(R.id.fragment_my_profile_user_profile_instgram)
        ImageView fragment_my_profile_user_profile_instgram;


        @BindView(R.id.fragment_my_profile_user_profile_snapchat)
        ImageView fragment_my_profile_user_profile_snapchat;


        @BindView(R.id.fragment_my_profile_user_profile_twitter)
        ImageView fragment_my_profile_user_profile_twitter;


        @BindView(R.id.fragment_my_profile_user_profile_whatsapp)
        ImageView fragment_my_profile_user_profile_whatsapp;


        @BindView(R.id.fragment_my_profile_follow_number)
        TextViewCast fragment_my_profile_follow_number;
        @BindView(R.id.fragment_my_profile_user_name)
        TextViewCast fragment_my_profile_user_name;
        @BindView(R.id.fragment_my_profile_user_info)
        TextViewCast fragment_my_profile_user_info;
        @BindView(R.id.fragment_my_profile_user_profile_block)
        TextViewCast fragment_my_profile_user_profile_block;
        @BindView(R.id.fragment_my_profile_user_img_type)
        ImageView fragment_my_profile_user_img_type;
        @BindView(R.id.fragment_my_profile_user_profile_page)
        ImageView fragment_my_profile_user_profile_page;

        public HeaderHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            fragment_my_profile_user_profile_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    AapHalper.CreateDialog(R.layout.dialog_block, context, new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {

                            ButtonView ok, cancel;

                            ok = (ButtonView) dialog.findViewById(R.id.dialog_block_ok);
                            cancel = (ButtonView) dialog.findViewById(R.id.dialog_block_cancel);


                            ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    User user = (User) list.get(getAdapterPosition());
                                    blockUser(user.getId());
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    context.startActivity(intent);
                                    context.finish();
                                }
                            });

                            cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                }
                            });


                        }
                    });


                }
            });


            fragment_my_profile_user_profile_facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());

                    showDilog(user.getSocial_media().getFacebook());

                }
            });


            fragment_my_profile_user_profile_instgram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());

                    showDilog(user.getSocial_media().getInstagram());

                }
            });


            fragment_my_profile_user_profile_snapchat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());

                    showDilog(user.getSocial_media().getSnapchat());

                }
            });


            fragment_my_profile_user_profile_twitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());

                    showDilog(user.getSocial_media().getTwitter());

                }
            });


            fragment_my_profile_user_profile_whatsapp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = (User) list.get(getAdapterPosition());

                    showDilog(user.getSocial_media().getWhatsapp());

                }
            });


        }
    }

    public class FollowHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_user_follower_user_img)
        ImageView item_user_follower_user_img;
        @BindView(R.id.item_user_follower_user_name)
        TextViewCast item_user_follower_user_name;
        @BindView(R.id.item_user_follower_user_info)
        TextViewCast item_user_follower_user_info;

        public FollowHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public class BrancheHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_branche_show_img)
        ImageView item_branche_show_img;
        @BindView(R.id.item_branche_show_map_location)
        ImageView item_branche_show_map_location;
        @BindView(R.id.item_branche_show_delete_branche)
        ButtonView item_branche_show_delete_branche;

        @BindView(R.id.item_branche_show_name)
        TextViewCast item_branche_show_name;
        @BindView(R.id.item_branche_show_location)
        TextViewCast item_branche_show_location;

        public BrancheHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item_branche_show_delete_branche.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Branche branche = (Branche) list.get(getAdapterPosition());


                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    //   notifyItemRangeChanged(getAdapterPosition(),list.size());
                    deleteBranch(branche.getId());

                }
            });


            item_branche_show_map_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Branche branche = (Branche) list.get(getAdapterPosition());
                    Intent intent = new Intent(context, MapsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("location", gson.toJson(new Location(new LatLng(branche.getLat(), branche.getLng()), Constant._TYPE_BRANCH), Location.class));
                    context.startActivity(intent);
                }
            });


        }
    }

    class TitelHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titel_text)
        TextViewCast titel_text;

        public TitelHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public class BlockHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_block_user_name)
        TextViewCast item_block_user_name;
        @BindView(R.id.item_block_user_about)
        TextViewCast item_block_user_about;
        @BindView(R.id.item_block_user_avatar)
        ImageView item_block_user_avatar;
        @BindView(R.id.item_block_cancel_block)
        ButtonView item_block_cancel_block;


        public BlockHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item_block_cancel_block.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Block block = (Block) list.get(getAdapterPosition());

                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    blockUser(block.getId());
                }
            });


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avi)
        AVLoadingIndicatorView avi;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }


    /**
     * @param header
     */
    public void setHeader(View header) {
        defaultHeader = header;

    }


    public void setOnLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setUrls(String urls) {
        this.urls = urls;
    }

    private void deleteBranch(int branchId) {
        request.MultipartRequest(Request.Method.POST, false, "branch/" + branchId + "/delete", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("deleteBranch", result);

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }


    public void blockUser(int userId) {
        map.put("user_id", userId + "");
        request.MultipartRequest(Request.Method.POST, false, "block", map, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("block", result);

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("block", errorMessage);

            }
        });

    }


    public void showDilog(final String msg) {


        AapHalper.CreateDialog(R.layout.dialog_social_media_info, context, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {

                TextViewCast text = (TextViewCast) dialog.findViewById(R.id.dialog_social_media_info_text);
                ButtonView ok = (ButtonView) dialog.findViewById(R.id.dialog_social_media_info_ok);
                if (msg != null && !msg.equalsIgnoreCase("")) {
                    text.setText(msg);

                }

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });


            }
        });


    }


}