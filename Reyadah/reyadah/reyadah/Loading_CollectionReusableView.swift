//
//  Loading_CollectionReusableView.swift
//  reyadah
//
//  Created by WASHM on 4/19/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Loading_CollectionReusableView: UICollectionReusableView {
   
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    public func hideIndicator(hide:Bool){
        if hide{
            indicator.stopAnimating()
        }else{
            indicator.startAnimating()
        }
    }
    
}
