package com.it.washmit.bookgoodmorning.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.interfac.pageIdCallback;
import com.it.washmit.bookgoodmorning.model.searchResult;

import java.util.List;

/**
 * Created by washm on 4/4/17.
 */

public class searchAdapter extends RecyclerView.Adapter {

    pageIdCallback collback;
    List<searchResult> list;
    Activity context;
    public searchAdapter(Activity context, List<searchResult> list) {
        this.list=list;
        this.context=context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new WishilistedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final searchResult get=(searchResult)list.get(position);
        if (holder instanceof WishilistedHolder) {

            ((WishilistedHolder)holder).title.setText(get.getBady());
            ((WishilistedHolder)holder).pageNumber.setText(String.valueOf(get.getPageId()));

            ((WishilistedHolder)holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.getPageId());
                }
            });



        }


    }



public void onpageIdCollback(pageIdCallback collback){
    this.collback=collback;
}
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishilistedHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;

        public WishilistedHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);


        }
    }



}
