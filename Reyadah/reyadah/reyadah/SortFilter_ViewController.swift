//
//  SortFilter_ViewController.swift
//  reyadah
//
//  Created by WASHM on 5/7/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class SortFilter_ViewController: DialogBase_ViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    var delegate:SortFilter!
    var items = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let container = view.subviews[0]
        container.layer.cornerRadius = 30
        container.layer.masksToBounds = true
        container.clipsToBounds = true
        tableView.separatorColor = Constants.themeColor
        tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
        self.tableView.register(Label_TableViewCell.self, forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        heightConstraint.constant = CGFloat((items.count) * 44)

    }
    
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = item["name"]
        cell.textLabel?.textColor = Constants.themeColor
        
        cell.textLabel?.textAlignment = .center
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = Constants.themeColor
        cell.selectedBackgroundView = bgColorView
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.textLabel?.textColor = UIColor.white
        self.delegate.selected_item(value: items[indexPath.row]["value"]!,type: items[indexPath.row]["type"]!)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.textLabel?.tintColor = Constants.themeColor
    }
 
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    

}

protocol SortFilter {
    func selected_item(value:String,type:String)
}
