package com.washimit.it.ryadah.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.washimit.it.ryadah.Fragment.BlockFragment;
import com.washimit.it.ryadah.Fragment.SharchFragment;
import com.washimit.it.ryadah.Fragment.WishlistedFragment;
import com.washimit.it.ryadah.Fragment.profile.AddBranchFragment;
import com.washimit.it.ryadah.Fragment.profile.AddProduactFragment;
import com.washimit.it.ryadah.Fragment.profile.EditMyProfileFragment;
import com.washimit.it.ryadah.Fragment.MangaFragment;
import com.washimit.it.ryadah.Fragment.profile.EditProduactFragment;
import com.washimit.it.ryadah.Fragment.profile.MyProfileFragment;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.HomeParallaxAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.NavigationBarItem;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.navigation_rv)
    RecyclerView navigation_rv;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.Page_name)
    TextViewCast Page_name;
    @BindView(R.id.app_bar)
    AppBarLayout app_bar;


    Fragment fragment;
    SharedPref pref;
    User user;
    ArrayList<NavigationBarItem> barItems;
    HomeParallaxAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        pref = new SharedPref(this);

        openFragment(MangaFragment.newInstants(Constant._PAGE_HOME));

        barItems = new ArrayList<>();
        user = pref.getUser();
        adapter = new HomeParallaxAdapter(barItems, this);
        navigation_rv.setHasFixedSize(true);
        navigation_rv.setLayoutManager(new LinearLayoutManager(this));
        navigation_rv.setAdapter(adapter);
        if (user != null) {

            updateNabBarNavBar(user.getType().getId());


        } else {
            updateNabBarNavBar(Constant._TYPE_VISITOR);

        }

        menu.setOnClickListener(this);
        back.setOnClickListener(this);
        search.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);

        } else if (fragment instanceof EditMyProfileFragment) {
            openFragment(new MyProfileFragment());

        } else if (fragment instanceof AddProduactFragment) {
            openFragment(new MyProfileFragment());

        } else if (fragment instanceof AddBranchFragment) {
            openFragment(new MyProfileFragment());

        } else if (fragment instanceof EditProduactFragment) {
            openFragment(new MyProfileFragment());

        } else if (fragment instanceof MangaFragment) {
            super.onBackPressed();

        } else {
            openFragment(MangaFragment.newInstants(Constant._PAGE_HOME));

        }
    }


    public void openFragment(Fragment fragment) {
        if (this.fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(this.fragment).commit();

        }
        this.fragment = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
        app_bar.setVisibility(View.VISIBLE);


        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
        if (fragment instanceof MangaFragment) {
            menu.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            Page_name.setText("");

            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);


        } else if (fragment instanceof MyProfileFragment) {
            menu.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            back.setVisibility(View.VISIBLE);
            Page_name.setText(getString(R.string.my_profile));
            app_bar.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        } else if (fragment instanceof EditMyProfileFragment) {
            menu.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            Page_name.setText(getString(R.string.edit_my_profile));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else if (fragment instanceof AddProduactFragment) {
            menu.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            Page_name.setText(getString(R.string.add_new_product));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else if (fragment instanceof AddBranchFragment) {
            menu.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            Page_name.setText(getString(R.string.add_new_branch));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else if (fragment instanceof BlockFragment) {
            menu.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            Page_name.setText(getString(R.string.block_men));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        } else if (fragment instanceof WishlistedFragment) {
            menu.setVisibility(View.VISIBLE);
            search.setVisibility(View.VISIBLE);
            back.setVisibility(View.GONE);
            Page_name.setText(getString(R.string.my_wishlist));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

        } else if (fragment instanceof EditProduactFragment) {
            menu.setVisibility(View.GONE);
            search.setVisibility(View.GONE);
            back.setVisibility(View.VISIBLE);
            Page_name.setText(getString(R.string.edit_product));
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        }

    }


    public void updateNabBarNavBar(int userType) {

        switch (userType) {
            case Constant._TYPE_VISITOR:
                barItems.add(new NavigationBarItem(Constant._SHARE, getString(R.string.invite_friend), R.drawable.ic_invite_friends_sidebar));
                //      barItems.add(new NavigationBarItem(Constant._HALPER, getString(R.string.help), R.drawable.ic_help_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_ABOUT_DEVLOPER, getString(R.string.about_developer), R.drawable.ic_developer_sidebar));
                barItems.add(new NavigationBarItem(Constant.SETTINGS, getString(R.string.setting), R.drawable.ic_sittings_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_LOGOUT, getString(R.string.sign_in), R.drawable.ic_login));

                break;
            case Constant._TYPE_COSTUMER:
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_PROFILE, getString(R.string.my_profile), R.drawable.ic_profile_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_SHARCH, getString(R.string.search), R.drawable.ic_search_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_BLOCK, getString(R.string.block_men), R.drawable.ic_block));
                barItems.add(new NavigationBarItem(Constant._PAGE_WISHLISTED, getString(R.string.favorite), R.drawable.ic_wishlist_sidebar));
                barItems.add(new NavigationBarItem(Constant._SHARE, getString(R.string.invite_friend), R.drawable.ic_invite_friends_sidebar));
                //barItems.add(new NavigationBarItem(Constant._HALPER, getString(R.string.help), R.drawable.ic_help_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_ABOUT_DEVLOPER, getString(R.string.about_developer), R.drawable.ic_developer_sidebar));
                barItems.add(new NavigationBarItem(Constant.SETTINGS, getString(R.string.setting), R.drawable.ic_sittings_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_LOGOUT, getString(R.string.sing_out), R.drawable.ic_logout_sidebar));

                break;
            case Constant._TYPE_STORE:
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_PROFILE, getString(R.string.my_profile), R.drawable.ic_profile_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_PROFILE, getString(R.string.requests), R.drawable.ic_requests_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_SHARCH, getString(R.string.search), R.drawable.ic_search_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_BLOCK, getString(R.string.block_men), R.drawable.ic_block));
                barItems.add(new NavigationBarItem(Constant._PAGE_WISHLISTED, getString(R.string.favorite), R.drawable.ic_wishlist_sidebar));
                barItems.add(new NavigationBarItem(Constant._SHARE, getString(R.string.invite_friend), R.drawable.ic_invite_friends_sidebar));
                //    barItems.add(new NavigationBarItem(Constant._HALPER, getString(R.string.help), R.drawable.ic_help_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_ABOUT_DEVLOPER, getString(R.string.about_developer), R.drawable.ic_developer_sidebar));
                barItems.add(new NavigationBarItem(Constant.SETTINGS, getString(R.string.setting), R.drawable.ic_sittings_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_LOGOUT, getString(R.string.sing_out), R.drawable.ic_logout_sidebar));
                break;

            case Constant._TYPE_SHOW:
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_PROFILE, getString(R.string.my_profile), R.drawable.ic_profile_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_BRANCH, getString(R.string.branchs), R.drawable.ic_my_stores_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_MY_PROFILE, getString(R.string.requests), R.drawable.ic_requests_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_SHARCH, getString(R.string.search), R.drawable.ic_search_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_BLOCK, getString(R.string.block_men), R.drawable.ic_block));
                barItems.add(new NavigationBarItem(Constant._PAGE_WISHLISTED, getString(R.string.favorite), R.drawable.ic_wishlist_sidebar));
                barItems.add(new NavigationBarItem(Constant._SHARE, getString(R.string.invite_friend), R.drawable.ic_invite_friends_sidebar));
                //      barItems.add(new NavigationBarItem(Constant._HALPER, getString(R.string.help), R.drawable.ic_help_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_ABOUT_DEVLOPER, getString(R.string.about_developer), R.drawable.ic_developer_sidebar));
                barItems.add(new NavigationBarItem(Constant.SETTINGS, getString(R.string.setting), R.drawable.ic_sittings_sidebar));
                barItems.add(new NavigationBarItem(Constant._PAGE_LOGOUT, getString(R.string.sing_out), R.drawable.ic_logout_sidebar));
                break;


        }
        adapter.notifyDataSetChanged();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.menu:

                if (!drawer.isOpaque()) {
                    drawer.openDrawer(GravityCompat.END);
                }
                break;
            case R.id.back:
                onBackPressed();
                break;
            case R.id.search:
                if (pref.getUser() != null) {
                    openFragment(new SharchFragment());
                } else {

                    AapHalper.CreateDialog(R.layout.dialog_sgin_up, this, new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            ButtonView signUp, close;
                            signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                            close = (ButtonView) dialog.findViewById(R.id.close);

                            signUp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    MainActivity.this.finish();
                                }
                            });
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                }
                            });
                        }
                    });
                }


                break;
        }
    }



}
