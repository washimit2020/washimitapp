<?php

namespace App\Http\Controllers;

use App\Http\Constants;
use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{

    /**
     * register user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            "name" => "required|max:255",
            "email" => "required|email|unique:users|max:255",
            "password" => "required|min:6",
            "reg_id"=>'required|string|max:1000',
        ]);
        $user = $this->create($request->only('name','email','password','reg_id'));

        return response()->json($this->get_current_user_object($user));

    }


    /**
     * to create the user instance
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    protected function create(array $data){
        $data["password"] = encrypt($data["password"]);
        $data["api_token"] = str_random(120);
        $data["user_role_id"] = Constants::USER_ROLE_COSTUMER;
        $this->distinct_reg_id(0,$data["reg_id"]);

        return User::query()->create($data);
    }

    public function login(Request $request)
    {
        $this->validate($request, ["email" => "required|email","reg_id"=>'required|string|max:1000', "password" => "required|max:255"]);

        $response = $this->get_user_instance($request->only('email', 'password','reg_id'));

        if (isset($response["user"])) {
            return response()->json($response);
        }

        return response()->json(["email" => [$response]], 422);
    }


    /**
     * @param array $data
     * @return mixed
     */
    protected function get_user_instance(array $data)
    {

        $user = User::where('email', $data["email"])->first();

        if (isset($user) && (decrypt($user->password) == $data["password"]) ) {
            //to make the reg_id unique
            $this->distinct_reg_id($user->id,$data["reg_id"]);

            $user->update([
                "api_token" => str_random(120),
                "reg_id"=>$data['reg_id']
            ]);

            return $this->get_current_user_object($user);
        }
        return trans('auth.authentication failed');
    }


    public function registration_id_updater(Request $request)
    {
        $this->validate($request, [
            "reg_id"=>'required|string|max:1000',
        ]);

        //to make the reg_id unique
        $this->distinct_reg_id($request->user()->id,$request->reg_id);

        $request->user()->reg_id = $request->reg_id;

        return response()->json('updated');

    }

    /**
     * get the user object
     *
     * @param $user
     * @return array
     */
    private function get_current_user_object($user)
    {
        return ["user" => $user->makeVisible('api_token')];
    }

    /**
     * distinct the reg_id
     *
     * @param integer $id
     * @param string $reg_id
     *
     */
    private function distinct_reg_id($id,$reg_id)
    {
        User::query()->where([
            ['reg_id',$reg_id],
            ['id','<>',$id]
        ])->update(["reg_id"=>null]);
    }
}
