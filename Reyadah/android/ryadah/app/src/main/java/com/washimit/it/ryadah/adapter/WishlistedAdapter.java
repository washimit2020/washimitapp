package com.washimit.it.ryadah.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Product;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammed Ali on 04/30/2017.
 */

public class WishlistedAdapter extends RecyclerView.Adapter {

    private final int PRODUCT = 1;
    private final int VIEW_PROG = 0;


    List<Product> list;
    Activity context;
    ApiRequest request;
    private int lastVisibleItem, totalItemCount;

    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;
    Gson gson;

    public WishlistedAdapter(List<Product> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        request = new ApiRequest(context);
        gson = new Gson();

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == PRODUCT) {
            return new WishlistedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wishlisted, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_load_more, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof WishlistedHolder) {
            Product product = list.get(position);
            ((WishlistedHolder) holder).item_wishlisted_user_name.setText(product.getUser().getName());
            ((WishlistedHolder) holder).item_wishlisted_addrese.setText(product.getUser().getSector_name() + " - " + product.getUser().getStreet_name());
            ((WishlistedHolder) holder).item_wishlisted_favorite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(1)));
            ((WishlistedHolder) holder).item_wishlisted_rating.setRating((float) product.getRating());
            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_store).into(((WishlistedHolder) holder).item_wishlisted_img);
        }


    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) != null ? PRODUCT : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishlistedHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_wishlisted_img)
        ImageView item_wishlisted_img;
        @BindView(R.id.item_wishlisted_favorite)
        ImageView item_wishlisted_favorite;
        @BindView(R.id.item_wishlisted_user_name)
        TextView item_wishlisted_user_name;
        @BindView(R.id.item_wishlisted_addrese)
        TextView item_wishlisted_addrese;
        @BindView(R.id.item_wishlisted_rating)
        RatingBar item_wishlisted_rating;

        public WishlistedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


            item_wishlisted_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openActivity(ProductDetailsActivity.class, gson.toJson(list.get(getAdapterPosition())));

                }
            });
            item_wishlisted_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());
                    list.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                    notifyItemRangeChanged(getAdapterPosition(), list.size()-1);
                    addToWishlisted(product.getId());
                }
            });
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avi)
        AVLoadingIndicatorView avi;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }


    private void addToWishlisted(int productId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product_id", productId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/wishlist", hashMap, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("wishlist", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("wishlist", errorMessage);
            }
        });


    }


    public void setUrl(String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    private void openActivity(Class activity, String data) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("data", data);
        context.startActivity(intent);

    }


}
