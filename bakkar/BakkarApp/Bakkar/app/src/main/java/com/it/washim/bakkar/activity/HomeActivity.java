package com.it.washim.bakkar.activity;


import android.os.Bundle;
//import android.util.Log;
import android.view.View;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.halper.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.leolin.shortcutbadger.ShortcutBadger;


public class HomeActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.biography_and_career)
    MyButtonView biography_and_career;
    @BindView(R.id.articles)
    MyButtonView articles;
    @BindView(R.id.books)
    MyButtonView books;
    @BindView(R.id.Sounds)
    MyButtonView Sounds;
    @BindView(R.id.video_page)
    MyButtonView video_page;
    @BindView(R.id.communiction)
    MyButtonView communiction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ShortcutBadger.removeCount(this);
        shardPref().setBadger(0);

        ButterKnife.bind(this);
        biography_and_career.setOnClickListener(this);
        articles.setOnClickListener(this);
        books.setOnClickListener(this);
        Sounds.setOnClickListener(this);
        video_page.setOnClickListener(this);
        communiction.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.biography_and_career:
                openActivity(MainActivity.class, "page_type", Constant.BIOGRAPHY_AND_CAREER);
                break;
            case R.id.articles:
                openActivity(MainActivity.class, "page_type", Constant.ARTICLES);
                break;

            case R.id.books:
                openActivity(MainActivity.class, "page_type", Constant.BOOKS);
                break;
            case R.id.Sounds:
                openActivity(MainActivity.class, "page_type", Constant.SOUNDS);
                break;
            case R.id.video_page:
                openActivity(MainActivity.class, "page_type", Constant.VIDEO);
                break;
            case R.id.communiction:
                openActivity(MainActivity.class, "page_type", Constant.AGENTS_AND_OPPOSED);

                break;
        }
    }


}
