package com.it.washmit.bookgoodmorning.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.coustumView.TextViewCustem;
import com.it.washmit.bookgoodmorning.model.Post;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by mohammad on 10/24/2017.
 */

public class BolgAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<Post> posts;

    public BolgAdapter(Context context, ArrayList<Post> posts) {
        this.context = context;
        this.posts = posts;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BlogViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blog, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BlogViewHolder) {
            Post post = posts.get(position);

            ((BlogViewHolder) holder).textView.setText(Html.fromHtml(post.caption));
            if (post.photos.size() > 0) {
                Picasso.with(context).load(post.photos.get(0).original_size.url).fit().placeholder(R.drawable.goodmorningicon).centerCrop().into(((BlogViewHolder) holder).imageView);
            }
        }

    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public class BlogViewHolder extends RecyclerView.ViewHolder {
        TextViewCustem textView;
        ImageView imageView;

        public BlogViewHolder(View itemView) {
            super(itemView);
            textView = (TextViewCustem) itemView.findViewById(R.id.text);
            imageView = (ImageView) itemView.findViewById(R.id.image);


        }
    }
}
