//
//  PageTitle_TableViewCell.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

class PageTitle_TableViewCell: UITableViewCell {

    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var pageTitle: UILabel!
    var index: Int!

    var page:Page!{
        didSet{
            pageNumber.text = "\(index ?? 0)"
            pageTitle.attributedText = page.quran
        }
    }
    var section:Section!{
        didSet{
            pageNumber.text = "\(section.id!)"
            pageTitle.text = section.title
        }
    }
}
