package com.it.washim.bakkar.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.example.jean.jcplayer.JCAudio;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyEditTextView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.halper.MyPalyer;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Sound;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SoundFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    ArrayList<Sound> list;
    @BindView(R.id.jcplayer)
    MyPalyer jcplayerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;

    @BindView(R.id.sarah)
    MyEditTextView search;
    @BindView(R.id.no_data)
    MyTextView no_data;

    @BindView(R.id.ratray)
    MyButtonView ratray;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sound, container, false);
        ButterKnife.bind(this, view);


        list = new ArrayList<>();


        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);
                getSounds("%20");
            }
        });
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    swipe_refresh.setRefreshing(true);

                    if (search.getText().toString().length() > 0) {

                        getSounds(search.getText().toString());
                    } else {
                        getSounds("%20");
                    }
                }


                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(search.getWindowToken(), 0);


                return false;
            }
        });

        swipe_refresh.setOnRefreshListener(this);

        ratray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipe_refresh.setRefreshing(true);
                getSounds("%20");
            }
        });
        return view;

    }

    @Override
    public void onStop() {
        super.onStop();
        jcplayerView.stop();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        jcplayerView.stop();

    }

    @Override
    public void onPause() {
        super.onPause();
        jcplayerView.stop();

    }


    private void getSounds(String search) {
        ratray.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);
        request().stringRequest(Request.Method.GET, getActivity(), "posts/" + Constant.PAGES_AUDIO + "/" + search, null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                jcplayerView.stop();

                JSONObject object;
                JSONObject posts;
                object = new JSONObject(response);
                posts = object.getJSONObject("posts");
                list.clear();
                list.addAll((Collection<? extends Sound>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Sound>>() {
                }.getType()));


                jcplayerView.initWithTitlePlaylist(list, posts.getString("next_page_url"));
                swipe_refresh.setRefreshing(false);


                if (list.size() > 0) {
                    no_data.setVisibility(View.GONE);
                    swipe_refresh.setVisibility(View.VISIBLE);
                } else {
                    no_data.setText("لا يوجد نتائج");

                    no_data.setVisibility(View.VISIBLE);
                    swipe_refresh.setVisibility(View.GONE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipe_refresh.setRefreshing(false);
                Toast.makeText(getActivity(), "توجد مشكلة بالاتصال بالانترنت", Toast.LENGTH_SHORT).show();
                swipe_refresh.setVisibility(View.GONE);
                no_data.setText("لا يوجد اتصال بالانترنت");
                no_data.setVisibility(View.VISIBLE);

                ratray.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRefresh() {

        getSounds("%20");

    }

}
