package com.it.washim.wojooohapp.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DistributionAgentsDetailsActivity extends BaseActivity {
    @BindView(R.id.name)
    MyTextView name;
    @BindView(R.id.content)
    MyTextView content;
    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution_agents_details);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            post = getGson().fromJson(getIntent().getExtras().getString("DistributionAgentsDetails"), Post.class);
        }
        if (post != null) {
            name.setText(post.title);
            content.setText(post.content);
        }
    }
}
