package com.it.washim.wojooohapp.Halper;

import android.content.Context;
import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Interfases.OnRequestPublishCollBack;
import com.it.washim.wojooohapp.Model.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mohammad on 9/19/2017.
 */

public class ApiRequests {
    private static final ApiRequests ourInstance = new ApiRequests();
    private static RequestQueue requestQueue;
    private static Map<String, String> map;
    Gson gson=new Gson();

    public static ApiRequests getInstance(Context context) {
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
            map = new HashMap<>();

        }
        return ourInstance;
    }

    private ApiRequests() {
    }

    public void request(int method, String url, @Nullable final Map<String, String> params, final OnRequestCollBack collBack) {
        StringRequest request = new StringRequest(method,Constant.URL+ url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                collBack.onResponse(gson.fromJson(response, Data.class));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                collBack.onErrorResponse(error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                map.put("Accept", "application/json");
                map.put("app-key", "base64:F5DFu9ou9t4q2S96/9l/vehX0FaRKbJ858iSGcbe3Ww=");
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }
    public void requestString(int method, String url, @Nullable final Map<String, String> params, final OnRequestPublishCollBack collBack) {
        StringRequest request = new StringRequest(method,Constant.URL+ url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                collBack.onResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                collBack.onErrorResponse(error);

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                map.put("Accept", "application/json");
                map.put("app-key", "base64:F5DFu9ou9t4q2S96/9l/vehX0FaRKbJ858iSGcbe3Ww=");
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(request);
    }


}
