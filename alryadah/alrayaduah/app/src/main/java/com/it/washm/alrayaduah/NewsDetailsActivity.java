package com.it.washm.alrayaduah;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.adapter.DetailesAttachmentsAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.customview.ObservableScrollView;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.Attachment;

public class NewsDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.btn_wishlist)
    ImageButton wishlist;
    @BindView(R.id.btn_share)
    ImageButton share;
    @BindView(R.id.btn_textSize)
    ImageButton textSize;
    @BindView(R.id.btn_back)
    ImageButton back;
    @BindView(R.id.Views_news_details)
    TextView views_news_details;
    @BindView(R.id.comment_news_details)
    TextView comment_news_details;
    @BindView(R.id.like_news_details)
    TextView like_news_details;
    @BindView(R.id.time_details)
    TextView time_details;
    @BindView(R.id.date_details)
    TextView date_details;
    @BindView(R.id.Attachment_recycler_view)
    RecyclerView AttachmentRecyclerView;
    @BindView(R.id.scrollView)
    ObservableScrollView scrollView;

    //insert from intent
    @BindView(R.id.user_profile_img)
    ImageView user_profile_img;
    @BindView(R.id.news_item_main_img)
    ImageView img_news_details;
    @BindView(R.id.title_details)
    TextView title_details;
    @BindView(R.id.name_details)
    TextView name_details;
    @BindView(R.id.content_details)
    TextView content_details;

    String newId, imgNew, imgUser, title, userName, content, time, date;

    Intent intent;
    Bundle extras;
    int views_count, likes_count, comments_count;
    boolean IsLike, is_wishlisted;
    Requests requests;
    public static HashMap<String, String> Stringparams;
    SharedPref pref;
    DetailesAttachmentsAdapter mAdapter;
    public static ArrayList<Attachment> attachments;
    Gson gson;
    CustamViewFont viewFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);
        viewFont = new CustamViewFont(this, "Cairo-Regular.ttf");
        setFont();
        gson = new Gson();

        AttachmentRecyclerView.setHasFixedSize(true);


        AttachmentRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));


        mAdapter = new DetailesAttachmentsAdapter(this, attachments);

        AttachmentRecyclerView.setAdapter(mAdapter);

//        AttachmentRecyclerView.smoothScrollToPosition(attachments.size())

        pref = new SharedPref(getApplicationContext().getApplicationContext(), "MyPref");

        Stringparams = new HashMap<String, String>();

        requests = new Requests(getApplicationContext(), this);

        extras = getIntent().getExtras();

        if (extras != null) {
            newId = extras.getString("newId");
            imgNew = extras.getString("img_new");
            imgUser = extras.getString("img_user");
            title = extras.getString("title");
            userName = extras.getString("name");
            content = extras.getString("content");
            time = extras.getString("time");
            date = extras.getString("date");


        }


        getNews();


        time_details.setText(time);
        date_details.setText(date);

        Picasso.with(this).load(Links.UrlImg + imgNew).fit().centerCrop().placeholder(R.drawable.ic_blank_image).into(img_news_details);

        Picasso.with(this).load(Links.UrlImg + imgUser).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic).into(user_profile_img);

        title_details.setText(title);
        name_details.setText(userName);
        content_details.setText(content);


        scrollView.setScrollViewListener(new ObservableScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {

                View view = scrollView.findViewById(R.id.news_item_main_img);

                if (view != null) {
                    view.setTranslationY(scrollView.getScrollY() / 2);
                }

            }
        });
        focusOnView();

        comment_news_details.setOnClickListener(this);
        like_news_details.setOnClickListener(this);
        wishlist.setOnClickListener(this);
        share.setOnClickListener(this);
        textSize.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    private final void focusOnView() {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, like_news_details.getBottom());
            }
        });
    }

    public void setFont() {
        viewFont.setfont(views_news_details);
        viewFont.setfont(comment_news_details);
        viewFont.setfont(like_news_details);
        viewFont.setfont(time_details);
        viewFont.setfont(date_details);
        viewFont.setfont(name_details);
        viewFont.setfont(content_details);
        viewFont.setfont(title_details, "Cairo-Bold.ttf");

    }

    @Override
    public void onBackPressed() {
        if (MainActivity.activity == true) {
            super.onBackPressed();
        } else {
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        getNews();
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.like_news_details:
                if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {
                    like_news_details.setClickable(false);

                    if (!IsLike) {

                        like_news_details.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_blue_act, 0);
                        IsLike = true;
                        like_news_details.setClickable(true);
                        like_news_details.setText(String.valueOf(likes_count + 1));
                        likes_count = likes_count + 1;

                    } else {


                        like_news_details.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_blue, 0);
                        IsLike = false;
                        like_news_details.setClickable(true);
                        like_news_details.setText(String.valueOf(likes_count - 1));
                        likes_count = likes_count - 1;

                    }

                    Stringparams.put("news_id", newId);
                    requests.StringRequest(Request.Method.POST, "like/news", Stringparams, "", new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            like_news_details.setClickable(true);

                        }

                        @Override
                        public void onRequestError(VolleyError errorMessage) {
                            like_news_details.setClickable(true);

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "يجب تسجيل الدخول للآعجاب!", Toast.LENGTH_LONG).show();

                }

                break;

            case R.id.comment_news_details:
                if (AppHelper.isOnline(this)) {
                    intent = new Intent(NewsDetailsActivity.this, commentActivity.class);
                    intent.putExtra("newId", newId);
                    startActivity(intent);
                } else {
                    Snackbar.make(getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                }

                break;
            case R.id.btn_wishlist:
                if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {

                    Stringparams.clear();
                    Stringparams.put("news_id", newId);
                    if (!is_wishlisted) {
                        AppHelper.wishlist("wishlist", this, Stringparams, "تم الاضافة الي المفضلة");
                        wishlist.setImageResource(R.drawable.ic_wish_list_act);
                        is_wishlisted = true;

                    } else {
                        is_wishlisted = false;
                        AppHelper.wishlist("wishlist", this, Stringparams, "تم الازاله من المفضلة");
                        wishlist.setImageResource(R.drawable.ic_wish_list);




                    }

                } else {
                    Toast.makeText(getApplicationContext(), "يجب تسجيل الدخول !", Toast.LENGTH_LONG).show();

                }
                break;
            case R.id.btn_share:
                Uri bmpUri = AppHelper.getLocalBitmapUri(img_news_details);
                if (bmpUri != null) {
                    // Construct a ShareIntent with link to image
                    Intent shareIntent = new Intent();
                    shareIntent.setAction(Intent.ACTION_SEND);
                    shareIntent.putExtra(Intent.EXTRA_TEXT, content_details.getText().toString());
                    shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                    shareIntent.setType("image/*");
                    // Launch sharing dialog for image
                    startActivity(Intent.createChooser(shareIntent, "Share Image"));
                } else {
                    // ...sharing failed, handle error
                }
                break;
            case R.id.btn_textSize:
                dilog();
                break;
            case R.id.btn_back:
                if (MainActivity.activity == true) {
                    this.finish();
                } else {
                    intent = new Intent(this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                }
                break;
        }


    }

    public void getNews() {
        requests.StringRequest(Request.Method.GET, "news/" + newId, "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                JSONObject object = null;
                JSONObject message = null;
                JSONObject news = null;
                try {
                    object = new JSONObject(result);

                    if (!object.getBoolean("success")) {
                        return;
                    }
                    message = object.getJSONObject("message");
                    news = message.getJSONObject("news");

                    views_count = news.getInt("views_count");
                    likes_count = news.getInt("likes_count");
                    comments_count = news.getInt("comments_count");

                    views_news_details.setText(views_count + "");
                    comment_news_details.setText(comments_count + "");
                    like_news_details.setText(likes_count + "");

                    if (news.getBoolean("is_wishlisted")) {
                        is_wishlisted = true;
                        wishlist.setImageResource(R.drawable.ic_wish_list_act);
                        Log.d("is_wishlistedt", news.getBoolean("is_wishlisted") + "");

                    } else {
                        Log.d("is_wishlistedf", news.getBoolean("is_wishlisted") + "");
                        is_wishlisted = false;

                    }
                    {

                    }
                    if (news.getBoolean("is_liked")) {
                        IsLike = true;
                        like_news_details.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_blue_act, 0);
                    } else {
                        IsLike = false;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }

    public void dilog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilog_text_size);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        LinearLayout small, normal, large;
        TextView small_text, normal_text, large_text, text_size;


        text_size = (TextView) dialog.findViewById(R.id.text_size);
        small_text = (TextView) dialog.findViewById(R.id.text_small);
        normal_text = (TextView) dialog.findViewById(R.id.text_normal);
        large_text = (TextView) dialog.findViewById(R.id.text_large);
        small = (LinearLayout) dialog.findViewById(R.id.small);
        normal = (LinearLayout) dialog.findViewById(R.id.normal);
        large = (LinearLayout) dialog.findViewById(R.id.large);


        viewFont.setfont(text_size);
        viewFont.setfont(small_text, "Cairo-Regular.ttf");
        viewFont.setfont(normal_text, "Cairo-Regular.ttf");
        viewFont.setfont(large_text, "Cairo-Regular.ttf");

        small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content_details.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.text_small));

            }
        });

        normal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content_details.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.text_normal));

            }
        });

        large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                content_details.setTextSize(TypedValue.COMPLEX_UNIT_SP, getResources().getDimension(R.dimen.text_large));

            }
        });




        dialog.show();
    }


}
