package com.it.washim.bakkar.fragment;


import android.app.Dialog;
import android.graphics.Typeface;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.ArticleDetailsActivity;
import com.it.washim.bakkar.activity.BookDetailsActivity;
import com.it.washim.bakkar.activity.MainActivity;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class BiographyAndCareerFragment extends BaseFragment {

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.text)
    MyTextView text;
    @BindView(R.id.sating)
    ImageView sating;

    public BiographyAndCareerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_biography_and_career, container, false);
        ButterKnife.bind(this, view);
        text.setText(getActivity().getString(R.string.hello));

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()) {
                    case R.id.back:
                        ((MainActivity) getActivity()).finish();

                        break;


                }


            }
        };


        sating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settingsDialog();
            }
        });


        back.setOnClickListener(listener);

        update();
        return view;
    }


    private void settingsDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_sating);

        Button day_mode = (MyButtonView) dialog.findViewById(R.id.day_mode);
        final MyTextView proogresbar = (MyTextView) dialog.findViewById(R.id.proogresbar);
        Button night_mode = (MyButtonView) dialog.findViewById(R.id.night_mode);
        final SeekBar text_size = (SeekBar) dialog.findViewById(R.id.text_size);
        final Spinner text_font_type = (Spinner) dialog.findViewById(R.id.text_font_type);
        final MyButtonView hide = (MyButtonView) dialog.findViewById(R.id.hide);

        day_mode.setVisibility(View.GONE);
        night_mode.setVisibility(View.GONE);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("TheSansArab-Plain_0.ttf");
        arrayList.add("TheSansArab-Light_0.ttf");
        arrayList.add("TheSansArab-Black_0.ttf");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_dropdown_item, arrayList);

        text_font_type.setAdapter(arrayAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        text_font_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        text_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                proogresbar.setText(progress + "%");
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                update();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                update();
            }
        });


        text_size.setProgress(shardPref().getTextSize());

        dialog.show();


    }


    private void update() {

        //text.setTextSize(shardPref().getTextSize());
        text.setTextSize(shardPref().getTextSize() > 14 ? shardPref().getTextSize() : 14);

        text.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), "fonts/" + shardPref().getFontType()));


    }


}
