<?php

namespace App\__ModelsProducts;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ShareProduct extends Model
{
    protected $fillable = ["product_id",'status_id','user_id','shareable_id',"shareable_type"];

    protected $with = ["product","shareable"];

    public function shareable()
    {
        return $this->morphTo();
    }

    public function product(){
        return $this->belongsTo(Product::class,"product_id");
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function sharedWith()
    {
        return $this->belongsTo(User::class,'shareable_id');
    }

}
