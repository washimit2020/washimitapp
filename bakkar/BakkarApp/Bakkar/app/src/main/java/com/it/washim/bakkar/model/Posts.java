package com.it.washim.bakkar.model;

import java.util.ArrayList;

/**
 * Created by mohammad on 8/7/2017.
 */

public class Posts {
    public int id;
    public String title;
    public String description;
    public double rating;
    public Link link;
    public String price;
    public String published_at;
    public int page_id;
    public int parent_id;
    public String created_at;
    public String updated_at;
    public boolean is_wishlisted;
    public double rated;
    public String started_at;
    public ArrayList<Posts> children;
    public ArrayList<Attachments> attachments;
}
