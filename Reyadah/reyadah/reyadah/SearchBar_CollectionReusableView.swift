//
//  SearchBar_CollectionReusableView.swift
//  reyadah
//
//  Created by WASHM on 5/6/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class SearchBar_CollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var sort: AspectFitButton!
    @IBOutlet weak var filter: AspectFitButton!
    @IBOutlet weak var bigShape: AspectFitButton!
    @IBOutlet weak var smallShape: AspectFitButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        filter.tag = 1
        sort.tag = 2
        bigShape.tag = 1
        smallShape.tag = 2
    }
    
}
