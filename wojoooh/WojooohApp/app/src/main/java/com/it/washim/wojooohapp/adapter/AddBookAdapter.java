package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washim.wojooohapp.CustemView.MyEditText;
import com.it.washim.wojooohapp.Interfases.OnBooksCollBack;
import com.it.washim.wojooohapp.Model.BookRequest;
import com.it.washim.wojooohapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/11/2017.
 */

public class AddBookAdapter extends RecyclerView.Adapter {
    ArrayList<BookRequest> list;
    OnBooksCollBack collBack;

    public AddBookAdapter(Context context, ArrayList<BookRequest> list) {
        this.list = list;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item, parent, false), new MyCustomEditTextTitelListener(), new MyCustomEditTextNumberlListener());
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof BookViewHolder) {

            final BookRequest request = list.get(position);


            ((BookViewHolder) holder).myCustomEditTextTitelListener.updatePosition(holder.getAdapterPosition());
            ((BookViewHolder) holder).myCustomEditTextNumberlListener.updatePosition(holder.getAdapterPosition());
            ((BookViewHolder) holder).bookName.setText(request.bookTitle);

            if (request.bookNumber > 0) {
                ((BookViewHolder) holder).numberOfBook.setText(request.bookNumber + "");
            } else {
                ((BookViewHolder) holder).numberOfBook.setText("");

            }

            if (position == list.size()) {
                ((BookViewHolder) holder).bookName.requestFocus();

            }


            if (position == 0) {
                ((BookViewHolder) holder).delete.setVisibility(View.GONE);
            } else {
                ((BookViewHolder) holder).delete.setVisibility(View.VISIBLE);
            }


        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class BookViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.delete)
        ImageView delete;
        @BindView(R.id.book_name)
        MyEditText bookName;
        @BindView(R.id.number_of_book)
        MyEditText numberOfBook;
        MyCustomEditTextTitelListener myCustomEditTextTitelListener;
        MyCustomEditTextNumberlListener myCustomEditTextNumberlListener;

        public BookViewHolder(View itemView, MyCustomEditTextTitelListener myCustomEditTextTitelListener, MyCustomEditTextNumberlListener myCustomEditTextNumberlListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.myCustomEditTextTitelListener = myCustomEditTextTitelListener;
            this.myCustomEditTextNumberlListener = myCustomEditTextNumberlListener;
            bookName.addTextChangedListener(myCustomEditTextTitelListener);
            numberOfBook.addTextChangedListener(myCustomEditTextNumberlListener);

            numberOfBook.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        list.add(new BookRequest("", 0));
                        notifyDataSetChanged();

                    }


                    return false;

                }
            });

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list.remove(getAdapterPosition());
                    //   notifyDataSetChanged();
//                    notifyItemRemoved(position);
//                    notifyItemRangeChanged(position,list.size());

                    notifyDataSetChanged();

                    collBack.onSend(list);

                }
            });
        }


    }


    public void setListOnBooklisenar(OnBooksCollBack collBack) {
        this.collBack = collBack;
    }


    private class MyCustomEditTextTitelListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            list.get(position).bookTitle = charSequence.toString();
            collBack.onSend(list);
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }

    private class MyCustomEditTextNumberlListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (!charSequence.toString().equalsIgnoreCase("")) {
                list.get(position).bookNumber = Integer.parseInt(charSequence.toString());
                collBack.onSend(list);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
}
