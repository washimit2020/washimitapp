<?php

namespace App\Events;

use App\Http\Constants;
use App\Notification;
use App\Notifications_Key;
use App\User;
use Illuminate\Queue\SerializesModels;

abstract class Event
{
    use SerializesModels;

    const SEND_URL = 'https://fcm.googleapis.com/fcm/send';
    const NOTIFICATION_URL = 'https://android.googleapis.com/gcm/notification';
    const SERVER_KEY = 'AAAA0mODNaU:APA91bHP2FrYW3JhGpKwr-naAhx7ywVtp48x8NCFbSvnKCC4oeNDUkVa9GI9SY4RnFdECHhcGo4pVq1eMCrSgh1nUOv6U-w8gVMSyC7axd6pAPi46U3bWxST0SGp4YWALx0tMFuYX5rm';
    const PROJECT_ID = '903612675493';
    /**
     *
     * @var array
     */

    public $headers = [
        'Authorization: key=' . self::SERVER_KEY,
        'Content-Type: application/json',
        "project_id: " . self::PROJECT_ID
    ];


    /**
     * The recipient information for the message.
     *
     * @var array
     */
    public $title = "";

    /**
     * The recipient information for the message.
     *
     * @var array
     */
    public $body = "";

    /**
     * The message data .
     *
     * @var array
     */
    public $data = [];


    public function send_notification()
    {

//        $notification_id = $this->create_fcm_notification($fields);

        User::query()->whereNotNull('reg_id')->where('user_role_id', Constants::USER_ROLE_COSTUMER)->chunk(100, function ($users)  {
            foreach ($users as $user) {
                $id = app('Faker\Provider\Uuid')->uuid();

                $fields = [
                    "to"=>$user->reg_id,
                    "notification" => [
                        "title" => $this->title,
                        "body" => $this->body,
                        "sound"=>"new",
                        "badge"=>$user->unread_notifications()->count() + 1
                    ],
                    'data' => $this->data,
                ];
                $fields["data"]["badge"] = $fields["notification"]["badge"];
                $fields["data"]["notification_id"] = $id;


                Notification::create([
                    'id' => $id,
                    'user_id' => $user->id,
                    'data' => json_encode($fields, JSON_PRETTY_PRINT),
                    'type' => self::class
                ]);

                $fields["notification"]['title'] = trans($this->title);
                $fields["notification"]['body'] = trans($this->body);
                $result = $this->send_curl($fields, self::SEND_URL);
            }
        });




//        $reuslt =
//
//        dd($reuslt);
    }

    private function create_fcm_notification(array $data)
    {
        $key = Notifications_Key::firstOrNew(['name' => self::class]);

        $reg_id = $key->registration_ids;

        if (!isset($reg_id)) {
            $reg_id = [];
        }

        User::whereNotNull('reg_id')->whereNotIn('reg_id', $reg_id)->where('user_role_id', Constants::USER_ROLE_COSTUMER)->chunk(100, function ($users) use ($data, &$reg_id) {
            foreach ($users as $user) {
                $reg_id [] = $user->reg_id;
                Notification::create([
                    'id' => app('Faker\Provider\Uuid')->uuid(),
                    'user_id' => $user->id,
                    'data' => json_encode($data, JSON_PRETTY_PRINT),
                    'type' => self::class
                ]);
            }
        });

        $key->registration_ids = $reg_id;

        $key->save();


        $fields = [
            'operation' => $key->wasRecentlyCreated ? 'create' : 'add',
            'notification_key_name' => self::class,
            'notification_key' => $key->key,
            'registration_ids' => $reg_id
        ];

        $json = $this->send_curl($fields, self::NOTIFICATION_URL);

        if (isset($json->error)) {
           $json = $this->check_json_error($fields,$json->error);
        }

        if (isset($json->notification_key)){
            $key->update(['key'=>$json->notification_key]);
        }

        return $key->key;
    }

    private function send_curl($fields, $url,$is_post = true)
    {
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_POST => $is_post,
            CURLOPT_HTTPHEADER => $this->headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 120,
            CURLOPT_TIMEOUT => 120,
        );

        if ($is_post){
            $options [CURLOPT_POSTFIELDS] =  json_encode($fields, JSON_PRETTY_PRINT);
        }
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);

        return json_decode($result);
    }

    private function check_json_error($fields,$error)
    {
        switch ($error) {
            case "notification_key already exists":
                $fields["operation"] = "add";
                $json = $this->send_curl($fields, self::NOTIFICATION_URL);
                if (isset($json->error) && $error != "notification_key already exists") {
                    $this->check_json_error($fields,$error);
                }
                return $json;
            default :
                $fields["operation"] = "add";

                $json = $this->send_curl($fields, self::NOTIFICATION_URL . "?notification_key_name=".self::class,false);
                return $json;
        }
    }


}
