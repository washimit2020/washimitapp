<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Events\PostCreated;
use App\Http\Constants;
use App\Post;
use App\Posts_Rating;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Symfony\Component\HttpFoundation\StreamedResponse;

class PostsController extends Controller
{
    public function index(Request $request,$type, $search = null)
    {
        if (isset($search)){
            $search = trim(str_replace("%20"," ",$search));
        }

        $posts = Post::where([
            ["page_id", $type],
            ["title", "like", "%{$search}%"],
        ])->doesntHave("parent")->latest();


        if ($request->has('filter') ){
//            switch ($request->filter){
//                case Constants::FREE_FILTER:
//                    break;
//                default :
//                    $posts->latest();
//                    break;
//            }
	        $posts->whereHas('categories',function ($query) use ($request){
	        	$query->where('categories.id',$request->filter);
	        });
        }
        $posts = $posts->paginate();
        return response()->json(["posts" => $posts]);
    }

    public function store(Request $request)
    {

        $rules = [
            "title" => "required|string|max:255",
            "page_id" => "required|exists:pages,id",
            "parent_id" => "exists:posts,id",
            "description" => "string|max:15000",
            "price" => "nullable|numeric|min:0.5|max:1000",
            "media_url" => "nullable|url|max:255",
            "android_url" => "nullable|url|max:255",
            "ios_url" => "nullable|url|max:255",
            "published_at" => "date|date_format:Y",
            "categories" => "array",
            "categories.*" => "required|exists:categories,id",
            "attachments" => "array",
            "attachments.*.type" => "required|exists:attachments__types,id",
//            "attachments.*.file" => "required|
//             mimetypes:image/png,image/jpeg,application/pdf,audio/midi,audio/mpeg,audio/webm,audio/ogg,audio/wav",
            "attachments.*.file" => "required|mimetypes:image/png,image/jpeg,application/pdf",
        ];

        $this->validate($request, $rules);

        $data = $request->only(array_keys($rules));

        if (isset($data['android_url'])){
        	$data["link"]["android_url"] = $data["android_url"];
        }

        if (isset($data['ios_url'])){
        	$data["link"]["ios_url"] = $data["ios_url"];
        }

        if (isset($data['media_url'])){
        	$data["link"]["media_url"] = $data["media_url"];
        }

        $post = Post::create($data);

        if ($request->has('categories')){
	        $post->categories()->attach($request->categories);
        }

        event(new PostCreated($post));


        if ($request->has('attachments')) {
            foreach ($request->attachments as $attachment) {
                $file = $attachment["file"];
                switch ($attachment["type"]) {
                    case Constants::ATTACHMENT_BOOK:
                        $dir = "book";
                        break;
                    case Constants::ATTACHMENT_COVER:
                        $dir = "cover";
                        break;
                    default:
                        $dir = "audio";
                }
                $dir = "posts/{$post->id}/{$dir}";
                Container::getInstance()->make(FilesystemFactory::class)->disk("local")->deleteDirectory($dir);

                $data = [
                    "path" => $file->store($dir),
                    "extension" => $file->extension(),
                    "mime" => $file->getMimeType(),
                    "attachment_type_id" => $attachment["type"]
                ];
                $post->attachments()->create($data);
            }
        }
        $post->load('attachments');
        return response()->json(['post' => $post]);
    }

    public function update(Request $request)
    {
        $rules = [
	        "post_id" => "required|exists:posts,id",
	        "title" => "required|string|max:255",
	        "page_id" => "required|exists:pages,id",
	        "parent_id" => "exists:posts,id",
	        "description" => "string|max:15000",
	        "price" => "nullable|numeric|min:0.5|max:1000",
	        "media_url" => "nullable|url|max:255",
	        "android_url" => "nullable|url|max:255",
	        "ios_url" => "nullable|url|max:255",
	        "published_at" => "date|date_format:Y",
	        "categories" => "array",
	        "categories.*" => "required|exists:categories,id",
	        "attachments" => "array",
	        "attachments.*.type" => "required|exists:attachments__types,id",
//            "attachments.*.file" => "required|
//             mimetypes:image/png,image/jpeg,application/pdf,audio/midi,audio/mpeg,audio/webm,audio/ogg,audio/wav",
	        "attachments.*.file" => "required|mimetypes:image/png,image/jpeg,application/pdf",
        ];

        $this->validate($request, $rules);

        $data = $request->only(array_keys($rules));


        $post = Post::find($data['post_id']);

	    if ($request->has('categories')){
		    $post->categories()->sync($request->categories);
	    }

	    if (isset($data['android_url'])){
		    $data["link"]["android_url"] = $data["android_url"];
	    }

	    if (isset($data['ios_url'])){
		    $data["link"]["ios_url"] = $data["ios_url"];
	    }

	    if (isset($data['media_url'])){
		    $data["link"]["media_url"] = $data["media_url"];
	    }

	    $post->update($data);

	    if ($request->has('attachments')) {
		    foreach ($request->attachments as $attachment) {
			    $file = $attachment["file"];
			    switch ($attachment["type"]) {
				    case Constants::ATTACHMENT_BOOK:
					    $dir = "book";
					    break;
				    case Constants::ATTACHMENT_COVER:
					    $dir = "cover";
					    break;
				    default:
					    $dir = "audio";
			    }
			    $dir = "posts/{$post->id}/{$dir}";
			    $data = [
				    "path" => $file->store($dir),
				    "extension" => $file->extension(),
				    "mime" => $file->getMimeType(),
				    "attachment_type_id" => $attachment["type"]
			    ];
			    $post->attachments()->create($data);
		    }
	    }
	    $post->load('attachments');
//        if ($request->has('attachments')) {
//            foreach ($request->attachments as $attachment) {
//                $file = $attachment["file"];
//                switch ($attachment["type"]) {
//                    case Constants::ATTACHMENT_BOOK:
//                        $dir = "book";
//                        break;
//                    case Constants::ATTACHMENT_COVER:
//                        $dir = "cover";
//                        break;
//                    default:
//                        $dir = "audio";
//                }
//                $dir = "posts/{$post->id}/{$dir}";
//                Container::getInstance()->make(FilesystemFactory::class)->disk("local")->deleteDirectory($dir);
//
//                $data = [
//                    "path" => $file->store($dir),
//                    "extension" => $file->extension(),
//                    "mime" => $file->getMimeType(),
//                    "attachment_type_id" => $attachment["type"]
//                ];
//                $post->attachments()->create($data);
//            }
//        }
//        $post->load('attachments');

        return response()->json(['post' => $post]);
    }

    public function attachment(Request $request,$id)
    {
        $attachment = Attachment::findOrFail($id);
        $factory = Container::getInstance()->make(FilesystemFactory::class)->disk("local");

        if ($factory->exists($attachment->path)) {
            $content = $factory->get($attachment->path);
//            $fullsize = $factory->size($attachment->path);
            $headers = ["content-type" => $attachment->mime];
//            $range = $request->header('Range');
//            $response_code = 200;
//            if(isset($range)) {
//                $eqPos = strpos($range, "=");
//                $toPos = strpos($range, "-");
//                $unit = substr($range, 0, $eqPos);
//                $start = intval(substr($range, $eqPos+1, $toPos));
//                $success = fseek($content, $start);
//                if($success == 0) {
//                    $fullsize -= $start;
//                    $response_code = 206;
//                    $headers["Accept-Ranges"] = $unit;
//                    $headers["Content-Range"] = $unit . " " . $start . "-" . ($fullsize-1) . "/" . $fullsize;
//                }
//            }
//            $headers["Content-Length"] = $fullsize;
//            $response = new StreamedResponse(
//                function () use ($content) {
//                    $out = fopen('php://output', 'w');
//                    fputs($out, $content);
//                    fclose($out);
//                }, $response_code, $headers);
//             $response->send();

            return response($content,200,$headers);
        }



        return response("File Not Found", 404);
    }

    public function rate(Request $request)
    {
        $this->validate($request, [
            "post_id" => "required|exists:posts,id",
            "rating" => "required|in:0,1,2,3,4,5"
        ]);

        $rate = Posts_Rating::firstOrNew([
            "post_id" => $request->post_id,
            "user_id" => $request->user()->id
        ]);
        $rate->rating = $request->rating;

        $rate->save();
        $rating = Posts_Rating::where("post_id", $rate->post_id)->avg('rating');

        $rate->post()->update(["rating" => $rating]);

        return response()->json(['new_rating' => $rating]);
    }
}
