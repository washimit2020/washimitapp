package com.it.washm.alrayaduah.adapter;

/**
 * Created by Mohammed Ali on 10/14/2016.
 */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.poliveira.parallaxrecyclerview.ParallaxRecyclerAdapter;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.it.washm.alrayaduah.NewsDetailsActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.model.news;

public class MainNewsParallaxAdapterHider extends ParallaxRecyclerAdapter<news> {
    private final int VIEW_ITEM = 1;
    private final int VIEW_HIDER = 2;
    private final int VIEW_PROG = 0;
    int i = 0;
    private List<news> newsList;
    private Activity activity;
    private int lastPosition = -1;
    private int recySize;
    public String urls;
    private Intent intent;
    private String[] dateAndTime;
    private SharedPref pref;

    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    Typeface SemiBold, Regular;


    public MainNewsParallaxAdapterHider(Activity activity, List<news> data, RecyclerView recyclerView) {
        super(data);
        this.newsList = data;
        this.activity = activity;
        pref = new SharedPref(activity, "MyPref");
        SemiBold = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-SemiBold.ttf");
        Regular = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-Regular.ttf");

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    if (urls != null) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                            // End has been reached
                            // Do something
                            if (onLoadMoreListener != null && !urls.equalsIgnoreCase("null")) {
                                onLoadMoreListener.onLoadMore();
                            }
                            loading = true;
                        }
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        i = position;
        if (position == 0 && this.hasHeader()) {
            return VIEW_HIDER;
        } else {
            if (this.hasHeader()) {
                i--;
            }
            return newsList.get(i) != null ? VIEW_ITEM : VIEW_PROG;
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolderImpl(ViewGroup parent, ParallaxRecyclerAdapter parallaxRecyclerAdapter, int viewType) {

        RecyclerView.ViewHolder vh;
        View v;
        if (viewType == VIEW_ITEM) {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.news_item, parent, false);

            vh = new NewsItemViewHolder(v);
        } else {
            v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }

        return vh;

    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCountImpl(ParallaxRecyclerAdapter parallaxRecyclerAdapter) {
//        if (this.hasHeader()) {
//            return newsList.size() - 1;
//        }

        return newsList.size();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolderImpl(final RecyclerView.ViewHolder holder, ParallaxRecyclerAdapter parallaxRecyclerAdapter, int position) {

      /*  Animation animation = AnimationUtils.loadAnimation(activity,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);/*/


        if (holder instanceof NewsItemViewHolder) {

            final news singleNew = (news) newsList.get(position);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.forLanguageTag("ar"));


            if (singleNew.getNews_status_id().equalsIgnoreCase("2")) {
                ((NewsItemViewHolder) holder).rejected_info.setVisibility(View.VISIBLE);
            } else {
                ((NewsItemViewHolder) holder).rejected_info.setVisibility(View.GONE);

            }


            ((NewsItemViewHolder) holder).news_item_tital.setText(singleNew.getTitle());

            try {
                ((NewsItemViewHolder) holder).news_item_Likes.setText(AppHelper.getDateDifferenceForDisplay(format.parse(singleNew.getUpdated_at()), activity, singleNew.getUpdated_at()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            ((NewsItemViewHolder) holder).news_item_Views.setText(singleNew.getViews_count());

            Picasso.with(activity).load(Links.UrlImg + getAtachment(singleNew, 0)).fit().centerCrop().placeholder(R.drawable.ic_blank_image).into(((NewsItemViewHolder) holder).news_item_mainImg);


            ((NewsItemViewHolder) holder).new_item.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    if (singleNew.getNews_status_id().equalsIgnoreCase("1")) {
                        if (AppHelper.isOnline(activity)) {
                            dateAndTime = AppHelper.getDateType(singleNew.getUpdated_at(), activity, "yyyy-MM-dd' 'HH:mm:ss").split(" ").clone();
                            intent = new Intent(activity, NewsDetailsActivity.class);
                            intent.putExtra("date", dateAndTime[0]);
                            intent.putExtra("time", dateAndTime[1]);
                            intent.putExtra("newId", singleNew.getId());
                            intent.putExtra("img_new", singleNew.getAttachment().get(0).getPath());
                            intent.putExtra("img_user", getAtachmentUser(singleNew, 0));
                            intent.putExtra("title", singleNew.getTitle());

                            if (singleNew.getUser().getNick_name() != null) {
                                intent.putExtra("name", singleNew.getUser().getNick_name());
                            } else {
                                intent.putExtra("name", singleNew.getUser().getFirst_name() + " " + singleNew.getUser().getLast_name());
                            }
                            intent.putExtra("content", singleNew.getContent());

                            NewsDetailsActivity.attachments = singleNew.getAttachment();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                Pair<View, String> p1 = Pair.create((View) ((NewsItemViewHolder) holder).news_item_mainImg, "moh");
                                //  Pair<View, String> p3 = Pair.create((View)((NewsItemViewHolder) holder).news_item_tital, "text");
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, p1);

//                                activity. startActivity(intent, options.toBundle());
                                activity.startActivity(intent);

                            } else {
                                activity.startActivity(intent);
                            }
                        } else {
                            Snackbar.make(activity.getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                        }
                    }
                }
            });


        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).avi.show();

        }

    }

    public void setLoaded() {
        loading = false;
    }

    public void setUrl(String url) {
        urls = url;
    }

    public String getAtachment(news singleNew, int i) {
        if (singleNew.getAttachment().size() != 0) {
            if (i <= singleNew.getAttachment().size()) {
                return singleNew.getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    public String getAtachmentUser(news singleNew, int i) {
        if (singleNew.getUser().getAttachment().size() != 0) {
            if (i <= singleNew.getUser().getAttachment().size()) {
                return singleNew.getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public class NewsItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.news_item_main_img)
        ImageView news_item_mainImg;
        @BindView(R.id.rejected_info)
        ImageView rejected_info;
        @BindView(R.id.news_item_tital)
        TextView news_item_tital;
        @BindView(R.id.news_item_Views)
        TextView news_item_Views;
        @BindView(R.id.news_item_Likes)
        TextView news_item_Likes;
        @BindView(R.id.new_item)
        CardView new_item;

        public NewsItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            news_item_tital.setTypeface(SemiBold);
            news_item_Views.setTypeface(Regular);
            news_item_Likes.setTypeface(Regular);

        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.avi)
        AVLoadingIndicatorView avi;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

    }


}
