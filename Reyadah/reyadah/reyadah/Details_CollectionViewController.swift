//
//  Details_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 4/20/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class Details_CollectionViewController: ItemsLoader_CollectionViewController {
    
    var reuseIdentifier:String?
    var headerReuseIndentifier:String?
    var headerNib:String?
    var cellNib:String?
    
    fileprivate var is_user_model = true
    fileprivate var has_comments = false
    var model:Any?{
        didSet{
            if  model! is User{
                headerReuseIndentifier = "userHeader"
                headerNib = "UserDetailsHeader_CollectionReusableView"
                reuseIdentifier = "productCell"
                cellNib = "DetailsProducts_CollectionViewCell"
                
            }else{
                reuseIdentifier = "comment"
                headerReuseIndentifier = "productHeader"
                headerNib = "ProductDetailsHeader_CollectionReusableView"
                cellNib = "Comment_CollectionViewCell"
                is_user_model = false
                has_comments = true
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if  let festival = model as? Festival{
            if !festival.from.isEmpty{
                self.reuseIdentifier = "comment"
                self.cellNib = "Comment_CollectionViewCell"
                has_comments = true
            }
        }
        
        self.delegate = self
        self.dataModelChanger = self
        
        self.collectionView?.frame = self.view.frame
        self.collectionView!.register(UINib(nibName: headerNib!, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerReuseIndentifier!)
        
        self.collectionView!.register(UINib(nibName: cellNib!, bundle: nil), forCellWithReuseIdentifier: reuseIdentifier!)
        
        self.emptyLabel?.frame.origin.y = emptyLabel!.frame.height + 334
        
        var name = ""
        
        var image:String? = nil
        
        if is_user_model{
            let user = (model as! User)
            link = RequestLinks.user_details.rawValue + "\(user.id!)"
            beginWithModel = "user"
            clearData()
            name = user.name
            if Constants.icon_name.indices.contains(user.type.id - 1){
                image =  Constants.icon_name[user.type.id - 1]
                
            }else{
                image =  Constants.icon_name[2]
            }
            
        }else{
            let product = (model as! Product)
            name = "\(product.user.name!) \n \(product.name!)"
            
        }
        if has_comments{
            self.collectionView?.viewWithTag(123)?.removeFromSuperview()
            self.items = (model as? Commentable)!.comments!
            
            self.items.append("empty")
            self.collectionView!.register(UINib(nibName: "AddComment_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "addComment")
        }
        
        makeDetailsTitle(self, title: name, image: image)
        
        // Do any additional setup after loading the view.
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:UICollectionViewCell? = collectionView.cellForItem(at: indexPath)
        if cell == nil{
            if has_comments{
                if indexPath.row == items.count - 1{
                    let costume = collectionView.dequeueReusableCell(withReuseIdentifier: "addComment", for: indexPath) as! AddComment_CollectionViewCell
                    cell = costume
                }else{
                    let costume = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier!, for: indexPath) as! Comment_CollectionViewCell
                    costume.layer.borderColor = Constants.themeColor.cgColor
                    costume.layer.borderWidth = 0.3
                    costume.commentModel = (model as! Commentable).comments?[indexPath.row]
                    cell = costume
                }
            }else{
                let costume = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier!, for: indexPath) as! DetailsProducts_CollectionViewCell
                costume.product = self.items[indexPath.row] as? Product
                costume.layer.borderColor = Constants.themeColor.cgColor
                costume.layer.borderWidth = 0.3
                cell =  costume
            }
        }
        
        return cell!
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        case UICollectionElementKindSectionHeader:
            var header:UICollectionReusableView? = collectionView.supplementaryView(forElementKind: kind, at: indexPath)
            if header == nil {
                let heading  = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerReuseIndentifier!, for: indexPath) as! UserDetailsHeader_CollectionReusableView
                heading.model = model
                heading.apiRequest = self.apiRequest
                heading.share.addTarget(nil, action: #selector(share(_:)), for: .touchUpInside)
                heading.location.addTarget(self, action: #selector(go_to_locations), for: .touchUpInside)

                if is_user_model{
                    heading.info.addTarget(nil, action: #selector(goToInfo), for: .touchUpInside)
                    heading.phoneCall.addTarget(self, action: #selector(make_phone_call), for: .touchUpInside)
                }
                header = heading
            }
            
            return header!
        default:
            
            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1  && has_comments{
            let comments = Comment_ViewController(nibName: "Comment_ViewController", bundle: nil)
            comments.model = self.model as! Commentable
            comments.apiRequest = apiRequest
            comments.delegate = self
            
            self.navigationController?.pushViewController(comments, animated: true)
        }
        
        if has_comments{
            return
        }
        let info = Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        info.model = (collectionView.cellForItem(at: indexPath) as! DetailsProducts_CollectionViewCell).product
        
        navigationController?.pushViewController(info, animated: true)
    }
    
    override func make_api_call(_ nextlink: String?) {
        if has_comments {
            return
        }
        super.make_api_call(nextlink)
    }
    
    func goToInfo(){
        let info = Info_TableViewController(nibName: "Info_TableViewController", bundle: nil)
        let user = self.model as! User
        info.user = user
        info.apiRequest = apiRequest
        navigationController?.pushViewController(info, animated: true)
    }
    
    func make_phone_call(){
        guard let number = URL(string: "telprompt://" + (model as? User)!.phone_number) else { return }
        UIApplication.shared.open(number)
    }
    
    func share(_ sender:AspectFitButton){
        let heading  = collectionView?.supplementaryView(forElementKind: UICollectionElementKindSectionHeader, at: IndexPath(row: 0, section: 0)) as! UserDetailsHeader_CollectionReusableView
        let img = heading.slider.currentSlideshowItem?.imageView.image
        var name = ""
        if is_user_model{
            name = (model as! User).name
        }else{
            name = (model as! Product).name
            
        }
        let activityViewController = shareProductToMedia(image: img!, name: name)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func go_to_locations(){
      perform_locations_segue(ProductOrUser: model!, ViewController: self, apiRequest)
    }
}

extension Details_CollectionViewController{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if has_comments{
            return CGSize(width: collectionView.frame.width , height: 100)
        }
        return CGSize(width: collectionView.frame.width/3 , height: 245)
    }
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        return CGSize(width: collectionView.frame.width, height: 355)
    }
    
    
}

extension Details_CollectionViewController:ItemsLoaderDelegate,productRefresher,DataModelChanger{
    
    func dataFiller(data: [JSON]) {
        if (!has_comments){
            for productData in data{
                self.items.append(Product(productData))
            }
        }
        
    }
    
    func refreshProduct(_ commentable: Commentable,_ updated:Bool ) {
        
        if !updated{
            return
        }
        
        self.items = commentable.comments!
        
        self.items.append("empty")
        
        self.collectionView?.reloadData()
    }
    
    func change(_ json: JSON) {
        //implemnt
        if !has_comments{
            let user = Branch(json,beginWith: "user")
            dataModel = user.dataModel
            model = user
        }
    }
    
    
    
}

