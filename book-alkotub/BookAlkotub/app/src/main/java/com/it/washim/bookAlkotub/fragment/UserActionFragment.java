package com.it.washim.bookAlkotub.fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washim.bookAlkotub.activity.MainActivity;
import com.it.washim.bookAlkotub.R;
import com.it.washim.bookAlkotub.adapter.UserActionAdapter;
import com.it.washim.bookAlkotub.helper.DataBHelper;
import com.it.washim.bookAlkotub.interfac.deletnotecallback;
import com.it.washim.bookAlkotub.interfac.pageIdCallback;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserActionFragment extends Fragment {
    RecyclerView recyclerView;
    DataBHelper bHelper;
    UserActionAdapter adapter;
    int type;
    List<?> list;
    ImageView page_img_icon;
    TextView page_titel;
    Typeface tf;

    public UserActionFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static UserActionFragment newInstance(int type) {
        UserActionFragment fragment = new UserActionFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_action, container, false);
        bHelper = new DataBHelper(getActivity());
        tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/TheSansArab-Bold_0.ttf");
        recyclerView = (RecyclerView) view.findViewById(R.id.userAction);
        page_img_icon = (ImageView) view.findViewById(R.id.page_img_icon);
        page_titel = (TextView) view.findViewById(R.id.page_titel);

        page_titel.setTypeface(tf, 1);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        if (type == 1) {
            list = bHelper.getListWishilsted();
         //   page_img_icon.setBackgroundResource(R.drawable.ic_icon_sidebar_favorite);
            page_titel.setText("مفضلتي");

        } else if (type == 2) {
            list = bHelper.getListComment();
          //  page_img_icon.setBackgroundResource(R.drawable.ic_icon_note);
            page_titel.setText("ملاحظاتي");

        } else if (type == 3) {
            page_titel.setText("فهرست الكتاب");
            list = bHelper.getListSection();

        }


        adapter = new UserActionAdapter(getActivity(), list);


        adapter.onpageIdCollback(new pageIdCallback() {
            @Override
            public void pageId(int pageId) {

                if (type != 3) {
                    ((MainActivity) getActivity()).openFragment(new ViewPagesFragment(pageId));

                } else {
                    ((MainActivity) getActivity()).openFragment(new ViewPagesFragment(bHelper.getSectionId(pageId)));

                }

            }
        });
        adapter.onpageNotDeletCollback(new deletnotecallback() {
            @Override
            public void deletNote(int pageId) {
                bHelper.insertComment(pageId, null);
            }
        });


        recyclerView.setAdapter(adapter);


        return view;
    }

}
