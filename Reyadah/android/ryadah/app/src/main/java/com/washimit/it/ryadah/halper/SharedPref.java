package com.washimit.it.ryadah.halper;

import android.content.Context;
import android.content.SharedPreferences;

import com.washimit.it.ryadah.model.user.User;
import com.google.gson.Gson;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by mohammad on 4/18/2017.
 */

public class SharedPref {


    SharedPreferences mPrefs;
    SharedPreferences.Editor editor;
    Gson gson;


    public SharedPref(Context context) {
        mPrefs = context.getSharedPreferences("MyPref", MODE_PRIVATE);
        editor = mPrefs.edit();
        gson = new Gson();
    }


    public void setUser(String user) {

        editor.putString("user", user);
        editor.commit();


    }

    public User getUser() {

        return gson.fromJson(mPrefs.getString("user", ""), User.class);


    }


    public void setLocal(String local) {

        editor.putString("local", local);
        editor.commit();


    }


    public String getLocal() {

        return mPrefs.getString("local", null);


    }


    public boolean getNotifacationStatus() {

        return mPrefs.getBoolean("facationStatus", true);


    }

    public void setNotifacationStatus(boolean local) {

        editor.putBoolean("facationStatus", local);
        editor.commit();


    }


    public String getLogInTocen() {
        return mPrefs.getString("tocen", "");

    }

    public void setLogInTocen(String tocen) {
        editor.putString("tocen", tocen);
        editor.commit();
    }

    public void SetfirstTimeHome() {

        editor.putBoolean("home", false);
        editor.commit();


    }

    public boolean getfirstTimeHome() {

        return mPrefs.getBoolean("home", true);


    }

    public void SetfirstTimeStore() {

        editor.putBoolean("store", false);
        editor.commit();


    }

    public boolean getfirstTimeStore() {

        return mPrefs.getBoolean("store", true);


    }

    public void SetfirstTimeShow() {

        editor.putBoolean("show", false);
        editor.commit();


    }

    public boolean getfirstTimeShow() {

        return mPrefs.getBoolean("show", true);


    }

    public void SetfirstTimeFestivel() {

        editor.putBoolean("festival", false);
        editor.commit();


    }

    public boolean getfirstTimeFestivel() {

        return mPrefs.getBoolean("festival", true);


    }

    public void clearUser() {
        mPrefs.edit().clear().commit();
    }


}
