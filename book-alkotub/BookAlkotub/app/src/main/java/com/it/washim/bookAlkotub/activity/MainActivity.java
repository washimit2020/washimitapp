package com.it.washim.bookAlkotub.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.it.washim.bookAlkotub.R;
import com.it.washim.bookAlkotub.coustumView.EditTextCursorWatcher;
import com.it.washim.bookAlkotub.coustumView.TextViewCustem;
import com.it.washim.bookAlkotub.fragment.ApoutFragment;
import com.it.washim.bookAlkotub.fragment.CvFragment;
import com.it.washim.bookAlkotub.fragment.SearchFragment;
import com.it.washim.bookAlkotub.fragment.StatisticsFragment;
import com.it.washim.bookAlkotub.fragment.UserActionFragment;
import com.it.washim.bookAlkotub.fragment.ViewPagesFragment;
import com.it.washim.bookAlkotub.helper.DataBHelper;
import com.it.washim.bookAlkotub.helper.PreferenceClass;
import com.it.washim.bookAlkotub.interfac.goToPagecallback;
import com.it.washim.bookAlkotub.interfac.wishilistedCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView add_to_wishiList, search, slide_par, sittings, book, communiction;
    LinearLayout wishilisted_page, comment_page, hom_page, book_page, cv_page, add_frend_page, apout_page, statistics;
    Toolbar toolbar;
    DataBHelper mDBHelper;
    public static int PAGE_ID;
    android.support.v4.app.Fragment fragment;
    PreferenceClass preferenceClass;
    DrawerLayout drawer;
    NavigationView navigationView;
    goToPagecallback pagecallback;
    boolean screnMode = true;
    Bundle bundle;
    ViewPagesFragment pagesFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_main);
        bundle = getIntent().getExtras();
        preferenceClass = new PreferenceClass(this);
        mDBHelper = new DataBHelper(this);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        slide_par = (ImageView) findViewById(R.id.slide_par);
        book = (ImageView) findViewById(R.id.book);
        communiction = (ImageView) findViewById(R.id.communiction);
        add_to_wishiList = (ImageView) findViewById(R.id.add_to_wishiList);
        search = (ImageView) findViewById(R.id.search);
        sittings = (ImageView) findViewById(R.id.sittings);
        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        wishilisted_page = (LinearLayout) findViewById(R.id.wishilisted_page);
        apout_page = (LinearLayout) findViewById(R.id.apout_page);
        add_frend_page = (LinearLayout) findViewById(R.id.add_frend_page);
        comment_page = (LinearLayout) findViewById(R.id.comment_page);
        hom_page = (LinearLayout) findViewById(R.id.hom_page);
        statistics = (LinearLayout) findViewById(R.id.statistics);
        book_page = (LinearLayout) findViewById(R.id.book_page);
        cv_page = (LinearLayout) findViewById(R.id.cv_page);

        //Check exists database
        File database = this.getDatabasePath(DataBHelper.DBNAME);
        if (false == database.exists()) {
            mDBHelper.getReadableDatabase();
            //Copy db
            if (copyDatabase(this)) {
                Log.d("safsafasf", "Copy database succes");
            } else {
                Log.d("afsasffas", "Copy data error");
            }
        }

        if (preferenceClass.getPageId() != 0) {
            openFragment(new ViewPagesFragment(preferenceClass.getPageId()));
        } else {
            openFragment(new ViewPagesFragment());
            // openFragment(new UserActionFragment());
        }
        if (bundle.getInt("type") == 1) {
            openFragment(new CvFragment());
        } else if (bundle.getInt("type") == 3) {
            openFragment(UserActionFragment.newInstance(3));
        }
        slide_par.setOnClickListener(this);
        add_to_wishiList.setOnClickListener(this);
        search.setOnClickListener(this);
        wishilisted_page.setOnClickListener(this);
        comment_page.setOnClickListener(this);
        sittings.setOnClickListener(this);
        hom_page.setOnClickListener(this);
        book.setOnClickListener(this);
        communiction.setOnClickListener(this);
        book_page.setOnClickListener(this);
        statistics.setOnClickListener(this);
        cv_page.setOnClickListener(this);
        apout_page.setOnClickListener(this);
        add_frend_page.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (fragment instanceof ViewPagesFragment && drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);

        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else if (fragment instanceof ViewPagesFragment) {
            super.onBackPressed();
        } else if (fragment instanceof UserActionFragment && bundle.getInt("type") == 3) {
            finish();

        } else if (fragment instanceof CvFragment && bundle.getInt("type") == 1) {
            finish();
        } else {
            openFragment(new ViewPagesFragment(PAGE_ID));
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.slide_par: {
                drawer.openDrawer(GravityCompat.END);
                break;
            }
            case R.id.add_to_wishiList: {
                mDBHelper.insertWishilsted(PAGE_ID);
                isWishilisted(PAGE_ID);
                break;
            }
            case R.id.wishilisted_page: {
                openFragment(UserActionFragment.newInstance(1));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.hom_page: {
                drawer.closeDrawer(GravityCompat.END);
                finish();
                break;
            }
            case R.id.comment_page: {
                openFragment(UserActionFragment.newInstance(2));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.book: {
                pageNumberDilog();
                break;
            }
            case R.id.communiction: {
                drawer.closeDrawer(GravityCompat.END);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "قم بتحميل تطبيق  كتاب الحج من على المتجر");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.it.washim.bookAlkotub");
                startActivity(Intent.createChooser(sharingIntent, "شارك التطبيق"));
                break;
            }
            case R.id.book_page: {
                openFragment(UserActionFragment.newInstance(3));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.cv_page: {
                openFragment(new CvFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.apout_page: {
                openFragment(new ApoutFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.statistics: {
                openFragment(new StatisticsFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.add_frend_page: {
                drawer.closeDrawer(GravityCompat.END);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "قم بتحميل تطبيق كتاب الحج من على المتجر");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.it.washim.bookAlkotub");
                startActivity(Intent.createChooser(sharingIntent, "أخبر صديق"));
                break;
            }
            case R.id.search: {
                openFragment(new SearchFragment());
                break;
            }
            case R.id.sittings: {
                sittings.setClickable(false);
                settingsDialog();
                break;
            }

        }
    }

    public void openFragment(android.support.v4.app.Fragment fragment) {
        this.fragment = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
        if (fragment instanceof ViewPagesFragment) {
            pagesFragment = (ViewPagesFragment) fragment;
            ((ViewPagesFragment) fragment).newwishilistedCollback(new wishilistedCallback() {
                @Override
                public void wishilistedCollback(int x) {

                    PAGE_ID = x;
                    preferenceClass.setPageId(x);
                    isWishilisted(PAGE_ID);
                }
            });
            toolbar.setVisibility(View.VISIBLE);
            add_to_wishiList.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.GONE);
            add_to_wishiList.setVisibility(View.GONE);
        }

    }

    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DataBHelper.DBNAME);
            String outFileName = DataBHelper.DBLOCATION + DataBHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[] buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity", "DB copied");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void isWishilisted(int i) {
        if (mDBHelper.isWishilisted(i) == 0) {
            //  add_to_wishiList.getResources().getDrawable(R.drawable.);
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_sidebar_favorite);
        } else {
            //  add_to_wishiList.getResources().getDrawable(R.drawable.ic_icon_book);
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_is_favorite);

        }


    }

    private void settingsDialog() {
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dilog_setings);

        Button day_mode = (Button) dialog.findViewById(R.id.day_mode);
        final TextViewCustem proogresbar = (TextViewCustem) dialog.findViewById(R.id.proogresbar);
        Button night_mode = (Button) dialog.findViewById(R.id.night_mode);
        final SeekBar text_size = (SeekBar) dialog.findViewById(R.id.text_size);
        final Spinner text_font_type = (Spinner) dialog.findViewById(R.id.text_font_type);


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("TheSansArab-Light_0.ttf");
        arrayList.add("TheSansArab-Black_0.ttf");
        arrayList.add("TheSansArab-Plain_0.ttf");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, arrayList);

        text_font_type.setAdapter(arrayAdapter);


        text_font_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pagesFragment.update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        text_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                proogresbar.setText(progress + "%");
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pagesFragment.update();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pagesFragment.update();
            }
        });


        day_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = true;

                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);

                pagesFragment.update();

            }
        });
        night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = false;
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pagesFragment.update();
            }
        });


        screnMode = preferenceClass.getscrenMode();
        text_size.setProgress(preferenceClass.gettextSize());
        sittings.setClickable(true);

        dialog.show();


    }

    public void OnGoToPage(goToPagecallback pagecallback) {
        this.pagecallback = pagecallback;
    }

    public void pageNumberDilog() {
        final Button go_to_page, cancel;
        final EditTextCursorWatcher page_number;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilog_go_to_page);
        go_to_page = (Button) dialog.findViewById(R.id.go_to_page);
        cancel = (Button) dialog.findViewById(R.id.cancel);
        page_number = (EditTextCursorWatcher) dialog.findViewById(R.id.page_number);


        go_to_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page_number.getText().toString().equalsIgnoreCase("") || page_number.getText().toString().length() > 6) {
                    InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                    dialog.cancel();
                } else {
                    pagecallback.goToPage(Integer.parseInt(page_number.getText().toString()));
                    pagesFragment.update();
                    InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                    dialog.cancel();
                }
            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                dialog.cancel();
            }
        });


        dialog.show();
    }

}
