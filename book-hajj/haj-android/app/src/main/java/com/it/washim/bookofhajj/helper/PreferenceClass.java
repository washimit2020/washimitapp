package com.it.washim.bookofhajj.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by washm on 4/9/17.
 */

public class PreferenceClass {
    public static final String PREFERENCE_NAME = "MyPref";
    private final SharedPreferences sharedpreferences;

    public PreferenceClass(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
    }

    public int getPageId() {
        int count = sharedpreferences.getInt("pagId", 0);
        return count;
    }

    public void setPageId(int pageId) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("pagId", pageId);
        editor.commit();
    }

    public void clearPageId() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("pagId");
        editor.commit();
    }


    public String getfontType() {
        String fontType = sharedpreferences.getString("fontType", "TheSansArab-Light_0.ttf");
        return fontType;
    }

    public void setfontType(String fontType) {
//        SharedPreferences.Editor editor = sharedpreferences.edit();
//        editor.putString("fontType", fontType);
//        editor.commit();
    }

    public void clearfontType() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("fontType");
        editor.commit();
    }

    public int gettextSize() {
        int textSize = sharedpreferences.getInt("textSize", 15);
        return textSize;
    }

    public void settextSize(int textSize) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("textSize", textSize);
        editor.commit();
    }

    public void cleartextSize() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("textSize");
        editor.commit();
    }

    public boolean getscrenMode() {
        boolean screnMode = sharedpreferences.getBoolean("screnMode", true);
        return screnMode;
    }

    public void setscrenMode(boolean screnMode) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("screnMode", screnMode);
        editor.commit();
    }

    public void clearscrenMode() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.remove("screnMode");
        editor.commit();
    }



    //A7sayat al app

    public String getLogInTime() {
        return sharedpreferences.getString("logInTime", "");
    }

    public void setlogInTime(String date) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("logInTime", date);
        editor.commit();
    }

    public String getLogOutTime() {
        return sharedpreferences.getString("logOutTime", "اليوم هي اول زيارة");
    }

    public void setlogOutTime(String date) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("logOutTime", date);
        editor.commit();
    }


    public String getAvgTime() {
        return sharedpreferences.getString("avgTime", "");
    }

    public void setAvgTime(String date) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("avgTime", date);
        editor.commit();
    }
}