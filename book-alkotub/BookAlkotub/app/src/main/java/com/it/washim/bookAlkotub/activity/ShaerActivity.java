package com.it.washim.bookAlkotub.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.it.washim.bookAlkotub.R;
import com.it.washim.bookAlkotub.coustumView.ButtonCustem;

import java.io.File;
import java.io.FileOutputStream;

public class ShaerActivity extends AppCompatActivity implements View.OnClickListener {
    String img, text;
    ButtonCustem cancel, sher;
    ImageView imageView;
    Resources r;
    int resID;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                imageView.setDrawingCacheEnabled(true);
                Bitmap bitmap = imageView.getDrawingCache();
                File root = Environment.getExternalStorageDirectory();
                File cachePath = new File(root.getAbsolutePath() + "/DCIM/Camera/image.jpg");
                try {
                    cachePath.createNewFile();
                    FileOutputStream ostream = new FileOutputStream(cachePath);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                    ostream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("image/*");
                share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(cachePath));
                startActivity(Intent.createChooser(share, "مشاركة الصورة"));
            } else {

                Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shaer);

        imageView = (ImageView) findViewById(R.id.img);
        cancel = (ButtonCustem) findViewById(R.id.cancel);
        sher = (ButtonCustem) findViewById(R.id.sher);

        Bundle bundle = getIntent().getExtras();
        img = bundle.getString("img");
        text = bundle.getString("text");

        r = getResources();

        resID = r.getIdentifier(img, "mipmap", "com.it.washim.bookAlkotub");


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageDrawable(r.getDrawable(resID, null));
        }


        cancel.setOnClickListener(this);
        sher.setOnClickListener(this);



    }


    private Drawable createMarkerIcon(Drawable backgroundImage, Drawable layer, int TextCloer, String text,
                                      int width, int height) {
        Resources r = getResources();

        Bitmap canvasBitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.ARGB_8888);
        // Create a canvas, that will draw on to canvasBitmap.
        Canvas imageCanvas = new Canvas(canvasBitmap);

        TextPaint paint = new TextPaint(Paint.FILTER_BITMAP_FLAG);
        paint.setAntiAlias(true);

        paint.setColor(r.getColor(TextCloer));
        // text size in pixels
//        paint.setTextSize(12);
        paint.setTypeface(Typeface.createFromAsset(getApplication().getAssets(), "fonts/TheSansArab-Bold_0.ttf"));

//        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        // text shadow
        // Draw the image to our canvas
        // layer.draw(imageCanvas);

        int textWidth = imageCanvas.getWidth() - (16);

        StaticLayout textLayout = new StaticLayout(text,
                paint, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, true);
        int textHeight = textLayout.getHeight();

        float x = (imageCanvas.getWidth() - textWidth) / 2;
        float y = (imageCanvas.getHeight() - textHeight) / 2;

        imageCanvas.save();
        imageCanvas.translate(x, y);
        textLayout.draw(imageCanvas);
        // Combine background and text to a LayerDrawable
        LayerDrawable layerDrawable = new LayerDrawable(
                new Drawable[]{backgroundImage, layer, new BitmapDrawable(getApplication().getResources(), canvasBitmap)});
        return layerDrawable;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.sher:
                if (requestPermission(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1)) {
                    imageView.setDrawingCacheEnabled(true);
                    Bitmap bitmap = imageView.getDrawingCache();
                    File root = Environment.getExternalStorageDirectory();
                    File cachePath = new File(root.getAbsolutePath() + "/DCIM/Camera/image.jpg");
                    try {
                        cachePath.createNewFile();
                        FileOutputStream ostream = new FileOutputStream(cachePath);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
                        ostream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    Intent share = new Intent(Intent.ACTION_SEND);
                    share.setType("image/*");
                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(cachePath));
                    startActivity(Intent.createChooser(share, "مشاركة الصورة"));
                }

                break;
            case R.id.cancel:
                finish();
                break;
            case R.id.black:

                //    imageView.setImageDrawable(createMarkerIcon(r.getDrawable(resID, null), r.getDrawable(R.color.layar_black), R.color.white, text, 200, 200));

                break;
            case R.id.white:
                //   imageView.setImageDrawable(createMarkerIcon(r.getDrawable(resID, null), r.getDrawable(R.color.layar_white), R.color.black, text, 200, 200));

                break;
            case R.id.blue:
                //    imageView.setImageDrawable(createMarkerIcon(r.getDrawable(resID, null), r.getDrawable(R.color.layar_blue), R.color.white, text, 200, 200));

                break;

        }
    }


    public boolean requestPermission(Activity activity, String[] permission, int requestCode) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                Toast.makeText(activity, "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(activity, permission, requestCode);

            } else {
                ActivityCompat.requestPermissions(activity, permission
                        , requestCode);
            }


            return false;

        } else {
            return true;
        }
    }


}
