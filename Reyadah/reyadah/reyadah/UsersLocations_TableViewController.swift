//
//  UsersLocations_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 5/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class UsersLocations_TableViewController: UITableViewController{
    var items =  [Branch]()
    
    let id = "cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath) as! UsersLoaction_TableViewCell
        let item = items[indexPath.row]
        
        cell.branch = item
        
        return cell
    }
  
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var item  = items[indexPath.row]
        let details =  Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        if item.user_id != 0{
            item = items[0]
        }
        
        details.model = item
        self.navigationController?.pushViewController(details, animated: true)
        
    }
  
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

  
}

