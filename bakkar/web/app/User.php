<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',"api_token","password","reg_id"
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',"api_token"
    ];


    public function comment()
    {
        return $this->hasMany(Users_Comment::class,'user_id');
    }

    public function wishlist()
    {
        return $this->belongsToMany(Post::class,'users__wishlists');
    }

    public function notifications(){
        return $this->hasMany(Notification::class);
    }

    public function unread_notifications(){
        return $this->notifications()->whereNull('read_at');
    }

}
