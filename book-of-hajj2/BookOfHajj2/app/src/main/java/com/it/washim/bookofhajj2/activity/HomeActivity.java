package com.it.washim.bookofhajj2.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.it.washim.bookofhajj2.R;
import com.it.washim.bookofhajj2.coustumView.TextViewCustem;
import com.it.washim.bookofhajj2.helper.PreferenceClass;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView mo2lef;
    ImageView reed_book;
    TextViewCustem section_of_book;
    PreferenceClass pref;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        pref = new PreferenceClass(this);
        mo2lef = (ImageView) findViewById(R.id.mo2alef);
        reed_book = (ImageView) findViewById(R.id.reed_book);
        section_of_book = (TextViewCustem) findViewById(R.id.section_of_book);
        mo2lef.setOnClickListener(this);
        reed_book.setOnClickListener(this);
        section_of_book.setOnClickListener(this);
        date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");
        Log.d("logIn", dateFormat.format(date) + "");
        pref.setlogInTime(dateFormat.format(date) + "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mo2alef:
                intent(MainActivity.class, 1);
                break;
            case R.id.reed_book:
                intent(MainActivity.class, 2);
                break;
            case R.id.section_of_book:
                intent(MainActivity.class, 3);
                //  mainActivity.openFragment(UserActionFragment.newInstance(3));
                break;
        }
    }


    public void intent(Class activity, int type) {
        Intent intent = new Intent(this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("type", type);
        startActivity(intent);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");
        Log.d("logOut", dateFormat.format(date) + "");
        pref.setlogOutTime(dateFormat.format(date) + "");
    }

    @Override
    protected void onStop() {
        super.onStop();
        date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");
        Log.d("logOut", dateFormat.format(date) + "");
        pref.setlogOutTime(dateFormat.format(date) + "");

    }


}
