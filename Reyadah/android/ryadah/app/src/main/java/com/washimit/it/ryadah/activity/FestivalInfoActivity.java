package com.washimit.it.ryadah.activity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FestivalInfoActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.activity_festival_info_festival_info)
    TextViewCast activity_festival_info_festival_info;

    @BindView(R.id.activity_festival_info_name)
    TextViewCast activity_festival_info_name;

    @BindView(R.id.activity_festival_info_follow_number)
    TextViewCast activity_festival_info_follow_number;


    @BindView(R.id.activity_festival_info_img_type)
    ImageView activity_festival_info_img_type;

    @BindView(R.id.activity_festival_info_img)
    ImageView activity_festival_info_img;

    @BindView(R.id.activity_festival_info_back)
    ImageView activity_festival_info_back;

    @BindView(R.id.activity_festival_info_whatsapp)
    ImageView activity_festival_info_whatsapp;

    @BindView(R.id.activity_festival_info_twitter)
    ImageView activity_festival_info_twitter;

    @BindView(R.id.activity_festival_info_snapchat)
    ImageView activity_festival_info_snapchat;

    @BindView(R.id.activity_festival_info_instgram)
    ImageView activity_festival_info_instgram;

    @BindView(R.id.activity_festival_info_facebook)
    ImageView activity_festival_info_facebook;


    Festivals festivals;
    Bundle bundle;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_festival_info);
        ButterKnife.bind(this);
        gson = new Gson();
        bundle = getIntent().getExtras();
        festivals = gson.fromJson(bundle.getString("festivals"), Festivals.class);
        Picasso.with(this).load(Constant._AVATAR_URL + festivals.getUser().getId()).placeholder(R.drawable.ic_profile_picture).fit().centerCrop().into(activity_festival_info_img);
        activity_festival_info_festival_info.setText(festivals.getInfo());
        activity_festival_info_name.setText(festivals.getName());
        activity_festival_info_follow_number.setText(festivals.getTotal_followers() + " " + "متابع");
        activity_festival_info_img_type.setImageDrawable(getResources().getDrawable(R.drawable.ic_festival_20));

        activity_festival_info_back.setOnClickListener(this);

        activity_festival_info_whatsapp.setOnClickListener(this);
        activity_festival_info_twitter.setOnClickListener(this);
        activity_festival_info_snapchat.setOnClickListener(this);
        activity_festival_info_instgram.setOnClickListener(this);
        activity_festival_info_facebook.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activity_festival_info_whatsapp:


                showDilog(festivals.getSocial_media().getWhatsapp());

                break;
            case R.id.activity_festival_info_twitter:

                showDilog(festivals.getSocial_media().getTwitter());

                break;
            case R.id.activity_festival_info_snapchat:

                showDilog(festivals.getSocial_media().getSnapchat());

                break;
            case R.id.activity_festival_info_instgram:

                showDilog(festivals.getSocial_media().getInstagram());

                break;
            case R.id.activity_festival_info_facebook:

                showDilog(festivals.getSocial_media().getFacebook());

                break;
            case R.id.activity_festival_info_back:

                this.finish();

                break;


        }


    }


    public void showDilog(final String msg) {


        AapHalper.CreateDialog(R.layout.dialog_social_media_info, this, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {

                TextViewCast text = (TextViewCast) dialog.findViewById(R.id.dialog_social_media_info_text);
                ButtonView ok = (ButtonView) dialog.findViewById(R.id.dialog_social_media_info_ok);
                if (msg != null && !msg.equalsIgnoreCase("")) {
                    text.setText(msg);

                }

                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });


            }
        });


    }


}
