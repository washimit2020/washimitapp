package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Interfases.OnSalePointsCallBck;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/18/2017.
 */

public class SalePointListAdapter extends RecyclerView.Adapter {

    ArrayList<Post> list;
    Context context;
    OnSalePointsCallBck callBck;

    public SalePointListAdapter(ArrayList<Post> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SalePointViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sale_point, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SalePointViewHolder) {

            final Post post = list.get(position);
            ((SalePointViewHolder) holder).pointName.setText(post.title);
            ((SalePointViewHolder) holder).pointSector.setText(post.post_meta.location.sector);
            ((SalePointViewHolder) holder).pointCountry.setText(post.post_meta.location.address);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBck.onClick(post);
                }
            });

        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class SalePointViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.point_country)
        MyTextView pointCountry;
        @BindView(R.id.point_name)
        MyTextView pointName;
        @BindView(R.id.point_sector)
        MyTextView pointSector;

        public SalePointViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


    public void OnSalePointsCallBck(OnSalePointsCallBck callBck) {
        this.callBck = callBck;
    }
}
