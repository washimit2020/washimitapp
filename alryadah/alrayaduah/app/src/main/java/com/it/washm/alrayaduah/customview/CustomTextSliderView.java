package com.it.washm.alrayaduah.customview;

/**
 * Created by washm on 2/1/17.
 */

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import com.it.washm.alrayaduah.R;

/**
 * Created by S.M_Emamian on 7/12/2015.
 */
public class CustomTextSliderView  extends BaseSliderView {
    private static Typeface font = null;
    private Context context ;
    public CustomTextSliderView(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.render_type_text,null);
        ImageView target = (ImageView)v.findViewById(R.id.daimajia_slider_image);
        TextView description = (TextView)v.findViewById(R.id.description);
        description.setText(getDescription());
        if(font == null){
            font = Typeface.createFromAsset(context.getAssets(), "Cairo-SemiBold.ttf");
        }
        description.setTypeface(font);
        bindEventAndShow(v, target);
        return v;
    }
}
