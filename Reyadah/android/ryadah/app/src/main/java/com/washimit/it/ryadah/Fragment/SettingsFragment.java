package com.washimit.it.ryadah.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Locale;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.viewCasting.TextViewCast;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {
    @BindView(R.id.notifecation_text_status)
    TextViewCast status;
    @BindView(R.id.notifecation_but)
    ImageView but;
    @BindView(R.id.lang)
    LinearLayout lang;

    @BindView(R.id.text_lang)
    TextViewCast text_lang;

    @BindView(R.id.notifecation)
    LinearLayout notifecation;

    public SettingsFragment() {
        // Required empty public constructor
    }

    SharedPref pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);
        pref = new SharedPref(getActivity());

        if (pref.getNotifacationStatus()) {
            status.setText(R.string.effective);
            but.setImageDrawable(getResources().getDrawable(R.drawable.ic_selecte));
        } else {
            but.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselected));
            status.setText(R.string.un_effective);

        }

        notifecation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pref.getNotifacationStatus()) {
                    status.setText(R.string.effective);
                    but.setImageDrawable(getResources().getDrawable(R.drawable.ic_selecte));
                    pref.setNotifacationStatus(true);
                } else {
                    but.setImageDrawable(getResources().getDrawable(R.drawable.ic_unselected));
                    pref.setNotifacationStatus(false);
                    status.setText(R.string.un_effective);

                }
            }
        });


//        if (pref.getLocal() != null) {
//            if (pref.getLocal().equalsIgnoreCase("ar")) {
//                text_lang.setText(R.string.language_type);
//            }
//            if (pref.getLocal().equalsIgnoreCase("en")) {
//                text_lang.setText(R.string.language_type);
//            }
//        }


        lang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AapHalper.CreateDialog(R.layout.dialog_lang, getActivity(), new DaelogCallback() {
                    @Override
                    public void dialog(final Dialog dialog) {
                        LinearLayout ar, en;
                        ar = (LinearLayout) dialog.findViewById(R.id.dialog_lang_ar);
                        en = (LinearLayout) dialog.findViewById(R.id.dialog_lang_en);


                        ar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setLocale("ar");
                                dialog.dismiss();
                            }
                        });
                        en.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                setLocale("en");
                                dialog.dismiss();

                            }
                        });
                    }
                });


            }
        });


        return view;

    }


    public void setLocale(String lang) {
        pref.setLocal(lang);
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(getActivity(), MainActivity.class);
        startActivity(refresh);
        getActivity().finish();
    }
}

