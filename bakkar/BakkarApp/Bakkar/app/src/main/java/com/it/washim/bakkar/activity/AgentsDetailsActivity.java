package com.it.washim.bakkar.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.AgentsAdapter;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.model.Articles;
import com.it.washim.bakkar.model.Posts;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AgentsDetailsActivity extends BaseActivity {
    @BindView(R.id.agents_list)
    RecyclerView agents_list;
    @BindView(R.id.title)
    MyTextView title;
    ArrayList<Posts> list;
    AgentsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agents_details);
        ButterKnife.bind(this);
        agents_list.setHasFixedSize(true);

        agents_list.setLayoutManager(new LinearLayoutManager(this));

        title.setText(getIntent().getExtras().getString("title"));
        list = gson().fromJson(getIntent().getExtras().getString("list"), new TypeToken<ArrayList<Posts>>() {
        }.getType());
        adapter = new AgentsAdapter(list, this);
        agents_list.setAdapter(adapter);


    }
}
