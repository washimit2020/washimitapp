<?php

namespace App\Policies;

use App\Http\Constants;
use App\User;
use App\ManyToPlacesModels\ShowBranch;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can update the showBranch.
     *
     * @param  \App\User $user
     * @param  \App\ManyToPlacesModels\ShowBranch $showBranch
     * @return mixed
     */
    public function update(User $user, ShowBranch $showBranch)
    {
        if ($showBranch->status_id == Constants::_STATUS_DELETED){
            return false;
        }
        return $showBranch->user->id == $user->id;
    }

    /**
     * Determine whether the user can view the branch
     *
     * @param User $user
     * @return bool
     */
    public function view(User $user)
    {
        return $user->type_id == Constants::_TYPE_SHOW;
    }


}
