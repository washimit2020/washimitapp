package com.washimit.it.ryadah.Fragment.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.registrationActivity.ImageAdd;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import java.io.File;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProduactFragment extends Fragment implements View.OnClickListener {
    EditTextView product_name, product_des, product_price;
    TextViewCast product_add_img;
    Spinner product_number;
    ImageView add_product;
    ArrayList<File> files;
    ArrayList<Part> parts;
    ApiRequest requiresApi;

    public AddProduactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant._TYPE_PICTURE) {
            files.clear();
            if (data != null) {

                if (data.getExtras().get("caver1") != null) {
                    files.add(new File(data.getExtras().getString("caver1")));
                    Log.d("caver1P", data.getExtras().get("caver1") + "");

                }
                if (data.getExtras().get("caver2") != null) {
                    files.add(new File(data.getExtras().getString("caver2")));
                    Log.d("caver2P", data.getExtras().get("caver2") + "");

                }
                if (data.getExtras().get("caver3") != null) {
                    files.add(new File(data.getExtras().getString("caver3")));
                    Log.d("caver3P", data.getExtras().get("caver3") + "");


                }


            }


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_produact, container, false);


        requiresApi = new ApiRequest(getActivity());
        product_name = (EditTextView) view.findViewById(R.id.product_name);
        product_des = (EditTextView) view.findViewById(R.id.product_des);
        product_price = (EditTextView) view.findViewById(R.id.product_price);
        product_add_img = (TextViewCast) view.findViewById(R.id.product_add_img);
        product_number = (Spinner) view.findViewById(R.id.product_number);
        add_product = (ImageView) view.findViewById(R.id.add_product);

        files = new ArrayList<>();
        parts = new ArrayList<>();

        add_product.setOnClickListener(this);
        product_add_img.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.add_product:


                if (validation()) {

                    parts.add(new StringPart("name", product_name.getText().toString()));
                    parts.add(new StringPart("number_of_eaters", product_number.getSelectedItem()+""));
                    parts.add(new StringPart("ingredients", product_des.getText().toString()));
                    parts.add(new StringPart("price", product_price.getText().toString()));
                    for (int i = 0; i < files.size(); i++) {
                        parts.add(new FilePart("images[" + i + "]", files.get(i)));
                    }


                    requiresApi.MultipartRequest("POST", "product", getString(R.string.add_product), parts, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.d("result", result);
                            ((MainActivity) getActivity()).openFragment(new MyProfileFragment());
                        }

                        @Override
                        public void onRequestError(String errorMessage) {
                            Log.d("errorMessage", errorMessage);

                        }
                    });

                }


                break;

            case R.id.product_add_img:
                Intent intent = new Intent(getActivity(), ImageAdd.class);
                intent.putExtra("requestType", Constant._TYPE_ADD_PRODUCT);
                startActivityForResult(intent, Constant._TYPE_PICTURE);
                break;
        }
    }


    public boolean validation() {


        if (product_name.getText().toString().equalsIgnoreCase("")) {
            product_name.setError(getResources().getString(R.string.fill_name));
            product_name.requestFocus();
            return false;
        }
        if (product_des.getText().toString().equalsIgnoreCase("")) {
            product_des.setError(getResources().getString(R.string.fill_ingredients));
            product_des.requestFocus();

            return false;
        }
        if (product_price.getText().toString().equalsIgnoreCase("")&&product_price.getText().toString().length()<4) {
            product_price.setError(getResources().getString(R.string.fill_price));
            product_price.requestFocus();
            return false;
        }

        return true;
    }
}
