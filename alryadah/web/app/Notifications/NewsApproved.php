<?php

namespace App\Notifications;


use App\Channels\FCM_Message;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewsApproved extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [\App\Channels\FCM::class];
    }


    public function toFCM($notifiable)
    {
        $fcm = new FCM_Message();


        $fcm->setData([
            "news" => ["msg" => "user liked this fuck"]]);


        return $fcm;
    }


}
