package com.it.washmit.bookgoodmorning.model;

/**
 * Created by washm on 4/3/17.
 */

public class searchResult {
    private  int pageId;
    private String titel;
    private String bady;

    public searchResult(int pageId, String titel, String bady) {
        this.pageId = pageId;
        this.titel = titel;
        this.bady = bady;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBady() {
        return bady;
    }

    public void setBady(String bady) {
        this.bady = bady;
    }
}
