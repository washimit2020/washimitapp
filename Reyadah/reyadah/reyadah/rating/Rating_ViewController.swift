//
//  Rating_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol RatingDelegate {
    func rating(_ rating: Double?)
}

class Rating_ViewController: DialogBase_ViewController {
   
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var done: UIButton!
    @IBOutlet weak var close: UIButton!
    fileprivate let reuseIndentifire = "ratingCell"
    var items = [String:String]()
    
    var delegate:RatingDelegate!
    
    var rated:Rated?
    var link:String!
    
    var model:Any?{
        didSet{
            if let user = model as? Branch {
                
                self.items =  [
                    "prices_rating":"الاسعار",
                    "products_rating":"تنوع المنتجات",
                    "users_rating":"تعامل الموظفين",
                    "view_rating":"طريقة العرض"
                ]
                rated = user.rated
                let id:Int = user.id!
                if user.user_id == 0{
                    params["user_id"] = "\(id)"
                    link  = RequestLinks.rateUser.rawValue
                }else{
                    params["festival_id"] = "\(id)"
                    link  = RequestLinks.rateFestival.rawValue
                }
               
            }else if let product = model as? Product{
                
                self.items =  [
                    "price":"السعر",
                    "quality":"الجودة",
                    "style":"الشكل",
                    "ingredients":"المكونات"
                ]
                params["product_id"] = "\(product.id!)"
                rated = product.rated
                
                link  = RequestLinks.rateProduct.rawValue

            }
        }
    }
    
    var params = [String:String]()
    
    var apiRequest:ApiRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.delegate = self
        self.tableview.dataSource = self
        
        tableview.register(UINib(nibName: "RatingCell", bundle: nil), forCellReuseIdentifier: reuseIndentifire)
        
        // Do any additional setup after loading the view.
        
        done.addTarget(nil, action: #selector(rate), for: .touchUpInside)
        close.addTarget(nil, action: #selector(closeDialog), for: .touchUpInside)
        let container = view.subviews[0]
        container.layer.cornerRadius = 30
        container.layer.masksToBounds = true
        container.clipsToBounds = true
       
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        height.constant = CGFloat((items.count+1) * 47)
        tableview.reloadData()

    }
    
    func rate()
    {
        var index = 0
        
        for (key,_) in items{
            let cell = self.tableview.cellForRow(at: IndexPath(row: index, section: 0)) as! RatingCell
            params[key] = "\(cell.cosmos.rating)"
            rated!.setter(key: key, value:  cell.cosmos.rating)
            
            index += 1
        }
       
        self.apiRequest!.make_request(parameters: params, apiImages: nil, link: link, method: .post, dialogMsg: nil, completion: {
            $0?.responseJSON(completionHandler: self.handler)
            
        })
        self.dismiss(animated: true, completion: nil)
    }
    
    func closeDialog(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func handler(_ data : DataResponse<Any>!){
        let json = JSON(data.data!)
        self.delegate.rating(json["msg"].dictionaryValue["rating"]?.doubleValue)
    }
    
   

}

extension Rating_ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIndentifire, for: indexPath) as! RatingCell
        
        var rating:Double = 1
        
        rating = rated![Array(items.keys)[indexPath.row]]
        
        cell.updateCell(text: Array(items.values)[indexPath.row], rating: rating)
        return cell
    }
}
