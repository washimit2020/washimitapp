<?php

use Illuminate\Database\Seeder;
use \App\Http\Helpers\Constants;
class UserAttacmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_attachment_types')->insert(
            [
                [
                    "id"=>Constants::USER_ATTACHMENT_PROFILE_IMAGE,
                    "name_en"=>"profile image",
                    "name_ar"=>"الصورة الشخصية"
                ]
            ]
        );
    }
}
