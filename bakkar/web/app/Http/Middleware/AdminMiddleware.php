<?php

namespace App\Http\Middleware;

use App\Http\Constants;
use Closure;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user()->cannot(Constants::CAN_ADMIN) ){
            abort(401);
        }

        return $next($request);
    }
}
