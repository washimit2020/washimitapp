package com.it.washim.bakkar.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.model.Articles;
import com.it.washim.bakkar.model.Posts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohammad on 7/26/2017.
 */

public class ArticleAndVideoAdapter extends BaseExpandableListAdapter {

    private Context _context;
    List<Articles> articles;



    public ArticleAndVideoAdapter(Context _context, List<Articles> articles) {
        this._context = _context;
        this.articles = articles;


    }

    @Override
    public int getGroupCount() {
        return articles.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return articles.get(groupPosition) != null ? articles.get(groupPosition).children.size() : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return articles.get(groupPosition);
    }

    @Override
    public ArrayList<Posts> getChild(int groupPosition, int childPosition) {
        return articles.get(groupPosition).children;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Posts child = (Posts) getChild(groupPosition, childPosition).get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        MyTextView txtListChild = (MyTextView) convertView
                .findViewById(R.id.lblListItem);

        txtListChild.setText(child.title);
        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Articles articles1 = (Articles) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        MyTextView lblListHeader = (MyTextView) convertView
                .findViewById(R.id.lblListHeader);

        MyTextView artical_number = (MyTextView) convertView
                .findViewById(R.id.artical_number);

        ImageView img = (ImageView) convertView
                .findViewById(R.id.img);
//        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(articles1.title);
        artical_number.setText((groupPosition + 1) + "");

        if (isExpanded) {
            img.setImageDrawable(_context.getResources().getDrawable(R.drawable.bakar_icon_arrow_up));
        } else {
            img.setImageDrawable(_context.getResources().getDrawable(R.drawable.bakar_icon_arrow_dwon));
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
