<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Users_Comment extends Model
{
    protected $fillable = ['post_id','user_id','body'];

    protected $with = ['user'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }

}