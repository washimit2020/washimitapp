package com.washimit.it.ryadah.model;

/**
 * Created by mohammad on 4/24/2017.
 */

public class Following {

   int total;

    public Following(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
