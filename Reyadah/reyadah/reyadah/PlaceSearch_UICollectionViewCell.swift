//
//  PlaceSearch_UICollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/7/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class PlaceSearch_UICollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var avatar: BorderedImageView!

    @IBOutlet weak var locationButton: AspectFitButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var followButton: AspectFitButton!
    
    var user:Festival?{
        didSet{
            updateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup_cosmos(cosmos: ratingStar)
    }
    
    func updateView()  {
        guard let user = user else {
            return
        }
        
        self.name.text  = user.name
        self.locationText.text = make_location_text(user)
        self.ratingStar.rating = user.rating
        avatar.set_avatar_image(user_id: user.id)
        followButton.setImage(check_user_follow_status(user: user), for: .normal)
        
    }
    
 
}
