<?php

namespace App\Http\Requests\Profile;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'street_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'sector_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'password' => 'required_without:token',
            'token' => 'required_without:password',
            'new_password' => 'min:6|confirmed',
            'info' => 'required|max:500',
            'lat' => 'required_with:lng|max:'.Constants::MAX_STRING_LENGTH,
            'lng' => 'required_with:lat|max:'.Constants::MAX_STRING_LENGTH,
            'facebook' => 'max:'.Constants::MAX_STRING_LENGTH,
            'twitter' => 'max:'.Constants::MAX_STRING_LENGTH,
            'snapchat' => 'max:'.Constants::MAX_STRING_LENGTH,
            'instagram' => 'max:'.Constants::MAX_STRING_LENGTH,
            'whatsapp' => 'max:'.Constants::MAX_STRING_LENGTH,
            'avatar' => Constants::MIME_TYPE,
            'cover' => 'array|max:' . Constants::MAXIMUM_ALLOWED_IMAGE,
            'cover.*' => Constants::MIME_TYPE
        ];

    }
}
