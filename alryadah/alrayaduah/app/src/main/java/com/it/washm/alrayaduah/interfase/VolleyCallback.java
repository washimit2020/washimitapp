package com.it.washm.alrayaduah.interfase;

import com.android.volley.VolleyError;

/**
 * Created by washm on 2/22/17.
 */

public interface VolleyCallback {
    /**
     *
     * @param result
     */
    void onSuccess(String result);

    /**
     *
     * @param errorMessage
     */
    void onRequestError(VolleyError errorMessage);


}