<?php

namespace App\Providers;

use App\__ModelsProducts\ShareProduct;
use App\Http\Constants;
use App\ManyToPlacesModels\ShowBranch;
use App\Policies\BranchesPolicy;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        ShowBranch::class => BranchesPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define(Constants::ABILITY_ADD_PRODUCT,function (User $user){
            if ($user->status_id != Constants::_STATUS_ACTIVE){
                return false;
            }
           return in_array($user->type_id,[Constants::_TYPE_SHOW,Constants::_TYPE_STORE]);
        });

        Gate::define(Constants::ABILITY_HAS_BRANCHES,function (User $user){
            if ($user->status_id != Constants::_STATUS_ACTIVE){
                return false;
            }
            return in_array($user->type_id,[Constants::_TYPE_SHOW]);
        });

    }
}
