<?php

namespace App\Http\Middleware;

use Closure;

class AppKeyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->header("app-key") != env("APP_KEY")){
            abort(401);
        }

        return $next($request);
    }
}
