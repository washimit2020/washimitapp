//
//  LoadingDialog_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/17/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class LoadingDialog_ViewController: DialogBase_ViewController {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var container: UIView!
    var msg = ""
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.cornerRadius = 30
       
        container.layer.masksToBounds = true
        container.clipsToBounds = true
               
        label.text = msg
        
        closeButton.addTarget(self, action: #selector(close_dialog(_:)), for: .touchUpInside)
 
    }
    
     func close_dialog(_ sender:Any?){
        self.dismiss(animated: true
            , completion: nil)
    }

}
