<?php

namespace App\ManyToPlacesModels;

use App\__ModelsFestival\Festival;
use App\Http\Constants;
use Illuminate\Database\Eloquent\Model;

class Follower extends Model
{
    protected $fillable = ['user_id', 'place_id', 'type_id'];

    protected $hidden= ['user_id','place_id','type_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * to get the place from the relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function place()
    {
        if ($this->type_id == Constants::_TYPE_FOLLOW_FESTIVAL) {
            return $this->belongsTo(Festival::class,'place_id');
        }

        return $this->belongsTo('App\User', 'place_id');
    }
}
