//
//  ImageAdder_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/20/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ImageAdder_ViewController: UIViewController {
    
    @IBOutlet weak var top: NSLayoutConstraint!
    var items = [UIImage!]()
    let defaultImage = UIImage(named:"discover_pic")
    var currentSender:UIImageView?
    var index = 0
    var didChooseCell = false
    var callCallback = true
    var hideAvatar = false
    var user_id:Int?
    var avatar:UIImage?
    var links:[URL?]?
    
    lazy var chosedImages = [UIImage]()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var avatarHolder: UIStackView!
    @IBOutlet var tap: UITapGestureRecognizer!
    
    var imagePicker = UIImagePickerController()
    var getImagesCallback:((_ avatar:UIImage?,_ covers:[UIImage]?)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tap.addTarget(self, action: #selector(showAlert(_:)))
        
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        
        if hideAvatar{
            avatarHolder.removeFromSuperview()
            top.constant = 8
            
        }
        
        
        if let avatar = avatar {
            (self.tap.view as? UIImageView)?.image = avatar
        }else{
            if let user_id = user_id{
                (tap.view as? UIImageView)?.set_avatar_image(user_id: user_id)
            }
        }
        
        
        if items.count < 3 {
            for _ in items.count..<3{
                items.append(defaultImage)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func didMove(toParentViewController parent: UIViewController?) {
        super.didMove(toParentViewController: parent)
        if parent == nil{
            if let callback = getImagesCallback{
                var image:UIImage? = nil
                if let avatar = (self.tap.view as? UIImageView)?.image{
                    image = avatar
                }
                
                callback(image,chosedImages)
            }
            
        }
    }
    
    
    public func showAlert(_ sender:Any){
        
        didChooseCell = false
        if let tap = sender as? UITapGestureRecognizer {
            currentSender = tap.view as? UIImageView
        }else if let cell = sender as? ImageCell_CollectionViewCell{
            index = collectionView.indexPath(for: cell)!.row
            didChooseCell = true
            currentSender = cell.image
        }
        
        
        let alert = UIAlertController(title: "اختر من ", message: nil, preferredStyle: .actionSheet)
        
        
        let photoLib = UIAlertAction(title: "الاستديو", style: .default, handler: {
            _ in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let camera = UIAlertAction(title: "الكاميرا", style: .default) {
            _ in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        
        
        
        let cancel = UIAlertAction(title: "اغلاق", style: .cancel)
        
        alert.addAction(photoLib)
        alert.addAction(camera)
        alert.addAction(cancel)
        
        if currentSender?.image != defaultImage{
            let delete = UIAlertAction(title: "حذف", style: .destructive) {
                _ in
                if self.didChooseCell {
                    self.items[self.index] = self.defaultImage
                    self.collectionView.reloadItems(at: [IndexPath(item: self.index, section: 0)])
                }else{
                    let image_view = self.tap.view as! UIImageView
                    image_view.image = self.defaultImage
                }
            }
            
            alert.addAction(delete)
            
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ImageAdder_ViewController:UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageCell_CollectionViewCell
        
        if items[indexPath.row] != defaultImage{//if it comes from the apiImages dict
            cell.image.image = items[indexPath.row]
        }
        else if let _ = user_id ,links!.indices.contains(indexPath.row){
            cell.image.kf.setImage(with: links? [indexPath.row])

        }else{
            cell.image.image = items[indexPath.row]

        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        showAlert(collectionView.cellForItem(at: indexPath)!)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width , height:  collectionView.frame.height )
    }
}

extension ImageAdder_ViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        guard let view = currentSender , let image = (info[UIImagePickerControllerEditedImage]) as? UIImage else {
            return
        }
        
        if didChooseCell{
            items[index] = image
            chosedImages.append(image)
            self.collectionView.reloadItems(at: [IndexPath(item: self.index, section: 0)])
            
        }else{
            view.image = image
        }
        
        self.dismiss(animated: true, completion: nil);
    }
    
}
