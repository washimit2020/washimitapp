package com.it.washm.alrayaduah.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by washm on 1/8/17.
 */

public class CustamViewFont extends View {
    private   Typeface typeface;
    private Context context;
    private String fontType;

    /**
     *
     * @param context
     * @param fontType
     */
    public CustamViewFont(Context context, String fontType) {
        super(context);
        this.context=context;
        this.fontType=fontType;
        setFontType(fontType);
    }


    /**
     *
     * @param textInputLayout
     */
    public  void setfont(TextInputLayout textInputLayout){
        textInputLayout.getEditText().setTypeface(typeface);
        textInputLayout.setTypeface(typeface);

    }

    /**
     *
     * @param textInputLayout
     * @param fontType
     */
    public  void setfont(TextInputLayout textInputLayout,String fontType){
        setFontType(fontType);
        textInputLayout.getEditText().setTypeface(typeface);
        textInputLayout.setTypeface(typeface);
        setFontType(this.fontType);


    }




    /**
     *
     * @param textView
     */
    public  void setfont(TextView textView){
        textView.setTypeface(typeface);

    }

    /**
     *
     * @param textView
     * @param fontType
     */
    public void setfont(TextView textView,String fontType){
        setFontType(fontType);
        this.fontType=fontType;
        textView.setTypeface(typeface);
        setFontType(this.fontType);

    }

    /**
     *
     * @param Button
     */
    public  void setfont(Button Button){
        Button.setTypeface(typeface);

    }

    /**
     *
     * @param Button
     * @param fontType
     */
    public  void setfont(Button Button,String fontType){
        setFontType(fontType);
        this.fontType=fontType;
        Button.setTypeface(typeface);
        setFontType(this.fontType);


    }

    /**
     *
     * @param RadioButton
     */
    public  void setfont(RadioButton RadioButton){
        RadioButton.setTypeface(typeface);

    }

    /**
     *
     * @param RadioButton
     * @param fontType
     */
    public  void setfont(RadioButton RadioButton,String fontType){
        setFontType(fontType);
        this.fontType=fontType;
        RadioButton.setTypeface(typeface);
        setFontType(this.fontType);

    }

    /**
     *
     * @param fontType
     */
    public void setFontType(String fontType) {
        typeface= Typeface.createFromAsset(context.getAssets(),"fonts/"+fontType);
    }

}
