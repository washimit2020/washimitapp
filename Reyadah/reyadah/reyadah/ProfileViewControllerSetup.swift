//
//  ProfileViewControllerSetup.swift
//  reyadah
//
//  Created by WASHM on 4/29/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

extension Profile_ViewController{
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tabbar"{
            self.tabBar_Controller = segue.destination as? ProfileTab_ViewController
            self.tabBar_Controller.selectedViewController = self.tabBar_Controller.viewControllers?[selectedItem]
        }
        
    }
    
    
    
    @IBAction func edit(_ sender: Any) {
        
        if profileProduct != nil && profileProduct.collectionView!.allowsMultipleSelection {
            return
        }
        
        let register = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.register.rawValue) as! RegisterTVC
        makeDetailsTitle(register, title: "تعديل الحساب", image: nil)
        register.user = self.user
        self.navigationController?.pushViewController(register, animated: true)
    }
    
    
    
    
    @IBAction func profileInfo(_ sender: Any) {
        if profileProduct != nil && profileProduct.collectionView!.allowsMultipleSelection {
            return
        }
        
        let info = Info_TableViewController(nibName: "Info_TableViewController", bundle: nil)
        info.items = self.followers
        info.user = self.user
        self.navigationController?.pushViewController(info, animated: true)
    }
    
    
    func share(_ sender:Any){
        guard let indexPaths = profileProduct.collectionView?.indexPathsForSelectedItems , indexPaths.count > 0  else{
            let alert = UIAlertController(title: "ملاحظة", message: "يجب اختيار منتج واحد على الاقل", preferredStyle: .actionSheet)
//            alert.view.subviews[0].backgroundColor = UIColor.white
            alert.addAction(.init(title: "تم", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        let shows = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.shareToPlace.rawValue) as! ShareToPlace_TableViewController
        var product:Product!
        for index in indexPaths{
            product = profileProduct.items[index.row] as? Product
            shows.products.append(product)
        }
        shows.delegate = self
        shows.apiRequest = apiRequest
        self.navigationController?.pushViewController(shows, animated: true)
    }
    
    func share_product(_ sender:AspectFitButton)  {
        if Gate.is_costumer(){
            return
        }
        
        if profileProduct == nil{
            profileProduct =  tabBar_Controller!.viewControllers?[3] as! MyProducts_CollectionViewController
        }
        
  
        
        if  profileProduct.collectionView!.allowsMultipleSelection {
            
            clearSelection()
        }else{
            tabBar_Controller!.selectedViewController = profileProduct
            
            if !Gate.is_show(){
                sender.setImage(UIImage(named:"ic_add_produce_to_store_active"), for: .normal)// when he give us the second
            }else{
                sender.setImage(UIImage(named:"ic_add_produce_to_store_active"), for: .normal)
                
            }
          
            self.addBranch.isEnabled = false
            self.addProduct.isEnabled = false
            self.navigationItem.setLeftBarButton(done, animated: true)
            self.navigationItem.setRightBarButton(cancel, animated: true)
            self.navigationItem.rightBarButtonItem?.action = #selector(clearSelection)
            self.navigationItem.leftBarButtonItem?.action = #selector(share(_:))
            self.navigationItem.leftBarButtonItem?.target = self
            self.navigationItem.rightBarButtonItem?.target = self
            
            tabBar_Controller?.tabBar.isUserInteractionEnabled = false
            profileProduct.collectionView?.allowsMultipleSelection = true
        }
        
    }
    
    func clearSelection() {
        guard let profileProduct = profileProduct , profileProduct.collectionView!.allowsMultipleSelection else {
            return
        }
       
        if !Gate.is_show(){
            self.shareProduct.setImage(UIImage(named:"ic_add_produce_to_store_unactive"), for: .normal)
        }else{
            self.shareProduct.setImage(UIImage(named:"ic_add_produce_to_store_unactive"), for: .normal)
        }
        self.addBranch.isEnabled = true
        self.addProduct.isEnabled = true
        
        tabBar_Controller?.tabBar.isUserInteractionEnabled = true
        profileProduct.collectionView?.allowsMultipleSelection = false
        profileProduct.collectionView?.allowsSelection = false
        
        self.navigationItem.rightBarButtonItem = slideMenuController()?.navigationItem.rightBarButtonItem
        
        let collectionView = self.profileProduct?.collectionView
        
        if let indexPaths = collectionView?.indexPathsForSelectedItems , indexPaths.count > 0 {
            indexPaths.forEach {
                collectionView?.deselectItem(at: $0, animated: true)
            }
        }
        
        collectionView?.reloadData()
        
        self.navigationItem.setLeftBarButton(nil, animated: true)
        self.navigationItem.setRightBarButton(nil, animated: true)
        self.navigationItem.hidesBackButton = false
    }
    
}

extension Profile_ViewController:LocationsDelegate{
    func canceled() {
        clearSelection()
    }
}


