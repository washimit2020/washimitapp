package com.washimit.it.ryadah.model.user;

/**
 * Created by mohammad on 4/25/2017.
 */

public class Rated {

    double view_rating;
    double products_rating;
    double users_rating;
    double prices_rating;

    public Rated(double view_rating, double products_rating, double users_rating, double prices_rating) {
        this.view_rating = view_rating;
        this.products_rating = products_rating;
        this.users_rating = users_rating;
        this.prices_rating = prices_rating;
    }

    public double getView_rating() {
        return view_rating;
    }

    public void setView_rating(double view_rating) {
        this.view_rating = view_rating;
    }

    public double getProducts_rating() {
        return products_rating;
    }

    public void setProducts_rating(double products_rating) {
        this.products_rating = products_rating;
    }

    public double getUsers_rating() {
        return users_rating;
    }

    public void setUsers_rating(double users_rating) {
        this.users_rating = users_rating;
    }

    public double getPrices_rating() {
        return prices_rating;
    }

    public void setPrices_rating(double prices_rating) {
        this.prices_rating = prices_rating;
    }
}
