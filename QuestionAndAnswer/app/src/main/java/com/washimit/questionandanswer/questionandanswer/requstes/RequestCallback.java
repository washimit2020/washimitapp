package com.washimit.questionandanswer.questionandanswer.requstes;

import org.json.JSONException;

/**
 * Created by mohammad on 5/31/2017.
 */


public  interface RequestCallback {

    void onResponse(String response) throws JSONException;

    void ErrorListener();


}