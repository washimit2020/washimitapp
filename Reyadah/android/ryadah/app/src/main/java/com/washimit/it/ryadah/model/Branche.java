package com.washimit.it.ryadah.model;

/**
 * Created by washm on 5/1/17.
 */

public class Branche {


    int id;
    int user_id;
    String name;
    String street_name;
    String sector_name;
    double lat;
    double lng;
    int status_id;
    String created_at;
    String updated_at;
    private boolean isSelected = false;

    public Branche(int id, int user_id, String name, String street_name, String sector_name, double lat, double lng, int status_id, String created_at, String updated_at, boolean isSelected) {
        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.street_name = street_name;
        this.sector_name = sector_name;
        this.lat = lat;
        this.lng = lng;
        this.status_id = status_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getSector_name() {
        return sector_name;
    }

    public void setSector_name(String sector_name) {
        this.sector_name = sector_name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
