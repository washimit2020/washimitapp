package com.washimit.it.ryadah.registrationActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.model.SocialMedia;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.log_in_email)
    EditTextView log_in_email;
    @BindView(R.id.log_in_password)
    EditTextView log_in_password;
    @BindView(R.id.log_in_forgot_password)
    TextViewCast log_in_forgot_password;
    @BindView(R.id.log_in_new_acount)
    TextViewCast log_in_new_acount;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.log_in)
    ButtonView log_in;
    @BindView(R.id.google_log_in)
    ImageView google_log_in;
    @BindView(R.id.log_in_facebook)
    ImageView log_in_facebook;
    @BindView(R.id.log_in_twitter)
    ImageView log_in_twitter;
    Bundle bundle;
    ApiRequest request;
    ArrayList<Part> parts;
    SharedPref pref;
    CallbackManager callbackManager;
    boolean login = true;
    TwitterSession session;
    TwitterAuthClient client;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInResult result;
    GoogleSignInOptions gso;
    SocialMedia media;
    Gson gson;
    String token;
    private static final int RC_SIGN_IN = 9001;

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "cKi0LqUIrdUcCjCSfcycgghhW";
    private static final String TWITTER_SECRET = "5KR6J0jrCWHTWzOqt3ewtdCsm5zcutZ9KKmawscL6ShglURMvk";

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        client.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestScopes(new Scope(Scopes.PLUS_LOGIN)).requestEmail().build();
        Fabric.with(this, new Twitter(authConfig));
        client = new TwitterAuthClient();
        gson = new Gson();
        callbackManager = CallbackManager.Factory.create();
        // FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_log_in);



        // Get token
        token = FirebaseInstanceId.getInstance().getToken();

        // Log
        Log.d("msg", getString(R.string.msg_token_fmt, token));



        ButterKnife.bind(this);


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();

        pref = new SharedPref(this);
        bundle = getIntent().getExtras();
        request = new ApiRequest(LogInActivity.this);
        parts = new ArrayList<Part>();
        log_in_new_acount.setOnClickListener(this);
        log_in.setOnClickListener(this);
        log_in_forgot_password.setOnClickListener(this);
        back.setOnClickListener(this);
        log_in_facebook.setOnClickListener(this);
        google_log_in.setOnClickListener(this);
        log_in_twitter.setOnClickListener(this);

    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.log_in_new_acount:
                openActivity(SignUpActivity.class, bundle.getInt("userType"));
                break;
            case R.id.back:
                this.finish();
                break;
            case R.id.log_in_forgot_password:
                openActivity(ForgotPasswordActivity.class, bundle.getInt("userType"));
                break;
            case R.id.log_in:
                if (validation()) {
                    parts.add(new StringPart("username", log_in_email.getText().toString()));
                    parts.add(new StringPart("password", log_in_password.getText().toString()));
                    parts.add(new StringPart("type_id", String.valueOf(bundle.getInt("userType"))));
                    parts.add(new StringPart("reg_id", token+""));
                    request.MultipartRequest("POST", "login", getString(R.string.try_login), parts, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result_) {
                            JSONObject result = null;
                            JSONObject msg = null;

                            try {
                                result = new JSONObject(result_);
                                msg = result.getJSONObject("msg");
                                Gson gson = new Gson();
                                com.washimit.it.ryadah.model.user.User user = gson.fromJson(msg.getString("user"), com.washimit.it.ryadah.model.user.User.class);
                                user.setToken(msg.getString("token"));
                                pref.setUser(gson.toJson(user));
                                openActivity(MainActivity.class, user.getType().getId());
                                finish();


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            Log.d("result", result_);
                        }

                        @Override
                        public void onRequestError(String errorMessage) {
                            Log.d("errorMessage", errorMessage);
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                        }
                    });

                }

                break;
            case R.id.log_in_facebook:

                LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                Log.v("LoginActivity", loginResult.toString());

                                if (login) {
                                    login = false;
                                    // App code
                                    GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                                        @Override
                                        public void onCompleted(JSONObject object, GraphResponse response) {
                                            Log.v("LoginActivity", response.toString());


                                            // Application code
                                            try {

                                                media = new SocialMedia(object.getString("name"), object.getString("email"), "", "", "", "", object.getString("id") + "", String.valueOf(bundle.getInt("userType")));


                                                //  loginWehtSocial();
                                                openActivity();

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    });

                                    Bundle parameters = new Bundle();

                                    parameters.putString("fields", "id,about,name,email,first_name,last_name,picture,gender,location");
                                    graphRequest.setParameters(parameters);
                                    graphRequest.executeAsync();
                                }
                            }

                            @Override
                            public void onCancel() {
                                // App code
                                Log.v("onCancelLoginActivity", "onCancel");

                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                                Log.v("FacebookoginActivity", exception + "");

                            }
                        });

                break;

            case R.id.google_log_in:
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;

            case R.id.log_in_twitter:
                //
                client.authorize(this, new Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();

                        session = twitterSessionResult.data;

                        String username = session.getUserName();

                        Long userid = session.getUserId();
                        session.getAuthToken();


                        Twitter.getApiClient(session).getAccountService().verifyCredentials(true, true).enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                            @Override
                            public void success(Result<com.twitter.sdk.android.core.models.User> userResult) {
                                final com.twitter.sdk.android.core.models.User user = userResult.data;
                                String twitterImage = user.profileImageUrl;

                                try {
                                    Log.d("imageurl", user.profileImageUrl);
                                    Log.d("name", user.name);
                                    Log.d("des", user.description);
                                    Log.d("followers ", String.valueOf(user.followersCount));

                                    client.requestEmail(session, new Callback<String>() {
                                        @Override
                                        public void success(Result<String> result) {
                                            // Do something with the result, which provides the email address
                                            Log.d("email", new String(result.data));
                                            media = new SocialMedia(user.name, new String(result.data), "", user.description + "", "", "", user.id + "", String.valueOf(bundle.getInt("userType")));


                                            openActivity();
                                        }

                                        @Override
                                        public void failure(TwitterException exception) {
                                            // Do something on failure
                                            String exceptionMsg = exception.getMessage();
                                            Toast.makeText(getApplicationContext(), "TwitterException = " + exceptionMsg, Toast.LENGTH_LONG).show();
                                        }
                                    });


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void failure(TwitterException e) {

                            }

                        });


                    }

                    @Override
                    public void failure(TwitterException e) {
                    }
                });
                break;

        }
    }


    public boolean validation() {
//        if (!(android.util.Patterns.EMAIL_ADDRESS.matcher(log_in_email.getText().toString()).matches() && log_in_email.getText().toString().length() >= 10)) {
//            log_in_email.setError(getResources().getString(R.string.invalid_email));
//            return false;
//        }
        if (!(log_in_password.getText().toString().length() >= 5)) {
            log_in_password.setError(getResources().getString(R.string.invalid_password));

            return false;
        }
        return true;
    }


    public void openActivity(Class activity, int userType) {
        Intent intent = new Intent(LogInActivity.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        startActivity(intent);
        this.finish();

    }

    public void
    openActivity() {
        Intent intent = new Intent(LogInActivity.this, SocialMediaActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("media", gson.toJson(media));
        startActivity(intent);

    }


    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("handleSignInResult", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

            GoogleSignInAccount acct = result.getSignInAccount();



            media = new SocialMedia(person.getDisplayName(), acct.getEmail(), "", "", "", "", acct.getId() + "", String.valueOf(bundle.getInt("userType")));


            openActivity();

        } else {
            // Signed out, show unauthenticated UI.
        }
    }


}
