//
//  static letants.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

struct Constants {
    
    static let APP_KEY = "base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84="
    static let TYPE_STORE = 1;
    static let TYPE_SHOW = 2;
    static let TYPE_COSTUMER = 3;
    static let TYPE_FESTIVAL = 4;
    static let TYPE_PRODUCT = 5;
    static let TYPE_AVATAR_IMAGE = 6;
    static let TYPE_COVER_IMAGE = 7;
    static let TYPE_PRODUCT_ATTACHMENT = 8;
    static let TYPE_PRODUCT_LIKE = 9;
    static let TYPE_PRODUCT_WISHLIST = 10;
    static let TYPE_FOLLOW_USER = 11;
    static let TYPE_FOLLOW_FESTIVAL = 12;
    
    //END TYPES
    
    //BEGIN STATUSES
    
    static let STATUS_ACTIVE = 1;
    static let STATUS_INACTIVE = 2;
    static let STATUS_HIDDEN = 3;
    static let STATUS_BLOCKED = 4;
    static let STATUS_DECLINED = 5;
    static let STATUS_DELETED = 6;
    
    //BEGIN STATUSES
    
    
    
    //BEGIN C0DE
    
    //local
//    static let domain = "192.168.0.102"
//    static let host = "http://\(Constants.domain)/reyadah/public/api/"
    
    //production
        static let domain = "reyadah.wojoooh.com"
        static let host = "http://\(Constants.domain)/api/"
    
    
    static let RESPONSE_SUCCESS = "SUCCESS";
    static let RESPONSE_FAILED = "FAILED";
    static let RESPONSE_CODE_UNAUTHORIZED = 401;
    static let RESPONSE_CODE_FORBIDDEN = 403;
    static let RESPONSE_CODE_UNPROCESSABLE = 422;
    
    static let  FESTIVAL_COMMENT = "festivals";
    static let  PRODUCT_COMMENT = "products";
    
    static let  USER_SHARED = "users";
    static let  BRANCH_SHARED = "branches";
    
    static let NORMAL_REGISTER = 1;
    static let SOCIAL_REGISTER = 2;
    
  
    
    static let GET_USER_COVER = "attachment/cover/"
    static let GET_USER_AVATAR = "attachment/avatar/"
    static let GET_PRODUCT_IMAGE = "attachment/product/"
    
    static let SEARCH_ALL = 1
    static let SEARCH_PRODUCT = 2
    static let SEARCH_STORE = 3
    static let SEARCH_SHOW = 4
    static let SEARCH_FESTIVAL = 5
   
    
    static let icon_name = ["ic_store","ic_showroom","ic_festival"]
    
    static let image_adder_id = "image_adder"

    static let themeColor = UIColor(red: 129/255, green: 51/255, blue: 49/255, alpha: 1)
    static let fadedThemeColor = UIColor(red: 129/255, green: 51/255, blue: 49/255, alpha: 0.5)

    static let emailErrorMsg = "Invalid Email"
    static let requiredErrorMsg = "This field is required"
    static let minLengthErrorMsg =  "Must be at least %ld characters long"
    static let confirmedErrorMsg =  "This field does not match"
    static let phoneErrorMsg =  "Enter a valid 10 digit phone number"
    
    
    
  
}

enum Storyboard_ids:String{
    case add_product = "add-product"
    case register = "register"
    case home = "HomeVc"
    case main_page = "main"
    case image_adder = "image_adder"
    case locationSearch = "locationSearch"
    case main_products = "home_products"
    case main_places = "home_places"
    case profile = "profile"
    case locations = "locations"
    case shareToPlace = "share_to_place"
    case addFestival = "addFestival"
}
