<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        "title", "news_main_type_id", "news_sub_type_id", "content", 'disapprove_description',"is_banner", "is_main", "user_id", "news_status_id"
    ];

    /**
     * @var array
     */
    protected $hidden = [
        "is_banner", "is_main", "news_sub_type_id", "news_main_type_id", "user_id", 
    ];

    /**
     * belongs to one news sub type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sub_type()
    {
        return $this->belongsTo('App\NewsSubType', 'news_sub_type_id');
    }

    /**
     * belongs to one main type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function main_type()
    {
        return $this->belongsTo('App\NewsMainType', 'news_main_type_id');
    }

    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * belongs to one status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo('App\NewsStatus', 'news_status_id');
    }

    /**
     * belongs to many user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachment()
    {
        return $this->hasMany('App\NewsAttachment', 'news_id');
    }

    /**
     * has many views
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany('App\SeenNews','news_id');
    }

    /**
     * has many likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany('App\NewsLike','news_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment','news_id');
    }
}
