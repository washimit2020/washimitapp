<?php

namespace App\Http\Middleware;

use App\Comment;
use App\Http\Constants;
use Closure;

class CommentMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->comment)) {
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }
        if ($request->comment instanceof Comment) {
            if ($request->comment->status_id == Constants::_STATUS_DELETED){
                abort(Constants::RESPONSE_CODE_FORBIDDEN);

            }
            if ($request->comment->user_id != $request->user()->id) {
                abort(Constants::RESPONSE_CODE_FORBIDDEN);
            }
        } else {
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }

        return $next($request);
    }
}
