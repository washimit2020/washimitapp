<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;



use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['name'];
	protected $hidden   =   ['pivot',"created_at","updated_at"];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}