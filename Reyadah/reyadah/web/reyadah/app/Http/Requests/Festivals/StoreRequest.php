<?php

namespace App\Http\Requests\Festivals;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'street_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'sector_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'organizer_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'organization' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'source' => 'required|max:'.Constants::MAX_STRING_LENGTH,
            'organizer_phone_number' => 'required|digits:10',
            'info' => 'required|max:5000',
            'lat' => 'required_with:lng|max:'.Constants::MAX_STRING_LENGTH,
            'lng' => 'required_with:lat|max:'.Constants::MAX_STRING_LENGTH,
            'start_in' => 'required|date|date_format:Y-m-d',
            'to' => 'required|date_format:G:i|after:from',
            'from' => 'required|date_format:G:i',
            'facebook' => 'max:'.Constants::MAX_STRING_LENGTH,
            'twitter' => 'max:'.Constants::MAX_STRING_LENGTH,
            'snapchat' => 'max:'.Constants::MAX_STRING_LENGTH,
            'instagram' => 'max:'.Constants::MAX_STRING_LENGTH,
            'whatsapp' => 'max:'.Constants::MAX_STRING_LENGTH,
            'images' => 'array|max:' . Constants::MAXIMUM_ALLOWED_IMAGE,
            'images.*' => Constants::MIME_TYPE,
//            'license' => 'required|'.Constants::MIME_TYPE
        ];
    }
}
