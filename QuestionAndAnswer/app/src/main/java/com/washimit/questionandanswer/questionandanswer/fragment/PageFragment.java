package com.washimit.questionandanswer.questionandanswer.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.washimit.questionandanswer.questionandanswer.Halper.Conestant;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
import com.washimit.questionandanswer.questionandanswer.activity.ShowImageActivity;
import com.washimit.questionandanswer.questionandanswer.adapter.AnswerAdapter;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.custumView.MyEditText;
import com.washimit.questionandanswer.questionandanswer.custumView.MyTextView;
import com.washimit.questionandanswer.questionandanswer.interfac.AnswerCollbackLisenar;
import com.washimit.questionandanswer.questionandanswer.interfac.changefont;
import com.washimit.questionandanswer.questionandanswer.interfac.wishilistedCallback;
import com.washimit.questionandanswer.questionandanswer.modle.Answers;
import com.washimit.questionandanswer.questionandanswer.modle.Comment;
import com.washimit.questionandanswer.questionandanswer.modle.Question;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PageFragment extends BaseFragment implements View.OnClickListener, changefont {


    //TODO: Rename and change types of parameters

    private String imageBody;
    private String imageTitle;
    private String imageAnswerBody;

    private String comment;
    private int id;
    ImageView image;
    RelativeLayout page;
    PreferenceClass preferenceClass;
    ScrollView scrollView;
    Gson gson;
    ApiRequest request;
    ArrayList<Question> list;
    Question question;
    @BindView(R.id.answer_title)
    MyTextView answer_title;
    @BindView(R.id.answer_body)
    MyTextView answer_body;
    @BindView(R.id.title)
    MyTextView title;
    @BindView(R.id.body)
    MyTextView body;

    @BindView(R.id.image_title)
    ImageView image_title;
    @BindView(R.id.image_body)
    ImageView image_body;
    @BindView(R.id.answer_img_body)
    ImageView answer_img_body;

    @BindView(R.id.answers)
    RecyclerView recyclerView;
    @BindView(R.id.answer)
    LinearLayout answer;
    @BindView(R.id.next_question)
    FloatingActionButton next_question;
    @BindView(R.id.add_comment)
    FloatingActionButton actionButtonAddComment;

    @BindView(R.id.no_conection)
    LinearLayout no_conection;

    @BindView(R.id.retry)
    MyButtonView retry;


    @BindView(R.id.View_for_answer)
    View viewForAnswer;
    @BindView(R.id.View_for_title)
    View viewForTitle;

    wishilistedCallback callback;

    String url;

    AnswerAdapter adapter;

    ArrayList<Answers> AnswerList;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_page, container, false);
        ButterKnife.bind(this, view);
        AnswerList = new ArrayList<>();

        recyclerView.setHasFixedSize(true);

        //    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        adapter = new AnswerAdapter(getActivity(), AnswerList);

        adapter.onAnswerClickLisenar(new AnswerCollbackLisenar() {
            @Override
            public void onClack(int answerId) {
                setAnswer(answerId);
            }
        });

        recyclerView.setAdapter(adapter);

        next_question.hide();
        actionButtonAddComment.hide();
        // views = new MyButtonView[]{answer1, answer2, answer3, answer4};

        preferenceClass = new PreferenceClass(getContext());
        request = new ApiRequest(getActivity());
        gson = new Gson();
        list = new ArrayList<>();
        image = (ImageView) view.findViewById(R.id.image);
//        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        page = (RelativeLayout) view.findViewById(R.id.page);

        scrollView = (ScrollView) view.findViewById(R.id.ScrollView);
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);


        pageSeting();

        if (getArguments() != null) {
            getQuestion(getArguments().getString("id"));
        } else {
            getQuestion();
        }


        actionButtonAddComment.setOnClickListener(this);
        next_question.setOnClickListener(this);
        answer_img_body.setOnClickListener(this);
        image_body.setOnClickListener(this);
        image_title.setOnClickListener(this);
        retry.setOnClickListener(this);
//        answer1.setOnClickListener(this);
//        answer2.setOnClickListener(this);
//        answer3.setOnClickListener(this);
//        answer4.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.retry: {

                if (getArguments() != null) {
                    getQuestion(getArguments().getString("id"));
                } else {
                    getQuestion();
                }
                break;
            }
            case R.id.add_comment: {

                dilogComment(id);

                break;
            }
            case R.id.answer_img_body: {
                startActivity(new Intent(getActivity(), ShowImageActivity.class).putExtra("img", imageAnswerBody));
                break;
            }
            case R.id.image_body: {
                startActivity(new Intent(getActivity(), ShowImageActivity.class).putExtra("img", imageBody));
                break;
            }
            case R.id.image_title: {
                startActivity(new Intent(getActivity(), ShowImageActivity.class).putExtra("img", imageTitle));
                break;
            }
            case R.id.next_question: {

                if (url != null) {

                    next_question.hide();
                    actionButtonAddComment.hide();
                    answer.setVisibility(View.GONE);
                    getNextQuestion(url);
                }

                break;
            }
//            case R.id.answer1: {
//                setAnswer(0);
//                break;
//            }
//            case R.id.answer2: {
//                setAnswer(1);
//                break;
//            }
//            case R.id.answer3: {
//                setAnswer(2);
//                break;
//            }
//            case R.id.answer4: {
//                setAnswer(3);
//                break;
//            }

        }
    }

    private void dilogComment(int pageId) {
        Button add_comment, cancel;
        final MyEditText comment_text;
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilog_comment);
        add_comment = (Button) dialog.findViewById(R.id.add_comment);
        cancel = (Button) dialog.findViewById(R.id.cancel);
        comment_text = (MyEditText) dialog.findViewById(R.id.comment_text);

        if (question.comment != null) {
            comment_text.setText(question.comment.body);
            add_comment.setText("تعديل");
        }


        add_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (question.comment == null) {
                    question.comment = new Comment();
                    question.comment.body = comment_text.getText().toString();
                }
                addComment(comment_text.getText().toString());
                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(comment_text.getWindowToken(), 0);
                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.cancel();


            }
        });
        dialog.show();

    }


    private void pageSeting() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + preferenceClass.getfontType());


        title.setTypeface(tf, 1);
        title.setTextSize(preferenceClass.gettextSize());


        body.setTypeface(tf, 1);
        body.setTextSize(preferenceClass.gettextSize());


        answer_title.setTypeface(tf, 1);
        answer_title.setTextSize(preferenceClass.gettextSize());

        answer_body.setTypeface(tf, 1);
        answer_body.setTextSize(preferenceClass.gettextSize());


        // textselect.setTypeface(tf, 1);

        // textselect.setTextSize(preferenceClass.gettextSize());

        if (preferenceClass.getscrenMode()) {
            page.setBackgroundColor(getResources().getColor(R.color.white));
            body.setTextColor(getResources().getColor(R.color.black));
            title.setTextColor(getResources().getColor(R.color.black));
            answer_title.setTextColor(getResources().getColor(R.color.black));
            answer_title.setTextColor(getResources().getColor(R.color.black));
            answer_body.setTextColor(getResources().getColor(R.color.black));
            viewForAnswer.setBackgroundColor(getResources().getColor(R.color.black));
            viewForTitle.setBackgroundColor(getResources().getColor(R.color.black));

        } else {
            page.setBackgroundColor(getResources().getColor(R.color.black));
            body.setTextColor(getResources().getColor(R.color.white));
            title.setTextColor(getResources().getColor(R.color.white));
            answer_title.setTextColor(getResources().getColor(R.color.white));
            answer_title.setTextColor(getResources().getColor(R.color.white));
            answer_body.setTextColor(getResources().getColor(R.color.white));
            viewForAnswer.setBackgroundColor(getResources().getColor(R.color.white));
            viewForTitle.setBackgroundColor(getResources().getColor(R.color.white));

        }

    }

    @Override
    public void changefont() {


        pageSeting();


    }

    public void getQuestion() {
        list.clear();
        showDialog();
        request.getRequst("question", new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject questions = null;

                Log.d("responses", response);

                jsonObject = new JSONObject(response);

                questions = jsonObject.getJSONObject("question");
                url = questions.getString("next_page_url");
                Log.d("jkdsghdksjdd", url + "");
                if (url != "null") {
                    next_question.show();
                } else {
                    next_question.hide();

                }

                list.addAll((Collection<? extends Question>) gson.fromJson(questions.getString("data"), new TypeToken<ArrayList<Question>>() {
                }.getType()));

                if (list.size() > 0) {
                    question = list.get(0);
                    setQuestion(question);

                }
                hideDialog();
                scrollView.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
            }

            @Override
            public void ErrorListener() {
                Log.d("responses", "ErrorListener");
                hideDialog();
                scrollView.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getNextQuestion(String urls) {
        list.clear();
        request.getNextRequst(urls, new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject questions = null;

                Log.d("responses", response);

                jsonObject = new JSONObject(response);

                questions = jsonObject.getJSONObject("question");
                url = questions.getString("next_page_url");

                Log.d("jkdsghdksjdd", url + "");

                if (url != "null") {
                    next_question.show();
                } else {
                    next_question.hide();

                }
                list.addAll((Collection<? extends Question>) gson.fromJson(questions.getString("data"), new TypeToken<ArrayList<Question>>() {
                }.getType()));

                Log.d("jkdsghdksjdd", list.size() + "");

                if (list.size() > 0) {
                    question = list.get(0);
                    setQuestion(question);

                }
                scrollView.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
            }

            @Override
            public void ErrorListener() {
                Log.d("responses", "ErrorListener");
                scrollView.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
            }
        });
    }


    public void getQuestion(String questionId) {
        list.clear();


        request.getRequst("question/" + questionId, new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {


                JSONObject jsonObject = null;

                JSONObject questions = null;

                Log.d("getQuestion", response);

                jsonObject = new JSONObject(response);
                questions = jsonObject.getJSONObject("question");
                if (questions.getJSONArray("data").length() > 0) {
                    Question question = gson.fromJson(questions.getJSONArray("data").get(0).toString(), Question.class);
                    setQuestion(question);

                } else {
                    Toast.makeText(getActivity(), "السؤال غير موجود", Toast.LENGTH_SHORT).show();

                }

                scrollView.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);

            }

            @Override
            public void ErrorListener() {
                Log.d("getQuestion", "ErrorListener");
                scrollView.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);

                Toast.makeText(getActivity(), "السؤال غير موجود", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setQuestion(Question question) {
        answer_img_body.setVisibility(View.GONE);
        image_title.setVisibility(View.GONE);
        image_body.setVisibility(View.GONE);
        actionButtonAddComment.hide();
        next_question.hide();
        AnswerList.clear();
        adapter.notifyDataSetChanged();

        this.question = question;
        title.setText(question.id + "-" + question.title);
        body.setText("-" + question.body);

        for (int i = 0; i < question.attachments.size(); i++) {

            if (question.attachments.get(i).attachments__type_id == Conestant.QUESTION_TITLE) {

                //     image_title.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_camera));
                Picasso.with(getActivity()).load(ApiRequest.URL_QUESTION + question.id + "/" + question.attachments.get(i).attachments__type_id).into(image_title);

                imageTitle = ApiRequest.URL_QUESTION + question.id + "/" + question.attachments.get(i).attachments__type_id;
                image_title.setVisibility(View.VISIBLE);

            } else if (question.attachments.get(i).attachments__type_id == Conestant.QUESTION_BODY) {

                Picasso.with(getActivity()).load(ApiRequest.URL_QUESTION + question.id + "/" + question.attachments.get(i).attachments__type_id).into(image_body);
                imageBody = ApiRequest.URL_QUESTION + question.id + "/" + question.attachments.get(i).attachments__type_id;
                //   image_body.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_camera));
                image_body.setVisibility(View.VISIBLE);
            }


        }

        AnswerList.addAll(question.answers);

        adapter.notifyDataSetChanged();

        callback.wishilistedCollback(question.is_wishlisted);
    }

    public void setAnswer(int answerP) {
        answer.setVisibility(View.VISIBLE);
        actionButtonAddComment.show();


        if (url != "null") {
            next_question.show();
        } else {
            next_question.hide();

        }
        if (question.answers.get(answerP).is_correct == 1) {


            answer_title.setText(question.answers.get(answerP).title);
//            answer_body.setText(question.answers.get(answerP).body);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                answer_body.setText(Html.fromHtml(question.answers.get(answerP).body, Html.FROM_HTML_MODE_COMPACT));
            } else {
                answer_body.setText(Html.fromHtml(question.answers.get(answerP).body));
            }
            for (int i = 0; i < question.answers.get(answerP).attachments.size(); i++) {
                Log.d("asffasf", question.answers.get(answerP).attachments.get(i).attachments__type_id+"");

                if (question.answers.get(answerP).attachments.get(i).attachments__type_id == Conestant.ANSWER_BODY) {
                    Picasso.with(getActivity()).load(ApiRequest.URL_ANSWER + question.answers.get(answerP).id + "/" + question.answers.get(answerP).attachments.get(i).attachments__type_id)
                            .into(answer_img_body);
                    imageAnswerBody = ApiRequest.URL_ANSWER + question.answers.get(answerP).id + "/" + question.answers.get(answerP).attachments.get(i).attachments__type_id;
                    //    answer_img_body.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_camera));
                    answer_img_body.setVisibility(View.VISIBLE);
                }

            }
        }

//         else {
//            for (int i = 0; i < question.answers.size(); i++) {
//                if (question.answers.get(i).is_correct == 1) {
//                    answer_title.setText(question.answers.get(i).title);
//                    answer_body.setText(question.answers.get(i).body);
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                        answer_body.setText(Html.fromHtml(question.answers.get(i).body, Html.FROM_HTML_MODE_COMPACT));
//                    } else {
//                        answer_body.setText(Html.fromHtml(question.answers.get(i).body));
//                    }
//
//                    for (int j = 0; j < question.answers.get(i).attachments.size(); j++) {
//                        if (question.answers.get(i).attachments.get(j).attachments__type_id == Conestant.ANSWER_BODY) {
//                            answer_img_body.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu_camera));
//                            answer_img_body.setVisibility(View.VISIBLE);
//
//                        }
//
//                    }
//
//                }
//            }
//        }

    }

    public void addComment(final String comment) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("body", comment);
        map.put("question_id", question.id + "");
        request.setRequst("comment", map, new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {
                Toast.makeText(getActivity(), "تم التعليق", Toast.LENGTH_SHORT).show();
                Log.d("comment", response);
                if (question.comment != null) {
                    question.comment.body = comment;
                }
            }

            @Override
            public void ErrorListener() {
                Toast.makeText(getActivity(), "توجد مشكلة", Toast.LENGTH_SHORT).show();

            }
        });

    }

    public void getQuestionId(wishilistedCallback callback) {
        this.callback = callback;
    }

    public Question getQuestionObject() {
        return question;
    }
}