package com.it.washim.wojooohapp.Model;

/**
 * Created by mohammad on 9/19/2017.
 */

public class Book {
    public String rating;
    public String page_size;
    public String last_publish;
    public String pages_count;
    public String publish_count;
    public String price;
    public int download_count;
}
