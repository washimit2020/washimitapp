package com.it.washm.alrayaduah.fragment;


import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.MultipartCallback;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.User;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {


    @BindView(R.id.ferstName_my_account)
    AutoCompleteTextView ferstName_my_account;
    @BindView(R.id.lastName_my_account)
    AutoCompleteTextView lastName_my_account;
    @BindView(R.id.fakeNames_my_account)
    AutoCompleteTextView fakeNames_my_account;
    @BindView(R.id.email_my_account)
    TextView email_my_account;
    @BindView(R.id.password_my_account)
    AutoCompleteTextView password_my_account;
    @BindView(R.id.btn_edit)
    Button btn_edit;
    @BindView(R.id.radioFemale_my_account)
    RadioButton radioFemale_my_account;
    @BindView(R.id.radioMale_my_account)
    RadioButton radioMale_my_account;
    @BindView(R.id.profile_img_my_account)
    ImageView profile_img_my_account;
    @BindView(R.id.tilferstName_my_account)
    TextInputLayout tilferstName_my_account;
    @BindView(R.id.tillastName_my_account)
    TextInputLayout tillastName_my_account;
    @BindView(R.id.tilefakeNames_my_account)
    TextInputLayout tilefakeNames_my_account;
    @BindView(R.id.tilpassword_my_account)
    TextInputLayout tilpassword_my_account;


    Requests requests;
    List<Part> Prams;

    String password;
    String ferstName;
    String lastName;
    String nickname;
    private static int RESULT_LOAD_IMAGE = 1;
    Intent intent;
    SharedPref pref;
    CustamViewFont viewFont;
    User user;
    Gson gson;
    String ImgPath = "";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container.clearDisappearingChildren();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_account, container, false);
        ButterKnife.bind(this, view);

        viewFont = new CustamViewFont(getActivity(), "Cairo-SemiBold.ttf");

        setFont();

        gson = new Gson();
        requests = new Requests(getActivity().getApplicationContext(), getActivity());
        pref = new SharedPref(getActivity(), "MyPref");

        Prams = new ArrayList();

        btn_edit.setOnClickListener(this);
        radioFemale_my_account.setOnClickListener(this);
        radioMale_my_account.setOnClickListener(this);
        profile_img_my_account.setOnClickListener(this);


        getprofil();

        return view;
    }


    public void setFont() {

        viewFont.setfont(email_my_account);
        viewFont.setfont(tilferstName_my_account);
        viewFont.setfont(tillastName_my_account);
        viewFont.setfont(tilefakeNames_my_account);
        viewFont.setfont(tilpassword_my_account);
        viewFont.setfont(radioFemale_my_account);
        viewFont.setfont(radioMale_my_account);
        viewFont.setfont(btn_edit, "Cairo-Regular.ttf");

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            File compressedImage = new Compressor.Builder(getActivity())
                    .setMaxWidth(640)
                    .setMaxHeight(480)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .build()
                    .compressToFile(new File(AppHelper.getPathfromURI(getActivity(), selectedImage)));
            ImgPath = compressedImage.getAbsolutePath();

            Log.d("ljklkj",ImgPath);
            profile_img_my_account.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {

            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "يجب اعطاء الاذن للتطبيق حتي تتمكن من الاستمرار  ", Toast.LENGTH_LONG).show();
            } else {
                //Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit:
                if (validate()) {
                    if (AppHelper.isOnline(getActivity())) {
                        if (ImgPath.length() > 0) {
                            Prams.add(new FilePart("profile_image", new File(ImgPath)));
                        }
                        Prams.add(new StringPart("first_name", ferstName));
                        Prams.add(new StringPart("last_name", lastName));
                        if(nickname.length()>0) {
                            Prams.add(new StringPart("nick_name", nickname));
                        }
                        Prams.add(new StringPart("password", password));


                        requests.MultipartRequest("profile", "جاري تعديل معلومات الحساب...", Prams,
                                new MultipartCallback() {


                                    @Override
                                    public void onSuccess(String result) {
                                        Log.d("", result);
                                        JSONObject object = null;
                                        JSONObject message = null;
                                        JSONObject user = null;
                                        JSONObject role = null;
                                        JSONArray attachment = null;

                                        //   Log.d("aqekfjaskfjas",result);
                                        try {
                                            Log.d("", result);
                                            object = new JSONObject(result);
                                            if (object.getBoolean("success")) {
                                                message = object.getJSONObject("message");
                                                user = message.getJSONObject("user");
                                                role = user.getJSONObject("role");
                                                attachment = user.getJSONArray("attachment");

                                                pref.clearRef();

                                                //user id
                                                pref.setShared("id", user.getString("id"));

                                                //first_name
                                                pref.setShared("first_name", user.getString("first_name"));

                                                //last_name
                                                pref.setShared("last_name", user.getString("last_name"));

                                                //nick_name
                                                pref.setShared("nick_name", user.getString("nick_name"));

                                                //role id
                                                pref.setShared("role_id", role.getString("id"));

                                                //user attachment
                                                if (attachment.length() > 0) {
                                                    pref.setShared("path", attachment.getJSONObject(0).getString("path"));
                                                } else {
                                                    pref.setShared("path", "");

                                                }
                                                //user logIn
                                                pref.setShared("is_log_in", "1");

                                                //user token
                                                pref.setShared("token", message.getString("token"));

                                                Snackbar.make(getActivity().getCurrentFocus(), "تم التعديل بنجاح", BaseTransientBottomBar.LENGTH_LONG).show();


                                            } else {
                                                Snackbar.make(getActivity().getCurrentFocus(), "رقم المرور غير صحيح", BaseTransientBottomBar.LENGTH_LONG).show();

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                    }

                                    @Override
                                    public void onRequestError(String errorMessage) {
                                        Snackbar.make(getActivity().getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                                    }

                                });
                    }
                }
                else {
                    Snackbar.make(getActivity().getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                }
                break;
            case R.id.profile_img_my_account:
                if (ContextCompat.checkSelfPermission(getActivity(),
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                } else {


                        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        pickIntent.setType("image/*");
                        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //   Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                        String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
                        Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
                        chooserIntent.putExtra
                                (
                                        Intent.EXTRA_INITIAL_INTENTS,
                                        new Intent[]{takePhotoIntent}
                                );

                       startActivityForResult(chooserIntent, RESULT_LOAD_IMAGE);

                }

                break;
        }
    }
    @Override
    public void onStop() {
        Ion.getDefault(getActivity()).cancelAll(getActivity());
        super.onStop();

    }


    public boolean validate() {
        boolean valid = true;

        password = password_my_account.getText().toString();
        ferstName = ferstName_my_account.getText().toString();
        lastName = lastName_my_account.getText().toString();
        nickname = fakeNames_my_account.getText().toString();

        if (ferstName.equalsIgnoreCase("")) {
            ferstName_my_account.setError("آدخل الاسم الاول");
            valid = false;
        } else {
            ferstName_my_account.setError(null);
        }

        if (lastName.equalsIgnoreCase("")) {
            lastName_my_account.setError("آدخل الاسم الثاني");
            valid = false;
        } else {
            lastName_my_account.setError(null);
        }


        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            password_my_account.setError("آكبر من ٤ احرف او اقل من ١٠ احرف");
            valid = false;
        } else {
            password_my_account.setError(null);
        }


        return valid;
    }


    public void getprofil() {
        requests.StringRequest(Request.Method.GET, "profile", "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                Log.d("", result);
                JSONObject object = null;
                JSONObject message = null;
                JSONObject userInfo = null;

                try {
                    Log.d("", result);
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        userInfo = message.getJSONObject("user");


                        user = gson.fromJson(userInfo.toString(), User.class);

                        if (user.getAttachment().size() > 0) {
                            Picasso.with(getContext()).load(Links.UrlImg + user.getAttachment().get(0).getPath()).fit()
                                    .placeholder(R.drawable.ic_defult_profile_pic).centerCrop().into(profile_img_my_account);
                        }
                        ferstName_my_account.setText(user.getFirst_name());
                        lastName_my_account.setText(user.getLast_name());
                        fakeNames_my_account.setText(user.getNick_name());
                        email_my_account.setText(user.getEmail());

                    } else {
                        // Toast.makeText(getActivity(), "" + object.getString("email"), Toast.LENGTH_LONG).show();
                        Snackbar.make(getActivity().getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }

}
