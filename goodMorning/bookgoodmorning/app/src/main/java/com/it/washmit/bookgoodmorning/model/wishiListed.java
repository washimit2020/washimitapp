package com.it.washmit.bookgoodmorning.model;

/**
 * Created by washm on 4/3/17.
 */

public class wishiListed {

    private int id;
    private String titel;

    public wishiListed(int id, String titel) {
        this.id = id;
        this.titel = titel;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
}
