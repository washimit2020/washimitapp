//
//  ProfileProduct_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

enum ProductActions:Int {
    case share = 1
    case edit = 2
    case hide = 3
    case delete = 4
}

class ProfileProduct_CollectionViewCell: WishlistCell_CollectionViewCell {
    let label_tag = 1
    let image_tag = 2
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var price: UILabel!
    var delegate : ProfileProductDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.productImage.layer.cornerRadius = 0
        layer.borderColor = Constants.themeColor.cgColor
        layer.borderWidth = 0.5
        
        
        
    }
    override func updateView() {
        super.updateView()
        price.text = set_price(product!.price)
        
        if isSelected{
            frameView.alpha = 1
        }else{
            frameView.alpha = 0
            is_hidden()
        }
    
    }
    
    func is_hidden(){
        if product?.status_id == Constants.STATUS_HIDDEN{
            self.frameView.alpha = 1
            self.frameView.viewWithTag(image_tag)?.isHidden = true
            self.frameView.viewWithTag(label_tag)?.isHidden = false
        }
    }
    
    func shareProduct()  {
        delegate.make_action(self, .share)
    }
    
    func editProduct() {
        delegate.make_action(self, .edit)
    }
    
    func hideProduct()  {
        delegate.make_action(self, .hide)
    }
    
    func deleteProduct()  {
        delegate.make_action(self, .delete)
    }
}
protocol ProfileProductDelegate {
    func make_action(_ cell:ProfileProduct_CollectionViewCell,_ action:ProductActions)
}
