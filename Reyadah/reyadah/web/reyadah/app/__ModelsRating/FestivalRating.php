<?php

namespace App\__ModelsRating;

use App\__ModelsFestival\Festival;
use App\User;
use Illuminate\Database\Eloquent\Model;

class FestivalRating extends Model
{
    protected $fillable=['user_id','festival_id','view_rating','products_rating','users_rating','prices_rating'];

    protected $hidden=['user_id','related_id','id'];

    /**
     * to get the place
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function festival(){
        return $this->belongsTo(Festival::class,'festival_id');
    }

    /**
     * to get the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
