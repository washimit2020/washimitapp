package com.washimit.it.ryadah.Fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.cleveroad.pulltorefresh.firework.FireworkyPullToRefreshLayout;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.adapter.HomeParallaxAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener, FireworkyPullToRefreshLayout.OnRefreshListener {

    @BindView(R.id.totrual_img)
    ImageView totrual_img;
    @BindView(R.id.no_data)
    TextViewCast no_data;
    Intent intent;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LAYOUT_MANAGER = "setLayoutManager";
    private static final String ARG_TYPE = "userType";
    private static final String ARG_HAS_HIDER = "hasHider";

    // TODO: Rename and change types of parameters
    private boolean hasHider;
    private int setLayoutManager;
    private int userType;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * @param setLayoutManager
     * @return
     */
    public static HomeFragment newInstance(int setLayoutManager, boolean hasHider, int userType) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_MANAGER, setLayoutManager);
        args.putInt(ARG_TYPE, userType);
        args.putBoolean(ARG_HAS_HIDER, hasHider);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            hasHider = getArguments().getBoolean(ARG_HAS_HIDER);
            setLayoutManager = getArguments().getInt(ARG_LAYOUT_MANAGER);
            userType = getArguments().getInt(ARG_TYPE);
        }
    }

    HomeParallaxAdapter adapter;
    ArrayList<Object> objects;
    ArrayList<Product> productList;
    ArrayList<User> users;
    RecyclerView place_rv;
    LinearLayout linearLayout_find, linearLayout_no_internt;
    ButtonView retry;
    ImageView find;
    ApiRequest request;
    Gson gson;
    FireworkyPullToRefreshLayout swipeRefreshLayout;
    SharedPref pref;
    String urls;

    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        gson = new Gson();
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);
        View hider = LayoutInflater.from(getContext()).inflate(R.layout.item_place_header, container, false);


        request = new ApiRequest(getActivity());
        objects = new ArrayList<>();
        productList = new ArrayList<>();
        users = new ArrayList<>();
        pref = new SharedPref(getContext());
        place_rv = (RecyclerView) view.findViewById(R.id.place_rv);
        linearLayout_find = (LinearLayout) view.findViewById(R.id.linearLayout_find);
        linearLayout_no_internt = (LinearLayout) view.findViewById(R.id.linearLayout_no_internt);
        retry = (ButtonView) view.findViewById(R.id.retry);
        find = (ImageView) view.findViewById(R.id.find);
        swipeRefreshLayout = (FireworkyPullToRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        place_rv.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    if (hasHider) {
                        return 2;
                    }
                }
                if (setLayoutManager == 2) {
                    return 1;
                } else {
                    return 2;
                }
            }
        });

        place_rv.setLayoutManager(gridLayoutManager);

        adapter = new HomeParallaxAdapter(objects, getActivity(), userType, place_rv);


        if (hasHider) {


            adapter.setParallaxHeader(hider, place_rv);
        }


        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                objects.add(null);
                adapter.notifyItemInserted(objects.size() - 1);
                getNext(urls);
            }
        });


        place_rv.setAdapter(adapter);


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                updateUi(userType);
            }
        });

        find.setOnClickListener(this);
        retry.setOnClickListener(this);
        totrual_img.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.find:

                linearLayout_find.setVisibility(View.GONE);
                place_rv.setVisibility(View.VISIBLE);
                ((MainActivity) getActivity()).openFragment(new ExplorerFragment());
                break;
            case R.id.retry:
                swipeRefreshLayout.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                updateUi(userType);
                break;
            case R.id.totrual_img:
                totrual_img.setVisibility(View.GONE);
                place_rv.setVisibility(View.VISIBLE);
                break;

            case R.id.add_festival:
                if (pref.getUser() != null) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    getActivity().startActivity(intent);
                } else {

                    AapHalper.CreateDialog(R.layout.dialog_sgin_up, getActivity(), new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            ButtonView signUp, close;
                            signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                            close = (ButtonView) dialog.findViewById(R.id.close);

                            signUp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getActivity().finish();
                                }
                            });
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                }
                            });
                        }
                    });

                }
                break;


        }
    }


    private void updateUi(int type) {

        switch (type) {




            case Constant._PAGE_SHOW:
                request(Constant._PAGE_SHOW);

                break;

            case Constant._PAGE_STORE:
                request(Constant._PAGE_STORE);

                break;

            case Constant._PAGE_PRODUCT:
                if (pref.getUser() != null) {
                    request(Constant._PAGE_PRODUCT);
                    if (pref.getfirstTimeHome()) {
                        totrual_img.setVisibility(View.VISIBLE);
                        place_rv.setVisibility(View.GONE);
                        pref.SetfirstTimeHome();

                    }
                } else {
                    swipeRefreshLayout.setRefreshing(false);

                    AapHalper.CreateDialog(R.layout.dialog_sgin_up, getActivity(), new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            ButtonView signUp, close;
                            signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                            close = (ButtonView) dialog.findViewById(R.id.close);

                            signUp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    getActivity().finish();
                                }
                            });
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                }
                            });
                        }
                    });

                }


                break;


        }


    }


    public void request(final int type) {
        String url = "";
        if (type != Constant._PAGE_PRODUCT) {
            url = "users" + "/" + type;
        } else {
            url = "product";
        }

        request.MultipartRequest(Request.Method.GET, false, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                JSONObject result = null;
                JSONObject msg = null;
                JSONObject user = null;
                JSONObject products = null;
                objects.clear();
                productList.clear();
                users.clear();

                Log.d("result", result_);
                result = new JSONObject(result_);

                msg = result.getJSONObject("msg");
                if (type != Constant._PAGE_PRODUCT) {


                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    adapter.setUrl(urls);
                    users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());
                    if (users.size() > 0) {
                        objects.addAll(users);
                        adapter.notifyDataSetChanged();
                        place_rv.setVisibility(View.VISIBLE);
                        no_data.setVisibility(View.GONE);

                    } else {
                        no_data.setVisibility(View.VISIBLE);

                    }
                    swipeRefreshLayout.setRefreshing(false);



                } else {
                    products = msg.getJSONObject("products");
                    urls = products.getString("next_page_url");
                    adapter.setUrl(urls);
                    productList = gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType());
                    if (productList.size() > 0) {
                        objects.addAll(productList);
                        adapter.notifyDataSetChanged();
                        place_rv.setVisibility(View.VISIBLE);
                        linearLayout_find.setVisibility(View.GONE);

                    } else {
                        linearLayout_find.setVisibility(View.VISIBLE);
                        place_rv.setVisibility(View.GONE);
                    }
                    swipeRefreshLayout.setRefreshing(false);

                }

                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);
                Log.d("errorMessage", errorMessage);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                place_rv.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    public void getNext(String url) {


        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                objects.remove(objects.size() - 1);
                adapter.notifyItemRemoved(objects.size() - 1);
                JSONObject result = null;
                JSONObject msg = null;
                JSONObject user = null;
                JSONObject products = null;

                Log.d("result_nuxt", result_);
                result = new JSONObject(result_);

                msg = result.getJSONObject("msg");
                if (userType != Constant._PAGE_PRODUCT) {


                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    adapter.setUrl(urls);
                    ArrayList<User> users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());

                    objects.addAll(users);
                    adapter.notifyDataSetChanged();

                    swipeRefreshLayout.setRefreshing(false);


                } else {

                    products = msg.getJSONObject("products");
                    urls = products.getString("next_page_url");
                    adapter.setUrl(urls);
                    ArrayList<Product> productList = gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType());
                    objects.addAll(productList);
                    adapter.notifyDataSetChanged();
                    place_rv.setVisibility(View.VISIBLE);
                    linearLayout_find.setVisibility(View.GONE);

                    swipeRefreshLayout.setRefreshing(false);

                }
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);

                swipeRefreshLayout.setRefreshing(false);
                adapter.setLoading();

            }
        });
    }

    @Override
    public void onRefresh() {
        updateUi(userType);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Ion.getDefault(getActivity()).cancelAll(getActivity());

    }


}
