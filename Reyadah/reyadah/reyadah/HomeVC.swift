//
//  HomeVC.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {
    
    let login_segue = "login"

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: get_font_name()+"-Plain", size: 16)!,NSForegroundColorAttributeName:Constants.themeColor]
        self.navigationController?.navigationBar.tintColor = Constants.themeColor
    }
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton, segue.identifier == login_segue {
            UserDefaults.standard.set( button.tag ,forKey: "user_type_id")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func proceedToLogin(_ sender: UIButton) {
        performSegue(withIdentifier: login_segue, sender: sender)
    }
    @IBAction func goToMain(_ sender: Any) {
        make_sidebar()
    }
    
}
