package com.it.washim.wojooohapp.Interfases;

import com.it.washim.wojooohapp.Model.BookRequest;

import java.util.ArrayList;

/**
 * Created by mohammad on 9/12/2017.
 */

public interface OnBooksCollBack {
   void onSend( ArrayList<BookRequest> list);


}
