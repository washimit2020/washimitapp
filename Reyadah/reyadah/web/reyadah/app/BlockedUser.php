<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockedUser extends Model
{

    protected $fillable = ['user_id', 'related_id'];
    protected $hidden = ['user_id', 'related_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function blocked()
    {
        return $this->belongsTo(User::class, 'related_id');
    }
}
