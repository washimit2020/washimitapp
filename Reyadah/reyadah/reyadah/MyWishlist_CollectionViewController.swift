//
//  MyWishlist_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/14/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

class MyWishlist_CollectionViewController: ItemsLoader_CollectionViewController {

    let reuseIdentifier = "wishlist"
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        delegate = self

        link = RequestLinks.wishlist.rawValue
        beginWithModel = "wishlisted"
        self.collectionView!.register(UINib(nibName: "WishlistCell_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)

        clearData()
        self.emptyLabel?.frame.origin.y = collectionView!.frame.height/4
    }

   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

   
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! WishlistCell_CollectionViewCell
    
        cell.product = items[indexPath.row] as? Product
        let tap = UITapGestureRecognizer(target: self, action: #selector(removeFromWishlist(_:)))
        cell.wishlist.addGestureRecognizer(tap)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView!.frame.width - 8)/2, height: 160)
    }

}
extension MyWishlist_CollectionViewController:ItemsLoaderDelegate{
    func dataFiller(data: [JSON]) {
        for item in data{
            self.items.append( Product(item))
        }
    }
    
    func removeFromWishlist(_ sender:UITapGestureRecognizer){
        let point = sender.location(in: collectionView)
        sender.isEnabled = false
        let index = collectionView!.indexPathForItem(at: point)
        let cell = collectionView!.cellForItem(at: index!) as! WishlistCell_CollectionViewCell
        self.apiRequest.call_wishlist_like_api(link: RequestLinks.wishlist.rawValue, product: cell.product!)

        collectionView?.performBatchUpdates({
            self.items.remove(at:  index!.row)
            self.collectionView?.deleteItems(at: [index!])
        }, completion: {
            if($0 && self.items.count == 0){
                self.showEmpty()
            }
        })

        
    }
}
