//
//  Page_ViewController.swift
//  al-haj
//
//  Created by WASHM on 7/24/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite
class Page_ViewController: UIViewController {
    
    @IBOutlet weak var imagesScroll: UIScrollView!
    @IBOutlet weak var pageTitle: UILabel!
    @IBOutlet weak var pageBody: UITextView!
    @IBOutlet weak var buttons: UIStackView!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet var refsView: UIView!
    @IBOutlet weak var refsLabel: UILabel!
    @IBOutlet weak var favButton: UIBarButtonItem!
    var colors:[UIColor] = [.red , .yellow , .cyan , .white]
    
    var page:Page!{
        didSet{
            attributedBody = NSMutableAttributedString(string: page.body!)
        }
    }
    
    var attributedBody:NSMutableAttributedString?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favButton.action = #selector(addToFav)
        attributedBody!.addAttribute(NSFontAttributeName, value: pageBody.font!, range: NSRange(location: 0, length: attributedBody!.length))
        attributedBody?.changeToQuranFont()
        
        if let title = page?.title{
            pageTitle.isHidden = false
            pageTitle.text = title
        }else{
            pageTitle.isHidden = true
        }
        
        if let sub = page?.subTitle{
            subTitle.isHidden = false
            subTitle.text = sub
        }else{
            subTitle.isHidden = true
        }
        refsLabel.text = page?.references

        setups()
    }
    
   
    
    @IBAction func add_comment(_ sender: Any) {
        let comment = Comment_ViewController()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        comment.preferredContentSize.height = self.view.frame.height / 2
        comment.page = page
        comment.updatePage = {
            self.page = $0
        }
        alert.setValue(comment, forKeyPath: "contentViewController")
        present(alert, animated: true, completion: nil)
    }
    
    public func highlight(_ sender: UIButton) {
        guard let attributed = attributedBody , pageBody.selectedRange.length > 0 else {
            return
        }
        attributed.addAttribute(NSBackgroundColorAttributeName, value: sender.backgroundColor!, range: pageBody.selectedRange)
        do {
            let highlighted = Utils.shared.highlights_t.filter(
                Expressions.page_id == page.id! &&
                    Expressions.start >= pageBody.selectedRange.location &&
                    Expressions.end <= pageBody.selectedRange.length
            )
            try Utils.shared.db!.run(highlighted.delete())
            
            if sender.backgroundColor != .white {
                try Utils.shared.db!.run(Utils.shared.highlights_t.insert(
                    Expressions.page_id <- page.id!,
                    Expressions.start <- pageBody.selectedRange.location,
                    Expressions.end <- pageBody.selectedRange.length,
                    Expressions.color <- sender.tag
                ))
            }
            
        }catch(let exp){
            print("highlighted exp: \(exp)")
        }
        
        pageBody.attributedText = attributed
        pageBody.textAlignment = .right
    }
    
    public func addToFav(){
        page.is_wishlisted = page.is_wishlisted == 0 ? 1 : 0
        change_icon( )
        
        do {
            let table = Utils.shared.pages_t.filter(Expressions.id == page.id!)
            try Utils.shared.db!.run(table.update(Expressions.is_wishlisted <- page.is_wishlisted))
            
        }catch(let exp){
            print("update fav exp: \(exp)")
        }
    }
    
    func setups(){
        setup_textView()
        setup_buttons()
        setup_images()
        check_if_highlighted()
        change_icon()
        
    }
    
    func setup_images(){
        do{
            
            let images = try Utils.shared.db!.prepare(Utils.shared.images_t.filter(Expressions.page_id == page.id!)).map({
                return Image($0)
            })
            
            if images.count == 0 {
               imagesScroll.isHidden = true
                return
            }else{
                for (key,image) in images.enumerated(){
                    var multiplier = CGFloat(key)
                    var contentMultiplier = multiplier + 1
                    if(images.count == 0){
                        multiplier += 1
                        contentMultiplier -= 1
                    }
                    let xPos = 190 *  multiplier
                    let view  = UIImageView(frame: .init(x: xPos, y: 0, width: 200, height: 200))
                    view.image = image.get_uiimage()
                    view.contentMode = .scaleAspectFill
                    view.clipsToBounds = true
                    imagesScroll.contentSize.width = 200 * contentMultiplier
                    imagesScroll.addSubview(view)
                }
                
                if images.count > 1{
                    imagesScroll.setContentOffset(.init(x: imagesScroll.contentSize.width - imagesScroll.frame.width , y: 0), animated: false)
                }
            }
        }catch(let exp){
            print("images \(exp)")
        }
        
    }
    
    func check_if_highlighted(){
        guard let attributed = attributedBody  else {
            return
        }
        do {
            let table = Utils.shared.highlights_t.filter(
                Expressions.page_id == page.id!
            )
            let highlighted = try Array( Utils.shared.db!.prepare(table))
            for highlight in highlighted{
                let color = colors[highlight[Expressions.color]]
                let range = NSRange(location: highlight[Expressions.start], length: highlight[Expressions.end])
                attributed.addAttribute(NSBackgroundColorAttributeName, value: color, range: range)
                pageBody.attributedText = attributed
                pageBody.textAlignment = .right
            }
        }catch(let exp){
            print("highlighted selecte exp: \(exp)")
        }
    }
    
    
    func setup_buttons(){
        for (key,color) in colors.enumerated(){
            let button = UIButton()
            button.addConstraint(.init(item: button, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
            button.addConstraint(.init(item: button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40))
            button.layer.cornerRadius = 20
            button.layer.shadowColor = UIColor.black.cgColor;
            button.layer.shadowOpacity = 0.8;
            button.layer.shadowRadius = 4;
            button.layer.shadowOffset = CGSize(width: 3, height: 3);
            button.addTarget(self, action: #selector(highlight(_:)), for: .touchUpInside)
            button.backgroundColor = color
            button.tag = key
            buttons.addArrangedSubview(button)
        }
    }
    
    func change_icon(){
        if page.is_wishlisted == 1{
            favButton.image = #imageLiteral(resourceName: "fill")
//            print("liked")
        }else{
            favButton.image = #imageLiteral(resourceName: "stroke")
//            print("unliked")
        }
    }
    
    
}
extension Page_ViewController:UITextViewDelegate{
    
    func setup_textView(){
        pageBody.delegate = self
        pageBody.minimumZoomScale = 1
        pageBody.maximumZoomScale = 15
        pageBody.attributedText = attributedBody
        pageBody.textAlignment = .right
        refsView.frame.size.height = 20
        
        guard let text = refsLabel.text else{
            return
        }
        
        let height = text.height(withConstrainedWidth: refsView.frame.width, font: refsLabel.font) * 1.5
        refsView.frame.size.height = height

        refsView.frame.origin.y = pageBody.contentSize.height
        refsView.frame.size.width = pageBody.frame.width
        pageBody.addSubview(refsView)
        
        pageBody.contentInset.bottom += height

        let view = UIView(frame: pageBody.frame)
        view.frame.size.height += 100
        view.frame.origin.y = 0
        view.backgroundColor = .clear
        view.tag = 1
        for views in pageBody.subviews{
            view.addSubview(views)
        }
        pageBody.addSubview(view)
        
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        let length = textView.selectedRange.length
        if length <= 0 {
            buttons.isHidden = true
            return
        }
        buttons.isHidden = false
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return pageBody.viewWithTag(1)
    }
}
