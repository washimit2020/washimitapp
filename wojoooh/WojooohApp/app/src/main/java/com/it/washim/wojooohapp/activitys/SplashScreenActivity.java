package com.it.washim.wojooohapp.activitys;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.Halper.SharedPref;
import com.it.washim.wojooohapp.Interfases.OnRequestPublishCollBack;
import com.it.washim.wojooohapp.R;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends BaseActivity {
    CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        if (SharedPref.getInstance(getApplicationContext()).getCategories() == null) {
            getCategories();

        }
        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                openActivity(getApplicationContext(), HomeActivity.class);
                finish();
            }
        }.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();


    }


    public void getCategories() {

        getRequest().requestString(Request.Method.GET, "categories", null, new OnRequestPublishCollBack() {
            @Override
            public void onResponse(String response) {
                JSONObject object;
                try {
                    object = new JSONObject(response);
                    SharedPref.getInstance(getApplicationContext()).setCategories(object.getString("data"));
                    Log.d("categories", "add :)");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

    }


}
