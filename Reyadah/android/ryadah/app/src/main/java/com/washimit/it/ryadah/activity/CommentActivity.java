package com.washimit.it.ryadah.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.CommentPageAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {
    @BindView(R.id.recycler_comment)
    RecyclerView recycler_comment;
    @BindView(R.id.add_comment_text)
    EditTextView add_comment_text;
    @BindView(R.id.add_comment)
    ImageView add_comment;
    @BindView(R.id.swipe_refresh_comment)
    SwipeRefreshLayout swipe_refresh_comment;

    CommentPageAdapter adapter;
    ArrayList<Comment> comment;

    int productOrFestivalId;
    Gson gson;
    ApiRequest request;
    LinearLayoutManager linearLayoutManager;
    ArrayList<Part> parts;
    String type;
    SharedPref pref;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        Bundle bundle = getIntent().getExtras();
        type = bundle.getString("type");
        productOrFestivalId = bundle.getInt("Id");

        ButterKnife.bind(this);
        gson = new Gson();
        request = new ApiRequest(this);
        parts = new ArrayList<Part>();
        add_comment_text.setVisibility(View.VISIBLE);
        pref = new SharedPref(this);
        user = pref.getUser();
        comment = new ArrayList<>();

        adapter = new CommentPageAdapter(comment, this);

        recycler_comment.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(this);
        recycler_comment.setLayoutManager(linearLayoutManager);
        recycler_comment.setAdapter(adapter);

        add_comment.setOnClickListener(this);
        swipe_refresh_comment.setOnRefreshListener(this);


        swipe_refresh_comment.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh_comment.setRefreshing(true);
                getComment();

            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_comment:
                if (!add_comment_text.getText().toString().equalsIgnoreCase("")) {
                    add_comment.setClickable(false);

                    addComment();

                } else {

                    Toast.makeText(getApplicationContext(), R.string.add_comment, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


    private void addComment() {

        parts.add(new StringPart("type", type));
        parts.add(new StringPart("body", add_comment_text.getText().toString()));
        parts.add(new StringPart(type.equalsIgnoreCase("products") ? "product_id" : "festival_id", productOrFestivalId + ""));
        request.MultipartRequest("POST", "comment", null, parts, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject object;
                JSONObject msg, commentOp;
                Log.d("comment", result);
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                commentOp = msg.getJSONObject("comment");

                comment.add(new Comment(add_comment_text.getText().toString(), commentOp.getInt("user_id"), commentOp.getInt("commentable_id"), commentOp.getString("commentable_type")
                        , commentOp.getString("updated_at"), commentOp.getString("created_at"), 1, user));
                adapter.notifyItemInserted(comment.size());
                linearLayoutManager.scrollToPosition(adapter.getItemCount() - 1);
                add_comment_text.setText("");
                add_comment.setClickable(true);


            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("comment", errorMessage);
                add_comment.setClickable(true);
                Toast.makeText(getApplicationContext(), "حدث خطأ ما يرجى المحاولة مره اخرى", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void getComment() {

        request.MultipartRequest(Request.Method.GET, false, "comment/" + type + "/" + productOrFestivalId, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                comment.clear();
                JSONObject object;
                JSONObject msg;
                JSONObject comments;
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                comments = msg.getJSONObject("comments");

                String url = comments.getString("next_page_url");

                adapter.setNextUrl(url);

                comment.addAll(AapHalper.get_comment_response(comments.getJSONArray("data")));
                adapter.notifyDataSetChanged();
                linearLayoutManager.scrollToPosition(adapter.getItemCount() - 1);

                if (!url.equalsIgnoreCase("null")) {
                    comment.add(0, null);
                    adapter.notifyItemInserted(0);
                }


                swipe_refresh_comment.setRefreshing(false);
            }

            @Override
            public void onRequestError(String errorMessage) {
                swipe_refresh_comment.setRefreshing(false);

            }
        });


    }

    @Override
    public void onRefresh() {
        getComment();
    }
}
