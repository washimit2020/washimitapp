package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.FestivalDetailsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.SliderViewItem;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class FestivalsParallaxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private float mScrollMultiplier = 0.5f;
    private final int VIEW_PROG = 0;
    public final int HEADER = 2;
    public final int Festivals = 3;


    private int usersType;
    List<?> list;
    Activity context;
    ApiRequest request;
    private CustomRelativeWrapper mHeader;
    private RecyclerView mRecyclerView;
    private boolean mShouldClipView = true;
    private String place;
    Gson gson;
    SharedPref pref;
    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;
    Intent intent;

    public FestivalsParallaxAdapter(List<?> list, Activity context, int usersType, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        this.usersType = usersType;
        request = new ApiRequest(context);
        place = AapHalper.userNameType(usersType, context);
        pref = new SharedPref(context);


        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            Log.d("ok", "ok");
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {


        if (viewHolder instanceof PlaceHeaderHolder) {
            ((PlaceHeaderHolder) viewHolder).mDemoSlider.removeAllSliders();

            final Festivals festivals = (Festivals) list.get(i);

            ((PlaceHeaderHolder) viewHolder).item_place_follow.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(festivals.getFollowed())));

            ((PlaceHeaderHolder) viewHolder).item_place_rating.setRating((float) festivals.getRating());


            for (int img = 0; img < 3; img++) {
                SliderViewItem textSliderView = new SliderViewItem(context);
                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                        if (pref.getUser() != null) {
                            openActivity(FestivalDetailsActivity.class, usersType, gson.toJson(festivals));
                        } else {

                            AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                                @Override
                                public void dialog(final Dialog dialog) {
                                    ButtonView signUp, close;
                                    signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                    close = (ButtonView) dialog.findViewById(R.id.close);

                                    signUp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            context.finish();
                                        }
                                    });
                                    close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.cancel();
                                        }
                                    });
                                }
                            });
                        }

                    }
                });

                textSliderView.image(Constant._FESTIVAL_URL + festivals.getId() + "/" + img).setScaleType(BaseSliderView.ScaleType.CenterCrop);

                ((PlaceHeaderHolder) viewHolder).mDemoSlider.addSlider(textSliderView);
            }


            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.getPagerIndicator().setIndicatorStyleResource(R.drawable.ic_selected_indicator, R.drawable.ic_unselected_indicator);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setCustomAnimation(new DescriptionAnimation());

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setDuration(4000);

            ((PlaceHeaderHolder) viewHolder).item_place_name.setText(place + " " + festivals.getName());


        } else if (viewHolder instanceof FestivalHolder) {

            Festivals festivals = (Festivals) list.get(i);
            ((FestivalHolder) viewHolder).item_place_foul.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(festivals.getFollowed())));
            ((FestivalHolder) viewHolder).item_place_name.setText(place + " " + festivals.getName());
            ((FestivalHolder) viewHolder).item_place_rating.setRating((float) festivals.getRating());
            ((FestivalHolder) viewHolder).item_place_adress.setText(festivals.getSector_name() + "-" + festivals.getStreet_name());

            Picasso.with(context)
                    .load(Constant._FESTIVAL_URL + festivals.getId() + "/" + 0)
                    .fit()
                    .centerCrop()
                    .into(((FestivalHolder) viewHolder).item_place_img);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == HEADER && mHeader != null) {
            return new PlaceHeaderHolder(mHeader);
        } else if (i == Festivals) {
            return new FestivalHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_place, viewGroup, false));
        } else {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));

        }
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && hasHeader()) {
            Log.d("Festivals", "HEADER");
            return HEADER;


        } else if (list.get(position) instanceof Festivals) {
            Log.d("Festivals", "Festivals");

            return Festivals;

        } else if (list.get(position) == null) {
            return VIEW_PROG;
        }

        return VIEW_PROG;
    }


    private class PlaceHeaderHolder extends RecyclerView.ViewHolder {
        private SliderLayout mDemoSlider;
        ImageView item_place_follow, place_img_type;
        TextView item_place_name;
        RatingBar item_place_rating;

        public PlaceHeaderHolder(View itemView) {
            super(itemView);
            mDemoSlider = (SliderLayout) itemView.findViewById(R.id.slider);
            place_img_type = (ImageView) itemView.findViewById(R.id.place_img_type);
            item_place_follow = (ImageView) itemView.findViewById(R.id.item_place_foul);
            item_place_name = (TextView) itemView.findViewById(R.id.item_place_name);
            item_place_rating = (RatingBar) itemView.findViewById(R.id.item_place_rating);


            item_place_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {

                        Festivals festivals = (Festivals) list.get(getAdapterPosition());
                        if (usersType == Constant._TYPE_FESTIVAL) {
                            if (festivals.getFollowed() == 0) {
                                item_place_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unfollow));
                                festivals.setFollowed(1);
                                festivals.setTotal_followers(festivals.getTotal_followers() + 1);

                            } else {
                                item_place_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow));
                                festivals.setFollowed(0);
                                festivals.setTotal_followers(festivals.getTotal_followers() - 1);

                            }
                            follow(Constant._TYPE_FOLLOW_FESTIVAL, festivals.getId());
                        }
                    } else {
                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }
                }
            });


            place_img_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(usersType)));

        }
    }

    private class FestivalHolder extends RecyclerView.ViewHolder {
        ImageView item_place_img, item_place_foul, place_img_type;
        TextView item_place_name, item_place_adress;
        RatingBar item_place_rating;

        public FestivalHolder(View itemView) {
            super(itemView);
            item_place_img = (ImageView) itemView.findViewById(R.id.item_place_img);
            place_img_type = (ImageView) itemView.findViewById(R.id.place_img_type);
            item_place_foul = (ImageView) itemView.findViewById(R.id.item_place_foul);
            item_place_name = (TextView) itemView.findViewById(R.id.item_place_name);
            item_place_adress = (TextView) itemView.findViewById(R.id.item_place_adress);
            item_place_rating = (RatingBar) itemView.findViewById(R.id.item_place_rating);


            item_place_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {
                        openActivity(FestivalDetailsActivity.class, usersType, gson.toJson(list.get(getAdapterPosition())));
                    } else {

                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }


                }
            });


            item_place_foul.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {

                        Festivals festivals = (Festivals) list.get(getAdapterPosition());
                        if (usersType == Constant._TYPE_FESTIVAL) {
                            if (festivals.getFollowed() == 0) {
                                item_place_foul.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unfollow));
                                festivals.setFollowed(1);
                                festivals.setTotal_followers(festivals.getTotal_followers() + 1);

                            } else {
                                item_place_foul.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow));
                                festivals.setFollowed(0);
                                festivals.setTotal_followers(festivals.getTotal_followers() - 1);

                            }
                            follow(Constant._TYPE_FOLLOW_FESTIVAL, festivals.getId());
                        }
                    } else {
                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }
                }
            });


            place_img_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(usersType)));


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }


    /**
     * Translates the adapter in Y
     *
     * @param of offset in px
     */
    private void translateHeader(float of) {
        float ofCalculated = of * mScrollMultiplier;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && of < mHeader.getHeight()) {
            mHeader.setTranslationY(ofCalculated);
        } else if (of < mHeader.getHeight()) {
            TranslateAnimation anim = new TranslateAnimation(0, 0, ofCalculated, ofCalculated);
            anim.setFillAfter(true);
            anim.setDuration(0);
            mHeader.startAnimation(anim);
        }
        mHeader.setClipY(Math.round(ofCalculated));

    }

    /**
     * Set the view as header.
     *
     * @param header The inflated header
     * @param view   The RecyclerView to set scroll listeners
     */
    public void setParallaxHeader(View header, final RecyclerView view) {
        mRecyclerView = view;
        mHeader = new CustomRelativeWrapper(header.getContext(), mShouldClipView);
        mHeader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mHeader.addView(header, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mHeader != null) {
                    translateHeader(mRecyclerView.getLayoutManager().getChildAt(0) == mHeader ?
                            mRecyclerView.computeVerticalScrollOffset() : mHeader.getHeight());

                }
            }
        });
    }


    /**
     * @return true if there is list header on this adapter, false otherwise
     */
    private boolean hasHeader() {
        return mHeader != null;
    }


    class CustomRelativeWrapper extends RelativeLayout {

        private int mOffset;
        private boolean mShouldClip;

        public CustomRelativeWrapper(Context context, boolean shouldClick) {
            super(context);
            mShouldClip = shouldClick;
        }

        @Override
        protected void dispatchDraw(Canvas canvas) {
            if (mShouldClip) {
                canvas.clipRect(new Rect(getLeft(), getTop(), getRight(), getBottom() + mOffset));
            }
            super.dispatchDraw(canvas);
        }

        public void setClipY(int offset) {
            mOffset = offset;
            invalidate();
        }
    }


    private void follow(int followType, int festivalId) {


        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));
        parts.add(new StringPart("festival_id", festivalId + ""));
        AapHalper.userAction("follow", parts, context, null);
    }


    private void openActivity(Class activity, int userType, String data) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        intent.putExtra("data", data);
        context.startActivity(intent);

    }


    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}