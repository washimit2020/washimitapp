package com.it.washim.wojooohapp.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.CustemView.MyEditText;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Halper.SharedPref;
import com.it.washim.wojooohapp.Interfases.OnRequestPublishCollBack;
import com.it.washim.wojooohapp.Model.Categorie;
import com.it.washim.wojooohapp.R;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestToPublishBookFragment extends BaseFragment implements View.OnClickListener {
    /**
     * A simple {@link Fragment} subclass.
     */
    public RequestToPublishBookFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.name)
    MyEditText name;
    @BindView(R.id.email_address)
    MyEditText email_address;
    @BindView(R.id.phone_number)
    MyEditText phone_number;
    @BindView(R.id.target_group)
    MyEditText target_group;
    @BindView(R.id.book_title)
    MyEditText book_title;
    @BindView(R.id.about_book)
    MyEditText about_book;
    @BindView(R.id.sent)
    MyButtonView sent;
    @BindView(R.id.city)
    MyEditText city;
    @BindView(R.id.category)
    Spinner category;


    @BindView(R.id.r_book_publish)
    RadioGroup r_book_publish;
    @BindView(R.id.r_first_book)
    RadioGroup r_first_book;
    @BindView(R.id.r_publish_type)
    RadioGroup r_publish_type;
    @BindView(R.id.r_book_need_design)
    RadioGroup r_book_need_design;
    @BindView(R.id.r_book_has_images)
    RadioGroup r_book_has_images;
    View view;
    HashMap<String, String> map;

    ArrayAdapter arrayAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_request_to_publish_book, container, false);
        ButterKnife.bind(this, view);

        map = new HashMap<String, String>();
        sent.setOnClickListener(this);

//
//        arrayAdapter = new ArrayAdapter(getActivity(), R.layout.item_spiner, getCategory());
//        category.setAdapter(arrayAdapter);
        if (SharedPref.getInstance(getActivity()).getCategories() != null) {

            arrayAdapter = new ArrayAdapter<Categorie>(getActivity(), R.layout.item_spiner, SharedPref.getInstance(getActivity()).getCategories());
            category.setAdapter(arrayAdapter);

        }
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sent:
                if (validation()) {
                    showProgress("الرجاء الانتظار");
                    map.put("name", name.getText().toString());
                    map.put("email", email_address.getText().toString());
                    map.put("phone_number", phone_number.getText().toString());
                    map.put("city", city.getText().toString());
                    map.put("category", category.getSelectedItem().toString());
                    map.put("title", book_title.getText().toString());
                    map.put("content", about_book.getText().toString());
                    map.put("was_published", getRadioName(r_book_publish.getCheckedRadioButtonId()).equalsIgnoreCase("لا") ? "0" : "1");
                    map.put("is_first_book", getRadioName(r_first_book.getCheckedRadioButtonId()).equalsIgnoreCase("لا") ? "0" : "1");
                    map.put("publish_type", getRadioName(r_publish_type.getCheckedRadioButtonId()).equalsIgnoreCase("مطبوع") ? "0" : "1");
                    map.put("needs_design", getRadioName(r_book_need_design.getCheckedRadioButtonId()).equalsIgnoreCase("لا") ? "0" : "1");
                    map.put("has_images", getRadioName(r_book_has_images.getCheckedRadioButtonId()).equalsIgnoreCase("لا") ? "0" : "1");

                    getRequest().requestString(Request.Method.POST, "post/" + Constant.PAGES_PUBLISH_REQUEST, map, new OnRequestPublishCollBack() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sadfas", response);
                            name.setText("");
                            email_address.setText("");
                            phone_number.setText("");
                            book_title.setText("");
                            about_book.setText("");
                            sent.setText("");
                            name.requestFocus();
                            hideProgress();
                            Toast.makeText(getActivity(), "تم ارسال الكتاب سيتم التواصل معك في اقرب وقت ", Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("sadfas", "error error");
                            Toast.makeText(getActivity(), "يوجد مشكلة بالاتصال بالانترنت اعد المحاولة!", Toast.LENGTH_LONG).show();

                            hideProgress();
                        }
                    });

                }
                break;

        }
    }


    public boolean validation() {
        if (name.getText().toString().equalsIgnoreCase("")) {
            name.setError("قم بأدخال اسم صحيح");
            name.requestFocus();
            return false;

        }
        if (email_address.getText().toString().equalsIgnoreCase("") || !Patterns.EMAIL_ADDRESS.matcher(email_address.getText().toString()).matches()) {
            email_address.setError("قم بأدخال ايميل صحيح");
            email_address.requestFocus();
            return false;

        }
        if (phone_number.getText().toString().equalsIgnoreCase("") || phone_number.getText().toString().length() != 10) {
            phone_number.setError("قم بأدخال رقم هاتف صحيح");
            phone_number.requestFocus();
            return false;

        }
//        if (target_group.getText().toString().equalsIgnoreCase("")) {
//            target_group.setError("قم بأدخال الفئة المستهدفة");
//            target_group.requestFocus();
//            return false;
//
//        }
        if (book_title.getText().toString().equalsIgnoreCase("")) {
            book_title.setError("قم بأدخال عنوان الكتاب");
            book_title.requestFocus();
            return false;

        }
        if (about_book.getText().toString().equalsIgnoreCase("")) {
            about_book.setError("قم بأدخال نبذة عن الكتاب");
            about_book.requestFocus();
            return false;

        }
        return true;
    }


    private String getRadioName(int i) {
        return ((RadioButton) view.findViewById(i)).getText().toString();

    }
}
