//
//  Comment_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Comment_TableViewCell: UITableViewCell {

    @IBOutlet weak var userImage: BorderedImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var commentText: UILabel!
    var comment:Comment?{
        didSet{
            updateView()
        }
    }
   

    
    public func updateView(){
        commentText.text = comment!.body!
        
        guard let user = comment?.user else {
            return
        }
        
        name.text = user.name
        userImage.set_avatar_image(user_id: user.id)
       
    }
    
}
