//
//  Settings_TableViewController.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

class Settings_TableViewController: UITableViewController {
    @IBOutlet weak var mainSlider: UISlider!
    @IBOutlet weak var subSlider: UISlider!
    @IBOutlet weak var bodySlider: UISlider!
    @IBOutlet weak var fontType: UIPickerView!
    @IBOutlet weak var nightMode: UISwitch!
    let items = [
        "Lotus-Bold","Lotus-Light" , "Helvetica-light"
    ]
    @IBOutlet weak var main: UILabel!
    @IBOutlet weak var sub: UILabel!
    @IBOutlet weak var body: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        fontType.dataSource = self
        fontType.delegate = self
        mainSlider.addTarget(self, action: #selector(font_size_changer(sender:)), for: .valueChanged)
        subSlider.addTarget(self, action: #selector(font_size_changer(sender:)), for: .valueChanged)
        bodySlider.addTarget(self, action: #selector(font_size_changer(sender:)), for: .valueChanged)
        nightMode.addTarget(self, action: #selector(switchDarkMode(sender:)), for: .valueChanged)
        
        let sizes = Utils.shared.get_font_sizes()
        mainSlider.value = sizes[0]
        subSlider.value = sizes[1]
        bodySlider.value = sizes[2]
        
        nightMode.isOn = Utils.shared.is_dark_mode()
//        let index = items.index(where:{ $0.lowercased() == Utils.shared.get_font_name().lowercased()})!
//        fontType.selectRow(index, inComponent: 0, animated: false)
        
        changeViews()
        title = " "
    }
    
    @objc public func font_size_changer(sender:UISlider){
       Utils.shared.set_font_sizes(sizes: [mainSlider.value,subSlider.value,bodySlider.value])
        changeViews()
    }
    
    @objc public func switchDarkMode(sender:UISwitch){
        Utils.shared.set_dark_mode(on: sender.isOn)
        (parent as? MainTabbar_UIViewController)?.vm.viewControllers[1] .view.backgroundColor = Utils.shared.is_dark_mode() ? Utils.shared.dark_mode_background : Utils.shared.light_mode_background
        changeViews()
    }
    
    func changeViews()  {
        main.font = UIFont.titleFont?.withSize(CGFloat(mainSlider.value))
//        sub.font = UIFont(name: items[fontType.selectedRow(inComponent: 0)], size: CGFloat(subSlider.value))
//        body.font = UIFont(name: items[fontType.selectedRow(inComponent: 0)], size: CGFloat(bodySlider.value))
        sub.font = UIFont(name: items[1], size: CGFloat(subSlider.value))
        body.font = UIFont(name: items[1], size: CGFloat(bodySlider.value))
        let view = body.superview?.superview
        if nightMode.isOn{
            view?.backgroundColor = Utils.shared.dark_mode_background
            main.textColor = Utils.shared.dark_mode_header
            sub.textColor =  Utils.shared.dark_mode_sub
            body.textColor =  Utils.shared.dark_mode_font
        }else{
            view?.backgroundColor = Utils.shared.light_mode_background
            main.textColor = Utils.shared.light_mode_header
            sub.textColor =  Utils.shared.light_mode_sub
            body.textColor =  Utils.shared.light_mode_font
        }
        view?.sizeToFit()
        DispatchQueue.global().async {
            guard let nav = self.slideMenuController()?.mainViewController as? UINavigationController , let tab = nav.viewControllers.first as? MainTabbar_UIViewController , let vc = tab.vm.viewControllers[1] as? Book_PageViewController else {return}
                Utils.shared.pages.forEach({ (page) in
                page.inhanceBody()
            })
        }
    }
    
}
extension Settings_TableViewController:UIPickerViewDataSource,UIPickerViewDelegate{
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
   
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if view != nil {
            return view!
        }
        let label = UILabel(frame: pickerView.frame)
        label.text = "ابجد هوز الترقيم العربي"
        label.textAlignment = .center
        label.font = UIFont(name: items[row], size: 14)
        return label
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return items.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        Utils.shared.set_font_name(font: items[row])
        changeViews()
    }
}
