package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.android.volley.Request;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.FestivalInfoActivity;
import com.washimit.it.ryadah.activity.MapAndlListLocationActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.activity.MyProfileInfoActivity;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.model.Location;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.SliderViewItem;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsPageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final int DETAILS_HEADER = 5;
    public final int COMMENT = 6;
    public final int PRODUCT_DETAILS = 7;
    private final int VIEW_PROG = 1;


    List<?> list;
    Activity context;
    ApiRequest request;
    private View defaultHeader;
    private boolean mShouldClipView = true;
    Gson gson;
    OnLoadMoreListener loadMoreListener;
    ImageView imageView;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    String urls;
    OnLoadMoreListener onLoadMoreListener;
    public static Intent intent;

    public DetailsPageAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        request = new ApiRequest(context);
        imageView = new ImageView(context);
        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (urls != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !urls.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {


        if (viewHolder instanceof DetailsHeaderHolder) {
            ((DetailsHeaderHolder) viewHolder).item_details_header_slider.removeAllSliders();

            if (list.get(i) instanceof Festivals) {

                final Festivals festivals = (Festivals) list.get(i);
                //use to share user
                Picasso.with(context).load(Constant._AVATAR_URL + festivals.getId()).into(imageView);


                ((DetailsHeaderHolder) viewHolder).product_and_festeval_details.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).product_titel.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).store_and_show_details.setVisibility(View.VISIBLE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_like.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_favourite.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_follower_number.setText(String.valueOf(festivals.getTotal_followers()));


                ((DetailsHeaderHolder) viewHolder).item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isFollow(festivals.getFollowed())), null, null);


                for (int img = 0; img < 3; img++) {
                    SliderViewItem textSliderView = new SliderViewItem(context);


                    textSliderView.image(Constant._COVER_URL + festivals.getId() + "/" + img).setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    ((DetailsHeaderHolder) viewHolder).item_details_header_slider.addSlider(textSliderView);

                }


                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setCustomAnimation(new DescriptionAnimation());

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setDuration(4000);
                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getPagerIndicator().setIndicatorStyleResource(R.drawable.ic_selected_indicator, R.drawable.ic_unselected_indicator);
                //  ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getPagerIndicator().setDefaultSelectedIndicatorSize(5,5, PagerIndicator.Unit.DP);

                ((DetailsHeaderHolder) viewHolder).item_details_header_about_text.setText(festivals.getInfo());
                ((DetailsHeaderHolder) viewHolder).item_details_header_location_addres.setText(festivals.getSector_name() + "-" + festivals.getStreet_name());
                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setRating(((float) festivals.getRating()));


                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ratingFestival(festivals, ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar);
                        return false;
                    }
                });
                ((DetailsHeaderHolder) viewHolder).item_details_header_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", festivals.getOrganizer_phone_number() + "", null));
                        context.startActivity(intent);

                    }
                });
                ((DetailsHeaderHolder) viewHolder).item_details_header_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AapHalper.sher(context, ((SliderViewItem) ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getCurrentSlider()).getTarget(), festivals.getName());
                    }
                });

                ((DetailsHeaderHolder) viewHolder).item_details_header_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(context, MapsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        intent.putExtra("location", gson.toJson(new Location(new LatLng(festivals.getLat(), festivals.getLng()), Constant._PAGE_FESTIVAL), Location.class));
                        context.startActivity(intent);


                    }
                });


            } else if (list.get(i) instanceof User) {

                final User user = (User) list.get(i);
                //use to share user
                Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).into(imageView);

                ((DetailsHeaderHolder) viewHolder).product_and_festeval_details.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).store_and_show_details.setVisibility(View.VISIBLE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_like.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_favourite.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_follower_number.setText(String.valueOf(user.getFollowed_by_count()));


                ((DetailsHeaderHolder) viewHolder).item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isFollow(user.getFollowed())), null, null);


                for (int img = 0; img < 3; img++) {
                    SliderViewItem textSliderView = new SliderViewItem(context);


                    textSliderView.image(Constant._COVER_URL + user.getId() + "/" + img).setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    ((DetailsHeaderHolder) viewHolder).item_details_header_slider.addSlider(textSliderView);

                }


                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setCustomAnimation(new DescriptionAnimation());

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setDuration(4000);
                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getPagerIndicator().setIndicatorStyleResource(R.drawable.ic_selected_indicator, R.drawable.ic_unselected_indicator);
                //  ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getPagerIndicator().setDefaultSelectedIndicatorSize(5,5, PagerIndicator.Unit.DP);

                ((DetailsHeaderHolder) viewHolder).item_details_header_about_text.setText(user.getInfo());
                ((DetailsHeaderHolder) viewHolder).item_details_header_location_addres.setText(user.getSector_name() + "-" + user.getStreet_name());
                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setRating(((float) user.getRating()));


                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        ratingPlace(user, ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar);
                        return false;
                    }
                });
                ((DetailsHeaderHolder) viewHolder).item_details_header_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", user.getPhone_number(), null));
                        context.startActivity(intent);

                    }
                });
                ((DetailsHeaderHolder) viewHolder).item_details_header_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AapHalper.sher(context, ((SliderViewItem) ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getCurrentSlider()).getTarget(), user.getName());
                    }
                });

                ((DetailsHeaderHolder) viewHolder).item_details_header_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(context, MapsActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                        intent.putExtra("location", gson.toJson(new Location(new LatLng(user.getLat(), user.getLng()), user.getType().getId()), Location.class));
                        context.startActivity(intent);


                    }
                });


            } else {

                final Product product = (Product) list.get(i);
                //use to share product
                Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).into(imageView);


                ((DetailsHeaderHolder) viewHolder).store_and_show_details.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).product_and_festeval_details.setVisibility(View.VISIBLE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_about.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_phone.setVisibility(View.GONE);
                ((DetailsHeaderHolder) viewHolder).item_details_header_follower_number.setVisibility(View.GONE);

                for (int img = 0; img < 3; img++) {
                    SliderViewItem textSliderView = new SliderViewItem(context);


                    textSliderView.image(Constant._PRODUCT_URL + product.getId() + "/" + img).setScaleType(BaseSliderView.ScaleType.CenterCrop);

                    ((DetailsHeaderHolder) viewHolder).item_details_header_slider.addSlider(textSliderView);
                }

                ((DetailsHeaderHolder) viewHolder).item_details_header_like.setText(product.getTotal_likes() + "");

                ((DetailsHeaderHolder) viewHolder).item_details_header_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(product.getIs_liked())), null, null);

                ((DetailsHeaderHolder) viewHolder).item_details_header_favourite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(product.getIs_wishlisted())));

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setCustomAnimation(new DescriptionAnimation());

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.setDuration(4000);

                ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getPagerIndicator().setIndicatorStyleResource(R.drawable.ic_selected_indicator, R.drawable.ic_unselected_indicator);
                ((DetailsHeaderHolder) viewHolder).item_details_header_price.setText(String.valueOf(product.getPrice()));
                ((DetailsHeaderHolder) viewHolder).item_details_header_ingredients.setText(product.getIngredients());
                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setRating((float) product.getRating());
                ((DetailsHeaderHolder) viewHolder).item_details_header_group.setText(String.valueOf(product.getNumber_of_eaters()));
                ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        ratingProduct(product, ((DetailsHeaderHolder) viewHolder).item_details_header_RatingBar);
                        return false;

                    }
                });

                ((DetailsHeaderHolder) viewHolder).item_details_header_share.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AapHalper.sher(context, ((SliderViewItem) ((DetailsHeaderHolder) viewHolder).item_details_header_slider.getCurrentSlider()).getTarget(), product.getName());
                    }
                });
                ;

                ((DetailsHeaderHolder) viewHolder).item_details_header_location.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        intent = new Intent(context, MapsActivity.class);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                        intent.putExtra("location", gson.toJson(new Location(new LatLng(product.getUser().getLat(), product.getUser().getLng()), product.getUser().getType().getId()), Location.class));
//                        context.startActivity(intent);
                        intent = new Intent(context, MapAndlListLocationActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("id", product.getId());
                        intent.putExtra("user_type_id", product.getUser().getType().getId());
                        intent.putExtra("location", gson.toJson(new Location(new LatLng(product.getUser().getLat(), product.getUser().getLng()), product.getUser().getType().getId()), Location.class));
                        intent.putExtra("user", gson.toJson(product.getUser()));
                        context.startActivity(intent);
                    }
                });


            }


        } else if (viewHolder instanceof CommentHolder) {

            Comment comment = (Comment) list.get(i);

            ((CommentHolder) viewHolder).item_comment_name.setText(comment.getUser().getName());
            ((CommentHolder) viewHolder).item_comment_text.setText(comment.getBody());

            Picasso.with(context)
                    .load(Constant._AVATAR_URL + comment.getUser().getId())
                    .placeholder(R.drawable.ic_profile_picture)
                    .fit()
                    .centerCrop()
                    .into(((CommentHolder) viewHolder).item_comment_user_img);


        } else if (viewHolder instanceof ProductDetailsHolder) {

            Product productDetails = (Product) list.get(i);

            Picasso.with(context)
                    .load(Constant._PRODUCT_URL + productDetails.getId() + "/" + 0)
//                    .placeholder(R.drawable.ic_store)
                    .fit()
                    .centerCrop()
                    .into(((ProductDetailsHolder) viewHolder).item_product_details_img);


            ((ProductDetailsHolder) viewHolder).item_product_details_liks_number.setText(String.valueOf(productDetails.getTotal_likes()));
            ((ProductDetailsHolder) viewHolder).item_product_details_number_of_etars.setText(String.valueOf(productDetails.getNumber_of_eaters()));
            ((ProductDetailsHolder) viewHolder).item_product_details_name.setText(productDetails.getName());
            ((ProductDetailsHolder) viewHolder).item_product_details_prise.setText(String.valueOf(productDetails.getPrice()));
            ((ProductDetailsHolder) viewHolder).item_product_details_rating.setRating((float) productDetails.getRating());

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        Log.d("zxgzgzgz", i + "");
        if (i == DETAILS_HEADER && defaultHeader != null) {
            return new DetailsHeaderHolder(defaultHeader);
        }

        if (i == PRODUCT_DETAILS) {
            return new ProductDetailsHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product_details, viewGroup, false));
        }
        if (i == VIEW_PROG) {
            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));
        }
        if (i == COMMENT) {
            return new CommentHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comment, viewGroup, false));
        }
        return null;
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && defaultHeader != null) {
            return DETAILS_HEADER;
        } else if (list.get(position) instanceof Product) {
            return PRODUCT_DETAILS;
        } else if (list.get(position) instanceof Comment) {
            return COMMENT;
        }
        return VIEW_PROG;

    }

    private class DetailsHeaderHolder extends RecyclerView.ViewHolder {
        RelativeLayout product_and_festeval_details, store_and_show_details;
        SliderLayout item_details_header_slider;
        TextViewCast item_details_header_like, item_details_header_price, item_details_header_follower_number, product_titel,
                item_details_header_ingredients, item_details_header_group, item_details_header_location_addres, item_details_header_about_text;
        RatingBar item_details_header_RatingBar;
        ImageView item_details_header_favourite, item_details_header_location, item_details_header_share, item_details_header_about, item_details_header_phone;

        public DetailsHeaderHolder(View itemView) {
            super(itemView);

            product_and_festeval_details = (RelativeLayout) itemView.findViewById(R.id.product_and_festeval_details);
            store_and_show_details = (RelativeLayout) itemView.findViewById(R.id.store_and_show_details);

            item_details_header_slider = (SliderLayout) itemView.findViewById(R.id.item_details_header_slider);

            item_details_header_like = (TextViewCast) itemView.findViewById(R.id.item_details_header_like);
            product_titel = (TextViewCast) itemView.findViewById(R.id.product_titel);
            item_details_header_follower_number = (TextViewCast) itemView.findViewById(R.id.item_details_header_follower_number);
            item_details_header_price = (TextViewCast) itemView.findViewById(R.id.item_details_header_price);
            item_details_header_ingredients = (TextViewCast) itemView.findViewById(R.id.item_details_header_ingredients);
            item_details_header_group = (TextViewCast) itemView.findViewById(R.id.item_details_header_group);
            item_details_header_location_addres = (TextViewCast) itemView.findViewById(R.id.item_details_header_location_addres);
            item_details_header_about_text = (TextViewCast) itemView.findViewById(R.id.item_details_header_about_text);

            item_details_header_RatingBar = (RatingBar) itemView.findViewById(R.id.item_details_header_RatingBar);


            item_details_header_favourite = (ImageView) itemView.findViewById(R.id.item_details_header_favourite);
            item_details_header_location = (ImageView) itemView.findViewById(R.id.item_details_header_location);
            item_details_header_share = (ImageView) itemView.findViewById(R.id.item_details_header_share);
            item_details_header_about = (ImageView) itemView.findViewById(R.id.item_details_header_about);
            item_details_header_phone = (ImageView) itemView.findViewById(R.id.item_details_header_phone);


            item_details_header_about.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list.get(getAdapterPosition()) instanceof User) {

                        Intent intent = new Intent(context, MyProfileInfoActivity.class);
                        intent.putExtra("user", gson.toJson((User) list.get(getAdapterPosition())));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    } else {
                        Intent intent = new Intent(context, FestivalInfoActivity.class);
                        intent.putExtra("festivals", gson.toJson((Festivals) list.get(getAdapterPosition())));
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(intent);
                    }
                }
            });


            item_details_header_follower_number.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (list.get(getAdapterPosition()) instanceof User) {
                        User user = (User) list.get(getAdapterPosition());

                        if (user.getFollowed() == 0) {
                            item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(R.drawable.ic_unfollow), null, null);
                            user.setFollowed(1);
                            user.setFollowed_by_count(user.getFollowed_by_count() + 1);
                            item_details_header_follower_number.setText(String.valueOf(user.getFollowed_by_count()));


                        } else {
                            user.setFollowed(0);
                            user.setFollowed_by_count(user.getFollowed_by_count() - 1);
                            item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(R.drawable.ic_follow), null, null);
                            item_details_header_follower_number.setText(String.valueOf(user.getFollowed_by_count()));

                        }
                        follow(Constant._TYPE_FOLLOW_USER, user.getId());
                    } else {

                        Festivals festivals = (Festivals) list.get(getAdapterPosition());
                        if (festivals.getFollowed() == 0) {
                            item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(R.drawable.ic_unfollow), null, null);

                            festivals.setFollowed(1);
                            festivals.setTotal_followers(festivals.getTotal_followers() + 1);
                            item_details_header_follower_number.setText(String.valueOf(festivals.getTotal_followers()));


                        } else {
                            item_details_header_follower_number.setCompoundDrawablesWithIntrinsicBounds(null, context.getResources().getDrawable(R.drawable.ic_follow), null, null);
                            festivals.setFollowed(0);
                            festivals.setTotal_followers(festivals.getTotal_followers() - 1);
                            item_details_header_follower_number.setText(String.valueOf(festivals.getTotal_followers()));


                        }
                        follow(Constant._TYPE_FOLLOW_FESTIVAL, festivals.getId());
                    }


                }
            });

            item_details_header_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());

                    if (product.getIs_liked() == 0) {
                        item_details_header_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(1)), null, null);
                        item_details_header_like.setText((product.getTotal_likes() + 1) + "");
                        product.setIs_liked(1);
                        product.setTotal_likes(product.getTotal_likes() + 1);
                    } else {
                        item_details_header_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(0)), null, null);
                        item_details_header_like.setText((product.getTotal_likes() - 1) + "");
                        product.setIs_liked(0);
                        product.setTotal_likes(product.getTotal_likes() - 1);

                    }
                    like(product.getId());
                }
            });
            item_details_header_favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());

                    if (product.getIs_wishlisted() == 0) {
                        item_details_header_favourite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(1)));
                        product.setIs_wishlisted(1);
                    } else {
                        item_details_header_favourite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(0)));
                        product.setIs_wishlisted(0);
                    }

                    addToWishlisted(product.getId());
                }
            });


        }
    }

    public class CommentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_comment_name)
        TextViewCast item_comment_name;
        @BindView(R.id.item_comment_text)
        TextViewCast item_comment_text;
        @BindView(R.id.item_comment_user_img)
        ImageView item_comment_user_img;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public class ProductDetailsHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_product_details_img)
        ImageView item_product_details_img;
        @BindView(R.id.item_product_details_name)
        TextViewCast item_product_details_name;
        @BindView(R.id.item_product_details_number_of_etars)
        TextViewCast item_product_details_number_of_etars;
        @BindView(R.id.item_product_details_prise)
        TextViewCast item_product_details_prise;
        @BindView(R.id.item_product_details_liks_number)
        TextViewCast item_product_details_liks_number;
        @BindView(R.id.item_product_details_rating)
        RatingBar item_product_details_rating;

        public ProductDetailsHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProductDetailsActivity.class);
                    intent.putExtra("data", gson.toJson(list.get(getAdapterPosition())));
                    context.startActivity(intent);
                }
            });
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        //  public AVLoadingIndicatorView avi;

        public ProgressViewHolder(View v) {
            super(v);
        }

    }

    /**
     * @param header
     */
    public void setHeader(View header) {
        defaultHeader = header;

    }


    private void follow(int followType, int userId) {

        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));

        if (followType == Constant._TYPE_FOLLOW_FESTIVAL) {
            parts.add(new StringPart("festival_id", userId + ""));

        } else {
            parts.add(new StringPart("user_id", userId + ""));

        }
        AapHalper.userAction("follow", parts, context, null);
    }

    private void ratingUser(int userId, double products_rating, double users_rating, double prices_rating, double view_rating, RatingBar ratingBar) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("products_rating", products_rating + ""));
        parts.add(new StringPart("users_rating", users_rating + ""));
        parts.add(new StringPart("prices_rating", prices_rating + ""));
        parts.add(new StringPart("view_rating", view_rating + ""));
        parts.add(new StringPart("user_id", userId + ""));
        AapHalper.userAction("rating/user", parts, context, ratingBar);


    }

    private void ratingFestival(int festivalId, double products_rating, double users_rating, double prices_rating, double view_rating, RatingBar ratingBar) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("products_rating", products_rating + ""));
        parts.add(new StringPart("users_rating", users_rating + ""));
        parts.add(new StringPart("prices_rating", prices_rating + ""));
        parts.add(new StringPart("view_rating", view_rating + ""));
        parts.add(new StringPart("festival_id", festivalId + ""));
        AapHalper.userAction("rating/festival", parts, context, ratingBar);


    }

    private void ratingProducts(int product_id, double quality, double style, double ingredients, double price, RatingBar ratingBar) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("price", price + ""));
        parts.add(new StringPart("ingredients", ingredients + ""));
        parts.add(new StringPart("style", style + ""));
        parts.add(new StringPart("quality", quality + ""));
        parts.add(new StringPart("product_id", product_id + ""));
        AapHalper.userAction("rating/product", parts, context, ratingBar);


    }


    private void ratingPlace(final User user, final RatingBar item_details_header_RatingBar) {
        AapHalper.CreateDialog(R.layout.dialog_rating_place, context, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final RatingBar dialog_rating_view_rating, dialog_rating_products_rating, dialog_rating_users_rating, dialog_rating_prices_rating;
                ImageView imageView = (ImageView) dialog.findViewById(R.id.rating);
                dialog_rating_view_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_view_rating);
                dialog_rating_products_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_products_rating);
                dialog_rating_users_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_users_rating);
                dialog_rating_prices_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_prices_rating);


                if (user.getRated() != null) {
                    dialog_rating_products_rating.setRating(((float) user.getRated().getProducts_rating()));
                    dialog_rating_users_rating.setRating(((float) user.getRated().getUsers_rating()));
                    dialog_rating_prices_rating.setRating(((float) user.getRated().getPrices_rating()));
                    dialog_rating_view_rating.setRating(((float) user.getRated().getView_rating()));
                }


                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ratingUser(user.getId(), dialog_rating_products_rating.getRating(),
                                dialog_rating_users_rating.getRating(),
                                dialog_rating_prices_rating.getRating(),
                                dialog_rating_view_rating.getRating(), item_details_header_RatingBar);
                        if (user.getRated() != null) {
                            user.getRated().setProducts_rating((double) dialog_rating_products_rating.getRating());
                            user.getRated().setUsers_rating((double) dialog_rating_users_rating.getRating());
                            user.getRated().setPrices_rating((double) dialog_rating_prices_rating.getRating());
                            user.getRated().setView_rating((double) dialog_rating_view_rating.getRating());
                        }
                        dialog.dismiss();
                    }
                });

            }
        });
    }

    private void ratingFestival(final Festivals festivals, final RatingBar item_details_header_RatingBar) {
        AapHalper.CreateDialog(R.layout.dialog_rating_place, context, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final RatingBar dialog_rating_view_rating, dialog_rating_products_rating, dialog_rating_users_rating, dialog_rating_prices_rating;
                ImageView imageView = (ImageView) dialog.findViewById(R.id.rating);
                dialog_rating_view_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_view_rating);
                dialog_rating_products_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_products_rating);
                dialog_rating_users_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_users_rating);
                dialog_rating_prices_rating = (RatingBar) dialog.findViewById(R.id.dialog_rating_prices_rating);


                if (festivals.getRated() != null) {
                    dialog_rating_products_rating.setRating(((float) festivals.getRated().getProducts_rating()));
                    dialog_rating_users_rating.setRating(((float) festivals.getRated().getUsers_rating()));
                    dialog_rating_prices_rating.setRating(((float) festivals.getRated().getPrices_rating()));
                    dialog_rating_view_rating.setRating(((float) festivals.getRated().getView_rating()));
                }


                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ratingFestival(festivals.getId(), dialog_rating_products_rating.getRating(),
                                dialog_rating_users_rating.getRating(),
                                dialog_rating_prices_rating.getRating(),
                                dialog_rating_view_rating.getRating(), item_details_header_RatingBar);
                        if (festivals.getRated() != null) {
                            festivals.getRated().setProducts_rating((double) dialog_rating_products_rating.getRating());
                            festivals.getRated().setUsers_rating((double) dialog_rating_users_rating.getRating());
                            festivals.getRated().setPrices_rating((double) dialog_rating_prices_rating.getRating());
                            festivals.getRated().setView_rating((double) dialog_rating_view_rating.getRating());
                        }
                        dialog.dismiss();
                    }
                });

            }
        });
    }

    private void ratingProduct(final Product product, final RatingBar item_details_header_RatingBar) {


        AapHalper.CreateDialog(R.layout.dialog_rating_product, context, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final RatingBar dialog_rating_product_quality, dialog_rating_product_style, dialog_rating_product_ingredients, dialog_rating_product_pris;
                ImageView imageView = (ImageView) dialog.findViewById(R.id.rating);
                dialog_rating_product_quality = (RatingBar) dialog.findViewById(R.id.dialog_rating_product_quality);
                dialog_rating_product_style = (RatingBar) dialog.findViewById(R.id.dialog_rating_product_style);
                dialog_rating_product_ingredients = (RatingBar) dialog.findViewById(R.id.dialog_rating_product_ingredients);
                dialog_rating_product_pris = (RatingBar) dialog.findViewById(R.id.dialog_rating_product_pris);


                if (product.getRated() != null) {

                    dialog_rating_product_quality.setRating(((float) product.getRated().getQuality()));
                    dialog_rating_product_style.setRating(((float) product.getRated().getStyle()));
                    dialog_rating_product_ingredients.setRating(((float) product.getRated().getIngredients()));
                    dialog_rating_product_pris.setRating(((float) product.getRated().getPric()));

                }


                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        ratingProducts(product.getId(), dialog_rating_product_quality.getRating(),
                                dialog_rating_product_style.getRating(),
                                dialog_rating_product_ingredients.getRating(),
                                dialog_rating_product_pris.getRating(), item_details_header_RatingBar);

                        if (product.getRated() != null) {
                            product.getRated().setQuality((double) dialog_rating_product_quality.getRating());
                            product.getRated().setStyle((double) dialog_rating_product_style.getRating());
                            product.getRated().setIngredients((double) dialog_rating_product_ingredients.getRating());
                            product.getRated().setPric((double) dialog_rating_product_pris.getRating());
                        }
                        dialog.dismiss();
                    }
                });

            }
        });
    }


    private void like(int productId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product_id", productId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/like", hashMap, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("Like", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("Like", errorMessage);
            }
        });


    }

    private void addToWishlisted(int productId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product_id", productId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/wishlist", hashMap, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("wishlist", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("wishlist", errorMessage);
            }
        });


    }

    public void setNextUrls(String urls) {
        this.urls = urls;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    public void setLoaded() {
        isLoading = false;
    }
}