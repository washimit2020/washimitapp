//
//  PageController_ViewController.swift
//  al-haj
//
//  Created by WASHM on 1/17/18.
//  Copyright © 2018 WASHM. All rights reserved.
//

import UIKit

class PageController_ViewController: UIViewController {
    @IBOutlet weak var pageNumber: NumberLabel!
    @IBOutlet weak var titleLabel: TitleLabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var body: C_UITextView!
    @IBOutlet weak var reference: C_UILabel!
    @IBOutlet weak var dividerHeight: NSLayoutConstraint!
    @IBOutlet weak var dividerLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    @IBOutlet weak var divider: UILabel!
    var vm:PageVm?
    var page:Page?
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var container: UIView!
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        vm?.check_if_highlighted()
        setups()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        guard let page = page else {return}
        vm = PageVm(page: page, body: body )
        titleLabel.text = nil
        if let title = page.title {
            titleLabel.text = title
        }
        dividerHeight.constant = 0
        reference.text = nil
        if let ref = page.references {
            dividerHeight.constant = 1
            dividerLeftConstraint.constant = page.is_full_line  == 0 ? view.frame.width/2 : 0
            reference.text = ref
        }
        imageView.image = nil
        imageHeight.constant = 0
        if  page.image?.title != nil{
            imageView.image = page.image?.get_uiimage()
            imageHeight.constant = 250
        }
        pageNumber.text = "\(page.id!)"
        pageNumber.textAlignment = .center
        scrollView.minimumZoomScale = 1
        scrollView.maximumZoomScale = 7
        scrollView.delegate = self
    }
    func setups(){
        let sizes = Utils.shared.get_font_sizes()
        let fontName = Utils.shared.get_font_name()
        let nightMode = Utils.shared.is_dark_mode()
        let fontColor = nightMode ? Utils.shared.dark_mode_font : Utils.shared.light_mode_font
        titleLabel.font = UIFont(name: "TheSansArab-Plain", size: CGFloat(sizes[0]))
        reference.font = UIFont(name: fontName, size: CGFloat(sizes[2]))
        //        pageTitle.font = UIFont(name: fontName, size: CGFloat(sizes[0]))
        titleLabel.textColor = nightMode ? Utils.shared.dark_mode_header : Utils.shared.light_mode_header
        reference.textColor = fontColor
        divider.backgroundColor = fontColor
        if nightMode {
            vm?.colors[3] = .black
        }else{
            vm?.colors[3] = .white
        }
        vm?.setup_buttons(view: self.view)
    }
}
extension PageController_ViewController:UIScrollViewDelegate{
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return container
    }
//    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
//        let size = Utils.shared.get_font_sizes()[2]
//        var newSize = CGFloat(size)
//        if scale > 1 {
//            newSize *=  scale
//        }
//        self.body.font = self.body.font?.withSize(newSize)
//        self.reference.font = self.reference.font?.withSize(newSize)
//    }
}
