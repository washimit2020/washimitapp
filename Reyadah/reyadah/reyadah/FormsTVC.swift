//
//  FormsTVC.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator

class FormsTVC: UITableViewController {
    
    var action:(()->())?
    var postValues = [String:String]()
    lazy var request = ApiRequest()

    var fields:[(Validatable,UILabel)]?{
        didSet{
            for (field, _) in fields! {
                if let field = field as? UITextField {
                    field.delegate = self
                }
                if let field = field as? UITextView{
                    field.delegate = self
                }
            }
        }
    }
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

        self.tableView.allowsSelection = false
        self.tableView.separatorStyle = .none
        self.tableView.keyboardDismissMode = .onDrag
    }
    
    
    func change_to_old_status(){
        if let fields  =  fields{
            for (field, error) in fields {
                if let field = field as? UITextField {
                    field.layer.borderColor = Constants.themeColor.cgColor
                }
                error.isHidden = true
                error.textColor = UIColor.red
            }
        }
    }
    
     func makeFocus(neededView:UIView)->Bool{
        let pointInTable:CGPoint = neededView.superview!.convert(neededView.frame.origin, to:tableView)
        var contentOffset:CGPoint = tableView.contentOffset
        if let headerview = tableView.tableHeaderView{
            contentOffset.y  = pointInTable.y - headerview.frame.height

        }
        tableView.setContentOffset(contentOffset, animated: true)
        return true;
    }
    
}

extension FormsTVC:ValidationDelegate{
    func validationSuccessful() {
        preconditionFailure("This method must be overridden")
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        if let field = errors.first?.0 as? UIView {
            field.becomeFirstResponder()

        }
        
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.textColor = UIColor.red
            error.errorLabel?.isHidden = false
        }
    }
}


extension FormsTVC:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       return self.makeFocus(neededView:textField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField != (fields?.last?.0 as? UITextField)  {
            let current_fields = fields!.enumerated()
            
            var textContainer:UIView?
            
            for (key, field) in  current_fields{
                if let field = field.0 as? UITextField {
                    if field == textField {
                        textContainer = fields![key + 1].0 as? UIView
                        break;
                    }
                }
                if let field = field.0 as? UITextView {
                    if field == textField {
                        textContainer = fields![key + 1].0 as? UIView
                        break;
                    }
                }
            }
            
            textContainer?.becomeFirstResponder()
           
            
        }else{
            self.action!()
        }
        return true
    }
}

extension FormsTVC:UITextViewDelegate{
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        return self.makeFocus(neededView:textView)
    }
    
//    func textViewDidChange(_ textView: UITextView) {
//        let fixedWidth = textView.frame.size.width
//        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
//        var newFrame = textView.frame
//        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
//        textView.frame = newFrame;
//    }
    
    //    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    //        let pointInTable:CGPoint = textField.superview!.convert(textField.frame.origin, to:tableView)
    //        var contentOffset:CGPoint = tableView.contentOffset
    //        contentOffset.y  = pointInTable.y - tableView.tableHeaderView!.frame.height
    //
    //        tableView.setContentOffset(contentOffset, animated: true)
    //        return true;
    //    }
    //
    //    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    //
    //        if textField != (fields?.last?.0 as! UITextField) {
    //            let current_fields = fields!.enumerated()
    //            var current_key = 0;
    //            for (key, field) in  current_fields{
    //                if let field = field.0 as? UITextField {
    //                    if field == textField {
    //                        current_key = key + 1
    //                        break;
    //                    }
    //                }
    //            }
    //
    //            if let field = fields![current_key].0 as? UITextField {
    //                field.becomeFirstResponder()
    //            }
    //
    //        }else{
    //            self.action!()
    //        }
    //        return true
    //    }
}
