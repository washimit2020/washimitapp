package com.washimit.it.ryadah.activity;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnBranchCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.Interface.OnuserCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.SelectShowOrBranchAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Branche;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectProductToShaerInShowrOrBranch extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.no_show)
    TextViewCast no_show;
    @BindView(R.id.cancel)
    ImageView cancel;
    @BindView(R.id.done)
    ImageView done;
    @BindView(R.id.recycler_select_show_or_branch)
    RecyclerView recyclerView;
    ArrayList<Object> objects;
    ArrayList<Product> products;
    ArrayList<User> user;
    ArrayList<Branche> branche;
    SelectShowOrBranchAdapter adapter;
    String type;
    ApiRequest request;
    String urls;
    Gson gson;
    SharedPref pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        request = new ApiRequest(this);
        objects = new ArrayList<>();
        gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            type = bundle.getString("type");
            Log.d("safdasdf", type);
            products = gson.fromJson(bundle.getString("products"), new TypeToken<ArrayList<Product>>() {
            }.getType());
        }
        pref = new SharedPref(this);

        setContentView(R.layout.activity_select_product_to_shaer_in_showr_or_branch);
        ButterKnife.bind(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new SelectShowOrBranchAdapter(objects, this, recyclerView);
        recyclerView.setAdapter(adapter);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                objects.add(null);
                adapter.notifyItemInserted(objects.size() - 1);
                switch (type) {


                    case Constant._TYPE_SHARE_USERS:
                        getNext(Constant._TYPE_SHOW, urls);
                        break;
                    case Constant._TYPE_SHARE_BRANCH:
                        getNext(urls);
                        break;

                }

            }
        });


        adapter.setOnUserCallback(new OnuserCallback() {

            @Override
            public void users(ArrayList<User> users) {
                user = users;
                done.setVisibility(users.size() > 0 ? View.VISIBLE : View.GONE);
            }
        });
        cancel.setOnClickListener(this);
        done.setOnClickListener(this);
        UpDateUi(type);

        adapter.setOnBranchCallback(new OnBranchCallback() {
            @Override
            public void branch(ArrayList<Branche> branches) {
                branche = branches;

                done.setVisibility(branches.size() > 0 ? View.VISIBLE : View.GONE);
            }
        });

    }


    public void UpDateUi(String type) {


        switch (type) {


            case Constant._TYPE_SHARE_USERS:
                getUser(Constant._TYPE_SHOW);
                break;
            case Constant._TYPE_SHARE_BRANCH:
                getBranchs();

                break;

        }

    }


    private void getUser(int userType) {
        request.MultipartRequest(Request.Method.GET, false, "following/" + userType, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();
                adapter.setLoading();

                JSONObject result;
                JSONObject msg;
                JSONObject user;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                user = msg.getJSONObject("users");
                urls = user.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                }.getType()));
                adapter.notifyDataSetChanged();

                if (objects.size() == 0) {
                    no_show.setVisibility(View.VISIBLE);
                } else {
                    no_show.setVisibility(View.GONE);

                }

                adapter.setLoading();


            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);
            }
        });

    }

    private void getNext(final int userType, String url) {

        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                objects.remove(objects.size() - 1);
                adapter.notifyItemRemoved(objects.size() - 1);
                Log.d("result", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject user;
                JSONObject products;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                if (userType == Constant._TYPE_STORE || userType == Constant._TYPE_SHOW) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    adapter.setUrl(urls);
                    objects.addAll((Collection<?>) gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType()));
                } else if (userType == Constant._TYPE_MY_PRODUCT_DETAILS) {
                    user = msg.getJSONObject("user");
                    products = user.getJSONObject("products");
                    urls = products.getString("next_page_url");
                    adapter.setUrl(urls);
                    objects.addAll((Collection<?>) gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType()));
                }

                adapter.setLoading();

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);
            }
        });
    }


    private void getBranchs() {

        request.MultipartRequest(Request.Method.GET, false, "branch/" + pref.getUser().getId(), null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject resultJ;
                JSONObject msg;
                JSONObject branches;
                resultJ = new JSONObject(result);
                msg = resultJ.getJSONObject("msg");
                branches = msg.getJSONObject("branches");
                urls = branches.getString("next_page_url");
                adapter.setUrl(urls);
                ArrayList<Branche> brancheslist = gson.fromJson(branches.getString("data"), new TypeToken<ArrayList<Branche>>() {
                }.getType());
                objects.addAll(brancheslist);
                adapter.notifyDataSetChanged();


                adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });


    }

    private void getNext(String nexturl) {

        request.MultipartRequest(Request.Method.GET, true, nexturl, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                objects.remove(objects.size() - 1);
                adapter.notifyItemRemoved(objects.size() - 1);


                JSONObject resultJ;
                JSONObject msg;
                JSONObject branches;

                resultJ = new JSONObject(result);
                msg = resultJ.getJSONObject("msg");
                branches = msg.getJSONObject("branches");

                urls = branches.getString("next_page_url");
                adapter.setUrl(urls);
                ArrayList<Branche> brancheslist = gson.fromJson(branches.getString("data"), new TypeToken<ArrayList<Branche>>() {
                }.getType());

                objects.addAll(brancheslist);
                adapter.notifyDataSetChanged();

                adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.done:


                addProductToShowOrBranch();

                break;
            case R.id.cancel:
                finish();
                break;

        }
    }


    public void addProductToShowOrBranch() {
        int p = 0;

        HashMap<String, String> map = new HashMap<String, String>();
        map.put("type", type);

        switch (type) {

            case Constant._TYPE_SHARE_USERS:
                for (int i = 0; i < user.size(); i++) {

                    for (int j = 0; j < products.size(); j++) {
                        map.put("shares[" + p + "][product_id]", products.get(j).getId() + "");
                        map.put("shares[" + p + "][user_id]", user.get(i).getId() + "");
                        p++;
                    }


                }
                break;
            case Constant._TYPE_SHARE_BRANCH:
                for (int i = 0; i < branche.size(); i++) {

                    for (int j = 0; j < products.size(); j++) {

                        map.put("shares[" + p + "][product_id]", products.get(j).getId() + "");
                        map.put("shares[" + p + "][branch_id]", branche.get(i).getId() + "");
                        p++;
                    }


                }
                break;


        }

        request.MultipartRequest(Request.Method.POST, false, "product/share", map, "جاري مشاركة المنتج الرجاْ الانتظار", new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("afgvkaf", result);

                if (type.equalsIgnoreCase(Constant._TYPE_SHARE_USERS)) {
                    AapHalper.CreateDialog(R.layout.dialog_info, SelectProductToShaerInShowrOrBranch.this, new DaelogCallback() {
                        @Override
                        public void dialog(Dialog dialog) {
                            LinearLayout dialog_info_close;
                            dialog_info_close = (LinearLayout) dialog.findViewById(R.id.dialog_info_close);


                            dialog_info_close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                        }
                    });
                } else {
                    finish();
                }
            }

            @Override
            public void onRequestError(String errorMessage) {
                Toast.makeText(getApplicationContext(), "حدثت مشكلة ما الرجاء المحاولة مرة اخرى", Toast.LENGTH_LONG).show();

            }
        });

    }
}

