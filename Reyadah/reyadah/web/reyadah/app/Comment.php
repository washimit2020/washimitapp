<?php

namespace App;

use App\Http\Constants;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable= ['body','user_id'];
    protected $with = ["user"];
    protected $hidden = ['status_id'];

    public function commentable()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class)->where('status_id',Constants::_STATUS_ACTIVE);
    }

}
