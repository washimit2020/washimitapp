package com.it.washim.bookofhajj2.model;

/**
 * Created by washm on 4/3/17.
 */

public class comment  {


   private int id;
   private String comment;
   private String titel;

    public comment(int id, String comment, String titel) {
        this.id = id;
        this.comment = comment;
        this.titel = titel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }
}
