package com.washimit.it.ryadah.registrationActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.forget_email)
    EditTextView forget_email;
    @BindView(R.id.email)
    LinearLayout email;
    @BindView(R.id.forget_send_email)
    Button forget_send_email;
    @BindView(R.id.back)
    ImageView back;
    ApiRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        request = new ApiRequest(this);
        forget_send_email.setOnClickListener(this);


    }

    public boolean validation() {

        if (!(android.util.Patterns.EMAIL_ADDRESS.matcher(forget_email.getText().toString()).matches() && forget_email.getText().toString().length() >= 10)) {
            forget_email.setError(getResources().getString(R.string.invalid_email));
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.forget_send_email:


                if (validation()) {
                    ArrayList<Part> prams = new ArrayList<Part>();
                    prams.add(new StringPart("email", forget_email.getText().toString()));
                    request.MultipartRequest("POST", "password/email", "جاري التحقق الرجاء الانتظار قد يستغرق دقيقة من الوقت", prams, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result) throws JSONException {
                            Log.d("password", result);
                            Toast.makeText(getApplicationContext(), "تم ارسال ايميل للبريد الاكتروني المدرج من قبلك", Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onRequestError(String errorMessage) {
                            Log.d("password", errorMessage);
                            Toast.makeText(getApplicationContext(), "توجد مشكلة ما يرجى المحاولة لاحقا أو ان الايميل غير صحيح تاكد من الايميل واعد المحاولة", Toast.LENGTH_LONG).show();
                        }
                    });

                }
                break;

            case R.id.back:
                this.finish();
                break;
        }
    }
}
