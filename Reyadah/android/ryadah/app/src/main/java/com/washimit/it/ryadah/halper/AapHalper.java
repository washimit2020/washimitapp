package com.washimit.it.ryadah.halper;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by mohammad on 4/12/2017.
 */

public class AapHalper {
    public static ApiRequest request;

    public static boolean gpsIsOn(Activity activity) {
        final LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps(activity);
            return false;
        } else {
            return true;
        }


    }

    private static void buildAlertMessageNoGps(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(R.string.gps_setting_enable_message)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public static int userIconType(int userType) {


        switch (userType) {
            case Constant._PAGE_SHOW:
                return R.drawable.ic_showroom_20;


            case Constant._PAGE_STORE:
                return R.drawable.ic_store_20;


            case Constant._PAGE_FESTIVAL:
                return R.drawable.ic_festival_20;


        }
        return 0;

    }
    public static int userMapIconType(int userType) {


        switch (userType) {
            case Constant._PAGE_SHOW:
                return R.drawable.ic_chose_location_deactive;


            case Constant._PAGE_STORE:
                return R.drawable.ic_store_location_deactive;


            case Constant._PAGE_FESTIVAL:
                return R.drawable.ic_festival_location;


        }
        return 0;

    }

    public static String userNameType(int userType, Context context) {


        switch (userType) {
            case Constant._PAGE_SHOW:
                return context.getString(R.string.show);


            case Constant._PAGE_STORE:
                return context.getString(R.string.store);


        }
        return "";

    }

    public static int isFollow(int followType) {


        switch (followType) {
            case 0:
                return R.drawable.ic_follow;


            case 1:
                return R.drawable.ic_unfollow;


        }
        return 2;

    }

    public static String getStatus(int followType) {


        switch (followType) {
            case 1:
                return "مقبول";

            case 5:

                return "مرفوض";

            case 2:

                return "أنتظار";


        }
        return "";

    }

    public static int isWhishlestd(int whishlestdType) {


        switch (whishlestdType) {
            case 0:
                return R.drawable.ic_favorite_1;


            case 1:
                return R.drawable.ic_wishlist_2;


        }
        return 2;

    }

    public static int isLike(int likeType) {


        switch (likeType) {
            case 0:
                return R.drawable.ic_like_20_of;


            case 1:
                return R.drawable.ic_like_20;


        }
        return 2;

    }

    public static boolean isHide(int hideType) {


        switch (hideType) {
            case Constant._STATUS_HIDDEN:
                return true;


            case Constant._STATUS_ACTIVE:
                return false;


        }
        return false;

    }

    public static void userAction(String url, ArrayList<Part> parts, Activity context, @Nullable final RatingBar ratingBar) {

        request = new ApiRequest(context);

        request.MultipartRequest("POST", url, null, parts, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                if (ratingBar != null) {

                    JSONObject object = new JSONObject(result);
                    JSONObject msg = object.getJSONObject("msg");
                    ratingBar.setRating((float) msg.getDouble("rating"));
                }
                Log.d("result__", result);


            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("result__", errorMessage);

            }
        });


    }

    public static void CreateDialog(int view, Context context, DaelogCallback callback) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);

        callback.dialog(dialog);


        dialog.show();


    }


    public static ArrayList<Comment> get_comment_response(JSONArray comment) throws JSONException {
        ArrayList<Comment> response = new ArrayList();

        for (int i = 0; i < comment.length(); i++) {

            Gson gson = new Gson();

            response.add(0, gson.fromJson(comment.get(i).toString(), Comment.class));

        }
        return response;
    }


    public static boolean requestPermission(Activity activity, String[] permission, int requestCode) {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                Toast.makeText(activity, "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(activity, permission, requestCode);

            } else {
                ActivityCompat.requestPermissions(activity, permission
                        , requestCode);
            }


            return false;

        } else {
            return true;
        }
    }


    public static void hidKepord(EditTextView editTextView, Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editTextView.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public static void share(String text, Activity activity) {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.setType("text/plain");

        intent.putExtra(Intent.EXTRA_TEXT, text);

        activity.startActivity(Intent.createChooser(intent, "مشاركة التطبيق"));
    }

    /**
     * @param imageView
     * @return
     */
    public static Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    public static void sher(Activity context, ImageView imageView, String productName) {
        if (AapHalper.requestPermission(context, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1)) {
            Uri bmpUri = AapHalper.getLocalBitmapUri(imageView);
            if (bmpUri != null) {
                // Construct a ShareIntent with link to image
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_TEXT, productName);
                shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
                shareIntent.setType("image/*");
                // Launch sharing dialog for image
                context.startActivity(Intent.createChooser(shareIntent, "Share Image"));
            } else {
                // ...sharing failed, handle error
            }
        }
    }



    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static Bitmap getBitmap(Activity context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (drawable instanceof BitmapDrawable) {
            return BitmapFactory.decodeResource(context.getResources(), drawableId);
        } else if (drawable instanceof VectorDrawable) {
            return getBitmap((VectorDrawable) drawable);
        } else {
            throw new IllegalArgumentException("unsupported drawable type");
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private static Bitmap getBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }
}



