//
//  User.swift
//  reyadah
//
//  Created by WASHM on 4/18/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//



class User : Model{
    
    var id:Int!
    var name:String!
    var email:String!
    var phone_number:String!
    var info:String!
    var sector_name:String!
    var street_name:String!
    var lat:String!
    var lng:String!
    var rating:Double!
    var followed:Bool!
    var socialMedia:SocialMedia!
    var status:Status!
    var type:ModelType!
    var totalFollowers:Int!
    var rated:Rated!
    
    var dataModel:DataModel?
    
    
     override func map() {
        id = json["id"].intValue
        name = json["name"].stringValue
        info = json["info"].stringValue
        rating = json["rating"].doubleValue
        email = json["email"].stringValue
        phone_number = json["phone_number"].stringValue
        sector_name = json["sector_name"].stringValue
        street_name = json["street_name"].stringValue
        lat = json["lat"].stringValue
        lng = json["lng"].stringValue
        followed = json["followed"].boolValue
        totalFollowers = json["total_followers"].intValue
        
        socialMedia = SocialMedia(json["social_media"])
        status = Status(json["status"])
        type = ModelType(json["type"])
        rated = Rated(json["rated"])
        dataModel = DataModel(json["products"])
    }

}

class Branch: User {
    var user_id:Int!

    
    override func map() {
        super.map()
        user_id = json["user_id"].intValue
    }

}

class Festival: Branch , Commentable {
    
    var organization:String!
    var organizer_name:String!
    var start_in:String!
    var from:String!
    var to:String!
    var soruce:String!
    var comments:[Comment]! = [Comment]()

    override func map() {
        super.map()
        organization = json["organization"].stringValue
        organizer_name = json["organizer_name"].stringValue
        phone_number = json["organizer_phone_number"].stringValue
        start_in = json["start_in"].stringValue
        from = json["from"].stringValue
        to = json["to"].stringValue
        soruce = json["soruce"].stringValue
        
        set_comments()
    }
    
    func set_comments() {
        let data = json["comments"].arrayValue
        var commented = [Comment]()
        if data.count > 0{
            for i in 0...data.count - 1{
                commented.append(Comment(data[i]))
            }
        }
        self.comments = commented
    }
}
