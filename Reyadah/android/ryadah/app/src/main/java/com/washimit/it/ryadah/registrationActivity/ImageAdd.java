package com.washimit.it.ryadah.registrationActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.viewCasting.ButtonView;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ImageAdd extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.select_avatar)
    ImageView select_avatar;
    @BindView(R.id.select_caver_1)
    ImageView select_caver_1;
    @BindView(R.id.select_caver_2)
    ImageView select_caver_2;
    @BindView(R.id.select_caver_3)
    ImageView select_caver_3;

    @BindView(R.id.delete_avatar)
    ImageView delete_avatar;
    @BindView(R.id.delete_caver_1)
    ImageView delete_caver_1;
    @BindView(R.id.delete_caver_2)
    ImageView delete_caver_2;
    @BindView(R.id.delete_caver_3)
    ImageView delete_caver_3;

    @BindView(R.id.add_imges)
    ButtonView ok;
    @BindView(R.id.cancel_add_img)
    ButtonView cancel;
    private final int avatar = 0;
    private final int caver_1 = 1;
    private final int caver_2 = 2;
    private final int caver_3 = 3;
    String avatarFile, caver_1File, caver_2File, caver_3File;

    SharedPref pref;
    private int type;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getImage(avatar);
                } else {

                    Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 1: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getImage(caver_1);
                } else {

                    Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 2: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getImage(caver_2);
                } else {

                    Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case 3: {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    getImage(caver_2);
                } else {

                    Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_add);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            type = (int) bundle.get("requestType");
            if (type != Constant._TYPE_SIGN_UP) {
                select_avatar.setVisibility(View.GONE);
            }
        }
        pref = new SharedPref(this);
//        if (pref.getUser() != null) {
//            User user = pref.getUser();
//            Picasso.with(this).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_store).into(select_avatar);
//            Picasso.with(this).load(Constant._COVER_URL + user.getId() + "/" + 0).fit().centerCrop().placeholder(R.drawable.ic_store).into(select_caver_1);
//            Picasso.with(this).load(Constant._COVER_URL + user.getId() + "/" + 1).fit().centerCrop().placeholder(R.drawable.ic_store).into(select_caver_2);
//            Picasso.with(this).load(Constant._COVER_URL + user.getId() + "/" + 2).fit().centerCrop().placeholder(R.drawable.ic_store).into(select_caver_3);
//        }
        select_avatar.setOnClickListener(this);
        select_caver_1.setOnClickListener(this);
        select_caver_2.setOnClickListener(this);
        select_caver_3.setOnClickListener(this);


        delete_avatar.setOnClickListener(this);
        delete_caver_1.setOnClickListener(this);
        delete_caver_2.setOnClickListener(this);
        delete_caver_3.setOnClickListener(this);
        ok.setOnClickListener(this);
        cancel.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.select_avatar:
               if( AapHalper.requestPermission(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0)){


                   getImage(avatar);


               }


                break;


            case R.id.select_caver_1:
                if( AapHalper.requestPermission(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1))
                {
                    getImage(caver_1);

                }


                break;


            case R.id.select_caver_2:
                if( AapHalper.requestPermission(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2))
                {
                    getImage(caver_2);

                }
                break;


            case R.id.select_caver_3:
                if( AapHalper.requestPermission(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 3))
                {
                    getImage(caver_3);

                }

                break;
            case R.id.delete_avatar:
                avatarFile = null;
                delete_avatar.setVisibility(View.GONE);
                select_avatar.setImageDrawable(getResources().getDrawable(R.drawable.app_icon));
                break;


            case R.id.delete_caver_1:
                caver_1File = null;
                delete_caver_1.setVisibility(View.GONE);
                select_caver_1.setImageDrawable(getResources().getDrawable(R.drawable.app_icon));
                break;


            case R.id.delete_caver_2:
                caver_2File = null;
                delete_caver_2.setVisibility(View.GONE);
                select_caver_2.setImageDrawable(getResources().getDrawable(R.drawable.app_icon));
                break;


            case R.id.delete_caver_3:
                caver_3File = null;
                delete_caver_3.setVisibility(View.GONE);
                select_caver_3.setImageDrawable(getResources().getDrawable(R.drawable.app_icon));
                break;

            case R.id.add_imges:
                Intent intent = new Intent();
                intent.putExtra("avatar", avatarFile);
                intent.putExtra("caver1", caver_1File);
                intent.putExtra("caver2", caver_2File);
                intent.putExtra("caver3", caver_3File);
                setResult(type, intent);
                finish();

                break;
            case R.id.cancel_add_img:
                finish();

                break;


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == avatar) {


            avatarFile = AapHalper.getRealPathFromURI(getApplicationContext(), data.getData());
            Log.d("avatar", avatarFile);
            select_avatar.setImageURI(data.getData());
            delete_avatar.setVisibility(View.VISIBLE);

        }
        if (resultCode == RESULT_OK && requestCode == caver_1) {

            caver_1File = AapHalper.getRealPathFromURI(getApplicationContext(), data.getData());

            Log.d("caver_1", caver_1File);
            select_caver_1.setImageURI(data.getData());
            delete_caver_1.setVisibility(View.VISIBLE);

        }
        if (resultCode == RESULT_OK && requestCode == caver_2) {
            caver_2File = AapHalper.getRealPathFromURI(getApplicationContext(), data.getData());

            Log.d("caver_2", caver_2File);
            select_caver_2.setImageURI(data.getData());
            delete_caver_2.setVisibility(View.VISIBLE);

        }
        if (resultCode == RESULT_OK && requestCode == caver_3) {
            caver_3File = AapHalper.getRealPathFromURI(getApplicationContext(), data.getData());

            Log.d("caver_3", caver_3File);
            select_caver_3.setImageURI(data.getData());
            delete_caver_3.setVisibility(View.VISIBLE);

        }


    }

    public void getImage(int request) {


        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);


        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryIntent);
        chooser.putExtra(Intent.EXTRA_TITLE, "title");

        Intent[] intentArray = {cameraIntent};
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, request);
    }


}
