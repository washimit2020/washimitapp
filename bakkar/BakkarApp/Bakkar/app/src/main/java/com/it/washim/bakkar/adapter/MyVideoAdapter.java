package com.it.washim.bakkar.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnDetailsCollBack;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.model.Posts;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 10/18/2017.
 */

public class MyVideoAdapter extends RecyclerView.Adapter {
    ArrayList<Posts> list;
    Context context;
    RecyclerView recyclerView;
    boolean loading = false;
    int add = 3;
    int ItemCount;
    int ItemPosition;
    OnLoadMorCollBack onLoadMorCollBack;
    String url;

    public MyVideoAdapter(ArrayList<Posts> list, Context context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        this.recyclerView = recyclerView;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (!url.equalsIgnoreCase("null")) {
                        ItemCount = layoutManager.getItemCount();
                        ItemPosition = layoutManager.findLastVisibleItemPosition();
                        if (!loading && ItemCount <= (ItemPosition + add)) {

                            if (onLoadMorCollBack != null) {

                                onLoadMorCollBack.onLoad();

                            }
                            loading = true;

                        }
                    }
                }
            });

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VideoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Posts video = list.get(position);

        if (holder instanceof VideoViewHolder) {

            ((VideoViewHolder) holder).lblListItem.setText(video.title);


        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class VideoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblListItem)
        MyTextView lblListItem;

        public VideoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent videoClient = new Intent(Intent.ACTION_VIEW);
//                    videoClient.setData(Uri.parse(list.get(getAdapterPosition()).link.media_url.replace("https","youtube")));
//                    context.startActivity(videoClient);

//                    try {
//                        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(getAdapterPosition()).link.media_url.replace("https", "youtube"))));
//                    } catch (android.content.ActivityNotFoundException anfe) {
//                        viewInBrowser(context, list.get(getAdapterPosition()).link.media_url);
//                    }
                    Intent applicationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(getAdapterPosition()).link.media_url.replace("https", "youtube")));
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(list.get(getAdapterPosition()).link.media_url));
                    try {
                        context.startActivity(applicationIntent);
                    } catch (ActivityNotFoundException ex) {
                        context.startActivity(browserIntent);
                    }

                }
            });
        }
    }


    public void addOnLoadMorLisenar(OnLoadMorCollBack onLoadMorCollBack) {
        this.onLoadMorCollBack = onLoadMorCollBack;
    }

    public void isLoading(Boolean loading) {
        this.loading = loading;
    }

    public void setUrl(String url) {
        this.url = url;
    }


}
