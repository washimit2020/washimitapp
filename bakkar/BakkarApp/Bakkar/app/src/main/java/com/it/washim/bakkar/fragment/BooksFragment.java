package com.it.washim.bakkar.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
//import android.util.Log;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.BookDetailsActivity;
import com.it.washim.bakkar.activity.HelloActivity;
import com.it.washim.bakkar.adapter.BookAdapter;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnDetailsCollBack;
import com.it.washim.bakkar.interfase.OnDialogCollBack;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Categories;
import com.it.washim.bakkar.model.CategoriesData;
import com.it.washim.bakkar.model.Posts;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BooksFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.books_list)
    RecyclerView books_list;

    @BindView(R.id.title)
    MyTextView title;

    @BindView(R.id.body)
    MyTextView body;

    @BindView(R.id.rating)
    RatingBar rating;

    @BindView(R.id.create)
    MyTextView create;

    @BindView(R.id.prise)
    MyTextView prise;

    @BindView(R.id.book_image)
    ImageView book_image;

    @BindView(R.id.downlod_book)
    MyButtonView downlod_book;

    @BindView(R.id.sher)
    ImageView sher;

    @BindView(R.id.wsh_list)
    ImageView wsh_list;

    @BindView(R.id.falter_type)
    Spinner falter_type;

    @BindView(R.id.na_data)
    MyTextView na_data;

    @BindView(R.id.next)
    MyTextView next;

    @BindView(R.id.page_book)
    LinearLayout page_book;

    @BindView(R.id.book_details)
    ScrollView book_details;

    @BindView(R.id.proogresbar)
    ProgressBar proogresbar;

    @BindView(R.id.ratray)
    MyButtonView ratray;

    BookAdapter adapter;

    ArrayList<Posts> list;

    private Posts book;

    HashMap<String, String> map = new HashMap<String, String>();

    private int falterType = -1;
    int po;
    String url;
    ArrayAdapter<CategoriesData> arrayAdapter;
    Categories categories;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_books, container, false);
        ButterKnife.bind(this, view);
        getCategories();
        final Animation animation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slid);
        list = new ArrayList<>();
        books_list.setHasFixedSize(true);
        books_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, true));
        adapter = new BookAdapter(list, getActivity(), books_list);
        adapter.addOnLoadMorLisenar(new OnLoadMorCollBack() {
            @Override
            public void onLoad() {
                getNextBooks();
            }
        });

        adapter.OnDetailsLesnar(new OnDetailsCollBack() {
            @Override
            public void details(Object o, int p) {

                if (o instanceof Posts) {
                    book_details.startAnimation(animation);
                    book = (Posts) o;
                    po = p;
                    title.setText(book.title);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        body.setText(Html.fromHtml(book.description + "", Html.FROM_HTML_MODE_COMPACT));
                    } else {
                        body.setText(Html.fromHtml(book.description + ""));

                    }

                    if (book.link != null) {
                        downlod_book.setVisibility(book.link.android != null ? View.VISIBLE : View.GONE);
                    }
                    {
                        downlod_book.setVisibility(View.GONE);

                    }
                    create.setText(("سنة الاصدار: " + book.published_at).replace("null", "لا يوجد"));
                    prise.setText(book.price == null ? "السعر: مجاني" : "السعر: " + book.price + " ريال");
                    rating.setRating((float) book.rating);
                    if (book.attachments.size() > 0) {
                        if (book.attachments.get(0).attachment_type_id == Constant.ATTACHMENT_COVER) {

                            Picasso.with(getActivity()).load(Constant.URL + "attachments/" + book.attachments.get(0).id).placeholder(R.drawable.bakkar_icon).into(book_image);
                        } else {
                            book_image.setImageDrawable(getResources().getDrawable(R.drawable.bakkar_icon));
                        }

                    } else {
                        book_image.setImageDrawable(getResources().getDrawable(R.drawable.bakkar_icon));
                    }
                    wsh_list.setImageDrawable(getResources().getDrawable(book.is_wishlisted ? R.drawable.bakar_icon_favorite : R.drawable.bakar_icon_favorite_of));

                }


            }
        });
        falter_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Log.d("asfasfaASDASsf", position + "");
                falterType = categories.data.get(position).id;
                url = null;
                getBooks();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        ArrayAdapter adapte = new ArrayAdapter(
//                getContext(),
//                R.layout.item_text,
//                Arrays.asList(getResources().getStringArray(R.array.book))
//        );


        books_list.setAdapter(adapter);


        body.setOnClickListener(this);
        sher.setOnClickListener(this);
        wsh_list.setOnClickListener(this);
        ratray.setOnClickListener(this);
        next.setOnClickListener(this);

        url = null;
        getBooks();
        return view;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null) {
                list.get(po).is_wishlisted = data.getExtras().getBoolean("is_wishlisted");
                book.is_wishlisted = data.getExtras().getBoolean("is_wishlisted");
                wsh_list.setImageDrawable(getResources().getDrawable(book.is_wishlisted ? R.drawable.bakar_icon_favorite : R.drawable.bakar_icon_favorite_of));
                list.get(po).rated = data.getExtras().getDouble("rating");
                book.rated = data.getExtras().getDouble("rating");
                //Log.d("ASfsafas", data.getExtras().getDouble("rating") + "");

            }

            adapter.notifyDataSetChanged();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.body:

                startActivityForResult(new Intent(getActivity(), BookDetailsActivity.class)
                        .putExtra("rated", book.rated)
                        .putExtra("book_id", book.id)
                        .putExtra("image_id", book.attachments.size() > 0 ? book.attachments.get(0).id : null)
                        .putExtra("book_description", book.description)
                        .putExtra("book_title", book.title)
                        .putExtra("is_wishlisted", book.is_wishlisted)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), 1);

                break;

            case R.id.downlod_book:

                break;
            case R.id.next:

                startActivityForResult(new Intent(getActivity(), BookDetailsActivity.class)
                        .putExtra("rated", book.rated)
                        .putExtra("book_id", book.id)
                        .putExtra("image_id", book.attachments.size() > 0 ? book.attachments.get(0).id : null)
                        .putExtra("book_description", book.description)
                        .putExtra("book_title", book.title)
                        .putExtra("is_wishlisted", book.is_wishlisted)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP), 1);
                break;

            case R.id.sher:
                if (book.link != null) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مشاركة");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, book.link.android + "");
                    startActivity(Intent.createChooser(sharingIntent, "مشاركة"));
                } else {
                    Toast.makeText(getActivity(), "لا يمكن مشاركة هذا الكتاب حاليا اعد المحاولة لاحقا", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.wsh_list:
                if (shardPref().getUser() != null) {
                    wsh_list.setImageDrawable(getResources().getDrawable(book.is_wishlisted ? R.drawable.bakar_icon_favorite_of : R.drawable.bakar_icon_favorite));
                    book.is_wishlisted = !book.is_wishlisted;

                    map.put("post_id", book.id + "");
                    request().stringRequest(Request.Method.POST, getActivity(), "wishlist", map, new OnRequestCollBack() {
                        @Override
                        public void onResponse(String response) throws JSONException {
                            //Log.d("wishlist", response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Log.d("wishlist", error.getMessage() + "rrrr");

                        }
                    });

                } else {
                    showAlertDialog("حتى تتمكن من اضافة الكتاب الى مفضلتك قم بتسجيل الدخول :)", new OnDialogCollBack() {
                        @Override
                        public void login() {
                            startActivity(new Intent(getActivity(), HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            getActivity().finish();
                        }

                        @Override
                        public void hide() {

                        }
                    });
                }
                break;
            case R.id.ratray:
                getBooks();
                break;

        }
    }

    private void getBooks() {

        ratray.setVisibility(View.GONE);
        na_data.setVisibility(View.GONE);

        if (url == null) {
            proogresbar.setVisibility(View.VISIBLE);
            page_book.setVisibility(View.GONE);
        }

        request().stringRequest(Request.Method.GET, getActivity(), url != null ? url.replace(Constant.URL, "") : "posts/" + Constant.PAGES_BOOKS + (falterType != -1 ? "?filter=" + falterType : ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                JSONObject object;
                JSONObject posts;
                object = new JSONObject(response);
                posts = object.getJSONObject("posts");
                list.clear();

                adapter.setFirst(false);
                url = (String) posts.getString("next_page_url");
                if (!url.equalsIgnoreCase("null")) {
                    url = url + "&" + (falterType != -1 ? "filter=" + falterType : "");
                }
                adapter.setUrl(url);
                // Log.d("asfasfas", url + "");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                proogresbar.setVisibility(View.GONE);
                if (list.size() > 0) {
                    page_book.setVisibility(View.VISIBLE);
                    na_data.setVisibility(View.GONE);
                } else {
                    na_data.setText("لايوجد كتب");
                    page_book.setVisibility(View.GONE);
                    na_data.setVisibility(View.VISIBLE);

                }
                adapter.isLoading(false);

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                proogresbar.setVisibility(View.GONE);
                na_data.setText("لايوجد اتصال بالانترنت");
                page_book.setVisibility(View.GONE);
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);


            }
        });
    }

    private void getNextBooks() {

        ratray.setVisibility(View.GONE);
        na_data.setVisibility(View.GONE);

//        Log.d("asfsfas", Constant.URL + url.replace(Constant.URL, ""));

        request().stringRequest(Request.Method.GET, getActivity(), url.replace(Constant.URL, ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject object;
                JSONObject posts;
                object = new JSONObject(response);
                posts = object.getJSONObject("posts");
                adapter.setFirst(false);
                url = (String) posts.getString("next_page_url");
                if (!url.equalsIgnoreCase("null")) {
                    url = url + "&" + (falterType != -1 ? "filter=" + falterType : "");
                }
                adapter.setUrl(url);
                // Log.d("asfasfas", url + "");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                proogresbar.setVisibility(View.GONE);
                if (list.size() > 0) {
                    page_book.setVisibility(View.VISIBLE);
                    na_data.setVisibility(View.GONE);
                } else {
                    na_data.setText("لايوجد كتب");
                    page_book.setVisibility(View.GONE);
                    na_data.setVisibility(View.VISIBLE);

                }
                adapter.isLoading(false);

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                proogresbar.setVisibility(View.GONE);
                na_data.setText("لايوجد اتصال بالانترنت");
                page_book.setVisibility(View.GONE);
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);


            }
        });
    }

    private void getCategories() {

        request().stringRequest(Request.Method.GET, getActivity(), "categories", null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject object = new JSONObject(response);

                categories = gson().fromJson(object.getString("categories"), Categories.class);
                arrayAdapter = new ArrayAdapter<CategoriesData>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categories.data);
                // Log.d("afasfasfasf", categories.from + "");

                falter_type.setAdapter(arrayAdapter);

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                getCategories();
            }
        });


    }
}