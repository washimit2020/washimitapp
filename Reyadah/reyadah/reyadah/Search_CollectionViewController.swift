//
//  Search_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/6/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON


class Search_CollectionViewController: ItemsLoader_CollectionViewController {
    enum cell_ids : String {
        case product = "product"
        case user = "user"
    }
    
    lazy var  dialog = SortFilter_ViewController(nibName: "SortFilter_ViewController", bundle: nil)
    
    var sorts = [
        ["name":"الاقل سعرا","value":"price","type":"sort"],
        ["name":"الافضل تقييما","value":"rating","type":"sort"],
        ]
    var filter = [
        ["name":"الكل","value":"\(Constants.SEARCH_ALL)","type":"type"],
        ["name":"المنتجات","value":"\(Constants.SEARCH_PRODUCT)","type":"type"],
        ["name":"المتاجر","value":"\(Constants.SEARCH_STORE)","type":"type"],
        ["name":"المعارض","value":"\(Constants.SEARCH_SHOW)","type":"type"],
        ["name":"المهرجانات","value":"\(Constants.SEARCH_FESTIVAL)","type":"type"],
        ]
    
    var numberOfCells:CGFloat = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        dialog.delegate = self
        
        self.delegate = self
        // Register cell classes
        self.collectionView!.register(UINib(nibName: "ProductSearch_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cell_ids.product.rawValue)
        self.collectionView!.register(UINib(nibName: "SearchBar_CollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "searchBar")
        self.collectionView!.register(UINib(nibName: "PlaceSearch_UICollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cell_ids.user.rawValue)
        
        self.link = RequestLinks.search.rawValue
        self.beginWithModel = "products"
        parameters["type"] = "\(Constants.SEARCH_ALL)"
        httpMethod = .post
        
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.sectionHeadersPinToVisibleBounds = true
        
        collectionView?.collectionViewLayout = layout
        // Do any additional setup after loading the view.
        clearData()
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        let item = items[indexPath.row]
        if let product =  item as? Product{
            let product_cell = collectionView.dequeueReusableCell(withReuseIdentifier: cell_ids.product.rawValue, for: indexPath) as! ProductSearch_CollectionViewCell
            product_cell.product = product
            
            cell = product_cell
            
        }else if let user  = item as? Festival{
            let placeCell = collectionView.dequeueReusableCell(withReuseIdentifier: cell_ids.user.rawValue, for: indexPath ) as! PlaceSearch_UICollectionViewCell
            
            placeCell.user = user
            placeCell.followButton.addTarget(nil, action: #selector(followUnFollow(_:)), for: .touchUpInside)
            placeCell.locationButton.addTarget(nil, action: #selector(go_to_locations(_:)), for: .touchUpInside)
            cell = placeCell
            
        }
        cell.layer.borderColor = Constants.themeColor.cgColor
        cell.layer.borderWidth = 0.5
        
        // Configure the cell
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "searchBar",
                                                                             for: indexPath) as! SearchBar_CollectionReusableView
            headerView.searchBar.delegate = self
            headerView.sort.addTarget(nil, action: #selector(show_sortDialog(_:)), for: .touchUpInside)
            headerView.filter.addTarget(nil, action: #selector(show_sortDialog(_:)), for: .touchUpInside)
            headerView.bigShape.addTarget(nil, action: #selector(change_shape(_:)), for: .touchUpInside)
            headerView.smallShape.addTarget(nil, action: #selector(change_shape(_:)), for: .touchUpInside)
            return headerView
        default:
            //4
            
            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width:CGFloat = collectionView.bounds.width/numberOfCells
        var height:CGFloat = 220
        
        if let type = parameters["type"]{
            if type != "\(Constants.SEARCH_ALL)" && type != "\(Constants.SEARCH_PRODUCT)"{
                width = collectionView.bounds.width
                height = 100
            }
        }
        return CGSize(width:  width, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize{
        return CGSize(width: collectionView.bounds.width, height: 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        
        let detals = Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        
        detals.model = item
        
        self.navigationController?.pushViewController(detals, animated: true)
        
        super.collectionView(collectionView, didSelectItemAt: indexPath)
        
    }
    
    
    
}

extension Search_CollectionViewController:ItemsLoaderDelegate,UISearchBarDelegate{
    
    func dataFiller(data: [JSON]) {
        if beginWithModel == "products" {
            for item in data{
                self.items.append(Product(item))
            }
        }else{
            for item in data{
                self.items.append(Festival(item))
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        parameters ["name"] = searchBar.text!
        
        clearData()
    }
    
    public func followUnFollow(_ sender:AnyObject) {
        var param = [String:String]()
        let point = sender.convert(CGPoint.zero, to: collectionView)
        guard let index = collectionView?.indexPathForItem(at: point) else{
            return
        }
        
        let cell = collectionView?.cellForItem(at: index) as! PlaceSearch_UICollectionViewCell
        param["follow_type"] = "\(Constants.TYPE_FOLLOW_USER)"
        param["user_id"] = "\(cell.user!.id!)"
        
        if cell.user?.user_id != 0{
            param["follow_type"] = "\(Constants.TYPE_FOLLOW_FESTIVAL)"

        }
        if cell.user!.followed{
            cell.user?.totalFollowers! -= 1
        }else{
            cell.user?.totalFollowers! += 1
        }
        
        cell.user?.followed = !cell.user!.followed!
        collectionView?.reloadItems(at: [collectionView!.indexPath(for: cell)!])

        
       
        self.apiRequest.call_follow_unfollow(cell.user!.id,param["follow_type"]! )
        
       
    }
    
    func go_to_locations(_ sender:AspectFitButton){
        let point = sender.convert(CGPoint.zero, to: collectionView)
        guard let index = collectionView?.indexPathForItem(at: point) else{
            return
        }
        selectedIndexPath = index
       let user = items[index.row] as? Festival
        
        perform_locations_segue(ProductOrUser: user!, ViewController: self, apiRequest)
    }
    
}

extension Search_CollectionViewController:SortFilter{
    
    func selected_item(value:String,type:String){
        parameters[type] = value
        if let type = parameters["type"]{
            if type != "\(Constants.SEARCH_ALL)" && type != "\(Constants.SEARCH_PRODUCT)"{
                if type == "\(Constants.SEARCH_FESTIVAL)"{
                    beginWithModel = "festivals"

                }else{
                    beginWithModel = "users"

                }
            }else{
                beginWithModel = "products"
            }
        }
        clearData()
    }
    
    func change_shape(_ sender:AspectFitButton){
        if  sender.tag == 1{
            numberOfCells = 2
            
        }else{
            numberOfCells = 3
        }
        
        collectionView?.reloadData()
    }
    
    func show_sortDialog(_ sender:AspectFitButton){
        if  sender.tag == 1{
            dialog.items = self.filter
            
        }else{
            dialog.items = self.sorts
            
        }
        self.present(dialog, animated: true, completion: nil)
    }
    
    
}
