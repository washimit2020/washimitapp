package com.washimit.it.ryadah.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.DetailsPageAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceDetailsActivity extends AppCompatActivity {
    @BindView(R.id.nodata)
    TextViewCast nodata;
    @BindView(R.id.activity_place_details_recycler_details)
    RecyclerView recycler_details;
    @BindView(R.id.activity_place_details_product_name)
    TextViewCast activity_details_product_name;
    @BindView(R.id.activity_details_user_name)
    TextViewCast activity_details_user_name;
    @BindView(R.id.activity_details_user_icon)
    ImageView activity_details_user_icon;
    DetailsPageAdapter adapter;
    ArrayList<Object> detailsItem;
    int userType;
    int itemNumber = 1;
    View header;
    String data;
    Object headerData;
    Gson gson;
    Intent intent;
    ApiRequest request;
    String urls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_details);
        ButterKnife.bind(this);
        gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        data = bundle.getString("data");
        userType = bundle.getInt("userType");
        request = new ApiRequest(this);

        activity_details_user_icon.setImageDrawable(getResources().getDrawable(AapHalper.userIconType(userType)));
        itemNumber = 3;
        headerData = gson.fromJson(data, User.class);


        header = getLayoutInflater().inflate(R.layout.item_details_header, null);

        detailsItem = new ArrayList<>();


        recycler_details.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, itemNumber);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return itemNumber;
                }
                return 1;

            }
        });

        recycler_details.setLayoutManager(gridLayoutManager);


        adapter = new DetailsPageAdapter(detailsItem, this, recycler_details);

        adapter.setHeader(header);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                detailsItem.add(null);
                adapter.notifyItemInserted(detailsItem.size() - 1);
                getNext();
            }
        });

        recycler_details.setAdapter(adapter);

        updateUi();


    }


    public void updateUi() {


        activity_details_user_name.setText(((User) headerData).getName());
        detailsItem.add(headerData);

        request.MultipartRequest(Request.Method.GET, false, "user/" + ((User) headerData).getId(), null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject user;
                JSONObject products;


                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                user = msg.getJSONObject("user");
                products = user.getJSONObject("products");
                urls = products.getString("next_page_url");
                adapter.setNextUrls(urls);
                detailsItem.addAll((Collection<?>) gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));

                //    nodata.setVisibility(detailsItem.size() == 1 ?View.VISIBLE:View.GONE);

                adapter.notifyDataSetChanged();


                adapter.setLoaded();

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);
            }
        });

    }

    public void getNext() {
        request.MultipartRequest(Request.Method.GET, true, urls, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                detailsItem.remove(detailsItem.size() - 1);
                adapter.notifyItemRemoved(detailsItem.size() - 1);
                Log.d("result", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject user;
                JSONObject products;


                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                user = msg.getJSONObject("user");
                products = user.getJSONObject("products");
                urls = products.getString("next_page_url");
                adapter.setNextUrls(urls);
                detailsItem.addAll((Collection<?>) gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));


                adapter.notifyDataSetChanged();


                adapter.setLoaded();

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);
            }
        });

    }


}
