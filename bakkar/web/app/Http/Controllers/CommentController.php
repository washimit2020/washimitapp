<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 6/18/17
 * Time: 12:20 PM
 */

namespace App\Http\Controllers;


use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Users_Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    const RESPONSE_KEY = "comment";
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'body' => 'required|max:8000',
            'post_id' => 'required|exists:posts,id',
        ]);

        $comment = Users_Comment::create([
            'user_id' => $request->user()->id,
            'post_id' => $request->post_id,
            "body"=>$request->body
        ]);

        return response()->json([self::RESPONSE_KEY=> $comment]);
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'id' => "required|exists:users__comments,id,user_id,{$request->user()->id}",
        ]);

        $request->user()->comment()->delete($request->input('id'));

        return response()->json(["delete" => true]);
    }


    public function index($post_id)
    {
        $comments = Users_Comment::where("post_id",$post_id)->paginate();

        return response()->json([self::RESPONSE_KEY=> $comments]);
    }


}