package com.washimit.questionandanswer.questionandanswer.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.MyEditText;
import com.washimit.questionandanswer.questionandanswer.custumView.TextViewCustem;
import com.washimit.questionandanswer.questionandanswer.fragment.ApoutFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.ConnectFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.CvFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.PageFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.SearchFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.StatisticsFragment;
import com.washimit.questionandanswer.questionandanswer.fragment.UserActionFragment;
import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
import com.washimit.questionandanswer.questionandanswer.interfac.wishilistedCallback;
import com.washimit.questionandanswer.questionandanswer.modle.Question;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener {

    ImageView add_to_wishiList, search, slide_par, sittings, book, communiction;
    LinearLayout wishilisted_page, comment_page, hom_page, book_page, cv_page, add_frend_page, apout_page, statistics,exit;
    Toolbar toolbar;
    android.support.v4.app.Fragment fragment;
    PreferenceClass preferenceClass;
    DrawerLayout drawer;
    NavigationView navigationView;
    boolean screnMode = true;
    Bundle bundle;
    PageFragment pageFragment;
    TextView test;
    ApiRequest request;
    Gson gson;
    PreferenceClass pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_main);
        request = new ApiRequest(this);
        gson = new Gson();
        pref=new PreferenceClass(this);
        bundle = getIntent().getExtras();
        preferenceClass = new PreferenceClass(this);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        slide_par = (ImageView) findViewById(R.id.slide_par);
        book = (ImageView) findViewById(R.id.book);
        communiction = (ImageView) findViewById(R.id.communiction);
        add_to_wishiList = (ImageView) findViewById(R.id.add_to_wishiList);
        search = (ImageView) findViewById(R.id.search);
        sittings = (ImageView) findViewById(R.id.sittings);
        toolbar = (Toolbar) findViewById(R.id.toolbar2);
        wishilisted_page = (LinearLayout) findViewById(R.id.wishilisted_page);
        apout_page = (LinearLayout) findViewById(R.id.apout_page);
        add_frend_page = (LinearLayout) findViewById(R.id.add_frend_page);
        exit = (LinearLayout) findViewById(R.id.exit);
        comment_page = (LinearLayout) findViewById(R.id.comment_page);
        hom_page = (LinearLayout) findViewById(R.id.hom_page);
        statistics = (LinearLayout) findViewById(R.id.statistics);
        book_page = (LinearLayout) findViewById(R.id.book_page);
        cv_page = (LinearLayout) findViewById(R.id.cv_page);
        test = (TextView) findViewById(R.id.test);
        slide_par.setOnClickListener(this);
        add_to_wishiList.setOnClickListener(this);
        search.setOnClickListener(this);
        wishilisted_page.setOnClickListener(this);
        comment_page.setOnClickListener(this);
        sittings.setOnClickListener(this);
        hom_page.setOnClickListener(this);
        book.setOnClickListener(this);
        communiction.setOnClickListener(this);
        book_page.setOnClickListener(this);
        exit.setOnClickListener(this);
        statistics.setOnClickListener(this);
        cv_page.setOnClickListener(this);
        apout_page.setOnClickListener(this);
        add_frend_page.setOnClickListener(this);
        openFragment(new PageFragment());




//        CountDownTimer timer=new CountDownTimer(10*1000,1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                millisUntilFinished/1000
//
//            }
//
//            @Override
//            public void onFinish() {
//
//            }
//        };
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (fragment instanceof PageFragment && drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else if (fragment instanceof PageFragment) {
            super.onBackPressed();
        }
//        else if (fragment instanceof UserActionFragment ) {
//            openFragment(new PageFragment());
//        } else if (fragment instanceof CvFragment ) {
//            openFragment(new PageFragment());
//
//        }

        else {
            openFragment(new PageFragment());
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.slide_par: {
                drawer.openDrawer(GravityCompat.END);
                break;
            }
            case R.id.add_to_wishiList: {
                addToWishiList(pageFragment.getQuestionObject());
                break;
            }
            case R.id.wishilisted_page: {
                openFragment(UserActionFragment.newInstance(1));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.hom_page: {
                openFragment(new PageFragment());
                drawer.closeDrawer(GravityCompat.END);
                //  finish();
                break;
            }
            case R.id.comment_page: {
                openFragment(UserActionFragment.newInstance(2));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.book: {
                pageNumberDilog();
                break;
            }
            case R.id.communiction: {
                openFragment(new ConnectFragment());
                break;
            }
            case R.id.book_page: {
                openFragment(UserActionFragment.newInstance(3));
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.cv_page: {
                openFragment(new CvFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.apout_page: {
                openFragment(new ApoutFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.statistics: {
                openFragment(new StatisticsFragment());
                drawer.closeDrawer(GravityCompat.END);
                break;
            }
            case R.id.add_frend_page: {
                drawer.closeDrawer(GravityCompat.END);
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "قم بتحميل تطبيق قدرات من على المتجر");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "قم بتحميل تطبيق قدرات من على المتجر");
                startActivity(Intent.createChooser(sharingIntent, "أخبر صديق"));
                break;
            }
            case R.id.search: {
                openFragment(new SearchFragment());
                break;
            }
            case R.id.sittings: {

                sittings.setClickable(false);
                settingsDialog();
                break;
            }
            case R.id.exit: {
                 startActivity(new Intent(this,LogInActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK));
                pref.clear();
                 finish();

                break;
            }

        }
    }


    public void openFragment(android.support.v4.app.Fragment fragment) {
        this.fragment = fragment;
        if (fragment instanceof PageFragment) {
            this.pageFragment = (PageFragment) fragment;
            pageFragment.getQuestionId(new wishilistedCallback() {
                @Override
                public void wishilistedCollback(boolean x) {
                    isWishilisted(x);
                }
            });
            toolbar.setVisibility(View.VISIBLE);
            add_to_wishiList.setVisibility(View.VISIBLE);
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, pageFragment).commit();

        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.content_main, fragment).commit();
            toolbar.setVisibility(View.GONE);
            add_to_wishiList.setVisibility(View.GONE);
        }
    }


    public void isWishilisted(boolean x) {
        if (!x) {
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_favorite);
        } else {
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_is_favorite);

        }


    }


    private void settingsDialog() {
        boolean ferst;
        Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dilog_setings);

        Button day_mode = (Button) dialog.findViewById(R.id.day_mode);
        final TextViewCustem proogresbar = (TextViewCustem) dialog.findViewById(R.id.proogresbar);
        Button night_mode = (Button) dialog.findViewById(R.id.night_mode);
        final SeekBar text_size = (SeekBar) dialog.findViewById(R.id.text_size);
        final Spinner text_font_type = (Spinner) dialog.findViewById(R.id.text_font_type);

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("TheSansArab-Light_0.ttf");
        arrayList.add("TheSansArab-Black_0.ttf");
        arrayList.add("TheSansArab-Plain_0.ttf");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(MainActivity.this, R.layout.spinner_dropdown_item, arrayList);

        text_font_type.setAdapter(arrayAdapter);


        text_font_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pageFragment.changefont();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        text_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                proogresbar.setText(progress + "%");
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pageFragment.changefont();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);

            }
        });
        day_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = true;
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pageFragment.changefont();

            }
        });
        night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = false;
                preferenceClass.setfontType(text_font_type.getSelectedItem().toString());
                preferenceClass.settextSize(text_size.getProgress());
                preferenceClass.setscrenMode(screnMode);
                pageFragment.changefont();

            }
        });


        screnMode = preferenceClass.getscrenMode();
        text_size.setProgress(preferenceClass.gettextSize());
        pageFragment.changefont();
        sittings.setClickable(true);
        dialog.show();


    }


    public void pageNumberDilog() {
        final Button go_to_page, cancel;
        final MyEditText page_number;
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dilog_go_to_page);
        go_to_page = (Button) dialog.findViewById(R.id.go_to_page);
        cancel = (Button) dialog.findViewById(R.id.cancel);
        page_number = (MyEditText) dialog.findViewById(R.id.page_number);


        go_to_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page_number.getText().toString().equalsIgnoreCase("") || page_number.getText().toString().length() > 6) {

                    InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                    //  dialog.dismiss();
                } else {
                    seQuestion(page_number.getText().toString());
                    InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                    dialog.dismiss();
                }
            }

        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager in = (InputMethodManager) getApplication().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(page_number.getWindowToken(), 0);
                dialog.cancel();
            }
        });


        dialog.show();
    }


    public void seQuestion(String question) {
        pageFragment.getQuestion(question);
    }


    public void addToWishiList(Question question) {

        if (question.is_wishlisted) {
            question.is_wishlisted = false;
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_favorite);
        } else {
            question.is_wishlisted = true;
            add_to_wishiList.setBackgroundResource(R.drawable.ic_icon_is_favorite);

        }
        HashMap<String, String> map = new HashMap<>();

        map.put("question_id", question.id + "");
        request.setRequst("wishlist", map, new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {
                Log.d("wishlist", response);
            }

            @Override
            public void ErrorListener() {

            }
        });


    }

}
