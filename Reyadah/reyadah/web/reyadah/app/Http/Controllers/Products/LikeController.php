<?php

namespace App\Http\Controllers\Products;

use App\__ModelsProducts\Like;
use App\__ModelsProducts\Product;
use App\Http\Constants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LikeController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id,status_id,' . Constants::_STATUS_ACTIVE
        ]);

        abort_if_blocked(Product::find($request->product_id)->user);
        $like = Like::where([
            ['product_id', $request->product_id],
            ['user_id', $request->user()->id],
        ])->first();

        if (isset($like)) {
            $status = $like->status_id == Constants::_STATUS_ACTIVE ? Constants::_STATUS_DELETED : Constants::_STATUS_ACTIVE;
            $like->status_id = $status;
            $like->update();
        } else {
            $like = new Like();
            $like->product_id = $request->product_id;
            $like->user_id = $request->user()->id;
            $like->save();
        }
        $total = Like::where([
            ['product_id', $request->product_id],
            ['status_id', Constants::_STATUS_ACTIVE],
        ])->count();

        $product = Product::find($request->product_id);
        $product->total_likes = $total;
        $product->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["total_likes" => $total]));
    }
}
