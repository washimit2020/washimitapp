package com.it.washim.bakkar.activity;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyEditTextView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.interfase.OnRequestCollBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogInActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.password)
    MyEditTextView password;
    @BindView(R.id.email)
    MyEditTextView email;
    @BindView(R.id.log_in)
    MyButtonView log_in;
    @BindView(R.id.create_account)
    MyTextView create_account;

    HashMap<String, String> map = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        create_account.setOnClickListener(this);
        log_in.setOnClickListener(this);





    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.create_account:
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                break;

            case R.id.log_in:
                showDialog("جاري تسجيل الدخول...");
                map.put("email", email.getText() + "");
                map.put("password", password.getText() + "");
                map.put("reg_id",  FirebaseInstanceId.getInstance().getToken()+ "");
                request().stringRequest(Request.Method.POST, getApplicationContext(), "login", map, new OnRequestCollBack() {
                    @Override
                    public void onResponse(String response) throws JSONException {
                        JSONObject object = new JSONObject(response);
                        shardPref().setUser(object.getString("user"));
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                        hideDialog();
                        finish();
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hideDialog();
                        Toast.makeText(getApplicationContext(), "توجد مشكلة بالاتصال بالانترنت او ان الايميل مسجل من قبل !", Toast.LENGTH_LONG).show();

                    }
                });

                break;

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (shardPref().getUser() != null) {
            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            finish();
        }
    }
}
