package com.washimit.it.ryadah.model.product;

import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.model.user.User;

import java.util.ArrayList;

/**
 * Created by mohammad on 4/17/2017.
 */

public class Product {
    private int id;
    private String name;
    private int number_of_eaters;
    private String ingredients;
    private int price;
    private double rating;
    private int total_likes;
    private int is_sponsored;
    private int user_id;
    private int status_id;
    private String created_at;
    private String updated_at;
    private ArrayList<Comment> comments;
    int is_liked;
    int is_wishlisted;
    private RatedProduct rated;
    private User user;
    User shared_with;
    private boolean isSelected = false;

    public Product(int id, String name, int number_of_eaters, String ingredients, int price, double rating, int total_likes, int is_sponsored, int user_id, int status_id, String created_at, String updated_at, ArrayList<Comment> comments, int is_liked, int is_wishlisted, RatedProduct rated, User user, User shared_with, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.number_of_eaters = number_of_eaters;
        this.ingredients = ingredients;
        this.price = price;
        this.rating = rating;
        this.total_likes = total_likes;
        this.is_sponsored = is_sponsored;
        this.user_id = user_id;
        this.status_id = status_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.comments = comments;
        this.is_liked = is_liked;
        this.is_wishlisted = is_wishlisted;
        this.rated = rated;
        this.user = user;
        this.shared_with = shared_with;
        this.isSelected = isSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber_of_eaters() {
        return number_of_eaters;
    }

    public void setNumber_of_eaters(int number_of_eaters) {
        this.number_of_eaters = number_of_eaters;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getTotal_likes() {
        return total_likes;
    }

    public void setTotal_likes(int total_likes) {
        this.total_likes = total_likes;
    }

    public int getIs_sponsored() {
        return is_sponsored;
    }

    public void setIs_sponsored(int is_sponsored) {
        this.is_sponsored = is_sponsored;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public int getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(int is_liked) {
        this.is_liked = is_liked;
    }

    public int getIs_wishlisted() {
        return is_wishlisted;
    }

    public void setIs_wishlisted(int is_wishlisted) {
        this.is_wishlisted = is_wishlisted;
    }

    public RatedProduct getRated() {
        return rated;
    }

    public void setRated(RatedProduct rated) {
        this.rated = rated;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getShared_with() {
        return shared_with;
    }

    public void setShared_with(User shared_with) {
        this.shared_with = shared_with;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}