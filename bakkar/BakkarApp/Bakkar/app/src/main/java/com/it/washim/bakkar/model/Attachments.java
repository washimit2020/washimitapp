package com.it.washim.bakkar.model;

import java.io.Serializable;

/**
 * Created by mohammad on 8/7/2017.
 */

public class Attachments {
    public int id;
    public String created_at;
    public String updated_at;
    public int attachment_type_id;
}
