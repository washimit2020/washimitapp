//
//  ShareToPlace_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ShareToPlace_TableViewCell: UITableViewCell {

    @IBOutlet weak var selection: UIImageView!
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    
    //extends users model :3
    var branch:Branch!{
        didSet{
            updateView()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        selection.isHighlighted = selected
    }
    
    func updateView(){
        
        if Gate.is_show(){
            avatar.set_avatar_image(user_id: branch.user_id)
        }else{
            avatar.set_avatar_image(user_id: branch.id)
        }
        
        name.text = branch.name
        location.text = make_location_text(branch)
    }
}
