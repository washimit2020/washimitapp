package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Interfases.OnLoadMoreCollBack;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.activitys.BookDetailsActivity;
import com.it.washim.wojooohapp.activitys.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/27/2017.
 */

public class SearchAdapter extends RecyclerView.Adapter {
    private final int SEARCH_ITEM = 0;
    private final int PROGRIS_ITEM = 1;
    Context context;
    ArrayList<Post> list;
    boolean loading = true;
    String url;
    OnLoadMoreCollBack onLoadMoreCollBack;

    public SearchAdapter(Context context, ArrayList<Post> list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    if (url != null) {

                        if (layoutManager.findLastVisibleItemPosition() + 3 >= layoutManager.getItemCount()) {

                            if (!loading) {

                                if (onLoadMoreCollBack != null)
                                    onLoadMoreCollBack.onLoad();
                                loading = true;
                            }
                        }


                    }

                }
            });

        }


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new SearchViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progres, parent, false));

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SearchViewHolder) {
            final Post post = list.get(position);
            ((SearchViewHolder) holder).title.setText(post.title);

             holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).openActivity(context, BookDetailsActivity.class, "book", ((MainActivity) context).getGson().toJson(post));

                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? PROGRIS_ITEM : SEARCH_ITEM;
    }


    public void setOnLoadMoreLesnar(OnLoadMoreCollBack onLoadMoreCollBack) {
        this.onLoadMoreCollBack = onLoadMoreCollBack;
    }

    public void isLoading() {
        loading = false;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_title)
        MyTextView title;

        public SearchViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {


        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }

}
