package com.it.washmit.bookgoodmorning.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.it.washmit.bookgoodmorning.model.comment;
import com.it.washmit.bookgoodmorning.model.highlight;
import com.it.washmit.bookgoodmorning.model.pages;
import com.it.washmit.bookgoodmorning.model.searchResult;
import com.it.washmit.bookgoodmorning.model.wishiListed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NgocTri on 11/7/2015.
 */
public class DataBHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "good.sqlite";
    public static final String DBLOCATION = "/data/data/com.it.washmit.bookgoodmorning/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DataBHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "highlight");
        return numRows;
    }


    //get commaend
    public List<pages> getListpagestList() {
        pages pages = null;
        List<pages> pagestList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.ID,pages.TITLE,pages.BADY,pages.IS_WISHILSTED,pages.COMMENT,images.PATH FROM pages LEFT JOIN images ON pages.ID=images.PAGE_ID ORDER BY pages.id DESC", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5));
            pagestList.add(pages);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return pagestList;
    }

    public List<highlight> getListhighlightList(int id) {
        highlight highlight = null;
        List<highlight> highlightList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT highlight.START,highlight.END,highlight.COLOR_ID FROM highlight  WHERE highlight.PAGE_ID=" + id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            highlight = new highlight(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));
            highlightList.add(highlight);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return highlightList;
    }

    public List<wishiListed> getListWishilsted() {
        wishiListed listeds = null;
        List<wishiListed> wishiListeds = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.ID,pages.TITLE FROM pages WHERE IS_WISHILSTED = 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listeds = new wishiListed(cursor.getInt(0), cursor.getString(1));
//            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3));
            wishiListeds.add(listeds);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return wishiListeds;
    }

    public List<comment> getListComment() {
        comment listeds = null;
        List<comment> commentList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.ID,pages.COMMENT,pages.TITLE FROM pages WHERE pages.COMMENT  NOT NULL", null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listeds = new comment(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
//            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3));
            commentList.add(listeds);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return commentList;
    }


    public List<searchResult> search(String s) {
        searchResult pages = null;
        List<searchResult> searchResults = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.ID,pages.TITLE,pages.BADY FROM pages WHERE pages.BADY LIKE '%" + s + "%' ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            pages = new searchResult(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            searchResults.add(pages);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return searchResults;
    }

    public int isWishilisted(int id) {
        int isWishilisted = 0;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.IS_WISHILSTED FROM pages WHERE pages.ID = " + id + " LIMIT 1", null);
        cursor.moveToFirst();
        isWishilisted = cursor.getInt(0);

        cursor.close();
        closeDatabase();

        return isWishilisted;
    }

    public int numberRead() {
        int numberRead = 0;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT COUNT (*) FROM  pages WHERE pages.IS_READ = 1", null);
        cursor.moveToFirst();
        numberRead = cursor.getInt(0);

        cursor.close();
        closeDatabase();

        return numberRead;
    }

    public boolean insertRead(int id) {

        openDatabase();
        String strSQL = "UPDATE pages SET IS_READ = 1 where ID = " + id;
        mDatabase.execSQL(strSQL);
        closeDatabase();

        return true;
    }

    //insert commaned
    public boolean insertHighlight(int start, int end, int page_id, int color_id) {
        deleteHighlight(start, end, page_id);

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("START", start);
        contentValues.put("END", end);
        contentValues.put("PAGE_ID", page_id);
        contentValues.put("COLOR_ID", color_id);
        db.insert("highlight", null, contentValues);
        return true;
    }

    public boolean insertWishilsted(int id) {

        openDatabase();
        String strSQL = "UPDATE pages SET IS_WISHILSTED = ((select count(*) from pages where ID = " + id + " AND IS_WISHILSTED = 0 LIMIT 1)>0) where ID = " + id;
        mDatabase.execSQL(strSQL);
        closeDatabase();

        return true;
    }


    public boolean insertComment(int id, String comment) {


        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("COMMENT", comment);
        db.update("pages", args, "ID" + "=" + id, null);
        return true;
    }

    public void deleteHighlight(int start, int end, int page_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("highlight", "PAGE_ID=? and START>=? and END<=?", new String[]{String.valueOf(page_id), String.valueOf(start), String.valueOf(end)});

    }
}
