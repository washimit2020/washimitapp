package com.washimit.it.ryadah.Interface;

import com.washimit.it.ryadah.model.Branche;

import java.util.ArrayList;

/**
 * Created by mohammad on 5/6/2017.
 */

public interface OnBranchCallback {
    public void branch(ArrayList<Branche> branches);
}
