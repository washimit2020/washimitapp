<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;
class NewsMainTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_main_types')->insert([
            [
                "id"=>Constants::NEWS_MAIN_TYPE_LOCAL,
                "name_en"=>"Local news",
                "name_ar"=>"أخبار محلية",
                "is_active"=>true
            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_INTERNATIONAL,
                "name_en"=>"International news",
                "name_ar"=>"أخبار دولية",
                "is_active"=>true
            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_SPORT,
                "name_en"=>"Sport news",
                "name_ar"=>"أخبار رياضية",
                "is_active"=>true

            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_ALRADYH_PUBLISH,
                "name_en"=>"Alradyh for delivery and publishing",
                "name_ar"=>"الرائدية للنشر و التوزيع",
                "is_active"=>false
            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_ALRADYH_VOLUNTEERS,
                "name_en"=>"Alradyh volunteer team",
                "name_ar"=>"فريق الرائدية التطوعي",
                "is_active"=>false
            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_ALRADYH_FEMALES,
                "name_en"=>"Alradyh female team",
                "name_ar"=>"فريق الرائدية النسائي",
                "is_active"=>false
            ],
            [
                "id"=>Constants::NEWS_MAIN_TYPE_ALRADYH_NETWORK,
                "name_en"=>"Alradyh network",
                "name_ar"=>"شبكة الرائدية",
                "is_active"=>false
            ]
        ]);
    }
}
