//
//  Shared_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/20/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

class Shared_CollectionViewController: ItemsLoader_CollectionViewController {
    
    let reuseIdentifier = "share"
    
    var type:Int! = Constants.TYPE_STORE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        link = RequestLinks.share.rawValue
        beginWithModel = "shared"
        
        self.collectionView!.register(UINib(nibName: "ShareProduct_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        clearData()
        self.emptyLabel?.frame.origin.y = collectionView!.frame.height/4
    }

    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ShareProduct_CollectionViewCell
        cell.shared = items[indexPath.row] as? Shared
        cell.accept.addTarget(nil, action: #selector(update_share(_:)), for: .touchUpInside)
        cell.decline.addTarget(nil, action: #selector(update_share(_:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.collectionView!.frame.width, height: 100)
    }
}

extension Shared_CollectionViewController:ItemsLoaderDelegate{
    func dataFiller(data: [JSON]) {
        for item in data{
            items.append(Shared(item))
        }
    }
    
    func unfollow(_ sender:AspectFitButton){
        sender.isEnabled = false
        
        let point = sender.convert(CGPoint.zero, to: collectionView)
        
        guard let index = collectionView!.indexPathForItem(at: point), let user = items[index.row] as? User else{
            return
        }
        apiRequest.call_follow_unfollow(user.id!)
        
        collectionView?.performBatchUpdates({
            self.items.remove(at: index.row)
            self.collectionView?.deleteItems(at: [index])
        }, completion: {
            if($0 && self.items.count == 0){
                self.showEmpty()
            }
        })
    }
    
    func update_share(_ sender:UIButton){
        let point = sender.convert(CGPoint.zero, to: collectionView)
        guard let index = collectionView?.indexPathForItem(at: point) else{
            return
        }
        
        let share = items[index.row] as? Shared
        
        apiRequest.make_request(parameters: ["status_id":"\(sender.tag)"], apiImages: nil, link: link + "/\(share!.id!)/update", method: .post,completion: nil)
        share?.status_id = sender.tag
        collectionView?.reloadItems(at: [index])
    }

}
