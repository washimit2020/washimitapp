//
//  MainPlaces_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class MainPlaces_CollectionViewController: ItemsLoader_CollectionViewController {
    var type:Int?{
        didSet{
            var index  = type!-1
            
            if index > 2{
                index = 2
                self.link = RequestLinks.festivals.rawValue
                self.beginWithModel = "festivals"
                parameters["follow_type"] = "\(Constants.TYPE_FOLLOW_FESTIVAL)"
                
            }else{
                parameters["follow_type"] = "\(Constants.TYPE_FOLLOW_USER)"
                self.link = RequestLinks.get_users.rawValue + "\(type!)"
                self.beginWithModel = "users"
                
            }
            userIcon = UIImage(named:Constants.icon_name[index])
            
        }
    }
    
    var userIcon:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        clearData()
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "festivalHeader", for: indexPath)
            
            if Gate.is_authed(){
                if let addFestival = header.viewWithTag(1) as? UIButton{
                    addFestival.isEnabled = true
                    addFestival.addTarget(self, action: #selector(go_to_add_festival), for: .touchUpInside)
                }
            }
            return header
        default:
            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if type  == Constants.TYPE_FESTIVAL{
            return CGSize(width: collectionView.frame.width, height: 42)
        }
        return .zero
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var reuseIdentifier = "place"
        if indexPath.row == 0
        {
            reuseIdentifier = "header"
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomePlaces_CollectionViewCell
        cell.userIcon.image = userIcon
        cell.user = items[indexPath.row] as? User
        cell.setupView()
        if let header = cell as? HomePlaces_HeadingCell {
            let gesture = UITapGestureRecognizer(target: self, action: #selector(didTap))
            header.userCovers.addGestureRecognizer(gesture)
        }
        if Gate.is_authed() {
            cell.followUser.addTarget(nil, action: #selector(followUnFollow(_:)), for: .touchUpInside)
        }
        
        make_materialCell(cell)
        // Configure the cell
        
        return cell
    }
    
    func didTap(){
        self.collectionView(collectionView!, didSelectItemAt: IndexPath(row: 0, section: 0))
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !Gate.is_authed(){
            return
        }
        move_to_details(viewController: self, cell: (collectionView.cellForItem(at: indexPath) as!  HomePlaces_CollectionViewCell))
        
        super.collectionView(collectionView, didSelectItemAt: indexPath)
    }
}

extension MainPlaces_CollectionViewController{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == 0 {
            return CGSize(width: collectionView.frame.width - 16, height: 300)
        }
        return CGSize(width: collectionView.bounds.width/2 -  8, height: 155)
    }
    
}


extension MainPlaces_CollectionViewController:ItemsLoaderDelegate{
    
    public func followUnFollow(_ sender:AnyObject) {
        
        var sender = sender
        var user_id = 0
        while ((sender.superview) != nil){
            if let cell = (sender.superview) as? HomePlaces_CollectionViewCell{
                user_id = cell.user!.id!
                
                if cell.user!.followed{
                    cell.user?.totalFollowers! -= 1
                }else{
                    cell.user?.totalFollowers! += 1
                }
                
                cell.user?.followed = !cell.user!.followed!
                
                
                collectionView?.reloadItems(at: [collectionView!.indexPath(for: cell)!])
                break
            }
            sender = sender.superview as AnyObject
        }
        
        apiRequest.call_follow_unfollow(user_id, parameters["follow_type"]!)
    }
    
    func dataFiller(data: [JSON]) {
        for userData in data{
            self.items.append(Festival(userData))
        }
        
    }
    
    func go_to_add_festival()  {
        let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.addFestival.rawValue)
        self.navigationController?.pushViewController(controller, animated: true)
    }
}


