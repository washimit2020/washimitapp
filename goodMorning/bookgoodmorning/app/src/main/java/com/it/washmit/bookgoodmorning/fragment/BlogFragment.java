package com.it.washmit.bookgoodmorning.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.adapter.BolgAdapter;
import com.it.washmit.bookgoodmorning.coustumView.TextViewCustem;
import com.it.washmit.bookgoodmorning.helper.ApiRequest;
import com.it.washmit.bookgoodmorning.interfac.OnRequestCollBack;
import com.it.washmit.bookgoodmorning.model.Post;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;


public class BlogFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    Gson gson;
    ArrayList<Post> post;
    RecyclerView recyclerView;
    TextViewCustem no_net, hed;
    SwipeRefreshLayout refreshLayout;
    BolgAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blog, container, false);

        gson = new Gson();
        post = new ArrayList<>();
        adapter = new BolgAdapter(getActivity(), post);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        no_net = (TextViewCustem) view.findViewById(R.id.no_net);
        hed = (TextViewCustem) view.findViewById(R.id.hed);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(adapter);

        refreshLayout.setOnRefreshListener(this);
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
                get();
            }
        });

        return view;
    }

    public void get() {

        ApiRequest.getInstance(getActivity()).stringRequest(new OnRequestCollBack() {
            @Override
            public void onResponse(String r) throws JSONException {
                post.clear();
                JSONObject object = new JSONObject(r);
                JSONObject response = object.getJSONObject("response");
                JSONObject blog = response.getJSONObject("blog");

                hed.setText(blog.getString("description"));
                Log.d("fsafasf", r);
                post.addAll((Collection<? extends Post>) gson.fromJson(response.getString("posts"), new TypeToken<ArrayList<Post>>() {
                }.getType()));

                Log.d("fsafasf", post.size() + "");


                adapter.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);

                no_net.setVisibility(View.GONE);

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                refreshLayout.setRefreshing(false);
                no_net.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRefresh() {
        get();
    }
}
