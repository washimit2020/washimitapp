package com.it.washm.alrayaduah.model;

import java.util.ArrayList;

/**
 * Created by washm on 1/10/17.
 */

public class NewsType {

    String id;
    String name_en;
    String name_ar;
    String is_active;
    ArrayList<NewsSubType> sub;

    public NewsType(String id, String name_en, String name_ar, String is_active, ArrayList<NewsSubType> sub) {
        this.id = id;
        this.name_en = name_en;
        this.name_ar = name_ar;
        this.is_active = is_active;
        this.sub = sub;
    }

    @Override
    public String toString() {
        return this.name_ar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public ArrayList<NewsSubType> getSub() {
        return sub;
    }

    public void setSub(ArrayList<NewsSubType> sub) {
        this.sub = sub;
    }
}
