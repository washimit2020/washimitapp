package com.washimit.it.ryadah.model;


import com.washimit.it.ryadah.model.user.Rated;
import com.washimit.it.ryadah.model.user.SocialMedia;
import com.washimit.it.ryadah.model.user.Type;

/**
 * Created by mohammad on 4/18/2017.
 */

public class Block {

    private int id;
    private String name;
    private String email;
    private String phone_number;
    private String info;
    private String sector_name;
    private String street_name;
    private float lat;
    private float lng;
    private double rating;
    private String created_at;
    private String updated_at;
    private int followed_by_count;
    private int followed;
    private Rated rated;
    private SocialMedia social_media;
    private Type type;

    public Block(int id, String name, String email, String phone_number, String info, String sector_name, String street_name, float lat, float lng, double rating, String created_at, String updated_at, int followed_by_count, int followed, Rated rated, SocialMedia social_media, Type type) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.info = info;
        this.sector_name = sector_name;
        this.street_name = street_name;
        this.lat = lat;
        this.lng = lng;
        this.rating = rating;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.followed_by_count = followed_by_count;
        this.followed = followed;
        this.rated = rated;
        this.social_media = social_media;
        this.type = type;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSector_name() {
        return sector_name;
    }

    public void setSector_name(String sector_name) {
        this.sector_name = sector_name;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getFollowed_by_count() {
        return followed_by_count;
    }

    public void setFollowed_by_count(int followed_by_count) {
        this.followed_by_count = followed_by_count;
    }

    public int getFollowed() {
        return followed;
    }

    public void setFollowed(int followed) {
        this.followed = followed;
    }

    public Rated getRated() {
        return rated;
    }

    public void setRated(Rated rated) {
        this.rated = rated;
    }

    public SocialMedia getSocial_media() {
        return social_media;
    }

    public void setSocial_media(SocialMedia social_media) {
        this.social_media = social_media;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}