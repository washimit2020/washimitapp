package com.it.washmit.bookgoodmorning.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.coustumView.TextViewCustem;
import com.it.washmit.bookgoodmorning.model.Vedio;

import java.util.ArrayList;

import static android.R.id.list;

/**
 * Created by mohammad on 10/24/2017.
 */

public class VedioAdapter extends RecyclerView.Adapter {

    Context context;
    ArrayList<Vedio> vedios;
    Resources res;

    public VedioAdapter(Context context, ArrayList<Vedio> vedios) {
        this.context = context;
        this.vedios = vedios;
        res = context.getResources();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VedioViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vedio, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VedioViewHolder) {
            final Vedio vedio = vedios.get(position);

            int resID = res.getIdentifier(vedio.imageNmae, "drawable", "com.it.washmit.bookgoodmorning");

            Drawable drawable = res.getDrawable(resID, null);

            ((VedioViewHolder) holder).imageView.setImageDrawable(drawable);
            ((VedioViewHolder) holder).imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent applicationIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(vedio.url.replace("https", "youtube")));
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(vedio.url));
                    try {
                        context.startActivity(applicationIntent);
                    } catch (ActivityNotFoundException ex) {
                        context.startActivity(browserIntent);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return vedios.size();
    }

    public class VedioViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;

        public VedioViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);


        }
    }
}
