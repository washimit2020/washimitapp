package com.washimit.it.ryadah.Fragment.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Fragment.MangaFragment;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MyProfileInfoActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Following;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyProfileFragment extends Fragment implements View.OnClickListener {

    User user;
    SharedPref pref;
    TextViewCast fragment_my_profile_follow_number, fragment_my_profile_user_name, fragment_my_profile_user_info, fragment_my_profile_user_profile_edit;
    ImageView fragment_my_profile_user_profile_page, fragment_my_profile_user_img_type;
    ApiRequest request;
    Gson gson;
    Following following;
    boolean isSelect;

    @BindView(R.id.done)
    ImageView done;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.cancel)
    ImageView cancel;
    @BindView(R.id.fragment_my_profile_add_product_to_branch)
    ImageView fragment_my_profile_add_product_to_branch;
    @BindView(R.id.fragment_my_profile_add_product_to_show)
    ImageView fragment_my_profile_add_product_to_show;
    @BindView(R.id.fragment_my_profile_add_product)
    ImageView fragment_my_profile_add_product;
    @BindView(R.id.fragment_my_profile_add_branch)
    ImageView fragment_my_profile_add_branch;
    MangaFragment fragment;

    @Override

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, view);
        pref = new SharedPref(getActivity());
        user = pref.getUser();
        gson = new Gson();
        request = new ApiRequest(getActivity());

        fragment_my_profile_follow_number = (TextViewCast) view.findViewById(R.id.fragment_my_profile_follow_number);
        fragment_my_profile_user_name = (TextViewCast) view.findViewById(R.id.fragment_my_profile_user_name);
        fragment_my_profile_user_info = (TextViewCast) view.findViewById(R.id.fragment_my_profile_user_info);
        fragment_my_profile_user_profile_edit = (TextViewCast) view.findViewById(R.id.fragment_my_profile_user_profile_edit);

        fragment_my_profile_user_profile_page = (ImageView) view.findViewById(R.id.fragment_my_profile_user_profile_page);
        fragment_my_profile_user_img_type = (ImageView) view.findViewById(R.id.fragment_my_profile_user_img_type);

        fragment_my_profile_user_profile_edit.setOnClickListener(this);
        fragment_my_profile_add_product.setOnClickListener(this);
        fragment_my_profile_add_product_to_show.setOnClickListener(this);
        fragment_my_profile_add_product_to_branch.setOnClickListener(this);
        fragment_my_profile_user_profile_page.setOnClickListener(this);
        fragment_my_profile_add_branch.setOnClickListener(this);
        done.setOnClickListener(this);
        cancel.setOnClickListener(this);
        back.setOnClickListener(this);


        updateUi(user.getType().getId());


        return view;
    }

    private void updateUi(int userType) {
        fragment_my_profile_user_name.setText(user.getName());
        fragment_my_profile_user_info.setText(user.getInfo());

        Picasso.with(getActivity()).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().skipMemoryCache().placeholder(R.drawable.ic_profile_picture).into(fragment_my_profile_user_profile_page);

        switch (userType) {
            case Constant._TYPE_COSTUMER:
                fragment_my_profile_follow_number.setVisibility(View.GONE);
                openFragment(MangaFragment.newInstants(Constant._PAGE_CUSTOMER_DETAILS));
                break;

            case Constant._TYPE_STORE:
                fragment_my_profile_user_img_type.setImageDrawable(getResources().getDrawable(R.drawable.ic_store_20));
                getNumberOfFollow();
                fragment = (MangaFragment) MangaFragment.newInstants(Constant._PAGE_STORE_DETAILS);
                openFragment(fragment);
                fragment_my_profile_add_product.setVisibility(View.VISIBLE);
                fragment_my_profile_add_product_to_show.setVisibility(View.VISIBLE);

                break;
            case Constant._TYPE_SHOW:
                fragment = (MangaFragment) MangaFragment.newInstants(Constant._PAGE_SHOW_DETAILS);

                fragment_my_profile_user_img_type.setImageDrawable(getResources().getDrawable(R.drawable.ic_showroom_20));
                getNumberOfFollow();
                openFragment(fragment);
                fragment_my_profile_add_branch.setVisibility(View.VISIBLE);
                fragment_my_profile_add_product.setVisibility(View.VISIBLE);
                fragment_my_profile_add_product_to_branch.setVisibility(View.VISIBLE);

                break;


        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.fragment_my_profile_user_profile_edit:


                ((MainActivity) getActivity()).openFragment(new EditMyProfileFragment());


                break;

            case R.id.fragment_my_profile_add_product:


                ((MainActivity) getActivity()).openFragment(new AddProduactFragment());


                break;
            case R.id.fragment_my_profile_add_product_to_show:
                isSelect = !isSelect;
                setSelectProduct(isSelect);
                if (isSelect) {
                    fragment_my_profile_add_product_to_show.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_produce_to_store_active));

                    back.setVisibility(View.GONE);
                    done.setVisibility(View.VISIBLE);
                    cancel.setVisibility(View.VISIBLE);
                } else {
                    fragment_my_profile_add_product_to_show.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_new_produce_unactive));
                    back.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                    cancel.setVisibility(View.GONE);
                }

                break;
            case R.id.fragment_my_profile_add_product_to_branch:
                isSelect = !isSelect;
                setSelectProduct(isSelect);
                if (isSelect) {
                    fragment_my_profile_add_product_to_branch.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_produce_to_store_active));
                    back.setVisibility(View.GONE);
                    done.setVisibility(View.VISIBLE);
                    cancel.setVisibility(View.VISIBLE);
                } else {
                    fragment_my_profile_add_product_to_branch.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_produce_to_store_unactive));
                    back.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                    cancel.setVisibility(View.GONE);
                }

                break;

            case R.id.fragment_my_profile_add_branch:
                ((MainActivity) getActivity()).openFragment(new AddBranchFragment());


                break;
            case R.id.done:
                if (user.getType().getId() == Constant._TYPE_STORE) {
                    shaerSelectProduct(Constant._TYPE_SHARE_USERS);
                } else {
                    shaerSelectProduct(Constant._TYPE_SHARE_BRANCH);

                }
                break;
            case R.id.cancel:
                fragment_my_profile_add_product_to_branch.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_new_produce_unactive));
                fragment_my_profile_add_product_to_show.setImageDrawable(getResources().getDrawable(R.drawable.ic_add_new_produce_unactive));
                isSelect = false;
                setSelectProduct(isSelect);
                back.setVisibility(View.VISIBLE);
                done.setVisibility(View.GONE);
                cancel.setVisibility(View.GONE);

                break;

            case R.id.back:
                ((MainActivity) getActivity()).onBackPressed();
                break;

            case R.id.fragment_my_profile_user_profile_page:

                Intent intent = new Intent(getActivity(), MyProfileInfoActivity.class);
                intent.putExtra("user", gson.toJson(pref.getUser()));
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                getActivity().startActivity(intent);


                break;


        }
    }


    private void getNumberOfFollow() {
        request.MultipartRequest(Request.Method.GET, false, "followed-by", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject msg;
                JSONObject resultJ;

                resultJ = new JSONObject(result);

                msg = resultJ.getJSONObject("msg");
                following = gson.fromJson(msg.getString("users"), Following.class);

                fragment_my_profile_follow_number.setText(String.valueOf(following.getTotal()) + " " + getString(R.string.follow));
            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Ion.getDefault(getActivity()).cancelAll(getActivity());

    }

    public void setSelectProduct(boolean selectProduct) {
        fragment.setSelectProduct(selectProduct);
    }


    public void shaerSelectProduct(String type) {

        fragment.shaerSelectProduct(type);

    }

    public void openFragment(Fragment fragment) {
        getFragmentManager().beginTransaction().replace(R.id.deta, fragment).commit();

    }
}
