package com.it.washim.bakkar.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
//import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyEditTextView;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.name)
    MyEditTextView name;
    @BindView(R.id.password)
    MyEditTextView password;
    @BindView(R.id.email)
    MyEditTextView email;
    @BindView(R.id.sign_up)
    MyButtonView sign_up;
    HashMap<String, String> map = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sgin_up);
        ButterKnife.bind(this);
        sign_up.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.sign_up:
                if (isValid()) {
                    map.clear();
                    showDialog("جاري عمل حساب ...");
                    map.put("name", name.getText().toString() + "");
                    map.put("email", email.getText().toString() + "");
                    map.put("password", password.getText().toString() + "");
                    map.put("reg_id", FirebaseInstanceId.getInstance().getToken() + "");

                    request().stringRequest(Request.Method.POST, this, "register", map, new OnRequestCollBack() {
                        @Override
                        public void onResponse(String response) throws JSONException {
                            JSONObject object = new JSONObject(response);
                            shardPref().setUser(object.getString("user"));
                            startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            hideDialog();
                            finish();
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            hideDialog();
                            Toast.makeText(getApplicationContext(), "توجد مشكلة بالاتصال بالانترنت او ان الايميل مسجل من قبل !", Toast.LENGTH_LONG).show();

                        }
                    });
                }

                break;


        }
    }


    public boolean isValid() {

        if (name.getText().toString().length() < 3) {
            name.requestFocus();
            name.setError("يجب ان يتجاوز 3 احرف على الاقل");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            email.requestFocus();
            email.setError("ايميل غير صحيح");
            return false;
        } else  if (password.getText().toString().length() <6) {
            password.requestFocus();
            password.setError("يجب ان يتجاوز 6 احرف على الاقل");
            return false;
        }

        return true;

    }
}
