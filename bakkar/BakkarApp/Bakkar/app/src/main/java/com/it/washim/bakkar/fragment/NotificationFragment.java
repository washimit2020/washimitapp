package com.it.washim.bakkar.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.MyVideoAdapter;
import com.it.washim.bakkar.adapter.NotificationAdapter;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Notification;
import com.it.washim.bakkar.model.Posts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.notification_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.ratray)
    MyButtonView ratray;
    @BindView(R.id.na_data)
    MyTextView na_data;
    NotificationAdapter adapter;
    ArrayList<Notification> list;
    String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NotificationAdapter( getActivity(),list, recyclerView);
        recyclerView.setAdapter(adapter);


        swipeRefresh.setOnRefreshListener(this);
        ratray.setOnClickListener(this);


        adapter.setOnLoadMoreLesnar(new OnLoadMorCollBack() {
            @Override
            public void onLoad() {
                getNextNotification();
            }
        });


        swipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(true);
                getNotification();
            }
        });


        return view;
    }

    private void getNotification() {

        na_data.setVisibility(View.GONE);
        ratray.setVisibility(View.GONE);
        request().stringRequest(Request.Method.GET, getActivity(), "notifications", null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                swipeRefresh.setRefreshing(false);
                list.clear();
                JSONObject object = new JSONObject(response);
                JSONObject notifications = object.getJSONObject("notifications");


                list.addAll((Collection<? extends Notification>) gson().fromJson(notifications.getString("data"), new TypeToken<ArrayList<Notification>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = notifications.getString("next_page_url");
                adapter.setUrl(url);
                if (list.size() == 0) {
                    na_data.setText("لا يوجد تنبيهات");
                    na_data.setVisibility(View.VISIBLE);
                    ratray.setVisibility(View.GONE);
                } else {
                    na_data.setVisibility(View.GONE);
                    ratray.setVisibility(View.GONE);
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                na_data.setText("لا يوجد اتصال بالانترنت");
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);
            }
        });


    }

    private void getNextNotification() {

        request().stringRequest(Request.Method.GET, getActivity(), url.replace(Constant.URL, ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                swipeRefresh.setRefreshing(false);
                JSONObject object = new JSONObject(response);
                JSONObject notifications = object.getJSONObject("notifications");
                list.addAll((Collection<? extends Notification>) gson().fromJson(notifications.getString("data"), new TypeToken<ArrayList<Notification>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = notifications.getString("next_page_url");
               adapter.setUrl(url);
                if (list.size() == 0) {
                    na_data.setText("لا يوجد تنبيهات");
                    na_data.setVisibility(View.VISIBLE);
                    ratray.setVisibility(View.GONE);
                } else {
                    na_data.setVisibility(View.GONE);
                    ratray.setVisibility(View.GONE);
                }

                adapter.isLoading();

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                na_data.setText("لا يوجد اتصال بالانترنت");
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    public void onRefresh() {
        getNotification();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ratray:
                swipeRefresh.setRefreshing(true);
                getNotification();

                break;


        }
    }
}
