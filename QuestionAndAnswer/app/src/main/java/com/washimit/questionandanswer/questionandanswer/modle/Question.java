package com.washimit.questionandanswer.questionandanswer.modle;

import java.util.ArrayList;

/**
 * Created by mohammad on 6/4/2017.
 */

public class Question {
    public int id;
    public String title;
    public String body;
    public String created_at;
    public String updated_at;
    public int section_id;
    public boolean is_wishlisted;
    public Comment comment;
    public ArrayList<Answers> answers;
    public ArrayList<Attachments> attachments;

}