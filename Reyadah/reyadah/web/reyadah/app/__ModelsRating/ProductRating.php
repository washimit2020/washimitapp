<?php

namespace App\__ModelsRating;

use App\__ModelsProducts\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    protected $fillable=['user_id','product_id','quality','price','style','ingredients'];

    /**
     * to get the place
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    /**
     * to get the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
