package com.it.washim.wojooohapp.Halper;

/**
 * Created by mohammad on 9/11/2017.
 */

public class Constant {
    //  public static final String URL = "http://192.168.0.101/wojoooh/api/";
//    public static final String IMAGE_URL = "http://192.168.0.101/wojoooh/attachment/";

    public static final String URL = "http://wojooh.wojoooh.com/api/";
    public static final String IMAGE_URL = "http://wojooh.wojoooh.com/attachment/";


    public static final int PAGES_SERVICES = 1;
    public static final int PAGES_PUBLISH_REQUEST = 2;
    public static final int PAGES_BUY_REQUEST = 3;
    public static final int PAGES_SPONSOR = 4;
    public static final int PAGES_BOOKS_SHOWROOM = 5;
    public static final int PAGES_SELLING_PLACES = 6;
    public static final int PAGES_BOOK = 7;

    //BEGIN ATTACHMENTS
    public static final int ATTACHMENT_TYPE_PDF = 1;
    public static final int ATTACHMENT_TYPE_COVER = 2;
    public static final int ATTACHMENT_TYPE_IMAGE = 3;

}
