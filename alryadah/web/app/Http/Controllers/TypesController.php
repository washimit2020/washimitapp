<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ResponseHelper;
use App\NewsMainType;
use App\NewsStatus;
use App\UserAttachmentType;
use App\UserRole;
use Illuminate\Http\Request;

class TypesController extends BaseController
{
    /**
     * @return array
     */
    public function user_roles(){
        return ResponseHelper::successResponse(["roles"=>UserRole::all()]);
    }

    /**
     * @return array
     */
    public function news_types(){
        return ResponseHelper::successResponse(["news_main_types"=>NewsMainType::with('sub')->get()]);
    }

    /**
     * @return array
     */
    public function news_statuses(){
        return ResponseHelper::successResponse(["news_statuses"=>NewsStatus::all()]);
    }

    /**
     * @return array
     */
    public function user_attachment_types(){
        return ResponseHelper::successResponse(["user_attachment_types"=>UserAttachmentType::all()]);
    }


}
