<?php

namespace App\__ModelsProducts;

use App\Http\Constants;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $fillable = ["user_id",'product_id'];

    protected $with = ['product'];


    public function user(){
        return $this->belongsTo(User::class,'user_id')->where('status_id',Constants::_STATUS_ACTIVE);
    }

    public function product(){
        return $this->belongsTo(Product::class,'product_id')->where('status_id',Constants::_STATUS_ACTIVE);
    }

}
