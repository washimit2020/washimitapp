//
//  Comment_ViewController.swift
//  al-haj
//
//  Created by WASHM on 7/26/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite
class Comment_ViewController: UIViewController, UITextViewDelegate {
    @IBOutlet weak var comment: UITextView!
    @IBOutlet weak var save: UIBarButtonItem!
    @IBOutlet weak var navItem: UINavigationItem!
    var page:Page!
    var updatePage:((_ page:Page)->())!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        comment.becomeFirstResponder()
        comment.keyboardDismissMode = .onDrag
        comment.delegate = self
        comment.text = page.comment
        comment.textAlignment = .right
        save.isEnabled = false
        save.action = #selector(saveComment(_:))
        navItem.addTitleImage()
    }

    
    @objc func saveComment(_ sender: UIButton) {
        sender.isEnabled = false
        page.comment = comment.text
        
        do {
            let table = DatabaseUtils.shared.pages_t.filter(Expressions.id == page.id!)
            try DatabaseUtils.shared.db!.run(table.update(Expressions.comment <- page.comment))
            
        }catch(let exp){
            print("update page comment exp: \(exp)")
        }
        
        self.parent?.dismiss(animated: true, completion: {
            self.updatePage(self.page)
        })
    }

    @IBAction func cancel(_ sender: Any) {
        self.parent?.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        save.isEnabled = !textView.text.isClear()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
