<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;
class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        for ($i = 0; $i < 200;$i++)
        DB::table('news')->insert([
            'title' => str_random(10),
            'content' => str_random(60),
            'is_banner' => random_int(0,1),
            'is_main' => random_int(0,1),
            'user_id' => 1,
            'news_status_id' => Constants::NEWS_STATUS_APPROVED,
            "news_main_type_id"=>random_int(Constants::NEWS_MAIN_TYPE_LOCAL,Constants::NEWS_MAIN_TYPE_ALRADYH_NETWORK),
            "news_sub_type_id"=>null,
            "created_at"=>\Carbon\Carbon::now(),
            "updated_at"=>\Carbon\Carbon::now(),
        ]);
    }
}
