package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Model.Attachment;
import com.it.washim.wojooohapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/17/2017.
 */

public class ImageAdapter extends RecyclerView.Adapter {

    ArrayList<Attachment> list;
    Context context;

    public ImageAdapter(ArrayList<Attachment> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImageViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ImageViewHolder) {

            //   ((ImageViewHolder)holder);
            Attachment attachment = list.get(position);
            Glide.with(context)
                    .load(Constant.IMAGE_URL + attachment.id)
                    .into(((ImageViewHolder) holder).item_image);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_image)
        ImageView item_image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
