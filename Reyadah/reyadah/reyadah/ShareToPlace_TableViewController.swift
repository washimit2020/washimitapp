//
//  ShareToPlace_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 5/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ShareToPlace_TableViewController: ItemsLoader_TableViewController  {
    let reuseId = "cell"
    
    var delegate:LocationsDelegate?
    var products = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

            if !Gate.is_show(){
                link = RequestLinks.following.rawValue + "\(Constants.TYPE_SHOW)"
                beginWithModel = "users"
            }else{
                link = RequestLinks.create_branch.rawValue + "/\(UserDefaultsHelper.get_user_data()!.id!)"
                beginWithModel = "branches"
            }
            self.tableView?.allowsSelection = true
            self.tableView.allowsMultipleSelection = true
        
        self.navigationItem.hidesBackButton = true
        
        let done = UIBarButtonItem(image: UIImage(named:"ic_done"), style: .plain, target: self, action: #selector(done(_:)))
        let cancel = UIBarButtonItem(image: UIImage(named:"ic_cancel"), style: .plain, target: self, action: #selector(cancel(_:)))
        self.navigationItem.setLeftBarButton(done, animated: true)
        self.navigationItem.setRightBarButton(cancel, animated: true)

        
        clearData()
    }
   
    
    @IBAction func cancel(_ sender: Any) {
        self.delegate?.canceled()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func done(_ sender: Any) {
        guard let indexs = tableView.indexPathsForSelectedRows  , indexs.count > 0  else{
            let alert = UIAlertController(title: "ملاحظة", message: "يجب اختيار معرض واحد على الاقل", preferredStyle: .actionSheet)
            //            alert.view.subviews[0].backgroundColor = UIColor.white
            alert.addAction(.init(title: "تم", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        var selected = [Int]()
        for index in indexs{
            selected.append(Branch(items[index.row]).id)
        }
        
        var type = Constants.USER_SHARED
        var param = "user_id"
        var msg = "تم انشاء الطلبات و سيتم الرد باقرب وقت"
        if Gate.is_show() {
            type = Constants.BRANCH_SHARED
            param = "branch_id"
            msg = "تم مشاركة المنتاجات في الفزوع"

        }
        var params =  ["type":"\(type)"]
        var counter = 0
        for (product) in products{
            for place in selected{
                
                params["shares[\(counter)][product_id]"] = "\(product.id!)"
                params["shares[\(counter)][\(param)]"] = "\(place)"
                counter += 1
            }
        }
        apiRequest.make_request(parameters: params, apiImages: nil, link: RequestLinks.share.rawValue, method: .post, dialogMsg: "جاري انشاء الطلبات", viewController:self,completion: {
            $0?.responseJSON(completionHandler: {
                print($0)
            })
            self.apiRequest.handle_error_dialog(msg)
        })
        apiRequest.costumeAlert.closeButton.addTarget(self, action: #selector(cancel(_:)), for: .touchUpInside)
        
    }
    
   
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let branch = Branch(items[indexPath.row])
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId) as! ShareToPlace_TableViewCell
         cell.branch = branch
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
}


@objc protocol LocationsDelegate {
    @objc optional func get_selected_segmant(_ selected:Int)
    @objc  func canceled()
}

