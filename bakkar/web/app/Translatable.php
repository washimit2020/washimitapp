<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Translatable extends Model
{
    protected $fillable = ["title"];

    public  function getTitleAttribute($val){
        return trans($val);
    }

}