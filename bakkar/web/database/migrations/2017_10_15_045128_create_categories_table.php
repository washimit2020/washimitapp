<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('categories', function (Blueprint $table) {
		    $table->increments("id");
		    $table->string("name");
		    $table->timestamps();
	    });

	    Schema::create('category_post', function (Blueprint $table) {
		    $table->increments("id");
		    $table->unsignedInteger("category_id");
		    $table->unsignedInteger("post_id");
		    $table->timestamps();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('categories');
	    Schema::drop('category_post');
    }
}
