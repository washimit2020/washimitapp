<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportComment extends CommentWithUser
{
    public $incrementing = true;
    /**
     * @var array primary key
     */
    protected $primaryKey = "id";

    /**
     * @var array fillable key
     */
    protected $fillable = [
        "comment_id", "user_id", "content"
    ];



}
