//
//  ViewController.swift
//  al-haj
//
//  Created by WASHM on 7/23/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.addTitleImage()
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let slideMenuController = SlideMenuController(mainViewController: segue.destination, rightMenuViewController: "")
//        self.app.rootViewController = slideMenuController
//        self.window?.makeKeyAndVisible()
//    }
    @IBAction func make_menu(_ sender: Any) {
        navigationItem.backBarButtonItem = nil
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withEnum: .main)
        let right = Menu_TableViewController()
        main.addRightBarButtonWithImage(#imageLiteral(resourceName: "sidemenu"))
        
        let nav = UINavigationController(rootViewController: main)
        nav.navigationBar.barTintColor = .white
        main.navigationItem.addTitleImage()
        let slideMenuController = SlideMenuController(mainViewController: nav, rightMenuViewController: right)
        present(slideMenuController, animated: true)
        title = " "
    }
    @IBAction func bookIndex(_ sender: Any) {
        let controller = BasePageGetter_TableViewController()
        controller.is_sections = true
        Utils.shared.makeHeaderView(For: controller, icon:  #imageLiteral(resourceName: "index"), title: "الفهرس")

        navigationController?.pushViewController(controller, animated: true)
    }
    
}


