package com.it.washim.bookofhajj.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.it.washim.bookofhajj.R;


public class ConnectFragment extends Fragment implements View.OnClickListener {

    LinearLayout twitter, instagram, email, website;

    public ConnectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect, container, false);
        twitter = (LinearLayout) view.findViewById(R.id.twitter);
        instagram = (LinearLayout) view.findViewById(R.id.instagram);
        email = (LinearLayout) view.findViewById(R.id.email);
        website = (LinearLayout) view.findViewById(R.id.website);

        twitter.setOnClickListener(this);
        instagram.setOnClickListener(this);
        email.setOnClickListener(this);
        website.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.twitter:

                break;

            case R.id.instagram:

                break;

            case R.id.email:

                break;

            case R.id.website:

                
                break;

        }


    }
}
