<?php

namespace App\Http\Requests\Ratings;

use Illuminate\Foundation\Http\FormRequest;

class PlaceRatingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'=>'required|'.make_place_validation_string(),
            'view_rating'=>'required|numeric|min:1|max:5',
            'products_rating'=>'required|numeric|min:1|max:5',
            'users_rating'=>'required|numeric|min:1|max:5',
            'prices_rating'=>'required|numeric|min:1|max:5',
        ];
    }
}
