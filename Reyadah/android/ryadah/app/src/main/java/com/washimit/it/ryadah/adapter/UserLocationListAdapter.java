package com.washimit.it.ryadah.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.PlaceDetailsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Shares;
import com.washimit.it.ryadah.model.product.SharesBranch;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Mohammed Ali on 04/30/2017.
 */

public class UserLocationListAdapter extends RecyclerView.Adapter {

    private final int VIEW_PROG = 0;
    public final int USER = 2;
    public final int BRANCHE = 3;


    List<?> list;
    Activity context;
    Gson gson;

    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;
    Intent intent;


    public UserLocationListAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();

        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();

            Log.d("ok", "ok");
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        } else if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    if (url != null) {
                        super.onScrolled(recyclerView, dx, dy);

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {
                            // End has been reached
                            // Do something
                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {
                                loadMoreListener.onLoadMore();
                            }
                            isLoading = true;
                        }
                    }
                }
            });
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == USER) {
            return new UserHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_shearch, parent, false));

        } else if (viewType == BRANCHE) {
            return new BrancheHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user_select_show_or_branch, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.item_load_more, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserHolder) {

            if (list.get(position) instanceof Shares) {
                final Shares shares = (Shares) list.get(position);

                ((UserHolder) holder).item_place_shearch_location.setText(shares.getShareable().getSector_name() + " - " + shares.getShareable().getStreet_name());
                ((UserHolder) holder).item_place_shearch_name.setText(shares.getShareable().getName());
                ((UserHolder) holder).item_place_shearch_rating.setRating((float) shares.getShareable().getRating());
                Picasso.with(context).load(Constant._AVATAR_URL + shares.getShareable().getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((UserHolder) holder).item_place_shearch_img);


                ((UserHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openActivity(PlaceDetailsActivity.class, shares.getShareable().getType().getId(), gson.toJson(shares.getShareable()));

                    }
                });
            } else {
                final User user = (User) list.get(position);
                ((UserHolder) holder).item_place_shearch_location.setText(user.getSector_name() + " - " + user.getStreet_name());
                ((UserHolder) holder).item_place_shearch_name.setText(user.getName());
                ((UserHolder) holder).item_place_shearch_rating.setRating((float) user.getRating());
                Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into(((UserHolder) holder).item_place_shearch_img);
                ((UserHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openActivity(PlaceDetailsActivity.class, user.getType().getId(), gson.toJson(user));

                    }
                });
            }

        } else if (holder instanceof BrancheHolder) {
            final SharesBranch branche = (SharesBranch) list.get(position);

            ((BrancheHolder) holder).item_user_select_show_or_branch_location.setText(branche.getShareable().getSector_name() + " - " + branche.getShareable().getStreet_name());

            ((BrancheHolder) holder).item_user_select_show_or_branch_name.setText(branche.getShareable().getName());

            Picasso.with(context).load(Constant._AVATAR_URL + branche.getUser_id()).fit().centerCrop().into(((BrancheHolder) holder).item_user_select_show_or_branch_img);


        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) == null) {
            return VIEW_PROG;
        } else if (list.get(position) instanceof SharesBranch) {
            return BRANCHE;


        } else if (list.get(position) instanceof Shares) {
            return USER;

        } else if (list.get(position) instanceof User) {
            return USER;

        }

        return super.getItemViewType(position);
    }


    public class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_place_shearch_img)
        ImageView item_place_shearch_img;
        @BindView(R.id.item_place_shearch_follow)
        ImageView item_place_shearch_follow;
        @BindView(R.id.item_place_shearch_map_location)
        ImageView item_place_shearch_map_location;

        @BindView(R.id.item_place_shearch_name)
        TextView item_place_shearch_name;
        @BindView(R.id.item_place_shearch_location)
        TextView item_place_shearch_location;

        @BindView(R.id.item_place_shearch_rating)
        RatingBar item_place_shearch_rating;


        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item_place_shearch_map_location.setVisibility(View.GONE);
            item_place_shearch_follow.setVisibility(View.GONE);


        }
    }

    public class BrancheHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_user_select_show_or_branch_switch)
        ImageView item_user_select_show_or_branch_switch;
        @BindView(R.id.item_user_select_show_or_branch_img)
        ImageView item_user_select_show_or_branch_img;
        @BindView(R.id.item_user_select_show_or_branch_location)
        TextViewCast item_user_select_show_or_branch_location;
        @BindView(R.id.item_user_select_show_or_branch_name)
        TextView item_user_select_show_or_branch_name;
        @BindView(R.id.item_user_select_show_or_branch_rating)
        RatingBar item_user_select_show_or_branch_rating;

        public BrancheHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item_user_select_show_or_branch_switch.setVisibility(View.GONE);
            item_user_select_show_or_branch_rating.setVisibility(View.GONE);

        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }

    private void follow(int followType, int userId) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));
        parts.add(new StringPart("user_id", userId + ""));
        AapHalper.userAction("follow", parts, context, null);
    }

    private void openActivity(Class activity, int userType, String data) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        intent.putExtra("data", data);
        context.startActivity(intent);

    }

    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}
