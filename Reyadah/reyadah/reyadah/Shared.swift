//
//  Shared.swift
//  reyadah
//
//  Created by WASHM on 5/11/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//


class Shared: Model {
    var id : String!
    var status_id : Int!
    var user_id : Int!
//    var shareable_id : Int!
    var shareable_type : String!
    var product:Product!
    var shareable:Branch!
    
    
    override func map() {
        id = json["id"].stringValue
        status_id = json["status_id"].intValue
        user_id = json["user_id"].intValue
        product = Product(json["product"])
        shareable = Branch(json["shareable"])
//        shareable_id = json["shareable_id"].intValue
        shareable_type = json["shareable_type"].stringValue
    }
}
