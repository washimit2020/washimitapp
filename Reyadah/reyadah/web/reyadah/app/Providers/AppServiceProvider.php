<?php

namespace App\Providers;

use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\Http\Constants;
use App\ManyToPlacesModels\ShowBranch;
use App\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            Constants::PRODUCT_COMMENT=>Product::class,
            Constants::FESTIVAL_COMMENT=>Festival::class,
            Constants::BRANCH_SHARED=>ShowBranch::class,
            Constants::USER_SHARED=>User::class,
        ]);
        Schema::defaultStringLength(191);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
