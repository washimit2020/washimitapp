<?php

namespace App\Notifications;

use App\Channels\FCM;
use App\Channels\FCM_Message;
use App\Comment;
use App\News;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CommentLiked extends Notification
{
    use Queueable;

    protected $comment;
    protected $user;

    /**
     * Create a new notification instance.
     *
     * NewsLiked constructor.
     * @param Comment $comment
     * @param User $user
     */

    public function __construct(Comment $comment ,User $user)
    {
        $this->user = $user;
        $this->comment = $comment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FCM::class,'database'];
    }

    public function toFCM($notifiable)
    {
        $fcm = new FCM_Message();

        $fcm->data= ['data'=>[
            'id'=>random_int(0,9999999),
            'user'=>$this->user,
            'comment'=>$this->comment,

            'type'=>self::class,
        ]
        ];
        $fcm->data["data"]["msg"] = trans('notification.comment.liked',translate_notification(self::class,$fcm->data["data"]));

        return $fcm;
    }
// in your notification
    public function toDatabase($notifiable)
    {
        return [
            'user'=>$this->user,
            'comment'=>$this->comment
        ];
    }
}
