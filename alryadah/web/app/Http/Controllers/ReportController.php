<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\Comments\ReportCommentRequest;
use App\Http\Requests\Replies\ReportReplyRequest;
use App\ReportComment;
use App\ReportReply;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * @var array
     */
    private $data;

    /**
     * @param ReportCommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment(ReportCommentRequest $request)
    {
        $this->data = [
            "user_id" => $request->user()->id,
            "comment_id" => $request->comment_id,
            "content" => $request["content"],
        ];
        return $this->report(new ReportComment());
    }

    /**
     * @param ReportReplyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(ReportReplyRequest $request)
    {
        $this->data = [
            "user_id" => $request->user()->id,
            "reply_id" => $request->reply_id,
            "content" => $request["content"],
        ];

        return $this->report(new ReportReply());
    }

    /**
     * @param Model $model
     * @return \Illuminate\Http\JsonResponse
     */
    private function report(Model $model)
    {
        $model::create($this->data);

        return response()->json(ResponseHelper::successResponse(["reported" => true]));
    }
}
