package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;


import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mohammad on 7/26/2017.
 */

public class ExpandableAdapter extends BaseExpandableListAdapter {

    private Context _context;
    ArrayList<Post> grope;
    int fragmentType;

    public ExpandableAdapter(Context _context, ArrayList<Post> grope, int fragmentType) {
        this._context = _context;
        this.grope = grope;
        this.fragmentType = fragmentType;
    }

    @Override
    public int getGroupCount() {
        return grope.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        // return child.get(grope.get(groupPosition)) != null ? child.get(grope.get(groupPosition)).size() : 0;
        return grope.get(groupPosition).children != null ? (fragmentType == Constant.PAGES_SERVICES ? 1 : grope.get(groupPosition).children.size()) : 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return grope.get(groupPosition);
    }

    @Override
    public ArrayList<Post> getChild(int groupPosition, int childPosition) {
        return grope.get(groupPosition).children;

    }

    public String getChild(int groupPosition) {
        return grope.get(groupPosition).content;

    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final Object child = fragmentType == Constant.PAGES_SERVICES ? getChild(groupPosition) : getChild(groupPosition, childPosition).get(childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_child, null);
        }

        MyTextView txtListChild = (MyTextView) convertView
                .findViewById(R.id.child_name);

        ImageView childImage = (ImageView) convertView
                .findViewById(R.id.child_image);

        childImage.setVisibility(child instanceof Post? (fragmentType== Constant.PAGES_SPONSOR ?View.GONE:View.VISIBLE) :View.GONE);

        txtListChild.setText(child instanceof Post ? (fragmentType== Constant.PAGES_SPONSOR ?((Post) child).title+ "\n"+((Post) child).content:((Post) child).title)  : child.toString());


        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Post group = (Post) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_grop, null);
        }

        MyTextView lblListHeader = (MyTextView) convertView
                .findViewById(R.id.grope_name);


        ImageView img = (ImageView) convertView
                .findViewById(R.id.indicator);
        lblListHeader.setText(group.title);
        if (isExpanded) {
            img.setImageDrawable(_context.getResources().getDrawable(R.drawable.wojooh_icon_arrow_up));
        } else {
            img.setImageDrawable(_context.getResources().getDrawable(R.drawable.wojooh_icon_arrow_down));
        }
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
