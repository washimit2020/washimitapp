//
//  ForgotPasswordTVC.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator

class ForgotPasswordTVC: FormsTVC {
    
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var email: UITextField!
    var apiRequest:ApiRequest?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fields = [(email,emailError)]

        validator.registerField(email, errorLabel: emailError, rules: [RequiredRule(message: Constants.requiredErrorMsg), EmailRule(message: Constants.emailErrorMsg)])
        
        action = send(_:)
    }
    
    
    
    override func validationSuccessful() {
        let params = ["email":email.text!]
        apiRequest?.make_request(parameters: params, apiImages: nil, link: RequestLinks.forgotPassword.rawValue, method: .post, dialogMsg: "سيتم ارسال بريد الكتروني باقرب وقت", completion: nil)
        apiRequest?.handle_error_dialog("سيتم ارسال بريد الكتروني باقرب وقت")
        apiRequest?.costumeAlert.closeButton.addTarget(nil, action: #selector(pop), for: .touchUpInside)
    }
    
    
    
    @IBAction func send(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
    public func pop(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
