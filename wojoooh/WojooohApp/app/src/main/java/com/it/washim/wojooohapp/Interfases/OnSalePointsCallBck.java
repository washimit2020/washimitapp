package com.it.washim.wojooohapp.Interfases;

import com.it.washim.wojooohapp.Model.Post;

/**
 * Created by mohammad on 12/20/2017.
 */

public interface OnSalePointsCallBck {

    void onClick(Post post);
}
