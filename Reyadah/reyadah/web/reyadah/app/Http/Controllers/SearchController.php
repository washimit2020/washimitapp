<?php

namespace App\Http\Controllers;

use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\Http\Constants;
use App\User;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request){
        $this->validate($request,[
            "name"=>'max:'.Constants::MAX_STRING_LENGTH,
            "type"=>'in:'.make_validation_array_in([Constants::SEARCH_TYPE_ALL,Constants::SEARCH_TYPE_PRODUCT,
                    Constants::SEARCH_TYPE_STORE,Constants::SEARCH_TYPE_SHOW,Constants::SEARCH_TYPE_FESTIVAL]),
            'sort'=>'in:'.make_validation_array_in([Constants::SEARCH_ORDER_RATING,Constants::SEARCH_ORDER_LOWEST_PRICE])
        ]);

        list($key,$model,$type) = $this->get_model($request->type);

        $conditions  [] = ["name",'like',"%{$request->name}%"];
        $conditions  [] = ["status_id", Constants::_STATUS_ACTIVE];

        if (isset($type)){
            $conditions [] = ["type_id",$type];
        }
        $is_product = ($model == Product::class || $model == Festival::class);

        if ($is_product){
            $conditions [] = ["user_id",'<>',$request->user()->id];
        }else{
            $conditions []= ["id",'<>',$request->user()->id];
        }

        $sort = ["created_at","desc"];
        if (isset($request->sort)){

            if ($request->sort == Constants::SEARCH_ORDER_LOWEST_PRICE){
                if ($is_product){
                    $sort[0] = Constants::SEARCH_ORDER_LOWEST_PRICE;
                    $sort[1] = "asc";
                }
            }else{
                $sort[0] = $request->sort;
            }
        }

        $model = $model::where($conditions)->orderBy($sort[0],$sort[1]);

        if (!$is_product){
            check_if_blocked($model);
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS,[$key=>$model->paginate(Constants::PAGINATION)]));
    }

    /**
     * @param $search_type
     * @return array
     */
    private function get_model($search_type){
        switch ($search_type){
            case Constants::SEARCH_TYPE_PRODUCT:
                return ['products', Product::class , null];
            case Constants::SEARCH_TYPE_SHOW:
                return ['users', User::class,Constants::_TYPE_SHOW];

            case Constants::SEARCH_TYPE_STORE:
                return ['users', User::class,Constants::_TYPE_STORE];

                case Constants::SEARCH_TYPE_FESTIVAL;
                    return ['festivals', Festival::class , null];
            default :
                return ['products', Product::class , null];
        }
    }
}
