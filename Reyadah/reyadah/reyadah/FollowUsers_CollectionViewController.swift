//
//  FollowUsers_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 4/17/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

private let reuseIdentifier = "cell"

class FollowUsers_CollectionViewController: ItemsLoader_CollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        self.link = RequestLinks.explorer.rawValue
        self.beginWithModel = "products"


        // Do any additional setup after loading the view.
        
        clearData()
    }

    

    // MARK: UICollectionViewDataSource

  
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! Explore_CollectionViewCell
        
        cell.product = items[indexPath.row] as? Product
        // Configure the cell
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        details.model = items[indexPath.row] as? Product
        self.navigationController?.pushViewController(details, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width/3
        return CGSize(width: width, height: width)
    }
}

extension FollowUsers_CollectionViewController:ItemsLoaderDelegate{
    func dataFiller(data: [JSON]) {
        for productData in data{
            self.items.append(Product(productData))
        }
    }
}
