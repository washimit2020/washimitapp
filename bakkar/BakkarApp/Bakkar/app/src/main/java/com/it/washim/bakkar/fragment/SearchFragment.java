package com.it.washim.bakkar.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyEditTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
//import com.it.washim.bakkar.model.Data;
import com.it.washim.bakkar.model.Posts;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.SearchAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BaseFragment implements TextView.OnEditorActionListener, View.OnClickListener {

    @BindView(R.id.book_title)
    MyEditTextView book_title;

    @BindView(R.id.book_author)
    Spinner book_author;

    @BindView(R.id.recycler_search)
    RecyclerView recyclerView;

    @BindView(R.id.no_data)
    TextView no_data;

    @BindView(R.id.retray)
    MyButtonView retray;

    @BindView(R.id.progress)
    ProgressBar progress;


    SearchAdapter adapter;
    ArrayList<Posts> list;
    String url;
    String search;
    int pageId = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SearchAdapter(getActivity(), list, recyclerView);
        book_title.setOnEditorActionListener(this);
        retray.setOnClickListener(this);
        adapter.setOnLoadMoreLesnar(new OnLoadMorCollBack() {
            @Override
            public void onLoad() {
                Log.d("jhgjhg", "next");
                list.add(null);
                adapter.notifyItemInserted(list.size() - 1);
                next();
            }
        });

        recyclerView.setAdapter(adapter);


        book_author.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    pageId = Constant.PAGES_ARTICLES;

                }
                if (position == 1) {
                    pageId = Constant.PAGES_BOOKS;
                }
                if (position == 2) {
                    pageId = Constant.PAGES_VIDEO;
                }
                get();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                pageId = Constant.PAGES_ARTICLES;
            }
        });


        get();
        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        switch (actionId) {

            case EditorInfo.IME_ACTION_SEARCH:
                hideSoftKeyboard();
                get();
                break;


        }


        return false;
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray:

                get();
                break;


        }
    }



    private void get() {
        progress.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        retray.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);

        search = book_title.getText().toString();
        Log.d("jkhkjh", pageId + search);
        request().stringRequest(Request.Method.GET, getActivity(), "search/" + pageId + "/" + (search.equalsIgnoreCase("") ? "%20" : search), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                list.clear();
                JSONObject object = new JSONObject(response);
                JSONObject posts = object.getJSONObject("posts");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = posts.getString("next_page_url");

                progress.setVisibility(View.GONE);


                adapter.setUrl(url);
                if (list.size() == 0) {
                     no_data.setText("لا يوجد نتائج");
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                no_data.setText("لا يوجد نتائج");
                progress.setVisibility(View.GONE);
                no_data.setVisibility(View.VISIBLE);
                retray.setVisibility(View.VISIBLE);
            }
        });


    }

    private void next() {

        Log.d("jhgjhg", url);
        request().stringRequest(Request.Method.GET, getActivity(), url.replace(Constant.URL, ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                list.remove(list.size()-1);
                adapter.notifyItemRemoved(list.size()-1);
                JSONObject object = new JSONObject(response);
                JSONObject posts = object.getJSONObject("posts");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = posts.getString("next_page_url");
                adapter.setUrl(url);
                if (list.size() == 0) {

                } else {
                }

                adapter.isLoading();

            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


    }

}
