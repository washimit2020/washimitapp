//
//  Product.swift
//  reyadah
//
//  Created by WASHM on 4/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import SwiftyJSON

class Product: Model,Commentable {
    var comments: [Comment]!
    var id:Int!
    var name:String!
    var numberOfEaters:String!
    var price:String!
    var rating:Double!
    var total_likes:String!
    var is_sponsored:Bool!
    var status_id:Int!
    var ingredients:String!
    var user:Branch!
    var is_liked:Bool!
    var is_wishlisted:Bool!
    var rated:Rated!
    var shared_with:Branch?
    
    override func map() {
        id = json["id"].intValue
        name = json["name"].stringValue
        numberOfEaters = json["number_of_eaters"].stringValue
        price = json["price"].stringValue
        rating = json["rating"].doubleValue
        total_likes = json["total_likes"].stringValue
        is_sponsored = json["is_sponsored"].boolValue
        status_id = json["status_id"].intValue
        ingredients = json["ingredients"].stringValue
        user = Branch(json["user"])
        is_liked = json["is_liked"].boolValue
        is_wishlisted = json["is_wishlisted"].boolValue
        rated = Rated(json["rated"])
        
        shared_with = Branch(json["shared_with"])
        
        set_comments()
    }
    
    func set_comments() {
        let data = json["comments"].arrayValue
        var commented = [Comment]()
        if data.count > 0{
            for i in 0...data.count - 1{
                commented.append(Comment(data[i]))
            }
        }
        self.comments = commented
    }
}

protocol Commentable {
     var comments:[Comment]!{
        set get
    }
    func set_comments()
}
