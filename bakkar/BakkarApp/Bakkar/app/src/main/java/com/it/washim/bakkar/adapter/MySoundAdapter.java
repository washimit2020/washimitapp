package com.it.washim.bakkar.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.it.washim.bakkar.R;

import com.example.jean.jcplayer.JCAudio;
import com.it.washim.bakkar.halper.MyPalyer;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.model.Sound;

import java.util.List;

/**
 * Created by mohammad on 8/3/2017.
 */

public class MySoundAdapter extends RecyclerView.Adapter<MySoundAdapter.AudioAdapterViewHolder> implements View.OnClickListener {
    private Context context;
    private MyPalyer activity;
    private List<Sound> list;
    private List<JCAudio> jcAudios;
    JCAudio JCAudio;
    boolean loading = false;
    int add = 3;
    int ItemCount;
    int ItemPosition;
    OnLoadMorCollBack onLoadMorCollBack;
    String url;

    public MySoundAdapter(MyPalyer activity, List<Sound> list, List<JCAudio> jcAudios, RecyclerView recyclerView) {
        this.activity = activity;
        this.context = activity.getContext();
        this.list = list;
        this.jcAudios = jcAudios;


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (!url.equalsIgnoreCase("null")) {
                        ItemCount = layoutManager.getItemCount();
                        ItemPosition = layoutManager.findLastVisibleItemPosition();
                        if (!loading && ItemCount <= (ItemPosition + add)) {

                            if (onLoadMorCollBack != null) {

                                onLoadMorCollBack.onLoad();

                            }
                            loading = true;

                        }
                    }
                }
            });

        }

    }

    @Override
    public AudioAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new ProgressBarHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false));


        } else {

            AudioAdapterViewHolder audiosViewHolder = new AudioAdapterViewHolder(LayoutInflater.from(context).inflate(R.layout.item_sound, parent, false));
            audiosViewHolder.itemView.setOnClickListener(this);
            return audiosViewHolder;
        }

    }

    @Override
    public void onBindViewHolder(AudioAdapterViewHolder holder, int position) {

        if (holder instanceof ProgressBarHolder) {

        } else {
            if (!list.get(position).play) {
                holder.audioImage.setImageDrawable(activity.getResources().getDrawable(R.drawable.bakar_icon_play));

            } else {

                holder.audioImage.setImageDrawable(activity.getResources().getDrawable(R.drawable.bakar_icon_pause));
            }

            holder.setIsRecyclable(false);
            String title = list.get(position).title;
            holder.sound_number.setText(position + 1 + "");
            holder.audioTitle.setText(title);
            holder.itemView.setTag(jcAudios.get(position));

        }
    }

    @Override
    public int getItemCount() {
        return jcAudios == null ? 0 : jcAudios.size();
    }

    @Override
    public int getItemViewType(int position) {
       // return super.getItemViewType(position);
        return jcAudios == null ? 0 : 1;
    }

    @Override
    public void onClick(View view) {
        JCAudio = (JCAudio) view.getTag();


        if (JCAudio != null) {
            Log.d("SAasfsafas", JCAudio.getPosition() + "");

            if (activity.isPlay()) {

                if (activity.getJCAudio().equals(JCAudio)) {

                    activity.pause();
                    list.get(JCAudio.getPosition()).play = false;
                    notifyItemChanged(JCAudio.getPosition());
                    // notifyDataSetChanged();
                } else {
                    activity.playAudio(JCAudio);


                    play(JCAudio.getPosition());
                }

            } else if (activity.isPaused()) {

                if (activity.getJCAudio().equals(JCAudio)) {

                    activity.continueAudio();
                    list.get(JCAudio.getPosition()).play = true;
                    notifyItemChanged(JCAudio.getPosition());

                    //     notifyDataSetChanged();
                } else {

                    activity.playAudio(JCAudio);
                    play(JCAudio.getPosition());
                }

            } else {
                activity.playAudio(JCAudio);
                play(JCAudio.getPosition());
            }

        }

    }


    public void play(int pos) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(pos).id != list.get(i).id) {
                list.get(i).play = false;
            } else {
                list.get(i).play = true;

            }
        }

        notifyDataSetChanged();

    }


    public void pause() {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).play = false;

        }

        notifyDataSetChanged();
    }

    static class AudioAdapterViewHolder extends RecyclerView.ViewHolder {
        private TextView audioTitle, sound_number;
        private ImageView audioImage;

        public AudioAdapterViewHolder(View view) {
            super(view);
            this.sound_number = (TextView) view.findViewById(R.id.sound_number);
            this.audioTitle = (TextView) view.findViewById(R.id.audio_title);
            this.audioImage = (ImageView) view.findViewById(R.id.play);
        }
    }

    static class ProgressBarHolder extends AudioAdapterViewHolder {


        public ProgressBarHolder(View view) {
            super(view);

        }
    }


    public void addOnLoadMorLisenar(OnLoadMorCollBack onLoadMorCollBack) {
        this.onLoadMorCollBack = onLoadMorCollBack;
    }

    public void isLoading(Boolean loading) {
        this.loading = loading;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
