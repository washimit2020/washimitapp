//
//  ItemsLoader_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 5/9/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SlackTextViewController
import Alamofire
import SwiftyJSON

class ItemsLoader_TableViewController: UITableViewController {
    
    var dataModel:DataModel?
    
    var link = ""
    
    var beginWithModel = ""
    
    var items = [JSON]()
    
   lazy var apiRequest:ApiRequest = ApiRequest()
    
    let activiy = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var updated = false
    
    var emptyLabel:UILabel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

        self.tableView?.allowsSelection = false
        
        
        tableView.estimatedRowHeight = 150
        
        let width =  tableView!.frame.width
        let height:CGFloat =  40
        let view = UILabel(frame: CGRect(x: 0 , y: tableView!.frame.height/2 - height, width: width, height: height))
        
        view.text = "لا يوجد نتائج"
        view.textAlignment = .center
        view.textColor = UIColor.gray
        view.isHidden = true
        emptyLabel = view
        
        tableView?.addSubview(emptyLabel!)
        
        
        self.tableView?.refreshControl = UIRefreshControl()
        self.tableView?.refreshControl?.addTarget(self, action: #selector(clearData(_:)), for: .valueChanged)
        self.tableView?.separatorStyle = .none
        
        activiy.startAnimating()
        activiy.hidesWhenStopped = true
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
 
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view  =  UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 30)))
        activiy.frame.origin = view.center
        
        view.addSubview(activiy)
        return view
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        guard let data = dataModel else {
            return
        }
        if indexPath.row != items.count - 2
            ||
            data.next_page_url == nil{
            return
        }
        
        make_api_call(dataModel!.next_page_url!)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}


extension ItemsLoader_TableViewController{
    
    func clearData(_ sender:Any? = nil)
    {
        make_api_call(nil)
    }
    
    func make_api_call(_ nextlink:String?){
        activiy.startAnimating()
        
        var current = link
        
        if let link = nextlink{
            current = link
        }
        
        apiRequest.make_request( link: current, method: .get, completion: data_handler)
        
    }
    
    private func hideLoaders(){
        self.tableView?.refreshControl?.endRefreshing()
        self.emptyLabel?.isHidden = true
        self.activiy.stopAnimating()
    }
    
    func data_handler(_ response:DataRequest?)->Void{
        guard let response = response else {
            self.present(self.apiRequest.costumeAlert, animated: true, completion: {
                
                self.apiRequest.handle_error_dialog(nil)
            })
            
            hideLoaders()
            
            return
        }
        
        response.responseJSON(completionHandler: {
            response in
            
            if response.response?.statusCode == 200{
                guard let json = response.data else{
                    self.hideLoaders()
                    return
                }
                
                self.dataModel = DataModel(JSON(json), beginWith: self.beginWithModel )
                
                guard let data = self.dataModel!.data else{
                    self.hideLoaders()

                    return
                }
                
                if self.dataModel!.to == 0 {
                    self.hideLoaders()
                    self.emptyLabel?.isHidden = false
                    return
                }
                
                if self.dataModel?.current_page == 1{
                    self.items.removeAll()
                }
                
                self.items.append(contentsOf: data)
                
                self.tableView?.reloadData()
                self.hideLoaders()
                
            }
            
        })
    }
    
    
}



   


//
//  Comment_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//





