<?php
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 1/4/2017
 * Time: 1:37 PM
 */

namespace App\Http\Helpers;


use App\Http\Controllers\Auth;
use App\User;

class  ResponseHelper
{
    /**
     * @param $msg
     * @return array
     */
    public static function successResponse($msg)
    {
        return [
            "success" => true,
            "code" => 1,
            "message" => $msg
        ];
    }

    /**
     * @param $msg
     * @return array
     */
    public static function failedResponse($msg)
    {
        return [
            "success" => false,
            "code" => 2,
            "message" => $msg
        ];
    }

    public static function userAuthnticationResponse(User $user)
    {
        $token = str_random(100);
        $user->api_token = $token;
        $user->update();
        $msg = [
            "user" => get_user_obj($user),
            "token" => $token
        ];
        return response()->json(ResponseHelper::successResponse($msg));
    }
}