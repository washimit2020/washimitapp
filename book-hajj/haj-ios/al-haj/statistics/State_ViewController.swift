//
//  State_ViewController.swift
//  al-haj
//
//  Created by WASHM on 1/9/18.
//  Copyright © 2018 WASHM. All rights reserved.
//

import UIKit
import Charts

class State_ViewController: UIViewController {

    @IBOutlet weak var readingTime: TitleLabel!
    @IBOutlet weak var totalPages: TitleLabel!
    @IBOutlet weak var readSections: TitleLabel!
    @IBOutlet weak var unreadSections: TitleLabel!
    @IBOutlet weak var chart: PieChartView!
    @IBOutlet weak var height: NSLayoutConstraint!
    var items:[Page] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        height.constant = view.frame.height / 2
        pieChartUpdate()
        addRightBarButtonWithImage()
    }

    
    func setupChart(){
        chart.chartDescription?.font = UIFont(name: "TheSansArab-Plain", size: 15)!
        chart.chartDescription?.textColor = .tintColor
        chart.chartDescription?.text = ""
        chart.holeColor = .clear
        chart.drawEntryLabelsEnabled = false
        chart.legend.font = UIFont(name: "TheSansArab-Plain", size: 11)!
        chart.legend.textColor = .tintColor
        chart.legend.form = .circle
        chart.legend.direction = .rightToLeft
    }
    
    func pieChartUpdate () {
        //future home of pie chart code
        var unread:Double = 0
        var read:Double = 0
        var sections:Int = 0
        var unreadSections = 0
        items.forEach { (page) in
            if(page.is_read == 1){
                read += 1
                if(page.section_id != nil){
                    sections += 1
                }
            }else{
                unread += 1
                if(page.section_id != nil){
                    unreadSections += 1
                }
            }
        }
        let entry1 = PieChartDataEntry(value: unread, label: "الصفحات الغير مقروءة")
        let entry2 = PieChartDataEntry(value: read, label: "الصفحات المقروءة")
        let dataSet = PieChartDataSet(values: [entry1, entry2], label: "")
        
        let data = PieChartData(dataSet: dataSet)
        dataSet.sliceSpace = 8
        
        dataSet.colors = [#colorLiteral(red: 0.7884268165, green: 0.7570940852, blue: 0.7167926431, alpha: 1) , .tintColor]
        chart.data = data
        setupChart()
        //All other additions to this function will go here
        
        //This must stay at end of function
        chart.notifyDataSetChanged()
        
        totalPages.text = "\(items.count)"
        readSections.text = "\(sections)"
        self.unreadSections.text = "\(unreadSections)"
        let time = Utils.shared.get_time()
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        self.readingTime.text = String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
}
