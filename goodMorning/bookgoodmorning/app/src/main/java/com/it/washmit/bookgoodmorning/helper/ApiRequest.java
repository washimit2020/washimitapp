package com.it.washmit.bookgoodmorning.helper;

import android.content.Context;
import android.support.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.it.washmit.bookgoodmorning.interfac.OnRequestCollBack;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mohammad on 8/7/2017.
 */

public class ApiRequest {
    private static RequestQueue queue;
    private static ApiRequest request = new ApiRequest();

    public static ApiRequest getInstance(Context context) {
        if (queue == null) queue = Volley.newRequestQueue(context);
        return request;
    }

    public ApiRequest() {

    }


    public void stringRequest(final OnRequestCollBack collBack) {
        StringRequest request = new StringRequest(Request.Method.GET, "https://api.tumblr.com/v2/blog/nedaaadelworld.tumblr.com/posts?api_key=tV6I3joxoD6LOWJUsv2bznuakBuFxyOc2zuKZV5UGSs4gbsWxO", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    collBack.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null)
                    collBack.onErrorResponse(error);

            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);

    }


}
