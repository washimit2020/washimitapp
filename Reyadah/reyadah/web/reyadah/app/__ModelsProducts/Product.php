<?php

namespace App\__ModelsProducts;

use App\__ModelsRating\ProductRating;
use App\Attachment;
use App\Comment;
use App\Http\Constants;
use App\User;
use function foo\func;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        "name", "number_of_eaters", "ingredients", "price"
    ];

    protected $with = ['user'];

    protected $appends = ['comments','is_liked','is_wishlisted','rated'];

    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * get all attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'related_id');
    }

    /**
     * to get the ratings of the place
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(ProductRating::class, 'product_id');
    }

    /**
     * get likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany(Like::class, 'product_id')->where('status_id',Constants::_STATUS_ACTIVE);
    }


    /**
     * get shares
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function shares()
    {
        return $this->hasMany(ShareProduct::class, 'product_id')->where('status_id',Constants::_STATUS_ACTIVE);
    }



    /**
     * get wishlisted
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wishlisted()
    {
        return $this->hasMany(Wishlist::class, 'product_id')->where('status_id',Constants::_STATUS_ACTIVE);
    }

    /**
     * get comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->latest()->where('status_id',Constants::_STATUS_ACTIVE)->whereHas('user',function($query){
            check_if_blocked($query);
        });
    }



    /**
     * to get the comments object
     *
     * @return mixed
     */
    public function getCommentsAttribute(){
        return $this->comments()->latest()->get()->take(2);
    }

    /**
     * @return int
     */
    public function getIsWishlistedAttribute()
    {
        $wishlisted = 0;
        if (auth()->guard('api')->check()) {
            $wishlist = $this->wishlisted()->where('user_id', auth()->guard('api')->user()->id)->where('status_id', Constants::_STATUS_ACTIVE)->first();
            if (!empty($wishlist)) {
                $wishlisted = 1;
            }
        }
        return $wishlisted;
    }

    /**
     * @return int
     */
    public function getIsLikedAttribute()
    {
        $liked = 0;
        if (auth()->guard('api')->check()) {
            $like = $this->likes()->where('user_id', auth()->guard('api')->user()->id)->where('status_id', Constants::_STATUS_ACTIVE)->first();
            if (!empty($like)) {
                $liked = 1;
            }
        }
        return $liked;
    }

    /**
     * @return null
     */
    public function getRatedAttribute()
    {
        $rated = null;
        if (auth()->guard('api')->check()) {
            $rated = $this->ratings()->where('user_id', auth()->guard('api')->user()->id)->first();
        }
        return $rated;
    }


}
