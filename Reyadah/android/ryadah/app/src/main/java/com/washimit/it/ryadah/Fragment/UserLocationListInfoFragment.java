package com.washimit.it.ryadah.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.UserLocationListAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Shares;
import com.washimit.it.ryadah.model.product.SharesBranch;
import com.washimit.it.ryadah.model.user.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserLocationListInfoFragment extends Fragment {

    @BindView(R.id.fragment_user_location_list_info)
    RecyclerView recyclerView;
    UserLocationListAdapter adapter;
    ArrayList<Object> objects;
    ApiRequest request;
    Gson gson;
    String nextUrl;
    User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_user_location_list_info, container, false);
        ButterKnife.bind(this, view);
        objects = new ArrayList<>();
        request = new ApiRequest(getActivity());
        gson = new Gson();

        user = gson.fromJson(getArguments().getString("user"), User.class);



        Log.d("asdasdasf",user.getName());
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        adapter = new UserLocationListAdapter(objects, getActivity(), recyclerView);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

            }
        });


        recyclerView.setAdapter(adapter);

        UpdateUi(getArguments().getInt("user_type_id"));

        return view;
    }


    public void UpdateUi(int userId) {
        switch (userId) {
            case Constant._TYPE_STORE:
                getSharesUser(getArguments().getInt("id"));
                break;

            case Constant._TYPE_SHOW:
                getSharesBranch(getArguments().getInt("id"));
                break;


        }


    }


    public void getSharesUser(int id) {

        request.MultipartRequest(Request.Method.GET, false, "product/shares/" + id + "/" + Constant._TYPE_SHARE_USERS, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("asdfasf", result);

                objects.clear();
                objects.add(user);
                JSONObject object = null;
                JSONObject msg = null;
                JSONObject shares = null;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");

                objects.addAll((Collection<Shares>) gson.fromJson(msg.getString("shares"), new TypeToken<ArrayList<Shares>>() {
                }.getType()));

                adapter.notifyDataSetChanged();
                adapter.setLoading();

                Log.d("mmmmmmm1", objects.size() + "");

            }

            @Override
            public void onRequestError(String errorMessage) {

                Log.d("asdfasf", errorMessage);


            }
        });
    }

    public void getSharesBranch(int id) {

        request.MultipartRequest(Request.Method.GET, false, "product/shares/" + id + "/" + Constant._TYPE_SHARE_BRANCH, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("asdfasf", result);
                objects.clear();
                objects.add(user);
                JSONObject object = null;
                JSONObject msg = null;
                JSONObject shares = null;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");

                //   shares = msg.getJSONObject("shares");
//
//                nextUrl = shares.getString("next_page_url");
//                adapter.setUrl(nextUrl);
                objects.addAll((Collection<SharesBranch>) gson.fromJson(msg.getString("shares"), new TypeToken<ArrayList<SharesBranch>>() {
                }.getType()));

                adapter.notifyDataSetChanged();
                adapter.setLoading();


                Log.d("mmmmmmm2", objects.size() + "");


            }

            @Override
            public void onRequestError(String errorMessage) {

                Log.d("asdfasf", errorMessage);


            }
        });
    }
}
