<?php

namespace App\Http\Controllers\Products;

use App\__ModelsProducts\Product;
use App\__ModelsProducts\ShareProduct;
use App\Attachment;
use App\Http\Constants;
use App\Http\Requests\Products\ProductStoreRequest;
use App\Http\Requests\Products\ProductUpdateRequest;
use App\User;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\UploadedFile;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ProductsController extends Controller
{

    public function new_index()
    {
        //        $perpage = 15;

//        $skip = $current * $perpage;
        $products = Product::whereHas('user', function ($query) {
            $this->validate_user($query);
        })->where([
            ['status_id', Constants::_STATUS_ACTIVE],
            ['is_sponsored', false],
        ])
            ->latest()
            ->paginate(Constants::PAGINATION);

        $sponsored = Product::where([
            ['status_id', Constants::_STATUS_ACTIVE],
            ['is_sponsored', true],
        ])->whereHas('user', function ($query) {
            $query->where('status_id', Constants::_STATUS_ACTIVE);
            check_if_blocked($query);
        })->latest()
            ->skip($products->currentPage() - 1)
            ->first();

        if (!empty($sponsored)) {
            $products->push($sponsored);
        }

        /*
  Product::query()->with(['shares','sharedBy'=>function($query){
        $query ->whereHas('user',function ($query){
             $query->whereHas('followedBy', function ($query) {
                 //user has followed by the current user :DDDDDDDDDDD
                 $query->where([
                     ['type_id', Constants::_TYPE_FOLLOW_USER],
                     ['status_id', Constants::_STATUS_ACTIVE]
                 ])->whereHas('place', function ($query) {
                     check_if_blocked($query);
                     $query->where([
                         ['user_id','<>' ,auth()->user()->id],
                         ['status_id', Constants::_STATUS_ACTIVE]
                     ]);
                 });
             });
         });//to stop viewing the data if the user currently following the shareable
     }])->whereHas('shares',function ($query){
         $query->where([
             ['shareable_id', '<>', auth()->user()->id],
             ['shareable_type', Constants::USER_SHARED],
         ])->whereHas('sharedWith',function ($query){
             $this->validate_user($query);
         });
     })->whereHas('user',function ($query){
         check_if_blocked($query);
     });
  */
        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['products' => $products]));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current = abs(Input::get('page', 1) - 1);

        /*
         * , 'product.shares' => function ($query) {
            $query->whereHas('user', function ($query) {
                $this->validate_user($query);
            })->take(Constants::PAGINATION)->get();
        }
       ->with( ['shares' => function ($query) {
            $query->take(Constants::PAGINATION)->get();
        }])->whereHas('user', function ($query) {
            $this->validate_user($query);
        }])
         */

        $shared = ShareProduct::whereHas('user', function ($query) {
            check_if_blocked($query);
        })->whereHas('sharedWith', function ($query) {
            $this->validate_user($query);
        })->whereHas('product', function ($query) {
            $query->where('status_id', Constants::_STATUS_ACTIVE);
        })->with(['sharedWith'])->where([
            ['status_id', Constants::_STATUS_ACTIVE],
            ['shareable_type', Constants::USER_SHARED],
        ])->paginate(5);

        $items = [];
        foreach ($shared as $item) {
            $product = new Product();
            $product->forceFill($item->product->toArray());
            $product->shared_with = $item->sharedWith;
            $items[] = $product;
        }

        $sponsored = Product::where([
            ['status_id', Constants::_STATUS_ACTIVE],
            ['is_sponsored', true],
        ])->whereHas('user', function ($query) {
            $query->where('status_id', Constants::_STATUS_ACTIVE);
            check_if_blocked($query);
        })->latest()
            ->skip($current)
            ->first();

        if (isset($sponsored)) {
            $items [] = $sponsored;
        }
        $products = Product::where([
            ['status_id', Constants::_STATUS_ACTIVE],
            ['is_sponsored', false]])
            ->whereHas('user', function ($query) {
                $this->validate_user($query);
            })->latest()->paginate(Constants::PAGINATION);


        foreach ($items as $item):
            $products->push($item);
        endforeach;
//        $data = array_merge($items,$products->toArray());
//        if (!empty($sponsored)){
//            $data = array_merge([$sponsored],$data);
//        }
//
//       $result = new Paginator($data,$perpage,null,["path"=>\request()->url()]);


        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['products' => $products]));
    }

    /**
     * @param $query
     */
    private function validate_user($query)
    {
        $query
            ->where('user_id', '<>', auth()->user()->id)
            ->whereHas('followedBy', function ($query) {
                //user has followed by the current user :DDDDDDDDDDD
                $query->where([
                    ['type_id', Constants::_TYPE_FOLLOW_USER],
                    ['status_id', Constants::_STATUS_ACTIVE]
                ])->whereHas('place', function ($query) {
                    check_if_blocked($query);
                    $query->where([
                        ['user_id', auth()->user()->id],
                        ['status_id', Constants::_STATUS_ACTIVE]
                    ]);
                });
            });
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function explorer()
    {
        $products = Product::whereHas('user', function ($query) {
            $query->where('user_id', '<>', auth()->user()->id)
                ->where('status_id', Constants::_STATUS_ACTIVE);
            check_if_blocked($query);
        })
            ->where([
                ['status_id', Constants::_STATUS_ACTIVE],
                ['is_sponsored', false],
            ])
            ->latest()
            ->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['products' => $products]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProductStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $product = $this->create_product($request);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['product' => $product]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductUpdateRequest $request
     * @param  \App\__ModelsProducts\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $product->update($request->only("name", "number_of_eaters", "ingredients", "price"));

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['product' => $product]));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\__ModelsProducts\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->status_id = Constants::_STATUS_DELETED;
        $product->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['deleted' => true]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\__ModelsProducts\Product $product
     * @return \Illuminate\Http\Response
     */
    public function hide(Product $product)
    {
        $hidden = true;

        if ($product->status_id == Constants::_STATUS_HIDDEN) {
            $product->status_id = Constants::_STATUS_ACTIVE;
            $hidden = false;
        } else {
            $product->status_id = Constants::_STATUS_HIDDEN;
        }

        $product->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['hidden' => $hidden]));
    }

    /**
     * create new product
     *
     * @param $request
     * @return mixed
     */
    private function create_product(ProductStoreRequest $request)
    {
        $user = auth()->user();

        $product = $user->products()->create($request->only("name", "number_of_eaters", "ingredients", "price"));

        $data ['type_id'] = Constants::_TYPE_PRODUCT_ATTACHMENT;
        $data ['related_id'] = $product->id;

        $dir = "users/{$user->id}/product/{$product->id}";

        if ($request->hasFile('images') && is_array($request->file('images'))) {
            foreach ($request->file('images') as $file) {
                $data['path'] = $file->storeAs($dir, time() . "_" . str_random());
                $data['mime_type'] = $file->getMimeType();
                $data['extension'] = $file->extension();
                Attachment::create($data);
            }
        }

        return $product;
    }
}
