package com.it.washim.bakkar.model;

import java.util.ArrayList;

/**
 * Created by mohammad on 10/16/2017.
 */

public class Categories {
    public int current_page;
    public ArrayList<CategoriesData> data;
    public int from;
    public int last_page;
    public String next_page_url;
    public String path;
    public int per_page;
    public String prev_page_url;
    public int to;
    public int total;
}
