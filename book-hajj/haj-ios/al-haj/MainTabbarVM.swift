//
//  MainTabbarVM.swift
//  al-haj
//
//  Created by WASHM on 12/31/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

class MainTabBarVM : NSObject{
    var viewControllers : [UIViewController] = {
        var controllers = [UIViewController]()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        controllers.append(storyboard.instantiateViewController(withEnum: .settings))
        controllers.append(storyboard.instantiateViewController(withEnum: .book))
        controllers.append(storyboard.instantiateViewController(withEnum: .search))
        return controllers
    }()
    var delegate:MainTabBarVMDelegate?
    var selectedViewController : UIViewController!{
        didSet{
            delegate?.didChangeController()
        }
    }
    var tabBar:UITabBar!
    init(_ tabbar: UITabBar) {
        self.tabBar = tabbar
        super.init()
        tabBar.tintColor = .white
        tabBar.selectedItem = tabBar.items?[2]
        tabBar.unselectedItemTintColor = .white
        tabBar.delegate = self
//        let width  = tabBar.frame.width / CGFloat(tabBar.items!.count - 1)
//        let size:CGSize = .init(width: width, height: tabBar.frame.height)
        let image = #imageLiteral(resourceName: "selected")
        
        tabBar.selectionIndicatorImage = image
        selectedViewController = viewControllers[1]
    }
    
}
protocol MainTabBarVMDelegate {
    func didChangeController()
    func didSelectShare()
}
extension MainTabBarVM:UITabBarDelegate{
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        var index = item.tag - 1
        if item.tag == 2 {
            tabBar.selectedItem = tabBar.items?[2]
            delegate?.didSelectShare()
            index = 1
        }
        if item.tag > 2 {
            index -= 1
        }
        selectedViewController = viewControllers[index]
//        if item.tag  == 3{
//            let book = (viewControllers[index] as! Book_PageViewController)
//        }
    }
}
