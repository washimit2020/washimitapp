package com.it.washim.bakkar.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnDialogCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Articles;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArticleDetailsActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.wish_listed)
    ImageView wish_listed;
    @BindView(R.id.shar)
    ImageView shar;
    @BindView(R.id.sating)
    ImageView sating;

    @BindView(R.id.article_image)
    ImageView article_image;

    @BindView(R.id.article_text)
    MyTextView article_text;

    @BindView(R.id.article_title)
    MyTextView article_title;

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.article_details)
    LinearLayout article_details;

    private int numberOfCads;


    private int articleId;
    private boolean isWishlisted;
    private Bundle bundle;
    private HashMap<String, String> map;
    private Intent intent;
    private boolean screnMode = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_details);
        ButterKnife.bind(this);
        bundle = getIntent().getExtras();
        map = new HashMap<>();
        intent = new Intent();
        if (bundle != null) {
            isWishlisted = (boolean) bundle.get("is_wishlisted");
            articleId = bundle.getInt("id");
            article_title.setText(bundle.getString("title"));
            wish_listed.setImageDrawable(getResources().getDrawable((boolean) bundle.get("is_wishlisted") ? R.drawable.bakar_icon_favorite : R.drawable.bakar_icon_favorite_of));
            if (bundle.getString("description") != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {

                    article_text.setText(Html.fromHtml(bundle.getString("description"), Html.FROM_HTML_MODE_COMPACT));
                } else {
                    article_text.setText(Html.fromHtml(bundle.getString("description")));
                }
            }
            if (bundle.get("img") != null) {
                Picasso.with(this).load(Constant.URL + "attachments/" + bundle.getInt("img")).placeholder(getResources().getDrawable(R.drawable.bakkar_icon)).into(article_image);
            }
        }

        back.setOnClickListener(this);
        wish_listed.setOnClickListener(this);
        shar.setOnClickListener(this);
        sating.setOnClickListener(this);
        update();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.wish_listed:
                if (shardPref().getUser() != null) {

                    wish_listed.setImageDrawable(getResources().getDrawable(isWishlisted ? R.drawable.bakar_icon_favorite_of : R.drawable.bakar_icon_favorite));
                    isWishlisted = !isWishlisted;

                    intent.putExtra("is_wishlisted", isWishlisted);
                    setResult(1, intent);

                    map.put("post_id", articleId + "");
                    request().stringRequest(Request.Method.POST, getApplicationContext(), "wishlist", map, new OnRequestCollBack() {
                        @Override
                        public void onResponse(String response) throws JSONException {
                            Log.d("wishlist", response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("wishlist", error.getMessage() + "rrrr");

                        }
                    });
                } else {
                    showAlertDialog("حتى تتمكن من اضافة الكتاب الى مفضلتك قم بتسجيل الدخول :)", new OnDialogCollBack() {
                        @Override
                        public void login() {
                            startActivity(new Intent(ArticleDetailsActivity.this, HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }

                        @Override
                        public void hide() {

                        }
                    });
                }
                break;
            case R.id.shar:

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مشاركة");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "www.google.com");
                startActivity(Intent.createChooser(sharingIntent, "مشاركة"));
                break;
            case R.id.sating:
                settingsDialog();
                break;
        }
    }


    private void settingsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_sating);

        Button day_mode = (MyButtonView) dialog.findViewById(R.id.day_mode);
        final MyTextView proogresbar = (MyTextView) dialog.findViewById(R.id.proogresbar);
        Button night_mode = (MyButtonView) dialog.findViewById(R.id.night_mode);
        final SeekBar text_size = (SeekBar) dialog.findViewById(R.id.text_size);
        final Spinner text_font_type = (Spinner) dialog.findViewById(R.id.text_font_type);
        final MyButtonView hide = (MyButtonView) dialog.findViewById(R.id.hide);


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("TheSansArab-Plain_0.ttf");
        arrayList.add("TheSansArab-Light_0.ttf");
        arrayList.add("TheSansArab-Black_0.ttf");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(ArticleDetailsActivity.this, android.R.layout.simple_spinner_dropdown_item, arrayList);

        text_font_type.setAdapter(arrayAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        text_font_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        text_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                proogresbar.setText(progress + "%");
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }
        });


        day_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = true;

                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);

                update();

            }
        });
        night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = false;
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();

            }
        });


        screnMode = shardPref().getScreenMode();
        text_size.setProgress(shardPref().getTextSize());

        dialog.show();


    }


    private void update() {
        article_text.setTextSize(shardPref().getTextSize() > 14 ? shardPref().getTextSize() : 14);

        article_text.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + shardPref().getFontType()));
        article_title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + shardPref().getFontType()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            article_text.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white, null));
            article_title.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white, null));
            article_details.setBackground(getResources().getDrawable(shardPref().getScreenMode() ? R.color.white : R.color.black, null));

        } else {
            article_text.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white));
            article_title.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white));
            article_details.setBackground(getResources().getDrawable(shardPref().getScreenMode() ? R.color.white : R.color.black));

        }


    }

}
