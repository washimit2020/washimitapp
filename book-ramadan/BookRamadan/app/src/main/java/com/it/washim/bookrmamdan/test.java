package com.it.washim.bookrmamdan;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.it.washim.bookrmamdan.coustumView.EditTextCursorWatcher;
import com.it.washim.bookrmamdan.helper.DataBHelper;
import com.it.washim.bookrmamdan.interfac.onselectChangecallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by washm on 4/1/17.
 */

public class test extends Activity implements View.OnClickListener {


    private EditTextCursorWatcher textselect;
    private FloatingActionButton actionButtonble, actionButtonred, actionButtonyallo, actionButtonsher;
    LinearLayout layout;
    DataBHelper mDBHelper;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.main);



        mDBHelper = new DataBHelper(this);

        //Check exists database
        File database = getApplicationContext().getDatabasePath(DataBHelper.DBNAME);
        if(false == database.exists()) {
            mDBHelper.getReadableDatabase();
            //Copy db
            if(copyDatabase(this)) {
                Toast.makeText(this, "Copy database succes", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Copy data error", Toast.LENGTH_SHORT).show();
                return;
            }
        }

            Log.d("qafsafsfasf", mDBHelper.numberOfRows()+"");
            Log.d("qafsafsfasf", mDBHelper.getListhighlightList(mDBHelper.getListpagestList().get(0).getId()).get(0).getColor_hex()+"");
            mDBHelper.insertHighlight(0,4,1,1);
            Log.d("qafsafsfasf", mDBHelper.numberOfRows()+"");



        // make sure the TextView's BufferType is Spannable, see the main.xml
        textselect = (EditTextCursorWatcher) findViewById(R.id.textselect);
        actionButtonble = (FloatingActionButton) findViewById(R.id.floatingActionButton2);
        actionButtonred = (FloatingActionButton) findViewById(R.id.floatingActionButton3);
        actionButtonyallo = (FloatingActionButton) findViewById(R.id.floatingActionButton4);
        actionButtonsher = (FloatingActionButton) findViewById(R.id.floatingActionButton5);
        layout = (LinearLayout) findViewById(R.id.selcter);

        actionButtonred.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.red)));
        actionButtonyallo.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.yaloo)));
        actionButtonble.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.white)));

        layout.setVisibility(View.GONE);

        textselect.setTextIsSelectable(true);
        textselect.setCursorVisible(false);
        textselect.setShowSoftInputOnFocus(false);

        actionButtonble.setOnClickListener(this);
        actionButtonred.setOnClickListener(this);
        actionButtonyallo.setOnClickListener(this);
        actionButtonsher.setOnClickListener(this);

        textselect.selectChange(new onselectChangecallback() {
            @Override
            public void selectChange(int selStart, int selEnd) {

                if (selStart != selEnd) {

                    layout.setVisibility(View.VISIBLE);
                } else {
                    layout.setVisibility(View.GONE);
                }


                // textselect.getText().setSpan(new UnderlineSpan(), selStart, selEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                // textselect.getText().setSpan(new ForegroundColorSpan(Color.BLUE), selStart, selEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


            }
        });



    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.floatingActionButton2: {
               // textselect.getText().setSpan(new StyleSpan(Typeface.BOLD), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                 textselect.getText().setSpan(new ForegroundColorSpan(Color.BLUE), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                 textselect.getText().setSpan(new BackgroundColorSpan(Color.WHITE), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                layout.setVisibility(View.GONE);
                textselect.clearFocus();

                break;
            }
            case R.id.floatingActionButton3: {
                textselect.getText().setSpan(new BackgroundColorSpan(Color.RED), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }
            case R.id.floatingActionButton4: {
                textselect.getText().setSpan(new BackgroundColorSpan(Color.YELLOW), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                // textselect.getText().setSpan(new StyleSpan(Typeface.ITALIC), textselect.getSelectionStart(), textselect.getSelectionEnd(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }
            case R.id.floatingActionButton5: {
                String x = "";
                for (int i = textselect.getSelectionStart(); i < textselect.getSelectionEnd(); i++) {

                    x = x + textselect.getText().toString().charAt(i);

                }

                Toast.makeText(getApplicationContext(), x, Toast.LENGTH_LONG).show();
                layout.setVisibility(View.GONE);
                textselect.clearFocus();
                break;
            }


        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private boolean copyDatabase(Context context) {
        try {

            InputStream inputStream = context.getAssets().open(DataBHelper.DBNAME);
            String outFileName = DataBHelper.DBLOCATION + DataBHelper.DBNAME;
            OutputStream outputStream = new FileOutputStream(outFileName);
            byte[]buff = new byte[1024];
            int length = 0;
            while ((length = inputStream.read(buff)) > 0) {
                outputStream.write(buff, 0, length);
            }
            outputStream.flush();
            outputStream.close();
            Log.w("MainActivity","DB copied");
            return true;
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
