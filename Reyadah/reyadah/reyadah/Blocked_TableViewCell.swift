//
//  Blocked_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/9/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Blocked_TableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: BorderedImageView!
   
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var locationText: UILabel!
    
    @IBOutlet weak var unblock: UIButton!
    
    override func awakeFromNib() {
        unblock.layer.cornerRadius = 5
        unblock.layer.borderWidth = 0.5
        unblock.layer.borderColor = UIColor.lightGray.cgColor
//        unblock.backgroundColor = UIColor.lightGray
        unblock.clipsToBounds = true
    }
    var user:User?{
        didSet{
            updateView()
        }
    }
    
    func updateView()  {
        guard let user = user else {
            return
        }
        avatar.set_avatar_image(user_id: user.id)
        username.text = user.name
        locationText.text = make_location_text(user)
    }
    
}
