//
//  BorderedImageView.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class BorderedImageView: UIImageView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        layer.borderColor = Constants.themeColor.cgColor
        layer.borderWidth = 1
        layer.cornerRadius = 5
        self.contentMode = .scaleAspectFill
        self.clipsToBounds = true
    }


}
