<?php

namespace App\Http\Middleware;

use App\Http\Constants;
use Closure;

class ShowsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (auth()->user()->type_id != Constants::_TYPE_SHOW){
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }
        return $next($request);
    }
}
