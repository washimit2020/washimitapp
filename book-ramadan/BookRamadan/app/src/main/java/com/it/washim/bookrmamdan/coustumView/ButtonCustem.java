package com.it.washim.bookrmamdan.coustumView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by washm on 4/9/17.
 */

public class ButtonCustem extends Button {
    public ButtonCustem(Context context) {
        super(context);
        init();
    }
    public ButtonCustem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public ButtonCustem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    public ButtonCustem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }
    public  void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/TheSansArab-Light_0.ttf");
        setTypeface(tf, 1);
    }
}
