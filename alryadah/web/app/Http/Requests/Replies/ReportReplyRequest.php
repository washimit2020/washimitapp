<?php

namespace App\Http\Requests\Replies;

use Illuminate\Foundation\Http\FormRequest;

class ReportReplyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "reply_id" => "required|exists:replies,id",
            "content" => "required|max:6000",
        ];
    }
}
