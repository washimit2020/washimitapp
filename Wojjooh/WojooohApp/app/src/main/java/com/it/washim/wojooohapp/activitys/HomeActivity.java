package com.it.washim.wojooohapp.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Model.HomeItem;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.HomeAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.home_recycler)
    RecyclerView recyclerView;

    HomeAdapter adapter;
    GridLayoutManager manager;
    ArrayList<HomeItem> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        list = new ArrayList<>();
        manager = new GridLayoutManager(this, 2);
        adapter = new HomeAdapter(this, list);


        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (list.get(position).type == 2) {
                    return 2;
                }
                return 1;

            }
        });
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(manager);

        recyclerView.setAdapter(adapter);


        addItem();

    }

    private void addItem() {
        list.add(new HomeItem("منافذ" + "\n" + "البيع", 1, Constant.PAGES_SELLING_PLACES));
        list.add(new HomeItem("قائمة" + "\n" + "الكتب", 1, Constant.PAGES_BOOK));
        list.add(new HomeItem("وكلاء" + "\n" + "التوزيع", 1, Constant.PAGES_SPONSOR));
        list.add(new HomeItem("معارض" + "\n" + "الكتب", 1, Constant.PAGES_BOOKS_SHOWROOM));
        list.add(new HomeItem("طلب " + "\n" + "نشر", 1, Constant.PAGES_PUBLISH_REQUEST));
        list.add(new HomeItem("طلب " + "\n" + "شراء", 1, Constant.PAGES_BUY_REQUEST));
        list.add(new HomeItem("الخدمات الفنية والتقنية", 2, Constant.PAGES_SERVICES));

        adapter.notifyDataSetChanged();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.request_book:
//                openActivity(this, MainActivity.class, "page_type", Constant.REQUEST_BOOK);
//                break;
//
//            case R.id.request_to_publish_book:
//                openActivity(this, MainActivity.class, "page_type", Constant.REQUEST_TO_PUBLISH_BOOK);
//                break;


        }
    }
}
