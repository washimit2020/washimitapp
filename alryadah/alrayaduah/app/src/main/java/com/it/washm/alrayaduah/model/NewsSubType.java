package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 1/9/17.
 */

public class NewsSubType {
    String id;
    String name_en;
    String name_ar;
    String news_main_type_id;


    public NewsSubType(String id, String name_en, String name_ar, String news_main_type_id) {
        this.id = id;
        this.name_en = name_en;
        this.name_ar = name_ar;
        this.news_main_type_id = news_main_type_id;
    }

    @Override
    public String toString() {
        return name_ar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getNews_main_type_id() {
        return news_main_type_id;
    }

    public void setNews_main_type_id(String news_main_type_id) {
        this.news_main_type_id = news_main_type_id;
    }
}
