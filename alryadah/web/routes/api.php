<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'appKey'], function () {


    /**
     *  gets with app key middleware
     **/

    Route::get('user-roles', 'TypesController@user_roles');
    Route::get('news-types', 'TypesController@news_types');
    Route::get('news-statuses', 'TypesController@news_statuses');
    Route::get('user-attachment-types', 'TypesController@user_attachment_types');

    Route::post('reset-password', 'Auth\ForgotPasswordController@sendResetLinkEmail');

    Route::get('news', 'NewsController@index');
    Route::get('news/{id}', 'NewsController@show')->middleware('news.action');

    Route::get('categorized_news/{main}/{sub_main?}', 'NewsController@type');

    Route::post('search', 'NewsController@search');


    Route::get('comments/{news_id}', 'CommentsController@index')->middleware('news.action');
    Route::get('replies/{comment_id}', 'ReplyController@index');

    /**
     *  end gets
     **/

    Route::group(['middleware' => 'auth:api'], function () {
        /*
         * user
         */
        Route::post('news', 'NewsController@store');

        Route::get('wishlist', 'WishlistController@index');


        Route::get('profile', 'Auth@profile');

        Route::post('profile', 'Auth@update_profile');

        Route::get('profile/news/{type}', 'Auth@get_news')->middleware('write.news');

        Route::get('notifications', 'Auth@get_notifications');

        Route::get('notifications/{id}', 'Auth@read_notification');

        /**
         * end gets
         */

        /**
         * stores
         */

        Route::group(['middleware' => 'news.action'], function () {

            Route::post('wishlist', 'WishlistController@store');

            Route::post('comments', 'CommentsController@store');

            Route::post('like/news', 'LikeController@news');

        });

        Route::post('like/reply', 'LikeController@reply');
        Route::post('like/comment', 'LikeController@comment');


        Route::post('replies', 'ReplyController@store');
        Route::delete('replies/{id}', 'ReplyController@destroy');

        Route::post('report/reply', 'ReportController@reply');
        Route::post('report/comment', 'ReportController@comment');

        Route::delete('comments/{id}', 'CommentsController@destroy');

//        Route::post('add_attachment', 'NewsController@add_attachment');

        /**
         * end stores
         */
    });


    /**
     * user login and sign up
     */

    Route::post('login', 'Auth@login');

    Route::post('signup', 'Auth@sign_up');

});


Route::get('image/{folder}/{id}/{type}/{file}', function ($folder, $type, $id, $file) {
    $path = implode('/', [$folder, $type, $id, $file]);

    if (!Storage::exists($path)) abort(404);

    $file = Storage::get($path);
    $type = Storage::mimeType($path);

    $response = response()->make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});


