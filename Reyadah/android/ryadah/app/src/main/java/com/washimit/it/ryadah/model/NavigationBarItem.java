package com.washimit.it.ryadah.model;

/**
 * Created by mohammad on 4/24/2017.
 */

public class NavigationBarItem {
    int id;
    String name;
    int img;

    public NavigationBarItem(int id, String name, int img) {
        this.id = id;
        this.name = name;
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
