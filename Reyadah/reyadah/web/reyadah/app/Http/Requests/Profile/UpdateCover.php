<?php

namespace App\Http\Requests\Profile;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCover extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return auth()->user()->can(Constants::ABILITY_ADD_PRODUCT);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required|min:6|max:'.Constants::MAX_STRING_LENGTH,
            'cover' => 'required|'.Constants::MIME_TYPE,
            'where' => 'required|in:0,1,2',
        ];
    }
}
