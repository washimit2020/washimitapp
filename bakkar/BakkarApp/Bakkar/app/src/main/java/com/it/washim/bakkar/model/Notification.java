package com.it.washim.bakkar.model;

/**
 * Created by mohammad on 11/20/2017.
 */

public class Notification {
    public int id;
    public int user_id;
    public NotificationData data;
    public String type;
    public String read_at;
    public String created_at;
    public String updated_at;
}



