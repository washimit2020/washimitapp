//
//  UsersLocationsMap_ViewController.swift
//  reyadah
//
//  Created by WASHM on 5/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import MapKit
class UsersLocationsMap_ViewController: UIViewController,  CLLocationManagerDelegate,MKMapViewDelegate{
    var items = [Branch]()
    
    
    let identifier = "CustomAnnotation"
    
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var ratingStars: CosmosView!
    
    var icons = [
        [
            "selected":UIImage(named:"ic_store_location_active")!,
            "normal":UIImage(named:"ic_store_location_deactive")!
        ],
        [
            "selected":UIImage(named:"ic_showroom_location_active")!,
            "normal":UIImage(named:"ic_showroom_location_deactive")!
        ],
        [
            "selected":UIImage(named:"ic_festival_location_active")!,
            "normal":UIImage(named:"ic_festival_location_deactive")!
        ],
        
        ]
    
    //    var icon:UIImage = UIImage(named:"ic_showroom_location_active")!
    //    var festival_icon:UIImage = UIImage(named:"ic_festival_location")!
    //    var selectedIcon:UIImage = UIImage(named:"ic_showroom_location_deactive")!
    
    var selected_item:Branch?
    var icon_index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup_cosmos(cosmos: ratingStars)
        map.delegate = self
        navigationItem.title = " "

    }
    
    func updateView()  {
        guard  let map = map else {
            return
        }
        if items.count > 0{
            set_up_view(item: items[0])
            
            for (key,item) in items.enumerated(){
                guard let lat = CLLocationDegrees(item.lat) , let lng = CLLocationDegrees(item.lng) else {
                    continue;
                }
                
                set_index(user: item)
                
                let annot = MKPointAnnotation();
                annot.title = "\(key)"
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                annot.coordinate = coordinate
                
                map.addAnnotation(annot)
                if map.annotations.count == 1 {
                    let span = MKCoordinateSpanMake(0.075, 0.075)
                    let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: lat, longitude: lng), span: span)
                    map.setRegion(region, animated: true)
                    
                    map.selectAnnotation(annot, animated: true)
                }
            }
        }
        
    }
    
    func set_index(user:User){
        if user is Festival{
            icon_index = 2
        }else{
            if user.type.id == 0 {
                icon_index = 1
            }else{
                icon_index  = user.type.id - 1
            }
        }
    }
    
    @IBAction func didTapImage(_ sender: Any) {
        
        let details =  Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        
        details.model = selected_item!
        self.navigationController?.pushViewController(details, animated: true)
        
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = false
            annotationView!.image = icons[icon_index]["normal"]
            
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        view.image = icons[icon_index]["normal"]
        
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annot = view.annotation , let title = annot.title! ,let index =  Int(title) else{
            return
        }
        
        view.image = icons[icon_index]["selected"]
        let item = items[index]
        set_up_view(item: item)
    }
    
    func set_up_view(item:Branch){
        selected_item = item
        if item.user_id == 0{
            ratingStars.rating = item.rating
            avatar.set_avatar_image(user_id: item.id)
            
        }else{
            avatar.set_avatar_image(user_id: item.user_id)
        }
        
        name.text = item.name
        locationText.text = make_location_text(item)
    }
}
