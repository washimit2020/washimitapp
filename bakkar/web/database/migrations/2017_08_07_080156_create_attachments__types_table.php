<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttachmentsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments__types', function (Blueprint $table) {
            $table->increments("id");
            $table->string("title");
        });
        \Illuminate\Support\Facades\DB::table('attachments__types')->insert([
                [
                    'title' => "attachments.book",
                ],
                [
                    'title' => "attachments.cover",
                ],
                [
                    'title' => "attachments.audio",
                ],

            ]

        );

        Schema::table('attachments', function (Blueprint $table) {
            $table->unsignedInteger("attachment_type_id");

            $table->foreign('attachment_type_id')
                ->references('id')->on('attachments__types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attachments__types');
    }
}
