package com.it.washim.wojooohapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.activitys.BookDetailsActivity;
import com.it.washim.wojooohapp.activitys.BookShowDetailsActivity;
import com.it.washim.wojooohapp.activitys.DistributionAgentsDetailsActivity;
import com.it.washim.wojooohapp.activitys.TecnhnicalAndServesDetalisActivity;
import com.it.washim.wojooohapp.adapter.ExpandableAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpandableFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @BindView(R.id.expandable_list)
    ExpandableListView listView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.no_data)
    TextView no_data;
    @BindView(R.id.retray)
    MyButtonView retray;
    ExpandableAdapter adapter;
    ArrayList<Post> grope;
    int fragmentType;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_expandable, container, false);
        ButterKnife.bind(this, view);
        fragmentType = getArguments().getInt("fragment_type");
        grope = new ArrayList<>();

        adapter = new ExpandableAdapter(getActivity(), grope,fragmentType);

        listView.setAdapter(adapter);
        swipeRefresh.setOnRefreshListener(this);
        retray.setOnClickListener(this);
        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < grope.size(); i++) {
                    if (groupPosition != i) {
                        listView.collapseGroup(i);
                    }
                }
            }
        });

        // Listview on child click listener
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
//                if (fragmentType == Constant.PAGES_SERVICES) {
//                    openActivity(getActivity(), TecnhnicalAndServesDetalisActivity.class, "TecnhnicalAndServesDetalis", getGson().toJson(grope.get(groupPosition).children.get(childPosition), Post.class));
//                } else
                 if (fragmentType == Constant.PAGES_BOOKS_SHOWROOM) {
                    openActivity(getActivity(), BookShowDetailsActivity.class, "BookShow", getGson().toJson(grope.get(groupPosition).children.get(childPosition), Post.class));
                }
//                else if (fragmentType == Constant.PAGES_SPONSOR) {
//                    openActivity(getActivity(), DistributionAgentsDetailsActivity.class, "DistributionAgentsDetails", getGson().toJson(grope.get(groupPosition).children.get(childPosition), Post.class));
//                }

                return false;
            }
        });


        swipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(true);
                getData(fragmentType);

            }
        });


        return view;
    }


    public void getData(int pageId) {

        no_data.setVisibility(View.GONE);
        swipeRefresh.setVisibility(View.VISIBLE);
        retray.setVisibility(View.GONE);

        getRequest().request(Request.Method.GET, "posts/" + pageId, null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                grope.clear();
                grope.addAll(response.data);
                adapter.notifyDataSetChanged();
                swipeRefresh.setRefreshing(false);

                if (grope.size() > 0) {

                    no_data.setVisibility(View.GONE);
                    swipeRefresh.setVisibility(View.VISIBLE);
                } else {
                    no_data.setText("لا يوجد");

                    no_data.setVisibility(View.VISIBLE);
                    swipeRefresh.setVisibility(View.GONE);
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                no_data.setText("لا يوجد اتصال بالنترنت");
                no_data.setVisibility(View.VISIBLE);
                retray.setVisibility(View.VISIBLE);
                swipeRefresh.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onRefresh() {
        getData(fragmentType);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray:
                swipeRefresh.setRefreshing(true);
                getData(fragmentType);
                break;


        }
    }
}
