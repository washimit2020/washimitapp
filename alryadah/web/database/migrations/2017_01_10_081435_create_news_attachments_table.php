<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('path');
            $table->string('mime_type');
            $table->unsignedInteger('news_attachment_type_id')->index();
            $table->unsignedInteger('news_id')->index();
            $table->timestamps();

            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('news_attachment_type_id')->references('id')->on('news_attachments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Illuminate\Support\Facades\Storage::deleteDirectory("news");

        Schema::dropIfExists('news_attachments');
    }
}
