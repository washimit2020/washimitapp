package com.it.washm.alrayaduah.interfase;

import android.app.Dialog;

/**
 * Created by washm on 1/28/17.
 */


public interface dialogCallback {
    /**
     *
     * @param dialog
     */
    public void ok(Dialog dialog);

    /**
     *
     * @param dialog
     */
    public void hidden(Dialog dialog);
}
