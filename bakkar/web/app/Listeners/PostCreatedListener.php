<?php

namespace App\Listeners;

use App\Events\PostCreated;

class PostCreatedListener
{


    /**
     * Handle the event.
     *
     * @param  PostCreated  $event
     * @return void
     */
    public function handle(PostCreated $event)
    {
        $event->send_notification();
    }
}


