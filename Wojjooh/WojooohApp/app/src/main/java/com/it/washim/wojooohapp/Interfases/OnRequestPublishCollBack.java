package com.it.washim.wojooohapp.Interfases;

import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.Model.Data;

/**
 * Created by mohammad on 9/19/2017.
 */

public interface OnRequestPublishCollBack {
    void onResponse(String response);

    void onErrorResponse(VolleyError error);
}
