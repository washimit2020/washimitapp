package com.it.washm.alrayaduah.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.registration.LogInActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {


    Spinner time, date;
    Switch notification;
    LinearLayout share, log_out;
    SharedPref pref;
    Intent intent;
    TextView time_Type, date_Type, sahar_app, notification_text, app_version, app_version_title;
    CustamViewFont viewFont;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        time_Type = (TextView) view.findViewById(R.id.time_Type);
        date_Type = (TextView) view.findViewById(R.id.date_Type);
        sahar_app = (TextView) view.findViewById(R.id.sahar_app);
        notification_text = (TextView) view.findViewById(R.id.notification_text);
        app_version = (TextView) view.findViewById(R.id.app_version);
        app_version_title = (TextView) view.findViewById(R.id.app_version_title);

        pref = new SharedPref(getContext(), "MyPref");
        time = (Spinner) view.findViewById(R.id.time);
        date = (Spinner) view.findViewById(R.id.date);
        notification = (Switch) view.findViewById(R.id.notification);
        share = (LinearLayout) view.findViewById(R.id.share);
        log_out = (LinearLayout) view.findViewById(R.id.log_out);
        viewFont = new CustamViewFont(getActivity(), "Cairo-Regular.ttf");

        setFont();

        if (!pref.gitShared("is_log_in").equalsIgnoreCase("1")){
            log_out.setVisibility(View.GONE);
        }

        if (pref.gitShared("time").equalsIgnoreCase("24")) {

            time.setSelection(0);


        } else {
            time.setSelection(1);

        }
        if (pref.gitShared("date").equalsIgnoreCase("m")) {
            date.setSelection(0);


        } else {
            date.setSelection(1);

        }
        if (pref.gitShared("notification").equalsIgnoreCase("1")) {
            notification.setChecked(true);

        } else {
            notification.setChecked(false);
        }


        time.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        //time_type
                        pref.setShared("time", "24");

                        break;
                    case 1:
                        pref.setShared("time", "12");

                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        date.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {

                    case 0:
                        //time_type
                        pref.setShared("date", "m");

                        break;
                    case 1:
                        pref.setShared("date", "h");
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        notification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {

                    if (isChecked) {
                        pref.setShared("notification", "1");

                    } else {
                        pref.setShared("notification", "0");

                    }
                } else {
                    notification.setChecked(false);
                    AppHelper.showDialog(getActivity(), "يجب تسجيل الدخول لتتمكن من استخدام هذه الاداة","تسجيل الدخول","", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getActivity(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
                }
            }
        });


        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppHelper.showDialog(getActivity(), "هل انت متأكد من تسجيل الخروج؟","تسجيل الخروج","الغاء", true, new dialogCallback() {
                    @Override
                    public void ok(Dialog dialog) {
                        pref = new SharedPref(getActivity(), "MyPref");
                        pref.clearRef();
                        intent = new Intent(getActivity(), LogInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finishAffinity();
                    }

                    @Override
                    public void hidden(Dialog dialog) {
                        dialog.cancel();
                    }
                });
            }
        });


        return view;
    }

    public void setFont() {
        viewFont.setfont(time_Type);
        viewFont.setfont(date_Type);
        viewFont.setfont(sahar_app);
        viewFont.setfont(notification_text);
        viewFont.setfont(app_version_title);
        viewFont.setfont(app_version);

    }
}

