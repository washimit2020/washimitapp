<?php

namespace App\Http\Controllers;

use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\Replies\Store;
use App\Reply;
use App\ReplyLike;
use App\User;
use Illuminate\Http\Request;

class ReplyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = get_user_obj();

        $replies = Reply::with(["user" => $user])
            ->withCount('likes')
            ->where('comment_id', $id)->latest()
            ->paginate(10);

        foreach ($replies as $reply) {
            $data['reply_id'] = $reply->id;
            $reply->is_liked = check_status($data, new ReplyLike());

        }

        return response()->json(ResponseHelper::successResponse(["replies" => $replies]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $request["user_id"] = $request->user()->id;
        $reply = Reply::create($request->all());

        return response()->json(ResponseHelper::successResponse(["reply" => $reply]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reply = Reply::findOrFail($id);
        $this->authorize('delete', $reply);

        $reply->delete();
        return response()->json(ResponseHelper::successResponse(["deleted" => true]));
    }
}
