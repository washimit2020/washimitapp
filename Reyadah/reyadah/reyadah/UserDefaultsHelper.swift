//
//  UserDefaultsHelper.swift
//  reyadah
//
//  Created by WASHM on 4/18/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import Foundation
import SwiftyJSON
import GoogleSignIn

class UserDefaultsHelper {
    
    static func get_current_user_type() -> Int {
        return UserDefaults.standard.integer(forKey: "user_type_id")
    }
    
    static func store_user_data(_ data:Data){
        UserDefaults.standard.set(data, forKey: "user")
    }
    
    
    
    static func get_user_data()->Branch?{
        guard let data = UserDefaults.standard.object(forKey: "user") as? Data else {
            return nil
        }
       
        let json = JSON(data)
        
        if json.isEmpty {
            return nil
        }
        
        let user  = Branch(json, beginWith: "user")
        return user
    }
    
    static func get_user_token()->String{
        
        guard let data = UserDefaults.standard.object(forKey: "user") else {
            return ""
        }
        
      
        if let token  = JSON(data)["msg"].dictionaryValue["token"]?.stringValue{
            return token
        }
        
        

        return ""
    }
    static func logOut(){
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.removeObject(forKey: "user_type_id")
        UserDefaults.standard.removeObject(forKey: "social_token")
        GIDSignIn.sharedInstance().signOut()
    }
    
    static func store_social_token(token:String){
        UserDefaults.standard.set(token, forKey: "social_token")
    }
    
    static func get_social_token()->String?{
        return UserDefaults.standard.string(forKey: "social_token")
    }

    static func store_is_first(){
        UserDefaults.standard.set(true, forKey: "first_time")
    }
    static func is_first()->Bool?{
        return UserDefaults.standard.bool(forKey: "first_time")
    }
    static func remove_is_first(){
         UserDefaults.standard.removeObject(forKey: "first_time")
    }
}

