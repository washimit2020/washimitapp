//
//  ApiRequest.swift
//  reyadah
//
//  Created by WASHM on 4/15/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import Foundation
import Alamofire
import Kingfisher
import SlideMenuControllerSwift
import SwiftyJSON

enum RequestLinks : String{
    case login = "login"
    case register = "register"
    case forgotPassword = "password/email"
    case get_users = "users/"
    case festivals = "festivals"
    case updateProfile = "update/profile"

    case user_details = "user/"
    case following = "following/"

    case follow = "follow"
    case product = "product"
    case explorer = "product/explorer"
    case rateUser = "rating/user"
    case rateFestival = "rating/festival"
    case rateProduct = "rating/product"
    case followed_by = "followed-by"
    case create_branch = "branch"
    case like = "product/like"
    case wishlist = "product/wishlist"
    case comment = "comment"

    case get_product_comments = "comment/products/"
    case get_festival_comments = "comment/festivals/"

    case search = "search"
    case block = "block"
    case share = "product/share"
    case productShares = "product/shares/"

}


class ApiRequest {
    
    var manager:NetworkReachabilityManager!
    var costumeAlert:LoadingDialog_ViewController!
    
    var headers: HTTPHeaders = [
        "app-key": "base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84=",
        "Accept": "application/json",
       
    ]
    
    //    static let modifier = AnyModifier { request in
    //        var r = request
    //        r.setValue("base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84=", forHTTPHeaderField: "app-key")
    //        return r
    //    }
    init() {
        manager = NetworkReachabilityManager(host:Constants.domain)
        headers["Authorization"] =  "Bearer \(UserDefaultsHelper.get_user_token())"
        
        manager?.startListening()
        
      
    }
    
    public func make_request(
        parameters:Dictionary<String,String>? = nil ,
         apiImages:[String:UIImage]? = nil,
        link:String ,
        method:HTTPMethod,
        dialogMsg:String?=nil,
        viewController:UIViewController?=nil,
        completion:((_ response:DataRequest?) -> Void)?
        ){
        
        if let msg = dialogMsg{
            costumeAlert = make_loading_dialog(msg)
            if let viewController = viewController {
                viewController.present(costumeAlert, animated: true, completion: nil)
            }else{
                UIApplication.shared.keyWindow?.rootViewController?.present(costumeAlert, animated: true, completion: nil)
            }
        }
        
        let to = Constants.host + link
        if !manager.isReachable {
            handle_error_dialog(nil)
            return
        }
        if method == .get{
            completion?(Alamofire.request(to, headers: headers))
            
        }else{
            
            Alamofire.upload(multipartFormData: {
                formData in
                if let images = apiImages{
                    for (name,image) in images{
                        print(name)
                        formData.append(image.jpeg(.medium)!, withName: name, fileName: name, mimeType: "image/jpeg")
                    }
                }
                if let parameters = parameters{
                    for parameter in parameters{
                        formData.append(parameter.value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: parameter.key)
                    }
                }
                
            }, usingThreshold: .max,
               to: to, method: method, headers: headers, encodingCompletion:  {
                encodingResult in
                
                switch  encodingResult{
                case .success(let upload, _, _):

                    upload.responseJSON(completionHandler: {
                    
                        if  $0.response?.statusCode != Constants.RESPONSE_CODE_UNAUTHORIZED{
                            completion?(upload)
                            return
                        }

                        let controller =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeVc") as! HomeVC
                        
                        let navigation = UINavigationController(rootViewController: controller)
                        UIApplication.shared.keyWindow?.rootViewController = navigation
                        UserDefaultsHelper.logOut()
                        self.costumeAlert = make_loading_dialog("يجب تسجيل الدخول")
                       
                        controller.present(self.costumeAlert, animated: false, completion: {

                            self.costumeAlert.closeButton.isHidden = false
                            self.costumeAlert.loader.stopAnimating()
                        })
                        
                        return
                    })
                
                    
                case .failure( let error) :
                    self.handle_error_dialog("واجهنا مشكلة اعد المحاولة لاحقا")
                    break
                }
            })
            
        }
    }
    
    
    
    public func handle_error_dialog(_ text:String?){
        guard let alert = costumeAlert , costumeAlert.isViewLoaded else {
            return
        }
        if let text = text{
            alert.label.text = text
        }else{
            alert.label.text = "تاكد من اتصالك بالانترنت"
        }
        alert.closeButton.isHidden = false
        alert.loader.stopAnimating()
    }
    
    
    
    public func likeOrWishlist(_ sender:AspectFitButton ,_ product:Product,_ total_likes :UILabel ) {
        var link = RequestLinks.wishlist.rawValue;
        
        if sender.tag == 2{
            link = RequestLinks.like.rawValue
            if product.is_liked {
                product.total_likes = "\(Int(product.total_likes!)! - 1)"
            }else{
                product.total_likes = "\(Int(product.total_likes!)! + 1)"
                
            }
            total_likes.text = "\(product.total_likes!) likes"
            
            product.is_liked = !product.is_liked
            change_tint_of_button(button: sender, checked: product.is_liked)

        }else{
            product.is_wishlisted = !product.is_wishlisted
            change_tint_of_button(button: sender, checked: product.is_wishlisted)
        }
        
        call_wishlist_like_api(link: link,product: product)
    }
    
    
    public func call_wishlist_like_api(link:String,product:Product){
        self.make_request(parameters: ["product_id":"\(product.id!)"],apiImages: nil,link: link, method: .post,  completion: {
            $0?.responseJSON{
                let json = JSON($0.data)
                
                if let total_likes = json["msg"].dictionary?["total_likes"]{
                    print(total_likes)
                    product.total_likes = total_likes.stringValue
                }
            }
        })
    }
    
    public func call_follow_unfollow(_ user_id:Int, _ follow_type:String = "\(Constants.TYPE_FOLLOW_USER)"){
        var params = ["follow_type": follow_type]
        if follow_type == "\(Constants.TYPE_FOLLOW_USER)"{
            params["user_id"] = "\(user_id)"
        }else{
            params["festival_id"] = "\(user_id)"

        }
        self.make_request(parameters: params,apiImages: nil,link: RequestLinks.follow.rawValue, method: .post,  completion: nil)
    }

}


