package com.it.washm.alrayaduah.helper;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.it.washm.alrayaduah.interfase.MultipartCallback;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.news;


/**
 * Created by washm on 1/7/17.
 */

public class Requests {
    private String result = "";
    private Activity activity;
    private Context context;
    private ProgressDialog pDialog;
    private String masgDilog = "";
    Vector<news> newses;

    private final String host = "http://alradyeh.wojoooh.com/api/";
    //private final String host="http://192.168.1.112/alreyadah/web/public/api/";
    public static RequestQueue queue;
    SharedPref pref;
    private NotificationManager mNotifyManager;
    private NotificationCompat.Builder mBuilder;

    public Requests(Context context, Activity activity) {
        this.activity = activity;
        this.context = context;
        queue = Volley.newRequestQueue(context);
        pref = new SharedPref(activity, "MyPref");
    }

    /**
     * @param userRequest
     * @param url
     * @param Params
     * @param masgDilog
     * @param callback
     * @return
     */
    public String StringRequest(int userRequest, String url, final HashMap<String, String> Params, final String masgDilog, final VolleyCallback callback) {


        if (masgDilog.length() > 1) {
            this.masgDilog = masgDilog;
            showpDialog();
        }
        StringRequest stringRequest = new StringRequest(userRequest, host + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                result = response;
                if (masgDilog.length() > 1) {

                    hideDialog();
                }
                callback.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onRequestError(error);
                if (masgDilog.length() > 1) {

                    hideDialog();
                }

            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return Params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<String, String>();
                header.put("Accept", "application/json");
                header.put("app-key", "base64:x22vAb9QjxPLi9oJCXPix4gS0rzrQkdf7KLUjrLT/MY=");
                header.put("Authorization", "Bearer" + " " + pref.gitShared("token"));


                return header;
            }


        };
        Volley.newRequestQueue(context).add(stringRequest);

        return result;
    }

    /**
     * @param userRequest
     * @param url
     * @param masgDilog
     * @param callback
     * @return
     */
    public String StringRequest(int userRequest, String url, String masgDilog, boolean fullURL, final VolleyCallback callback) {
        String rurl;
        if (fullURL) {
            rurl = host + url;
        } else {
            rurl = url;
        }
        StringRequest stringRequest = new StringRequest(userRequest, rurl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                result = response;
                callback.onSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onRequestError(error);


            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<String, String>();
                header.put("Accept", "application/json");
                header.put("app-key", "base64:x22vAb9QjxPLi9oJCXPix4gS0rzrQkdf7KLUjrLT/MY=");
                header.put("Authorization", "Bearer" + " " + pref.gitShared("token"));

                return header;
            }
        };

        queue.add(stringRequest);

        return result;
    }

    /**
     * @param url
     * @param masg
     * @param prams
     * @param callback
     */
    public void MultipartRequest(String url, String masg, List<Part> prams,
                                 final MultipartCallback callback) {
//        mNotifyManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        mBuilder = new NotificationCompat.Builder(this);
        this.masgDilog = masg;
        showpDialog();
        Ion.with(activity)
                .load("POST", host + url)
                .setHeader("Accept","application/json")
                .setHeader("app-key","base64:x22vAb9QjxPLi9oJCXPix4gS0rzrQkdf7KLUjrLT/MY=")
                .setHeader("Authorization","Bearer" + " " + pref.gitShared("token"))
                .setLogging("MyLogs", Log.DEBUG)
                .uploadProgressHandler(new ProgressCallback() {
                    @Override
                    public void onProgress(long uploaded, long total) {
//                        // Displays the progress bar for the first time.
//                        mNotifyManager.notify(1, mBuilder.build());
//                          mBuilder.setProgress((int) total, (int) uploaded, false);
                    }
                })
                .addMultipartParts(prams)
//              .setMultipartParameter("title","mohammed")
                // .setMultipartFile("profile_image", "image/jpeg", main_attachment)
                //  .addMultipartParts(files)
//              .setMultipartFile("attachments["+(1)+"]", "video/mp4", fileToUpload2)

                .asJsonObject()
                // run a callback on completion
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        // When the loop is finished, updates the notification
//                        mBuilder.setContentText("Upload complete")
                        // Removes the progress bar
//                                .setProgress(0, 0, false);
//                        mNotifyManager.notify(1, mBuilder.build());
                        hideDialog();
                        if (e != null) {
                            try {
                                callback.onRequestError(result.toString());

                            } catch (Exception v) {

                            }
                            return;
                        }
                        try {
                            Log.d("result", result + "");
                            callback.onSuccess(result.toString());
                        } catch (Exception v) {

                        }

                    }
                });
    }


    private void showpDialog() {
        onProgress();
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void onProgress() {
        pDialog = new ProgressDialog(activity);
        pDialog.setMessage(masgDilog);
        pDialog.setCancelable(false);
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }


}