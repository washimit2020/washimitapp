<?php

namespace App\Http\Controllers;

use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\Comment;
use App\Http\Constants;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    public function store(Request $request)
    {

        $this->validate($request, [
            'type' => "required|in:" . make_validation_array_in([Constants::PRODUCT_COMMENT, Constants::FESTIVAL_COMMENT]),
            "product_id" => 'required_if:type,' . Constants::PRODUCT_COMMENT . '|exists:products,id,status_id,' . Constants::_STATUS_ACTIVE,
            "festival_id" => 'required_if:type,' . Constants::FESTIVAL_COMMENT . '|'.make_festival_validation_string(),
            "body" => 'required|max:5000',
        ]);

        $comment = new Comment();
        $comment->body = $request->body;
        $comment->user_id = $request->user()->id;
        if ($request->type == Constants::PRODUCT_COMMENT) {

            $comment = Product::find($request->product_id)->comments()->create($comment->toArray());
        } else {
            $comment = Festival::find($request->festival_id)->comments()->create($comment->toArray());
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["comment" => $comment]));
    }

    public function index($type, $id)
    {
        $comments = Comment::where([
            ['commentable_type', $type],
            ['commentable_id', $id],
            ['status_id', Constants::_STATUS_ACTIVE],
        ])->whereHas('user',function ($query){
           check_if_blocked($query);

        })->latest()->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["comments" => $comments]));
    }

    public function update(Comment $comment, Request $request)
    {
        $this->validate($request, [
            "body" => 'required|max:5000',
        ]);

        $comment->body = $request->body;
        $comment->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["comment" => $comment]));
    }

    public function destroy(Comment $comment)
    {
        $comment->status_id = Constants::_STATUS_DELETED;
        $comment->update();
        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["deleted"]));
    }
}
