package com.it.washim.bakkar.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
//import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.custemView.ObservableScrollView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnDialogCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookDetailsActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.scrollView)
    ObservableScrollView scrollView;
    @BindView(R.id.wish_listed)
    ImageView wish_listed;

    @BindView(R.id.shar)
    ImageView shar;
    @BindView(R.id.sating)
    ImageView sating;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.book_img)
    ImageView book_img;
    @BindView(R.id.book_des)
    MyTextView book_des;
    @BindView(R.id.book_title)
    MyTextView book_title;
    @BindView(R.id.book_details)
    RelativeLayout book_details;
    @BindView(R.id.rating)
    RatingBar rating;
    private int bookId;
    private boolean isWishlisted;
    private double rating_;
    private Bundle bundle;
    private HashMap<String, String> map;
    private Intent intent;
    private boolean screnMode = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
        map = new HashMap<>();
        intent = new Intent();

        if (bundle != null) {

            if (bundle.get("image_id") != null) {
                Picasso.with(this).load(Constant.URL + "attachments/" + bundle.getInt("image_id")).placeholder(R.drawable.bakkar_icon).into(book_img);
            }
            bookId = bundle.getInt("book_id");
            isWishlisted = bundle.getBoolean("is_wishlisted");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                book_des.setText(Html.fromHtml(bundle.getString("book_description"), Html.FROM_HTML_MODE_COMPACT));
            } else {
                book_des.setText(Html.fromHtml(bundle.getString("book_description")));

            }
            rating.setRating((float) bundle.getDouble("rated"));
            book_title.setText(bundle.getString("book_title"));
            wish_listed.setImageDrawable(getResources().getDrawable(isWishlisted ? R.drawable.bakar_icon_favorite : R.drawable.bakar_icon_favorite_of));

        }

        scrollView.setScrollViewListener(new ObservableScrollView.ScrollViewListener() {
            @Override
            public void onScrollChanged(ObservableScrollView scrollView, int x, int y, int oldx, int oldy) {

                View view = scrollView.findViewById(R.id.book_img);

                if (view != null) {
                    view.setTranslationY(scrollView.getScrollY() / 2);
                }

            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        wish_listed.setOnClickListener(this);
        shar.setOnClickListener(this);
        sating.setOnClickListener(this);
        update();

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float r, boolean fromUser) {


                if (shardPref().getUser() != null) {
                    if (fromUser) {

                        ratingBook(r);

                    }
                } else {
                    if (fromUser) {
                        showAlertDialog("حتى تتمكن من تقييم الكتاب قم بتسجيل الدخول اولا :)", new OnDialogCollBack() {
                            @Override
                            public void login() {
                                startActivity(new Intent(BookDetailsActivity.this, HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            }

                            @Override
                            public void hide() {

                            }
                        });
                    }
                    rating.setRating(0);

                }


            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.wish_listed:
                if (shardPref().getUser() != null) {
                    wish_listed.setImageDrawable(getResources().getDrawable(isWishlisted ? R.drawable.bakar_icon_favorite_of : R.drawable.bakar_icon_favorite));
                    isWishlisted = !isWishlisted;
                    setResult();

                    map.put("post_id", bookId + "");
                    request().stringRequest(Request.Method.POST, getApplicationContext(), "wishlist", map, new OnRequestCollBack() {
                        @Override
                        public void onResponse(String response) throws JSONException {
                            //Log.d("wishlist", response);
                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //Log.d("wishlist", error.getMessage() + "rrrr");

                        }
                    });
                } else {
                    showAlertDialog("حتى تتمكن من اضافة الكتاب الى مفضلتك قم بتسجيل الدخول :)", new OnDialogCollBack() {
                        @Override
                        public void login() {
                            startActivity(new Intent(BookDetailsActivity.this, HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        }

                        @Override
                        public void hide() {

                        }
                    });
                }
                break;
            case R.id.shar:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "مشاركة");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "www.google.com");
                startActivity(Intent.createChooser(sharingIntent, "مشاركة"));
                break;
            case R.id.sating:
                settingsDialog();
                break;

        }
    }


    private void settingsDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_sating);

        Button day_mode = (MyButtonView) dialog.findViewById(R.id.day_mode);
        final MyTextView proogresbar = (MyTextView) dialog.findViewById(R.id.proogresbar);
        Button night_mode = (MyButtonView) dialog.findViewById(R.id.night_mode);
        final SeekBar text_size = (SeekBar) dialog.findViewById(R.id.text_size);
        final Spinner text_font_type = (Spinner) dialog.findViewById(R.id.text_font_type);
        final MyButtonView hide = (MyButtonView) dialog.findViewById(R.id.hide);


        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("TheSansArab-Plain_0.ttf");
        arrayList.add("TheSansArab-Light_0.ttf");
        arrayList.add("TheSansArab-Black_0.ttf");


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter(BookDetailsActivity.this, android.R.layout.simple_spinner_dropdown_item, arrayList);

        text_font_type.setAdapter(arrayAdapter);

        hide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        text_font_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        text_size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                proogresbar.setText(progress + "%");
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();
            }
        });


        day_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = true;

                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);

                update();

            }
        });
        night_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                screnMode = false;
                shardPref().setFontType(text_font_type.getSelectedItem().toString());
                shardPref().SetTextSize(text_size.getProgress() > 14 ? text_size.getProgress() : 14);
                shardPref().SetScreenMode(screnMode);
                update();

            }
        });


        screnMode = shardPref().getScreenMode();
        text_size.setProgress(shardPref().getTextSize());

        dialog.show();


    }


    private void update() {

        book_des.setTextSize(shardPref().getTextSize() > 14 ? shardPref().getTextSize() : 14);
        book_title.setTextSize(shardPref().getTextSize() > 14 ? shardPref().getTextSize() - 4 : 10);
        book_des.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + shardPref().getFontType()));
        book_title.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/" + shardPref().getFontType()));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            book_des.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white, null));
            book_title.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white, null));
            book_details.setBackground(getResources().getDrawable(shardPref().getScreenMode() ? R.color.white : R.color.black, null));

        } else {
            book_des.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white));
            book_title.setTextColor(getResources().getColor(shardPref().getScreenMode() ? R.color.black : R.color.white));
            book_details.setBackground(getResources().getDrawable(shardPref().getScreenMode() ? R.color.white : R.color.black));

        }


    }

    private void ratingBook(double v) {
        rating_ = v;
        setResult();
        HashMap<String, String> map = new HashMap<>();
        map.put("post_id", bookId + "");
        map.put("rating", v + "");
        request().stringRequest(Request.Method.POST, this, "rate", map, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                //Log.d("asafs", response);
            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


    }


    public void setResult() {


        intent.putExtra("is_wishlisted", isWishlisted);
        intent.putExtra("rating", rating_);
        setResult(1, intent);
    }
}
