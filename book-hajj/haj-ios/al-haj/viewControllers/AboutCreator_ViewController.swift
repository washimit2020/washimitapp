//
//  AboutCreator_ViewController.swift
//  al-haj
//
//  Created by WASHM on 1/2/18.
//  Copyright © 2018 WASHM. All rights reserved.
//

import UIKit

class AboutCreator_ViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "background1-page1"))
        scrollView.layer.isOpaque = false
        navigationItem.addTitleImage()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if slideMenuController()?.mainViewController != nil {
            addRightBarButtonWithImage()
        }else{
            navigationItem.rightBarButtonItem = nil
        }
    }
   

}
