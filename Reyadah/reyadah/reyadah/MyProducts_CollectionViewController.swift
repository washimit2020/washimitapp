//
//  MyProducts_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/14/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

private let reuseIdentifier = "profileProduct"

class MyProducts_CollectionViewController: ItemsLoader_CollectionViewController {

    var user:User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        dataModelChanger = self
        link = RequestLinks.user_details.rawValue + "\(UserDefaultsHelper.get_user_data()!.id!)"
        beginWithModel = "products"
        
        self.collectionView!.register(UINib(nibName: "ProfileProduct_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        clearData()
        self.emptyLabel?.frame.origin.y = collectionView!.frame.height/4

        // Do any additional setup after loading the view.
        
        DispatchQueue.global().async {
            let share = UIMenuItem(title: "مشاركة", action: #selector(ProfileProduct_CollectionViewCell.shareProduct))
            let edit = UIMenuItem(title: "تعديل", action: #selector(ProfileProduct_CollectionViewCell.editProduct))
            let delete = UIMenuItem(title: "حذف", action: #selector(ProfileProduct_CollectionViewCell.deleteProduct))
            let update = UIMenuItem(title: "اظهار/اخفاء", action: #selector(ProfileProduct_CollectionViewCell.hideProduct))
            
            UIMenuController.shared.menuItems = [share,edit,delete,update]
            UIMenuController.shared.menuItems?.reverse()
            UIMenuController.shared.update()
        }
        self.collectionView?.allowsSelection = false
        self.collectionView?.allowsMultipleSelection = false
        
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

   
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProfileProduct_CollectionViewCell
        cell.product = items[indexPath.row] as? Product
        cell.delegate = self
        return cell
    }

    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        cell.frameView.alpha = 1
        cell.frameView.viewWithTag(cell.label_tag)?.isHidden = true
        cell.frameView.viewWithTag(cell.image_tag)?.isHidden = false
    }
    
    override func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        cell.frameView.alpha = 0
        cell.frameView.viewWithTag(cell.image_tag)?.isHidden = false
        cell.frameView.viewWithTag(cell.label_tag)?.isHidden = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        return cell.product?.status_id == Constants.STATUS_ACTIVE
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldDeselectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! ProfileProduct_CollectionViewCell
        return cell.product?.status_id == Constants.STATUS_ACTIVE
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
       
        return  !collectionView.allowsMultipleSelection
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
       
        if collectionView.allowsMultipleSelection{
            return false
        }
        
        if action == #selector(copy(_:)) || action == #selector(cut(_:)) || action == #selector(paste(_:)){
            return false
        }
        
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        //required always
    }
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView!.frame.width )/3, height: 210)
    }

}

extension MyProducts_CollectionViewController:ItemsLoaderDelegate,DataModelChanger,ProfileProductDelegate{
    func dataFiller(data: [JSON]) {
        for item in data{
            self.items.append( Product(item))
        }
    }
    
    func change(_ json: JSON) {
        //implemnt
        user = User(json,beginWith: "user")

        dataModel = user!.dataModel

    }
    
    func make_action(_ cell: ProfileProduct_CollectionViewCell, _ action: ProductActions) {
        
        selectedIndexPath = collectionView?.indexPath(for: cell)
        let link = "product/\(cell.product!.id!)/"
        
        switch action {

        case .edit:
            let productController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.add_product.rawValue) as! AddProduct_TableViewController
            
            productController.product = cell.product
            
            self.navigationController?.pushViewController(productController, animated: true)
            break
        case .delete:
            self.apiRequest.make_request(link: link+"delete", method: .post, completion: nil)

            collectionView?.performBatchUpdates({
                self.items.remove(at: self.selectedIndexPath!.row)
                self.collectionView?.deleteItems(at: [self.selectedIndexPath!])
            }, completion: {
                if($0 && self.items.count == 0){
                    self.showEmpty()
                }
            })
            break
        case .hide:
            self.apiRequest.make_request(link: link+"hide", method: .post, completion: nil)

            collectionView?.performBatchUpdates({
                let product = self.items[self.selectedIndexPath!.row] as! Product
                
                if(product.status_id == Constants.STATUS_HIDDEN){
                    product.status_id = Constants.STATUS_ACTIVE
                }else{
                    product.status_id = Constants.STATUS_HIDDEN
                }
                
                self.collectionView?.reloadItems(at: [self.selectedIndexPath!])
            }, completion: nil)
            break
        default:
            print(action)
        }
    }
    
    
}
