package com.washimit.it.ryadah.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.UserInfoPageAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Block;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BlockFragment extends Fragment {

    @BindView(R.id.recycler_block)
    RecyclerView recycler_block;
    @BindView(R.id.no_data)
    TextViewCast no_data;
    UserInfoPageAdapter adapter;
    ArrayList<Object> blocks;
    ApiRequest request;
    Gson gson;
    SharedPref pref;
    String urls;
    LinearLayout social_media, info;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_block, container, false);
        View hed = inflater.inflate(R.layout.item_user_profile_info, null);

        social_media = (LinearLayout) hed.findViewById(R.id.social_media);
        info = (LinearLayout) hed.findViewById(R.id.info);
        social_media.setVisibility(View.GONE);
        info.setVisibility(View.GONE);
        ButterKnife.bind(this, view);
        blocks = new ArrayList<>();
        request = new ApiRequest(getActivity());
        gson = new Gson();
        pref = new SharedPref(getActivity());
        recycler_block.setHasFixedSize(true);
        recycler_block.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new UserInfoPageAdapter(blocks, getActivity(), recycler_block);

        recycler_block.setAdapter(adapter);

        getBlock();
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                blocks.add(null);
                adapter.notifyItemInserted(blocks.size() - 1);
                getrNext(urls);
            }
        });

        adapter.setHeader(hed);
        blocks.add(pref.getUser());
        blocks.add("قائمة الحظر");


        return view;

    }

    private void getBlock() {
        request.MultipartRequest(Request.Method.GET, false, "block", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject object;
                JSONObject msg;
                JSONObject blocked;
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                blocked = msg.getJSONObject("blocked");
                urls = blocked.getString("next_page_url");
                adapter.setUrls(urls);
                blocks.addAll((Collection<? extends Block>) gson.fromJson(blocked.getString("data"), new TypeToken<ArrayList<Block>>() {
                }.getType()));

                adapter.notifyDataSetChanged();

                if (blocks.size() == 2) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }

    private void getrNext(String url) {
        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject object;
                JSONObject msg;
                JSONObject blocked;
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                blocked = msg.getJSONObject("blocked");
                urls = blocked.getString("next_page_url");
                adapter.setUrls(urls);
                blocks.addAll((Collection<? extends Block>) gson.fromJson(blocked.getString("data"), new TypeToken<ArrayList<Block>>() {
                }.getType()));

                adapter.notifyDataSetChanged();

                if (blocks.size() == 2) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }

}
