package com.it.washim.wojooohapp.activitys;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.ImageAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BookShowDetailsActivity extends BaseActivity implements View.OnClickListener {
    ImageAdapter adapter;
    LinearLayoutManager manager;
    @BindView(R.id.recycler_show_image)
    RecyclerView recyclerView;
    @BindView(R.id.show_name)
    MyTextView show_name;
    @BindView(R.id.show_date)
    MyTextView show_date;
    @BindView(R.id.show_room_number)
    MyTextView show_room_number;
    @BindView(R.id.show_description)
    MyTextView show_description;
    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_show_details);
        ButterKnife.bind(this);

        if (getIntent().getExtras() != null) {
            post = getGson().fromJson(getIntent().getExtras().getString("BookShow"), Post.class);

            show_name.setText(post.title);
            show_date.setText("الفترة " + post.post_meta.location.worktime);
            show_room_number.setText(post.post_meta.location.exactly_in);
            show_description.setText(post.content);
        }

        adapter = new ImageAdapter(post.attachments, this);
        manager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);
        show_name.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_name:
                openActivity(this, MapsActivity.class);
                break;
        }
    }
}
