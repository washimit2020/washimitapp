<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaceRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('place_ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('related_id');

            $table->unsignedTinyInteger('view_rating');
            $table->unsignedTinyInteger('products_rating');
            $table->unsignedTinyInteger('users_rating');
            $table->unsignedTinyInteger('prices_rating');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('related_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('place_ratings');
    }
}
