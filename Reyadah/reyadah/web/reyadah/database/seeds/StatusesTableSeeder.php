<?php

use Illuminate\Database\Seeder;
use App\Http\Constants;
class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $date = \Carbon\Carbon::now();
        DB::table('statuses')->insert([
            [
                'id'=>Constants::_STATUS_ACTIVE,
                'title'=>'statuses.active',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_STATUS_INACTIVE,
                'title'=>'statuses.inactive',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_STATUS_HIDDEN,
                'title'=>'statuses.hidden',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_STATUS_BLOCKED,
                'title'=>'statuses.blocked',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_STATUS_DECLINED,
                'title'=>'statuses.declined',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_STATUS_DELETED,
                'title'=>'statuses.deleted',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
        ]);
    }
}
