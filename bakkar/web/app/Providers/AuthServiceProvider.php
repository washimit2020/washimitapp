<?php

namespace App\Providers;

use App\Http\Constants;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        app('translator')->setLocale('ar');

        $this->app['auth']->viaRequest('api', function (Request $request) {

            $token = null;
            if ($request->input('api_token')) {
                $token = $request->input('api_token');
            }elseif($request->header('Authorization') && str_contains($request->header("Authorization"), "Bearer ")){
                $token = str_replace("Bearer ","",$request->header("Authorization"));
            }
            if ($token){
                return User::where('api_token',$token)->first();
            }
            return null;
        });

        $this->app["gate"]->define(Constants::CAN_ADMIN,function ($user){
           return $user->user_role_id == Constants::USER_ROLE_ADMIN;
        });


    }
}
