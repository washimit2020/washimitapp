package com.washimit.it.ryadah.Fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.MyProfileAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.product.ShowSharedProduct;
import com.washimit.it.ryadah.model.product.StoreSharedProduct;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.ion.Ion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MyProfileItemFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {


    @BindView(R.id.linearLayout_no_internt)
    LinearLayout linearLayout_no_internt;
    @BindView(R.id.retry)
    ButtonView retry;
    @BindView(R.id.recycler_fragment_my_profile_item)
    RecyclerView recycler_fragment_my_profile_item;
    @BindView(R.id.swipe_refresh_fragment_my_profile_item)
    SwipeRefreshLayout swipe_refresh_fragment_my_profile_item;
    @BindView(R.id.no_data)
    TextViewCast no_data;


    private int setLayoutManager;
    private int userType;
    MyProfileAdapter adapter;
    ArrayList<Object> objects;
    ApiRequest request;
    Gson gson;
    SharedPref pref;
    User user;
    String urls;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_LAYOUT_MANAGER = "setLayoutManager";
    private static final String ARG_TYPE = "userType";

    public static MyProfileItemFragment newInstance(int setLayoutManager, int PageType) {
        MyProfileItemFragment fragment = new MyProfileItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_MANAGER, setLayoutManager);
        args.putInt(ARG_TYPE, PageType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            setLayoutManager = getArguments().getInt(ARG_LAYOUT_MANAGER);
            userType = getArguments().getInt(ARG_TYPE);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_profile_item, container, false);

        ButterKnife.bind(this, view);
        request = new ApiRequest(getActivity());
        gson = new Gson();
        pref = new SharedPref(getActivity());
        user = pref.getUser();
        objects = new ArrayList<>();

        retry.setOnClickListener(this);
        swipe_refresh_fragment_my_profile_item.setOnRefreshListener(this);
        recycler_fragment_my_profile_item.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 3);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (setLayoutManager == 2) {
                    return 1;
                } else {
                    return 3;
                }
            }
        });


        recycler_fragment_my_profile_item.setLayoutManager(gridLayoutManager);

        adapter = new MyProfileAdapter(objects, getActivity(), recycler_fragment_my_profile_item);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                objects.add(null);
                adapter.notifyItemInserted(objects.size() - 1);
                getNext(userType, urls);
            }
        });


        recycler_fragment_my_profile_item.setAdapter(adapter);


        swipe_refresh_fragment_my_profile_item.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh_fragment_my_profile_item.setRefreshing(true);
                updateUi(userType);
            }
        });

        return view;
    }


    private void updateUi(int type) {

        switch (type) {

            case Constant._TYPE_STORE:
                getUser(type);
                break;

            case Constant._TYPE_SHOW:
                getUser(type);
                break;


            case Constant._TYPE_REQUEST_SHOW_PRODUCT_STORE_DETAILS:
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                getShowRequstProduct();
                break;

            case Constant._TYPE_REQUEST_STORE_PRODUCT_SHOW_DETAILS:
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                getStoreRequstProduct();

                break;
            case Constant._TYPE_FESTIVAL:
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                getFestivalRequst();
                break;

            case Constant._TYPE_MY_PRODUCT_DETAILS:

                getProduct();

                break;


        }


    }


    @Override
    public void onRefresh() {
        updateUi(userType);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Ion.getDefault(getActivity()).cancelAll(getActivity());

    }

    private void getProduct() {

        request.MultipartRequest(Request.Method.GET, false, "user/" + user.getId(), null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();
                adapter.setLoading();

                JSONObject result;
                JSONObject msg;
                JSONObject user;
                JSONObject products;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                user = msg.getJSONObject("user");
                products = user.getJSONObject("products");
                urls = products.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));
                adapter.notifyDataSetChanged();

                if (objects.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);

            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);
                Log.d("errorMessage", errorMessage);
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
            }
        });

    }

    private void getUser(int userType) {
        request.MultipartRequest(Request.Method.GET, false, "following/" + userType, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();
                adapter.setLoading();

                JSONObject result;
                JSONObject msg;
                JSONObject user;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                user = msg.getJSONObject("users");
                urls = user.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                if (objects.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }

            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);

                linearLayout_no_internt.setVisibility(View.VISIBLE);
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);

                Log.d("errorMessage", errorMessage);
            }
        });

    }

    private void getStoreRequstProduct() {
        request.MultipartRequest(Request.Method.GET, false, "product/share", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();
                adapter.setLoading();
                JSONObject result;
                JSONObject msg;
                JSONObject shared;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                shared = msg.getJSONObject("shared");
                urls = shared.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(shared.getString("data"), new TypeToken<ArrayList<StoreSharedProduct>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                if (objects.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }


            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);

                linearLayout_no_internt.setVisibility(View.VISIBLE);
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);

            }
        });
    }

    private void getShowRequstProduct() {
        request.MultipartRequest(Request.Method.GET, false, "product/share", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();

                JSONObject result;
                JSONObject msg;
                JSONObject shared;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                shared = msg.getJSONObject("shared");
                urls = shared.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(shared.getString("data"), new TypeToken<ArrayList<ShowSharedProduct>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                if (objects.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
                adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);

                linearLayout_no_internt.setVisibility(View.VISIBLE);
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);

            }
        });
    }

    private void getFestivalRequst() {
        request.MultipartRequest(Request.Method.GET, false, "festivals/user", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                objects.clear();
                JSONObject result;
                JSONObject msg;
                JSONObject festivals;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                festivals = msg.getJSONObject("festivals");
                urls = festivals.getString("next_page_url");
                adapter.setUrl(urls);
                objects.addAll((Collection<?>) gson.fromJson(festivals.getString("data"), new TypeToken<ArrayList<Festivals>>() {
                }.getType()));

                adapter.notifyDataSetChanged();
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);
                if (objects.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
                adapter.setLoading();


            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);

            }
        });
    }


    public void setSelectProduct(boolean selectProduct) {

        adapter.setSelectProduct(selectProduct);

    }

    public void shaerSelectProduct(String type) {

        adapter.shaerSelect(type);

    }

    private void getNext(final int userType, String url) {

        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                objects.remove(objects.size() - 1);
                adapter.notifyItemRemoved(objects.size() - 1);
                Log.d("result", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject user;
                JSONObject products;
                JSONObject festivals;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                if (userType == Constant._TYPE_STORE || userType == Constant._TYPE_SHOW) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    adapter.setUrl(urls);
                    objects.addAll((Collection<?>) gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType()));
                } else if (userType == Constant._TYPE_MY_PRODUCT_DETAILS) {
                    user = msg.getJSONObject("user");
                    products = user.getJSONObject("products");
                    urls = products.getString("next_page_url");
                    adapter.setUrl(urls);
                    objects.addAll((Collection<?>) gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType()));
                } else if (userType == Constant._TYPE_FESTIVAL) {
                    msg = result.getJSONObject("msg");
                    festivals = msg.getJSONObject("festivals");
                    urls = festivals.getString("next_page_url");
                    adapter.setUrl(urls);
                    objects.addAll((Collection<?>) gson.fromJson(festivals.getString("data"), new TypeToken<ArrayList<Festivals>>() {
                    }.getType()));
                }

                adapter.setLoading();


                adapter.notifyDataSetChanged();
                swipe_refresh_fragment_my_profile_item.setRefreshing(false);


            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("errorMessage", errorMessage);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry:
                swipe_refresh_fragment_my_profile_item.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                updateUi(userType);
                break;
        }
    }
}
