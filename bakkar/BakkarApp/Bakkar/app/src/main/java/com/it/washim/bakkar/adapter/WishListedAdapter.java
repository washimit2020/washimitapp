package com.it.washim.bakkar.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.BookDetailsActivity;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.model.Posts;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 10/22/2017.
 */

public class WishListedAdapter extends RecyclerView.Adapter {
    ArrayList<Posts> list;
    Context context;
    RecyclerView recyclerView;
    boolean loading = false;
    int add = 3;
    int ItemCount;
    int ItemPosition;
    OnLoadMorCollBack onLoadMorCollBack;
    String url;

    public WishListedAdapter(ArrayList<Posts> list, Context context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        this.recyclerView = recyclerView;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (!url.equalsIgnoreCase("null")) {
                        ItemCount = layoutManager.getItemCount();
                        ItemPosition = layoutManager.findLastVisibleItemPosition();
                        if (!loading && ItemCount <= (ItemPosition + add)) {

                            if (onLoadMorCollBack != null) {

                                onLoadMorCollBack.onLoad();

                            }
                            loading = true;

                        }
                    }
                }
            });

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WishListedViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Posts wishList = list.get(position);

        if (holder instanceof WishListedViewHolder) {

            ((WishListedViewHolder) holder).lblListItem.setText(wishList.title);

            ((WishListedViewHolder) holder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                  context.  startActivity(new Intent(context, BookDetailsActivity.class)
                            .putExtra("rated", wishList.rated)
                            .putExtra("book_id", wishList.id)
                            .putExtra("image_id", wishList.attachments.size() > 0 ? wishList.attachments.get(0).id : null)
                            .putExtra("book_description", wishList.description)
                            .putExtra("book_title", wishList.title)
                            .putExtra("is_wishlisted", wishList.is_wishlisted)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishListedViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lblListItem)
        MyTextView lblListItem;

        public WishListedViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }


    public void addOnLoadMorLisenar(OnLoadMorCollBack onLoadMorCollBack) {
        this.onLoadMorCollBack = onLoadMorCollBack;
    }

    public void isLoading(Boolean loading) {
        this.loading = loading;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public static void viewInBrowser(Context context, String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        //if (null != intent.resolveActivity(context.getPackageManager())) {
        context.startActivity(intent);
        // }
    }

    public void watchYoutubeVideo(String id) {

    }

}
