package com.washimit.it.ryadah.Interface;

import org.json.JSONException;

/**
 * Created by mohammad on 4/15/2017.
 */

public interface MultipartCallback {
    void onSuccess(String result) throws JSONException;


    void onRequestError(String errorMessage);

}
