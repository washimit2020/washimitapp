package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 1/10/17.
 */

public class UserRole {
    String id;
    String name_en;
    String name_ar;
    String token;

    public UserRole(String id, String name_en, String name_ar, String token) {
        this.id = id;
        this.name_en = name_en;
        this.name_ar = name_ar;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_en() {
        return name_en;
    }

    public void setName_en(String name_en) {
        this.name_en = name_en;
    }

    public String getName_ar() {
        return name_ar;
    }

    public void setName_ar(String name_ar) {
        this.name_ar = name_ar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
