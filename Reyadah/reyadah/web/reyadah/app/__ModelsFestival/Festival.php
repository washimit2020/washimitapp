<?php

namespace App\__ModelsFestival;

use App\__ModelsRating\FestivalRating;
use App\Comment;
use App\Http\Constants;
use App\SocialMedia;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Festival extends Model
{

    protected $fillable = ["name","organization","source","lat","lng","street_name","sector_name",
        "organizer_name","organizer_phone_number","start_in","from" ,"to","info","social_media_id"];

    protected $hidden = ['social_media_id'];

    protected $with = ["user","social_media"];

    protected $appends = ['comments','followed', 'rated','total_followers'];

    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function social_media(){
        return $this->belongsTo(SocialMedia::class,'social_media_id');
    }

    /**
     * get comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable')->where('status_id',Constants::_STATUS_ACTIVE);
    }


    /**
     * to get all the followers instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followedBy()
    {
        return $this->hasMany('App\ManyToPlacesModels\Follower', 'place_id')->where('type_id',Constants::_TYPE_FOLLOW_FESTIVAL);
    }

    /**
     * to get the ratings of the place
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(FestivalRating::class, 'festival_id');
    }
    /**
     * to get the comments object
     *
     * @return mixed
     */
    public function getCommentsAttribute(){
        return $this->comments()->latest()->get()->take(2);
    }

    /**
     * @return int
     */
    public function getFollowedAttribute()
    {
        $follow = 0;
        if (auth()->guard('api')->check()) {
            $followed = $this->followedBy()->where('user_id', auth()->guard('api')->user()->id)->where('status_id', Constants::_STATUS_ACTIVE)->first();
            if (!empty($followed)) {
                $follow = 1;
            }
        }
        return $follow;
    }

    /**
     * @return null
     */
    public function getRatedAttribute()
    {
        $rated = null;
        if (auth()->guard('api')->check()) {
            $rated = $this->ratings()->where('user_id', auth()->guard('api')->user()->id)->first();
        }
        return $rated;
    }


    /**
     * @return int
     */
    public function getTotalFollowersAttribute()
    {
        return $this->followedBy()->where('status_id',Constants::_STATUS_ACTIVE)->count();
    }
}
