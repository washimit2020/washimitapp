package com.it.washim.wojooohapp.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.PagerAdapter;
import com.it.washim.wojooohapp.adapter.SalePointListAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.id.list;

public class SalePointsListFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @BindView(R.id.sale_point_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.no_data)
    TextView no_data;
    @BindView(R.id.retray)
    MyButtonView retray;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;

    SalePointListAdapter adapter;
    ArrayList<Post> list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sale_points_list_fragment, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        adapter = new SalePointListAdapter(list, getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);
        swipe_refresh.setOnRefreshListener(this);
        retray.setOnClickListener(this);
        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);
                getSalePoints();
            }
        });
        return view;
    }


    public void getSalePoints() {

        recyclerView.setVisibility(View.VISIBLE);
        retray.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);

        getRequest().request(Request.Method.GET, "posts/" + Constant.PAGES_SELLING_PLACES, null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                list.clear();
                list.addAll(response.data);

                swipe_refresh.setRefreshing(false);

                adapter.notifyDataSetChanged();


                if (list.size() > 0) {

                    recyclerView.setVisibility(View.VISIBLE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setText("لا يوجد");
                    recyclerView.setVisibility(View.GONE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipe_refresh.setRefreshing(false);
                no_data.setText("لا يوجد اتصال بالانترنت");
                recyclerView.setVisibility(View.GONE);
                retray.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onRefresh() {
        getSalePoints();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray:
                swipe_refresh.setRefreshing(true);
                getSalePoints();
                break;


        }
    }
}
