package com.washimit.it.ryadah.model.product;

/**
 * Created by mohammad on 4/25/2017.
 */

public class RatedProduct {

    double quality;
    double style;
    double ingredients;
    double pric;

    public RatedProduct(double quality, double style, double ingredients, double pric) {
        this.quality = quality;
        this.style = style;
        this.ingredients = ingredients;
        this.pric = pric;
    }

    public double getQuality() {
        return quality;
    }

    public void setQuality(double quality) {
        this.quality = quality;
    }

    public double getStyle() {
        return style;
    }

    public void setStyle(double style) {
        this.style = style;
    }

    public double getIngredients() {
        return ingredients;
    }

    public void setIngredients(double ingredients) {
        this.ingredients = ingredients;
    }

    public double getPric() {
        return pric;
    }

    public void setPric(double pric) {
        this.pric = pric;
    }
}
