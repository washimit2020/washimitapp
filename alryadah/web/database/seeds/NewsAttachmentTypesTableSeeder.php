<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;

class NewsAttachmentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_attachment_types')->insert(
            [
                [
                    "id" => Constants::NEWS_ATTACHMENT_MAIN,
                    "name_en" => "Main",
                    "name_ar" => "رئيسي"
                ],
                [
                "id" => Constants::NEWS_ATTACHMENT_SUB,
                "name_en" => "Sub",
                "name_ar" => "فرعي"
                ]
            ]
        );
    }
}
