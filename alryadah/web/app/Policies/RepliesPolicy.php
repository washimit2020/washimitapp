<?php

namespace App\Policies;

use App\Http\Helpers\Constants;
use App\User;
use App\Reply;
use Illuminate\Auth\Access\HandlesAuthorization;

class RepliesPolicy
{
    use HandlesAuthorization;


    public function delete(User $user, Reply $reply)
    {
        return ($user->id == $reply->user->id || $user->can(Constants::ADMINISTRATOR));
    }
}
