package com.it.washim.wojooohapp.activitys;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.fragment.AboutAppFragment;
import com.it.washim.wojooohapp.fragment.BookFragment;
import com.it.washim.wojooohapp.fragment.DeveloperFragment;
import com.it.washim.wojooohapp.fragment.ExpandableFragment;
import com.it.washim.wojooohapp.fragment.RequestBookFragment;
import com.it.washim.wojooohapp.fragment.RequestToPublishBookFragment;
import com.it.washim.wojooohapp.fragment.SalePointsFragment;
import com.it.washim.wojooohapp.fragment.SearchFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.slide_menu)
    ImageView slideMenu;
    @BindView(R.id.page_name)
    MyTextView pageName;

    @BindView(R.id.main)
    LinearLayout main;

    @BindView(R.id.search)
    LinearLayout search;

    @BindView(R.id.book_page)
    LinearLayout book_page;

    @BindView(R.id.sale)
    LinearLayout sale;

    @BindView(R.id.book_show_page)
    LinearLayout book_show_page;

    @BindView(R.id.distribution_agents_page)
    LinearLayout distribution_agents_page;

    @BindView(R.id.request_publish_book)
    LinearLayout request_publish_book;


    @BindView(R.id.request_book)
    LinearLayout request_book;


    @BindView(R.id.serves)
    LinearLayout serves;

    @BindView(R.id.about_app)
    LinearLayout about_app;

    @BindView(R.id.about_developer)
    LinearLayout about_developer;

    private Bundle bundle;
    private Bundle bundleFragment;
    SalePointsFragment salePointsFragment = new SalePointsFragment();
    BookFragment bookFragment = new BookFragment();
    SearchFragment searchFragment = new SearchFragment();
    RequestBookFragment requestBookFragment = new RequestBookFragment();
    RequestToPublishBookFragment requestToPublishBookFragment = new RequestToPublishBookFragment();
    ExpandableFragment expandableFragment = new ExpandableFragment();


    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        bundleFragment = new Bundle();
        bundle = getIntent().getExtras();

        slideMenu.setOnClickListener(this);
        back.setOnClickListener(this);
        main.setOnClickListener(this);
        search.setOnClickListener(this);
        book_page.setOnClickListener(this);
        sale.setOnClickListener(this);
        book_show_page.setOnClickListener(this);
        distribution_agents_page.setOnClickListener(this);
        request_publish_book.setOnClickListener(this);
        request_book.setOnClickListener(this);
        serves.setOnClickListener(this);
        about_app.setOnClickListener(this);
        about_developer.setOnClickListener(this);


        if (bundle != null) {

            selectFragment(bundle.getInt("page_type"));

        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }


    private void openFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.content_view, fragment).commit();

        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        }
    }

    public void selectFragment(int type) {
        switch (type) {

            case Constant.PAGES_SELLING_PLACES:
                openFragment(salePointsFragment);
                pageName.setText("منافذ التوزيع");
                break;
            case Constant.PAGES_BOOK:
                openFragment(bookFragment);
                pageName.setText("قائمة الكتب");

                break;

            case Constant.PAGES_BUY_REQUEST:
                openFragment(requestBookFragment);
                pageName.setText("طلب شراء");
                break;

            case Constant.PAGES_PUBLISH_REQUEST:
                openFragment(requestToPublishBookFragment);
                pageName.setText("طلب نشر");
                break;
            case Constant.PAGES_SERVICES:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SERVICES);
                pageName.setText("الخدمات الفنية");
                expandableFragment.setArguments(bundleFragment);

                openFragment(expandableFragment);

                break;

            case Constant.PAGES_BOOKS_SHOWROOM:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_BOOKS_SHOWROOM);
                pageName.setText("معارض الكتب");
                expandableFragment.setArguments(bundleFragment);

                openFragment(expandableFragment);
                break;
            case Constant.PAGES_SPONSOR:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SPONSOR);
                pageName.setText("وكلاء التوزيع");
                expandableFragment.setArguments(bundleFragment);

                openFragment(expandableFragment);
                break;

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.slide_menu:
                if (!drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.openDrawer(GravityCompat.END);
                }
                break;

            case R.id.main:
                if (drawer.isDrawerOpen(GravityCompat.END)) {
                    drawer.closeDrawer(GravityCompat.END);
                }
                finish();

                break;
            case R.id.search:
                openFragment(new SearchFragment());
                pageName.setText("البحث");

                break;
            case R.id.book_page:
                openFragment(new BookFragment());
                pageName.setText("قائمة الكتب");

                break;
            case R.id.sale:
                openFragment(new SalePointsFragment());
                pageName.setText("منافذ التوزيع");

                break;
            case R.id.book_show_page:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_BOOKS_SHOWROOM);
                pageName.setText("معارض الكتب");

                ExpandableFragment fragment = new ExpandableFragment();
                fragment.setArguments(bundleFragment);
                openFragment(fragment);

                break;
            case R.id.distribution_agents_page:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SPONSOR);
                pageName.setText("وكلاء التوزيع");

                ExpandableFragment fragment2 = new ExpandableFragment();
                fragment2.setArguments(bundleFragment);
                openFragment(fragment2);
                break;
            case R.id.request_publish_book:
                openFragment(new RequestToPublishBookFragment());
                pageName.setText("طلب نشر");

                break;
            case R.id.request_book:
                openFragment(new RequestBookFragment());
                pageName.setText("طلب شراء");

                break;
            case R.id.serves:
                bundleFragment.clear();
                bundleFragment.putInt("fragment_type", Constant.PAGES_SERVICES);
                pageName.setText("الخدمات الفنية");

                ExpandableFragment fragment3 = new ExpandableFragment();
                fragment3.setArguments(bundleFragment);
                openFragment(fragment3);
                break;
            case R.id.about_app:

                openFragment(new AboutAppFragment());
                pageName.setText("حول التطبيق");
                break;
            case R.id.about_developer:
                openFragment(new DeveloperFragment());
                pageName.setText("حول المطور");
                break;

        }
    }

}
