package com.washimit.it.ryadah.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.ExplorerAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExplorerFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    @BindView(R.id.recycler_explorer)
    RecyclerView recycler_explorer;

    @BindView(R.id.refresh_explorer)
    SwipeRefreshLayout refresh_explorer;

    @BindView(R.id.linearLayout_no_internt)
    LinearLayout linearLayout_no_internt;
    @BindView(R.id.retry)
    ButtonView retry;
    @BindView(R.id.no_data)
    TextViewCast no_data;

    ExplorerAdapter adapter;
    ArrayList<Product> products;
    ApiRequest request;
    Gson gson;
    String urls;


    public ExplorerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_explorer, container, false);
        ButterKnife.bind(this, view);
        products = new ArrayList<>();
        request = new ApiRequest(getActivity());
        gson = new Gson();
        recycler_explorer.setHasFixedSize(true);
        recycler_explorer.setLayoutManager(new GridLayoutManager(getActivity(), 3));

        adapter = new ExplorerAdapter(products, getActivity(), recycler_explorer);
        refresh_explorer.setOnRefreshListener(this);


        recycler_explorer.setAdapter(adapter);

        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                products.add(null);
                adapter.notifyItemInserted(products.size() - 1);
                getNext(urls);
            }
        });

        refresh_explorer.post(new Runnable() {
            @Override
            public void run() {
                refresh_explorer.setRefreshing(true);
                getExplorer();


            }
        });

        retry.setOnClickListener(this);
        return view;

    }

    private void getExplorer() {


        request.MultipartRequest(Request.Method.GET, false, "product/explorer", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                products.clear();
                adapter.setLoading();
                Log.d("result", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject product;


                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                product = msg.getJSONObject("products");
                urls = product.getString("next_page_url");
                Log.d("url", urls);
                adapter.setUrl(urls);
                products.addAll((Collection<? extends Product>) gson.fromJson(product.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));

                if (products.size() > 0) {
                    no_data.setVisibility(View.GONE);

                } else {
                    no_data.setVisibility(View.VISIBLE);

                }
                adapter.notifyDataSetChanged();

                refresh_explorer.setRefreshing(false);


            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                Log.d("festivals", errorMessage);
                refresh_explorer.setRefreshing(false);
            }
        });


    }


    private void getNext(String url) {


        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                products.remove(products.size() - 1);
                adapter.notifyItemRemoved(products.size() - 1);
                Log.d("result_next", result_);
                JSONObject result;
                JSONObject msg;
                JSONObject product;


                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                product = msg.getJSONObject("products");
                urls = product.getString("next_page_url");
                adapter.setUrl(urls);
                products.addAll((Collection<? extends Product>) gson.fromJson(product.getString("data"), new TypeToken<ArrayList<Product>>() {
                }.getType()));


                adapter.notifyDataSetChanged();

                refresh_explorer.setRefreshing(false);
                adapter.setLoading();


            }

            @Override
            public void onRequestError(String errorMessage) {
                refresh_explorer.setRefreshing(false);
                adapter.setLoading();


            }
        });


    }


    @Override
    public void onRefresh() {
        getExplorer();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.retry:
                refresh_explorer.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                recycler_explorer.setVisibility(View.VISIBLE);
                getExplorer();
                break;

        }
    }
}
