//
//  PageVM.swift
//  al-haj
//
//  Created by WASHM on 1/17/18.
//  Copyright © 2018 WASHM. All rights reserved.
//
import UIKit
import SQLite

class PageVm: NSObject {
    var buttons:UIStackView!
    var page:Page
   
    var body:C_UITextView
    var colors:[UIColor] = [.red , .yellow , .cyan , .white]
    var attributedBody:NSMutableAttributedString?{
        didSet{
            body.attributedText = attributedBody
            body.textAlignment = .right
        }
    }
    init(page:Page, body: C_UITextView) {
        self.page = page
        self.body = body
        super.init()
        readPage()
        check_if_highlighted()
        self.body.delegate = self
    }
}

//UI
extension PageVm{
    func setup_buttons(view:UIView){
        let nightMode = Utils.shared.is_dark_mode()
        if let buttons = buttons{
            buttons.removeFromSuperview()
        }
        buttons = UIStackView(frame: .init(x: 40, y: 70, width: 40, height:  48 * colors.count))
        buttons.tag = 99
        buttons.spacing = 8
        buttons.axis = .vertical
        buttons.distribution = .fillEqually
        for (key,color) in colors.enumerated(){
            let button = UIButton()
            button.frame.size.width = buttons.frame.width
            button.frame.size.height = 40
            button.layer.cornerRadius = 20
            if nightMode{
                button.layer.shadowColor = UIColor.lightGray.cgColor;
            }else{
                button.layer.shadowColor = UIColor.black.cgColor;
            }
            button.layer.shadowOpacity = 0.8;
            button.layer.shadowRadius = 4;
            button.layer.shadowOffset = CGSize(width: 3, height: 3);
            button.addTarget(self, action: #selector(highlight(_:)), for: .touchUpInside)
            button.backgroundColor = color
            button.tag = key
            buttons.addArrangedSubview(button)
        }
        buttons.sizeToFit()
        buttons.isHidden = true
        view.addSubview(buttons)
    }
}
//db
extension PageVm {
    private func readPage(){
        Utils.shared.save_page(page: page)
        if page.is_read == 1 {
            return
        }
        page.is_read = 1
        let index = Utils.shared.pages.index { (item) -> Bool in
            return item.id == page.id
        }
        Utils.shared.pages[index!] = page
        DispatchQueue.global(qos: .background).async {
            do{
                let pages_t = DatabaseUtils.shared.pages_t.filter(Expressions.id == self.page.id!)
                try DatabaseUtils.shared.db!.run(pages_t.update(
                    Expressions.is_read <- 1
                ))
            }catch{
                print(error)
            }
        }
    }
    func check_if_highlighted(){
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let table = DatabaseUtils.shared.highlights_t.filter(
                    Expressions.page_id == self.page.id!
                )
                let attributed = self.page.body!
                let highlighted = try Array( DatabaseUtils.shared.db!.prepare(table))
                if highlighted.count > 0 {
                    print(highlighted)
                    for highlight in highlighted{
                        let color = self.colors[highlight[Expressions.color]]
                        let range = NSRange(location: highlight[Expressions.start], length: highlight[Expressions.end])
                        attributed.addAttribute(NSAttributedStringKey.backgroundColor, value: color, range: range)
                        if  Utils.shared.is_dark_mode(){
                            if  color != .black {
                                attributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range)
                            }else{
                                attributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: range)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.attributedBody = attributed
                }
            }catch(let exp){
                print("highlighted select exp: \(exp)")
            }
        }
    }
    
    @objc public func highlight(_ sender: UIButton) {
        guard let attributed = attributedBody , body.selectedRange.length > 0 else {
            return
        }
        let range = body.selectedRange
       
        attributed.addAttribute(NSAttributedStringKey.backgroundColor, value: sender.backgroundColor!, range: body.selectedRange)
        if  Utils.shared.is_dark_mode() {
            if  sender.backgroundColor != .black {
                self.attributedBody!.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: body.selectedRange)
            }else{
                self.attributedBody!.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: body.selectedRange)
            }
        }
        attributedBody = attributed
        
        do {
            let highlighted = DatabaseUtils.shared.highlights_t.filter(
                Expressions.page_id == page.id! &&
                    Expressions.start >= body.selectedRange.location &&
                    Expressions.end <= body.selectedRange.length
            )
            try DatabaseUtils.shared.db!.run(highlighted.delete())
            print(range.length)
            print(range.location)
            if (!Utils.shared.is_dark_mode() && sender.backgroundColor != .white)  || (Utils.shared.is_dark_mode() && sender.backgroundColor != .black) {
                try DatabaseUtils.shared.db!.run(DatabaseUtils.shared.highlights_t.insert(
                    Expressions.page_id <- page.id!,
                    Expressions.start <- range.location,
                    Expressions.end <- range.length,
                    Expressions.color <- sender.tag
                ))
            }
        }catch(let exp){
            print("highlighted exp: \(exp)")
        }
    }
  
   
}
extension PageVm: UITextViewDelegate{
    func textViewDidChangeSelection(_ textView: UITextView) {
        guard let buttons = buttons else {
            return
        }
        let length = textView.selectedRange.length
        if length <= 0 {
            buttons.isHidden = true
            return
        }
        self.buttons.isHidden = false
    }
    
}

