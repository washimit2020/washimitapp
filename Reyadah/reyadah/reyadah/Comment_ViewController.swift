//
//  Comment_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SlackTextViewController
import Alamofire
import SwiftyJSON

class Comment_ViewController: SLKTextViewController {
    
    fileprivate let reuseID = "comment"
    
    var dataModel:DataModel?
    
    var link = ""
    
    var beginWithModel = ""
    
    var comments = [Comment]()
    var params = [String:String]()
    var model:Commentable?{
        
        didSet{
            if let product = model as? Product{
                link = RequestLinks.get_product_comments.rawValue + "\(product.id!))"
                params = ["product_id":"\(product.id!)","type":Constants.PRODUCT_COMMENT]
                
            }else if let festival = model as? Festival{
                link = RequestLinks.get_festival_comments.rawValue + "\(festival.id!))"
                params = ["festival_id":"\(festival.id!)","type":Constants.FESTIVAL_COMMENT]
                
            }
        }
    }
    
    var apiRequest:ApiRequest?
    
    var delegate:productRefresher?
    
    let activiy = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    var updated = false
    
    var emptyLabel:UILabel?
    
    var selectedIndexPath:IndexPath?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

        self.tableView?.register(UINib(nibName: "Comment_TableViewCell", bundle: nil), forCellReuseIdentifier: reuseID)
        self.tableView?.allowsSelection = false
        
        self.tableView?.estimatedRowHeight = 130
        
        
        let width =  tableView!.frame.width
        let height:CGFloat =  40
        
        let view = UILabel(frame: CGRect(x: 0 , y: tableView!.frame.height/2 - height, width: width, height: height))
        view.text = "لا يوجد نتائج"
        view.textAlignment = .center
        view.textColor = UIColor.gray
        view.isHidden = true
        emptyLabel = view
        
        tableView?.addSubview(emptyLabel!)
        
        
        
        beginWithModel = "comments"
        
        self.tableView?.refreshControl = UIRefreshControl()
        self.tableView?.refreshControl?.addTarget(self, action: #selector(clearData(_:)), for: .valueChanged)
        self.tableView?.separatorStyle = .none
        
        activiy.startAnimating()
        activiy.hidesWhenStopped = true
        self.textView.placeholder = "اضف تعليقا"
        self.textView.font = UIFont(name: get_font_name()+"-Plain", size: 19)
        
        self.textInputbar.backgroundColor = UIColor.white
        self.textInputbar.isTranslucent = false
        self.rightButton.setTitle("تعليق", for: .normal)
        self.rightButton.titleLabel?.font = UIFont(name: get_font_name()+"-Plain", size: 14)
        self.textInputbar.editorRightButton.setTitle("حفظ", for: .normal)
        self.textInputbar.editorRightButton.titleLabel?.font = UIFont(name: get_font_name()+"-Plain", size: 14)
        self.textInputbar.editorLeftButton.setTitle("الغاء", for: .normal)
        self.textInputbar.editorLeftButton.titleLabel?.font = UIFont(name: get_font_name()+"-Plain", size: 14)
        clearData()
        
        // Do any additional setup after loading the view.
    }
    
    override class func tableViewStyle(for decoder: NSCoder) -> UITableViewStyle {
        return .plain
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseID) as! Comment_TableViewCell
        cell.transform = (self.tableView?.transform)!
        cell.comment = comments[indexPath.row]
        if cell.comment?.user?.id == UserDefaultsHelper.get_user_data()?.id{
            cell.accessoryType = .disclosureIndicator
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view  =  UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 30)))
        activiy.frame.origin = view.center
        
        view.addSubview(activiy)
        return view
    }
    
        override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
            return comments[indexPath.row].user!.id == UserDefaultsHelper.get_user_data()?.id
        }
    
        override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
            let remove = UITableViewRowAction(style: .destructive, title: "حذف", handler: {
                (rowAction: UITableViewRowAction, indexPath: IndexPath) -> Void in
                self.apiRequest?.make_request( link: RequestLinks.comment.rawValue + "/\(self.comments[indexPath.row].id!)/delete", method: .post, completion: nil)
                self.comments.remove(at: indexPath.row)
                
                if(self.comments.count == 0){
                    self.emptyLabel?.isHidden = false
                }
                
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .automatic)
                tableView.endUpdates()
                
                self.add_last_comments()
           
            })
            
            let edit = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: "تعديل", handler: {
                (rowAction: UITableViewRowAction, indexPath: IndexPath) -> Void in
                self.selectedIndexPath = indexPath
                let comment = self.comments[indexPath.row]
                self.editText(comment.body)
            })
    
            return [remove,edit]
        }
    
    

    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        guard let data = dataModel else {
            return
        }
        if indexPath.row != comments.count - 2
            ||
            data.next_page_url == nil{
            return
        }
        
        make_api_call(link + "?page=\(dataModel!.current_page! + 1)")
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.dismissKeyboard(true)
        
        delegate?.refreshProduct(self.model!,updated)
    }
    
}


extension Comment_ViewController{
    
    func clearData(_ sender:Any? = nil)
    {
        make_api_call(nil)
    }
    
    func make_api_call(_ nextlink:String?){
        activiy.startAnimating()
        
        var current = link
        
        if let link = nextlink{
            current = link
        }
        
        
        apiRequest!.make_request( link: current, method: .get,  completion: data_handler)
        
    }
    
    private func hideLoaders(){
        self.tableView?.refreshControl?.endRefreshing()
        self.emptyLabel?.isHidden = true
        self.activiy.stopAnimating()
    }
    
    func data_handler(_ response:DataRequest?)->Void{
        guard let response = response else {
            self.present(self.apiRequest!.costumeAlert, animated: true, completion: {
                
                self.apiRequest?.handle_error_dialog(nil)
            })
            
            hideLoaders()
            
            return
        }
        
        response.responseJSON(completionHandler: {
            response in
            
            if response.response?.statusCode == 200{
                guard let json = response.data else{
                    return
                }
                self.dataModel = DataModel(JSON(json), beginWith: self.beginWithModel )
                
                
                guard let data = self.dataModel!.data else{
                    
                    return
                }
                
                if self.dataModel!.total == 0 {
                    self.hideLoaders()
                    self.emptyLabel?.isHidden = false

                    return
                }
                if self.dataModel?.current_page == 1{
                    self.comments.removeAll()
                }
                
                
                for datum in data{
                    self.comments.append(Comment(datum))
                }
                
                self.tableView?.reloadData()
                self.hideLoaders()
                
            }
            
        })
    }
    
    func add_last_comments(){
        var comments = [Comment]()
        let count = self.comments.count
        
       
        if self.comments.indices.contains(count - 1){
            comments.append(self.comments[count - 1])
        }
        
        if self.comments.indices.contains(count - 2){
            comments.append(self.comments[count - 2])
        }
        
       
        model?.comments = comments
        updated = true
    }
    
    override func didPressRightButton(_ sender: Any?) {
        let indexPath = IndexPath(row: 0, section: 0)
        let rowAnimation: UITableViewRowAnimation = self.isInverted ? .bottom : .top
        let scrollPosition: UITableViewScrollPosition = self.isInverted ? .bottom : .top
        let body = textView.text!
        params["body"] = body
        apiRequest?.make_request(parameters: params, apiImages: nil, link: RequestLinks.comment.rawValue, method: .post, dialogMsg: nil, completion: {
            
            $0?.responseJSON(completionHandler: {
                let json = JSON($0.data)
                print(json)
                let comment = Comment(json, beginWith: "comment")
                comment.user = UserDefaultsHelper.get_user_data()
                
                self.tableView?.beginUpdates()
                self.comments.insert(comment, at: 0)
                self.tableView?.insertRows(at: [indexPath], with: rowAnimation)
                self.tableView?.endUpdates()
                self.tableView?.scrollToRow(at: indexPath, at: scrollPosition, animated: true)
                self.tableView?.reloadRows(at: [indexPath], with: .automatic)
                self.add_last_comments()
            })
        })
        
        super.didPressRightButton(sender)
        
        
    }
    
    
    
    override func didCommitTextEditing(_ sender: Any?) {
        
        let message:String = self.textView.text
        let comment = comments[selectedIndexPath!.row]
        params["body"] = message
        apiRequest?.make_request(parameters: params, apiImages: nil, link: RequestLinks.comment.rawValue + "/\(comment.id!)/update", method: .post, completion: nil)
        
        comment.body = message
        self.tableView?.reloadData()
        self.add_last_comments()
        
        super.didCommitTextEditing(sender)
    }
    
    override func didCancelTextEditing(_ sender: Any) {
        tableView?.reloadRows(at: [selectedIndexPath!], with: .none)
        super.didCancelTextEditing(sender)
    }
}


protocol productRefresher {
    func refreshProduct(_ commentable:Commentable ,_ updated:Bool);
}

