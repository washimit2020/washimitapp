//
//  InfoCell_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class InfoCell_TableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var totalFollowers: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var blockUser: UIButton!
    
    
    var user:User?{
        didSet{
            updateView()
        }
    }
    func updateView() {
        guard let userData  = user else{
            return
        }
        
        avatar.set_avatar_image(user_id: userData.id)
       
        icon.setUserIcon(user: user!)
        totalFollowers.text = " \(userData.totalFollowers!) متابع"
        name.text = userData.name
        location.text = make_location_text(userData)
        info.text = userData.info
        
    }
    
    @IBAction func showSocialDialog(_ sender: AspectFitButton) {
        let alert = UIAlertController(title: sender.restorationIdentifier, message: self.user!.socialMedia[sender.restorationIdentifier!],  preferredStyle: .alert)
        if alert.message!.isEmpty{
            alert.message = "لم يتم تعبئة المعلومات"
        }
        alert.addAction(.init(title: "تم", style: .default, handler: nil))
        
        window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
}
