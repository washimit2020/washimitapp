package com.it.washim.bakkar.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.model.Posts;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 8/22/2017.
 */

public class AgentsAdapter extends RecyclerView.Adapter {

    ArrayList<Posts> list;
    Context context;

    public AgentsAdapter(ArrayList<Posts> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AgentsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_agents, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof  AgentsViewHolder){
            Posts posts=list.get(position);

            ((AgentsViewHolder)holder).title.setText(posts.title);
            ((AgentsViewHolder)holder).date.setText(posts.started_at);
            ((AgentsViewHolder)holder).des.setText(posts.description);

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class AgentsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.des)MyTextView des;
        @BindView(R.id.title)MyTextView title;
        @BindView(R.id.date)MyTextView date;

        public AgentsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
