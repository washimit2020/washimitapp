//
//  ImageCell_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/20/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ImageCell_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
}
