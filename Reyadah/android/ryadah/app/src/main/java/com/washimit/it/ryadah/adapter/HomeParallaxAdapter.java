package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.washimit.it.ryadah.Fragment.AboutDevloperFragment;
import com.washimit.it.ryadah.Fragment.BlockFragment;
import com.washimit.it.ryadah.Fragment.HalperFragment;
import com.washimit.it.ryadah.Fragment.SettingsFragment;
import com.washimit.it.ryadah.Fragment.SharchFragment;
import com.washimit.it.ryadah.Fragment.WishlistedFragment;
import com.washimit.it.ryadah.Fragment.profile.MyProfileFragment;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.CommentActivity;
import com.washimit.it.ryadah.activity.MapAndlListLocationActivity;
import com.washimit.it.ryadah.activity.MyProfileInfoActivity;
import com.washimit.it.ryadah.activity.PlaceDetailsActivity;
import com.washimit.it.ryadah.activity.ProductDetailsActivity;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Location;
import com.washimit.it.ryadah.model.NavigationBarItem;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.registrationActivity.SelectTheUserActivity;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.SliderViewItem;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeParallaxAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private float mScrollMultiplier = 0.5f;
    private final int VIEW_PROG = 0;
    public final int PRODUCT = 1;
    public final int HEADER = 2;
    public final int PLACE = 3;
    public final int ADVERTISING = 5;
    public final int NAVIGATION = 6;


    private int usersType;
    List<?> list;
    Activity context;
    ApiRequest request;
    private CustomRelativeWrapper mHeader;
    private RecyclerView mRecyclerView;
    private boolean mShouldClipView = true;
    private String place;
    Gson gson;
    SharedPref pref;
    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading;
    OnLoadMoreListener loadMoreListener;
    Intent intent;

    public HomeParallaxAdapter(List<?> list, Activity context, int usersType, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        this.usersType = usersType;
        gson = new Gson();
        request = new ApiRequest(context);
        place = AapHalper.userNameType(usersType, context);
        pref = new SharedPref(context);


        if (recyclerView.getLayoutManager() instanceof GridLayoutManager) {
            Log.d("ok", "ok");
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = gridLayoutManager.getItemCount();
                        lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }

    public HomeParallaxAdapter(List<?> list, Activity context) {
        this.list = list;
        this.context = context;
        request = new ApiRequest(context);
        gson = new Gson();
        pref = new SharedPref(context);

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {


        if (viewHolder instanceof PlaceHeaderHolder) {
            ((PlaceHeaderHolder) viewHolder).mDemoSlider.removeAllSliders();

            final User user = (User) list.get(i);

            ((PlaceHeaderHolder) viewHolder).item_place_follow.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(user.getFollowed())));

            ((PlaceHeaderHolder) viewHolder).item_place_rating.setRating((float) user.getRating());


            for (int img = 0; img < 3; img++) {
                SliderViewItem textSliderView = new SliderViewItem(context);
                textSliderView.setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                    @Override
                    public void onSliderClick(BaseSliderView slider) {
                        if (pref.getUser() != null) {
                            openActivity(PlaceDetailsActivity.class, usersType, gson.toJson(user));

                        } else {

                            AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                                @Override
                                public void dialog(final Dialog dialog) {
                                    ButtonView signUp, close;
                                    signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                    close = (ButtonView) dialog.findViewById(R.id.close);

                                    signUp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            context.finish();
                                        }
                                    });
                                    close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.cancel();
                                        }
                                    });
                                }
                            });
                        }

                    }
                });

                textSliderView.image(Constant._COVER_URL + user.getId() + "/" + img).setScaleType(BaseSliderView.ScaleType.CenterCrop);

                ((PlaceHeaderHolder) viewHolder).mDemoSlider.addSlider(textSliderView);
            }


            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.getPagerIndicator().setIndicatorStyleResource(R.drawable.ic_selected_indicator, R.drawable.ic_unselected_indicator);

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setCustomAnimation(new DescriptionAnimation());

            ((PlaceHeaderHolder) viewHolder).mDemoSlider.setDuration(4000);

            ((PlaceHeaderHolder) viewHolder).item_place_name.setText(place + " " + user.getName());


        } else if (viewHolder instanceof ProductHolder) {
            final Product product = (Product) list.get(i);

            if (product.getShared_with() != null) {

                ((ProductHolder) viewHolder).item_product_user_name.setText(product.getShared_with().getName());
                ((ProductHolder) viewHolder).item_product_sub_user_name.setText(product.getUser().getName());

                ((ProductHolder) viewHolder).item_proditem_product_user_type_img.setImageDrawable(context.getResources().
                        getDrawable(AapHalper.userIconType(product.getShared_with().getType().getId())));

                Picasso.with(context).load(Constant._AVATAR_URL + product.getShared_with().getId()).placeholder(R.drawable.ic_profile_picture).fit().centerCrop().into(((ProductHolder) viewHolder).item_product_user_avatar);

            } else {


                ((ProductHolder) viewHolder).item_product_user_name.setText(product.getUser().getName());
                ((ProductHolder) viewHolder).item_proditem_product_user_type_img.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(product.getUser().getType().getId())));

                Picasso.with(context).load(Constant._AVATAR_URL + product.getUser().getId()).placeholder(R.drawable.ic_profile_picture).fit().centerCrop().into(((ProductHolder) viewHolder).item_product_user_avatar);


            }


            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).fit().centerCrop().into(((ProductHolder) viewHolder).item_product_img);

            ((ProductHolder) viewHolder).item_product_number_of_eaters.setText(product.getNumber_of_eaters() + "");
            ((ProductHolder) viewHolder).item_product_number_of_like.setText(product.getTotal_likes() + "");
            ((ProductHolder) viewHolder).item_product_number_of_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(product.getIs_liked())), null, null);
            ((ProductHolder) viewHolder).item_product_add_to_favorite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(product.getIs_wishlisted())));

            if (product.getComments().size() > 0) {
                ((ProductHolder) viewHolder).item_product_comment_name_2.setText(product.getComments().get(0).getUser().getName() + " ");
                ((ProductHolder) viewHolder).item_product_comment_2.setText(product.getComments().get(0).getBody());
            } else {
                ((ProductHolder) viewHolder).comments.setVisibility(View.GONE);
            }
            if (product.getComments().size() > 1) {

                ((ProductHolder) viewHolder).item_product_comment_name_1.setText(product.getComments().get(1).getUser().getName() + " ");
                ((ProductHolder) viewHolder).item_product_comment_1.setText(product.getComments().get(1).getBody());
            }


            ((ProductHolder) viewHolder).item_product_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(context, MapAndlListLocationActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("id", product.getId());
                    intent.putExtra("user_type_id", product.getUser().getType().getId());
                    intent.putExtra("location", gson.toJson(new Location(new LatLng(product.getUser().getLat(), product.getUser().getLng()), product.getUser().getType().getId()), Location.class));
                    intent.putExtra("user", gson.toJson(product.getUser()));
                    context.startActivity(intent);
                }
            });
            ((ProductHolder) viewHolder).item_product_add_comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(context, CommentActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("productId", product.getId());
                    intent.putExtra("type", Constant._TYPE_PRODUCT_COMMENT);
                    context.startActivity(intent);
                }
            });


        } else if (viewHolder instanceof PlaceHolder) {

            User user = (User) list.get(i);
            ((PlaceHolder) viewHolder).item_place_foul.setImageDrawable(context.getResources().getDrawable(AapHalper.isFollow(user.getFollowed())));
            ((PlaceHolder) viewHolder).item_place_name.setText(place + " " + user.getName());
            ((PlaceHolder) viewHolder).item_place_rating.setRating((float) user.getRating());
            ((PlaceHolder) viewHolder).item_place_adress.setText(user.getSector_name() + "-" + user.getStreet_name());
            Picasso.with(context)
                    .load(Constant._AVATAR_URL + user.getId())
                    .fit()
                    .centerCrop()
                    .into(((PlaceHolder) viewHolder).item_place_img);

        } else if (viewHolder instanceof NavigationHolder) {
            NavigationBarItem barItem = (NavigationBarItem) list.get(i);
            ((NavigationHolder) viewHolder).item_navigation_img.setImageDrawable(context.getResources().getDrawable(barItem.getImg()));
            ((NavigationHolder) viewHolder).item_navigation_name.setText(barItem.getName());

        } else if (viewHolder instanceof AdvertisingHolder) {
            Product product = (Product) list.get(i);

            Picasso.with(context).load(Constant._AVATAR_URL + product.getUser().getId()).placeholder(R.drawable.ic_profile_picture).fit().centerCrop().skipMemoryCache().into(((AdvertisingHolder) viewHolder).user_img);

            Picasso.with(context).load(Constant._PRODUCT_URL + product.getId() + "/" + 0).placeholder(R.drawable.ic_store).fit().centerCrop().skipMemoryCache().into(((AdvertisingHolder) viewHolder).img);
            ((AdvertisingHolder) viewHolder).user_name.setText(AapHalper.userNameType(product.getUser().getType().getId(), context) + " " + product.getUser().getName());

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == HEADER && mHeader != null) {
            return new PlaceHeaderHolder(mHeader);
        } else if (i == PRODUCT) {
            return new ProductHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_product, viewGroup, false));
        } else if (i == PLACE) {
            return new PlaceHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_place, viewGroup, false));

        } else if (i == NAVIGATION) {
            return new NavigationHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_navigation_bar, viewGroup, false));

        } else if (i == VIEW_PROG) {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));

        } else {
            return new AdvertisingHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_advertising, viewGroup, false));

        }
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && hasHeader()) {
            return HEADER;
        } else if (list.get(position) == null) {
            return VIEW_PROG;
        } else if (list.get(position) instanceof Product) {
            if (((Product) list.get(position)).getIs_sponsored() != 0) {
                return ADVERTISING;

            } else {
                return PRODUCT;

            }
        } else if ((list.get(position) instanceof User)) {
            return PLACE;
        } else if ((list.get(position) instanceof NavigationBarItem)) {
            return NAVIGATION;
        }

        return ADVERTISING;

    }


    private class PlaceHeaderHolder extends RecyclerView.ViewHolder {
        private SliderLayout mDemoSlider;
        ImageView item_place_follow, place_img_type;
        TextView item_place_name;
        RatingBar item_place_rating;

        public PlaceHeaderHolder(View itemView) {
            super(itemView);
            mDemoSlider = (SliderLayout) itemView.findViewById(R.id.slider);
            place_img_type = (ImageView) itemView.findViewById(R.id.place_img_type);
            item_place_follow = (ImageView) itemView.findViewById(R.id.item_place_foul);
            item_place_name = (TextView) itemView.findViewById(R.id.item_place_name);
            item_place_rating = (RatingBar) itemView.findViewById(R.id.item_place_rating);


            item_place_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {

                        User user = (User) list.get(getAdapterPosition());
                        if (usersType != Constant._TYPE_FOLLOW_FESTIVAL) {
                            if (user.getFollowed() == 0) {
                                item_place_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unfollow));
                                user.setFollowed(1);
                                user.setFollowed_by_count(user.getFollowed_by_count() + 1);

                            } else {
                                item_place_follow.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow));
                                user.setFollowed(0);
                                user.setFollowed_by_count(user.getFollowed_by_count() - 1);

                            }
                            follow(Constant._TYPE_FOLLOW_USER, user.getId());
                        } else {
                            follow(Constant._TYPE_FOLLOW_FESTIVAL, user.getId());

                        }
                    } else {
                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }
                }
            });


            place_img_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(usersType)));

        }
    }

    public class AdvertisingHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.user_img)
        ImageView user_img;
        @BindView(R.id.img)
        ImageView img;
        @BindView(R.id.user_name)
        TextViewCast user_name;


        public AdvertisingHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    public class ProductHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_product_img)
        ImageView item_product_img;
        @BindView(R.id.item_product_user_avatar)
        ImageView item_product_user_avatar;
        @BindView(R.id.item_product_user_type_img)
        ImageView item_proditem_product_user_type_img;
        @BindView(R.id.item_product_share)
        ImageView item_product_share;
        @BindView(R.id.item_product_location)
        ImageView item_product_location;
        @BindView(R.id.item_product_add_to_favorite)
        ImageView item_product_add_to_favorite;
        @BindView(R.id.item_product_add_comment)
        ImageView item_product_add_comment;
        @BindView(R.id.item_product_user_name)
        TextViewCast item_product_user_name;
        @BindView(R.id.item_product_sub_user_name)
        TextViewCast item_product_sub_user_name;
        @BindView(R.id.item_product_number_of_like)
        TextViewCast item_product_number_of_like;
        @BindView(R.id.item_product_number_of_eaters)
        TextViewCast item_product_number_of_eaters;
        @BindView(R.id.item_product_comment_1)
        TextViewCast item_product_comment_1;
        @BindView(R.id.item_product_comment_name_1)
        TextViewCast item_product_comment_name_1;
        @BindView(R.id.item_product_comment_2)
        TextViewCast item_product_comment_2;
        @BindView(R.id.item_product_comment_name_2)
        TextViewCast item_product_comment_name_2;

        @BindView(R.id.comments)
        LinearLayout comments;


        public ProductHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            item_product_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openActivity(ProductDetailsActivity.class, usersType, gson.toJson(list.get(getAdapterPosition())));

                }
            });
            item_product_number_of_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());

                    if (product.getIs_liked() == 0) {
                        item_product_number_of_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(1)), null, null);
                        item_product_number_of_like.setText((product.getTotal_likes() + 1) + "");
                        product.setIs_liked(1);
                        product.setTotal_likes(product.getTotal_likes() + 1);
                    } else {
                        item_product_number_of_like.setCompoundDrawablesRelativeWithIntrinsicBounds(null, context.getResources().getDrawable(AapHalper.isLike(0)), null, null);
                        item_product_number_of_like.setText((product.getTotal_likes() - 1) + "");
                        product.setIs_liked(0);
                        product.setTotal_likes(product.getTotal_likes() - 1);
                    }
                    like(product.getId());
                }
            });
            item_product_add_to_favorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());
                    if (product.getIs_wishlisted() == 0) {
                        item_product_add_to_favorite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(1)));
                        product.setIs_wishlisted(1);
                    } else {
                        item_product_add_to_favorite.setImageDrawable(context.getResources().getDrawable(AapHalper.isWhishlestd(0)));
                        product.setIs_wishlisted(0);
                    }

                    addToWishlisted(product.getId());
                }
            });

            item_product_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Product product = (Product) list.get(getAdapterPosition());
                    AapHalper.sher(context, item_product_img, product.getName());
                }
            });


        }
    }

    private class PlaceHolder extends RecyclerView.ViewHolder {
        ImageView item_place_img, item_place_foul, place_img_type;
        TextView item_place_name, item_place_adress;
        RatingBar item_place_rating;

        public PlaceHolder(View itemView) {
            super(itemView);
            item_place_img = (ImageView) itemView.findViewById(R.id.item_place_img);
            place_img_type = (ImageView) itemView.findViewById(R.id.place_img_type);
            item_place_foul = (ImageView) itemView.findViewById(R.id.item_place_foul);
            item_place_name = (TextView) itemView.findViewById(R.id.item_place_name);
            item_place_adress = (TextView) itemView.findViewById(R.id.item_place_adress);
            item_place_rating = (RatingBar) itemView.findViewById(R.id.item_place_rating);


            item_place_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {
                        gson.toJson(list.get(getAdapterPosition()));
                        openActivity(PlaceDetailsActivity.class, usersType, gson.toJson(list.get(getAdapterPosition())));
                    } else {

                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }


                }
            });


            item_place_foul.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (pref.getUser() != null) {
                        User user = (User) list.get(getAdapterPosition());
                        if (usersType != Constant._TYPE_FOLLOW_FESTIVAL) {
                            if (user.getFollowed() == 0) {
                                item_place_foul.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unfollow));
                                user.setFollowed(1);
                                user.setFollowed_by_count(user.getFollowed_by_count() + 1);

                            } else {
                                item_place_foul.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_follow));
                                user.setFollowed(0);
                                user.setFollowed_by_count(user.getFollowed_by_count() - 1);

                            }
                            follow(Constant._TYPE_FOLLOW_USER, user.getId());
                        } else {
                            follow(Constant._TYPE_FOLLOW_FESTIVAL, user.getId());

                        }


                    } else {

                        AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                            @Override
                            public void dialog(final Dialog dialog) {
                                ButtonView signUp, close;
                                signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                close = (ButtonView) dialog.findViewById(R.id.close);

                                signUp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        context.finish();
                                    }
                                });
                                close.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.cancel();
                                    }
                                });
                            }
                        });
                    }
                }
            });


            place_img_type.setImageDrawable(context.getResources().getDrawable(AapHalper.userIconType(usersType)));


        }
    }

    private class NavigationHolder extends RecyclerView.ViewHolder {
        TextViewCast item_navigation_name;
        ImageView item_navigation_img;

        public NavigationHolder(final View itemView) {
            super(itemView);
            item_navigation_name = (TextViewCast) itemView.findViewById(R.id.item_navigation_name);
            item_navigation_img = (ImageView) itemView.findViewById(R.id.item_navigation_img);
            intent = new Intent(context, MyProfileInfoActivity.class);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    NavigationBarItem barItem = (NavigationBarItem) list.get(getAdapterPosition());
                    switch (barItem.getId()) {
                        case Constant._PAGE_MY_PROFILE:
                            ((MainActivity) context).openFragment(new MyProfileFragment());
                            break;

                        case Constant._PAGE_WISHLISTED:
                            ((MainActivity) context).openFragment(new WishlistedFragment());
                            break;
                        case Constant._SHARE:
                            AapHalper.share("مشاركة تطبيق ريادة", context);
                            break;
                        case Constant._PAGE_MY_BRANCH:
                            intent.putExtra("user", gson.toJson(pref.getUser()));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            context.startActivity(intent);
                            break;
                        case Constant._PAGE_SHARCH:
                            ((MainActivity) context).openFragment(new SharchFragment());
                            break;
                        case Constant._PAGE_BLOCK:
                            ((MainActivity) context).openFragment(new BlockFragment());
                            break;
                        case Constant._PAGE_ABOUT_DEVLOPER:
                            ((MainActivity) context).openFragment(new AboutDevloperFragment());
                            break;
                        case Constant._HALPER:
                            ((MainActivity) context).openFragment(new HalperFragment());
                            break;
                        case Constant.SETTINGS:
                            ((MainActivity) context).openFragment(new SettingsFragment());
                            break;
                        case Constant._PAGE_LOGOUT:
                            pref.clearUser();
                            logOut();
                            break;

                    }
                }
            });


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }


    /**
     * Translates the adapter in Y
     *
     * @param of offset in px
     */
    private void translateHeader(float of) {
        float ofCalculated = of * mScrollMultiplier;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB && of < mHeader.getHeight()) {
            mHeader.setTranslationY(ofCalculated);
        } else if (of < mHeader.getHeight()) {
            TranslateAnimation anim = new TranslateAnimation(0, 0, ofCalculated, ofCalculated);
            anim.setFillAfter(true);
            anim.setDuration(0);
            mHeader.startAnimation(anim);
        }
        mHeader.setClipY(Math.round(ofCalculated));

    }

    /**
     * Set the view as header.
     *
     * @param header The inflated header
     * @param view   The RecyclerView to set scroll listeners
     */
    public void setParallaxHeader(View header, final RecyclerView view) {
        mRecyclerView = view;
        mHeader = new CustomRelativeWrapper(header.getContext(), mShouldClipView);
        mHeader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mHeader.addView(header, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mHeader != null) {
                    translateHeader(mRecyclerView.getLayoutManager().getChildAt(0) == mHeader ?
                            mRecyclerView.computeVerticalScrollOffset() : mHeader.getHeight());

                }
            }
        });
    }


    /**
     * @return true if there is list header on this adapter, false otherwise
     */
    private boolean hasHeader() {
        return mHeader != null;
    }


    class CustomRelativeWrapper extends RelativeLayout {

        private int mOffset;
        private boolean mShouldClip;

        public CustomRelativeWrapper(Context context, boolean shouldClick) {
            super(context);
            mShouldClip = shouldClick;
        }

        @Override
        protected void dispatchDraw(Canvas canvas) {
            if (mShouldClip) {
                canvas.clipRect(new Rect(getLeft(), getTop(), getRight(), getBottom() + mOffset));
            }
            super.dispatchDraw(canvas);
        }

        public void setClipY(int offset) {
            mOffset = offset;
            invalidate();
        }
    }


    private void follow(int followType, int userId) {
        ArrayList<Part> parts = new ArrayList<Part>();
        parts.add(new StringPart("follow_type", followType + ""));
        parts.add(new StringPart("user_id", userId + ""));
        AapHalper.userAction("follow", parts, context, null);
    }


    private void like(int productId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product_id", productId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/like", hashMap, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("Like", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("Like", errorMessage);
            }
        });


    }


    private void addToWishlisted(int productId) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("product_id", productId + "");
        request.MultipartRequest(Request.Method.POST, false, "product/wishlist", hashMap, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("wishlist", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("wishlist", errorMessage);
            }
        });


    }


    private void openActivity(Class activity, int userType, String data) {
        Intent intent = new Intent(context, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        intent.putExtra("data", data);
        context.startActivity(intent);

    }

    private void logOut() {
        Intent intent = new Intent(context, SelectTheUserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((MainActivity) context).finish();


    }

    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

}