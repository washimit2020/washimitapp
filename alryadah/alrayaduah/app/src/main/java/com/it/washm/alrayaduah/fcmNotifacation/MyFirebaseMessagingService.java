/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.it.washm.alrayaduah.fcmNotifacation;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import com.it.washm.alrayaduah.CommentReplieActivity;
import com.it.washm.alrayaduah.MainActivity;
import com.it.washm.alrayaduah.NewsDetailsActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.model.NotificationModel;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String[] dateAndTime;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. NotificationModel messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            JSONObject object = new JSONObject(remoteMessage.getData());


            Gson gson = new Gson();
            try {
                NotificationModel notificationModel = gson.fromJson(new JSONObject(object.getString("data")).toString(), NotificationModel.class);

                sendNotification(notificationModel.msg, notificationModel.title, notificationModel.id, notificationModel.type, notificationModel);

                Log.d("mdj,LFJASKLFJAKF", notificationModel.type);
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message NotificationModel Body: " + remoteMessage.getNotification().getBody());
            Log.d(TAG, "Message NotificationModel Body: " + remoteMessage.getNotification().getTitle());
            // sendNotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void sendNotification(String messageBody, String Title, int id, String type, NotificationModel notificationModel) {
        Intent intent;
        if (type.equalsIgnoreCase(Links.NewsLiked)) {
            dateAndTime = AppHelper.getDateType(notificationModel.news.getUpdated_at(), this, "yyyy-MM-dd' 'HH:mm:ss").split(" ").clone();
            intent = new Intent(this, NewsDetailsActivity.class);
            intent.putExtra("date", dateAndTime[0]);
            intent.putExtra("time", dateAndTime[1]);
            intent.putExtra("newId", notificationModel.news.getId());
            intent.putExtra("img_new", notificationModel.news.getAttachment().get(0).getPath());
            intent.putExtra("img_user", AppHelper.getAtachmentUser(notificationModel.news, 0));
            intent.putExtra("title", notificationModel.news.getTitle());
            if (notificationModel.news.getUser().getNick_name() != null) {
                intent.putExtra("name", notificationModel.news.getUser().getNick_name());
            } else {
                intent.putExtra("name", notificationModel.news.getUser().getFirst_name() + " " + notificationModel.news.getUser().getLast_name());
            }
            intent.putExtra("content", notificationModel.news.getContent());

            NewsDetailsActivity.attachments = notificationModel.news.getAttachment();

        } else if (type.equalsIgnoreCase(Links.commentLiked)) {
            String[] a = AppHelper.getDateType(notificationModel.comment.getCreated_at(), this, "yyyy-MM-dd' 'HH:mm:ss").split(" ").clone();
            intent = new Intent(this, CommentReplieActivity.class);
            intent.putExtra("commentId", notificationModel.comment.getId());
            intent.putExtra("date", a[0]);
            intent.putExtra("time", a[1]);

            if (notificationModel.comment.getUser().getNick_name() == null) {

                intent.putExtra("user_name", notificationModel.comment.getUser().getFirst_name() + " "
                        + notificationModel.comment.getUser().getLast_name());

            } else {
                intent.putExtra("user_name", notificationModel.comment.getUser().getNick_name());

            }
            intent.putExtra("img_user", AppHelper.getAtachmentComment(notificationModel.comment, 0));
            intent.putExtra("comment", notificationModel.comment.getContent());
            intent.putExtra("like_count", notificationModel.comment.getLikes_count());
            intent.putExtra("IsLike", notificationModel.comment.is_liked());

        } else if (type.equalsIgnoreCase(Links.Newscomment)) {
            intent = new Intent(this, MainActivity.class);
//            intent=new Intent(this,commentActivity.class);
        } else if (type.equalsIgnoreCase(Links.CommentReplie)) {
            intent = new Intent(this, MainActivity.class);
//            intent=new Intent(this,CommentReplieActivity.class);
        } else if (type.equalsIgnoreCase(Links.CommentReplieLike)) {
            intent = new Intent(this, MainActivity.class);
//            intent=new Intent(this,CommentReplieActivity.class);
        } else if (type.equalsIgnoreCase(Links.NewsStatus)) {
            intent = new Intent(this, MainActivity.class);
//            intent=new Intent(this,CommentReplieActivity.class);
        }

        else {
            intent = new Intent(this, MainActivity.class);
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_alraidia_logo_slide_menu)
                .setContentTitle(Title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setOnlyAlertOnce(false)
                .setContentIntent(pendingIntent);

        notificationBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
    }


}
