<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('news_main_type_id')->index();
            $table->unsignedInteger('news_sub_type_id')->index()->nullable();
            $table->string('title');
            $table->text('content');
            $table->text('disapprove_description')->nullable();
            $table->boolean('is_banner')->default(false);
            $table->boolean('is_main')->default(false);

            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('news_status_id')->index();

            $table->timestamps();

            $table->foreign('news_main_type_id')->references('id')->on('news_main_types');
            $table->foreign('news_sub_type_id')->references('id')->on('news_sub_types');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('news_status_id')->references('id')->on('news_statuses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
