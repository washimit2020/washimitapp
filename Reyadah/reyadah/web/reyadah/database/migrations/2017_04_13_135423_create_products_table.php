<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->unsignedTinyInteger('number_of_eaters');
            $table->text('ingredients');
            $table->string('price',3);
            $table->double('rating', 2, 1)->default(0.0);
            $table->unsignedInteger('total_likes')->default(0);
            $table->boolean('is_sponsored')->default(0);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('status_id')->default(\App\Http\Constants::_STATUS_ACTIVE);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
