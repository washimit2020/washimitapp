package com.it.washim.wojooohapp.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.it.washim.wojooohapp.Halper.ApiRequests;

/**
 * Created by mohammad on 9/11/2017.
 */

public class BaseActivity extends AppCompatActivity {
    Gson gson;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gson = new Gson();
    }

    public void openActivity(Context context, Class activity) {


        startActivity(new Intent(context, activity));

    }

    public void openActivity(Context context, Class activity, String key, int value) {


        startActivity(new Intent(context, activity).putExtra(key, value));


    }
    public void openActivity(Context context, Class activity, String key, String value) {


        startActivity(new Intent(context, activity).putExtra(key, value));


    }


    public ApiRequests getRequest() {
        return ApiRequests.getInstance(this);

    }

    public Gson getGson() {
        return gson;

    }


}
