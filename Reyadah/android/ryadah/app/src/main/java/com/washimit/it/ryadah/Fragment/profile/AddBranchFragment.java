package com.washimit.it.ryadah.Fragment.profile;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AddBranchFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.add_branch_name)
    EditTextView add_branch_name;
    @BindView(R.id.add_branch_sector)
    EditTextView add_branch_sector;
    @BindView(R.id.add_branch_street_name)
    EditTextView add_branch_street_name;
    @BindView(R.id.add_branch_show_location)
    ButtonView add_branch_show_location;
    @BindView(R.id.add_branch_but)
    ImageView add_branch_but;
    ApiRequest request;
    HashMap<String, String> prams;
    LatLng lng;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant._TYPE_MAP) {

            if (data != null) {
                lng = (LatLng) data.getExtras().get("latLng");
                Log.d("LatLnge", lng.latitude + " " + lng.longitude);
                prams.put("lat", lng.latitude + "");
                prams.put("lng", lng.longitude + "");
            } else {

            }


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_branch, container, false);
        ButterKnife.bind(this, view);
        request = new ApiRequest(getActivity());
        add_branch_but.setOnClickListener(this);
        add_branch_show_location.setOnClickListener(this);
        prams = new HashMap<String, String>();

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.add_branch_but:

                if (validation()) {

                    addBranch();

                }

                break;
            case R.id.add_branch_show_location:
                openActivity(MapsActivity.class, Constant._TYPE_MAP);
                break;


        }

    }


    public boolean validation() {


        if (add_branch_name.getText().toString().equalsIgnoreCase("")) {
            add_branch_name.setError(getResources().getString(R.string.fill_name));
            add_branch_name.requestFocus();
            return false;
        }


        if (add_branch_sector.getText().toString().equalsIgnoreCase("")) {
            add_branch_sector.setError(getResources().getString(R.string.fill_street));
            add_branch_sector.requestFocus();

            return false;
        }
        if (add_branch_street_name.getText().toString().equalsIgnoreCase("")) {
            add_branch_street_name.setError(getResources().getString(R.string.fill_street));
            add_branch_street_name.requestFocus();
            return false;
        }

        return true;
    }

    private void addBranch() {
        prams.put("name", add_branch_name.getText().toString());
        prams.put("street_name", add_branch_street_name.getText().toString());
        prams.put("sector_name", add_branch_sector.getText().toString());

        request.MultipartRequest(Request.Method.POST, false, "branch", prams, getString(R.string.add_branch), new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("addBranch", result);
                ((MainActivity) getActivity()).openFragment(new MyProfileFragment());
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("addBranch", errorMessage);

            }
        });

    }

    public void openActivity(Class activity, int requstCode) {

        Intent intent = new Intent(getActivity(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, requstCode);

    }

}
