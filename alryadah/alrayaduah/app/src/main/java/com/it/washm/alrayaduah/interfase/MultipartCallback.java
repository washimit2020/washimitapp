package com.it.washm.alrayaduah.interfase;

/**
 * Created by washm on 2/23/17.
 */

public interface MultipartCallback {
    /**
     *
     * @param result
     */
    void onSuccess(String result);

    /**
     *
     * @param errorMessage
     */
    void onRequestError(String errorMessage);

}