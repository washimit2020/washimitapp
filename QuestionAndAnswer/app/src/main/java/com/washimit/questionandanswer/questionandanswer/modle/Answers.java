package com.washimit.questionandanswer.questionandanswer.modle;

import java.util.ArrayList;

/**
 * Created by mohammad on 6/4/2017.
 */

public class Answers {


    public int id;
    public String title;
    public String body;
    public int is_correct;
    public boolean select = false;
    public String created_at;
    public String updated_at;
    public ArrayList<Attachments> attachments;
}
