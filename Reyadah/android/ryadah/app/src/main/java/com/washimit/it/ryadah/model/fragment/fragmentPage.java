package com.washimit.it.ryadah.model.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by mohammad on 4/17/2017.
 */

public class fragmentPage {
    private Fragment fragment;
    private String title;

    /**
     * @param fragment
     * @param title
     */
    public fragmentPage(Fragment fragment, String title) {
        this.fragment = fragment;
        this.title = title;
    }

    /**
     * @return fragment
     */
    public Fragment getFragment() {
        return fragment;
    }


    /**
     * @return title
     */
    public String getTitle() {return title;}
}
