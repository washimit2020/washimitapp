<?php

namespace App\Http\Middleware;

use Closure;

class ApplicationKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasHeader('app-key') || ( $request->header("app-key") != config('app.key') )){
            abort(403);
        }
        return $next($request);
    }
}
