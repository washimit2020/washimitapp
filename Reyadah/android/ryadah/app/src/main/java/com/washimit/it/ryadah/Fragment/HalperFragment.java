package com.washimit.it.ryadah.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.washimit.it.ryadah.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HalperFragment extends Fragment {


    public HalperFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_halper, container, false);
    }

}
