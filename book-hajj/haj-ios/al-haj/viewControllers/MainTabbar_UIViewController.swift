//
//  MainTabbar_UIViewController.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

class MainTabbar_UIViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    var vm:MainTabBarVM!
    var section:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //        navigationController?.navigationBar.setBackgroundImage(#imageLiteral(resourceName: "background"), for: .default)
        //        selectedViewController = viewControllers[2]
        vm = MainTabBarVM(tabBar)
        vm.delegate = self
        didChangeController()
        if let section = section {
            Utils.shared.go_to_section(tabBarController: self , section:section )
        }
        title = " "
    }
    
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        
        // Add Child View as Subview
        viewController.view.frame = containerView.bounds
        containerView.addSubview(viewController.view)
        // Configure Child View
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
}
extension MainTabbar_UIViewController : MainTabBarVMDelegate {
    func didChangeController() {
        vm.viewControllers.forEach { (vc) in
            self.remove(asChildViewController: vc)
        }
        add(asChildViewController:  vm.selectedViewController)
        if let book = vm.selectedViewController as? Book_PageViewController {
            navigationItem.leftBarButtonItems = [book.favIcon, book.comment]
        }else{
            navigationItem.leftBarButtonItems?.removeAll()
        }
    }
    func didSelectShare() {
        guard let vc = (vm.selectedViewController as? Book_PageViewController)?.viewControllers?.first as? PageController_ViewController else {return}
        var content = [Any] ()
        content.append(vc.body.text!)
        if let image = vc.imageView.image {
            content.append(image)
        }
        let act = UIActivityViewController(activityItems: content, applicationActivities: [])
        act.popoverPresentationController?.sourceRect = tabBar.bounds
        act.popoverPresentationController?.sourceView = tabBar
        present(act, animated: true)
    }
}



