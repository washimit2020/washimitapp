package com.it.washim.wojooohapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by mohammad on 9/18/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    ArrayList<String> titleArrayList;
    ArrayList<Fragment> fragmentArrayList;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        titleArrayList = new ArrayList<>();
        fragmentArrayList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleArrayList.get(position);
    }


    public void addPage(Fragment fragment, String title) {
        titleArrayList.add(title);
        fragmentArrayList.add(fragment);
    }
}
