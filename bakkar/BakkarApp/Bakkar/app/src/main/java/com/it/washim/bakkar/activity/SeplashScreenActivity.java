package com.it.washim.bakkar.activity;


import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.model.Posts;

public class SeplashScreenActivity extends BaseActivity {
    CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seplash_screen);


        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d("aSFRASFASF", key + value);

                if (key.equalsIgnoreCase("post")) {

                    switch (gson().fromJson(value.toString(), Posts.class).page_id) {
                        case Constant.PAGES_ARTICLES:
                            openActivity(MainActivity.class, "page_type", Constant.ARTICLES);
                            break;
                        case Constant.PAGES_AUDIO:
                            openActivity(MainActivity.class, "page_type", Constant.SOUNDS);
                            break;
                        case Constant.PAGES_BOOKS:
                            openActivity(MainActivity.class, "page_type", Constant.BOOKS);
                            break;


                    }
                } else if (key.equalsIgnoreCase("profile")) {
                    start();
                }
            }
        } else {
            start();
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        if (timer != null) {
            timer.cancel();
            finish();
        }

    }

    private void start() {

        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                if (shardPref().getUser() != null) {
                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), HelloActivity.class));
                    finish();
                }

            }
        }.start();
    }
}
