package com.it.washim.wojooohapp.Model;

/**
 * Created by mohammad on 9/12/2017.
 */

public class BookRequest {
    public String bookTitle;
    public int bookNumber;

    public BookRequest(String bookTitle, int bookNumber) {
        this.bookTitle = bookTitle;
        this.bookNumber = bookNumber;
    }
}
