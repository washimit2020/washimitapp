//
//  CollectionImageView.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class CollectionImageView: UIImageView {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        add_grey_border(layer: layer)
    }

}
