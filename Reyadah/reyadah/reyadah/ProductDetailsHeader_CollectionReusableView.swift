//
//  ProductDetails_CollectionReusableView.swift
//  reyadah
//
//  Created by WASHM on 4/23/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ProductDetailsHeader_CollectionReusableView: UserDetailsHeader_CollectionReusableView {
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var wishlist: AspectFitButton!
    @IBOutlet weak var like: AspectFitButton!
    @IBOutlet weak var totalLikes: UILabel!
    @IBOutlet weak var numberOfEaters: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        wishlist.tag = 1
        like.tag = 2
        
        like.addTarget(self, action: #selector(like_or_wishilst(_:)), for: .touchUpInside)
        wishlist.addTarget(self, action: #selector(like_or_wishilst(_:)), for: .touchUpInside)
    }
    
    override func updateView() {
        guard let product = model as? Product else {
            return
        }
        let images = make_slider_images(id: product.id ,placeholder: nil,url: Constants.GET_PRODUCT_IMAGE)
        wishlist.imageView?.contentMode = .scaleAspectFit
        like.imageView?.contentMode = .scaleAspectFit
        slider.setImageInputs(images as! [KingfisherSource])
        ratingStars.rating = product.rating
        price.text = set_price(product.price)
        shortDescription.text = product.ingredients
        totalLikes.text = set_likes(product.total_likes)
        numberOfEaters.text = product.numberOfEaters
        change_like_wishlist()
        
        super.updateView()
        
    }
    
    func like_or_wishilst(_ sender:AspectFitButton)  {
        apiRequest?.likeOrWishlist(sender, (model as? Product)!, totalLikes)
    }
    
    public func change_like_wishlist(){
        guard let product = model as? Product else {
            return
        }
        change_tint_of_button(button: like, checked: product.is_liked)
        change_tint_of_button(button: wishlist, checked: product.is_wishlisted)
    }
}
