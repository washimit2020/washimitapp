package com.washimit.questionandanswer.questionandanswer.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.activity.MainActivity;
import com.washimit.questionandanswer.questionandanswer.adapter.searchAdapter;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.interfac.pageIdCallback;
import com.washimit.questionandanswer.questionandanswer.modle.Question;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends BaseFragment {

    EditText search;
    TextView no_data;
    List<Question> searchResult;
    RecyclerView search_result;
    searchAdapter searchAdapter;
    ApiRequest request;
    Gson gson;
    LinearLayout no_conection;
    MyButtonView retry;
    RelativeLayout search_page;

    @BindView(R.id.progress)
    ProgressBar progress;

    public SearchFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this,view);
        request = new ApiRequest(getActivity());
        gson = new Gson();
        search = (EditText) view.findViewById(R.id.search_textt);
        no_data = (TextView) view.findViewById(R.id.no_data);
        search_result = (RecyclerView) view.findViewById(R.id.search_result);
        no_conection = (LinearLayout) view.findViewById(R.id.no_conection);
        retry = (MyButtonView) view.findViewById(R.id.retry);
        search_page = (RelativeLayout) view.findViewById(R.id.search_page);


        searchResult = new ArrayList<>();

        search_result.setHasFixedSize(true);
        search_result.setLayoutManager(new LinearLayoutManager(getActivity()));

        searchAdapter = new searchAdapter(getActivity(), searchResult);

        search_result.setAdapter(searchAdapter);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    if (!search.getText().toString().equalsIgnoreCase("")) {
//                        progress.setVisibility(View.VISIBLE);
//                        // Toast.makeText(getContext(), search.getText().toString(), Toast.LENGTH_LONG).show();
//                        request.getRequst("search/" + search.getText().toString(), new RequestCallback() {
//                            @Override
//                            public void onResponse(String response) throws JSONException {
//
//                                JSONObject jsonObject = null;
//
//                                JSONObject questions = null;
//
//                                Log.d("IME_ACTION_SEARCH", response);
//
//                                jsonObject = new JSONObject(response);
//
//                                questions = jsonObject.getJSONObject("question");
//                                searchResult.clear();
//
//
//                                searchResult.addAll((Collection<? extends Question>) gson.fromJson(questions.getString("data"), new TypeToken<ArrayList<Question>>() {
//                                }.getType()));
//
//
//                                searchAdapter.notifyDataSetChanged();
//
//                                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//                                in.hideSoftInputFromWindow(search.getWindowToken(), 0);
//                                if (searchResult.size() == 0) {
//                                    no_data.setVisibility(View.VISIBLE);
//                                    search_result.setVisibility(View.GONE);
//                                } else {
//                                    search_result.setVisibility(View.VISIBLE);
//                                    no_data.setVisibility(View.GONE);
//                                }
//
//                                search_page.setVisibility(View.VISIBLE);
//                                no_conection.setVisibility(View.GONE);
//                            }
//
//                            @Override
//                            public void ErrorListener() {
//                                Toast.makeText(getContext(), " يوجد مشكلة بالاتصال!", Toast.LENGTH_LONG).show();
//
//                                search_page.setVisibility(View.GONE);
//                                no_conection.setVisibility(View.VISIBLE);
//                            }
//                        });
//                    } else {
                        get();
              //     }

                }

                return true;
            }
        });


        searchAdapter.onpageIdCollback(new pageIdCallback() {
            @Override
            public void pageId(int pageId) {
                PageFragment pageFragment = new PageFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", pageId + "");
                pageFragment.setArguments(bundle);
                ((MainActivity) getActivity()).openFragment(pageFragment);

            }
        });


        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get();
            }
        });
        get();
        return view;
    }

    public void get() {
        progress.setVisibility(View.VISIBLE);
        request.getRequst("search/" + search.getText().toString() + "", new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject questions = null;

                Log.d("IME_ACTION_SEARCH", response);

                jsonObject = new JSONObject(response);

                questions = jsonObject.getJSONObject("question");
                searchResult.clear();


                searchResult.addAll((Collection<? extends Question>) gson.fromJson(questions.getString("data"), new TypeToken<ArrayList<Question>>() {
                }.getType()));


                searchAdapter.notifyDataSetChanged();

                InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                in.hideSoftInputFromWindow(search.getWindowToken(), 0);
                if (searchResult.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                    search_result.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);
                } else {
                    search_result.setVisibility(View.VISIBLE);
                    no_data.setVisibility(View.GONE);
                    progress.setVisibility(View.GONE);

                }

                search_page.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
                progress.setVisibility(View.GONE);

            }

            @Override
            public void ErrorListener() {
                Toast.makeText(getContext(), " يوجد مشكلة بالاتصال!", Toast.LENGTH_LONG).show();

                search_page.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
                progress.setVisibility(View.GONE);

            }
        });

    }


}
