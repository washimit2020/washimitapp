//
//  LocationSearch_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 4/17/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import MapKit

class LocationSearch_TableViewController: UITableViewController {
    var items = [MKMapItem]()
    var map:MKMapView? = nil
    
    let id = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(LocationsSearch_TableViewCell.self, forCellReuseIdentifier: id)
    }
  
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
        
        // Configure the cell...
        let selectedItem = items[indexPath.row].placemark
        cell.textLabel?.text = selectedItem.name
        cell.detailTextLabel?.text = ""
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedItem = items[indexPath.row].placemark
        change_mapRegion(placemark: selectedItem)
    }
    
    func change_mapRegion(placemark:MKPlacemark) {
        guard let map = map else {
            return
        }
        self.dismiss(animated: true, completion: {
            map.removeAnnotations(map.annotations)
            map.showsUserLocation = false
            map.setCenter(placemark.coordinate, animated: true)
            let annotaion = MKPointAnnotation()
            annotaion.coordinate = placemark.coordinate
            map.addAnnotation(annotaion)
        })
        
    }
    
}

extension LocationSearch_TableViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let request = MKLocalSearchRequest()
        guard let map = map, let searchBarText = searchController.searchBar.text else {
            return
        }
        
        request.naturalLanguageQuery = searchBarText
        request.region = map.region
        let search = MKLocalSearch(request: request)
        search.start { response, _ in
            guard let response = response else {
                return
            }
            self.items = response.mapItems
            self.tableView.reloadData()
        }
    }
    
}
