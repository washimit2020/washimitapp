package com.it.washm.alrayaduah.interfase;

/**
 * Created by washm on 1/9/17.
 */

public interface OnLoadMoreListener {
    /**
     * load more item to recycler view :)
     */
    void onLoadMore();
}
