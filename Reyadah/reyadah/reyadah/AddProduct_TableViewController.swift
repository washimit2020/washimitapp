//
//  AddProduct_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 4/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import SwiftyJSON

class AddProduct_TableViewController: FormsTVC {
    
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var nameError: UILabel!
    @IBOutlet weak var ingred: UITextView!
    @IBOutlet weak var ingredError: UILabel!
    @IBOutlet weak var numberOfEaters: UIPickerView!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var priceError: UILabel!
    @IBOutlet weak var locationSegmant: UISegmentedControl!
    @IBOutlet weak var addImageStack: UIStackView!
    
    let pickerDataSource = ["1", "2", "3", "4", "5", "6","7","8","9","10"];
    //    var viewController:MainViewController?
    var apiImages = [String:UIImage]()
    var product:Product?
    var msg = "جاري انشاء منتج"
    var link = RequestLinks.product.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRightBarButtonWithImage(UIImage(named:"Side_bar_icon")!)
        
        numberOfEaters.dataSource = self
        numberOfEaters.delegate = self

        registerFields()
        
//        locationSegmant.addTarget(nil, action: #selector(show_locations(_:)), for: .valueChanged)
    }
    
    private func registerFields(){
        fields = [
            (name, nameError),(ingred ,ingredError),(price,priceError)
        ]
        
        validator.registerField(name, errorLabel: nameError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 255)])
        validator.registerField(ingred, errorLabel: ingredError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 500)])
        validator.registerField(price, errorLabel: priceError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 4)])
        
        if let product = product{
            name.text = product.name
            ingred.text = product.ingredients
            numberOfEaters.selectRow((Int(product.numberOfEaters)! - 1), inComponent: 0, animated: false)
            price.text = product.price
            link += "/\(product.id!)/update"
            addImageStack.isHidden = true
        }
        
        action = done(_:)
    }
    
    override func validationSuccessful() {
        
        postValues["name"] = name.text
        
        postValues["ingredients"] = ingred.text
        
        postValues["number_of_eaters"] = pickerDataSource[numberOfEaters.selectedRow(inComponent: 0)]
        
        postValues["price"] = price.text
        
        if let product = product{
            product.name = postValues["name"]!
            product.ingredients = postValues["ingredients"]!
            product.numberOfEaters = postValues["number_of_eaters"]!
            product.price = postValues["price"]!
        }
        
        request.make_request(parameters: postValues,
                             apiImages: apiImages, link: link,
                             method: .post,
                             dialogMsg: msg,
                             completion: handler)
        
        
    }
    
    private func handler(_ response:DataRequest?)->Void{
        
        guard let response = response else {
            self.request.handle_error_dialog(nil)
            return
        }
        
        response.responseJSON{
            response in
            
            
            if response.response?.statusCode == 422{
                self.request.handle_error_dialog(nil)
            }
                
            else if response.response?.statusCode == 200{
                self.request.costumeAlert.closeButton.isHidden = false
                self.request.costumeAlert.loader.stopAnimating()
                self.request.costumeAlert.dismiss(animated: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            }else{
                self.request.handle_error_dialog(nil)
            }
        }
        
    }
    
    @IBAction func addImage(_ sender: Any) {
        
        let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.image_adder_id) as! ImageAdder_ViewController
        controller.hideAvatar = true
        for (_,value) in self.apiImages{
            controller.items.append(value)
        }
        
        controller.getImagesCallback = {
            (avatar , covers) in
            
            if let covers = covers {
                for i in 0...covers.count-1{
                    self.apiImages["images[\(i)]"] = covers[i]
                }
            }
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func done(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
//    func show_locations(_ sender:UISegmentedControl){
//        let locations = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Storyboard_ids.locations.rawValue) as! Locations_ViewController
//        locations.selected = sender.selectedSegmentIndex
//        locations.delegate = self
//        self.navigationController?.pushViewController(locations, animated: true)
//    }
    
}

extension AddProduct_TableViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
    
//    func get_locations(_ selected: Int) {
//        locationSegmant.selectedSegmentIndex = selected
//    }
}

