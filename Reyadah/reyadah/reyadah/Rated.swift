//
//  Rated.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//


class Rated: Model {
    var user_rating :Double!
    var view_rating:Double!
    var products_rating:Double!
    var prices_rating:Double!
    
    //product
    var quality :Double!
    var price:Double!
    var style:Double!
    var ingredients:Double!
    
    override func map() {
        
        user_rating = json["users_rating"].doubleValue
        view_rating = json["view_rating"].doubleValue
        products_rating = json["products_rating"].doubleValue
        prices_rating = json["prices_rating"].doubleValue
        
        
        quality = json["quality"].doubleValue
        price = json["price"].doubleValue
        style = json["style"].doubleValue
        ingredients = json["ingredients"].doubleValue
    }
    
    subscript(_ key: String)->Double{
        switch key {
        case "users_rating":
            return user_rating
        case "view_rating":
            return view_rating
        case "products_rating":
            return products_rating
        case "prices_rating":
            return prices_rating
            
            //product
        case "quality":
            return quality
            
        case "price":
            return price
            
        case "style":
            return style
            
        case "ingredients":
            return ingredients
        default :
            return 1.0
        }
    }
    
    func setter(key:String, value:Double)  {
        switch key {
        case "users_rating":
            user_rating = value
        case "view_rating":
             view_rating = value
        case "products_rating":
             products_rating = value
        case "prices_rating":
           prices_rating = value
            
            //product
            
        case "quality":
            quality = value
            
        case "price":
            price = value
            
        case "style":
            style = value
             
        case "ingredients":
             ingredients = value
            
        default :
            assert(false,"unkown")
        }
    }
}
