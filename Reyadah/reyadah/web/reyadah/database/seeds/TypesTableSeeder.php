<?php

use Illuminate\Database\Seeder;
use App\Http\Constants;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now();
        DB::table('types')->insert([
            [
                'id'=>Constants::_TYPE_STORE,
                'title'=>'types.store',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_SHOW,
                'title'=>'types.show',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_COSTUMER,
                'title'=>'types.costumer',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_FESTIVAL,
                'title'=>'types.festival',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_PRODUCT,
                'title'=>'types.product',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_AVATAR_IMAGE,
                'title'=>'types.avatar-image',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_COVER_IMAGE,
                'title'=>'types.cover-image',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_PRODUCT_ATTACHMENT,
                'title'=>'types.product-attachment',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_PRODUCT_LIKE,
                'title'=>'types.product-like',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_PRODUCT_WISHLIST,
                'title'=>'types.product-wishlist',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_FOLLOW_USER,
                'title'=>'types.follow-user',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_FOLLOW_FESTIVAL,
                'title'=>'types.follow-festival',
                'created_at'=>$date,
                'updated_at'=>$date
            ],

            [
                'id'=>Constants::_TYPE_FESTIVAL_LICENSE,
                'title'=>'types.festival_license',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
            [
                'id'=>Constants::_TYPE_FESTIVAL_ATTACHMENT,
                'title'=>'types.festival_attachment',
                'created_at'=>$date,
                'updated_at'=>$date
            ],
        ]);
    }
}
