package com.it.washm.alrayaduah.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.refactor.typer.OnWriteTextChangedListener;
import cn.refactor.typer.TyperEditText;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutAppFragment extends Fragment {

    CustamViewFont viewFont;
    TextView aboutApp, content;
    private TyperEditText mTyperEditText;

    public AboutAppFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_about_app, container, false);
        aboutApp = (TextView) view.findViewById(R.id.aboutApp);
       // content = (TextView) view.findViewById(R.id.content);
        mTyperEditText=(TyperEditText)view.findViewById(R.id.content) ;
        mTyperEditText.setText(R.string.Hi);
        mTyperEditText.start();

        mTyperEditText.setOnWriteTextChangedListener(new OnWriteTextChangedListener() {
            @Override
            public void onChanged(int index) {

            }

            @Override
            public void onCompleted() {

                mTyperEditText.setCursorVisible(false);
            }
        });

        viewFont = new CustamViewFont(getActivity(), "Cairo-Regular.ttf");
        setFont();
        return view;
    }
    public void setFont() {
        viewFont.setfont(aboutApp);
        viewFont.setfont(mTyperEditText);

    }
}
