package com.washimit.it.ryadah.Fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.cleveroad.pulltorefresh.firework.FireworkyPullToRefreshLayout;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.AddFestival;
import com.washimit.it.ryadah.adapter.FestivalsParallaxAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FestivalsFragment extends Fragment implements View.OnClickListener, FireworkyPullToRefreshLayout.OnRefreshListener {
    @BindView(R.id.festival)
    LinearLayout festival;
    @BindView(R.id.add_festival)
    ImageView add_festival;
    @BindView(R.id.festivals_rv)
    RecyclerView festivals_rv;
    @BindView(R.id.swipeRefreshLayout)
    FireworkyPullToRefreshLayout swipeRefreshLayout;
    @BindView(R.id.linearLayout_no_internt)
    LinearLayout linearLayout_no_internt;
    @BindView(R.id.retry)
    ButtonView retry;
    @BindView(R.id.no_data)
    TextViewCast no_data;
    Intent intent;
    ArrayList<Festivals> list;
    FestivalsParallaxAdapter adapter;
    ApiRequest request;
    String NextUrl;
    Gson gson;

    SharedPref pref;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_festivals, container, false);

        View hider = LayoutInflater.from(getContext()).inflate(R.layout.item_place_header, container, false);

        intent = new Intent(getActivity(), AddFestival.class);

        ButterKnife.bind(this, view);
        request = new ApiRequest(getActivity());
        gson = new Gson();
        pref = new SharedPref(getActivity());
        list = new ArrayList<>();
        festivals_rv.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 2;
                }
                return 1;

            }
        });

        festivals_rv.setLayoutManager(gridLayoutManager);
        adapter = new FestivalsParallaxAdapter(list, getActivity(), Constant._TYPE_FESTIVAL, festivals_rv);

        adapter.setParallaxHeader(hider, festivals_rv);


        adapter.setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                list.add(list.size() - 1, null);
                adapter.notifyItemInserted(list.size() - 1);
                getNext(NextUrl);
            }
        });


        festivals_rv.setAdapter(adapter);


        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                updateUi();

            }
        });


        retry.setOnClickListener(this);
        add_festival.setOnClickListener(this);
        swipeRefreshLayout.setOnRefreshListener(this);


        return view;
    }

    private void getNext(String url) {
        request.MultipartRequest(Request.Method.GET, true, url, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size() - 1);

                Log.d("NextFestivals1", result_);

                JSONObject result = null;
                JSONObject msg = null;
                JSONObject festivals = null;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                festivals = msg.getJSONObject("festivals");

                NextUrl = festivals.getString("next_page_url");
                adapter.setUrl(NextUrl);

                list.addAll((Collection<? extends Festivals>) gson.fromJson(festivals.getString("data"), new TypeToken<ArrayList<Festivals>>() {
                }.getType()));

                Log.d("listsisdjojsd", list.size() + "");
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                linearLayout_no_internt.setVisibility(View.VISIBLE);
                festivals_rv.setVisibility(View.GONE);
                Log.d("festivals", errorMessage);
                swipeRefreshLayout.setRefreshing(false);


            }
        });

    }

    private void updateUi() {
        request.MultipartRequest(Request.Method.GET, false, "festivals", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {

                Log.d("festivals1", result_);

                list.clear();
                JSONObject result = null;
                JSONObject msg = null;
                JSONObject festivals = null;
                result = new JSONObject(result_);
                msg = result.getJSONObject("msg");
                festivals = msg.getJSONObject("festivals");

                NextUrl = festivals.getString("next_page_url");
                adapter.setUrl(NextUrl);

                list.addAll((Collection<? extends Festivals>) gson.fromJson(festivals.getString("data"), new TypeToken<ArrayList<Festivals>>() {
                }.getType()));

                if (list.size() > 0) {
                    no_data.setVisibility(View.GONE);

                } else {
                    no_data.setVisibility(View.VISIBLE);

                }
                Log.d("listsisdjojsd", list.size() + "");
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                adapter.setLoading();
            }

            @Override
            public void onRequestError(String errorMessage) {
                no_data.setVisibility(View.GONE);

                linearLayout_no_internt.setVisibility(View.VISIBLE);
//                festivals_rv.setVisibility(View.GONE);
                Log.d("festivals", errorMessage);
                swipeRefreshLayout.setRefreshing(false);


            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_festival:
                if (pref.getUser() != null) {

                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    festival.setVisibility(View.VISIBLE);
                } else {
                    AapHalper.CreateDialog(R.layout.dialog_sgin_up, getActivity(), new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            ButtonView signUp, close;
                            signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                            close = (ButtonView) dialog.findViewById(R.id.close);

                            signUp.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                    getActivity().finish();
                                }
                            });
                            close.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.cancel();
                                }
                            });
                        }
                    });
                }
                break;
            case R.id.retry:
                swipeRefreshLayout.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                festivals_rv.setVisibility(View.VISIBLE);
                updateUi();
                break;



        }
    }

    @Override
    public void onRefresh() {
        linearLayout_no_internt.setVisibility(View.GONE);
//        festivals_rv.setVisibility(View.VISIBLE);
        updateUi();
    }
}
