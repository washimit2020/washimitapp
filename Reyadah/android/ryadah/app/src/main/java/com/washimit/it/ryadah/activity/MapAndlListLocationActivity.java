package com.washimit.it.ryadah.activity;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.washimit.it.ryadah.Fragment.MapFragment;
import com.washimit.it.ryadah.Fragment.UserLocationListInfoFragment;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.fragmentMangerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapAndlListLocationActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.locations_view_pager)
    ViewPager vPager;
    @BindView(R.id.list)
    ImageView list;
    @BindView(R.id.map)
    ImageView map;
    @BindView(R.id.frame_list)
    FrameLayout frame_list;
    @BindView(R.id.frame_map)
    FrameLayout frame_map;
    fragmentMangerAdapter adapter;
    UserLocationListInfoFragment listInfoFragment;
    MapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_andl_list_location);

        ButterKnife.bind(this);

        Bundle bundle = new Bundle();

        bundle.putInt("id", getIntent().getExtras().getInt("id"));
        bundle.putInt("user_type_id", getIntent().getExtras().getInt("user_type_id"));
        bundle.putString("user", getIntent().getExtras().getString("user"));

        adapter = new fragmentMangerAdapter(getSupportFragmentManager());

        listInfoFragment = new UserLocationListInfoFragment();
        mapFragment = new MapFragment();

        listInfoFragment.setArguments(bundle);
        mapFragment.setArguments(bundle);

        adapter.addFragment(listInfoFragment, "");

        adapter.addFragment(mapFragment, "");

        vPager.setAdapter(adapter);

        vPager.setOffscreenPageLimit(1);

        vPager.setCurrentItem(1);

        frame_map.setOnClickListener(this);

        frame_list.setOnClickListener(this);

        vPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    map.setImageDrawable(getResources().getDrawable(R.drawable.ic_location));
                    frame_map.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground));
                    list.setImageDrawable(getResources().getDrawable(R.drawable.ic_chose_location_by_list_on));
                    frame_list.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground_on));

                } else {
                    map.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_map_on));
                    frame_map.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground_on));
                    list.setImageDrawable(getResources().getDrawable(R.drawable.ic_chose_location_by_list));
                    frame_list.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground));

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frame_map:
                vPager.setCurrentItem(1);
                map.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_map_on));
                frame_map.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground_on));
                list.setImageDrawable(getResources().getDrawable(R.drawable.ic_chose_location_by_list));
                frame_list.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground));

                break;

            case R.id.frame_list:
                vPager.setCurrentItem(0);

                map.setImageDrawable(getResources().getDrawable(R.drawable.ic_location));
                frame_map.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground));
                list.setImageDrawable(getResources().getDrawable(R.drawable.ic_chose_location_by_list_on));
                frame_list.setBackground(getResources().getDrawable(R.drawable.location_list_map_back_ground_on));

                break;


        }
    }
}
