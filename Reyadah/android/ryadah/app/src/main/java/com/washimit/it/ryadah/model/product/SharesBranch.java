package com.washimit.it.ryadah.model.product;

import com.washimit.it.ryadah.model.Branche;

/**
 * Created by mohammad on 5/23/2017.
 */

public class SharesBranch {

    int id;
    int shareable_id;
    String shareable_type;
    int user_id;
    int product_id;
    int status_id;
    String created_at;
    String updated_at;
    Product product;
    Branche shareable;

    public SharesBranch(int id, int shareable_id, String shareable_type, int user_id, int product_id, int status_id, String created_at, String updated_at, Product product, Branche shareable) {
        this.id = id;
        this.shareable_id = shareable_id;
        this.shareable_type = shareable_type;
        this.user_id = user_id;
        this.product_id = product_id;
        this.status_id = status_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.product = product;
        this.shareable = shareable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShareable_id() {
        return shareable_id;
    }

    public void setShareable_id(int shareable_id) {
        this.shareable_id = shareable_id;
    }

    public String getShareable_type() {
        return shareable_type;
    }

    public void setShareable_type(String shareable_type) {
        this.shareable_type = shareable_type;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Branche getShareable() {
        return shareable;
    }

    public void setShareable(Branche shareable) {
        this.shareable = shareable;
    }
}
