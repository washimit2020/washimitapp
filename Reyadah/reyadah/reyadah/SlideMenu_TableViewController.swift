//
//  SlideMenu_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class SlideMenu_TableViewController: UITableViewController {
    fileprivate let cell_id = "Cell"
    enum SidebarActions:String {
        case home = "1"
        case profile = "2"
        case block = "3"
        case settings = "4"
        case invite = "5"
        case help = "6"
        case developers = "7"
        case logout = "8"
        case login = "9"
        case branches = "10"
        case wishlist = "11"
        case requests = "12"
        case search = "13"
    }
    var items = [[String:String]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup_slider()
        tableView!.register(UINib(nibName: "SlideMenu_TableViewCell", bundle: nil), forCellReuseIdentifier:cell_id)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 70
        }
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cell_id, for: indexPath) as! SlideMenu_TableViewCell
        if indexPath.row == 0{
            cell.maxHeight = 50
        }
        
        cell.updateView(dict: items[indexPath.row])
        return cell
    }
    
    func setup_slider(){
        items = [
            [
                "image":"ic_logo_sidebar",
                "title":"الرئيسية",
                "action":SidebarActions.home.rawValue
            ],
            [
                "image":"ic_profile_sidebar",
                "title":"ملفي",
                "action":SidebarActions.profile.rawValue
                
            ]
            ,[
                "image":"ic_search_sidebar",
                "title":"بحث",
                "action":SidebarActions.search.rawValue
                
            ],
             [
                "image":"ic_block",
                "title":"قائمة الحظر",
                "action":SidebarActions.block.rawValue
                
            ] ,
             [
                "image":"ic_wishlist_sidebar",
                "title":"مفضلتي",
                "action":SidebarActions.wishlist.rawValue
                
            ] ,
             [
                "image":"ic_invite_friends_sidebar",
                "title":"دعوة صديق",
                "action":SidebarActions.invite.rawValue
                
            ],
             //          [
            //            "image":"ic_settings_sidebar",
            //            "title":"الاعادادت",
            //            "action":SidebarActions.settings.rawValue
            //
            //        ],
            //          [
            //            "image":"ic_help_sidebar",
            //            "title":"مساعدة",
            //            "action":SidebarActions.help.rawValue
            //
            //        ],
            [
                "image":"ic_developer_sidebar",
                "title":"عن المطور",
                "action":SidebarActions.developers.rawValue
                
            ],
            [
                "image":"ic_logout_sidebar",
                "title":"تسجيل الخروج",
                "action":SidebarActions.logout.rawValue
                
            ],
        ]
        
        if Gate.is_show() {
            items.insert([
                "image":"ic_my_stores_sidebar",
                "title":"الفروع",
                "action":SidebarActions.branches.rawValue
                
                ], at: 2)
        }
        if !Gate.is_costumer() && Gate.is_authed(){
            items.insert( [
                "image":"ic_requests_sidebar",
                "title":"طلبات",
                "action":SidebarActions.requests.rawValue
                
                ], at: 3)
        }
        
        if !Gate.is_authed(){
            for _ in 0...3{
                items.remove(at: 1)
            }
            items[items.count - 1] = [
                "image":"ic_login",
                "title":"تسجيل الدخول",
                "action":SidebarActions.login.rawValue
            ]
            
        }
        
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controller:UIViewController!
        let nvc = (self.slideMenuController()?.mainViewController as! UINavigationController)
        
        slideMenuController()?.closeRight()
        
        switch items[indexPath.row]["action"]! {
        case SidebarActions.home.rawValue:
            nvc.popToRootViewController(animated: true)
            return
            
        case SidebarActions.profile.rawValue:
            controller = goToProfile(storyboard)
            
        case SidebarActions.block.rawValue:
            let blocked = Blocked_TableViewController(nibName: "Blocked_TableViewController", bundle: nil)
            controller = blocked
            break
            
        case SidebarActions.requests.rawValue:
            controller = goToProfile(storyboard,selectedItem: 4)
            break
            
        case SidebarActions.wishlist.rawValue:
            controller = goToProfile(storyboard)
            break
            
        case SidebarActions.search.rawValue:
            controller = Search_CollectionViewController(nibName: "Search_CollectionViewController", bundle: nil)
            break
            
        case SidebarActions.branches.rawValue:
            let locations = storyboard.instantiateViewController(withIdentifier: Storyboard_ids.locations.rawValue) as! Locations_ViewController
            locations.apiRequest = ApiRequest()
            locations.branch = UserDefaultsHelper.get_user_data()
            locations.get_branches = true
            controller = locations
            break
        case SidebarActions.login.rawValue:
            goToMain(storyboard, nvc)
            return
            
        case SidebarActions.developers.rawValue:
            let textOnly = TextOnly_ViewController(nibName: "TextOnly_ViewController", bundle: nil)
            textOnly.text = "وشم..محطة للتقنية و الالبداع..التطبيقات الالكترونية جزء من عملنا و كذلك برمجة و تصميم المواقع الالكترونية<<لنا بصمة حيثما يكون التصميم ، الابداع."
            controller = textOnly
        case SidebarActions.logout.rawValue:
            
            let alert = UIAlertController(title: "تسجيل الخروج", message: "هل انت متاكد من تسجيل الخروج؟", preferredStyle: .alert)
            alert.view.isOpaque = false
            alert.addAction(.init(title: "نعم", style: .destructive, handler:{
                _ in
                UserDefaultsHelper.logOut()
                self.goToMain(storyboard, nvc)
            }))
            alert.addAction(.init(title: "لا", style: .cancel, handler:nil))
            self.present(alert, animated: true, completion: nil)
            //
            return
        default:
            return
        }
        
        if nvc.viewControllers.count > 1 {
            nvc.viewControllers.removeLast()
        }
        nvc.pushViewController(controller, animated: true)
    }
    
    
    func goToMain(_ storyboard:UIStoryboard ,_ nvc:UINavigationController) {
        let home = storyboard.instantiateViewController(withIdentifier: Storyboard_ids.home.rawValue)
        UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: home)
        //        nvc.setViewControllers([home], animated: false)
        //        nvc.popToRootViewController(animated: true)
    }
    
    func goToProfile(_ storyboard:UIStoryboard,selectedItem:Int = 0)->UIViewController{
        let profile = storyboard.instantiateViewController(withIdentifier: Storyboard_ids.profile.rawValue) as! Profile_ViewController
        profile.user = UserDefaultsHelper.get_user_data()
        profile.selectedItem = selectedItem
        return profile
    }
    
}
