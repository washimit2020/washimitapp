//
//  Locations_ViewController.swift
//  reyadah
//
//  Created by WASHM on 5/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Locations_ViewController: UIViewController  {
  
    @IBOutlet weak var height: NSLayoutConstraint!
    let embed_segue = "tabBar"
    @IBOutlet weak var displayChanger: UISegmentedControl!

    var selected = 0
    var tabBar:LocationsTabBar_ViewController!
    var delegate:LocationsDelegate?
    var apiRequest:ApiRequest?
    var product:Product?
    var branch:Branch?
    
    var get_branches = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

        displayChanger.selectedSegmentIndex = selected
        tabBar.set_selectedViewController = selected
        displayChanger.addTarget(self, action: #selector(segmanted_change), for: .valueChanged)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  segue.identifier == embed_segue {
            tabBar = segue.destination as! LocationsTabBar_ViewController
            tabBar.apiRequest = apiRequest
            
            if branch != nil{
                tabBar.shared.append(branch!)
                if !get_branches{
                    tabBar.set_selectedViewController = 1
                    displayChanger.isHidden = true
                    height.constant = 0
                }
            }
            tabBar.get_branches = get_branches
            if  product != nil  {
                tabBar.product = product
            }
        }
    }
    
    func segmanted_change()  {
        tabBar.set_selectedViewController = displayChanger.selectedSegmentIndex
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let get_selectd = delegate?.get_selected_segmant{
            get_selectd(displayChanger.selectedSegmentIndex)
        }
    }
   
}


