package com.it.washm.alrayaduah.fragment;


import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.adapter.ViewPagerAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.model.MyNews;
import com.it.washm.alrayaduah.model.NewsType;

/**
 * A simple {@link Fragment} subclass.
 */
public class SubNewsFragment extends Fragment {
    NewsType newsType;
    List<MyNews> myNews;
    boolean IsNewsType;
    int x;
    CustamViewFont viewFont;

    public SubNewsFragment(NewsType newsType) {
        this.newsType = newsType;
        IsNewsType = true;
    }

    public SubNewsFragment(List<MyNews> myNews) {
        this.myNews = myNews;
        IsNewsType = false;
    }

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_accepted,
            R.drawable.ic_rejected,
            R.drawable.ic_pending,
    };


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container.clearDisappearingChildren();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_news, container, false);

        viewFont=new CustamViewFont(getActivity(),"Cairo-Regular.ttf");


        tabLayout = (TabLayout) view.findViewById(R.id.tablayout);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        if (IsNewsType) {
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);

        } else {
            setupViewPagerMyNews(viewPager);
            tabLayout.setupWithViewPager(viewPager);
            setupTabIcons();

        }

        // viewPager.setPageTransformer(true,new DepthPageTransformer());
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#FFFFFF"));

        changeTabsFont();

        return view;


    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());


        if (newsType.getSub().size() > 0) {
            for (int i = 0; i < newsType.getSub().size(); i++) {
                adapter.addFragment(SubNewsDetailsFragment.newInstance(newsType.getId(), newsType.getSub().get(i).getId()), newsType.getSub().get(i).getName_ar());
                //  tabLayout.setVisibility(View.VISIBLE);
            }
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        } else {
            adapter.addFragment(SubNewsDetailsFragment.newInstance(newsType.getId(), ""), newsType.getName_ar());
            //tabLayout.setVisibility(View.GONE);
            tabLayout.setTabMode(TabLayout.MODE_FIXED);

        }

        viewPager.setAdapter(adapter);
    }

    private void setupViewPagerMyNews(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());


        if (myNews.size() > 0) {
            for (int i = 0; i < myNews.size(); i++) {
                adapter.addFragment(MyNewsFragment.newInstance(myNews.get(i).getId()), myNews.get(i).getName_ar());

            }
            tabLayout.setTabMode(TabLayout.MODE_FIXED);

        }

        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {

        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);


    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
             viewFont.setfont((TextView) tabViewChild);


                }
            }
        }
    }


}
