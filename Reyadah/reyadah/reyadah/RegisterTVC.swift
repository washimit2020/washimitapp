//
//  RegisterTVC.swift
//  reyadah
//
//  Created by WASHM on 4/15/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import SwiftyJSON
import MapKit

class RegisterTVC: FormsTVC {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var nameError: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var infoText: UITextView!
    @IBOutlet weak var infoError: UILabel!
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var phoneError: UILabel!
    @IBOutlet weak var street: UITextField!
    @IBOutlet weak var streetError: UILabel!
    @IBOutlet weak var sector: UITextField!
    @IBOutlet weak var sectorError: UILabel!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var passwordConfirmation: UITextField!
    
    @IBOutlet weak var passwordConfirmationError: UILabel!
    
    
    var apiImages = [String:UIImage]()
    
    var user_type:Int?
    var user:User?
    var link = RequestLinks.register.rawValue
    let homeSegue = "login"
    var msg = "جاري انشاء الحساب"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerFields()
        configureView()
        
        email.text = postValues["email"]
        nameInput.text = postValues["name"]
        
        if let userData = user{
            nameInput.text = userData.name
            infoText.text = userData.info
            email.text = userData.email
            phone.text = userData.phone_number
            street.text = userData.street_name
            sector.text = userData.sector_name
            postValues["facebook"] = userData.socialMedia.facebook
            postValues["whatsapp"] = userData.socialMedia.whatsapp
            postValues["instagram"] = userData.socialMedia.instagram
            postValues["snapchat"] = userData.socialMedia.snapchat
            postValues["twitter"] = userData.socialMedia.twitter
            postValues["lat"] = userData.lat
            postValues["lng"] = userData.lng
            if let token = UserDefaultsHelper.get_social_token(){
                postValues["token"] = token
            }
            navigationItem.title = "تعديل الحساب"
            link = RequestLinks.updateProfile.rawValue
            msg = "جاري تعديل الحساب"
        }
        
    }
    
    
    
    
    private func configureView(){
        postValues["type_id"] = "\(user_type!)"
        
        var name:String
        var info:String
        
        switch user_type! {
        case Constants.TYPE_STORE:
            name = "اسم المتجر"
            info = "التعريف بالمتجر "
            break
        case Constants.TYPE_SHOW:
            name = "اسم المعرض"
            info = "التعريف بالمعرض"
            break
            
        default:
            name = "اسم المستخدم"
            info = "التعريف"
        }
        nameLabel.text = name
        infoLabel.text = info
    }
    
    private func registerFields(){
        fields = [
            (nameInput, nameError),(infoText ,infoError),(email,emailError),(phone,phoneError),(street,streetError),(sector,sectorError)
        ]
        
        user_type = UserDefaultsHelper.get_current_user_type()
        
        validator.registerField(nameInput, errorLabel: nameError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        validator.registerField(infoText, errorLabel: infoError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 500)])
        validator.registerField(email, errorLabel: emailError, rules: [RequiredRule(message: Constants.requiredErrorMsg), EmailRule(message: Constants.emailErrorMsg)])
        validator.registerField(phone, errorLabel: phoneError, rules: [RequiredRule(message: Constants.requiredErrorMsg), PhoneNumberRule(message: Constants.phoneErrorMsg)])
        validator.registerField(street, errorLabel: streetError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        validator.registerField(sector, errorLabel: sectorError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        
        if postValues["token"] == nil{
            fields?.append((password,passwordError))
            validator.registerField(password, errorLabel: passwordError, rules: [RequiredRule(message: Constants.requiredErrorMsg), MinLengthRule(length: 6, message: Constants.minLengthErrorMsg)])
        }
        
        if  user == nil && postValues["token"] == nil{
            fields?.append((passwordConfirmation,passwordConfirmationError))
            validator.registerField(passwordConfirmation, errorLabel: passwordConfirmationError, rules: [RequiredRule(message: Constants.requiredErrorMsg), ConfirmationRule(confirmField: password,message:Constants.confirmedErrorMsg)])
        }
        
        action = register(_:)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if user_type == Constants.TYPE_COSTUMER{
            if indexPath.row == 4 || indexPath.row == 5{
                return 0
            }
        }
        
        if indexPath.row == 1 {
            return 170
        }
        
        if postValues["token"] != nil && (indexPath.row == 7 || indexPath.row == 8){
           return 0
        }
        
        if user != nil && indexPath.row == 8{
            return 0
        }
        
        return 110
    }
    
    override func validationSuccessful() {
        postValues["email"] = email.text
        postValues["password"] = password.text
        postValues["password_confirmation"] = passwordConfirmation.text
        postValues["name"] = nameInput.text
        postValues["street_name"] = street.text
        postValues["sector_name"] = sector.text
        postValues["phone_number"] = phone.text
        postValues["info"] = infoText.text
        postValues["reg_id"] = "12321312321"
        
        if postValues["token"] == nil{
            postValues["login_type"] = "\(Constants.NORMAL_REGISTER)"
        }
        
        if let user = user{
            user.email = postValues["email"]
            user.name = postValues["name"]
            user.sector_name = postValues["sector_name"]
            user.street_name = postValues["street_name"]
            user.phone_number = postValues["phone_number"]
            user.info = postValues["info"]
            user.lat = postValues["lat"]
            user.lng = postValues["lng"]
            user.socialMedia.facebook = postValues["facebook"]
            user.socialMedia.twitter = postValues["twitter"]
            user.socialMedia.snapchat = postValues["snapchat"]
            user.socialMedia.instagram = postValues["instagram"]
            user.socialMedia.whatsapp = postValues["whatsapp"]
        }
        
        request.make_request(parameters: postValues,
                             apiImages: apiImages, link: link,
                             method: .post,
                             dialogMsg: msg,
                             completion: registerHandler)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  let controller = segue.destination as? AddLocation_ViewController {
            if let lattiude = postValues["lat"],
                let long = postValues["lng"],
                let lat = CLLocationDegrees(lattiude) ,
                let lng  = CLLocationDegrees(long ){
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                
                controller.current = coordinate
            }
            controller.userLocationCallBack = {
                annotaiton in
                if let annotation = annotaiton{
                    self.postValues["lat"] = "\(annotation.coordinate.latitude)"
                    self.postValues["lng"] = "\(annotation.coordinate.longitude)"
                }
            }
        }
    }
    
    
    @IBAction func register(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
    
    private func registerHandler(_ response:DataRequest?)->Void{
        
        guard let response = response else {
            self.request.handle_error_dialog(nil)
            return
        }
        response.responseJSON{
            response in
            if response.response?.statusCode == Constants.RESPONSE_CODE_UNPROCESSABLE{
                self.request.handle_error_dialog(JSON(response.data!)["msg"].stringValue)
            }
            else if response.response?.statusCode == 200{
                UserDefaultsHelper.store_user_data(response.data!)
                self.user = UserDefaultsHelper.get_user_data()
                
                if self.link == RequestLinks.register.rawValue{
                    self.request.costumeAlert.dismiss(animated: true, completion:{
                        UserDefaultsHelper.store_is_first()
                        make_sidebar()
                    })
                }else{
                    self.request.handle_error_dialog("تم التعديل بنجاح")
                }
                
            }
        }
        
    }
    
    @IBAction func addPhotos(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.image_adder_id) as! ImageAdder_ViewController
        
        
        if apiImages.count > 0 {
            for (key,image) in apiImages{
                if key == "avatar"{
                    controller.avatar = image
                }else{
                    controller.items.append(image)
                }
            }
        }
        
//        if let user = user{
//            controller.user_id = user.id
//               if controller.items.count == 0 {
//                controller.links = get_cover_links(id: user.id)
//            }
//        }
        
        
        
        
        controller.getImagesCallback = {
            (avatar , covers) in
            
            if let avatar = avatar {
                self.apiImages["avatar"] = avatar
            }
            if covers?.count == 0 {
                return
            }
            if let covers = covers {
                for i in 0...covers.count-1{
                    self.apiImages["cover[\(i)]"] = covers[i]
                }
            }
            
        }
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func showSocialDialog(_ sender: AspectFitButton) {
        let alert = UIAlertController(title: "قم بادخال الرابط او اسم المستخدم", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: {
            $0.text = self.postValues[sender.restorationIdentifier!]
        })
        alert.addAction(.init(title: "تم", style: .default, handler: {
            _ in
            self.postValues[sender.restorationIdentifier!] = alert.textFields![0].text!
        }))
        alert.addAction(.init(title: "الغاء", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
