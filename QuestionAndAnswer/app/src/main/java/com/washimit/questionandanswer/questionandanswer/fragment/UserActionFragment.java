package com.washimit.questionandanswer.questionandanswer.fragment;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.activity.MainActivity;
import com.washimit.questionandanswer.questionandanswer.adapter.UserActionAdapter;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.custumView.MyTextView;
import com.washimit.questionandanswer.questionandanswer.custumView.TextViewCustem;
import com.washimit.questionandanswer.questionandanswer.interfac.deletnotecallback;
import com.washimit.questionandanswer.questionandanswer.interfac.pageIdCallback;
import com.washimit.questionandanswer.questionandanswer.modle.Comment;
import com.washimit.questionandanswer.questionandanswer.modle.Question;
import com.washimit.questionandanswer.questionandanswer.modle.Sections;
import com.washimit.questionandanswer.questionandanswer.modle.wishiListed;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserActionFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    UserActionAdapter adapter;
    int type;
    ArrayList<Object> list;
    ImageView page_img_icon;
    TextViewCustem page_titel;
    Typeface tf;
    ApiRequest request;
    Gson gson;
    LinearLayout no_conection;
    MyButtonView retry;
    RelativeLayout user_action;
    MyTextView no_data;

    public UserActionFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static UserActionFragment newInstance(int type) {
        UserActionFragment fragment = new UserActionFragment();
        Bundle args = new Bundle();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            type = getArguments().getInt("type");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_action, container, false);
        request = new ApiRequest(getActivity());
        gson = new Gson();
        list = new ArrayList<>();
        tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/TheSansArab-Bold_0.ttf");
        recyclerView = (RecyclerView) view.findViewById(R.id.userAction);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.Swipe_refresh_layout);
        page_img_icon = (ImageView) view.findViewById(R.id.page_img_icon);
        page_titel = (TextViewCustem) view.findViewById(R.id.page_titel);
        no_data = (MyTextView) view.findViewById(R.id.no_data);
        no_conection = (LinearLayout) view.findViewById(R.id.no_conection);
        retry = (MyButtonView) view.findViewById(R.id.retry);
        user_action = (RelativeLayout) view.findViewById(R.id.user_action);


        page_titel.setTypeface(tf, 1);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        swipeRefreshLayout.setOnRefreshListener(this);
        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 1) {
                    //   list = bHelper.getListWishilsted();
                    page_img_icon.setBackgroundResource(R.drawable.ic_icon_sidebar_favorite);
                    page_titel.setText("مفضلتي");
                    getWishlist();
                } else if (type == 2) {
                    getComment();
                    page_img_icon.setBackgroundResource(R.drawable.ic_icon_note);
                    page_titel.setText("ملاحظاتي");

                } else if (type == 3) {
                    page_titel.setText("فهرست الكتاب");
                    getSections();
                }
            }
        });

        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);
                if (type == 1) {
                    //   list = bHelper.getListWishilsted();
                    page_img_icon.setBackgroundResource(R.drawable.ic_icon_sidebar_favorite);
                    page_titel.setText("مفضلتي");
                    getWishlist();
                } else if (type == 2) {
                    getComment();
                    page_img_icon.setBackgroundResource(R.drawable.ic_icon_note);
                    page_titel.setText("ملاحظاتي");

                } else if (type == 3) {
                    page_titel.setText("فهرست الكتاب");
                    getSections();
                }

            }
        });


        adapter = new UserActionAdapter(getActivity(), list);


        adapter.onpageIdCollback(new pageIdCallback() {
            @Override
            public void pageId(int pageId) {
                PageFragment pageFragment = new PageFragment();
                Bundle bundle = new Bundle();
                bundle.putString("id", pageId + "");
                pageFragment.setArguments(bundle);
                ((MainActivity) getActivity()).openFragment(pageFragment);
            }
        });
        adapter.onpageNotDeletCollback(new deletnotecallback() {
            @Override
            public void deletNote(int pageId) {
                deleteComment(pageId);
                Log.d("deleteComment", pageId + "");
            }
        });


        recyclerView.setAdapter(adapter);


        return view;
    }

    public void getSections() {
        request.getRequst("sections", new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject sections = null;

                Log.d("section", response);
                list.clear();
                jsonObject = new JSONObject(response);

                sections = jsonObject.getJSONObject("sections");

                list.addAll((Collection<Sections>) gson.fromJson(sections.getString("data"), new TypeToken<ArrayList<Sections>>() {
                }.getType()));

                adapter.notifyDataSetChanged();

                user_action.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (list.size() > 0) {
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void ErrorListener() {
                swipeRefreshLayout.setRefreshing(false);
                no_data.setVisibility(View.GONE);

                user_action.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
            }
        });
    }


    public void getComment() {
        request.getRequst("comment", new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject comment = null;

                Log.d("getComment", response);
                list.clear();

                jsonObject = new JSONObject(response);


                comment = jsonObject.getJSONObject("comment");

                list.addAll((Collection<Comment>) gson.fromJson(comment.getString("data"), new TypeToken<ArrayList<Comment>>() {
                }.getType()));

                adapter.notifyDataSetChanged();


                user_action.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (list.size() > 0) {
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void ErrorListener() {
                swipeRefreshLayout.setRefreshing(false);
                user_action.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.GONE);

            }
        });
    }

    public void getWishlist() {

        request.getRequst("wishlist", new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {

                JSONObject jsonObject = null;

                JSONObject wishlist = null;

                Log.d("getComment", response);
                list.clear();

                jsonObject = new JSONObject(response);

                wishlist = jsonObject.getJSONObject("question");

                list.addAll((Collection<Comment>) gson.fromJson(wishlist.getString("data"), new TypeToken<ArrayList<wishiListed>>() {
                }.getType()));

                adapter.notifyDataSetChanged();

                user_action.setVisibility(View.VISIBLE);
                no_conection.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                if (list.size() > 0) {
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void ErrorListener() {
                no_data.setVisibility(View.GONE);

                user_action.setVisibility(View.GONE);
                no_conection.setVisibility(View.VISIBLE);
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    public void deleteComment(int commentId) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("comment_id", commentId + "");
        request.setRequst("comment/delete", map, new RequestCallback() {
            @Override
            public void onResponse(String response) throws JSONException {
                Log.d("deleteComment", "sucsses");
            }

            @Override
            public void ErrorListener() {
                Log.d("deleteComment", "ErrorListener");

            }
        });
    }

    @Override
    public void onRefresh() {
        if (type == 1) {
            //   list = bHelper.getListWishilsted();
            page_img_icon.setBackgroundResource(R.drawable.ic_icon_sidebar_favorite);
            page_titel.setText("مفضلتي");
            getWishlist();
        } else if (type == 2) {
            getComment();
            page_img_icon.setBackgroundResource(R.drawable.ic_icon_note);
            page_titel.setText("ملاحظاتي");

        } else if (type == 3) {
            page_titel.setText("فهرست الكتاب");
            getSections();
        }

    }
}

