//
//  Gate.swift
//  reyadah
//
//  Created by WASHM on 4/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import Foundation

class Gate:NSObject {
    static func is_authed ()->Bool{
    
        return !UserDefaultsHelper.get_user_token().isEmpty
    }
    
    static func is_costumer()->Bool{
        return UserDefaultsHelper.get_current_user_type() == Constants.TYPE_COSTUMER
    }
    
    static func is_show()->Bool{
        return UserDefaultsHelper.get_current_user_type() == Constants.TYPE_SHOW
    }
}
