<?php
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 1/4/2017
 * Time: 1:37 PM
 */

namespace App\Http\Helpers;


class  Constants
{
    const ADMINISTRATOR = 'admin';
    const NEWSMAN = 'newsman';
    const WRITE_NEWS = 'write-news';
    // begin user_roles
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_NEWSMAN = 2;
    const USER_ROLE_COSTUMER = 3;
    //  end user_roles

    // begin user_attachment_types
    const USER_ATTACHMENT_PROFILE_IMAGE = 1;
    //  end user_attachment_types

    // begin news_main_types
    const NEWS_MAIN_TYPE_LOCAL = 1;
    const NEWS_MAIN_TYPE_INTERNATIONAL = 2;
    const NEWS_MAIN_TYPE_SPORT = 3;
    const NEWS_MAIN_TYPE_ALRADYH_PUBLISH = 4;
    const NEWS_MAIN_TYPE_ALRADYH_VOLUNTEERS = 5;
    const NEWS_MAIN_TYPE_ALRADYH_FEMALES = 6;
    const NEWS_MAIN_TYPE_ALRADYH_NETWORK = 7;
    //  end news_main_types

    // begin news_status
    const NEWS_STATUS_APPROVED = 1;
    const NEWS_STATUS_DISAPPROVED = 2;
    const NEWS_STATUS_PENDING = 3;
    //end news status

    // begin user_attachment_types
    const NEWS_ATTACHMENT_MAIN = 1;
    const NEWS_ATTACHMENT_SUB = 2;
    //  end user_attachment_types

}