package com.it.washim.bakkar.halper;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.example.jean.jcplayer.JCAudio;
import com.example.jean.jcplayer.JCPlayerExceptions.AudioListNullPointerException;
import com.example.jean.jcplayer.JCPlayerExceptions.AudioUrlInvalidException;
import com.example.jean.jcplayer.JCPlayerService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.MySoundAdapter;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.interfase.OnSoundCollBack;
import com.it.washim.bakkar.model.Sound;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by mohammad on 8/3/2017.
 */

public class MyPalyer extends LinearLayout implements JCPlayerService.JCPlayerServiceListener, View.OnClickListener, SeekBar.OnSeekBarChangeListener {

    private LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
    private TextView txtCurrentMusic;
    private ImageView btnPrev;
    private ImageView btnPlay;
    private List<JCAudio> playlist;
    ArrayList<Sound> list = new ArrayList<>();
    private ProgressBar progressBarPlayer;
    private RecyclerView recyclerView;
    private MyJCAudioPlayer jcAudioPlayer;
    private MySoundAdapter audioAdapter;
    private TextView txtDuration;
    private ImageView btnNext;
    private SeekBar seekBar;
    private TextView txtCurrentDuration;
    private OnSoundCollBack collBack;
    JCAudio JCAudio;
    private String URL;
    Gson gson = new Gson();
    ArrayList<Sound> sounds;

    public MyPalyer(Context context) {
        super(context);
        init();
    }

    public MyPalyer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyPalyer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    private void init() {
        inflate(getContext(), R.layout.view_jcplayer2, this);

        this.recyclerView = (RecyclerView) findViewById(R.id.JCAudioList);
        this.progressBarPlayer = (ProgressBar) findViewById(R.id.progress_bar_player);
        this.btnNext = (ImageView) findViewById(R.id.btn_next);
        this.btnPrev = (ImageView) findViewById(R.id.btn_prev);
        this.btnPlay = (ImageView) findViewById(R.id.btn_play);
        this.txtDuration = (TextView) findViewById(R.id.txt_total_duration);
        this.txtCurrentDuration = (TextView) findViewById(R.id.txt_current_duration);
        this.txtCurrentMusic = (TextView) findViewById(R.id.txt_current_music);
        this.seekBar = (SeekBar) findViewById(R.id.seek_bar);
        this.btnPlay.setTag(R.drawable.bakar_icon_play);

        btnNext.setOnClickListener(this);
        btnPrev.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);
    }

    /**
     * Initialize an anonymous playlist, but with a custom title for all
     *
     * @param urls List of urls strings
     */
    public void initWithTitlePlaylist(ArrayList<Sound> urls, String url) {
        JCAudio jcAudio;
        list.clear();
        list.addAll(urls);
        if (playlist == null) {
            playlist = new ArrayList<>();
        }
        playlist.clear();

        for (int i = 0; i < urls.size(); i++) {

            //   if (isUrlValid(Constant.URL + "attachments/" + (urls.get(i).attachments.size() > 0? urls.get(i).attachments.get(0).id:0))) {

            if (urls.get(i).link != null) {
                if (urls.get(i).link.media_url != null) {
                    if (isUrlValid(urls.get(i).link.media_url)) {
                        jcAudio = new JCAudio();
                        jcAudio.setId(i);
                        jcAudio.setPosition(i);
                        //  jcAudio.setUrl(Constant.URL + "attachments/" + (urls.get(i).attachments.size() > 0? urls.get(i).attachments.get(0).id:0));
                        jcAudio.setUrl(urls.get(i).link.media_url);
                        //     jcAudio.setUrl("http://s2.voscast.com:10444/");
                        playlist.add(jcAudio);

                        generateTitleAudio(urls.get(i).title, i);
                    } else {
                        try {
                            throw new AudioUrlInvalidException(Constant.URL + "attachments/" + (urls.get(i).attachments.size() > 0 ? urls.get(i).attachments.get(0).id : 0));
                        } catch (AudioUrlInvalidException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        jcAudioPlayer = new MyJCAudioPlayer(getContext(), playlist, this);
        adapterSetup(url);
    }


    /**
     * Initialize an anonymous playlist, but with a custom title for all
     *
     * @param title Audio title
     * @param url   List of urls strings
     */
    public void addAudio(String title, String url) {
        if (isUrlValid(url)) {
            if (playlist == null)
                playlist = new ArrayList<>();

            int lastPosition = playlist.size();
            playlist.add(lastPosition,
                    new JCAudio(url, title, /* id */ lastPosition + 1, /* position */ lastPosition + 1));

            if (jcAudioPlayer == null)
                jcAudioPlayer = new MyJCAudioPlayer(getContext(), playlist, this);
            //   adapterSetup();
        } else {
            try {
                throw new AudioUrlInvalidException(url);
            } catch (AudioUrlInvalidException e) {
                e.printStackTrace();
            }
        }
    }


    private void generateTitleAudio(String title, int position) {
        playlist.get(position).setTitle(title);
    }

    private boolean isUrlValid(String url) {
        return url.startsWith("http") || url.startsWith("https");
    }

    protected void adapterSetup(String nextUrl) {
        recyclerView.setLayoutManager(layoutManager);
        audioAdapter = new MySoundAdapter(this, list, playlist, recyclerView);

        audioAdapter.setUrl(nextUrl);
        URL = nextUrl;
        audioAdapter.addOnLoadMorLisenar(new OnLoadMorCollBack() {
            @Override
            public void onLoad() {
//
//                playlist.add(null);
//                audioAdapter.notifyItemInserted(playlist.size());
                getNextSound();


            }
        });

        recyclerView.setAdapter(audioAdapter);

    }

    @Override
    public void onClick(View view) {
        if (playlist != null)
            if (view.getId() == R.id.btn_play) {
                YoYo.with(Techniques.Pulse)
                        .duration(200)
                        .playOn(btnPlay);

                if (btnPlay.getTag().equals(R.drawable.bakar_icon_pause))
                    pause();
                else
                    continueAudio();
            }

        if (view.getId() == R.id.btn_next) {
            YoYo.with(Techniques.Pulse)
                    .duration(200)
                    .playOn(btnNext);
            next();
        }

        if (view.getId() == R.id.btn_prev) {
            YoYo.with(Techniques.Pulse)
                    .duration(200)
                    .playOn(btnPrev);
            previous();
        }
    }

    public void setOnSoundCall(OnSoundCollBack collBack) {
        this.collBack = collBack;
    }

    public void playAudio(JCAudio JCAudio) {
        this.JCAudio = JCAudio;
        showProgressBar();
        try {

            jcAudioPlayer.playAudio(JCAudio);
            audioAdapter.play(jcAudioPlayer.getCarnt().getPosition());
        } catch (AudioListNullPointerException e) {
            dismissProgressBar();
            e.printStackTrace();
        }
    }

    public JCAudio getJCAudio() {
        return JCAudio;

    }

    private void next() {
        resetPlayerInfo();
        showProgressBar();
        try {
            jcAudioPlayer.nextAudio();
            audioAdapter.play(jcAudioPlayer.getCarnt().getPosition());

        } catch (AudioListNullPointerException e) {
            dismissProgressBar();
            e.printStackTrace();
        }
    }

    public void continueAudio() {
        showProgressBar();

        try {
            if (jcAudioPlayer.getCarnt() != null) {
                audioAdapter.play(jcAudioPlayer.getCarnt().getPosition());

                jcAudioPlayer.continueAudio();
            }
        } catch (AudioListNullPointerException e) {
            dismissProgressBar();
            e.printStackTrace();
        }
    }

    public void pause() {
        jcAudioPlayer.pauseAudio();

        audioAdapter.pause();

    }

    public void stop() {
        resetPlayerInfo();
        if (jcAudioPlayer != null) {
            if (jcAudioPlayer.jcPlayerService != null) {
                jcAudioPlayer.jcPlayerService.stop();
//            jcAudioPlayer.kill();

                btnPlay.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.bakar_icon_play, null));
                btnPlay.setTag(R.drawable.bakar_icon_play);
            }
        }
        if (audioAdapter != null)
            audioAdapter.pause();
    }

    private void previous() {
        resetPlayerInfo();
        showProgressBar();

        try {
            jcAudioPlayer.previousAudio();
            audioAdapter.play(jcAudioPlayer.getCarnt().getPosition());

        } catch (AudioListNullPointerException e) {
            dismissProgressBar();
            e.printStackTrace();
        }

    }

    public boolean isPlay() {
        return jcAudioPlayer.isPlaying();
    }

    public boolean isPaused() {
        return jcAudioPlayer.isPaused();
    }

    @Override
    public void onPreparedAudio(String audioName, int duration) {
        dismissProgressBar();

        long aux = duration / 1000;
        int minute = (int) (aux / 60);
        int second = (int) (aux % 60);

        final String sDuration =
                // Minutes
                (minute < 10 ? "0" + minute : minute + "")
                        + ":" +
                        // Seconds
                        (second < 10 ? "0" + second : second + "");

        seekBar.setMax(duration);

        txtDuration.post(new Runnable() {
            @Override
            public void run() {
                txtDuration.setText(sDuration);
            }
        });
    }

    @Override
    public void onCompletedAudio() {
        resetPlayerInfo();

        try {
            jcAudioPlayer.nextAudio();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetPlayerInfo() {
        seekBar.setProgress(0);
        txtCurrentMusic.setText("");
        txtCurrentDuration.setText("00:00");
        txtDuration.setText("00:00");
    }

    private void showProgressBar() {
        progressBarPlayer.setVisibility(ProgressBar.VISIBLE);
        btnPlay.setVisibility(Button.GONE);
        btnNext.setClickable(false);
        btnPrev.setClickable(false);

    }

    private void dismissProgressBar() {
        progressBarPlayer.setVisibility(ProgressBar.GONE);
        btnPlay.setVisibility(Button.VISIBLE);
        btnNext.setClickable(true);
        btnPrev.setClickable(true);
    }

    @Override
    public void onPaused() {
        btnPlay.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.bakar_icon_play, null));
        btnPlay.setTag(R.drawable.bakar_icon_play);
    }

    @Override
    public void onContinueAudio() {
        dismissProgressBar();
    }

    @Override
    public void onPlaying() {
        btnPlay.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.bakar_icon_pause, null));
        btnPlay.setTag(R.drawable.bakar_icon_pause);
    }

    @Override
    public void onTimeChanged(long currentPosition) {
        long aux = currentPosition / 1000;
        int minutes = (int) (aux / 60);
        int seconds = (int) (aux % 60);
        final String sMinutes = minutes < 10 ? "0" + minutes : minutes + "";
        final String sSeconds = seconds < 10 ? "0" + seconds : seconds + "";

        seekBar.setProgress((int) currentPosition);
        txtCurrentDuration.post(new Runnable() {
            @Override
            public void run() {
                txtCurrentDuration.setText(String.valueOf(sMinutes + ":" + sSeconds));
            }
        });
    }

    @Override
    public void updateTitle(String title) {
        final String mTitle = title;

        YoYo.with(Techniques.FadeInLeft)
                .duration(600)
                .playOn(txtCurrentMusic);

        txtCurrentMusic.post(new Runnable() {
            @Override
            public void run() {
                txtCurrentMusic.setText(mTitle);
            }
        });
    }

    /**
     * Create a notification player with same playlist with a custom icon.
     *
     * @param iconResource icon path.
     */
    public void createNotification(int iconResource) {
        if (jcAudioPlayer != null)
            jcAudioPlayer.createNewNotification(iconResource);
    }

    /**
     * Create a notification player with same playlist with a default icon
     */
    public void createNotification() {
        int iconResource = R.drawable.ic_notification_default;
        if (jcAudioPlayer != null)
            jcAudioPlayer.createNewNotification(iconResource);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean fromUser) {
        if (fromUser)
            jcAudioPlayer.seekTo(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        showProgressBar();
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        dismissProgressBar();
    }

    private void getNextSound() {

        ApiRequest.getInstance(getContext()).stringRequest(Request.Method.GET, getContext(), URL.replace(Constant.URL, ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {

//                playlist.remove(playlist.size());
//                audioAdapter.notifyItemInserted(playlist.size()-1);
                JSONObject object;
                JSONObject posts;
                object = new JSONObject(response);
                posts = object.getJSONObject("posts");
                if (sounds == null) {
                    sounds = new ArrayList<Sound>();
                }
                sounds.clear();
                sounds.addAll((Collection<? extends Sound>) gson.fromJson(posts.getString("data"), new TypeToken<ArrayList<Sound>>() {
                }.getType()));


                URL = posts.getString("next_page_url");
                JCAudio jcAudio;


                for (int i = list.size(); i < sounds.size() + list.size(); i++) {


                    if (sounds.get(i - list.size()).link != null) {
                        if (sounds.get(i - list.size()).link.media_url != null) {
                            if (isUrlValid(sounds.get(i - list.size()).link.media_url)) {
                                jcAudio = new JCAudio();
                                jcAudio.setId(i);
                                jcAudio.setPosition(i);
                                jcAudio.setUrl(sounds.get(i - list.size()).link.media_url);
                                playlist.add(jcAudio);
                                generateTitleAudio(sounds.get(i - list.size()).title, i);
                                audioAdapter.notifyItemInserted(playlist.size());

                            } else {
                                try {
                                    throw new AudioUrlInvalidException("");
                                } catch (AudioUrlInvalidException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }
                audioAdapter.isLoading(false);

                list.addAll(sounds);

                //     initWithTitleNextPlaylist(list, posts.getString("next_page_url"));


            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


    }

}
