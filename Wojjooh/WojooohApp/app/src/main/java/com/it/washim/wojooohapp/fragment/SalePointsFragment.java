package com.it.washim.wojooohapp.fragment;


import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.PagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SalePointsFragment extends Fragment {


    PagerAdapter adapter;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.table_layout)
    TabLayout layout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sale_points, container, false);
        ButterKnife.bind(this, view);
        adapter = new PagerAdapter(getFragmentManager());
        layout.setupWithViewPager(pager);

        adapter.addPage(new SalePointsListFragment(), "اللائحه");
        adapter.addPage(new MapsFragment(), "الخريطة");

        pager.setOffscreenPageLimit(1);
        pager.setAdapter(adapter);


        return view;
    }

}
