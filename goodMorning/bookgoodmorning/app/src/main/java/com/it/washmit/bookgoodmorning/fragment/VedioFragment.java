package com.it.washmit.bookgoodmorning.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.it.washmit.bookgoodmorning.R;
import com.it.washmit.bookgoodmorning.adapter.BolgAdapter;
import com.it.washmit.bookgoodmorning.adapter.VedioAdapter;
import com.it.washmit.bookgoodmorning.coustumView.TextViewCustem;
import com.it.washmit.bookgoodmorning.helper.ApiRequest;
import com.it.washmit.bookgoodmorning.interfac.OnRequestCollBack;
import com.it.washmit.bookgoodmorning.model.Post;
import com.it.washmit.bookgoodmorning.model.Vedio;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A simple {@link Fragment} subclass.
 */
public class VedioFragment extends Fragment  {


    ArrayList<Vedio> vedios;
    RecyclerView recyclerView;
    TextViewCustem no_net;
    VedioAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vedio, container, false);

        vedios = new ArrayList<>();
        adapter = new VedioAdapter(getActivity(), vedios);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        no_net = (TextViewCustem) view.findViewById(R.id.no_net);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        recyclerView.setAdapter(adapter);

                get();


        return view;
    }

    public void get() {
        vedios.add(new Vedio("maxresdefault","https://www.youtube.com/watch?v=Z3CjLCzRlMk"));
        vedios.add(new Vedio("maxresdefault2","https://www.youtube.com/watch?v=rgNXzgmZUhw&feature=youtu.be"));
        adapter.notifyDataSetChanged();

    }

}
