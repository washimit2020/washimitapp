<?php
/**
 * Created by PhpStorm.
 * User: mohammad
 * Date: 1/25/2017
 * Time: 10:31 AM
 */

namespace App\Channels;


class FCM_Message
{
    public $url = 'https://fcm.googleapis.com/fcm/send';
    const SERVER_KEY = 'AAAAdWpm0oQ:APA91bHaG7l6BEcvSeamI2-EU_eh9EY-II7JhnbMFmjSmMPZ1Y23RVvd1Z3_FNUvMU8WCb2VeJSPd47Iho_G6qtObTaN-cSjTJQfbP0_wN-5HgbZwFHtDN15c64hAIN9ESe5owBmnD1y';
    /**
     *
     * @var array
     */

    public $headers = ['Authorization: key='.self::SERVER_KEY,'Content-Type: application/json'];



    /**
     * The recipient information for the message.
     *
     * @var array
     */
    public $title = "";

    /**
     * The message data .
     *
     * @var string
     */
    public $body = "";

    /**
     * The message data .
     *
     * @var array
     */
    public $reg_id = [];

    /**
     * @param array $reg_id
     */
    public function add_reg($reg_id)
    {
        array_push($this->reg_id,$reg_id);
    }

    /**
     * The message data .
     *
     * @var array
     */
    public $data ;



    /**
     * Set the recipient address for the mail message.
     *
     * @param  string|array $reg_id
     * @return $this
     */
    public function to($reg_id)
    {
        $this->to = $reg_id;

        return $this;
    }


    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * @param string $message
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * Get the data array for the mail message.
     *
     * @return array
     */
    public function msg_data()
    {
        return $this->data;
    }
}