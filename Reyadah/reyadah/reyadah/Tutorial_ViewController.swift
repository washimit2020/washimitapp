//
//  Tutorial_ViewController.swift
//  reyadah
//
//  Created by WASHM on 5/29/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Tutorial_ViewController: DialogBase_ViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var container: UIView!
    
    var images = [UIImage(named:"ic_dialogue_store"),UIImage(named:"ic_dialogue_showroom"),UIImage(named:"ic_dialogue_festival")]
    var labels = ["هنا يمكنك مشاهدة المتاجر",
                  "هنا يمكنك مشاهدة المعارض",
                 "هنا يمكنك مشاهدة المهرجانات"]
    var selected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        container.layer.borderColor = UIColor.black.cgColor
        container.layer.borderWidth = 1
        container.layer.cornerRadius = 10
        
        label.text = labels[selected]
        image.image = images[selected]
    }

   
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true,completion: nil)
    }

}
