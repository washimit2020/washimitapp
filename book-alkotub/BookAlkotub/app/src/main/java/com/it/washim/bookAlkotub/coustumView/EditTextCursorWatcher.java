package com.it.washim.bookAlkotub.coustumView;

/**
 * Created by washm on 4/1/17.
 */


import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import com.it.washim.bookAlkotub.interfac.onselectChangecallback;

public class EditTextCursorWatcher extends EditText {

    private onselectChangecallback change;

    public EditTextCursorWatcher(Context context, AttributeSet attrs,
                                 int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    public EditTextCursorWatcher(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public EditTextCursorWatcher(Context context) {
        super(context);
        init();

    }

public void selectChange(onselectChangecallback change){
    this.change=change;
}
    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
//        Toast.makeText(getContext(), "selStart is " + selStart + "selEnd is " + selEnd+getText().charAt(0), Toast.LENGTH_LONG).show();

        if(change!=null)
        change.selectChange(selStart,selEnd);
    }


    public  void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "LotusLinotype-Light.otf");

        setTypeface(tf, 1);
    }

}