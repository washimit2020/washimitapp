package com.washimit.it.ryadah.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.washimit.it.ryadah.model.fragment.fragmentPage;

import java.util.ArrayList;

/**
 * Created by mohammad on 4/15/2017.
 */

public class fragmentMangerAdapter extends FragmentStatePagerAdapter {
    ArrayList<fragmentPage> fragmentArrayList;

    public fragmentMangerAdapter(FragmentManager fm) {
        super(fm);
        fragmentArrayList = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentArrayList.get(position).getFragment();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return fragmentArrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentArrayList.get(position).getTitle();
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentArrayList.add(new fragmentPage(fragment, title));
    }
}
