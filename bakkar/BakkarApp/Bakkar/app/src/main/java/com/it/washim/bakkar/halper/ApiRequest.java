package com.it.washim.bakkar.halper;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.it.washim.bakkar.interfase.OnRequestCollBack;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mohammad on 8/7/2017.
 */

public class ApiRequest {
    private static RequestQueue queue;
    private static ApiRequest request = new ApiRequest();

    public static ApiRequest getInstance(Context context) {
        if (queue == null) queue = Volley.newRequestQueue(context);
        return request;
    }

    public ApiRequest() {

    }

    public void stringRequest(int method, final Context context, String url, @Nullable final HashMap<String, String> params, final OnRequestCollBack collBack) {
        StringRequest request = new StringRequest(method, Constant.URL + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    collBack.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null)
                    collBack.onErrorResponse(error);

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("app-key", "aYEqaU3O9BoXsvrmuHYf5WPfZ4IuOC4N");
                if (SharedPref.getSharedInstance(context).getUser() != null) {
                    headers.put("Authorization", "Bearer " + SharedPref.getSharedInstance(context).getUser().api_token);
                }
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(request);


    }


}
