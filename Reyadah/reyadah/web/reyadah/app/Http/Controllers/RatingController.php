<?php

namespace App\Http\Controllers;

use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\__ModelsRating\FestivalRating;
use App\__ModelsRating\PlaceRating;
use App\__ModelsRating\ProductRating;
use App\Http\Constants;
use App\Http\Requests\Ratings\FestivalRatingRequest;
use App\Http\Requests\Ratings\PlaceRatingRequest;
use App\User;
use Illuminate\Support\Facades\DB;

class RatingController extends Controller
{

    /**
     * to add user ratings
     *
     * @param PlaceRatingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(PlaceRatingRequest $request)
    {

        $rating = new PlaceRating();

        $data = $request->only($rating->getFillable());

        $data['related_id'] = $data['user_id'];

        $data['user_id'] = auth()->user()->id;

        abort_if_blocked(User::find($data['related_id']));

        $rating = PlaceRating::updateOrCreate([['related_id', $data['related_id']],
            ['user_id', $data['user_id']]], $data);

        $place = $rating->related;

        $new_rating = $this->calculate_rating($place, ['view_rating', 'products_rating', 'users_rating', 'prices_rating']);

        $place->rating = $new_rating;

        $place->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['rating' => $place->rating]));
    }

    /**
     * to add festival ratings
     *
     * @param FestivalRatingRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function festival(FestivalRatingRequest $request)
    {

        $rating = new FestivalRating();

        $data = $request->only($rating->getFillable());


        $data['user_id'] = auth()->user()->id;

        abort_if_blocked(Festival::find($data['festival_id'])->user);


        $rating = FestivalRating::updateOrCreate([
            ['festival_id', $data['festival_id']],
            ['user_id', $data['user_id']
            ]], $data);

        $festival = $rating->festival;

        $new_rating = $this->calculate_rating($festival, ['view_rating', 'products_rating', 'users_rating', 'prices_rating']);

        $festival->rating = $new_rating;

        $festival->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['rating' => $festival->rating]));
    }



    /**
     * to add user ratings
     *
     * @param \App\Http\Requests\Ratings\ProductRating $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function product(\App\Http\Requests\Ratings\ProductRating $request)
    {

        $data = $request->only(( new ProductRating)->getFillable());

        $data ["user_id"] = $request->user()->id;
        abort_if_blocked(Product::find($data['product_id'])->user);

        $rating = ProductRating::updateOrCreate([
            ['product_id', $request->product_id],
            ['user_id',$request->user()->id]
            ], $data);

        $place = $rating->product;

        $new_rating = $this->calculate_rating($place, ['quality', 'price', 'style', 'ingredients']);

        $place->rating = $new_rating;

        $place->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['rating' => $place->rating]));
    }

    /**
     * pass model and get the new rating of the model
     *
     * @param $model
     * @param array $cols
     * @return float|int
     */
    private function calculate_rating($model, $cols = [])
    {

        $new_rating = $model->ratings()
            ->select(DB::raw(' SUM(' . implode('+', $cols) . ')/' . count($cols) . ' as total , COUNT(*) as count'))
            ->first();

        if ($new_rating->count == 0) {
            $new_rating = 0;
        } else {
            $new_rating = $new_rating->total / $new_rating->count;
        }

        return $new_rating;
    }
}
