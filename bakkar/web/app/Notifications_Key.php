<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notifications_Key extends Model
{
    protected $fillable = [
        "id","key","name","registration_ids"
    ];

    protected $casts = [
      'registration_ids'=>'array'
    ];

}