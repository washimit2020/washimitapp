//
//  Page_TableViewController.swift
//  al-haj
//
//  Created by WASHM on 7/26/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite

class Page_TableViewController: UITableViewController {
    
    @IBOutlet weak var pageTitle: C_UILabel!
    @IBOutlet weak var ref: UILabel!
    @IBOutlet weak var titleMargin: NSLayoutConstraint!
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var pageRefs: C_UILabel!
    @IBOutlet weak var pageBody: C_UITextView!
    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var left: NSLayoutConstraint!
    
    var colors:[UIColor] = [.red , .yellow , .cyan , .white] {
        didSet{
            setup_buttons()
        }
    }
    var buttons:UIStackView!
    
    var page:Page!
    var currentPage:Int?
    var attributedBody:NSMutableAttributedString?{
        didSet{
            self.pageBody.attributedText = self.attributedBody
            self.pageBody.textAlignment = .right
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44
        left.constant = page.is_full_line == 0 ? tableView.frame.width/2 : 0
        pageTitle.text = nil
        if let title = page?.title{
            pageTitle.text = title
            titleMargin.constant = 0
        }else{
            titleMargin.constant = -30
        }
        pageRefs.text = page?.references
        pageBody.delegate = self
        pageBody.isScrollEnabled = false
//        readPage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // to reset the sidebar selected id
        pageNumber.text = "\(currentPage ?? 0)"
        setups()
    }
    
    
    func setups(){
        let sizes = Utils.shared.get_font_sizes()
        let fontName = Utils.shared.get_font_name()
        let nightMode = Utils.shared.is_dark_mode()
        let fontColor = nightMode ? Utils.shared.dark_mode_font : Utils.shared.light_mode_font
        pageTitle.font = UIFont(name: "TheSansArab-Plain", size: CGFloat(sizes[0]))
        pageRefs.font = UIFont(name: fontName, size: CGFloat(sizes[2]))
        //        pageTitle.font = UIFont(name: fontName, size: CGFloat(sizes[0]))
        tableView.backgroundColor = nightMode ? Utils.shared.dark_mode_background : Utils.shared.light_mode_background
        pageTitle.textColor = nightMode ? Utils.shared.dark_mode_header : Utils.shared.light_mode_header
        setup_images()
        check_if_highlighted()
      
        self.attributedBody = self.page.body!
        pageRefs.textColor = fontColor
        ref.backgroundColor = fontColor
        if nightMode {
            colors[3] = .black
        }else{
            colors[3] = .white
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if imageView == nil , indexPath.row == 1{
            return 0
        }
        if (pageRefs.text == nil || pageRefs.text!.isClear()) , indexPath.row == 3{
            return 0
        }
        return UITableViewAutomaticDimension
    }
    
    private func readPage(){
        guard let page = page  else {return}
        page.is_read = 1
        let index = Utils.shared.pages.index { (item) -> Bool in
            return item.id == page.id
        }
        Utils.shared.save_page(page: page)
        Utils.shared.pages[index!] = page
        DispatchQueue.global(qos: .background).async {
            do{
                let pages_t = DatabaseUtils.shared.pages_t.filter(Expressions.id == page.id!)
                try DatabaseUtils.shared.db!.run(pages_t.update(
                    Expressions.is_read <- 1
                ))
            }catch{
                print(error)
            }
        }
    }
   
    @objc public func highlight(_ sender: UIButton) {
        guard let attributed = attributedBody , pageBody.selectedRange.length > 0 else {
            return
        }
        attributed.addAttribute(NSAttributedStringKey.backgroundColor, value: sender.backgroundColor!, range: pageBody.selectedRange)
        if  Utils.shared.is_dark_mode() {
            if  sender.backgroundColor != .black {
                self.attributedBody!.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: pageBody.selectedRange)
            }else{
                self.attributedBody!.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: pageBody.selectedRange)
            }
        }
        do {
            let highlighted = DatabaseUtils.shared.highlights_t.filter(
                Expressions.page_id == page.id! &&
                    Expressions.start >= pageBody.selectedRange.location &&
                    Expressions.end <= pageBody.selectedRange.length
            )
            try DatabaseUtils.shared.db!.run(highlighted.delete())
            if (!Utils.shared.is_dark_mode() && sender.backgroundColor != .white)  || (Utils.shared.is_dark_mode() && sender.backgroundColor != .black) {
                try DatabaseUtils.shared.db!.run(DatabaseUtils.shared.highlights_t.insert(
                    Expressions.page_id <- page.id!,
                    Expressions.start <- pageBody.selectedRange.location,
                    Expressions.end <- pageBody.selectedRange.length,
                    Expressions.color <- sender.tag
                ))
            }
        }catch(let exp){
            print("highlighted exp: \(exp)")
        }
        pageBody.attributedText = attributed
        pageBody.textAlignment = .right
    }
}

extension Page_TableViewController{
    func check_if_highlighted(){
        DispatchQueue.global(qos: .userInitiated).async {
            do {
                let table = DatabaseUtils.shared.highlights_t.filter(
                    Expressions.page_id == self.page.id!
                )
                let attributed = self.page.body!
                let highlighted = try Array( DatabaseUtils.shared.db!.prepare(table))
                if highlighted.count > 0 {
                    for highlight in highlighted{
                        let color = self.colors[highlight[Expressions.color]]
                        let range = NSRange(location: highlight[Expressions.start], length: highlight[Expressions.end])
                        attributed.addAttribute(NSAttributedStringKey.backgroundColor, value: color, range: range)
                        if  Utils.shared.is_dark_mode(){
                            if  color != .black {
                                attributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black, range: range)
                            }else{
                                attributed.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: range)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.attributedBody = attributed
                }
            }catch(let exp){
                print("highlighted selecte exp: \(exp)")
            }
        }
    }
    
    func setup_buttons(){
        let nightMode = Utils.shared.is_dark_mode()
        if let buttons = buttons{
            buttons.removeFromSuperview()
        }
        buttons = UIStackView(frame: .init(x: 40, y: 70, width: 40, height:  48 * colors.count))
        buttons.tag = 99
        buttons.spacing = 8
        buttons.axis = .vertical
        buttons.distribution = .fillEqually
        for (key,color) in colors.enumerated(){
            let button = UIButton()
            button.frame.size.width = buttons.frame.width
            button.frame.size.height = 40
            button.layer.cornerRadius = 20
            if nightMode{
                button.layer.shadowColor = UIColor.lightGray.cgColor;
            }else{
                button.layer.shadowColor = UIColor.black.cgColor;
            }
            button.layer.shadowOpacity = 0.8;
            button.layer.shadowRadius = 4;
            button.layer.shadowOffset = CGSize(width: 3, height: 3);
            button.addTarget(self, action: #selector(highlight(_:)), for: .touchUpInside)
            button.backgroundColor = color
            button.tag = key
            buttons.addArrangedSubview(button)
        }
        buttons.sizeToFit()
        buttons.isHidden = true
        self.view.addSubview(buttons)
    }
    
    func setup_images(){
        if  self.imageView == nil || self.imageView.image != nil {
            return
        }
        if  page.image?.title == nil{
            self.imageView.isHidden = true
            self.imageView.removeFromSuperview()
            return
        }else{
            self.imageView.image = page.image?.get_uiimage()
            self.tableView.reloadData()
        }
//        DispatchQueue.global().async {
//            do{
//                let images = try DatabaseUtils.shared.db!.prepare(DatabaseUtils.shared.images_t.filter(Expressions.page_id == self.page.id!)).map({
//                    return Image($0)
//                })
//                DispatchQueue.main.async {
//                    if images.count == 0 {
//                        self.imageView.isHidden = true
//                        self.imageView.removeFromSuperview()
//                        return
//                    }else{
//                        self.imageView.image = images.first?.get_uiimage()
//                        self.tableView.reloadData()
//                    }
//                }
        
                //                }else{
                //                for (key,image) in images.enumerated(){
                //                    var multiplier = CGFloat(key)
                //                    var contentMultiplier = multiplier + 1
                //                    if(images.count == 0){
                //                        multiplier += 1
                //                        contentMultiplier -= 1
                //                    }
                //                    let xPos = 190 *  multiplier
                //                    let view  = UIImageView(frame: .init(x: xPos, y: 0, width: 200, height: 200))
                //                    view.image = image.get_uiimage()
                //                    view.contentMode = .scaleAspectFill
                //                    view.clipsToBounds = true
                //                    imageScroll.contentSize.width = 200 * contentMultiplier
                //                    imageScroll.addSubview(view)
                //                }
                //
                //                if images.count > 1{
                //                    imageScroll.setContentOffset(.init(x: imageScroll.contentSize.width - imageScroll.frame.width + 48 , y: 0), animated: false)
                //                }
                //                }
                
//            }catch(let exp){
//                print("images \(exp)")
//            }
//        }
        
    }
    
}

extension Page_TableViewController : UITextViewDelegate{
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        guard let buttons = buttons else {
            return
        }
        let length = textView.selectedRange.length
        if length <= 0 {
            buttons.isHidden = true
            return
        }
        self.buttons.transform = CGAffineTransform(translationX: 0, y: self.tableView.contentOffset.y);
        
        self.buttons.isHidden = false
        
    }
    
}
