//
//  Comment.swift
//  reyadah
//
//  Created by WASHM on 4/29/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//


class Comment: Model {
    var id :Int!
    var commentable_id :Int!
    var commentable_type:String!
    var body:String!
    var user:User?
    
    override func map() {
        
        id = json["id"].intValue
        commentable_id = json["commentable_id"].intValue
        commentable_type = json["commentable_type"].stringValue
        body = json["body"].stringValue
        user = User(json["user"])
    }
}

