package com.it.washim.bakkar.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
//import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.NavBarAdapter;
import com.it.washim.bakkar.fragment.AgentsAndShowsFragment;
import com.it.washim.bakkar.fragment.ArticlesAndVideoFragment;
import com.it.washim.bakkar.fragment.BiographyAndCareerFragment;
import com.it.washim.bakkar.fragment.BooksFragment;
import com.it.washim.bakkar.fragment.CommunictionFragment;
//import com.it.washim.bakkar.fragment.SoundFragment;
import com.it.washim.bakkar.fragment.DeveloperFragment;
import com.it.washim.bakkar.fragment.NotificationFragment;
import com.it.washim.bakkar.fragment.SearchFragment;
import com.it.washim.bakkar.fragment.SoundFragment;
import com.it.washim.bakkar.fragment.VideoFragment;
import com.it.washim.bakkar.fragment.WishListedFragment;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.NavItem;

import org.json.JSONException;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    int pageType;
    @BindView(R.id.menu)
    ImageView menu;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.recycler_nav_bar)
    RecyclerView recycler_nav_bar;
    private NavBarAdapter adapter;
    ArrayList<NavItem> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        list = new ArrayList<>();
        adapter = new NavBarAdapter(this, list);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.d("aslkfjalkfja", "Refreshed token: " + refreshedToken);
        notification();

        ShortcutBadger.removeCount(this);
        shardPref().setBadger(0);

        recycler_nav_bar.setHasFixedSize(true);
        recycler_nav_bar.setLayoutManager(new LinearLayoutManager(this));
        recycler_nav_bar.setAdapter(adapter);

        if (getIntent().getExtras() != null) {
            pageType = getIntent().getExtras().getInt("page_type");
            openPage(pageType);
        }
        menu.setOnClickListener(this);
        back.setOnClickListener(this);


        addItemToNavBar();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public void openPage(int pageId) {
        if (pageId == Constant.ARTICLES) {
            Bundle bundle = new Bundle();
            bundle.putInt("id", Constant.PAGES_ARTICLES);
            ArticlesAndVideoFragment fragment = new ArticlesAndVideoFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);
        } else if (pageId == Constant.BOOKS) {
            openFragment(new BooksFragment());
        } else if (pageId == Constant.SOUNDS) {
            openFragment(new SoundFragment());
        } else if (pageId == Constant.VIDEO) {
            Bundle bundle = new Bundle();
            bundle.putInt("id", Constant.PAGES_VIDEO);
            VideoFragment fragment = new VideoFragment();
            fragment.setArguments(bundle);
            openFragment(fragment);


        }else  if (pageId == Constant.SEARCH) {
           openFragment(new SearchFragment());
        }else  if (pageId == Constant.NOTIFICATION) {
            openFragment(new NotificationFragment());
        }
        if (pageId == Constant.BIOGRAPHY_AND_CAREER) {
            openFragment(new BiographyAndCareerFragment());
            toolbar.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        } else {
            toolbar.setVisibility(View.VISIBLE);

        }
        if (pageId == Constant.COMMUNICATION) {
            openFragment(new CommunictionFragment());
            toolbar.setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        }
        if (pageId == Constant.AGENTS_AND_OPPOSED) {
            openFragment(new AgentsAndShowsFragment());
        }
        if (pageId == Constant.DEVELOPER_PAGE) {
            openFragment(new DeveloperFragment());

        } else if (pageId == Constant.WISH_LISTED) {
            openFragment(new WishListedFragment());
        } else if (pageId == Constant.EXIT) {
            shardPref().clear();
            startActivity(new Intent(getApplicationContext(), HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        } else if (pageId == Constant.MAIN) {
            if (shardPref().getUser() == null) {
                startActivity(new Intent(getApplicationContext(), HelloActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                finish();
            } else {
                finish();
            }
        }

        drawer.closeDrawer(GravityCompat.END);
    }

    public void openFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.content, fragment).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:

                if (!drawer.isOpaque()) {
                    drawer.openDrawer(GravityCompat.END);
                } else {
                    drawer.closeDrawer(GravityCompat.END);
                }
                break;
            case R.id.back:

                finish();
                break;

        }
    }

    private void addItemToNavBar() {
        list.add(new NavItem("الرئيسية", R.drawable.ic_icon_sidebar_home, Constant.MAIN));
        list.add(new NavItem("البحث", R.drawable.bakar_icon_search_2, Constant.SEARCH));
        list.add(new NavItem("التنبيهات", R.drawable.ic_notifications, Constant.NOTIFICATION));
        list.add(new NavItem("سيرة ومسيرة", R.drawable.pen_tool, Constant.BIOGRAPHY_AND_CAREER));
        list.add(new NavItem("المقالات", R.drawable.post_it, Constant.ARTICLES));
        list.add(new NavItem("الكتب", R.drawable.filing_cabinet, Constant.BOOKS));
        list.add(new NavItem("الصوتيات", R.drawable.lecture, Constant.SOUNDS));
        if (shardPref().getUser() != null)
            list.add(new NavItem("المفضلة", R.drawable.bakar_icon_favorite_of_slider, Constant.WISH_LISTED));
        list.add(new NavItem("قنوات التواصل", R.drawable.social_media, Constant.COMMUNICATION));
        list.add(new NavItem("حول المطور", R.drawable.search, Constant.DEVELOPER_PAGE));
        list.add(new NavItem(shardPref().getUser() != null ? "تسجيل الخروج" : "تسجيل الدخول", shardPref().getUser() != null ? R.drawable.logout : R.drawable.login, Constant.EXIT));
        adapter.notifyDataSetChanged();
    }

    private void notification() {
        request().stringRequest(Request.Method.POST, this, "notifications/reed", null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {

            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

    }

}
