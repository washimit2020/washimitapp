package com.it.washim.bakkar.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.adapter.MyVideoAdapter;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Posts;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.video_recycler)
    RecyclerView recyclerView;
    @BindView(R.id.ratray)
    MyButtonView ratray;
    @BindView(R.id.na_data)
    MyTextView na_data;
    MyVideoAdapter adapter;
    ArrayList<Posts> list;
    String url;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new MyVideoAdapter(list, getActivity(), recyclerView);
        recyclerView.setAdapter(adapter);


        swipeRefresh.setOnRefreshListener(this);
        ratray.setOnClickListener(this);


        adapter.addOnLoadMorLisenar(new OnLoadMorCollBack() {
            @Override
            public void onLoad() {
                getNextVideo();
            }
        });


        swipeRefresh.post(new Runnable() {
            @Override
            public void run() {
                swipeRefresh.setRefreshing(true);
                getVideo();
            }
        });


        return view;
    }

    private void getVideo() {

        na_data.setVisibility(View.GONE);
        ratray.setVisibility(View.GONE);
        request().stringRequest(Request.Method.GET, getActivity(), "posts/" + Constant.PAGES_VIDEO, null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                swipeRefresh.setRefreshing(false);
                list.clear();
                JSONObject object = new JSONObject(response);
                JSONObject posts = object.getJSONObject("posts");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = posts.getString("next_page_url");
                adapter.setUrl(url);
                if (list.size() == 0) {
                    na_data.setText("لا يوجد مقاطع فيديو بعد");
                    na_data.setVisibility(View.VISIBLE);
                    ratray.setVisibility(View.GONE);
                } else {
                    na_data.setVisibility(View.GONE);
                    ratray.setVisibility(View.GONE);
                }

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                na_data.setText("لا يوجد اتصال بالانترنت");
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);
            }
        });


    }

    private void getNextVideo() {

        request().stringRequest(Request.Method.GET, getActivity(), url.replace(Constant.URL, ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {
                swipeRefresh.setRefreshing(false);
                JSONObject object = new JSONObject(response);
                JSONObject posts = object.getJSONObject("posts");
                list.addAll((Collection<? extends Posts>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Posts>>() {
                }.getType()));
                adapter.notifyDataSetChanged();
                url = posts.getString("next_page_url");
                adapter.setUrl(url);
                if (list.size() == 0) {
                    na_data.setText("لا يوجد مقاطع فيديو بعد");
                    na_data.setVisibility(View.VISIBLE);
                    ratray.setVisibility(View.GONE);
                } else {
                    na_data.setVisibility(View.GONE);
                    ratray.setVisibility(View.GONE);
                }

                adapter.isLoading(false);

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipeRefresh.setRefreshing(false);
                na_data.setText("لا يوجد اتصال بالانترنت");
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    public void onRefresh() {
        getVideo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ratray:
                swipeRefresh.setRefreshing(true);
                getVideo();

                break;


        }
    }
}
