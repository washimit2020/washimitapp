package com.washimit.it.ryadah.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.SharchAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SharchFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.retry)
    ButtonView retry;
    @BindView(R.id.linearLayout_no_internt)
    LinearLayout linearLayout_no_internt;
    @BindView(R.id.no_data)
    TextViewCast no_data;
    @BindView(R.id.sharch_text)
    EditTextView sharch_text;
    @BindView(R.id.recycler_sharch)
    RecyclerView recycler_sharch;
    @BindView(R.id.sharch_filter_view_3)
    ImageView sharch_filter_view_3;
    @BindView(R.id.sharch_filter_view_2)
    ImageView sharch_filter_view_2;
    @BindView(R.id.sharch_dialog_filter_result_1)
    ImageView sharch_dialog_filter_result_1;
    @BindView(R.id.sharch_dialog_filter_result_2)
    ImageView sharch_dialog_filter_result_2;
    @BindView(R.id.swipe_refresh_sharch)
    SwipeRefreshLayout swipe_refresh_sharch;

    SharchAdapter adapter;
    ArrayList<Object> list;
    ApiRequest request;
    HashMap<String, String> map;
    Gson gson;
    int type = 0;
    String sort = "";
    String urls;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sharch, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        gson = new Gson();
        request = new ApiRequest(getActivity());
        recycler_sharch.setHasFixedSize(true);
        map = new HashMap<String, String>();

        sharch_dialog_filter_result_1.setOnClickListener(this);
        sharch_dialog_filter_result_2.setOnClickListener(this);
        sharch_filter_view_2.setOnClickListener(this);
        sharch_filter_view_3.setOnClickListener(this);
        retry.setOnClickListener(this);
        swipe_refresh_sharch.setOnRefreshListener(this);
        swipe_refresh_sharch.setOnRefreshListener(this);
        swipe_refresh_sharch.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh_sharch.setRefreshing(true);
                search();
            }
        });


        sharch_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    AapHalper.hidKepord(sharch_text, getActivity());


                }

                return false;
            }
        });
        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.sharch_dialog_filter_result_1:

                AapHalper.CreateDialog(R.layout.dialog_filter_result_1, getActivity(), new DaelogCallback() {
                    @Override
                    public void dialog(final Dialog dialog) {
                        LinearLayout dialog_filter_result_1_all, dialog_filter_result_1_product, dialog_filter_result_1_store, dialog_filter_result_1_show, dialog_filter_result_1_festival;

                        dialog_filter_result_1_all = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_1_all);
                        dialog_filter_result_1_product = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_1_product);
                        dialog_filter_result_1_store = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_1_store);
                        dialog_filter_result_1_show = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_1_show);
                        dialog_filter_result_1_festival = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_1_festival);

                        dialog_filter_result_1_all.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                type = Constant._SEARCH_TYPE_ALL;
                                search();
                                dialog.dismiss();
                            }
                        });
                        dialog_filter_result_1_product.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                type = Constant._SEARCH_TYPE_PRODUCT;
                                search();
                                dialog.dismiss();


                            }
                        });
                        dialog_filter_result_1_store.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                type = Constant._SEARCH_TYPE_STORE;
                                search();
                                dialog.dismiss();

                            }
                        });
                        dialog_filter_result_1_show.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                type = Constant._SEARCH_TYPE_SHOW;
                                search();
                                dialog.dismiss();

                            }
                        });
                        dialog_filter_result_1_festival.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                type = Constant._SEARCH_TYPE_FESTIVAL;
                                search();
                                dialog.dismiss();

                            }
                        });


                    }
                });
                break;

            case R.id.sharch_dialog_filter_result_2:
                AapHalper.CreateDialog(R.layout.dialog_filter_result_2, getActivity(), new DaelogCallback() {
                    @Override
                    public void dialog(final Dialog dialog) {
                        LinearLayout dialog_filter_result_2_all, dialog_filter_result_2_lowest_price, dialog_filter_result_2_best_rating;
                        dialog_filter_result_2_all = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_2_all);
                        dialog_filter_result_2_lowest_price = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_2_lowest_price);
                        dialog_filter_result_2_best_rating = (LinearLayout) dialog.findViewById(R.id.dialog_filter_result_2_best_rating);

                        dialog_filter_result_2_all.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sort = Constant._SEARCH_SORT_ALL;
                                search();
                                dialog.dismiss();

                            }
                        });
                        dialog_filter_result_2_lowest_price.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sort = Constant._SEARCH_SORT_PRICE;
                                search();
                                dialog.dismiss();

                            }
                        });
                        dialog_filter_result_2_best_rating.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sort = Constant._SEARCH_SORT_RATING;
                                search();
                                dialog.dismiss();


                            }
                        });

                    }
                });

                break;

            case R.id.sharch_filter_view_2:
                if (type == Constant._SEARCH_TYPE_PRODUCT || type == Constant._SEARCH_TYPE_ALL) {
                    recycler_sharch.setLayoutManager(new GridLayoutManager(getActivity(), 2));
                    adapter.notifyDataSetChanged();

                }

                break;

            case R.id.sharch_filter_view_3:
                if (type == Constant._SEARCH_TYPE_PRODUCT || type == Constant._SEARCH_TYPE_ALL) {

                    recycler_sharch.setLayoutManager(new GridLayoutManager(getActivity(), 3));

                    adapter.notifyDataSetChanged();
                }

                break;
            case R.id.retry:
                swipe_refresh_sharch.setRefreshing(true);
                linearLayout_no_internt.setVisibility(View.GONE);
                search();

                break;


        }
    }


    private void search() {
        if (adapter != null) {
            adapter.setUrl(null);
            urls = null;
        }
        swipe_refresh_sharch.setRefreshing(true);
        map.put("name", sharch_text.getText().toString() + "");
        if (type != 0) {
            map.put("type", type + "");
        } else {
            type = 1;
            map.put("type", type + "");

        }
        if (!sort.equalsIgnoreCase("")) {
            map.put("sort", sort);
        }

        if (!sharch_text.getText().toString().equalsIgnoreCase("")) {
            map.put("text", sharch_text.getText().toString());
        }
        request.MultipartRequest(Request.Method.POST, false, "search", map, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                list.clear();

                JSONObject object;
                JSONObject msg;
                JSONObject user;
                JSONObject products;
                JSONObject festivals;


                object = new JSONObject(result);
                msg = object.getJSONObject("msg");


                if (type == Constant._SEARCH_TYPE_STORE) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");

                    ArrayList<User> users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());
                    recycler_sharch.setLayoutManager(new LinearLayoutManager(getActivity()));
                    adapter = new SharchAdapter(list, getActivity(), recycler_sharch);
                    adapter.setUrl(urls);
                    recycler_sharch.setAdapter(adapter);
                    adapter.setLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            list.add(null);
                            adapter.notifyItemInserted(list.size() - 1);
                            getNext(urls);
                        }
                    });

                    list.addAll(users);


                } else if (type == Constant._SEARCH_TYPE_SHOW) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    ArrayList<User> users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());
                    recycler_sharch.setLayoutManager(new LinearLayoutManager(getActivity()));
                    adapter = new SharchAdapter(list, getActivity(), recycler_sharch);
                    adapter.setUrl(urls);
                    recycler_sharch.setAdapter(adapter);
                    adapter.setLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            list.add(null);
                            adapter.notifyItemInserted(list.size() - 1);
                            getNext(urls);
                        }
                    });
                    list.addAll(users);

                } else if (type == Constant._SEARCH_TYPE_FESTIVAL) {
                    festivals = msg.getJSONObject("festivals");
                    urls = festivals.getString("next_page_url");
                    ArrayList<Festivals> data = gson.fromJson(festivals.getString("data"), new TypeToken<ArrayList<Festivals>>() {
                    }.getType());
                    recycler_sharch.setLayoutManager(new LinearLayoutManager(getActivity()));
                    adapter = new SharchAdapter(list, getActivity(), recycler_sharch);
                    adapter.setUrl(urls);
                    recycler_sharch.setAdapter(adapter);
                    adapter.setLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            list.add(null);
                            adapter.notifyItemInserted(list.size() - 1);
                            getNext(urls);
                        }
                    });
                    list.addAll(data);
                } else {
                    products = msg.getJSONObject("products");
                    urls = products.getString("next_page_url");

                    ArrayList<Product> productList = gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType());
                    recycler_sharch.setLayoutManager(new GridLayoutManager(getContext(), 2));
                    adapter = new SharchAdapter(list, getActivity(), recycler_sharch);
                    adapter.setUrl(urls);
                    recycler_sharch.setAdapter(adapter);
                    adapter.setLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            list.add(null);
                            adapter.notifyItemInserted(list.size() - 1);
                            getNext(urls);
                        }
                    });
                    list.addAll(productList);

                }
                if (list.size() == 0) {
                    no_data.setVisibility(View.VISIBLE);
                } else {
                    no_data.setVisibility(View.GONE);

                }
                adapter.notifyDataSetChanged();
                swipe_refresh_sharch.setRefreshing(false);

            }

            @Override
            public void onRequestError(String errorMessage) {
                swipe_refresh_sharch.setRefreshing(false);
                linearLayout_no_internt.setVisibility(View.VISIBLE);

            }
        });

    }


    private void getNext(final String url) {

        request.MultipartRequest(Request.Method.POST, true, url, map, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size() - 1);
                JSONObject object;
                JSONObject msg;
                JSONObject user;
                JSONObject products;

                object = new JSONObject(result);
                msg = object.getJSONObject("msg");


                if (type == Constant._SEARCH_TYPE_STORE) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");

                    ArrayList<User> users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());
                    list.addAll(users);

                } else if (type == Constant._SEARCH_TYPE_SHOW) {
                    user = msg.getJSONObject("users");
                    urls = user.getString("next_page_url");
                    ArrayList<User> users = gson.fromJson(user.getString("data"), new TypeToken<ArrayList<User>>() {
                    }.getType());
                    list.addAll(users);
                } else if (type == Constant._SEARCH_TYPE_FESTIVAL) {

                } else {
                    products = msg.getJSONObject("products");
                    urls = products.getString("next_page_url");
                    ArrayList<Product> productList = gson.fromJson(products.getString("data"), new TypeToken<ArrayList<Product>>() {
                    }.getType());
                    list.addAll(productList);

                }
                adapter.setUrl(urls);
                adapter.notifyDataSetChanged();
                swipe_refresh_sharch.setRefreshing(false);
                adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {
                swipe_refresh_sharch.setRefreshing(false);

            }
        });


    }

    @Override
    public void onRefresh() {
        search();
    }
}
