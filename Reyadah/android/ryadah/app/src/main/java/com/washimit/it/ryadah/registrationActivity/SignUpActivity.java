package com.washimit.it.ryadah.registrationActivity;

import android.app.Dialog;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.text_name)
    TextViewCast text_name;
    @BindView(R.id.text_des)
    TextViewCast text_des;
    @BindView(R.id.social_media_show)
    TextViewCast social_media_show;
    @BindView(R.id.email)
    LinearLayout email;
    @BindView(R.id.mobile)
    LinearLayout mobile;
    @BindView(R.id.place_des)
    LinearLayout place_des;
    @BindView(R.id.location_des)
    LinearLayout location_des;
    @BindView(R.id.social_media)
    LinearLayout social_media;
    @BindView(R.id.add_img)
    TextViewCast add_img;
    @BindView(R.id.name)
    EditTextView name;
    @BindView(R.id.email_text)
    EditTextView email_text;
    @BindView(R.id.des)
    EditTextView des;
    @BindView(R.id.phone)
    EditTextView phone;
    @BindView(R.id.street_name)
    EditTextView street_name;
    @BindView(R.id.sector)
    EditTextView sector;
    @BindView(R.id.password)
    EditTextView password;
    @BindView(R.id.password_confirmation)
    EditTextView password_confirmation;
    @BindView(R.id.registration)
    ImageView registration;
    @BindView(R.id.show_location)
    ButtonView show_location;

    @BindView(R.id.facebook)
    ImageView facebook;
    @BindView(R.id.instgram)
    ImageView instgram;
    @BindView(R.id.snapchat)
    ImageView snapchat;
    @BindView(R.id.twitter)
    ImageView twitter;
    @BindView(R.id.whatsapp)
    ImageView whatsapp;
    Bundle bundle;

    ApiRequest request;

    ArrayList<Part> parts;
    ArrayList<File> files;
    File avatar;
    int userType;
    LatLng lng;
    SharedPref pref;
    String facebook_ = "", instagram_ = "", twitter_ = "", snapchat_ = "", whatsapp_ = "";
    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sgin_up);
        ButterKnife.bind(this);
        pref = new SharedPref(this);
        bundle = getIntent().getExtras();
        request = new ApiRequest(SignUpActivity.this);
        userType = bundle.getInt("userType");


        // Get token
        token = FirebaseInstanceId.getInstance().getToken();

        // Log
        Log.d("msg", getString(R.string.msg_token_fmt, token));


        updateUi(userType);
        registration.setOnClickListener(this);
        show_location.setOnClickListener(this);
        add_img.setOnClickListener(this);
        facebook.setOnClickListener(this);
        instgram.setOnClickListener(this);
        snapchat.setOnClickListener(this);
        twitter.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        back.setOnClickListener(this);


        parts = new ArrayList<>();
        files = new ArrayList<>();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant._TYPE_MAP) {

            if (data != null) {
                lng = (LatLng) data.getExtras().get("latLng");
                Log.d("LatLng", lng.latitude + " " + lng.longitude);
                parts.add(new StringPart("lat", lng.latitude + ""));
                parts.add(new StringPart("lng", lng.longitude + ""));
            } else {

            }


        } else if (requestCode == Constant._TYPE_PICTURE) {
            files.clear();
            if (data != null) {
                if (data.getExtras().get("avatar") != null) {
                    avatar = new File(data.getExtras().getString("avatar"));
                    Log.d("avatar", data.getExtras().get("avatar") + "");
                }
                if (data.getExtras().get("caver1") != null) {
                    files.add(new File(data.getExtras().getString("caver1")));
                    Log.d("caver1", data.getExtras().get("caver1") + "");

                }
                if (data.getExtras().get("caver2") != null) {
                    files.add(new File(data.getExtras().getString("caver2")));
                    Log.d("caver2", data.getExtras().get("caver2") + "");

                }
                if (data.getExtras().get("caver3") != null) {
                    files.add(new File(data.getExtras().getString("caver3")));
                    Log.d("caver3", data.getExtras().get("caver3") + "");


                }


            }


        }


    }

    public void updateUi(int userType) {

        if (userType == 1) {

            text_name.setText(R.string.store_name);
            text_des.setText(R.string.bio_store);
            social_media_show.setText(R.string.social_media_store);
        }
        if (userType == 2) {

        }
        if (userType == 3) {

            text_name.setText(R.string.name);
            text_des.setText(R.string.desc);
            email.setVisibility(View.VISIBLE);
            mobile.setVisibility(View.VISIBLE);
            social_media_show.setVisibility(View.GONE);
            social_media.setVisibility(View.GONE);
            location_des.setVisibility(View.GONE);
            add_img.setVisibility(View.GONE);
        }


    }


    public boolean validation() {


        if (name.getText().toString().equalsIgnoreCase("")) {
            name.setError(getResources().getString(R.string.fill_name));
            name.requestFocus();
            return false;
        }
        if (des.getText().toString().equalsIgnoreCase("")) {
            des.setError(getResources().getString(R.string.fill_desc));
            des.requestFocus();

            return false;
        }

        if (!(Patterns.PHONE.matcher(phone.getText().toString()).matches() && phone.getText().toString().length() >= 10)) {
            phone.setError(getResources().getString(R.string.fill_mobile_number));
            phone.requestFocus();
            return false;
        }
        if (sector.getText().toString().equalsIgnoreCase("")) {
            sector.setError(getResources().getString(R.string.fill_street));
            sector.requestFocus();

            return false;
        }
        if (street_name.getText().toString().equalsIgnoreCase("")) {
            street_name.setError(getResources().getString(R.string.fill_street));
            street_name.requestFocus();
            return false;
        }


        if (!(password.getText().toString().length() >= 5)) {
            password.setError(getResources().getString(R.string.invalid_password));
            password.requestFocus();

            return false;
        }
        if (!password.getText().toString().equalsIgnoreCase(password_confirmation.getText().toString())) {
            password_confirmation.setError(getResources().getString(R.string.password_not_match));
            password_confirmation.requestFocus();

            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registration:
                if (validation()) {
                    parts.add(new StringPart("login_type", String.valueOf(Constant.NORMAL_REGISTER)));
                    parts.add(new StringPart("name", name.getText().toString()));
                    parts.add(new StringPart("email", email_text.getText().toString()));
                    parts.add(new StringPart("reg_id", token + ""));
                    parts.add(new StringPart("type_id", String.valueOf(userType)));
                    parts.add(new StringPart("street_name", street_name.getText().toString()));
                    parts.add(new StringPart("sector_name", sector.getText().toString()));
                    parts.add(new StringPart("phone_number", phone.getText().toString()));
                    parts.add(new StringPart("password", password.getText().toString()));
                    parts.add(new StringPart("password_confirmation", password_confirmation.getText().toString()));
                    parts.add(new StringPart("info", des.getText().toString()));
                    parts.add(new StringPart("facebook", facebook_));
                    parts.add(new StringPart("twitter", twitter_));
                    parts.add(new StringPart("instagram", instagram_));
                    parts.add(new StringPart("snapchat", snapchat_));
                    parts.add(new StringPart("whatsapp", whatsapp_));


                    if (avatar != null) {
                        parts.add(new FilePart("avatar", avatar));
                    }
                    for (int i = 0; i < files.size(); i++) {

                        parts.add(new FilePart("cover[" + i + "]", files.get(i)));
                    }
                    request.MultipartRequest("POST", "register", getString(R.string.register), parts, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result_) {
                            JSONObject result = null;
                            JSONObject msg = null;

                            try {
                                result = new JSONObject(result_);
                                msg = result.getJSONObject("msg");
                                Gson gson = new Gson();
                                User user = gson.fromJson(msg.getString("user"), User.class);
                                user.setToken(msg.getString("token"));

                                pref.setUser(gson.toJson(user));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("result", result_);

                            openActivity(MainActivity.class, 3);


                        }

                        @Override
                        public void onRequestError(String errorMessage) {
                            Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();

                        }
                    });

                }

                break;
            case R.id.show_location:


                openActivity(MapsActivity.class, Constant._TYPE_MAP);


                break;
            case R.id.back:


                this.finish();


                break;

            case R.id.add_img:


                openActivity(ImageAdd.class, userType, Constant._TYPE_PICTURE);


                break;

            case R.id.facebook:


                openDialog(0);


                break;

            case R.id.twitter:


                openDialog(1);


                break;

            case R.id.instgram:


                openDialog(2);


                break;

            case R.id.snapchat:


                openDialog(3);


                break;

            case R.id.whatsapp:


                openDialog(4);


                break;

        }
    }

    public void openActivity(Class activity, int userType, int requstCode) {

        Intent intent = new Intent(SignUpActivity.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        intent.putExtra("requestType", Constant._TYPE_SIGN_UP);
        startActivityForResult(intent, requstCode);

    }

    public void openActivity(Class activity, int requstCode) {

        Intent intent = new Intent(SignUpActivity.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, requstCode);

    }

    private void openDialog(final int x) {
        AapHalper.CreateDialog(R.layout.dialog_social_media, this, new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final EditTextView text = (EditTextView) dialog.findViewById(R.id.dialog_social_media_text);
                ButtonView ok = (ButtonView) dialog.findViewById(R.id.dialog_social_media_ok);
                ButtonView cancel = (ButtonView) dialog.findViewById(R.id.dialog_social_media_cancel);


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        switch (x) {

                            case 0:
                                facebook_ = text.getText().toString();
                                break;
                            case 1:
                                twitter_ = text.getText().toString();
                                break;
                            case 2:
                                instagram_ = text.getText().toString();
                                break;
                            case 3:
                                snapchat_ = text.getText().toString();
                                break;

                            case 4:
                                whatsapp_ = text.getText().toString();
                                break;
                        }

                        dialog.dismiss();

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

            }
        });


    }
}
