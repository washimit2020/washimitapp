<?php

namespace App\Http\Controllers\Products;

use App\__ModelsProducts\Product;
use App\__ModelsProducts\ShareProduct;
use App\Http\Constants;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SharedProductsController extends Controller
{
    /**
     * get all resources
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        $conditions [] = ["shareable_type", Constants::USER_SHARED];

        if(auth()->user()->type_id == Constants::_TYPE_SHOW){
            $conditions[] = ["shareable_id" , auth()->user()->id];
        }else{
            $conditions[] = ["user_id" , auth()->user()->id];
        }

        $shared = ShareProduct::where($conditions)->latest()->orderBy('status_id');
        if(auth()->user()->type_id == Constants::_TYPE_SHOW){
            $conditions[] = ["shareable_id" , auth()->user()->id];
        }else{
           $shared->whereHas('sharedWith',function ($query){
                   check_if_blocked($query);
               });
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS,["shared"=>$shared->paginate(Constants::PAGINATION)]));
    }

    /**
     * create the instance
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => "required|in:" . make_validation_array_in([Constants::USER_SHARED, Constants::BRANCH_SHARED]),
            "shares"=>"required|array",
            "shares.*.product_id"=>"required|exists:products,id,status_id,".Constants::_STATUS_ACTIVE.",user_id,".$request->user()->id,
            "shares.*.user_id" => 'required_if:type,' . Constants::USER_SHARED
                . "|exists:users,id,status_id," . Constants::_STATUS_ACTIVE . ',type_id,' . Constants::_TYPE_SHOW . ',id,!' . auth()->user()->id,

            "shares.*.branch_id" => 'required_if:type,' . Constants::BRANCH_SHARED
                . "|exists:show_branches,id,status_id," . Constants::_STATUS_ACTIVE . ',user_id,' . auth()->user()->id,
        ]);

        foreach ($request->shares as $share){
            if ($request->type == Constants::USER_SHARED) {
                $share["status_id"] = Constants::_STATUS_INACTIVE;
                $share["shareable_type"]  = Constants::USER_SHARED;
                $share["shareable_id"]  = $share["user_id"];

                abort_if_blocked(User::find($share['user_id']));
            } else {
                $this->authorize(Constants::ABILITY_HAS_BRANCHES);

                $share["status_id"] = Constants::_STATUS_ACTIVE;
                $share["shareable_type"]  = Constants::BRANCH_SHARED;
                $share["shareable_id"]  = $share["branch_id"];

            }
            $share["user_id"] = auth()->user()->id;
            $sharedProduct =  ShareProduct::where([
                ["user_id",auth()->user()->id],
                ["shareable_type",$share["shareable_type"]],
                ["shareable_id",$share["shareable_id"]]
            ])->whereIn('status_id',[Constants::_STATUS_ACTIVE,Constants::_STATUS_INACTIVE])->first();

            if(empty($sharedProduct)){
                ShareProduct::create($share);
            }
        }


        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["shared" => true]));
    }

    /**
     * update the current shareProduct
     *
     * @param Request $request
     * @param ShareProduct $shareable
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request , ShareProduct $shareable)
    {

        $statuses = [Constants::_STATUS_ACTIVE,Constants::_STATUS_DELETED];


        if ($shareable->shareable_type != Constants::USER_SHARED){
            if($shareable->user_id != auth()->user()->id){
                abort(Constants::RESPONSE_CODE_FORBIDDEN);
            }
            abort_if_blocked($shareable->shareable);
        }else{
            if($shareable->shareable_id != auth()->user()->id){
                abort(Constants::RESPONSE_CODE_FORBIDDEN);
            }
            $statuses [] = Constants::_STATUS_DECLINED;
        }

        $this->validate($request, [
            'status_id' => "required|in:" . make_validation_array_in($statuses),
        ]);

        if($request->status_id == Constants::_STATUS_DELETED){
            $shareable->delete();
        }else{
            $shareable->update($request->only("status_id"));
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["shared" => true]));
    }

    public function product(Product $product,$type=Constants::USER_SHARED){

        $shares = $product->shares()->whereHas('user',function ($query){
            check_if_blocked($query);
        })->where('shareable_type',$type);

        if ($type == Constants::USER_SHARED){
           $shares->whereHas('sharedWith',function ($query){
                check_if_blocked($query);
            });
        }

        $shares = $shares->get();


        return response()->json(response_json(Constants::RESPONSE_SUCCESS,["shares"=>$shares]));
    }


}
