//
//  Comment_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher

class Comment_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var image: UIImageView!
    var commentModel:Comment?{
        didSet{
            updateView()
        }
    }
    
    
    public func updateView(){
        comment.text = commentModel!.body!
        
        guard let user = commentModel?.user else {
            return
        }
        
        name.text = user.name
        
        image.set_avatar_image(user_id: user.id)
        
    }
    
}
