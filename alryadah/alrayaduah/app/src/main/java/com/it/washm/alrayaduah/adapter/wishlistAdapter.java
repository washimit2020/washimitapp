package com.it.washm.alrayaduah.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.it.washm.alrayaduah.NewsDetailsActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.model.wishlist;

/**
 * Created by washm on 1/16/17.
 */

public class wishlistAdapter extends RecyclerView.Adapter {
    private int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private List<wishlist> wishlists;
    public static HashMap<String, String> Stringparams;
    SharedPref pref;
    public Requests requests;
    private Activity activity;
    private int lastPosition = -1;
    private int recySize;
    public String urls;
    Intent intent;
    String[] a;
    Date date;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    CustamViewFont viewFont;

    public wishlistAdapter(Activity activity, List<wishlist> wishlists, RecyclerView recyclerView, CustamViewFont viewFont) {
        this.wishlists = wishlists;
        this.activity = activity;
        this.viewFont=viewFont;
        requests = new Requests(activity.getApplicationContext(), activity);
        Stringparams = new HashMap<String, String>();
        pref = new SharedPref(activity.getApplicationContext(), "MyPref");


        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();


            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView,
                                       int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null && !urls.equalsIgnoreCase("null")) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return wishlists.get(position) != null ? VIEW_ITEM : VIEW_PROG;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.news_item, parent, false);

            vh = new NewsItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.progressbar_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

      /*  Animation animation = AnimationUtils.loadAnimation(activity,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);/*/


        if (holder instanceof NewsItemViewHolder) {
            final wishlist singleWishlist = (wishlist) wishlists.get(position);


            viewFont.setfont(((NewsItemViewHolder) holder).news_item_tital ,"Cairo-SemiBold.ttf");
            viewFont.setfont(  ((NewsItemViewHolder) holder).news_item_Views,"Cairo-Regular.ttf");
            viewFont.setfont(  ((NewsItemViewHolder) holder).news_item_Likes,"Cairo-Regular.ttf");


                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.forLanguageTag("ar"));

                try {
                    date = format.parse(singleWishlist.getUpdated_at());
                } catch (ParseException e) {
                    e.printStackTrace();
                }



            ((NewsItemViewHolder) holder).wishlist_but.setVisibility(View.VISIBLE);


            ((NewsItemViewHolder) holder).news_item_tital.setText(singleWishlist.getNews().getTitle());
            ((NewsItemViewHolder) holder).news_item_Likes.setText(AppHelper.getDateDifferenceForDisplay(date,activity,singleWishlist.getUpdated_at()));
            ((NewsItemViewHolder) holder).news_item_Views.setText(singleWishlist.getNews().getViews_count());

            Picasso.with(activity).load(Links.UrlImg + getAtachment(singleWishlist, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic)
                    .into(((NewsItemViewHolder) holder).news_item_mainImg);


            ((NewsItemViewHolder) holder).wishlist_but.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {
                        Stringparams.put("news_id",singleWishlist.getNews().getId());
                        AppHelper.wishlist("wishlist",activity,Stringparams,"تم الازاله من المفضلة");
                        wishlists.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position,wishlists.size());
                    }
                    else {
                        Toast.makeText(activity, "يجب تسجيل الدخول !", Toast.LENGTH_LONG).show();

                    }

                }
            });


            ((NewsItemViewHolder) holder).new_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (AppHelper.isOnline(activity)) {

                    a = AppHelper.getDateType(singleWishlist.getUpdated_at(),activity,"yyyy-MM-dd' 'HH:mm:ss").split(" ").clone();
                    intent = new Intent(activity, NewsDetailsActivity.class);
                    intent.putExtra("date", a[0]);
                    intent.putExtra("time", a[1]);
                    intent.putExtra("newId", singleWishlist.getNews().getId());
                    intent.putExtra("img_new", getAtachment(singleWishlist, 0));

                    intent.putExtra("img_user", getAtachmentUser(singleWishlist,0));
                    intent.putExtra("title", singleWishlist.getNews().getTitle());

                    if (singleWishlist.getNews().getUser().getNick_name() != null) {
                        intent.putExtra("name", singleWishlist.getNews().getUser().getNick_name());
                    } else {
                        intent.putExtra("name", singleWishlist.getNews().getUser().getFirst_name() + " " + singleWishlist.getNews().getUser().getLast_name());
                    }
                    intent.putExtra("content", singleWishlist.getNews().getContent());
                    NewsDetailsActivity.attachments = singleWishlist.getNews().getAttachment();


                    activity.startActivity(intent);
                    } else {
                        Snackbar.make(activity.getCurrentFocus(), "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }
                }
            });


        } else {
            ((ProgressViewHolder) holder).avi.show();

        }

    }

    public void setLoaded() {
        loading = false;
    }

    public void setUrl(String url) {
        urls = url;
    }

    public String getAtachment(wishlist singleWishlist, int i) {
        if (singleWishlist.getNews().getAttachment().size() != 0) {
            if (i <= singleWishlist.getNews().getAttachment().size()) {
                return singleWishlist.getNews().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    public String getAtachmentUser(wishlist singleWishlist, int i) {
        if (singleWishlist.getNews().getUser().getAttachment().size() != 0) {
            if (i <= singleWishlist.getNews().getUser().getAttachment().size()) {
                return singleWishlist.getNews().getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    @Override
    public int getItemCount() {
        return wishlists.size();
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


    public class NewsItemViewHolder extends RecyclerView.ViewHolder {
        ImageView news_item_mainImg, wishlist_but;
        TextView news_item_tital, news_item_Views, news_item_Likes;
        CardView new_item;

        public NewsItemViewHolder(View view) {
            super(view);
            news_item_mainImg = (ImageView) view.findViewById(R.id.news_item_main_img);
            wishlist_but = (ImageView) view.findViewById(R.id.wishlist_but);

            news_item_tital = (TextView) view.findViewById(R.id.news_item_tital);
            news_item_Views = (TextView) view.findViewById(R.id.news_item_Views);
            news_item_Likes = (TextView) view.findViewById(R.id.news_item_Likes);
            new_item = (CardView) view.findViewById(R.id.new_item);


        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;

        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
        }

    }


}
