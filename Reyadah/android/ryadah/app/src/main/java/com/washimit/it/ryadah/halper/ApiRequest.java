package com.washimit.it.ryadah.halper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import org.json.JSONException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by mohammad on 4/15/2017.
 */

public class ApiRequest {
    Activity context;
    Context contexts;
    String msg;
    private ProgressDialog pDialog;
    SharedPref pref;
    static RequestQueue requestQueue;
    String token = "";

    public ApiRequest(Activity context) {
        pref = new SharedPref(context);
        this.context = context;
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
            Log.d("requestQueue", "new request Queue!");
        }
        if (pref.getUser() != null) {
            token = pref.getUser().getToken();
        }
    }

    public ApiRequest(Context context) {
        pref = new SharedPref(context);
        this.contexts = context;
        if (requestQueue == null) {
            requestQueue = Volley.newRequestQueue(context);
            Log.d("requestQueue", "new request Queue!");
        }
        if (pref.getUser() != null) {
            token = pref.getUser().getToken();
        }
    }

    /**
     * @param method
     * @param url
     * @param msg
     * @param prams
     * @param callback
     */
    public void MultipartRequest(String method, String url, @Nullable final String msg, List<Part> prams, final MultipartCallback callback) {
        this.msg = msg;
        if (msg != null) {
            showDialog();
        }
        Ion.with(context)
                .load(method, Constant._IP_URL + url)
                .setTimeout(60 * 60 * 1000)
                .setHeader("Accept", "application/json")
                .setHeader("app-key", "base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84=")
                .setHeader("Authorization", "Bearer" + " " + token)
                .addMultipartParts(prams)
                .asJsonObject()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonObject> result) {


                        if (e != null) {
                            callback.onRequestError(e.getMessage());
                            if (msg != null) {
                                hideDialog();
                            }
                            return;
                        }


                        if (result.getHeaders().code() == 401) {

                            AapHalper.CreateDialog(R.layout.dialog_sgin_up, context, new DaelogCallback() {
                                @Override
                                public void dialog(final Dialog dialog) {
                                    ButtonView signUp, close;
                                    signUp = (ButtonView) dialog.findViewById(R.id.signUp);
                                    close = (ButtonView) dialog.findViewById(R.id.close);

                                    signUp.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            context.finish();
                                        }
                                    });
                                    close.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.cancel();
                                        }
                                    });
                                }
                            });

                        }


                        if (result.getHeaders().code() == 422) {


                            callback.onRequestError(result.getResult().toString());


                            if (msg != null) {
                                hideDialog();
                            }
                            return;
                        }

                        if (result.getHeaders().code() == 404) {
                            callback.onRequestError(context.getString(R.string.general_Failed_error));

                            if (msg != null) {
                                hideDialog();
                            }
                            return;
                        }
                        try {
                            callback.onSuccess(result.getResult() + "");
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                        if (msg != null) {
                            hideDialog();
                        }
                    }
                });


    }

    /**
     * @param requestMethod
     * @param foulUrl
     * @param url
     * @param msg
     * @param callback
     */
    public void MultipartRequest(int requestMethod, boolean foulUrl, String url, @Nullable final String msg, final MultipartCallback callback) {
        this.msg = msg;
        String urls;
        if (msg != null) {
            showDialog();
        }
        if (foulUrl) {
            urls = url;
        } else {
            urls = Constant._IP_URL + url;

        }


        StringRequest stringRequest = new StringRequest(requestMethod, urls, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    callback.onSuccess(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onRequestError("");
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("Accept", "application/json");
                stringMap.put("app-key", "base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84=");
                stringMap.put("Authorization", "Bearer" + " " + token);


                return stringMap;
            }
        };
        requestQueue.add(stringRequest);


    }

    /**
     * @param requestMethod
     * @param foulUrl
     * @param url
     * @param prams
     * @param msg
     * @param callback
     */
    public void MultipartRequest(int requestMethod, boolean foulUrl, String url, final HashMap<String, String> prams, @Nullable final String msg, final MultipartCallback callback) {
        this.msg = msg;
        String urls;
        if (msg != null) {
            showDialog();
        }

        if (foulUrl) {
            urls = url;
        } else {
            urls = Constant._IP_URL + url;

        }


        StringRequest stringRequest = new StringRequest(requestMethod, urls, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    callback.onSuccess(response);
                    if (msg != null) {
                        hideDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onRequestError("");

                if (msg != null) {
                    hideDialog();

                }
            }
        }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return prams;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("Accept", "application/json");
                stringMap.put("app-key", "base64:Iz/afcDgUZsUNbqnOOaAdY6xFhfNKf88pvJI1fF6p84=");
                stringMap.put("Authorization", "Bearer" + " " + token);


                return stringMap;
            }
        };
        requestQueue.add(stringRequest);


    }


    private void showDialog() {
        onProgress();
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void onProgress() {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(msg);
        pDialog.setCancelable(false);
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

}
