//
//  Info_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Info_TableViewController: ItemsLoader_TableViewController {
    enum reuseIdentifiers : String {
        case infoCell = "infoCell"
        case followers = "follower"
    }

    var user:User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "
        link = RequestLinks.followed_by.rawValue
        beginWithModel = "users"
        
        tableView!.register(UINib(nibName: "InfoCell_TableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifiers.infoCell.rawValue)
        tableView!.register(UINib(nibName: "Follower_TableViewCell", bundle: nil), forCellReuseIdentifier: reuseIdentifiers.followers.rawValue)
        tableView.estimatedRowHeight = 190
        tableView.allowsSelection = false
        
        if(user.id == UserDefaultsHelper.get_user_data()?.id){
            if items.count == 0 {
                clearData()
            }
        }else{
            self.refreshControl?.removeFromSuperview()
        }
    }


    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        
        if Gate.is_costumer(){
            return 1
        }
        
         if(user.id != UserDefaultsHelper.get_user_data()?.id){
            
            return 1
        }
        
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        if section == 0 {
            return 1
        }else{
         return items.count
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        
        guard let data = dataModel, indexPath.section == 1 else {
            return
        }
        
        if indexPath.row != items.count - 2
            ||
            data.next_page_url == nil{
            return
        }
        
        make_api_call(dataModel!.next_page_url!)
    }
    
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return nil
        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 44))
            
            view.backgroundColor = .white
            
            let label = UILabel(frame: view.frame)
            label.frame.size.width  = label.frame.size.width - 8
            label.text = "المتابعون"
            label.textAlignment = .right
            label.textColor = Constants.themeColor
            label.font = UIFont(name: get_font_name(), size: 15)!
            label.numberOfLines = 1
            view.addSubview(label)
            
            return view
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifiers.infoCell.rawValue ,for: indexPath) as! InfoCell_TableViewCell
            cell.user = self.user
            if user?.type.id == 0{
                cell.blockUser.isHidden = true
            }
            if user?.id == UserDefaultsHelper.get_user_data()?.id {
                cell.blockUser.isHidden = true
            }
            cell.blockUser.addTarget(nil, action: #selector(block), for: .touchUpInside)
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifiers.followers.rawValue, for: indexPath) as! Follower_TableViewCell

        cell.user = User(items[indexPath.row])

        return cell
    }
 
   
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     
       return UITableViewAutomaticDimension
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        if indexPath.section == 0 {
            return false
        }
        return true
    }

    func block()  {
        self.apiRequest.make_request(parameters: ["user_id":"\(user!.id!)"], apiImages: nil, link: RequestLinks.block.rawValue, method: .post, dialogMsg: "جاري حظر المستخدم", completion: {
            _ in
            self.apiRequest.costumeAlert.dismiss(animated: true, completion: {
                self.navigationController?.popToRootViewController(animated: true)
            })
        })
    }
    
}
