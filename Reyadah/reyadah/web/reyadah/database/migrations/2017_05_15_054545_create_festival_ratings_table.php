<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFestivalRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('festival_ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->unsignedInteger('festival_id');

            $table->unsignedTinyInteger('view_rating');
            $table->unsignedTinyInteger('products_rating');
            $table->unsignedTinyInteger('users_rating');
            $table->unsignedTinyInteger('prices_rating');

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('festival_id')->references('id')->on('festivals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('festival_ratings');
    }
}
