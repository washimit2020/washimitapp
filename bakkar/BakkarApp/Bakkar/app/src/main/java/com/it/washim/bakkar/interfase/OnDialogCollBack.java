package com.it.washim.bakkar.interfase;

/**
 * Created by mohammad on 8/13/2017.
 */

public interface OnDialogCollBack {
    void login();
    void hide();
}
