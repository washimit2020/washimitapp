//
//  SlideMenu_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class SlideMenu_TableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
   
    var maxHeight:CGFloat = 0
    
    func updateView(dict:[String:String]){
        self.icon.image = UIImage(named: (dict["image"])! )
        self.titleText.text = dict["title"]
        
        if maxHeight > 0{
            height.constant = maxHeight
            width.constant = maxHeight
        }
    }
}
