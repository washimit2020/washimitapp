package com.it.washim.bakkar.fragment;


import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.MainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class CommunictionFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.back)
    ImageView back;

    @BindView(R.id.facebook)
    ImageView facebook;

    @BindView(R.id.twitter)
    ImageView twitter;

    @BindView(R.id.youtube)
    ImageView youtube;

    @BindView(R.id.playstore)
    ImageView playstore;

    @BindView(R.id.appstore)
    ImageView appstore;

    @BindView(R.id.google_plus)
    ImageView google_plus;


    public CommunictionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_communiction, container, false);
        ButterKnife.bind(this, view);


        back.setOnClickListener(this);
        facebook.setOnClickListener(this);
        twitter.setOnClickListener(this);
        google_plus.setOnClickListener(this);
        youtube.setOnClickListener(this);
        playstore.setOnClickListener(this);
        appstore.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                ((MainActivity) getActivity()).finish();
                break;
            case R.id.facebook:
                open("https://www.facebook.com/DrBakkar1/", "fb://profile/263445073677828");
                break;
            case R.id.twitter:
                open("https://twitter.com/drbakkar", "twitter://user?screen_name=drbakkar");

                break;
            case R.id.youtube:
                open("https://www.youtube.com/channel/UCNg6QEnJrTrBzqTZUaYUjhg", "https://www.youtube.com/channel/UCNg6QEnJrTrBzqTZUaYUjhg");

                break;
            case R.id.google_plus:
                open("https://www.instagram.com/drbakkar/", "instagram://user?username=drbakkar");

                break;
            case R.id.playstore:
                open("https://play.google.com/store/apps/developer?id=WashimIT", "https://play.google.com/store/apps/developer?id=WashimIT");
                break;
            case R.id.appstore:
                open("https://itunes.apple.com/sa/developer/ammar-al-hawamdeh/id1129033864", "https://itunes.apple.com/sa/developer/ammar-al-hawamdeh/id1129033864");
                break;
        }
    }


    private void open(String url, String urlApp) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlApp)));
        } catch (Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }
}
