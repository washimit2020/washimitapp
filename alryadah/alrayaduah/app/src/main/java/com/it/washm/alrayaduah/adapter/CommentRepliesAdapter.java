package com.it.washm.alrayaduah.adapter;

/**
 * Created by Mohammed Ali on 10/14/2016.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.replies;


public class CommentRepliesAdapter extends RecyclerView.Adapter {
    private int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private List<replies> repliesList;
    private Activity activity;
    private int lastPosition = -1;
    private int recySize;
    public String urls;
    Intent intent;
    String[] a;
    SharedPref pref;
    public static HashMap<String, String> Stringparams;
    boolean IsMyComment;
    public  Requests requests;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    Typeface Regular,Bold;

    public CommentRepliesAdapter(Activity activity, List<replies> repliesList, RecyclerView recyclerView) {
        this.repliesList = repliesList;
        this.activity = activity;
        pref = new SharedPref(activity.getApplicationContext(), "MyPref");
        Stringparams = new HashMap<String, String>();
        requests = new Requests(activity.getApplicationContext(), activity);
        Regular = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-Regular.ttf");
        Bold = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-Bold.ttf");


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {


        return repliesList.get(position) != null ? VIEW_ITEM : VIEW_PROG;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.replie_item, parent, false);

            vh = new CommentItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.see_more_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

      /*  Animation animation = AnimationUtils.loadAnimation(activity,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);/*/


        if (holder instanceof CommentItemViewHolder) {
            final replies singlereplies = (replies) repliesList.get(position);

            a = AppHelper.getDateType(singlereplies.getCreated_at(),activity,"yyyy-MM-dd' 'HH:mm:ss").split(" ");
            ((CommentItemViewHolder) holder).time_replie.setText(a[0]);
            ((CommentItemViewHolder) holder).date_replie.setText(a[1]);

            ((CommentItemViewHolder) holder).like_replie.setText(singlereplies.getLikes_count() + "");

            if (singlereplies.is_liked()) {
                ((CommentItemViewHolder) holder).like_replie.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);
            } else {


                ((CommentItemViewHolder) holder).like_replie.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);

            }


            ((CommentItemViewHolder) holder).like_replie.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (AppHelper.IsLogIn(activity)) {
                        ((CommentItemViewHolder) holder).like_replie.setClickable(false);

                        if (!singlereplies.is_liked()) {
                            ((CommentItemViewHolder) holder).like_replie.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);

                            singlereplies.setIs_liked(true);
                            ((CommentItemViewHolder) holder).like_replie.setClickable(true);
                            ((CommentItemViewHolder) holder).like_replie.setText(String.valueOf(singlereplies.getLikes_count() + 1));
                            singlereplies.setLikes_count(singlereplies.getLikes_count() + 1);

                        } else {


                            ((CommentItemViewHolder) holder).like_replie.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);
                            singlereplies.setIs_liked(false);
                            ((CommentItemViewHolder) holder).like_replie.setClickable(true);
                            ((CommentItemViewHolder) holder).like_replie.setText(String.valueOf(singlereplies.getLikes_count() - 1));
                            singlereplies.setLikes_count(singlereplies.getLikes_count() - 1);

                        }

                        Stringparams.put("reply_id", singlereplies.getId().toString());

                        requests.StringRequest(Request.Method.POST, "like/reply", Stringparams, "", new VolleyCallback() {
                            @Override
                            public void onSuccess(String result) {
                                JSONObject object = null;
                                JSONObject message = null;
                                try {
                                    object = new JSONObject(result);
                                    message = object.getJSONObject("message");


                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void onRequestError(VolleyError errorMessage) {
                                ((CommentItemViewHolder) holder).like_replie.setClickable(true);

                            }
                        });
                    } else {
                        Toast.makeText(activity, "يجب تسجيل الدخول للآعجاب!", Toast.LENGTH_LONG).show();

                    }
                }
            });


            Picasso.with(activity).load(Links.UrlImg + getAtachment(singlereplies, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic)
                    .into(((CommentItemViewHolder) holder).img_main_replaie_item);


            if (singlereplies.getUser().getNick_name() == null) {
                ((CommentItemViewHolder) holder).name_replie_comment.setText(singlereplies.getUser().getFirst_name() + " " + singlereplies.getUser().getLast_name());

            } else {
                ((CommentItemViewHolder) holder).name_replie_comment.setText(singlereplies.getUser().getNick_name());
            }

            ((CommentItemViewHolder) holder).comment_replie.setText(singlereplies.getContent());


                    ((CommentItemViewHolder)holder).replaie_item.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {
                            if (AppHelper.IsLogIn(activity)) {

                                dilog(singlereplies.getId(),singlereplies.getUser().getId(),singlereplies.getContent(),position);
                            }
                            else {
                                Toast.makeText(activity, "يجب تسجيل الدخول !", Toast.LENGTH_LONG).show();

                            }




                            return false;
                        }
                    });

        } else {

            ((ProgressViewHolder) holder).cardView.setVisibility(View.VISIBLE);
            ((ProgressViewHolder) holder).avi.setVisibility(View.GONE);
            ((ProgressViewHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    getNextRepliesComment(urls);

                    ((ProgressViewHolder) holder).cardView.setVisibility(View.GONE);
                    ((ProgressViewHolder) holder).avi.setVisibility(View.VISIBLE);
                    ((ProgressViewHolder) holder).avi.show();

                }
            });

        }

    }


    public String getAtachment(replies singleReplie, int i) {
        if (singleReplie.getUser().getAttachment().size() != 0) {
            if (i <= singleReplie.getUser().getAttachment().size()) {
                return singleReplie.getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    public void dilog(final String RplaytId, final String UserId, final String content, final int Position){
        IsMyComment=false;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dilog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FIRST_SYSTEM_WINDOW;
        window.setAttributes(wlp);

        CardView cardView = (CardView) dialog.findViewById(R.id.action);
        final TextView actionText = (TextView) dialog.findViewById(R.id.actionText);
        final TextView cancelText = (TextView) dialog.findViewById(R.id.cancelText);
        final CardView cancel = (CardView) dialog.findViewById(R.id.cancel);

        actionText.setTypeface(Regular);
        cancelText.setTypeface(Regular);

        if(UserId.equalsIgnoreCase(pref.gitShared("id")))
        {
            actionText.setText("حذف الرد ");
            IsMyComment=true;
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!IsMyComment){
                    Stringparams.clear();
                    Stringparams.put("reply_id",RplaytId);
                    Stringparams.put("content",content);
                    AppHelper.report("report/reply",activity,Stringparams,"تم الابلاغ");
                }

                    else {

                        AppHelper.delete("replies/"+RplaytId,activity,"تم الحذف");
                        repliesList.remove(Position);
                        notifyItemRemoved(Position);
                        notifyItemRangeChanged(Position,repliesList.size());

                }



dialog.cancel();


            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });






        dialog.show();
    }


    public void setLoaded() {
        loading = false;
    }

    public void setUrl(String url) {
        urls = url;

    }

    @Override
    public int getItemCount() {
        return repliesList.size();
    }


    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void getNextRepliesComment(String url) {
        requests.StringRequest(Request.Method.GET, url, null, false, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {


                try {
                    repliesList.remove(0);
                    notifyItemRemoved(0);

                } catch (Exception e) {

                }
                JSONObject object = null;
                JSONObject message = null;
                JSONObject replies = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        replies = message.getJSONObject("replies");

                        urls = replies.getString("next_page_url");

                        repliesList.addAll(0, AppHelper.get_comment_replies(replies.getJSONArray("data")));

                        if (!urls.equalsIgnoreCase("null")) {
                            repliesList.add(0, null);
                            notifyItemInserted(0);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();


            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });
    }


    public class CommentItemViewHolder extends RecyclerView.ViewHolder {
        TextView name_replie_comment, comment_replie, like_replie, time_replie, date_replie;
        ImageView img_main_replaie_item;

        LinearLayout replaie_item;
        public CommentItemViewHolder(View view) {
            super(view);
            name_replie_comment = (TextView) view.findViewById(R.id.name_replie_comment);
            comment_replie = (TextView) view.findViewById(R.id.comment_replie);
            like_replie = (TextView) view.findViewById(R.id.like_replie);
            time_replie = (TextView) view.findViewById(R.id.time_replie);
            date_replie = (TextView) view.findViewById(R.id.date_replie);
            img_main_replaie_item = (ImageView) view.findViewById(R.id.img_main_replaie_item);
            replaie_item=(LinearLayout)view.findViewById(R.id.replaie_item);
           like_replie.setTypeface(Regular);
           time_replie.setTypeface(Regular);
           date_replie.setTypeface(Regular);
           name_replie_comment.setTypeface(Bold);
           comment_replie.setTypeface(Regular);


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;

        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment= (TextView) v.findViewById(R.id.reed_more_comment_text);
            reed_more_comment.setTypeface(Regular);
        }

    }


}
