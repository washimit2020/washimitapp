<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;



class Page extends Translatable
{

    public function posts()
    {
        return $this->hasMany(Post::class,"page_id");
    }
}