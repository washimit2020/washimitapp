package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 2/13/17.
 */

public class NotificationModel {
    public int id;
    public String type;
    public String read_at;
    public String created_at;
    public String title;
    public String msg;
    public news news;
    public  User user;
    public Comments comment;
}
