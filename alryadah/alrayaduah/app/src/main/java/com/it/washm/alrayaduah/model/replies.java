package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 1/14/17.
 */

public class replies {
    String  id;
    String  comment_id;
    String  content;
    String  created_at;
    String  updated_at;
    int  likes_count;
    boolean  is_liked;
    User user;

    public replies(String id, String comment_id, String content, String created_at, String updated_at, int likes_count, boolean is_liked, User user) {
        this.id = id;
        this.comment_id = comment_id;
        this.content = content;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.likes_count = likes_count;
        this.is_liked = is_liked;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(int likes_count) {
        this.likes_count = likes_count;
    }

    public boolean is_liked() {
        return is_liked;
    }

    public void setIs_liked(boolean is_liked) {
        this.is_liked = is_liked;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
