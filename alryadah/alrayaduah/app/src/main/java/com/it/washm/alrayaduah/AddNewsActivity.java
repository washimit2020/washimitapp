package com.it.washm.alrayaduah;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;

import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.interfase.MultipartCallback;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.model.NewsSubType;
import com.it.washm.alrayaduah.model.NewsType;


public class AddNewsActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.sendNews)
    TextView sendNews;
    @BindView(R.id.addNews)
    TextView addNews;
    @BindView(R.id.news_sub)
    TextView news_sub;
    @BindView(R.id.main)
    TextView main;
    @BindView(R.id.sub)
    TextView sub;
    @BindView(R.id.SubType)
    Spinner SubType;

    @BindView(R.id.MainType)
    Spinner MainType;

    @BindView(R.id.lsubNewsImgOrVideo1)
    LinearLayout lsubNewsImgOrVideo1;

    @BindView(R.id.lsubNewsImgOrVideo2)
    LinearLayout lsubNewsImgOrVideo2;

    @BindView(R.id.lsubNewsImgOrVideo3)
    LinearLayout lsubNewsImgOrVideo3;

    @BindView(R.id.subNewsvideoOrVideo1)
    VideoView subNewsvideo1;

    @BindView(R.id.subNewsvideoOrVideo2)
    VideoView subNewsvideo2;

    @BindView(R.id.subNewsvideoOrVideo3)
    VideoView subNewsvideo3;

    @BindView(R.id.cancelNews)
    TextView cancelNews;

    @BindView(R.id.ManiNewsImg)
    ImageView ManiNewsImg;

    @BindView(R.id.subNewsImgOrVideo1)
    ImageView subNewsImgOrVideo1;

    @BindView(R.id.subNewsImgOrVideo2)
    ImageView subNewsImgOrVideo2;

    @BindView(R.id.subNewsImgOrVideo3)
    ImageView subNewsImgOrVideo3;

    @BindView(R.id.tile_news_main_text)
    TextInputLayout tile_news_main_text;

    @BindView(R.id.news_main_text)
    AutoCompleteTextView news_main_text;

    @BindView(R.id.tile_add_text)
    TextInputLayout tile_add_text;

    @BindView(R.id.add_text)
    AutoCompleteTextView add_text;

    @BindView(R.id.RelativeLayout)
    RelativeLayout relativeLayout;

    @BindView(R.id.des_img)
    ImageView des_img;

    @BindView(R.id.des_img1)
    ImageView des_img1;

    @BindView(R.id.des_img2)
    ImageView des_img2;

    @BindView(R.id.des_img3)
    ImageView des_img3;


    Gson gson;
    Requests requests;
    List<Part> Prams;
    List<String> Paths;
    ArrayList<NewsType> NewsMainAndSubType;

    String MainImgPath = "", newsMainText, addText, mainNewsId = "", subNewsId = "";
    NewsType newsType;
    NewsSubType subType;
    CustamViewFont viewFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_news);
        ButterKnife.bind(this);
        viewFont = new CustamViewFont(this, "Cairo-SemiBold.ttf");
        setFont();

        gson = new Gson();
        NewsMainAndSubType = new ArrayList<>();
        requests = new Requests(getApplicationContext(), this);

        getNewsTypeAndSubType();

        MainType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                newsType = (NewsType) MainType.getSelectedItem();

                mainNewsId = newsType.getId();
                Log.d("MAINid", mainNewsId);

                ArrayAdapter<NewsSubType> adapter = new ArrayAdapter<NewsSubType>(AddNewsActivity.this, android.R.layout.simple_spinner_item, NewsMainAndSubType.get(position).getSub());

                SubType.setAdapter(adapter);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // mainNewsId="";

            }
        });

        SubType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                subType = (NewsSubType) SubType.getSelectedItem();

                subNewsId = subType.getId();

                Log.d("SUBid", subNewsId);

                mainNewsId = newsType.getId();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                subNewsId = "";
            }
        });


        Paths = new ArrayList();
        Prams = new ArrayList();

        sendNews.setOnClickListener(this);

        cancelNews.setOnClickListener(this);

        ManiNewsImg.setOnClickListener(this);

        lsubNewsImgOrVideo1.setOnClickListener(this);

        lsubNewsImgOrVideo2.setOnClickListener(this);

        lsubNewsImgOrVideo3.setOnClickListener(this);
    }

    public void setFont() {
        viewFont.setfont(tile_add_text);
        viewFont.setfont(tile_news_main_text);
        viewFont.setfont(news_sub);
        viewFont.setfont(main);
        viewFont.setfont(sub);
        viewFont.setfont(cancelNews, "Cairo-Regular.ttf");
        viewFont.setfont(sendNews, "Cairo-Regular.ttf");
        viewFont.setfont(addNews, "Cairo-Regular.ttf");


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {
            Uri uri = data.getData();
            String p = AppHelper.getPathfromURI(this, uri);
            File file = new File(p);
            File compressedImage = null;

            if (uri.toString().contains("images")) {
                // Compress image in main thread using custom Compressor
                compressedImage = new Compressor.Builder(this)
                        .setMaxWidth(640)
                        .setMaxHeight(480)
                        .setQuality(75)
                        .setCompressFormat(Bitmap.CompressFormat.JPEG)
                        .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES).getAbsolutePath())
                        .build()
                        .compressToFile(file);


            }

            if (requestCode == 0) {
                if (fileSize(compressedImage)) {
                    des_img.setVisibility(View.GONE);

                } else {
                    des_img.setVisibility(View.VISIBLE);

                }
                MainImgPath = compressedImage.getAbsolutePath();
                ManiNewsImg.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));

            } else if (requestCode == 1) {
                lsubNewsImgOrVideo2.setVisibility(View.VISIBLE);
                setRes(1, uri, p, compressedImage, file, subNewsImgOrVideo1, subNewsvideo1, des_img1);

            } else if (requestCode == 2) {

                lsubNewsImgOrVideo3.setVisibility(View.VISIBLE);
                setRes(2, uri, p, compressedImage, file, subNewsImgOrVideo2, subNewsvideo2, des_img2);

            } else if (requestCode == 3) {
                setRes(3, uri, p, compressedImage, file, subNewsImgOrVideo3, subNewsvideo3, des_img3);
            }


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للتطبيق حتي تتمكن من نشر الخبر ", Toast.LENGTH_LONG).show();
        } else {
            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, requestCode);
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.sendNews:
                if (validate()) {
                    if (AppHelper.isOnline(AddNewsActivity.this)) {

                        Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());

                        Prams.add(new FilePart("main_attachment", new File(MainImgPath)));
                        Prams.add(new StringPart("title", news_main_text.getText().toString()));
                        Prams.add(new StringPart("content", add_text.getText().toString()));

                        for (int i = 0; i < Paths.size(); i++) {
                            File file = new File(Paths.get(i));

                            if (fileSize(file))
                                Prams.add(new FilePart("attachments[" + (i) + "]", file));


                        }

                        Prams.add(new StringPart("main_type", mainNewsId));
                        Prams.add(new StringPart("sub_type", subNewsId));

                        Log.d("mainNewsId", mainNewsId);
                        Log.d("subNewsId", subNewsId);

                        requests.MultipartRequest("news", "جاري نشر الخبر...", Prams, new MultipartCallback() {

                            @Override
                            public void onSuccess(String result) {
                                JSONObject object;
                                try {
                                    object = new JSONObject(result);
                                    Toast.makeText(getApplicationContext(), "" + result, Toast.LENGTH_LONG).show();

                                    AppHelper.showDialog(AddNewsActivity.this, "سيتم نشرة بعد القبول ", "", "", false, new dialogCallback() {
                                        @Override
                                        public void ok(Dialog dialog) {
                                            AddNewsActivity.this.finish();
                                            dialog.cancel();
                                        }

                                        @Override
                                        public void hidden(Dialog dialog) {

                                        }
                                    });


                                    // AddNewsActivity.this.finish();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void onRequestError(String errorMessage) {

                            }
                        });
                    } else {
                        Snackbar.make(relativeLayout, "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }


                }
                break;

            case R.id.cancelNews:
                this.finish();
                break;

            case R.id.ManiNewsImg:
                showImg(0);
                break;
            case R.id.lsubNewsImgOrVideo1:
                showImgOrVideo(1);
                break;
            case R.id.lsubNewsImgOrVideo2:
                showImgOrVideo(2);
                break;
            case R.id.lsubNewsImgOrVideo3:
                showImgOrVideo(3);
                break;

        }


    }


    public void showImgOrVideo(int p) {


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, p);
        } else {

            Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/* video/*");
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
            chooserIntent.putExtra
                    (
                            Intent.EXTRA_INITIAL_INTENTS,
                            new Intent[]{takePhotoIntent, takeVideoIntent}
                    );

            startActivityForResult(chooserIntent, p);
        }
    }


    public void showImg(int p) {


        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, p);
        } else {
            Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/*");
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
            chooserIntent.putExtra
                    (
                            Intent.EXTRA_INITIAL_INTENTS,
                            new Intent[]{takePhotoIntent}
                    );

            startActivityForResult(chooserIntent, p);
        }
    }

    @Override
    protected void onStop() {
        Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());
        super.onStop();
    }

    public boolean validate() {
        boolean valid = true;

        newsMainText = news_main_text.getText().toString();
        addText = add_text.getText().toString();

        if (newsMainText.equalsIgnoreCase("")) {
            news_main_text.setError("أدخل عنوان للخبر");
            valid = false;
        } else {
            news_main_text.setError(null);
        }

        if (addText.equalsIgnoreCase("")) {
            add_text.setError("أدخل تفاصل الخبر ");
            valid = false;
        } else {
            add_text.setError(null);
        }

        if (MainImgPath.equalsIgnoreCase("")) {
            Snackbar.make(relativeLayout, "يجب اختيار صوره رئيسية للخبر", BaseTransientBottomBar.LENGTH_LONG).show();

        }


        return valid;
    }


    private void getNewsTypeAndSubType() {

        requests.StringRequest(Request.Method.GET, "news-types", "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                Log.d("", result);
                JSONObject object = null;
                JSONObject message = null;
                JSONArray news_main_types = null;
                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        news_main_types = message.getJSONArray("news_main_types");

                        for (int i = 0; i < news_main_types.length(); i++) {
                            if (news_main_types.getJSONObject(i).getString("is_active").equalsIgnoreCase("1")) {
                                NewsMainAndSubType.add(gson.fromJson(news_main_types.getJSONObject(i).toString(), NewsType.class));
                            }
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                ArrayAdapter<NewsType> adapter = new ArrayAdapter<NewsType>(AddNewsActivity.this, android.R.layout.simple_spinner_item, NewsMainAndSubType);
                MainType.setAdapter(adapter);
            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });

    }


    public boolean fileSize(File file) {
        if ((file.length() / 1024) < 5000) {

            return true;

        } else {
            Snackbar.make(relativeLayout, "حجم الملف كبير لن يتم رفعه في حال النشر !", BaseTransientBottomBar.LENGTH_LONG).show();
            return false;

        }


    }


    public void setRes(int i, Uri uri, String p, File compressedImage, File file, ImageView imageView, VideoView videoView, ImageView des) {

        if (uri.toString().contains("images")) {
            videoView.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));
            if (fileSize(compressedImage)) {
                des.setVisibility(View.GONE);

            } else {
                des.setVisibility(View.VISIBLE);

            }
            if (Paths.size() >= i) {
                Paths.remove(i - 1);
                Paths.add(compressedImage.getAbsolutePath());
            } else {
                Paths.add(compressedImage.getAbsolutePath());

            }
        } else if (uri.toString().contains("video")) {
            videoView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.GONE);
            videoView.setVideoPath(p);
            if (fileSize(file)) {
                des.setVisibility(View.GONE);

            } else {
                des.setVisibility(View.VISIBLE);

            }
            videoView.seekTo(500);     // 100 milliseconds (0.1 s) into the clip.
            if (Paths.size() >= i) {
                Paths.remove(i - 1);
                Paths.add(p);
            } else {
                Paths.add(p);

            }
        }
    }


}
