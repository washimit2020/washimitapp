package com.washimit.it.ryadah.halper;

/**
 * Created by mohammad on 4/12/2017.
 */

public class Constant {

    public static final int _TYPE_GUEST = 0;

    //public static final String _IP_URL = "http://192.168.0.101/reyadah/public/api/";
    public static final String _IP_URL = "http://reyadah.wojoooh.com/api/";
    public static final String _PRODUCT_URL = _IP_URL + "attachment/product/";
    public static final String _COVER_URL = _IP_URL + "attachment/cover/";
    public static final String _AVATAR_URL = _IP_URL + "attachment/avatar/";
    public static final String _FESTIVAL_URL = _IP_URL + "attachment/festival/";

    public static final String _NOTIFICATIONS_COMMENT = "App\\Notifications\\commentableCommented";
    public static final String _NOTIFICATIONS_PRODUCT_LIKE = "App\\Notifications\\ProductLiked";
    public static final String _NOTIFICATIONS_PRODUCT_SHARA_ANSWER = "App\\Notifications\\ProductSharaRequestAnswered";
    public static final String _NOTIFICATIONS_PRODUCT_SHARA_RECEIVED = "App\\Notifications\\ProductSharaRequestReceived";
    public static final int _TYPE_PICTURE = 2;
    public static final int _TYPE_MAP = 1;


    public static final int _TYPE_SIGN_UP = 1;
    public static final int _TYPE_ADD_PRODUCT = 2;


    public static final int _TYPE_VISITOR = 0;
    public static final int _TYPE_STORE = 1;
    public static final int _TYPE_SHOW = 2;
    public static final int _TYPE_COSTUMER = 3;
    public static final int _TYPE_FESTIVAL = 4;
    public static final int _TYPE_PRODUCT = 5;
    public static final int _PAGE_STORE = 1;
    public static final int _PAGE_SHOW = 2;
    public static final int _PAGE_FESTIVAL = 4;
    public static final int _PAGE_PRODUCT = 5;
    public static final int _PAGE_EXPLORER = 6;
    public static final int _PAGE_CUSTOMER_DETAILS = 6;
    public static final int _PAGE_SHOW_DETAILS = 7;
    public static final int _PAGE_STORE_DETAILS = 8;
    public static final int _PAGE_HOME = 9;
    public static final int _TYPE_USER_DETAILS = 10;
    public static final int _TYPE_FAEVARET_DETAILS = 11;
    public static final int _TYPE_BRANCH = 12;


    public static final int _TYPE_MY_PRODUCT_DETAILS = 13;
    public static final int _TYPE_REQUEST_SHOW_PRODUCT_STORE_DETAILS = 14;
    public static final int _TYPE_REQUEST_STORE_PRODUCT_SHOW_DETAILS = 15;
    public static final int _TYPE_USER_PROFILE_INFO = 15;

    public static final int _TYPE_AVATAR_IMAGE = 6;
    public static final int _TYPE_COVER_IMAGE = 7;
    public static final int _TYPE_PRODUCT_ATTACHMENT = 8;
    public static final int _TYPE_PRODUCT_LIKE = 9;
    public static final int _TYPE_PRODUCT_WISHLIST = 10;
    public static final int _TYPE_FOLLOW_USER = 11;
    public static final int _TYPE_FOLLOW_FESTIVAL = 12;


    public static final int _STATUS_ACTIVE = 1;
    public static final int _STATUS_INACTIVE = 2;
    public static final int _STATUS_DECLINED = 5;

    public static final int _STATUS_HIDDEN = 3;
    public static final int _STATUS_BLOCKED = 4;
    public static final int _STATUS_DELETED = 6;


    public static final int NORMAL_REGISTER = 1;
    public static final int SOCIAL_REGISTER = 2;

    public static final int linearLayoutManager = 1;
    public static final int gridLayoutManager = 2;

    //share type
    public static final String _TYPE_SHARE_USERS = "users";
    public static final String _TYPE_SHARE_BRANCH = "branches";


    //navigation hider action
    public static final int _PAGE_MY_PROFILE = 1;
    public static final int _PAGE_MY_BRANCH = 2;
    public static final int _PAGE_SHARCH = 3;
    public static final int _PAGE_BLOCK = 4;
    public static final int _PAGE_WISHLISTED = 5;
    public static final int _SHARE = 6;
    public static final int _HALPER = 7;
    public static final int SETTINGS = 8;
    public static final int _PAGE_LOGOUT = 9;
    public static final int _PAGE_ABOUT_DEVLOPER = 10;


    //comment type
    public static final String _TYPE_PRODUCT_COMMENT = "products";
    public static final String _TYPE_FESTIVAL_COMMENT = "festivals";


    //search type

    public static final int _SEARCH_TYPE_ALL = 1;
    public static final int _SEARCH_TYPE_PRODUCT = 2;
    public static final int _SEARCH_TYPE_STORE = 3;
    public static final int _SEARCH_TYPE_SHOW = 4;
    public static final int _SEARCH_TYPE_FESTIVAL = 5;
    public static final String _SEARCH_SORT_ALL = "";
    public static final String _SEARCH_SORT_PRICE = "price";
    public static final String _SEARCH_SORT_RATING = "rating";


}
