//
//  Book_PageViewController.swift
//  al-haj
//
//  Created by WASHM on 7/24/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite
class Book_PageViewController: UIPageViewController {
    
    var timer = Timer()
    var seconds:Int = 0
    var offset = 0
    let limit = 100
    var has_next = true
    var favIcon = UIBarButtonItem(image: #imageLiteral(resourceName: "liked"), style: .plain , target: nil, action: nil)
    var comment = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_note"), style: .plain , target: nil , action: nil)
    var currentIndex = 0
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Utils.shared.save_time(seconds: seconds)
        timer.invalidate()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.seconds = Utils.shared.get_time()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (timer) in
            self.seconds += 1
        })
        viewControllers?.forEach({ (vc) in
            vc.viewWillAppear(false)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favIcon.action = #selector(addToFav)
        favIcon.target = self
        comment.action = #selector(add_comment(_:))
        comment.target = self
        favIcon.tintColor = .white
        comment.tintColor = .white
        dataSource = self
        delegate = self
        slideMenuController()?.delegate = self
        title = " "
        //        self.parent?.navigationItem.leftBarButtonItems = [favIcon,comment]
        view.backgroundColor = Utils.shared.is_dark_mode() ? Utils.shared.dark_mode_background : Utils.shared.light_mode_background
        // Do any additional setup after loading the view.
        for recognizer in gestureRecognizers {
            if recognizer is UITapGestureRecognizer {
                recognizer.isEnabled = false
            }
        }
        
        //        get_pages()
        if Utils.shared.pages.first != nil{
            let id = Utils.shared.get_last_page()
            let index = Utils.shared.pages.index(where: { (page) -> Bool in
                return page.id == id
            })

            setViewControllers([newViewController(index ?? 0)],
                               direction: .forward,
                               animated: true,
                               completion: nil)
            change_icon()
        }
        
    }
    
    func newViewController(_ index:Int) -> UIViewController {
        //        let viewController = (storyboard?.instantiateViewController(withIdentifier: "page")  as! UINavigationController)
        let viewController = PageController_ViewController()
        let item = Utils.shared.pages[index]
        //        (viewController.viewControllers.first as! Page_TableViewController).page = item
        viewController.page = item
//        viewController.currentPage = index + 1
        return viewController
    }
    
    
    
    
    //    func get_pages (until:Int? = nil){
    //
    //        do {
    //            let rows = try DatabaseUtils.shared.db!.prepare(DatabaseUtils.shared.pages_t.limit(limit, offset: offset * limit)).map({
    //                return Page($0)
    //            })
    //            let count = try  DatabaseUtils.shared.db!.scalar(DatabaseUtils.shared.pages_t.select(Expressions.id.count))
    //            if count == 0 {
    //                return
    //            }
    //            if rows.count == 0 {
    //                has_next = false
    //            }
    //            Utils.shared.pages.append(contentsOf: rows)
    //            offset += 1
    //            if let id = until {
    //                if id > count{
    //                    setViewControllers([newViewController(Utils.shared.pages.count - 1)],
    //                                       direction: .reverse,
    //                                       animated: true,
    //                                       completion: nil)
    //                    return
    //                }
    //                if id > Utils.shared.pages.count{
    //                    get_pages(until:id)
    //                }else{
    //                    setViewControllers([newViewController(id - 1)],
    //                                       direction: .reverse,
    //                                       animated: true,
    //                                       completion: nil)
    //                }
    //            }
    //        }catch(let exp){
    //            print(exp)
    //        }
    //    }
    
    func check_if_page_exist(page:Int){
        for  i in 0..<page{
            setViewControllers([newViewController(i)],
                               direction: .reverse,
                               animated: false,
                               completion: nil)
        }
        //        if page > Utils.shared.pages.count{
        //            get_pages(until: page)
        //        }else{
        //            setViewControllers([newViewController(page - 1)],
        //                               direction: .reverse,
        //                               animated: true,
        //                               completion: nil)
        //        }
    }
    
    @objc func add_comment(_ sender: Any) {
        let comment = Comment_ViewController()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        comment.preferredContentSize.height = self.view.frame.height / 2
        comment.page = Utils.shared.pages[currentIndex]
        comment.updatePage = {
            Utils.shared.pages[self.currentIndex] = $0
        }
        alert.setValue(comment, forKeyPath: "contentViewController")
        present(alert, animated: true, completion: nil)
    }
    
    func change_icon(){
        if Utils.shared.pages[currentIndex].is_wishlisted == 1{
            favIcon.image = #imageLiteral(resourceName: "disliked")
            //            print("liked")
            
        }else{
            favIcon.image = #imageLiteral(resourceName: "liked")
            //            print("unliked")
        }
    }
    
    @objc public func addToFav(){
        Utils.shared.pages[currentIndex].is_wishlisted = Utils.shared.pages[currentIndex].is_wishlisted == 0 ? 1 : 0
        change_icon( )
        DispatchQueue.global(qos: .background).async {
            do {
                let table = DatabaseUtils.shared.pages_t.filter(Expressions.id == Utils.shared.pages[self.currentIndex].id!)
                try DatabaseUtils.shared.db!.run(table.update(Expressions.is_wishlisted <- Utils.shared.pages[self.currentIndex].is_wishlisted))
                
            }catch(let exp){
                print("update fav exp: \(exp)")
            }
        }
    }
    
    
    
}
extension Book_PageViewController:UIPageViewControllerDataSource,UIPageViewControllerDelegate{
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        //        let pageController = (viewController  as! UINavigationController).viewControllers.first as! Page_TableViewController
//        let pageController = viewController   as! Page_TableViewController
        let pageController = viewController   as! PageController_ViewController

        
        let index = getIndex(controller:pageController)
        if index >= Utils.shared.pages.count - 1 {
            return nil
        }
        
        //        if index == Utils.shared.pages.count - 2 , has_next{
        //            get_pages()
        //        }
        
        //        Utils.shared.pages[index] = pageController.page
        
        return newViewController(index + 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
//        let pageController = viewController   as! Page_TableViewController
        let pageController = viewController   as! PageController_ViewController
        let index = getIndex(controller:pageController)
        
        if index <= 0 {
            return nil
        }
        //        Utils.shared.pages[index] = pageController.page
        
        return newViewController(index - 1 )
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool){
        if !finished, !completed{
            return
        }
        let controller = pageViewController.viewControllers!.first   as! PageController_ViewController
//        let controller = pageViewController.viewControllers!.first as! Page_TableViewController
        currentIndex = getIndex(controller:controller)
        
        change_icon()
    }
    
//    private func getIndex(controller:Page_TableViewController)->Int{
//        let page_id = controller.page.id
//        let index = Utils.shared.pages.index(where: { (page) -> Bool in
//            page.id == page_id
//        }) ?? 0
//
//        return index
//    }
    private func getIndex(controller:PageController_ViewController)->Int{
        guard let page_id = controller.vm!.page.id else {return 0}
        let index = Utils.shared.pages.index(where: { (page) -> Bool in
            page.id == page_id
        }) ?? 0
        
        return index
    }
}

extension Book_PageViewController : SlideMenuControllerDelegate{
    func rightWillOpen() {
        BookPan(enable: false)
    }
    
    func rightDidClose() {
        BookPan(enable: true )
    }
    
    func BookPan(enable:Bool){
        for recognizer in gestureRecognizers{
            if recognizer is UIPanGestureRecognizer{
                recognizer.isEnabled = enable
            }
        }
    }
}
