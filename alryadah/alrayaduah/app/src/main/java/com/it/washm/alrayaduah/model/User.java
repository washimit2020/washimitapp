package com.it.washm.alrayaduah.model;

import java.util.ArrayList;

/**
 * Created by washm on 1/9/17.
 */

public class User {
    String id;
    String first_name;
    String last_name;
    String nick_name;
    String email;
    String gender;
    String preferred_language;
    //role info
    UserRole role;
    //user attachment
    ArrayList<Attachment> attachment;

    public User(String id, String first_name, String last_name, String nick_name, String email, String gender, String preferred_language, UserRole role, ArrayList<Attachment> attachment) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.nick_name = nick_name;
        this.email = email;
        this.gender = gender;
        this.preferred_language = preferred_language;
        this.role = role;
        this.attachment = attachment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getNick_name() {
        return nick_name;
    }

    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPreferred_language() {
        return preferred_language;
    }

    public void setPreferred_language(String preferred_language) {
        this.preferred_language = preferred_language;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<Attachment> getAttachment() {
        return attachment;
    }

    public void setAttachment(ArrayList<Attachment> attachment) {
        this.attachment = attachment;
    }
}