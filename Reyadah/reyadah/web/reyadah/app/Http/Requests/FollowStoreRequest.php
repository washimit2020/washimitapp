<?php

namespace App\Http\Requests;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class FollowStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $arr = [Constants::_TYPE_FOLLOW_USER, Constants::_TYPE_FOLLOW_FESTIVAL];
        $in = make_validation_array_in($arr);
        return [
            'follow_type' => "required|in:$in",
            'user_id' => "required_if:follow_type,{$arr[0]}|" . make_place_validation_string(),
            'festival_id' => "required_if:follow_type,{$arr[1]}|".make_festival_validation_string(),
        ];
    }
}
