package com.washimit.it.ryadah.Interface;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mohammad on 4/17/2017.
 */

public interface LocationCallback {
    public void setLatLng(LatLng latLng);
}
