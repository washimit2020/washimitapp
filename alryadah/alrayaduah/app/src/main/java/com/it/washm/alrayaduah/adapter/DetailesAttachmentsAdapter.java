package com.it.washm.alrayaduah.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.model.Attachment;

/**
 * Created by washm on 1/21/17.
 */

public class DetailesAttachmentsAdapter extends RecyclerView.Adapter {

    private final int Img_ITEM = 0;
    private final int VIDEO_ITEM = 1;
    ArrayList<Attachment> attachments;
    Context context;

    public DetailesAttachmentsAdapter(Context context, ArrayList<Attachment> attachments) {
        this.attachments = attachments;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == Img_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.img_item, parent, false);

            vh = new ImgItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.video_item, parent, false);

            vh = new VideoViewHolder(v);
        }
        return vh;


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Attachment SingleAttachment = (Attachment) attachments.get(position);

        if (holder instanceof ImgItemViewHolder) {


            Picasso.with(context).load(Links.UrlImg + SingleAttachment.getPath()).fit().placeholder(R.drawable.ic_blank_image).into(((ImgItemViewHolder) holder).img);

            Log.d("SingelAttachment", SingleAttachment.getPath());


        } else {

            ((VideoViewHolder) holder).videoView.setVideoURI(Uri.parse(Links.UrlImg + SingleAttachment.getPath()));
//             MediaController  ctlr = new MediaController(context);
//            ctlr.setMediaPlayer( ((VideoViewHolder)holder).videoView);

//            ((VideoViewHolder)holder).videoView.setMediaController(ctlr);
            ((VideoViewHolder) holder).videoView.requestFocus();
            ((VideoViewHolder) holder).videoView.seekTo(500);     // 100 milliseconds (0.1 s) into the clip.
//          ((VideoViewHolder)holder).videoView.setBackgroundColor(Color.argb(0, 0, 0, 0));
            ((VideoViewHolder) holder).videoView.setDrawingCacheEnabled(true);
            ((VideoViewHolder) holder).des_img1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Links.UrlImg + SingleAttachment.getPath()));
                    intent.setDataAndType(Uri.parse(Links.UrlImg + SingleAttachment.getPath()), "video/mp4");
                    context.startActivity(intent);
                }
            });
        }


    }

    @Override
    public int getItemViewType(int position) {
        Log.d("jhjhhjkh", "getItemViewType: " + attachments.get(position).getMime_type());
        return attachments.get(position).getMime_type().contains("image") ? Img_ITEM : VIDEO_ITEM;
    }

    @Override
    public int getItemCount() {
        return attachments.size();
    }


    public class ImgItemViewHolder extends RecyclerView.ViewHolder {

        ImageView img;

        public ImgItemViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.image);
        }

    }
    public class VideoViewHolder extends RecyclerView.ViewHolder {
        VideoView videoView;
        ImageView des_img1;

        public VideoViewHolder(View itemView) {
            super(itemView);
            videoView = (VideoView) itemView.findViewById(R.id.video);
           des_img1=(ImageView)itemView.findViewById(R.id.des_img1);
        }

    }
}
