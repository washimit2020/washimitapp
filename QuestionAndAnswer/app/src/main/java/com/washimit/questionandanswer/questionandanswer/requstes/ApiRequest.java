package com.washimit.questionandanswer.questionandanswer.requstes;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
import com.washimit.questionandanswer.questionandanswer.activity.LogInActivity;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mohammad on 5/31/2017.
 */

public class ApiRequest {
    public static RequestQueue queue;
    Context context;

    //CHANGE THE URL REQUEST WEN U NED TO FUK APP
    //public final String URL = "http://192.168.0.101/questioneer/public/";
    public static final String URL = "http://questioneer.wojoooh.com/";
    public static final String URL_QUESTION = URL + "/attachment/question/";
    public static final String URL_ANSWER = URL + "/attachment/answer/";

    PreferenceClass pref;

    public ApiRequest(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
        }
        this.context = context;
        pref = new PreferenceClass(context);
    }

    public void setRequst(String url, final HashMap<String, String> map, final RequestCallback callback) {
        final StringRequest request = new StringRequest(Request.Method.POST, URL + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    callback.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.d("asfffas", "TimeoutError");
                } else if (error instanceof AuthFailureError) {
                    //TODO
                    Log.d("asfffas", "AuthFailureError");
                } else if (error instanceof ServerError) {
                    Log.d("asfffas", "ServerError");

                    //TODO
                } else if (error instanceof NetworkError) {
                    Log.d("asfffas", "NetworkError");
                    //TODO
                } else if (error instanceof ParseError) {
                    Log.d("asfffas", "ParseError");
                    //TODO
                }

                 callback.ErrorListener();


            }
        })

        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map1 = new HashMap<>();
                map1.put("app-key", "xxBVIZ4Jch57B9Tn0O9sC83nUphvqz1b");
                if (pref.getUser() != null) {
                    map1.put("Authorization", "Bearer " + pref.getUser().getApi_token() + "");
                }
                return map1;
            }
        };
        queue.add(request);

    }

    public void getRequst(String url, final RequestCallback callback) {
        final StringRequest request = new StringRequest(Request.Method.GET, URL + url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    callback.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.ErrorListener();
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map1 = new HashMap<>();
                map1.put("app-key", "xxBVIZ4Jch57B9Tn0O9sC83nUphvqz1b");
                if (pref.getUser() != null) {
                    map1.put("Authorization", "Bearer " + pref.getUser().getApi_token() + "");
                }
                return map1;
            }
        };
        queue.add(request);

    }


    public void getNextRequst(String url, final RequestCallback callback) {
        final StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    callback.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.ErrorListener();
            }
        }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> map1 = new HashMap<>();
                map1.put("app-key", "xxBVIZ4Jch57B9Tn0O9sC83nUphvqz1b");
                if (pref.getUser() != null) {
                    map1.put("Authorization", "Bearer " + pref.getUser().getApi_token() + "");
                }
                return map1;
            }
        };
        queue.add(request);

    }


}




