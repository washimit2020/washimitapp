<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;
class NewsStatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news_statuses')->insert([
            [
                "id"=>Constants::NEWS_STATUS_APPROVED,
                "name_en"=>"Approved",
                "name_ar"=>"مقبول",
            ],
            [
                "id"=>Constants::NEWS_STATUS_DISAPPROVED,
                "name_en"=>"Disapproved",
                "name_ar"=>"مرفوض",
            ],
            [
                "id"=>Constants::NEWS_STATUS_PENDING,
                "name_en"=>"Pending",
                "name_ar"=>"قيد الدراسة",
            ]
        ]);
    }
}
