<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommonTypes extends Model
{
    /**
     * only the fillable column
     *
     * @var array
     */
    protected $fillable = ['title', 'created_at', 'updated_at'];

    /**
     * has many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the translated title.
     *
     * @param  string  $value
     * @return string
     */
    public function getTitleAttribute($value)
    {
        return trans($value);
    }
}
