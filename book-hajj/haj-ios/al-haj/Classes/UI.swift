//
//  C_UILabel.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

fileprivate var font_name = Utils.shared.get_font_name()
class C_UILabel: UILabel {
  
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        self.font = UIFont(name: font_name, size: self.font.pointSize)
        
    }
}

@IBDesignable class TitleLabel: C_UILabel {
    override func commonInit(){
        let size = self.font.pointSize
        self.font = UIFont.titleFont?.withSize(size)
        textColor = .tintColor
    }
}
@IBDesignable class BodyLabel: C_UILabel {
    override func commonInit(){
        let size = self.font.pointSize
        self.font = UIFont.bodyFont?.withSize(size)
    }
}

@IBDesignable class NumberLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        backgroundColor = .tintColor
        textColor = .white
    }
}

@IBDesignable class NumberView: UIView {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        backgroundColor = .tintColor
    }
}


class C_UITextView: UITextView {
  
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.commonInit()
    }
    
    func commonInit(){
        self.font = UIFont(name: font_name, size: self.font?.pointSize ?? 14)
    }
}

class C_UITextField: UITextField {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        self.font = UIFont(name: font_name, size: self.font?.pointSize ?? 14)
    }
}
class C_BarButton:UIBarButtonItem{
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    override init() {
        super.init()
        self.commonInit()
    }
    func commonInit(){
        self.setTitleTextAttributes([NSAttributedStringKey.font : UIFont.titleFont], for: .normal)
        self.setTitleTextAttributes([NSAttributedStringKey.font : UIFont.titleFont], for: .disabled)
    }
}
