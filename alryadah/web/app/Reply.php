<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends NewsWithUser
{
    public $incrementing = true;
    /**
     * @var array primary key
     */
    protected $primaryKey = "id";

    /**
     * @var array fillable key
     */
    protected $fillable = [
        "comment_id", "user_id", "content"
    ];

    /**
     * has many likes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->hasMany('App\ReplyLike', 'reply_id');
    }

    /**
     * belongs to comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo('App\Comment', 'comment_id');
    }


}
