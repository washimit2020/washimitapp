package com.it.washim.bookrmamdan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.it.washim.bookrmamdan.R;


/**
 * A simple {@link Fragment} subclass.
 */


public class ApoutFragment extends Fragment {
    TextView apout;

    public ApoutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.fragment_apout, container, false);


        return view;
    }




}
