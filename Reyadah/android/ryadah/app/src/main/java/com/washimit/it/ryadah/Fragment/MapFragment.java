package com.washimit.it.ryadah.Fragment;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Shares;
import com.washimit.it.ryadah.model.product.SharesBranch;
import com.washimit.it.ryadah.model.user.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    MapView mMapView;
    private GoogleMap mMap;
    User user;
    Gson gson;
    ApiRequest request;
    ArrayList<Object> objects;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        gson = new Gson();
        objects = new ArrayList<>();
        request = new ApiRequest(getActivity());
        user = gson.fromJson(getArguments().getString("user"), User.class);
        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        // For dropping a marker at a point on the Map
        LatLng mainUser = new LatLng(user.getLat(), user.getLng());
        MarkerOptions markerOptions = new MarkerOptions().position(mainUser);
      //  markerOptions.icon(BitmapDescriptorFactory.fromResource(AapHalper.userMapIconType(getArguments().getInt("user_type_id"))));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(getActivity(),AapHalper.userMapIconType(getArguments().getInt("user_type_id")))));
        mMap.addMarker(markerOptions);

        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(mainUser).zoom(12).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));




        UpdateUi(getArguments().getInt("user_type_id"));


    }

    public void UpdateUi(int userId) {
        switch (userId) {
            case Constant._TYPE_STORE:
                getSharesUser(getArguments().getInt("id"));

                break;

            case Constant._TYPE_SHOW:
                getSharesBranch(getArguments().getInt("id"));
                break;


        }


    }


    public void getSharesUser(int id) {

        request.MultipartRequest(Request.Method.GET, false, "product/shares/" + id + "/" + Constant._TYPE_SHARE_USERS, null, new MultipartCallback() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onSuccess(String result) throws JSONException {

                objects.clear();

                JSONObject object = null;
                JSONObject msg = null;
                JSONObject shares = null;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");

                objects.addAll((Collection<Shares>) gson.fromJson(msg.getString("shares"), new TypeToken<ArrayList<Shares>>() {
                }.getType()));


                for (int i = 0; i < objects.size(); i++) {
              //      mMap.addMarker(new MarkerOptions().position(new LatLng(((Shares) objects.get(i)).getShareable().getLat(), ((Shares) objects.get(i)).getShareable().getLng())).icon(BitmapDescriptorFactory.fromResource());
                    mMap.addMarker(new MarkerOptions().position(new LatLng(((Shares) objects.get(i)).getShareable().getLat(), ((Shares) objects.get(i)).getShareable().getLng())).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(getActivity(),AapHalper.userMapIconType(((Shares) objects.get(i)).getShareable().getType().getId())))));

                }

            }

            @Override
            public void onRequestError(String errorMessage) {

                Log.d("errorMessage", errorMessage);


            }
        });
    }

    public void getSharesBranch(int id) {

        request.MultipartRequest(Request.Method.GET, false, "product/shares/" + id + "/" + Constant._TYPE_SHARE_BRANCH, null, new MultipartCallback() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onSuccess(String result) throws JSONException {
                objects.clear();
                JSONObject object = null;
                JSONObject msg = null;
                JSONObject shares = null;

                object = new JSONObject(result);

                msg = object.getJSONObject("msg");


                objects.addAll((Collection<SharesBranch>) gson.fromJson(msg.getString("shares"), new TypeToken<ArrayList<SharesBranch>>() {
                }.getType()));
                for (int i = 0; i < objects.size(); i++) {
            //        mMap.addMarker(new MarkerOptions().position(new LatLng(((SharesBranch) objects.get(i)).getShareable().getLat(), ((SharesBranch) objects.get(i)).getShareable().getLng())).icon(BitmapDescriptorFactory.fromResource(AapHalper.userMapIconType(((Shares) objects.get(i)).getShareable().getType().getId()))));
                    mMap.addMarker(new MarkerOptions().position(new LatLng(((SharesBranch) objects.get(i)).getShareable().getLat(), ((SharesBranch) objects.get(i)).getShareable().getLng())).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(getActivity(),AapHalper.userMapIconType(((Shares) objects.get(i)).getShareable().getType().getId())))));

                }

            }

            @Override
            public void onRequestError(String errorMessage) {

                Log.d("errorMessage", errorMessage);


            }
        });
    }
}
