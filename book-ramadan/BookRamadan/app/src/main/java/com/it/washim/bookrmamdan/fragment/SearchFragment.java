package com.it.washim.bookrmamdan.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.it.washim.bookrmamdan.activity.MainActivity;
import com.it.washim.bookrmamdan.R;
import com.it.washim.bookrmamdan.adapter.searchAdapter;
import com.it.washim.bookrmamdan.helper.DataBHelper;
import com.it.washim.bookrmamdan.interfac.pageIdCallback;
import com.it.washim.bookrmamdan.model.searchResult;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    EditText search;
    TextView no_data;
    List<searchResult> searchResult;
    DataBHelper helper;
    RecyclerView search_result;
    searchAdapter searchAdapter;

    public SearchFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        search = (EditText) view.findViewById(R.id.search_textt);
        no_data = (TextView) view.findViewById(R.id.no_data);
        search_result = (RecyclerView) view.findViewById(R.id.search_result);
        helper = new DataBHelper(getActivity());
        searchResult = new ArrayList<>();

        search_result.setHasFixedSize(true);
        search_result.setLayoutManager(new LinearLayoutManager(getActivity()));

        searchAdapter = new searchAdapter(getActivity(), searchResult);

        search_result.setAdapter(searchAdapter);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // Toast.makeText(getContext(), ic_icon_sidebar_info.getText().toString(), Toast.LENGTH_LONG).show();
                    searchResult.clear();
                    searchResult.addAll(helper.search(search.getText().toString()));
                    searchAdapter.notifyDataSetChanged();
                    InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(search.getWindowToken(), 0);
                    if (searchResult.size() == 0) {
                        no_data.setVisibility(View.VISIBLE);
                        search_result.setVisibility(View.GONE);
                    } else {
                        search_result.setVisibility(View.VISIBLE);
                        no_data.setVisibility(View.GONE);
                    }
                }

                return true;
            }
        });

        searchAdapter.onpageIdCollback(new pageIdCallback() {
            @Override
            public void pageId(int pageId) {
                ((MainActivity) getActivity()).openFragment(new ViewPagesFragment(pageId));

            }
        });


        return view;
    }


}
