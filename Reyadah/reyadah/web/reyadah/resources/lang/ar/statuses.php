<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Statuses Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the systems.
    |
    */

    'active'=>'فعال',
    'inactive'=>'قيد الدراسة',
    'hidden'=>'مخفي',
    'blocked'=>'محظور',
    'declined'=>'مرفوض',
    'deleted'=>'محذوف',
];