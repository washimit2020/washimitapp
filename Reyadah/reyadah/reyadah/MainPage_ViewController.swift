//
//  MainPage_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class MainPage_ViewController: UIViewController,UITabBarDelegate {
    
    var firstTimeLogin = [false,false,false,false]
    var currentSelectionIndex = 0
    var is_shown = false
    
    @IBOutlet weak var contianerView: UIView!
    @IBOutlet weak var tabBar: UITabBar!
    let create_product_segue = "addProductFromMain"
    
    lazy var productsVC: HomeProductsCVC? = {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "home_products") as! HomeProductsCVC
        
        self.addChildToViewController(viewController: viewController)
        
        return viewController
    }()
    
    lazy var showPlace: MainPlaces_CollectionViewController? = {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "home_places") as! MainPlaces_CollectionViewController
        
        viewController.type = Constants.TYPE_SHOW
        self.addChildToViewController(viewController: viewController)
        
        return viewController
    }()
    
    lazy var festivalPlace: MainPlaces_CollectionViewController? = {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "home_places") as! MainPlaces_CollectionViewController
        
        viewController.type = Constants.TYPE_FESTIVAL
        self.addChildToViewController(viewController: viewController)
        
        return viewController
    }()
    
    lazy var storePlace: MainPlaces_CollectionViewController? = {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "home_places") as! MainPlaces_CollectionViewController
        
        viewController.type = Constants.TYPE_STORE
        self.addChildToViewController(viewController: viewController)
        
        return viewController
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.delegate = self

        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.backgroundColor = UIColor.white
        navigationItem.title = " "
        tabBar.isTranslucent = false
        tabBar.selectedItem = tabBar.items?[3]
        updateView()
        productsVC?.clearData()
        
        self.addRightBarButtonWithImage(UIImage(named: "Side_bar_icon")!)
        let search = UIBarButtonItem(image: UIImage(named: "ic_search")!, style: .plain, target: self, action: #selector(load_searchbar))
        search.isEnabled = Gate.is_authed()
        self.navigationItem.leftBarButtonItem = search
    }
    
    @IBAction func createProduct(_ sender: Any) {
        if Gate.is_authed(){
            performSegue(withIdentifier: create_product_segue, sender: sender)
        }
    }
    
    func load_searchbar(){
        self.navigationController?.pushViewController(Search_CollectionViewController(nibName: "Search_CollectionViewController", bundle: nil), animated: true)
    }
    
    private func addChildToViewController( viewController:UIViewController){
        addChildViewController(viewController)
        viewController.view.frame = contianerView.frame
        viewController.view.setNeedsLayout()
        viewController.view.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        
        viewController.didMove(toParentViewController: self)
        contianerView.addSubview(viewController.view)
    }
    
    private func updateView(){
        guard let productsVC = productsVC , let showPlace = showPlace, let storePlace = storePlace, let festivalPlace = festivalPlace else {
            return
        }
        productsVC.view.isHidden = true
        showPlace.view.isHidden = true
        storePlace.view.isHidden = true
        festivalPlace.view.isHidden = true
        
        var current:ItemsLoader_CollectionViewController = productsVC
        
        if is_shown{
            productsVC.view.viewWithTag(33)?.removeFromSuperview()
        }
        switch tabBar.selectedItem!.tag {
        case  1:
            storePlace.view.isHidden = false
            productsVC.clearData()
            showPlace.clearData()
            current = storePlace
           
            
        case  2:
            showPlace.view.isHidden = false
            festivalPlace.clearData()
            storePlace.clearData()
            current = showPlace
            
        case  3:
            
            festivalPlace.view.isHidden = false
            productsVC.clearData()
            showPlace.clearData()
            
            current = festivalPlace
            
        default:
            productsVC.view.isHidden = false
            
            storePlace.clearData()
            
        }
        show_tutorial()
        shouldClear(controller: current)
        self.currentSelectionIndex = tabBar.selectedItem!.tag
    }
    
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        self.updateView()
    }
    
    deinit {
        productsVC = nil
        showPlace = nil
        storePlace = nil
        festivalPlace = nil
    }
    
    func removeTutorialImage(_ sender:UITapGestureRecognizer)  {
        sender.view?.removeFromSuperview()
    }
    
    func show_tutorial(){
        if !firstTimeLogin[tabBar.selectedItem!.tag]{
            return
        }
        
        if tabBar.selectedItem!.tag == 0 {
            let view = UIImageView(frame: self.view.frame)
            view.tag = 33
            view.image = UIImage(named:"discover_pic_1")
            view.isUserInteractionEnabled = true
            view.backgroundColor = .white
            view.contentMode = .scaleAspectFit
            let tap = UITapGestureRecognizer(target: self, action: #selector(removeTutorialImage))
            view.addGestureRecognizer(tap)
            productsVC?.view.addSubview(view)
            is_shown = true
        }else{
            let dialog = Tutorial_ViewController(nibName: "Tutorial_ViewController", bundle: nil)
            dialog.selected = tabBar.selectedItem!.tag - 1
            self.present(dialog, animated: true, completion: nil)
        }
        
        firstTimeLogin[tabBar.selectedItem!.tag] = false
    }
    
    func shouldClear(controller :ItemsLoader_CollectionViewController){
        let diff = abs(currentSelectionIndex - tabBar.selectedItem!.tag)

        if diff > 1{
            controller.clearData()
        }
    }
}


