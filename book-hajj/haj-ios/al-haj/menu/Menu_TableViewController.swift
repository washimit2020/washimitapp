//
//  Menu_TableViewController.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite
class Menu_TableViewController: UITableViewController {
    var items : [MenuItem] = [
        MenuItem(image: #imageLiteral(resourceName: "home"), title: "الصفحة الرئيسية"),//done
        MenuItem(image: #imageLiteral(resourceName: "aboutOwner"), title: "المؤلف سيرة ومسيرة"),//done
        MenuItem(image: #imageLiteral(resourceName: "index"), title: "الفهرس"),//done
        MenuItem(image: #imageLiteral(resourceName: "liked"), title: "المفضلة"),//done
        MenuItem(image: #imageLiteral(resourceName: "search"), title: "البحث"),//done
        MenuItem(image: #imageLiteral(resourceName: "icon_note"), title: "ملاحظاتي"),
        MenuItem(image: #imageLiteral(resourceName: "liked"), title: "تواصل مع المؤلف"),//need design
        MenuItem(image: #imageLiteral(resourceName: "invite"), title: "اخبر صديق"),
        MenuItem(image: #imageLiteral(resourceName: "statistics"), title: "احصائيات"),
        MenuItem(image: #imageLiteral(resourceName: "aboutDeveloper"), title: "عن المطور"),
        ]
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView!.register(UINib(nibName: "Menu_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        let image = UIImageView(image: #imageLiteral(resourceName: "sidebar"))
        image.contentMode = .scaleAspectFill
        image.clipsToBounds = true
        tableView.backgroundView = image
        let header = Bundle.main.loadNibNamed("SlideMenuHeader", owner: nil, options: nil)?.first as? UIView
        let footer = Bundle.main.loadNibNamed("SlideMenuFooter", owner: nil, options: nil)?.first as? UIView
        header?.frame = .init(origin: .zero, size: .init(width: tableView.frame.width, height: 130))
        footer?.frame = .init(origin: .zero, size: .init(width: tableView.frame.width, height: 240))
//        let header = UIView(frame: .init(origin: .zero, size: .init(width: tableView.frame.width, height: 130)))
//        let imageView = UIImageView(frame: .init(x: tableView.frame.width - 100, y: 30, width: 100, height: 100))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = #imageLiteral(resourceName: "sidebar_header")
//        header.addSubview(imageView)
        tableView.tableHeaderView = header
        tableView.tableFooterView = footer
        tableView.separatorColor = .tintColor
        tableView.separatorInset = .init(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! Menu_TableViewCell
        cell.menu = items[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        slideMenuController()?.closeRight()
        var controller:UIViewController = BasePageGetter_TableViewController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nvc = (self.slideMenuController()?.mainViewController as! UINavigationController)
        controller.title = " "
        var stack:[UIViewController] = [nvc.viewControllers.first!]
        switch indexPath.row {
        case 1:
            controller = storyboard.instantiateViewController(withEnum: .cv)
            break
        case 2:
            guard let vc = (controller as? BasePageGetter_TableViewController) else {return}
            vc.is_sections = true
            Utils.shared.makeHeaderView(For: vc, icon:  #imageLiteral(resourceName: "index"), title: "الفهرس")
            break
        case 3:
            guard let vc = (controller as? BasePageGetter_TableViewController) else {return}
            vc.pages = Utils.shared.pages.filter({ (page) -> Bool in
                return page.is_wishlisted == 1
            })
            Utils.shared.makeHeaderView(For: vc, icon:  #imageLiteral(resourceName: "liked"), title: "المفضلة")
            break
        case 4:
            guard let vc = (stack.first as? MainTabbar_UIViewController) else {return}
            vc.vm.tabBar.selectedItem = vc.vm.tabBar.items?.last
            vc.vm.selectedViewController = vc.vm.viewControllers.last
            vc.didChangeController()
            nvc.setViewControllers(stack, animated: true)
            return
        case 5:
            guard let vc = (controller as? BasePageGetter_TableViewController) else {return}
            vc.pages = Utils.shared.pages.filter({ (page) -> Bool in
                return page.comment != nil
            })
            Utils.shared.makeHeaderView(For: vc, icon:  #imageLiteral(resourceName: "icon_note"), title: "ملاحظاتي")
        case 7:
            guard let view = (slideMenuController()?.mainViewController as? UINavigationController)?.viewControllers.last?.view else {return}
            let shareText = "حمل تطبيق الحج اهميته و مناسكة للدكتور سليمان ابا الخيل"
            let shareURL = URL(string: "https://www.google.jo/")!
            let vc = UIActivityViewController(activityItems: [shareText, shareURL], applicationActivities: [])
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.bounds
            present(vc, animated: true)
            return
        case 8:
            let vc = State_ViewController()
            if let book = ((stack.first as? MainTabbar_UIViewController)?.vm.viewControllers[1] as? Book_PageViewController){
                vc.items = Utils.shared.pages
            }
            controller = vc
            break
        case 9:
            controller = Info_ViewController()
            break
        default:
            nvc.setViewControllers(stack, animated: true)
            return
            //        case 14:
            //            guard let vc = (controller as? BasePageGetter_TableViewController) else {return}
            //            vc.table = vc.table.filter(Expressions.comment != nil)
            //            break
            //        case 13:
            //            let alert = UIAlertController(title: "اذهب الى", message: "", preferredStyle: .alert)
            //            alert.addTextField(configurationHandler: {
            //                textField in
            //                textField.keyboardType = .numberPad
            //            })
            //            alert.addAction(.init(title: "اغلاق", style: .cancel, handler: nil))
            //            alert.addAction(.init(title: "اذهب ", style: .default, handler: { (action) in
            //                guard let text = alert.textFields!.first!.text, let index = Int(text) else{
            //                    return
            //                }
            //
            //                //                Utils.shared.go_to_index(tabBarController:  nvc.viewControllers.first as! MainTabbar_UIViewController, page: index)
            //                self.slideMenuController()?.closeRight()
            //            }))
            //            nvc.present(alert, animated: true, completion: nil)
            //            return
        }
        
        controller.restorationIdentifier = "\(indexPath.row)"
        if controller.restorationIdentifier == nvc.viewControllers.last?.restorationIdentifier{
            return
        }
        stack.append(controller)
        nvc.setViewControllers(stack, animated: true)
    }
}

