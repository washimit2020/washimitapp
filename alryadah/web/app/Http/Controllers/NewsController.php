<?php

namespace App\Http\Controllers;

use App\Http\Helpers\Constants;
use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\News\Store;
use App\Http\Requests\NewsShowRequest;
use App\News;
use App\NewsAttachment;
use App\NewsLike;
use App\NewsMainType;
use App\NewsSubType;
use App\SeenNews;
use App\Wishlist;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Exception;

class NewsController extends BaseController
{
    private $counts = ['views', 'likes', 'comments'];
    private $relations = ["sub_type", "main_type", "status"];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = get_news_obj()->where('news_status_id', Constants::NEWS_STATUS_APPROVED)->where('is_main', true)->where('is_banner', 1)->latest('updated_at')->take(10)->get();
        $news = get_news_obj()->withCount($this->counts)->where('news_status_id', Constants::NEWS_STATUS_APPROVED)->where('is_main', true)->where('is_banner', 0)->latest('updated_at')->paginate(10);

        return response()->json(ResponseHelper::successResponse(["news" => $news, "banners" => $banner]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {

        $user = $request->user();
        $this->authorize('create', News::class);

        $news_type = NewsMainType::find($request->main_type);
        $news = new News();

        $news->title = $request["title"];
        $news->content = $request["content"];
        $news->news_main_type_id = $request["main_type"];
        $news->user_id = $user->id;
        $news->news_status_id = Constants::NEWS_STATUS_PENDING;

        if ($news_type->sub->count() > 0) {
            $this->validate($request, ['sub_type' => 'required']);
            $sub = NewsSubType::find($request->sub_type);

            if (empty($sub) || $sub->main->id != $request["main_type"]) {
                return response()->json(ResponseHelper::failedResponse(["sub_type" => [trans('validation.invalid')]]));
            }
            $news->news_sub_type_id = $request["sub_type"];

        }


        $news->save();

        $path = 'news/' . $news->id . '/' . Constants::NEWS_ATTACHMENT_MAIN;

        Storage::deleteDirectory($path);

        $file = $request->file('main_attachment');

        $file_data = [
            'news_id' => $news->id,
            'path' => $file->store($path),
            'mime_type' => $file->getMimeType(),
            'news_attachment_type_id' => Constants::NEWS_ATTACHMENT_MAIN
        ];

        NewsAttachment::create($file_data);

        if ($request->hasFile('attachments')) {
            $path = 'news/' . $news->id . '/' . Constants::NEWS_ATTACHMENT_SUB;
            Storage::deleteDirectory($path);

            foreach ($request->file('attachments') as $file) {
                $file_data = [
                    'news_id' => $news->id,
                    'path' => $file->store($path),
                    'mime_type' => $file->getMimeType(),
                    'news_attachment_type_id' => Constants::NEWS_ATTACHMENT_SUB
                ];
                NewsAttachment::create($file_data);
            }
        }

        return response()->json(ResponseHelper::successResponse(["news" => "added"]));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        News::findOrFail($id);

        if (auth('api')->check()) {
            try {
                $seen = new SeenNews();
                $seen->user_id = request()->user('api')->id;
                $seen->news_id = $id;
                $seen->save();
            } catch (QueryException $exception) {
            }
        }

        $news = News::withCount($this->counts)->findOrFail($id);

        $data = [
            "news_id" => $id
        ];

        $news->is_liked = check_status($data, new NewsLike());
        $news->is_wishlisted = check_status($data, new Wishlist());

        return response()->json(ResponseHelper::successResponse(["news" => $news]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $user = User::find(1);
//        $user->notify(new NewsApproved());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //costume functions

    /**
     * @param $main_type
     * @param null $sub_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function type($main_type, $sub_type = null)
    {
        $conditions = [
            ['news_main_type_id', $main_type],
            ['news_status_id', Constants::NEWS_STATUS_APPROVED]
        ];
        if (isset($sub_type)) {
            $conditions[] = ['news_sub_type_id', $sub_type];
        }
        $news = News::with(['user' => get_user_obj(), 'attachment' => function ($query) {
            $query->with('type')->get();
        }])->withCount($this->counts)->where($conditions)->latest('updated_at')->paginate(10);


        return response()->json(ResponseHelper::successResponse(['news' => $news]));
    }

    /**
     * @param $search
     * @param null $main_type
     * @param null $sub_type
     * @return \Illuminate\Http\JsonResponse
     */
    public function search(Request $request)
    {
        $conditions = [];
        $news = News::where('news_status_id', Constants::NEWS_STATUS_APPROVED)->with(['user' => get_user_obj(), 'attachment' => function ($query) {
            $query->with('type')->get();
        }])->withCount($this->counts);
        if ($request->has('search')) {
            $conditions[] = ['title', 'like', '%' . $request->search . '%'];
        }
        if ($request->has('main_type')) {
            $conditions[] = ['news_main_type_id', $request->main_type];
        }
        if ($request->has('sub_type')) {
            $conditions[] = ['news_sub_type_id', $request->sub_type];
        }
        $news = $news->where($conditions)->latest()->paginate(10);

        return response()->json(ResponseHelper::successResponse(['news' => $news]));

    }

    private function add_attachment(Request $request)
    {

        $this->validate($request, [
            "news_id" => "required|exists:news,id",
            "attachment_type" => "required|exists:news_attachment_types,id",
            "attachment" => "required|max:1024|mimes:png,jpeg,3gp,mp4",
        ]);


    }

}
