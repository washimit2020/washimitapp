package com.it.washim.wojooohapp.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.CustemView.MyEditText;
import com.it.washim.wojooohapp.Interfases.OnLoadMoreCollBack;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.SearchAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends BaseFragment implements TextView.OnEditorActionListener, View.OnClickListener {

    @BindView(R.id.book_title)
    MyEditText book_title;

    @BindView(R.id.book_author)
    MyEditText book_author;

    @BindView(R.id.recycler_search)
    RecyclerView recyclerView;

    @BindView(R.id.no_data)
    TextView no_data;

    @BindView(R.id.retray)
    MyButtonView retray;

    @BindView(R.id.progress)
    ProgressBar progress;


    SearchAdapter adapter;
    ArrayList<Post> list;
    String url;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SearchAdapter(getActivity(), list, recyclerView);
        recyclerView.setAdapter(adapter);
        book_title.setOnEditorActionListener(this);
        book_author.setOnEditorActionListener(this);
        retray.setOnClickListener(this);
        adapter.setOnLoadMoreLesnar(new OnLoadMoreCollBack() {
            @Override
            public void onLoad() {
                list.add(null);
                adapter.notifyItemInserted(list.size() - 1);
                nextSearch();
            }
        });

        search();
        return view;
    }


    public void search() {
        no_data.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        progress.setVisibility(View.VISIBLE);
        retray.setVisibility(View.GONE);
        getRequest().request(Request.Method.GET, "book/" + book_title.getText().toString() + "/" + book_author.getText().toString(), null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                list.clear();
                url = response.links.next;
                adapter.setUrl(url);
                list.addAll(response.data);
                adapter.notifyDataSetChanged();
                adapter.isLoading();
                progress.setVisibility(View.GONE);
                if (response.data.size() > 0) {

                    no_data.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                } else {
                    no_data.setText("لا يوجد");
                    no_data.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                adapter.isLoading();
                no_data.setText("لا يوجد اتصال بالنترنت");
                no_data.setVisibility(View.VISIBLE);
                retray.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        });
    }

    public void nextSearch() {
        getRequest().request(Request.Method.GET, url.replace("http://192.168.0.101/wojoooh/api/", ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {

                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size());

                url = response.links.next;
                adapter.setUrl(url);

                for (int i = 0; i < response.data.size(); i++) {
                    list.add(response.data.get(i));
                    adapter.notifyItemInserted(list.size() - 1);

                }
                adapter.isLoading();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                adapter.isLoading();
            }
        });


    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

        switch (actionId) {

            case EditorInfo.IME_ACTION_SEARCH:
                hideSoftKeyboard();
                search();
                break;


        }


        return false;
    }

    public void hideSoftKeyboard() {
        if (getActivity().getCurrentFocus() != null) {

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray:
                search();
                break;


        }
    }
}
