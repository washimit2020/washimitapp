<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 6/18/17
 * Time: 12:20 PM
 */

namespace App\Http\Controllers;


use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Users_Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request,[
            'post_id'=>'required|exists:posts,id'
        ]);

        $wishlist = Users_Wishlist::firstOrNew([
            'user_id' => $request->user()->id,
            'post_id' => $request->post_id,
        ]);

        $is_wishlisted = $wishlist->exists;
        if ($is_wishlisted) {
            $wishlist->where([
                ['user_id', $request->user()->id],
                ['post_id', $wishlist->post_id]])->delete();
        } else {
            $wishlist->save();
        }

        return response()->json(["wishlisted" => !$is_wishlisted]);
    }

    public function index(Request $request)
    {
        return response()->json(["posts" => $request->user()->wishlist()->paginate()]);
    }
}