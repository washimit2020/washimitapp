//
//  BasePageGetter_TableViewController.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite

class BasePageGetter_TableViewController: UITableViewController {

    var pages:[Any] = []
    let limit = 15
    var offset = 0
    var has_next = true
//    var table = DatabaseUtils.shared.pages_t
    var is_sections = false
    var didFinish = true
    var header:UIView?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44
        tableView.keyboardDismissMode = .onDrag
        tableView.register(UINib(nibName: "PageTitle_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableView.separatorStyle = .none
        title = " "
        navigationItem.addTitleImage()
        if is_sections {
            pages = Utils.shared.sections
        }
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let header = header {
            tableView.tableHeaderView = header
        }
        if slideMenuController()?.mainViewController != nil {
            addRightBarButtonWithImage(slideMenuController()?.navigationItem.rightBarButtonItem?.image ?? #imageLiteral(resourceName: "sidemenu"))
        }else{
            navigationItem.rightBarButtonItem = nil
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pages.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PageTitle_TableViewCell
        let item = pages[indexPath.row]
        cell.index = indexPath.row + 1
        if let page = item as? Page{
            cell.page = page
        }else{
            cell.section = item as? Section
        }
        
        return cell
    }
    
//    public func call_db(shouldClear:Bool = false)  {
//        self.didFinish = false
//        DispatchQueue.global().async {
//            do{
//                self.table = self.table.limit(self.limit, offset: self.limit * self.offset)
//                var rows:[Any]
//                if self.is_sections{
//                    rows = try DatabaseUtils.shared.db!.prepare(self.table).map({
//                        return Section($0)
//                    })
//                }else{
//                    rows = try DatabaseUtils.shared.db!.prepare(self.table).map({
//                        return Page($0)
//                    })
//                }
//                DispatchQueue.main.sync {
//                    if shouldClear {
//                        self.pages.removeAll()
//                        self.tableView.reloadData()
//                    }
//                    if rows.count == 0 {
//                        self.has_next = false
//                        self.didFinish = true
//                        return
//                    }
//                    self.pages.append(contentsOf: rows)
//                    self.offset += 1
//                    self.didFinish = true
//                    self.tableView.reloadData()
//                }
//
//            }catch(let exp){
//                print("search \(exp)")
//            }
//        }
//
//    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nvc = slideMenuController()?.mainViewController as? UINavigationController
        if nvc == nil {
            moveToPageFromMain((pages[indexPath.row] as? Section))
           return
        }
        guard let nav = nvc, let vc = nav.viewControllers.first as? MainTabbar_UIViewController else {
            return
        }
        nav.popToRootViewController(animated: true)
        var id:Int?
        if let page =  (pages[indexPath.row] as? Page){
            id = page.id
            Utils.shared.go_to_index(tabBarController: vc , page:id ?? 1 )
        }else if let section = (pages[indexPath.row] as? Section) {
            Utils.shared.go_to_section(tabBarController: vc, section: section.id!)
        }
    }
    
    func moveToPageFromMain(_ section:Section?){
        guard  let section = section else {
            return
        }
        navigationItem.backBarButtonItem = nil
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let main = storyboard.instantiateViewController(withEnum: .main) as! MainTabbar_UIViewController
        let right = Menu_TableViewController()
        main.addRightBarButtonWithImage(#imageLiteral(resourceName: "sidemenu"))
        main.section = section.id
        let nav = UINavigationController(rootViewController: main)
        nav.navigationBar.barTintColor = .white
        let slideMenuController = SlideMenuController(mainViewController: nav, rightMenuViewController: right)
        
        present(slideMenuController, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return Utils.shared.make_no_data_view(container: tableView, condition: pages.count > 0)
    }
}
