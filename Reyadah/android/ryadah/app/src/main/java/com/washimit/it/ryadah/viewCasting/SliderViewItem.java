package com.washimit.it.ryadah.viewCasting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.washimit.it.ryadah.R;

/**
 * Created by mohammad on 4/19/2017.
 */

public class SliderViewItem extends BaseSliderView {
    ImageView target;
    public SliderViewItem(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.castim_item_slider,null);
         target = (ImageView)v.findViewById(R.id.slider_image);
        bindEventAndShow(v, target);
        return v;

    }

    public ImageView getTarget() {
        return target;
    }
}
