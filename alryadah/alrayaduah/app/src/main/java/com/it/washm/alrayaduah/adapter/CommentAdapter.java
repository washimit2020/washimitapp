package com.it.washm.alrayaduah.adapter;

/**
 * Created by Mohammed Ali on 10/14/2016.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import com.it.washm.alrayaduah.CommentReplieActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.Comments;

public class CommentAdapter extends RecyclerView.Adapter {
    public Requests requests;
    public static HashMap<String, String> Stringparams;
    private final int VIEW_PROG = 0;
    public String urls;
    Intent intent;
    String[] a;
    SharedPref pref;
    private int VIEW_ITEM = 1;
    private List<Comments> CommentList;
    private Activity activity;
    boolean IsMyComment;
    private int lastPosition = -1;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 3;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    RecyclerView recyclerView;
    public static int p;
    Typeface SemiBold, Regular,Bold;

    public CommentAdapter(Activity activity, List<Comments> commentList, RecyclerView recyclerView) {
        this.CommentList = commentList;
        this.activity = activity;
        this.recyclerView = recyclerView;
        requests = new Requests(activity.getApplicationContext(), activity);
        Stringparams = new HashMap<String, String>();
        pref = new SharedPref(activity.getApplicationContext(), "MyPref");
        SemiBold = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-SemiBold.ttf");
        Regular = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-Regular.ttf");
        Bold = Typeface.createFromAsset(activity.getAssets(), "fonts/Cairo-Bold.ttf");


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {

        return CommentList.get(position) != null ? VIEW_ITEM : VIEW_PROG;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.comment_item, parent, false);

            vh = new CommentItemViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.see_more_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onViewDetachedFromWindow(RecyclerView.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.itemView.clearAnimation();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

      /*  Animation animation = AnimationUtils.loadAnimation(activity,
                (position > lastPosition) ? R.anim.up_from_bottom
                        : R.anim.down_from_top);
        holder.itemView.startAnimation(animation);/*/


        if (holder instanceof CommentItemViewHolder) {
            final Comments singleComment = (Comments) CommentList.get(position);



            a = AppHelper.getDateType(singleComment.getCreated_at(),activity,"yyyy-MM-dd' 'HH:mm:ss").split(" ");


            ((CommentItemViewHolder) holder).time_comment_item.setText(a[0]);
            ((CommentItemViewHolder) holder).date_comment_item.setText(a[1]);
            ((CommentItemViewHolder) holder).like_comment_item.setText(singleComment.getLikes_count() + "");


            if (singleComment.is_liked()) {

                ((CommentItemViewHolder) holder).like_comment_item.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);

            } else {


                ((CommentItemViewHolder) holder).like_comment_item.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);

            }

            ((CommentItemViewHolder) holder).add_replie_main_comment_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    redMor(position,singleComment);

                }
            });


            ((CommentItemViewHolder) holder).like_comment_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (AppHelper.IsLogIn(activity)) {
                        ((CommentItemViewHolder) holder).like_comment_item.setClickable(false);

                        if (!singleComment.is_liked()) {
                            ((CommentItemViewHolder) holder).like_comment_item.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);
                            singleComment.setIs_liked(true);
                            ((CommentItemViewHolder) holder).like_comment_item.setClickable(true);
                            ((CommentItemViewHolder) holder).like_comment_item.setText(String.valueOf(singleComment.getLikes_count() + 1));
                            singleComment.setLikes_count(singleComment.getLikes_count() + 1);

                        } else {

                            ((CommentItemViewHolder) holder).like_comment_item.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);

                            singleComment.setIs_liked(false);
                            ((CommentItemViewHolder) holder).like_comment_item.setClickable(true);
                            ((CommentItemViewHolder) holder).like_comment_item.setText(String.valueOf(singleComment.getLikes_count() - 1));
                            singleComment.setLikes_count(singleComment.getLikes_count() - 1);

                        }
                        Stringparams.clear();
                        Stringparams.put("comment_id", singleComment.getId().toString());
                        requests.StringRequest(Request.Method.POST, "like/comment", Stringparams, "", new VolleyCallback() {
                            @Override
                            public void onSuccess(String result) {
                                ((CommentItemViewHolder) holder).like_comment_item.setClickable(true);

                            }

                            @Override
                            public void onRequestError(VolleyError errorMessage) {
                                ((CommentItemViewHolder) holder).like_comment_item.setClickable(true);

                            }
                        });
                    } else {
                        Toast.makeText(activity, "يجب تسجيل الدخول للآعجاب!", Toast.LENGTH_LONG).show();

                    }
                }
            });


            Picasso.with(activity).load(Links.UrlImg + getAtachment(singleComment, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic).into(((CommentItemViewHolder) holder).img_main_comment_item);


            if (singleComment.getUser().getNick_name() == null) {
                ((CommentItemViewHolder) holder).name_main_comment_item.setText(singleComment.getUser().getFirst_name() + " " + singleComment.getUser().getLast_name());

            } else {
                ((CommentItemViewHolder) holder).name_main_comment_item.setText(singleComment.getUser().getNick_name());

            }
            ((CommentItemViewHolder) holder).comment_main.setText(singleComment.getContent());


            if (singleComment.getReplies().size() != 0) {

                ((CommentItemViewHolder) holder).sub2_comment_item.setVisibility(View.VISIBLE);
                ((CommentItemViewHolder) holder).sub1_comment_item.setVisibility(View.VISIBLE);
                ((CommentItemViewHolder) holder).replies_comment_item.setVisibility(View.VISIBLE);

                if (singleComment.getReplies().size() >= 2) {


                    Picasso.with(activity).load(Links.UrlImg + getAtachmentReplies(singleComment, 0, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic).into(((CommentItemViewHolder) holder).img_sub2_comment_item);


                    ((CommentItemViewHolder) holder).comment_sub2.setText(singleComment.getReplies().get(0).getContent());


                    Picasso.with(activity).load(Links.UrlImg + getAtachmentReplies(singleComment, 1, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic).into(((CommentItemViewHolder) holder).img_sub1_comment_item);

                    ((CommentItemViewHolder) holder).comment_sub1.setText(singleComment.getReplies().get(1).getContent());


                } else if (singleComment.getReplies().size() == 1) {

                    Picasso.with(activity).load(Links.UrlImg + getAtachmentReplies(singleComment, 0, 0)).fit().centerCrop().placeholder(R.drawable.ic_defult_profile_pic).into(((CommentItemViewHolder) holder).img_sub1_comment_item);

                    ((CommentItemViewHolder) holder).comment_sub1.setText(singleComment.getReplies().get(0).getContent());


                    ((CommentItemViewHolder) holder).sub2_comment_item.setVisibility(View.GONE);

                }


            } else {
                ((CommentItemViewHolder) holder).replies_comment_item.setVisibility(View.GONE);
            }


            ((CommentItemViewHolder) holder).comment_item.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (AppHelper.IsLogIn(activity)) {

                        dilog(singleComment.getId(), singleComment.getUser().getId(), singleComment.getContent(), position);
                    } else {
                        Toast.makeText(activity, "يجب تسجيل الدخول !", Toast.LENGTH_LONG).show();

                    }

                    return false;
                }
            });

            ((CommentItemViewHolder) holder).reed_more_comment_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    redMor(position,singleComment);
                }
            });

        } else {
            ((ProgressViewHolder) holder).cardView.setVisibility(View.VISIBLE);
            ((ProgressViewHolder) holder).avi.setVisibility(View.GONE);
            ((ProgressViewHolder) holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    getNextComment(urls);

                    ((ProgressViewHolder) holder).cardView.setVisibility(View.GONE);
                    ((ProgressViewHolder) holder).avi.setVisibility(View.VISIBLE);
                    ((ProgressViewHolder) holder).avi.show();

                }
            });

        }

    }

    public void setLoaded() {
        loading = false;
    }

    public void setUrl(String url) {
        urls = url;

    }


    public String getAtachment(Comments singleComments, int i) {
        if (singleComments.getUser().getAttachment().size() != 0) {
            if (i <= singleComments.getUser().getAttachment().size()) {
                return singleComments.getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    public String getAtachmentReplies(Comments singleComments, int i, int y) {
        if (singleComments.getReplies().get(i).getUser().getAttachment().size() != 0
                ) {
            if (y <= singleComments.getReplies().get(i).getUser().getAttachment().size()) {
                return singleComments.getReplies().get(i).getUser().getAttachment().get(y).getPath();
            }
        }

        return "";

    }

    public void dilog(final String CommentId, final String UserId, final String content, final int Position) {
        IsMyComment = false;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.custom_dilog);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.flags &= ~WindowManager.LayoutParams.FIRST_SYSTEM_WINDOW;
        window.setAttributes(wlp);

        CardView cardView = (CardView) dialog.findViewById(R.id.action);
        final TextView actionText = (TextView) dialog.findViewById(R.id.actionText);
        final TextView cancelText = (TextView) dialog.findViewById(R.id.cancelText);
        final CardView cancel = (CardView) dialog.findViewById(R.id.cancel);
        actionText.setTypeface(Regular);
        cancelText.setTypeface(Regular);

        if (UserId.equalsIgnoreCase(pref.gitShared("id"))) {
            actionText.setText("حذف التعليق");
            IsMyComment = true;
        }

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if (!IsMyComment) {
                        Stringparams.clear();
                        Stringparams.put("comment_id", CommentId);
                        Stringparams.put("content", content);
                        AppHelper.report("report/comment", activity, Stringparams, "تم الابلاغ");
                        dialog.dismiss();
                    } else {

                        AppHelper.delete("comments/" + CommentId, activity, "تم الحذف");
                        CommentList.remove(Position);
                        notifyItemRemoved(Position);
                        notifyItemRangeChanged(Position, CommentList.size());

                    }



                dialog.cancel();


            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }


    @Override
    public int getItemCount() {
        return CommentList.size();
    }


    public void getNextComment(String url) {
        requests.StringRequest(Request.Method.GET, url, null, false, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {


                try {
                    CommentList.remove(0);
                    notifyItemRemoved(0);

                } catch (Exception e) {

                }
                JSONObject object = null;
                JSONObject message = null;
                JSONObject comments = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        comments = message.getJSONObject("comments");
                        setUrl(comments.getString("next_page_url"));

                        urls = comments.getString("next_page_url");

                        CommentList.addAll(0, AppHelper.get_comment_response(comments.getJSONArray("data")));

                        if (!urls.equalsIgnoreCase("null")) {
                            CommentList.add(0, null);
                            notifyItemInserted(0);

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();


            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });
    }


    public class CommentItemViewHolder extends RecyclerView.ViewHolder {
        TextView reed_more_comment, name_main_comment_item, add_replie_main_comment_item, comment_main, comment_sub1, comment_sub2, like_comment_item, time_comment_item, date_comment_item;
        ImageView img_main_comment_item, img_sub1_comment_item, img_sub2_comment_item;
        CardView comment_item, sub1_comment_item, sub2_comment_item, reed_more_comment_item;
        LinearLayout replies_comment_item;

        public CommentItemViewHolder(View view) {
            super(view);
            reed_more_comment= (TextView) view.findViewById(R.id.reed_more_comment);
            name_main_comment_item = (TextView) view.findViewById(R.id.name_main_comment_item);
            add_replie_main_comment_item = (TextView) view.findViewById(R.id.add_replie_main_comment_item);
            comment_main = (TextView) view.findViewById(R.id.comment_main_comment_item);
            comment_sub1 = (TextView) view.findViewById(R.id.comment_sub1_comment_item);
            comment_sub2 = (TextView) view.findViewById(R.id.comment_sub2_comment_item);
            like_comment_item = (TextView) view.findViewById(R.id.like_comment_item);
            date_comment_item = (TextView) view.findViewById(R.id.date_comment_item);
            time_comment_item = (TextView) view.findViewById(R.id.time_comment_item);

            img_main_comment_item = (ImageView) view.findViewById(R.id.img_main_comment_item);
            img_sub1_comment_item = (ImageView) view.findViewById(R.id.img_sub1_comment_item);
            img_sub2_comment_item = (ImageView) view.findViewById(R.id.img_sub2_comment_item);

            comment_item = (CardView) view.findViewById(R.id.comment_item);
            sub1_comment_item = (CardView) view.findViewById(R.id.sub1_comment_item);
            sub2_comment_item = (CardView) view.findViewById(R.id.sub2_comment_item);
            reed_more_comment_item = (CardView) view.findViewById(R.id.reed_more_comment_item);

            replies_comment_item = (LinearLayout) view.findViewById(R.id.replies_comment_item);


            comment_main.setTypeface(Regular);
            comment_sub1.setTypeface(Regular);
            comment_sub2.setTypeface(Regular);
            like_comment_item.setTypeface(Regular);
            time_comment_item.setTypeface(Regular);
            date_comment_item.setTypeface(Regular);
            reed_more_comment.setTypeface(Regular);
            name_main_comment_item.setTypeface(Bold);
            add_replie_main_comment_item.setTypeface(SemiBold);


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment= (TextView) v.findViewById(R.id.reed_more_comment_text);
            reed_more_comment.setTypeface(Regular);

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void redMor(int position , Comments singleComment){
        a =  AppHelper.getDateType(singleComment.getCreated_at(),activity,"yyyy-MM-dd' 'HH:mm:ss").split(" ").clone();
        intent = new Intent(activity, CommentReplieActivity.class);
        intent.putExtra("commentId", singleComment.getId());
        intent.putExtra("date", a[0]);
        intent.putExtra("time", a[1]);

        if (singleComment.getUser().getNick_name() == null) {

            intent.putExtra("user_name", singleComment.getUser().getFirst_name() + " "
                    + singleComment.getUser().getLast_name());

        } else {
            intent.putExtra("user_name", singleComment.getUser().getNick_name());

        }
        intent.putExtra("img_user", getAtachment(singleComment, 0));
        intent.putExtra("comment", singleComment.getContent());
        intent.putExtra("like_count", singleComment.getLikes_count());
        intent.putExtra("IsLike", singleComment.is_liked());

        activity.startActivity(intent);
    }


}
