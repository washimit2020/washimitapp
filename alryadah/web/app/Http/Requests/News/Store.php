<?php

namespace App\Http\Requests\News;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"=>'required|max:255',
            "content"=>'required|max:5000',
            "main_type"=>'required|exists:news_main_types,id',
            "sub_type"=>'exists:news_sub_types,id',
            "main_attachment"=>'required|mimes:png,jpeg|max:1024',
            "attachments"=>'array|max:3',
            "attachments.*"=>'mimes:jpeg,png,mov,mp4|max:5128'
        ];
    }
}
