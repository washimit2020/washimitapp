//
//  RatingCell.swift
//  reyadah
//
//  Created by WASHM on 4/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class RatingCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var cosmos: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setup_cosmos(cosmos: cosmos)
        cosmos.updateOnTouch  = true
        cosmos.minTouchRating = 1
       
    }
    
    func updateCell(text:String,rating:Double){
        self.label.text = text
        
        if rating <= 0 {
            cosmos.rating = 1
        }else{
            cosmos.rating = rating
        }
    }
    
    
}
