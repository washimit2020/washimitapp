<?php

namespace App\Http\Middleware;

use App\Http\Constants;
use Closure;

class AppKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_KEY') != $request->header('app-key')) {
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }
        return $next($request);
    }
}
