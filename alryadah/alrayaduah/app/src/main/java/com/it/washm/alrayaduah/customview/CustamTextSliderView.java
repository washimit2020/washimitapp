package com.it.washm.alrayaduah.customview;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderTypes.BaseSliderView;

import com.it.washm.alrayaduah.R;

/**
 * Created by washm on 2/22/17.
 */

public class CustamTextSliderView extends BaseSliderView {
    public CustamTextSliderView(Context context) {
        super(context);
    }
    CustamViewFont viewFont;

    @Override
    public View getView() {
        viewFont=new CustamViewFont(getContext(),"Cairo-Regular.ttf");
        View v = LayoutInflater.from(getContext()).inflate(R.layout.render_type_text,null);
        ImageView target = (ImageView)v.findViewById(R.id.daimajia_slider_image);
        TextView description = (TextView)v.findViewById(R.id.description);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;

        description.setLayoutParams(params);
        viewFont.setfont(description);
        description.setText(getDescription());
        bindEventAndShow(v, target);
        return v;
    }


}

