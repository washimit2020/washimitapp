<?php
use  App\Http\Constants;

/**
 * to make the common tables
 *
 * @param \Illuminate\Database\Schema\Blueprint $table
 */
function common_table_fields(\Illuminate\Database\Schema\Blueprint $table)
{
    $table->increments('id');
    $table->string('title');
    $table->timestamps();
}

/**
 * to get the user types only
 * @return array
 */
function get_app_user_types()
{
    return [Constants::_TYPE_STORE, Constants::_TYPE_SHOW, Constants::_TYPE_COSTUMER];
}

/**
 * to help the in validation
 *
 * @param array $array
 * @return string
 */
function make_validation_array_in(array $array)
{
    return implode(',', $array);
}

/**
 * response helper
 *
 * @param $status
 * @param $msg
 * @return array
 */
function response_json($status, $msg)
{
    $data = [
        'status' => $status,
        'msg' => $msg
    ];
    return $data;
}

/**
 * to refresh the api token
 *
 * @param \App\User $user
 */
function make_api_token(\App\User $user)
{
    $user->api_token = bcrypt(str_random(100) . $user->name);
    $user->update();
}

/**
 * @param \Illuminate\Database\Eloquent\Model[] ...$model
 */
function from_db_to_trans(\Illuminate\Database\Eloquent\Model...$model)
{
    foreach ($model as $item)
        $item->title = trans($item->title);
}

/**
 * @param \App\User $user
 * @return \Illuminate\Http\JsonResponse
 */
function user_response(\App\User $user)
{
    $user->load('social_media', 'status', 'type');
    return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['user' => $user, 'token' => $user->api_token]));
}

/**
 * to validate a place from db
 * @return string
 */
function make_place_validation_string()
{
    return "exists:users,id,status_id," . Constants::_STATUS_ACTIVE . ',type_id,!' . Constants::_TYPE_COSTUMER . ',id,!' . auth()->user()->id;
}

/**
 * to validate a place from db
 * @return string
 */
function make_festival_validation_string()
{
    return "exists:festivals,id,status_id," . Constants::_STATUS_ACTIVE;
}

/**
 * attachment data Cover and Avatar
 *
 * @param $request
 * @param $user
 */
function register_user_attachment(\Illuminate\Http\Request $request, $user)
{

    $dir = "users/{$user->id}/";

    $data = [
        'type_id' => Constants::_TYPE_AVATAR_IMAGE,
        'related_id' => $user->id
    ];

    if ($request->hasFile('avatar')) {
        $attachment = \App\Attachment::where($data)->first();

        if (isset($attachment)) {
            \Illuminate\Support\Facades\Storage::delete($attachment->path);
        }

        $file = $request->file('avatar');
        $data['path'] = $file->store($dir . 'avatar');
        $data['mime_type'] = $file->getMimeType();
        $data['extension'] = $file->extension();

        if (isset($attachment)) {
            $attachment->update($data);
        } else {
            \App\Attachment::create($data);
        }
    }

    if ($user->cannot(Constants::ABILITY_ADD_PRODUCT)) {
        return;
    }

    $data ['type_id'] = Constants::_TYPE_COVER_IMAGE;

    if ($request->hasFile('cover') && is_array($request->file('cover'))) {
        $attachments = \App\Attachment::where($data)->get();

        foreach ($request->file("cover") as $key => $file) {
            if (isset($attachments[$key])) {
                \Illuminate\Support\Facades\Storage::delete($attachments[$key]->path);
            }
            $data['path'] = $file->store($dir . 'cover');
            $data['mime_type'] = $file->getMimeType();
            $data['extension'] = $file->extension();

            if (isset($attachments[$key])) {
                $attachments[$key]->update($data);
            } else {
                \App\Attachment::create($data);
            }
        }
    }


}

function check_if_blocked($query){
    $current_user = auth()->guard('api')->user();
    if (!isset($current_user)){
        return;
    }
    $query->whereDoesntHave('blocked_users', function ($query)use ($current_user) {
        $query->where('related_id', $current_user->id);
    })->whereDoesntHave('blocked_by', function ($query) use ($current_user) {
        $query->where('user_id', $current_user->id);
    });
}


function abort_if_blocked(\App\User $user){
    if (auth()->guard('api')->guest()) {
       return;
    }

    $current_user = auth()->guard('api')->user();

     $blocked = \App\BlockedUser::where([
        ['related_id',$current_user->id],
        ['user_id',$user->id]
     ])->orWhere([
        ['related_id',$user->id],
        ['user_id',$current_user->id]
    ])->first();


    if (!empty($blocked)){
        abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
    }

}