package com.it.washim.bakkar.halper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.example.jean.jcplayer.JCAudio;
import com.example.jean.jcplayer.JCAudioPlayer;
import com.example.jean.jcplayer.JCNotificationPlayer;
import com.example.jean.jcplayer.JCPlayerExceptions.AudioListNullPointerException;
import com.example.jean.jcplayer.JCPlayerService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mohammad on 8/3/2017.
 */

public class MyJCAudioPlayer {
    public JCPlayerService jcPlayerService;
    private JCPlayerService.JCPlayerServiceListener listener;
    private JCPlayerService.JCPlayerServiceListener notificationListener;
    private JCNotificationPlayer jcNotificationPlayer;
    private List<JCAudio> JCAudioList;
    private JCAudio currentJCAudio;
    private int currentPositionList;
    private Context context;
    private static MyJCAudioPlayer instance;
    public boolean mBound = false;
    private boolean playing;
    private boolean paused;
    private int position = 1;

    public MyJCAudioPlayer(Context context, List<JCAudio> JCAudioList, JCPlayerService.JCPlayerServiceListener listener) {
        this.context = context;
        this.JCAudioList = JCAudioList;
        this.listener = listener;
        instance = MyJCAudioPlayer.this;
        this.jcNotificationPlayer = new JCNotificationPlayer(context);
    }

    public void registerNotificationListener(JCPlayerService.JCPlayerServiceListener notificationListener) {
        this.notificationListener = notificationListener;
        if (jcNotificationPlayer != null)
            jcPlayerService.registerNotificationListener(notificationListener);
    }

    public static MyJCAudioPlayer getInstance() {
        return instance;
    }

    public void playAudio(JCAudio JCAudio) throws AudioListNullPointerException {
        if (JCAudioList == null || JCAudioList.size() == 0)
            throw new AudioListNullPointerException();
        else {

            currentJCAudio = JCAudio;

            if (!mBound)
                initJCPlayerService();
            else {
                jcPlayerService.play(currentJCAudio);
                updatePositionAudioList();
                mBound = true;
                playing = true;
                paused = false;
            }
        }
    }

    public void nextAudio() throws AudioListNullPointerException {
        if (JCAudioList == null || JCAudioList.size() == 0)
            throw new AudioListNullPointerException();

        else {
            if (currentJCAudio != null) {
                try {
                    JCAudio nextJCAudio = JCAudioList.get(currentPositionList + position);
                    this.currentJCAudio = nextJCAudio;
                    jcPlayerService.stop();
                    jcPlayerService.play(nextJCAudio);

                } catch (IndexOutOfBoundsException e) {
                    playAudio(JCAudioList.get(0));
                    e.printStackTrace();
                }
            }

            updatePositionAudioList();
            playing = true;
            paused = false;
        }
    }

    public void previousAudio() throws AudioListNullPointerException {
        if (JCAudioList == null || JCAudioList.size() == 0)
            throw new AudioListNullPointerException();

        else {
            if (currentJCAudio != null) {
                try {
                    JCAudio previousJCAudio = JCAudioList.get(currentPositionList - position);
                    this.currentJCAudio = previousJCAudio;
                    jcPlayerService.stop();
                    jcPlayerService.play(previousJCAudio);

                } catch (IndexOutOfBoundsException e) {
                    playAudio(JCAudioList.get(0));
                    e.printStackTrace();
                }
            }

            updatePositionAudioList();
            playing = true;
            paused = false;
        }
    }

    public void pauseAudio() {
        jcPlayerService.pause(currentJCAudio);
        paused = true;
        playing = false;
    }

    public void continueAudio() throws AudioListNullPointerException {
        if (JCAudioList == null || JCAudioList.size() == 0)
            throw new AudioListNullPointerException();

        else {
            if (currentJCAudio == null)
                currentJCAudio = JCAudioList.get(0);
            playAudio(currentJCAudio);
            playing = true;
            paused = false;
        }
    }

    public void createNewNotification(int iconResource) {
        if (currentJCAudio != null)
            jcNotificationPlayer.createNotificationPlayer(currentJCAudio.getTitle(), iconResource);
    }

    public void updateNotification() {
        jcNotificationPlayer.updateNotification();
    }

    public void seekTo(int time) {
        if (jcPlayerService != null)
            jcPlayerService.seekTo(time);
    }

    public JCAudio getCarnt() {
        return currentJCAudio;
    }

    private void updatePositionAudioList() {
        for (int i = 0; i < JCAudioList.size(); i++) {
            if (JCAudioList.get(i).getId() == currentJCAudio.getId())
                this.currentPositionList = i;
        }
    }

    private synchronized void initJCPlayerService() {
        if (!mBound) {
            Intent intent = new Intent(context.getApplicationContext(), JCPlayerService.class);
            intent.putExtra(JCNotificationPlayer.PLAYLIST, (Serializable) JCAudioList);
            intent.putExtra(JCNotificationPlayer.CURRENT_AUDIO, currentJCAudio);
            context.bindService(intent, mConnection, context.getApplicationContext().BIND_AUTO_CREATE);
        }
    }


    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            JCPlayerService.JCPlayerServiceBinder binder = (JCPlayerService.JCPlayerServiceBinder) service;
            jcPlayerService = binder.getService();
            jcPlayerService.registerListener(listener);
            jcPlayerService.play(currentJCAudio);
            updatePositionAudioList();
            mBound = true;
            playing = true;
            paused = false;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBound = false;
            playing = false;
            paused = true;
        }
    };

    public boolean isPlaying() {
        return playing;
    }

    public boolean isPaused() {
        return paused;
    }

    public void kill() {
        jcPlayerService.destroy();
        context.unbindService(mConnection);
    }
}
