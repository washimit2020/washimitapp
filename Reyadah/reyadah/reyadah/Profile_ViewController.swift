//
//  Profile_ViewController.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class Profile_ViewController: UIViewController {
    
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var icon: UIImageView!
    
    @IBOutlet weak var addProduct: UIButton!
    @IBOutlet weak var myMenu: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var totalFollowers: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    var  done:UIBarButtonItem?
    var cancel:UIBarButtonItem?
    
    var user:User?
    var followers  = [JSON]()
    var dataModel:DataModel?
    var backButton:UIBarButtonItem!
    
    var selectedItem = 0
    
    @IBOutlet weak var addBranch: AspectFitButton!
    @IBOutlet weak var shareProduct: AspectFitButton!
    var tabBar_Controller:ProfileTab_ViewController!
    var profileProduct:MyProducts_CollectionViewController!
    
    
    lazy var apiRequest = ApiRequest()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        

        if let userData = user{
            icon.image = UIImage(named:Constants.icon_name[(userData.type.id - 1)])
            let link = get_user_avatar_link(id: userData.id)
            name.text = userData.name
            location.text = make_location_text(userData)
            avatar.kf.indicatorType = .activity
            avatar.kf.setImage(with: link)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.title = " "

        if Gate.is_costumer(){
            icon.isHidden = true
            addProduct.isHidden = true
            addBranch.isHidden = true
            shareProduct.isHidden = true
            location.isHidden = true
        }else{
            done = UIBarButtonItem(image: UIImage(named:"ic_done"), style: .done, target: nil, action: nil)
            cancel = UIBarButtonItem(image: UIImage(named:"ic_cancel"), style: .done, target: nil, action: nil)
            backButton = self.navigationItem.backBarButtonItem
            shareProduct.addTarget(nil, action: #selector(share_product(_:)), for: .touchUpInside)
            myMenu.isHidden = true
            
            if Gate.is_show(){
                addBranch.isHidden = false
            }else{
                addBranch.isHidden = true
            }
            apiRequest.make_request(link: RequestLinks.followed_by.rawValue, method: .get,  completion: {
                $0?.responseJSON(completionHandler: {
                    let dataModel = DataModel(JSON($0.data),beginWith: "users")
                    guard let total = dataModel.total else {
                        return
                    }
                    self.totalFollowers.text = "\(total) متابع"
                    self.totalFollowers.backgroundColor = UIColor.white
                    self.user?.totalFollowers = dataModel.total
                    self.followers.removeAll()
                    if dataModel.total > 0 {
                        for data in dataModel.data {
                            self.followers.append(data)
                        }
                    }
                    self.dataModel = dataModel
                })
            })
            
            
        }
    }
    
    
    
    deinit {
        avatar = nil
        icon = nil
        
        addProduct = nil
        myMenu = nil
        location = nil
        name = nil
        totalFollowers = nil
        containerView = nil
        done = nil
        cancel = nil
        user = nil
        dataModel = nil
        backButton = nil
        addBranch = nil
        shareProduct = nil
        tabBar_Controller = nil
        profileProduct = nil
    }
    
}



