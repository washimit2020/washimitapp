<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsWithUser extends Model
{
    public $incrementing = false;
    /**
     * @var array of primary arrays
     */
    protected $primaryKey = ["news_id", "user_id"];

    /**
     * @var array of fillable arrays
     */
    protected $fillable = ["news_id", "user_id"];

    /**
     * @var array of hidden arrays
     */
    protected $hidden = ["news_id", "user_id"];


    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * belongs to one news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function news()
    {
        return $this->belongsTo('App\News', 'news_id');
    }


}
