package com.washimit.it.ryadah.Fragment.profile;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;

import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditProduactFragment extends Fragment implements View.OnClickListener {


    EditTextView product_name, product_des, product_price;
    Spinner product_number;
    ImageView add_product;
    ArrayList<Part> parts;
    ApiRequest requiresApi;
    Gson gson;
    Product product;

    public EditProduactFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_produact, container, false);
        gson = new Gson();
        if (getArguments() != null) {
            product = gson.fromJson(getArguments().getString("product"), Product.class);
        }
        requiresApi = new ApiRequest(getActivity());


        product_name = (EditTextView) view.findViewById(R.id.product_name);
        product_des = (EditTextView) view.findViewById(R.id.product_des);
        product_price = (EditTextView) view.findViewById(R.id.product_price);
        product_number = (Spinner) view.findViewById(R.id.product_number);
        add_product = (ImageView) view.findViewById(R.id.add_product);

        product_name.setText(product.getName());
        product_des.setText(product.getIngredients());
        product_price.setText(product.getPrice() + "");
        // product_number.setText(product.getName()+"");


        parts = new ArrayList<>();

        add_product.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.add_product:


                if (validation()) {

                    parts.add(new StringPart("name", product_name.getText().toString()));
                    parts.add(new StringPart("number_of_eaters", "1"));
                    parts.add(new StringPart("ingredients", product_des.getText().toString()));
                    parts.add(new StringPart("price", product_price.getText().toString()));

                    requiresApi.MultipartRequest("POST", "product/" + product.getId() + "/update", getString(R.string.edit_new_product), parts, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result) {
                            Log.d("updateProduct", result);
                            ((MainActivity) getActivity()).openFragment(new MyProfileFragment());
                        }

                        @Override
                        public void onRequestError(String errorMessage) {
                            Log.d("updateProduct", errorMessage);

                        }
                    });

                }


                break;

        }
    }


    public boolean validation() {


        if (product_name.getText().toString().equalsIgnoreCase("")) {
            product_name.setError(getResources().getString(R.string.fill_name));
            product_name.requestFocus();
            return false;
        }
        if (product_des.getText().toString().equalsIgnoreCase("")) {
            product_des.setError(getResources().getString(R.string.fill_ingredients));
            product_des.requestFocus();

            return false;
        }
        if (product_price.getText().toString().equalsIgnoreCase("") && product_price.getText().toString().length() < 4) {
            product_price.setError(getResources().getString(R.string.fill_price));
            product_price.requestFocus();
            return false;
        }

        return true;
    }
}
