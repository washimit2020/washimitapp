package com.washimit.it.ryadah.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Location;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;


public class  MapsActivity extends FragmentActivity implements View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMyLocationButtonClickListener, PlaceSelectionListener {

    private GoogleMap mMap;
    LatLng latLng;
    FloatingActionButton add_location;
    Bundle bundle;
    Location location;
    PlaceAutocompleteFragment autocompleteFragment;
    SupportMapFragment mapFragment;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        gson = new Gson();
        bundle = getIntent().getExtras();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);

        mapFragment.getMapAsync(this);

        add_location = (FloatingActionButton) findViewById(R.id.add_location);

        add_location.setOnClickListener(this);

        if (bundle != null) {
            location = gson.fromJson(bundle.getString("location"), Location.class);
            autocompleteFragment.getView().setVisibility(View.GONE);

        }

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (location != null) {
            mMap.clear();
            mMap.addMarker(new MarkerOptions().position(location.getLatLng()).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(this,AapHalper.userMapIconType(location.getType())))));
            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(location.getLatLng()).zoom(15).build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            mMap.setOnMapClickListener(this);
            mMap.setOnMyLocationButtonClickListener(this);

            if (AapHalper.requestPermission(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 2)) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
            }


        }


    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);


        } else {
            finish();
            Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للاستمرار بالتطبيق", Toast.LENGTH_LONG).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMapClick(LatLng latLng) {
        this.latLng = latLng;
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(this,R.drawable.ic_add_location))));
        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        add_location.setVisibility(View.VISIBLE);

    }

    @Override
    public boolean onMyLocationButtonClick() {

        if (AapHalper.gpsIsOn(this)) {
            if (mMap.getMyLocation() != null) {
                mMap.clear();
                latLng = new LatLng(mMap.getMyLocation().getLatitude(), mMap.getMyLocation().getLongitude());
                mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(this,R.drawable.ic_add_location))));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                add_location.setVisibility(View.VISIBLE);

                return true;
            } else {

            }
        }

        return false;
    }

    @Override
    public void onPlaceSelected(Place place) {
        mMap.clear();
        latLng = place.getLatLng();
        mMap.addMarker(new MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromBitmap(AapHalper.getBitmap(this,R.drawable.ic_add_location))));
        // For zooming automatically to the location of the marker
        CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng).zoom(15).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        add_location.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(Status status) {

    }


    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("latLng", latLng);
        setResult(Constant._TYPE_MAP, intent);
        finish();//finishing activity
    }


}
