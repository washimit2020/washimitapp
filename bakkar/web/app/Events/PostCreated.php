<?php

namespace App\Events;

use App\Http\Constants;
use App\Post;

class PostCreated extends Event
{
    /**
     * @var Post $post
     */
    public $post;

    /**
     * Create a new event instance.
     *
     * @param  Post $post
     */
    public function __construct(Post $post = null)
    {

        $this->post = $post;

        switch ($post->page_id){
            case Constants::PAGES_ARTICLES:
                $title = "added_an_article";
                break;
            case Constants::PAGES_BOOKS:
                $title = "added_a_book";
                break;
            case Constants::PAGES_AUDIO:
                $title = "added_an_audio";
                break;
            default:
                $title = "added_an_agent";
                break;
        }
//        $this->title = trans("notifications.$title");
        $this->body = "notifications.$title";

        $this->data["post"] = $post->toArray();
    }
}
