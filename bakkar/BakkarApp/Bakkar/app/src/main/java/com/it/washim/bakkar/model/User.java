package com.it.washim.bakkar.model;

/**
 * Created by mohammad on 8/8/2017.
 */

public class User {
    public String name;
    public String email;
    public String api_token;
    public String updated_at;
    public String created_at;
    public int id;
}
