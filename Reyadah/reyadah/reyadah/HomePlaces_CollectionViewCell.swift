//
//  HomePlaces_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher

class HomePlaces_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var userCover: UIImageView!
    @IBOutlet weak var userLocation: UILabel!
    @IBOutlet weak var followUser: AspectFitButton!
    @IBOutlet weak var ratingStar: CosmosView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        setup_cosmos(cosmos: ratingStar)
    }
    public var user:User?
    
    public func setupView(){
        let user  = self.user!
        
        if (userCover) != nil{
            userCover.set_avatar_image(user_id: user.id)
          
            userLocation.text = make_location_text(user)

        }
        username.text = user.name
        ratingStar.rating = user.rating!
        
        
        followUser.setImage(check_user_follow_status(user: user), for: .normal)

    }
}
