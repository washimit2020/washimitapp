package com.it.washm.alrayaduah.registration;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;
import com.koushikdutta.ion.Ion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.zelory.compressor.Compressor;
import com.it.washm.alrayaduah.MainActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.MultipartCallback;

/**
 * Created by washm on 1/3/17.
 */


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.ferstName_sign_up)
    AutoCompleteTextView ferstName_sign_up;
    @BindView(R.id.lastName_sign_up)
    AutoCompleteTextView lastName_sign_up;
    @BindView(R.id.fakeNames_sign_up)
    AutoCompleteTextView fakeNames_sign_up;
    @BindView(R.id.email_sign_up)
    AutoCompleteTextView email_sign_up;
    @BindView(R.id.password_sign_up)
    AutoCompleteTextView password_sign_up;
    @BindView(R.id.btn_sign_up_text)
    Button btn_sign_up_text;
    @BindView(R.id.btn_sign_up)
    RippleView btn_sign_up;
    @BindView(R.id.radioFemale_sign_up)
    RadioButton radioFemale_sign_up;
    @BindView(R.id.radioMale_sign_up)
    RadioButton radioMale_sign_up;
    @BindView(R.id.radioAccept_sign_up)
    RadioButton radioAccept_sign_up;
    @BindView(R.id.profile_img_sign_up)
    ImageView profile_img_sign_up;
    @BindView(R.id.tilferstName_sign_up)
    TextInputLayout tilferstName_sign_up;
    @BindView(R.id.tillastName_sign_up)
    TextInputLayout tillastName_sign_up;
    @BindView(R.id.tilefakeNames_sign_up)
    TextInputLayout tilfakeNames_sign_up;
    @BindView(R.id.tilemail_sign_up)
    TextInputLayout tilemail_sign_up;
    @BindView(R.id.tilpassword_sign_up)
    TextInputLayout tilpassword_sign_up;
    @BindView(R.id.RelativeLayout)
    RelativeLayout relativeLayout;

    Requests requests;
    List<Part> Prams;

    String email;
    String password;
    String ferstName;
    String lastName;
    String nickname="";
    String gender = "1";
    String MainImgPath = "";
    private static int RESULT_LOAD_IMAGE = 1;
    public static String TAG = "SIGN_UP";
    Intent intent;
    SharedPref pref;
    Uri path;
    CustamViewFont viewFont;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        profile_img_sign_up.setImageDrawable(getResources().getDrawable(R.drawable.ic_defult_profile_pic));

        viewFont = new CustamViewFont(SignUpActivity.this, "Cairo-SemiBold.ttf");
        setFont();
        requests = new Requests(getApplicationContext(), this);
        pref = new SharedPref(getApplicationContext(), "MyPref");

        Prams = new ArrayList();

        btn_sign_up.setOnClickListener(this);
        radioFemale_sign_up.setOnClickListener(this);
        radioMale_sign_up.setOnClickListener(this);
        profile_img_sign_up.setOnClickListener(this);
    }

    public void setFont() {
        viewFont.setfont(tilferstName_sign_up);
        viewFont.setfont(tillastName_sign_up);
        viewFont.setfont(tilfakeNames_sign_up);
        viewFont.setfont(tilemail_sign_up);
        viewFont.setfont(tilpassword_sign_up);
        viewFont.setfont(radioFemale_sign_up);
        viewFont.setfont(radioMale_sign_up);
        viewFont.setfont(radioAccept_sign_up);
        viewFont.setfont(btn_sign_up_text, "Cairo-Regular.ttf");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            File compressedImage = new Compressor.Builder(this)
                    .setMaxWidth(640)
                    .setMaxHeight(480)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).getAbsolutePath())
                    .build()
                    .compressToFile(new File(AppHelper.getPathfromURI(this, selectedImage)));
            MainImgPath = compressedImage.getAbsolutePath();

            profile_img_sign_up.setImageBitmap(BitmapFactory.decodeFile(compressedImage.getAbsolutePath()));

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {

            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), "يجب اعطاء الاذن للتطبيق حتي تتمكن من الاستمرار في التسجيل ", Toast.LENGTH_LONG).show();
            } else {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up:
                if (validate()) {
                    if (AppHelper.isOnline(SignUpActivity.this)) {
                        Log.d("getToken", FirebaseInstanceId.getInstance().getToken() + "");
                         if (MainImgPath.length() > 0) {
                            Prams.add(new FilePart("profile_image", new File(MainImgPath)));
                        }
                        Prams.add(new StringPart("first_name", ferstName));
                        Prams.add(new StringPart("last_name", lastName));
                        if(nickname.length()>0) {
                            Prams.add(new StringPart("nick_name", nickname));
                        }
                        Prams.add(new StringPart("password", password));
                        Prams.add(new StringPart("email", email));
                        Prams.add(new StringPart("gender", gender));
                        Prams.add(new StringPart("reg_id", FirebaseInstanceId.getInstance().getToken() + ""));

                        requests.MultipartRequest("signup", "جاري عمل حساب جديد", Prams, new MultipartCallback() {
                            @Override
                            public void onSuccess(String result) {
                                Log.d(TAG, result);
                                JSONObject object = null;
                                JSONObject message = null;
                                JSONObject user = null;
                                JSONObject role = null;
                                JSONArray attachment = null;

                                   Log.d("aqekfjaskfjas",result);
                                try {
                                    Log.d(TAG, result);
                                    object = new JSONObject(result);
                                    if (object.getBoolean("success")) {
                                        message = object.getJSONObject("message");
                                        user = message.getJSONObject("user");
                                        role = user.getJSONObject("role");
                                        attachment = user.getJSONArray("attachment");

                                        pref.clearRef();

                                        //user id
                                        pref.setShared("id", user.getString("id"));

                                        //first_name
                                        pref.setShared("first_name", user.getString("first_name"));

                                        //last_name
                                        pref.setShared("last_name", user.getString("last_name"));

                                        //nick_name
                                        pref.setShared("nick_name", user.getString("nick_name"));

                                        //role id
                                        pref.setShared("role_id", role.getString("id"));

                                        //user attachment
                                        if (attachment.length() > 0) {
                                            pref.setShared("path", attachment.getJSONObject(0).getString("path"));
                                        } else {
                                            pref.setShared("path", "");
                                        }
                                        //user logIn
                                        pref.setShared("is_log_in", "1");

                                        //user token
                                        pref.setShared("token", message.getString("token"));

                                        //time_type
                                        pref.setShared("time", "24");


                                        //date_type
                                        pref.setShared("date", "m");


                                        //notification
                                        pref.setShared("notification", "1");

                                        intent = new Intent(SignUpActivity.this, MainActivity.class);
                                        startActivity(intent);
                                        SignUpActivity.this.finish();
                                        LogInActivity.activity.finish();

                                    } else {
                                        Toast.makeText(getApplicationContext(), "" + object.getString("email"), Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onRequestError(String errorMessage) {
                                Log.d("aqekfjaskfjas",errorMessage+"");

                            }

                        });
                    } else {

                        Snackbar.make(relativeLayout, "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }

                }
                break;
            case R.id.radioFemale_sign_up:
                gender = "0";
                break;

            case R.id.radioMale_sign_up:
                gender = "1";
                break;
            case R.id.profile_img_sign_up:


                if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                } else {

                    Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    pickIntent.setType("image/*");
                    Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    //   Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

                    String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
                    Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
                    chooserIntent.putExtra
                            (
                                    Intent.EXTRA_INITIAL_INTENTS,
                                    new Intent[]{takePhotoIntent}
                            );

                    startActivityForResult(chooserIntent, RESULT_LOAD_IMAGE);

                }
                //  Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                break;
        }
    }


    @Override
    protected void onStop() {
        Ion.getDefault(getApplicationContext()).cancelAll(getApplicationContext());
        super.onStop();

    }

    public boolean validate() {
        boolean valid = true;

        email = email_sign_up.getText().toString();
        password = password_sign_up.getText().toString();
        ferstName = ferstName_sign_up.getText().toString();
        lastName = lastName_sign_up.getText().toString();
        nickname = fakeNames_sign_up.getText().toString();

        if (ferstName.equalsIgnoreCase("")) {
            ferstName_sign_up.setError("آدخل الاسم الاول");
            valid = false;
        } else {
            ferstName_sign_up.setError(null);
        }

        if (lastName.equalsIgnoreCase("")) {
            lastName_sign_up.setError("آدخل الاسم الثاني");
            valid = false;
        } else {
            lastName_sign_up.setError(null);
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            email_sign_up.setError("آدخل ايميل صحيح");
            valid = false;
        } else {
            email_sign_up.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            password_sign_up.setError("آكبر من ٤ احرف او اقل من ١٠ احرف");
            valid = false;
        } else {
            password_sign_up.setError(null);
        }


        return valid;
    }


}
