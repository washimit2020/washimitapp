package com.it.washm.alrayaduah.adapter;

/**
 * Created by washm on 1/4/17.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.model.NewsType;

public class CustomDrawerAdapter extends ArrayAdapter<NewsType> {

    Context context;
    List<NewsType> drawerItemList;
    int layoutResID;
    CustamViewFont viewFont;

    /**
     *
     * @param context
     * @param layoutResourceID
     * @param listItems
     * @param viewFont
     */
    public CustomDrawerAdapter(Context context, int layoutResourceID,
                               List<NewsType> listItems, CustamViewFont viewFont) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;
        this.viewFont=viewFont;
    }

    /**
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();

            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view
                    .findViewById(R.id.drawer_itemName);

            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }

        viewFont.setfont(drawerHolder.ItemName);


        NewsType dItem = (NewsType) this.drawerItemList.get(position);


        if(dItem.getIs_active().equalsIgnoreCase("1")){
            drawerHolder.ItemName.setText(dItem.getName_ar());

        }
        else {
            view.setEnabled(false);
            view.setOnClickListener(null);
            drawerHolder.ItemName.setTextColor(0xffbdbdbd);
            drawerHolder.ItemName.setText(dItem.getName_ar());

        }







        return view;
    }


    private static class DrawerItemHolder {
        TextView ItemName;
    }
}
