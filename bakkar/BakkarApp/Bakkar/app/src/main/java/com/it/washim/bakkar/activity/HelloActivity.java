package com.it.washim.bakkar.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyButtonView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HelloActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.log_in)
    MyButtonView log_in;
    @BindView(R.id.skip)
    MyButtonView skip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haloo);
        ButterKnife.bind(this);
        log_in.setOnClickListener(this);
        skip.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.log_in:
                startActivity(new Intent(getApplicationContext(), LogInActivity.class));

                break;
            case R.id.skip:
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));



                break;


        }
    }


}
