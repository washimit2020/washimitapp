package com.washimit.questionandanswer.questionandanswer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.jesusm.holocircleseekbar.lib.HoloCircleSeekBar;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.TextViewCustem;
import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
//import com.jesusm.holocircleseekbar.lib.HoloCircleSeekBar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.washimit.questionandanswer.questionandanswer.R.id.seekBar;

/**
 * A simple {@link Fragment} subclass.
 */
public class  StatisticsFragment extends Fragment {
    HoloCircleSeekBar seekBar;
    TextViewCustem read_time, page_number, page_number_no_read, last_log_in;
    PreferenceClass pref;
    Date date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_statistics, container, false);
        pref = new PreferenceClass(getActivity());
        read_time = (TextViewCustem) view.findViewById(R.id.read_time);
        page_number = (TextViewCustem) view.findViewById(R.id.page_number);
        last_log_in = (TextViewCustem) view.findViewById(R.id.last_log_in);
        page_number_no_read = (TextViewCustem) view.findViewById(R.id.page_number_no_read);
        seekBar = (HoloCircleSeekBar) view.findViewById(R.id.seekBar);

        seekBar.setValue(13);
        last_log_in.setText(pref.getLogInTime() + " " + "أخر دخول");
       seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });

        page_number.setText(12 + " " + "عدد الاجابات");
        page_number_no_read.setText(87 - 6> 10 ? 87 -6 + " " + "صفحة" : 87 - 6 + " " + "صفحات");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a");
        date = new Date();
        pref.setlogOutTime(dateFormat.format(date) + "");

        try {

            Date date1 = dateFormat.parse(new PreferenceClass(getContext()).getLogInTime());
            Date date2 = dateFormat.parse(new PreferenceClass(getContext()).getLogOutTime());

            printDifference(date1, date2);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return view;
    }


    public void printDifference(Date startDate, Date endDate) {

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
//        different = different % minutesInMilli;

//        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d يوم, %d ساعة, %d دقيقة",
                elapsedDays,
                elapsedHours, elapsedMinutes);


        read_time.setText(elapsedHours + ":" + elapsedMinutes + " " + "دقيقة قراءة");
    }
}
