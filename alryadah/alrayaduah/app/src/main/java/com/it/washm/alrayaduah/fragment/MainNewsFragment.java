package com.it.washm.alrayaduah.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import com.it.washm.alrayaduah.NewsDetailsActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.adapter.MainNewsParallaxAdapterHider;
import com.it.washm.alrayaduah.customview.CustamTextSliderView;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.news;

/**
 * A simple {@link Fragment} subclass.
 */

public class MainNewsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ViewPagerEx.OnPageChangeListener {
    Requests requests;
    SliderLayout mDemoSlider;
    RecyclerView mRecyclerView;
    MainNewsParallaxAdapterHider mAdapter;
    LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    ArrayList<news> newsList;
    ArrayList<news> response;
    Button retry;
    TextView noData;
    ProgressBar progressBar;

    Handler handler;
    Runnable runnable;
    String[] date;
    int position;
    public String urls;
    Intent intent;
    public static Activity activity;
    CustamViewFont viewFont;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container.clearDisappearingChildren();

        View view = inflater.inflate(R.layout.fragment_main_news, container, false);
        // Inflate the layout for this fragment
        View hid = LayoutInflater.from(getActivity()).inflate(R.layout.slider_header, null);

        viewFont = new CustamViewFont(getActivity(), "Cairo-SemiBold.ttf");

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        activity = getActivity();


        newsList = new ArrayList<>();
        response = new ArrayList<>();

        handler = new Handler();

        // textSliderView = new TextSliderView(getContext());

        noData = (TextView) view.findViewById(R.id.noData);

        retry = (Button) view.findViewById(R.id.retry);

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        mDemoSlider = (SliderLayout) hid.findViewById(R.id.slider);


        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);

        requests = new Requests(getContext(), getActivity());

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        // use a linear layout manager
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setVisibility(View.GONE);

        // create an Object for Adapter
        mAdapter = new MainNewsParallaxAdapterHider(getActivity(), newsList, mRecyclerView);

        mAdapter.setHasStableIds(true);

        mAdapter.setParallaxHeader(hid, mRecyclerView);
        //mAdapter.setHasStableIds(true);
        // set the adapter object to the Recyclerview

        mRecyclerView.setAdapter(mAdapter);
        mDemoSlider.stopAutoCycle();
        mDemoSlider.addOnPageChangeListener(this);
        //textSliderView.setOnSliderClickListener(this);

        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //add null , so the adapter will check view_type and show progress bar at bottom

                runnable = new Runnable() {
                    public void run() {
                        newsList.add(null);
                        mAdapter.notifyItemInserted(newsList.size() - 1);
                    }
                };
                handler.post(runnable);
                getNextNews(urls);

            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);

                                        gitNews();
                                        //gitBaner();

                                    }
                                }
        );

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeRefreshLayout.setRefreshing(true);
                gitNews();

            }
        });

        setFont();


        return view;
    }


    public void setFont() {
        //viewFont.setfont((TextView) textSliderView.getView().findViewById(R.id.description));
        viewFont.setfont(noData, "Cairo-Regular.ttf");
        viewFont.setfont(retry, "Cairo-Regular.ttf");
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


    }

    @Override
    public void onPageSelected(int position) {
        this.position = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        gitNews();
        // gitBaner();

    }

    public void gitNews() {
        // progressBar.setVisibility(View.VISIBLE);
        // mRecyclerView.setVisibility(View.GONE);
        requests.StringRequest(Request.Method.GET, "news", "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                newsList.clear();
                mDemoSlider.removeAllSliders();

                JSONObject object = null;
                JSONObject message = null;
                JSONObject news = null;
                JSONArray data = null;
                JSONArray banners = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {

                        message = object.getJSONObject("message");

                        banners = message.getJSONArray("banners");

                        news = message.getJSONObject("news");

                        urls = news.getString("next_page_url");

                        mAdapter.setUrl(news.getString("next_page_url"));

                        data = news.getJSONArray("data");


                        response =AppHelper.get_news_response(banners);

                        newsList.addAll(AppHelper.get_news_response(data));

                        for (int i = 0; i < response.size(); i++) {

                            CustamTextSliderView textSliderView = new CustamTextSliderView(getActivity());

                            // initialize a SliderLayout
                            textSliderView
                                    .description(response.get(i).getTitle())
                                //    .description("احتمال شديد لتساقط الثلوج اليوم في المملكة")
                                    .image(Links.UrlImg + response.get(i).getAttachment().get(0).getPath())
                                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)

                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            Log.d("slider_position", position + "id=" + response.get(position).getId());
                                            date = response.get(position).getUpdated_at().split(" ");

                                            intent = new Intent(getActivity(), NewsDetailsActivity.class);
                                            intent.putExtra("date", date[0]);
                                            intent.putExtra("time", date[1]);
                                            intent.putExtra("newId", response.get(position).getId());
                                            intent.putExtra("img_new", response.get(position).getAttachment().get(0).getPath());
                                            intent.putExtra("img_user", response.get(position).getUser().getAttachment().get(0).getPath());
                                            intent.putExtra("title", response.get(position).getTitle());
                                            if (response.get(position).getUser().getNick_name() != null) {
                                                intent.putExtra("name", response.get(position).getUser().getNick_name());
                                            } else {
                                                intent.putExtra("name", response.get(position).getUser().getFirst_name() + " " + response.get(position).getUser().getLast_name());
                                            }
                                            intent.putExtra("content", response.get(position).getContent());

                                            NewsDetailsActivity.attachments = response.get(position).getAttachment();

                                            startActivity(intent);
                                        }
                                    });


                            //add your extra information
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle().putString("extra", response.get(i).getContent());
                            mDemoSlider.addSlider(textSliderView);
                            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.ZoomOut);
                         //   mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                            mDemoSlider.setCustomAnimation(new DescriptionAnimation());

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (response.size() > 0) {
                    mDemoSlider.setVisibility(View.VISIBLE);

                } else {
                    mDemoSlider.setVisibility(View.GONE);

                }

                if (newsList.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    noData.setVisibility(View.GONE);
                    retry.setVisibility(View.GONE);
                    mAdapter.setLoaded();
                    mAdapter.notifyDataSetChanged();


                } else {
                    progressBar.setVisibility(View.GONE);
                    noData.setText("لا يوجد أخبار");
                    noData.setVisibility(View.VISIBLE);
                    retry.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.GONE);
                }


                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onRequestError(VolleyError errorMessage) {
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                noData.setText("لا يوجد اتصال بالانترنت");
                noData.setVisibility(View.VISIBLE);
                retry.setVisibility(View.VISIBLE);

                mRecyclerView.setVisibility(View.GONE);


            }
        });

    }

    public void getNextNews(String url) {
        requests.StringRequest(Request.Method.GET, url, "", false, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {


                try {
                    newsList.remove(newsList.size() - 1);
                    mAdapter.notifyItemRemoved(newsList.size() + 1);

                } catch (Exception e) {

                }
                JSONObject object = null;
                JSONObject message = null;
                JSONObject news = null;
                JSONArray data = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {

                        message = object.getJSONObject("message");
                        news = message.getJSONObject("news");

                        urls = news.getString("next_page_url");
                        mAdapter.setUrl(news.getString("next_page_url"));

                        data = news.getJSONArray("data");

                        //  newsList.addAll(AppHelper.get_news_response(data));

                        for (int i = 0; i < data.length(); i++) {

                            Gson gson = new Gson();
                            newsList.add(gson.fromJson(data.get(i).toString(), news.class));
                            mAdapter.notifyItemInserted(newsList.size());

                        }


                        mAdapter.setLoaded();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            @Override
            public void onRequestError(VolleyError errorMessage) {


            }
        });

    }


}
