package com.it.washm.alrayaduah;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.adapter.CommentAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.model.Attachment;
import com.it.washm.alrayaduah.model.AttachmentType;
import com.it.washm.alrayaduah.model.Comments;
import com.it.washm.alrayaduah.model.User;
import com.it.washm.alrayaduah.model.UserRole;
import com.it.washm.alrayaduah.model.replies;
import com.it.washm.alrayaduah.registration.LogInActivity;

public class commentActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.comment_Recycler_view)
    RecyclerView recyclerView;
    // @InjectView(R.id.comment_refresh_layout)SwipeRefreshLayout refreshLayout;
    @BindView(R.id.comment_text)
    EditText comment;
    @BindView(R.id.comment_but)
    ImageView comment_but;
    @BindView(R.id.no_comment)
    TextView no_comment;
    @BindView(R.id.progressBar)
    AVLoadingIndicatorView progressBar;

    HashMap<String, String> Stringparams;
    LinearLayoutManager manager;
    CommentAdapter mAdapter;
    public String urls;
    Requests requests;
    ArrayList<Comments> commentsList;
    // Handler handler ;
    // Runnable runnable;
    String NewsId;
    Gson gson;
    Bundle extras;
    SharedPref pref;
    Intent intent;
    CustamViewFont viewFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        viewFont = new CustamViewFont(this, "Cairo-Regular.ttf");
        setFont();
        pref = new SharedPref(getApplicationContext(), "MyPref");
        Stringparams = new HashMap<String, String>();

        extras = getIntent().getExtras();
        if (extras != null) {
            NewsId = extras.getString("newId");

        }


        commentsList = new ArrayList<>();
        gson = new Gson();
        //   handler = new Handler();

        requests = new Requests(getApplicationContext(), this);

        recyclerView.setHasFixedSize(true);

        manager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(manager);


        mAdapter = new CommentAdapter(this, commentsList, recyclerView);

        recyclerView.setAdapter(mAdapter);


        getComments();

        comment_but.setClickable(false);

        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (comment.getText().length() > 0) {
                    comment_but.setClickable(true);
                    comment_but.setImageResource(R.drawable.ic_send_act);
                } else {
                    comment_but.setClickable(false);
                    comment_but.setImageResource(R.drawable.ic_send);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (comment.getText().length() > 0) {
                    comment_but.setClickable(true);
                    comment_but.setImageResource(R.drawable.ic_send_act);
                } else {
                    comment_but.setClickable(false);
                    comment_but.setImageResource(R.drawable.ic_send);
                }
            }
        });

//        refreshLayout.setOnRefreshListener(this);
//
//        refreshLayout.post(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        refreshLayout.setRefreshing(true);
//                                        getComments();
//
//                                    }
//                                }
//        );


        comment_but.setOnClickListener(this);

    }

    public void setFont() {
        viewFont.setfont(comment, "Cairo-Regular.ttf");
        viewFont.setfont(no_comment, "Cairo-Regular.ttf");

    }

    public void getComments() {
        if (AppHelper.isOnline(this)) {
            recyclerView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            requests.StringRequest(Request.Method.GET, "comments/" + NewsId, "", true, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {

                    commentsList.clear();
                    JSONObject object = null;
                    JSONObject message = null;
                    JSONObject comments = null;

                    try {
                        object = new JSONObject(result);
                        if (object.getBoolean("success")) {
                            message = object.getJSONObject("message");
                            comments = message.getJSONObject("comments");

                            urls = comments.getString("next_page_url");
                            mAdapter.setUrl(urls);

                            mAdapter.setUrl(comments.getString("next_page_url"));
                            //  commentsList.addAll((Collection<? extends Comments>) gson.fromJson(comments.getJSONArray("data").toString(), new TypeToken<List<Comments>>() {}.getType()));


                            commentsList.addAll(AppHelper.get_comment_response(comments.getJSONArray("data")));
                            if (!urls.equalsIgnoreCase("null")) {
                                commentsList.add(0, null);
                                mAdapter.notifyItemInserted(0);
                            }
                        }

                        //  refreshLayout.setRefreshing(false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (commentsList.size() > 0) {
                        no_comment.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setLoaded();
                        mAdapter.notifyDataSetChanged();

                        recyclerView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                recyclerView.smoothScrollToPosition(commentsList.size());

                            }
                        }, 10);

                    } else {
                        progressBar.setVisibility(View.GONE);
                        no_comment.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }


                }

                @Override
                public void onRequestError(VolleyError errorMessage) {

                }
            });
        } else {
            no_comment.setText("لا يوجد اتصال بالانترنت :(");
            no_comment.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }


//    @Override
//    public void onRefresh() {
//        refreshLayout.setRefreshing(true);
//        getComments();
//
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.comment_but:
                if (AppHelper.IsLogIn(this)) {
                    comment_but.setClickable(false);
                    Stringparams.put("content", comment.getText().toString());
                    Stringparams.put("news_id", NewsId);
                    requests.StringRequest(Request.Method.POST, "comments", Stringparams, "", new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            JSONObject object = null;
                            JSONObject message = null;
                            JSONObject comments = null;


                            try {
                                object = new JSONObject(result);
                                if (!object.getBoolean("success")) {
                                    return;
                                }
                                message = object.getJSONObject("message");
                                comments = message.getJSONObject("comment");
                                ArrayList<Attachment> attachments = new ArrayList<Attachment>();
                                attachments.add(new Attachment("", pref.gitShared("path"), pref.gitShared("id"), "", new AttachmentType("", "", "")));

                                commentsList.add(commentsList.size(), new Comments(comments.getString("id"), comments.getString("content"),
                                        comments.getString("created_at"), comments.getString("updated_at"), 0, ""
                                        , false, new ArrayList<replies>()
                                        , new User(pref.gitShared("id"), pref.gitShared("first_name"), pref.gitShared("last_name"),
                                        pref.gitSharedNek("nick_name"), "", "", "", new UserRole("", "", "", ""), attachments)));

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (commentsList.size() > 0) {
                                no_comment.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                mAdapter.notifyItemInserted(commentsList.size() + 1);
                                recyclerView.smoothScrollToPosition(commentsList.size());

                            } else {
                                no_comment.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }

                            comment_but.setClickable(true);
                            comment.setText("");
                        }

                        @Override
                        public void onRequestError(VolleyError errorMessage) {
                            comment_but.setClickable(true);

                        }
                    });
                } else {
                    AppHelper.showDialog(this, "يجب تسجيل الدخول للتعليق","تسجيل الدخول","", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getApplicationContext(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
                }
                break;


        }
    }
}
