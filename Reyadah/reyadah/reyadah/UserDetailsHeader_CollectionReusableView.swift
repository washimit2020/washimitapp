//
//  UserDetailsHeader_CollectionReusableView.swift
//  reyadah
//
//  Created by WASHM on 4/23/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import ImageSlideshow
import Alamofire
import SwiftyJSON

class UserDetailsHeader_CollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var shortDescription: UILabel!
    @IBOutlet weak var locationText: UILabel!
    @IBOutlet weak var totalFollowers: UILabel!
    @IBOutlet weak var followButton: UIButton!
    @IBOutlet weak var ratingStars: CosmosView!
    @IBOutlet weak var phoneCall: UIButton!
    @IBOutlet weak var share: UIButton!
    @IBOutlet weak var location: UIButton!
    @IBOutlet weak var info: AspectFitButton!
    @IBOutlet weak var slider: ImageSlideshow!
    
    @IBOutlet weak var products_label: UILabel!
    var model:Any?{
        didSet{
            updateView()
        }
    }
    lazy  var parameters = [String:String]()
    lazy var  dialog = Rating_ViewController(nibName: "Rating_ViewController", bundle: nil)
    var apiRequest:ApiRequest?{
        didSet{
            dialog.apiRequest = apiRequest
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        self.slider.addGestureRecognizer(gesture)
        setup_cosmos(cosmos: ratingStars)
        setup_slider(slider: slider)
    }
    
    func updateView() {
        
        if Gate.is_authed(){
            dialog.model = model
            dialog.delegate = self
      
            
            ratingStars.didTouchCosmos = {
                _ in
                self.ratingStars.isUserInteractionEnabled = false
                self.window?.rootViewController?.present(self.dialog, animated: true, completion: {
                    self.ratingStars.isUserInteractionEnabled = true
                })
            }
        }
        
        guard let user = self.model as? Branch else{
            return
        }
        
        parameters["user_id"] = "\(user.id!)"
        parameters["follow_type"] = "\(Constants.TYPE_FOLLOW_USER)"
        
        let images = make_slider_images(id: user.id )
        
        slider.setImageInputs(images as! [KingfisherSource])
        ratingStars.rating = user.rating
        followButton.setImage(check_user_follow_status(user: user), for: .normal)
        followButton.addTarget(nil, action: #selector(followUnFollow(_:)), for: .touchUpInside)
        shortDescription.text = user.info
        locationText.text = make_location_text(user)
        totalFollowers.text = "\(user.totalFollowers!)"
        products_label.isHidden = user.user_id != 0

        
    }
    
    func didTap() {
        self.slider.presentFullScreenController(from: self.window!.rootViewController!)
    }
}

extension UserDetailsHeader_CollectionReusableView:RatingDelegate{
    
    public func followUnFollow(_ sender:AnyObject) {
        guard let user = model as? Festival else {
            return
        }
        
        if user.followed{
            user.totalFollowers! -= 1
        }else{
            user.totalFollowers! += 1
        }
        
        self.totalFollowers.text = "\(user.totalFollowers!)"
        
        user.followed = !user.followed
        
        self.followButton.setImage(check_user_follow_status(user: user), for: .normal)
        
        if !user.from.isEmpty{
            apiRequest?.call_follow_unfollow(user.id,"\(Constants.TYPE_FOLLOW_FESTIVAL)")
        }else{
            apiRequest?.call_follow_unfollow(user.id)
        }

    }
    
    
    func rating(_ rating: Double?) {
        guard let rating = rating else {
            return
        }
        
        if let user  = self.model as? User{
            user.rating = rating
        }else if let product = self.model as? Product{
            product.rating = rating
        }
        
        self.ratingStars.rating = rating

    }
}
