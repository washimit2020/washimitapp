package com.washimit.it.ryadah.registrationActivity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.SocialMedia;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SocialMediaActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.text_name)
    TextViewCast text_name;
    @BindView(R.id.text_des)
    TextViewCast text_des;

    @BindView(R.id.registration)
    ImageView registration;

    @BindView(R.id.name)
    EditTextView name;
    @BindView(R.id.email_text)
    EditTextView email_text;
    @BindView(R.id.des)
    EditTextView des;
    @BindView(R.id.phone)
    EditTextView phone;
    @BindView(R.id.street_name)
    EditTextView street_name;
    @BindView(R.id.sector)
    EditTextView sector;
    SocialMedia media;
    Gson gson;
    ApiRequest request;
    SharedPref pref;
    ArrayList<Part> parts;
    String token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_social_media);
        ButterKnife.bind(this);

        // Get token
         token = FirebaseInstanceId.getInstance().getToken();

        // Log
        Log.d("msg", getString(R.string.msg_token_fmt, token));




        gson = new Gson();
        media = gson.fromJson(getIntent().getExtras().getString("media"), SocialMedia.class);
        request = new ApiRequest(this);
        pref = new SharedPref(this);
        parts = new ArrayList<Part>();

        addData();
        updateUi(Integer.parseInt(media.getType_id()));
        back.setOnClickListener(this);
        registration.setOnClickListener(this);





    }


    public void loginWithSocial() {


        parts.add(new StringPart("login_type", String.valueOf(Constant.SOCIAL_REGISTER)));
        parts.add(new StringPart("name", name.getText().toString()));
        parts.add(new StringPart("email", email_text.getText().toString()));
        parts.add(new StringPart("reg_id", token+""));
        parts.add(new StringPart("type_id", media.getType_id()));
        parts.add(new StringPart("street_name", street_name.getText().toString()));
        parts.add(new StringPart("sector_name", sector.getText().toString()));
        parts.add(new StringPart("phone_number", phone.getText().toString()));
        parts.add(new StringPart("info", des.getText().toString()));
        parts.add(new StringPart("token", media.getToken()));

        pref.setLogInTocen(media.getToken());
        request.MultipartRequest("POST", "register", getString(R.string.register), parts, new MultipartCallback() {
            @Override
            public void onSuccess(String result_) throws JSONException {
                Log.d("result", result_);
                JSONObject result = null;
                JSONObject msg = null;

                try {
                    result = new JSONObject(result_);
                    msg = result.getJSONObject("msg");
                    Gson gson = new Gson();
                    com.washimit.it.ryadah.model.user.User user = gson.fromJson(msg.getString("user"), com.washimit.it.ryadah.model.user.User.class);
                    user.setToken(msg.getString("token"));

                    pref.setUser(gson.toJson(user));


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("result", result_);

                openActivity(MainActivity.class, Integer.parseInt(media.getType_id()));


            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("lksjglksdjgsg", errorMessage);

            }
        });

    }


    public void openActivity(Class activity, int userType) {
        Intent intent = new Intent(SocialMediaActivity.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);

        startActivity(intent);
        this.finish();

    }


    public boolean validation() {


        if (name.getText().toString().equalsIgnoreCase("")) {
            name.setError(getResources().getString(R.string.fill_name));
            name.requestFocus();
            return false;
        }
        if (des.getText().toString().equalsIgnoreCase("")) {
            des.setError(getResources().getString(R.string.fill_desc));
            des.requestFocus();

            return false;
        }

        if (!(Patterns.PHONE.matcher(phone.getText().toString()).matches() && phone.getText().toString().length() >= 10)) {
            phone.setError(getResources().getString(R.string.fill_mobile_number));
            phone.requestFocus();
            return false;
        }
        if (sector.getText().toString().equalsIgnoreCase("")) {
            sector.setError(getResources().getString(R.string.fill_street));
            sector.requestFocus();

            return false;
        }
        if (street_name.getText().toString().equalsIgnoreCase("")) {
            street_name.setError(getResources().getString(R.string.fill_street));
            street_name.requestFocus();
            return false;
        }


        return true;
    }

    public void addData() {
        name.setText(media.getName());
        email_text.setText(media.getEmail());
        des.setText(media.getInfo());
        street_name.setText(media.getStreet_name());
        sector.setText(media.getSector());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.registration:


                if (validation()) {

                    loginWithSocial();
                }


                break;
            case R.id.back:

                finish();


                break;


        }
    }


    public void updateUi(int userType) {

        if (userType == 1) {

            text_name.setText(R.string.store_name);
            text_des.setText(R.string.bio_store);
        }
        if (userType == 2) {

        }
        if (userType == 3) {

            text_name.setText(R.string.name);
            text_des.setText(R.string.desc);

        }


    }
}
