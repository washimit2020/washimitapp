package com.washimit.questionandanswer.questionandanswer.custumView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by mohammad on 5/31/2017.
 */

public class MyEditText  extends EditText {

    public MyEditText(Context context, AttributeSet attrs,
                                 int defStyle) {
        super(context, attrs, defStyle);
        init();

    }

    public MyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public MyEditText(Context context) {
        super(context);
        init();

    }

    public  void init() {

        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/TheSansArab-Light_0.ttf");
        setTypeface(tf, 1);
    }

}