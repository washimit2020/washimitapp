<?php

namespace App\Http\Controllers\Places;

use App\BlockedUser;
use App\Http\Constants;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param int $type
     * @return \Illuminate\Http\Response
     */
    public function index($type = Constants::_TYPE_STORE)
    {
        $conditions [] = ['type_id', $type];
        $conditions [] = ['status_id', Constants::_STATUS_ACTIVE];

        $users = User::where($conditions);

        if (auth()->guard('api')->check()) {
            $current_user = auth()->guard('api')->user();
            $conditions [] = ["id", "<>", $current_user->id];
//            $conditions [] = ["email", "<>", $current_user->email];
            $users = User::where($conditions);
             check_if_blocked($users);

        }
        $users = $users->withCount(['followedBy' => function ($query) {
            $query->where('status_id', Constants::_STATUS_ACTIVE);
        }])->orderBy('rating', 'DESC')->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['users' => $users]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if ($user->status_id != Constants::_STATUS_ACTIVE) {
            abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
        }
        abort_if_blocked($user);

        $condition = [Constants::_STATUS_ACTIVE];

        $is_costumer = $user->type_id == Constants::_TYPE_COSTUMER;

        if (auth()->guard('api')->check()) {
            if ((auth()->guard('api')->user()->id != $user->id)) {
                if ($is_costumer)
                abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
            }else{
                $condition [] = Constants::_STATUS_HIDDEN;
            }



        } elseif ($is_costumer) {
            abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
        }

        $user->products = $user->products()->whereIn("status_id",$condition)->latest()->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['user' => $user]));
    }

}
