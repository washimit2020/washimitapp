//
//  AppDelegate.swift
//  al-haj
//
//  Created by WASHM on 7/23/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit
import SQLite

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DatabaseUtils.shared.copyDb()
        UITableViewCell.appearance().backgroundColor = .clear
        UITextView.appearance().backgroundColor = .clear
        UINavigationBar.appearance().setBackgroundImage(#imageLiteral(resourceName: "nav") , for: .default)
        UINavigationBar.appearance().shadowImage = #imageLiteral(resourceName: "shadow")
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white , NSAttributedStringKey.font : UIFont.titleFont]
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "icon_back")
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = #imageLiteral(resourceName: "icon_back")
        //        UIFont.familyNames.forEach({ familyName in
        //            let fontNames = UIFont.fontNames(forFamilyName: familyName)
        //            print(familyName, fontNames)
        //        })
        
        do {
            let pages =  DatabaseUtils.shared.pages_t
            let images = DatabaseUtils.shared.images_t
            let query = pages.select([pages[*],images[Expressions.title]])
            Utils.shared.pages = try DatabaseUtils.shared.db!.prepare(query.join(.leftOuter ,images, on: Expressions.page_id == pages[Expressions.id])).map({ (row)  in
                return Page(row)
            })
           Utils.shared.sections = try DatabaseUtils.shared.db!.prepare(DatabaseUtils.shared.sections_t).map({
                return Section($0)
            })
        }catch{
            print(error)
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
}

