<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        "id","data","read_at","user_id","type"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function read(){
        $this->update(['read_at'=>Carbon::now()]);
    }
}