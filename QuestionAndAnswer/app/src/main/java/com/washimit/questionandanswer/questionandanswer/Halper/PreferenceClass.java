package com.washimit.questionandanswer.questionandanswer.Halper;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.washimit.questionandanswer.questionandanswer.modle.User;

/**
 * Created by washm on 4/9/17.
 */

public class PreferenceClass {
    public static final String PREFERENCE_NAME = "MyPref";
    private final SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    Gson gson;
    public PreferenceClass(Context context) {
        sharedpreferences = context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        gson = new Gson();
    }




    public void setUser(String user) {

        editor.putString("user", user);
        editor.commit();


    }

    public User getUser() {

        return gson.fromJson(sharedpreferences.getString("user", null), User.class);


    }


    public void clear() {
        sharedpreferences.edit().clear().commit();
    }



    public int getPageId() {
        int count = sharedpreferences.getInt("pagId", 0);
        return count;
    }

    public void setPageId(int pageId) {
        editor.putInt("pagId", pageId);
        editor.commit();
    }

    public void clearPageId() {
        editor.remove("pagId");
        editor.commit();
    }


    public String getfontType() {
        String fontType = sharedpreferences.getString("fontType", "TheSansArab-Light_0.ttf");
        return fontType;
    }

    public void setfontType(String fontType) {
        editor.putString("fontType", fontType);
        editor.commit();
    }

    public void clearfontType() {
        editor.remove("fontType");
        editor.commit();
    }

    public int gettextSize() {
        int textSize = sharedpreferences.getInt("textSize", 15);

        return textSize<10?10:textSize;
    }

    public void settextSize(int textSize) {
        editor.putInt("textSize", textSize);
        editor.commit();
    }

    public void cleartextSize() {
        editor.remove("textSize");
        editor.commit();
    }

    public boolean getscrenMode() {
        boolean screnMode = sharedpreferences.getBoolean("screnMode", true);
        return screnMode;
    }

    public void setscrenMode(boolean screnMode) {
        editor.putBoolean("screnMode", screnMode);
        editor.commit();
    }

    public void clearscrenMode() {
        editor.remove("screnMode");
        editor.commit();
    }




    //A7sayat al app

    public String getLogInTime() {
        return sharedpreferences.getString("logInTime", "");
    }

    public void setlogInTime(String date) {
        editor.putString("logInTime", date);
        editor.commit();
    }

    public String getLogOutTime() {
        return sharedpreferences.getString("logOutTime", "اليوم هي اول زيارة");
    }

    public void setlogOutTime(String date) {
        editor.putString("logOutTime", date);
        editor.commit();
    }


    public String getAvgTime() {
        return sharedpreferences.getString("avgTime", "");
    }

    public void setAvgTime(String date) {
        editor.putString("avgTime", date);
        editor.commit();
    }
}