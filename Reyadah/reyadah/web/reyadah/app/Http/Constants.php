<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/16/17
 * Time: 11:23 AM
 */

namespace App\Http;


use Illuminate\Support\Facades\Storage;

class Constants
{
    //BEGIN TYPES

    CONST _TYPE_STORE = 1;
    CONST _TYPE_SHOW = 2;
    CONST _TYPE_COSTUMER = 3;
    CONST _TYPE_FESTIVAL = 4;
    CONST _TYPE_PRODUCT = 5;
    CONST _TYPE_AVATAR_IMAGE = 6;
    CONST _TYPE_COVER_IMAGE = 7;

    CONST _TYPE_PRODUCT_ATTACHMENT = 8;
    CONST _TYPE_PRODUCT_LIKE = 9;
    CONST _TYPE_PRODUCT_WISHLIST = 10;

    CONST _TYPE_FOLLOW_USER = 11;
    CONST _TYPE_FOLLOW_FESTIVAL = 12;
    CONST _TYPE_FESTIVAL_LICENSE = 13;
    CONST _TYPE_FESTIVAL_ATTACHMENT = 14;


    //END TYPES

    //BEGIN STATUSES

    CONST _STATUS_ACTIVE = 1;
    CONST _STATUS_INACTIVE = 2;
    CONST _STATUS_HIDDEN = 3;
    CONST _STATUS_BLOCKED = 4;
    CONST _STATUS_DECLINED = 5;
    CONST _STATUS_DELETED = 6;

    //BEGIN STATUSES


    //BEGIN C0DE

    //begin middleware and abilities
    CONST _MIDDLEWARE_SHOWS = 'shows_middleware';
    CONST _MIDDLEWARE_ADD_PRODUCT = 'add_product_middleware';
    CONST _MIDDLEWARE_UPDATE_COMMENT = 'update_comment_middleware';
    CONST _MIDDLEWARE_UPDATE_PRODUCT = 'update_product_middleware';
    CONST ABILITY_ADD_PRODUCT = 'add-product';
    CONST ABILITY_HAS_BRANCHES = 'has-branches';
    //end middleware

    CONST RESPONSE_SUCCESS = 'SUCCESS';
    CONST RESPONSE_FAILED = 'FAILED';
    CONST RESPONSE_CODE_UNAUTHORIZED = 401;
    CONST RESPONSE_CODE_FORBIDDEN = 403;
    CONST RESPONSE_CODE_UNPROCESSABLE = 422;

    CONST MAX_STRING_LENGTH = 191;


    CONST NORMAL_REGISTER = 1;
    CONST SOCIAL_REGISTER = 2;

    CONST FESTIVAL_COMMENT = 'festivals';
    CONST PRODUCT_COMMENT = 'products';

    CONST USER_SHARED = 'users';
    CONST BRANCH_SHARED = 'branches';

    CONST MIME_TYPE = "max:6000|mimes:jpeg,bmp,png";
    CONST MAXIMUM_ALLOWED_IMAGE = 3;

    CONST DEFAULT_AVATAR = 'default-avatar.png';
    CONST DEFAULT_COVER = 'default-cover.png';
    CONST DEFAULT_PRODUCT = 'default-product.png';

    CONST PAGINATION = 19;



    CONST SEARCH_TYPE_ALL = 1;
    CONST SEARCH_TYPE_PRODUCT = 2;
    CONST SEARCH_TYPE_STORE = 3;
    CONST SEARCH_TYPE_SHOW = 4;
    CONST SEARCH_TYPE_FESTIVAL = 5;

    CONST SEARCH_ORDER_RATING = "rating";
    CONST SEARCH_ORDER_LOWEST_PRICE = "price";

    //END CODE
}