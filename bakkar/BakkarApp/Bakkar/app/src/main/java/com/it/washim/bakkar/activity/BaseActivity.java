package com.it.washim.bakkar.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;

import com.google.gson.Gson;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.ApiRequest;
import com.it.washim.bakkar.halper.SharedPref;
import com.it.washim.bakkar.interfase.OnDialogCollBack;

/**
 * Created by mohammad on 8/7/2017.
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressDialog dialog;
    private Gson gson;
    private Dialog alertDialog;

    /**
     *
     * @param msg
     */
    public void showDialog(String msg) {
        if (dialog == null) dialog = new ProgressDialog(this);
        dialog.setTitle("بكار");
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(msg);
        if (!dialog.isShowing()) dialog.show();

    }


    public void hideDialog() {
        if (dialog.isShowing()) dialog.dismiss();

    }

    /**
     *
     * @return
     */
    public ApiRequest request() {
        return ApiRequest.getInstance(this);
    }

    /**
     *
     * @return
     */
    public Gson gson() {
        if (gson == null) gson = new Gson();
        return gson;
    }

    /**
     *
     * @param msg
     * @param collBack
     */
    public void showAlertDialog(String msg, final OnDialogCollBack collBack) {
        if (dialog == null) alertDialog = new Dialog(this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        alertDialog.setContentView(R.layout.dialog_login);

        MyTextView text = (MyTextView) alertDialog.findViewById(R.id.msg);
        text.setText(msg);

        alertDialog.findViewById(R.id.log_in).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collBack.login();
            }
        });
        alertDialog.findViewById(R.id.hide).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collBack.hide();
                alertDialog.cancel();
            }
        });


        if (!alertDialog.isShowing()) alertDialog.show();
    }

    /**
     *
     * @return
     */
    public SharedPref shardPref() {

        return SharedPref.getSharedInstance(this);

    }

    /**
     *
     * @param activity
     * @param key
     * @param value
     */
    public void openActivity(Class activity, String key, int value) {
        startActivity(new Intent(this, activity).putExtra(key, value));
    }
}
