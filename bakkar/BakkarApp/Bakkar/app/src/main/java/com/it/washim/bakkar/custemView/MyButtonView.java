package com.it.washim.bakkar.custemView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by mohammad on 7/27/2017.
 */

public class MyButtonView extends Button {
    public MyButtonView(Context context) {
        super(context);
        init();
    }

    public MyButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/TheSansArab-Light_0.ttf");
        setTypeface(tf, 1);
    }
}
