package com.it.washim.wojooohapp.Interfases;

import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.Model.Data;

import java.util.ArrayList;

/**
 * Created by mohammad on 9/19/2017.
 */

public interface OnRequestCollBack {
    void onResponse(Data response);

    void onErrorResponse(VolleyError error);
}
