//
//  ItemsLoader_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 4/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ItemsLoaderDelegate {
    func dataFiller(data:[JSON]);
    
}

protocol DataModelChanger {
    func change(_ json:JSON);
    
}
class ItemsLoader_CollectionViewController: UICollectionViewController{
    
    var refresher:UIRefreshControl?
    
    var dataModel:DataModel?
    
    var didCall = false
    
    var link = ""
    
    lazy var apiRequest = ApiRequest()
    
    var beginWithModel = ""
    
    var items = [Any]()
    
    var parameters = [String:String]()
    
    var loader_hidden = true
    
    var delegate: ItemsLoaderDelegate? = nil
    
    var dataModelChanger: DataModelChanger? = nil
    
    var selectedIndexPath:IndexPath?
    
    var httpMethod:HTTPMethod = .get
    
    var emptyLabel:UILabel?
    
    var always_hidden = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "
        if self.refresher == nil {
            let refresh = UIRefreshControl()
            
            refresh.addTarget(self, action: #selector(clearData(_:)), for: .valueChanged)
            self.refresher = refresh
            self.refresher?.tag = 123
            collectionView!.addSubview(self.refresher!)
            
            let width =  collectionView!.frame.width
            let height:CGFloat =  40
            
            let view = UILabel(frame: CGRect(x: 0 , y: collectionView!.frame.height/2 - height, width: width, height: height))
            view.text = "لا يوجد نتائج"
            view.textAlignment = .center
            view.textColor = UIColor.gray
            view.isHidden = true
            emptyLabel = view
            
            collectionView?.addSubview(emptyLabel!)
            
        }
        
        self.collectionView!.register(UINib(nibName: "Loading_CollectionReusableView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: "loader")
        
        
        collectionView?.alwaysBounceVertical = true
        
        items.removeAll()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let path_index = selectedIndexPath{
            self.collectionView?.reloadItems(at: [path_index])
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
            
        case UICollectionElementKindSectionFooter:
            let footView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                           withReuseIdentifier: "loader",
                                                                           for: indexPath) as! Loading_CollectionReusableView
            
            return footView
            
        default:
            //4
            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if !loader_hidden{
            loader_hidden = true
            
        }
        
        guard let data = dataModel else {
            return
        }
        if indexPath.row != items.count - 2
            ||
            data.next_page_url == nil{
            return
        }
        
        
        make_api_call(link + "?page=\(dataModel!.current_page! + 1)")
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
    }
    
    
    deinit {
        refresher = nil
        
        dataModel = nil
        
        items.removeAll()
        
        delegate = nil
        
        dataModelChanger = nil
        
        selectedIndexPath = nil
        
        emptyLabel = nil
        
        collectionView = nil
        
    }
}

extension ItemsLoader_CollectionViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForFooterInSection section: Int) -> CGSize{
        if !loader_hidden {
            return CGSize(width: self.view.frame.width, height: 30)
        }else{
            return CGSize.zero
        }
    }
}

extension ItemsLoader_CollectionViewController{
    
    func clearData(_ sender:Any? = nil)
    {
        make_api_call(nil)
    }
    
    
    
    func make_api_call(_ nextlink:String?){
        var current = link
        
        if let link = nextlink{
            current = link
        }
        
        loader_hidden = false
        
        apiRequest.make_request(parameters:parameters,apiImages: nil, link: current, method: self.httpMethod, completion: data_handler)
        
    }
    
    private func hideLoaders(){
        self.didCall = true
        self.refresher?.endRefreshing()
        self.emptyLabel?.isHidden = true
        self.loader_hidden = true
    }
    
    func showEmpty(){
        if !self.always_hidden{
            self.emptyLabel?.isHidden = false
        }
    }
    
    func data_handler(_ response:DataRequest?)->Void{
        guard let response = response else {
            showEmpty()
            self.present(self.apiRequest.costumeAlert, animated: true, completion: {
                self.apiRequest.handle_error_dialog(nil)
            })
            hideLoaders()
            return
        }
        
        
        response.responseJSON(completionHandler: {
            response in
            if response.result.isFailure{
                self.showEmpty()
                return
            }
            
            if response.response?.statusCode == 200{
                guard let json = response.data else{
                    self.showEmpty()
                    return
                }
                
                if let changer = self.dataModelChanger{
                    
                    changer.change(JSON(json))
                }else{
                    self.dataModel = DataModel(JSON(json), beginWith: self.beginWithModel )
                }
                
                guard let data_model = self.dataModel else{
                    self.items.removeAll()
                    self.collectionView?.reloadData()
                    self.showEmpty()
                    
                    return
                }
                
                if let page = data_model.current_page {
                    if page <= 1  {
                        self.items.removeAll()
                    }
                }else{
                    self.items.removeAll()
                    
                }
                
                guard let data = data_model.data else{
                    self.showEmpty()
                    self.collectionView?.reloadData()
                    return
                }
                
                if self.dataModel!.to == 0 {
                    self.showEmpty()
                    self.collectionView?.reloadData()
                    
                    return
                }
                
                self.delegate?.dataFiller(data: data)
                
                
                self.collectionView?.reloadData()
                
                
            }
        })
        self.hideLoaders()
    }
    
    
}


