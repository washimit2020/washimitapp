package com.it.washim.wojooohapp.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MapsFragment extends BaseFragment implements OnMapReadyCallback, View.OnClickListener {

    @BindView(R.id.map)
    MapView mapView;
    @BindView(R.id.no_data)
    TextView no_data;
    @BindView(R.id.retray)
    MyButtonView retray;
    private GoogleMap mMap;
    ArrayList<Post> list;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        retray.setOnClickListener(this);

        return view;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        getSalePoints();
        // Add a marker in Sydney and move the camera

    }


    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    public void getSalePoints() {

        mapView.setVisibility(View.VISIBLE);
        retray.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);

        getRequest().request(Request.Method.GET, "posts/" + Constant.PAGES_SELLING_PLACES, null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                list.addAll(response.data);
                Log.d("asfasfasf", list.size() + "");


                if (list.size() > 0) {
                    mapView.setVisibility(View.VISIBLE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);

                    mMap.clear();
                    for (int i = 0; i < list.size(); i++) {
                        LatLng newAddress = new LatLng(list.get(i).post_meta.location.lat, list.get(i).post_meta.location.lng);
                        mMap.addMarker(new MarkerOptions().position(newAddress).title(list.get(i).title));
                        // mMap.moveCamera(CameraUpdateFactory.newLatLng(newAddress));
                    }
                } else {
                    no_data.setText("لا يوجد");
                    mapView.setVisibility(View.GONE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                no_data.setText("لا يوجد اتصال بالانترنت");
                mapView.setVisibility(View.GONE);
                retray.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray:
                getSalePoints();
                break;


        }
    }
}
