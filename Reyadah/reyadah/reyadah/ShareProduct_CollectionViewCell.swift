//
//  ShareProduct_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/10/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ShareProduct_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var decline: UIButton!
    @IBOutlet weak var accept: UIButton!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var productName: UILabel!
    
    var shared:Shared?{
        didSet{
            updateView()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        accept.layer.cornerRadius = 10
        accept.clipsToBounds = true
        decline.layer.cornerRadius = 10
        decline.clipsToBounds = true
        decline.tag = Constants.STATUS_DECLINED
        accept.tag = Constants.STATUS_ACTIVE
    }
    
    func updateView(){
        guard let shared = shared else {
            return
        }
        
        avatar.set_product_image(product_id: shared.product.id)
        username.text = shared.shareable.name
        productName.text = shared.product.name
        if !Gate.is_show() || shared.status_id != Constants.STATUS_INACTIVE{
            make_status_text(status, shared.status_id)
            status.isHidden = false
            decline.superview?.isHidden = true
        }else{
            decline.superview?.isHidden = false
            status.isHidden = true
        }
    }
    
}
