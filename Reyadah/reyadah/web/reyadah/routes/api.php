<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'app-key'], function () {

    Route::post('register', 'Auth\RegisterController@create');
    Route::post('login', 'Auth\LoginController@login');

    Route::get('users/{type?}', 'Places\UserController@index')->where('type',
        '^[' . \App\Http\Constants::_TYPE_STORE .
        '|' . \App\Http\Constants::_TYPE_SHOW . ']$');

    Route::get('festivals', 'Places\FestivalsController@index');
    Route::get('user/{user}', 'Places\UserController@show');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    Route::group(['middleware' => 'auth:api'], function () {

        Route::post('search', 'SearchController@search');

        Route::group(['namespace' => 'Auth', 'prefix' => 'update'], function () {
            Route::post('profile', 'UpdateProfileController@profile');
//            Route::post('avatar', 'UpdateProfileController@avatar');
            Route::post('cover', 'UpdateProfileController@cover');
            Route::post('regId', 'UpdateProfileController@update_reg_id');
        });

        Route::group(['namespace' => 'Places'], function () {

            //begin follow
            Route::post('follow', 'FollowController@store');
            Route::get('following/{type}', 'FollowController@following')->where('type',
                '^[' . \App\Http\Constants::_TYPE_STORE .
                '|' . \App\Http\Constants::_TYPE_SHOW .'|' . \App\Http\Constants::_TYPE_FESTIVAL. ']$');
            Route::get('followed-by', 'FollowController@followed_by');
            //end follow

            //being branch
            Route::get('branch/{user?}', 'ShowBranchesController@index');

            Route::group(['middleware' => \App\Http\Constants::_MIDDLEWARE_SHOWS, 'prefix' => 'branch'], function () {
                Route::post('', 'ShowBranchesController@store');
                Route::post('{showBranch}/update', 'ShowBranchesController@update');
                Route::post('{showBranch}/delete', 'ShowBranchesController@destroy');
            });
            //end branch

            //begin block
            Route::post('block', 'BlockController@store');
            Route::get('block', 'BlockController@index');
            //end block

            //festivals
            Route::group(['prefix'=>'festivals'],function(){
                Route::post('/','FestivalsController@store');
                Route::get('user', 'FestivalsController@user_festivals');
            });
        });

        //products
        Route::group(['namespace' => 'Products', 'prefix' => 'product'], function () {
            Route::get('', 'ProductsController@index');
//            Route::get('new', 'ProductsController@new_index');
            Route::get('explorer', 'ProductsController@explorer');

            Route::group(['middleware' => \App\Http\Constants::_MIDDLEWARE_ADD_PRODUCT], function () {
                Route::post('', 'ProductsController@store');
            });

            Route::group(['middleware' => \App\Http\Constants::_MIDDLEWARE_UPDATE_PRODUCT], function () {

                Route::post('{product}/hide', 'ProductsController@hide');
                Route::post('{product}/update', 'ProductsController@update');
                Route::post('{product}/delete', 'ProductsController@destroy');

            });

            Route::post('share', 'SharedProductsController@store');
            Route::get('share', 'SharedProductsController@index');
            Route::get('shares/{product}/{type?}', 'SharedProductsController@product');

            Route::post('share/{shareable}/update', 'SharedProductsController@update');


            Route::get('wishlist', 'WishlistController@index');
            Route::post('wishlist', 'WishlistController@store');
            Route::post('like', 'LikeController@store');

        });


        Route::group(['prefix' => 'rating'], function () {
            Route::post('user', 'RatingController@user');
            Route::post('product', 'RatingController@product');
            Route::post('festival', 'RatingController@festival');
        });


        Route::group(['prefix' => 'comment'], function () {
            Route::post('', 'CommentController@store');

            Route::group(['middleware' => \App\Http\Constants::_MIDDLEWARE_UPDATE_COMMENT], function () {
                Route::post('{comment}/update', 'CommentController@update');
                Route::post('{comment}/delete', 'CommentController@destroy');
            });

            Route::get('{type}/{id}', 'CommentController@index');
        });



    });

});
