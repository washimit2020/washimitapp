package com.it.washim.wojooohapp.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.it.washim.wojooohapp.CustemView.MyTextView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnLoadMoreCollBack;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.activitys.BookDetailsActivity;
import com.it.washim.wojooohapp.activitys.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 9/17/2017.
 */

public class BookAdapter extends Adapter {
    private final int BOOK_ITEM = 0;
    private final int PROGRESS_TEM = 1;
    Context context;
    ArrayList<Post> list;
    boolean loading = true;
    String url;
    OnLoadMoreCollBack onLoadMoreCollBack;

    public BookAdapter(Context context, ArrayList<Post> list, RecyclerView recyclerView) {
        this.context = context;
        this.list = list;



        if (recyclerView.getLayoutManager() instanceof StaggeredGridLayoutManager) {

            final StaggeredGridLayoutManager layoutManager = (StaggeredGridLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        if (layoutManager.findLastVisibleItemPositions(null)[layoutManager.findLastVisibleItemPositions(null).length - 1] + 2 >= layoutManager.getItemCount()) {

                            if (!loading) {

                                if (onLoadMoreCollBack != null) {
                                    onLoadMoreCollBack.onLoad();
                                }
                                loading = true;

                            }

                        }
                    }
                }
            });
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progres, parent, false));

        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof BookViewHolder) {

            final Post post = list.get(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((MainActivity) context).openActivity(context, BookDetailsActivity.class, "book", ((MainActivity) context).getGson().toJson(post));


                }
            });


            ((BookViewHolder) holder).usa.setText("US " + Integer.parseInt(post.post_meta.book.price) * 0.266);
            ((BookViewHolder) holder).ksa.setText("KS " + post.post_meta.book.price);
            ((BookViewHolder) holder).bookName.setText(post.title);

            if (post.attachments.size() > 0) {
                Glide.with(context)
                        .load(Constant.IMAGE_URL + post.attachments.get(0).id)
                        .into(((BookViewHolder) holder).image);
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ((BookViewHolder) holder).image.setImageDrawable(context.getResources().getDrawable(R.drawable.defult, null));
                } else {
                    ((BookViewHolder) holder).image.setImageDrawable(context.getResources().getDrawable(R.drawable.defult));

                }
            }

        }
    }

    @Override
    public int getItemViewType(int position) {
        return list.get(position) == null ? PROGRESS_TEM : BOOK_ITEM;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setOnLoadMoreLesnar(OnLoadMoreCollBack onLoadMoreCollBack) {
        this.onLoadMoreCollBack = onLoadMoreCollBack;
    }

    public void isLoading() {
        loading = false;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.book_name)
        MyTextView bookName;
        @BindView(R.id.USA)
        MyTextView usa;
        @BindView(R.id.KSA)
        MyTextView ksa;

        public BookViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }
}
