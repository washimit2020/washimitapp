package com.it.washmit.bookgoodmorning.model;

/**
 * Created by washm on 4/2/17.
 */

public class images {
    String path;

    public images(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
