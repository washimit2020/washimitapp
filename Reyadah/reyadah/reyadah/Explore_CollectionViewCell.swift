//
//  Explore_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher
class Explore_CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    var product:Product?{
        didSet{
            productImage.set_product_image(product_id: product!.id)
        }
    }
}
