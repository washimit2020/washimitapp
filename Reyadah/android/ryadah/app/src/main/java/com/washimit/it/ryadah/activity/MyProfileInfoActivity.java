package com.washimit.it.ryadah.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.UserInfoPageAdapter;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.Branche;
import com.washimit.it.ryadah.model.Following;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyProfileInfoActivity extends AppCompatActivity {
    @BindView(R.id.recycler_user_info)
    RecyclerView recycler_user_info;
    @BindView(R.id.no_branch)
    TextViewCast no_branch;
    UserInfoPageAdapter adapter;
    View view;
    ArrayList<Object> list;
    User user;
    SharedPref pref;
    Gson gson;
    Following following;
    ApiRequest request;
    String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile_info);
        ButterKnife.bind(this);
        gson = new Gson();
        pref = new SharedPref(this);
        request = new ApiRequest(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            user = gson.fromJson(bundle.getString("user"), User.class);
        }

        view = getLayoutInflater().inflate(R.layout.item_user_profile_info, null);


        list = new ArrayList<>();


        recycler_user_info.setHasFixedSize(true);


        recycler_user_info.setLayoutManager(new LinearLayoutManager(this));


        adapter = new UserInfoPageAdapter(list, this, recycler_user_info);


        adapter.setHeader(view);

        list.add(user);


        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                list.add(null);
                adapter.notifyDataSetChanged();
                getNext(user.getType().getId(), url);
            }
        });


        recycler_user_info.setAdapter(adapter);


        UpdatUi(user.getId());

    }


    public void UpdatUi(int userId) {


        switch (user.getType().getId()) {


            case Constant._TYPE_COSTUMER:
                break;

            case Constant._TYPE_STORE:

                if (pref.getUser().getId() == userId) {
                    list.add(getString(R.string.followers));
                    getNumberOfFollow();
                }

                break;


            case Constant._TYPE_SHOW:
                if (pref.getUser().getId() == userId) {
                    list.add(getString(R.string.branchs));
                    getBranchs();

                }
                break;


        }


    }

    private void getNumberOfFollow() {
        request.MultipartRequest(Request.Method.GET, false, "followed-by", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject msg;
                JSONObject users;
                JSONObject resultJ;

                resultJ = new JSONObject(result);

                msg = resultJ.getJSONObject("msg");
                users = msg.getJSONObject("users");


                list.addAll((Collection<? extends User>) gson.fromJson(users.getJSONArray("data").toString(), new TypeToken<ArrayList<User>>() {
                }.getType()));

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }


    private void getBranchs() {
        request.MultipartRequest(Request.Method.GET, false, "branch/" + user.getId(), null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject resultJ;
                JSONObject msg;
                JSONObject branches;
                resultJ = new JSONObject(result);
                msg = resultJ.getJSONObject("msg");
                branches = msg.getJSONObject("branches");

//                url = branches.getString("next_page_url");
//                adapter.setUrls(url);
                ArrayList<Branche> brancheslist = gson.fromJson(branches.getString("data"), new TypeToken<ArrayList<Branche>>() {
                }.getType());
                list.addAll(brancheslist);
                adapter.notifyDataSetChanged();

                if (brancheslist.size() == 0) {
                    no_branch.setVisibility(View.VISIBLE);
                } else {
                    no_branch.setVisibility(View.GONE);
                }

               // adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }

    private void getNext(int userType, String nexturl) {
        request.MultipartRequest(Request.Method.GET, true, nexturl, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size() - 1);


                JSONObject resultJ;
                JSONObject msg;
                JSONObject branches;

                resultJ = new JSONObject(result);
                msg = resultJ.getJSONObject("msg");
                branches = msg.getJSONObject("branches");

                url = branches.getString("next_page_url");
                adapter.setUrls(url);
                ArrayList<Branche> brancheslist = gson.fromJson(branches.getString("data"), new TypeToken<ArrayList<Branche>>() {
                }.getType());

                list.addAll(brancheslist);
                adapter.notifyDataSetChanged();

                adapter.setLoading();

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });
    }
}
