package com.it.washim.bakkar.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.it.washim.bakkar.R;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnDetailsCollBack;
import com.it.washim.bakkar.interfase.OnLoadMorCollBack;
import com.it.washim.bakkar.model.Posts;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 7/31/2017.
 */

public class BookAdapter extends RecyclerView.Adapter {
    ArrayList<Posts> list;
    Context context;
    OnDetailsCollBack collBack;
    boolean first = false;
    RecyclerView recyclerView;
    boolean loading = false;
    int add = 3;
    int ItemCount;
    int ItemPosition;
    OnLoadMorCollBack onLoadMorCollBack;
    String url;

    public BookAdapter(ArrayList<Posts> list, Context context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        this.recyclerView = recyclerView;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (!url.equalsIgnoreCase("null")) {
                        ItemCount = layoutManager.getItemCount();
                        ItemPosition = layoutManager.findLastVisibleItemPosition();
                        if (!loading && ItemCount <= (ItemPosition + add)) {

                            if (onLoadMorCollBack != null) {

                                onLoadMorCollBack.onLoad();

                            }
                            loading = true;

                        }
                    }
                }
            });

        }
//        if(recyclerView.getLayoutManager() .equals(LinearLayoutManager.HORIZONTAL) ){
//
//        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof BookViewHolder) {
            final Posts book = list.get(position);

            if (book.attachments.size() > 0) {
                if (book.attachments.get(0).attachment_type_id == Constant.ATTACHMENT_COVER) {
                    Picasso.with(context).load(Constant.URL + "attachments/" + book.attachments.get(0).id).placeholder(R.drawable.bakkar_icon).fit().into(((BookViewHolder) holder).book_image);
                } else {
                    ((BookViewHolder) holder).book_image.setImageDrawable(context.getResources().getDrawable(R.drawable.bakkar_icon));

                }
            } else {
                ((BookViewHolder) holder).book_image.setImageDrawable(context.getResources().getDrawable(R.drawable.bakkar_icon));

            }
            ((BookViewHolder) holder).book_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collBack.details(book, position);
                }
            });
            if (!first) {
                collBack.details(list.get(0), position);
                first = true;
            }

        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class BookViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_images)
        ImageView book_image;

        public BookViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public void OnDetailsLesnar(OnDetailsCollBack collBack) {
        this.collBack = collBack;
    }

    public void setFirst(Boolean first) {
        this.first = first;

    }

    public void addOnLoadMorLisenar(OnLoadMorCollBack onLoadMorCollBack) {
        this.onLoadMorCollBack = onLoadMorCollBack;
    }

    public void isLoading(Boolean loading) {
        this.loading = loading;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
