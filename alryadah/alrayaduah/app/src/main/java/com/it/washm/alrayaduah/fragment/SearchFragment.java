package com.it.washm.alrayaduah.fragment;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.adapter.NewsAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.news;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    String SearchTitle;
    Requests requests;
    RecyclerView mRecyclerView;
    NewsAdapter mAdapter;
    LinearLayoutManager mLayoutManager;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView noData;
    ArrayList<news> newsList;
    Handler handler;
    Runnable runnable;
    public String urls;
    HashMap<String, String> Stringparams;
CustamViewFont viewFont;

    public SearchFragment() {
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param Search
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String Search) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString("Search", Search);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SearchTitle = getArguments().getString("Search");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        container.clearDisappearingChildren();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        viewFont=new CustamViewFont(getActivity(),"Cairo-Regular.ttf");

        newsList = new ArrayList<>();

        handler = new Handler();

        Stringparams = new HashMap<String, String>();


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.search_refresh_layout);

        noData = (TextView) view.findViewById(R.id.noData);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.search_recycler_view);

        requests = new Requests(getContext(), getActivity());

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        // use a linear layout manager
        mRecyclerView.setLayoutManager(mLayoutManager);

        // create an Object for Adapter
        mAdapter = new NewsAdapter(getActivity(), newsList, mRecyclerView);
        mAdapter.setHasStableIds(true);

        // set the adapter object to the Recyclerview
        mRecyclerView.setAdapter(mAdapter);


        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //add null , so the adapter will check view_type and show progress bar at bottom

                runnable = new Runnable() {
                    public void run() {
                        newsList.add(null);
                        mAdapter.notifyItemInserted(newsList.size() - 1);
                    }
                };
                handler.post(runnable);
                getNextNews(urls);

            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        gitNews();

                                    }
                                }
        );


        return view;
    }
    public void setFont(){
        viewFont.setfont(noData);


    }
    public void gitNews() {
        Stringparams.put("search", SearchTitle);
        if (AppHelper.isOnline(getActivity())) {
            requests.StringRequest(Request.Method.POST, "search", Stringparams, "", new VolleyCallback() {


                @Override
                public void onSuccess(String result) {
                    newsList.clear();
                    JSONObject object = null;
                    JSONObject message = null;
                    JSONObject news = null;
                    JSONArray data = null;

                    try {
                        object = new JSONObject(result);
                        if (object.getBoolean("success")) {


                            message = object.getJSONObject("message");
                            news = message.getJSONObject("news");

                            urls = news.getString("next_page_url");
                            mAdapter.setUrl(news.getString("next_page_url"));

                            data = news.getJSONArray("data");

                            newsList.addAll(AppHelper.get_news_response(data));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if (newsList.size() > 0) {
                        mRecyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setLoaded();
                        mAdapter.notifyDataSetChanged();
                    } else {
                        noData.setText("لا يوجد نتائج");
                        noData.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }

                    swipeRefreshLayout.setRefreshing(false);

                }


                @Override
                public void onRequestError(VolleyError errorMessage) {
                    swipeRefreshLayout.setRefreshing(false);

                }
            });
        } else {
            noData.setText("لا يوجد اتصال بالشبكة");
            swipeRefreshLayout.setRefreshing(false);
            noData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    public void getNextNews(String url) {
        requests.StringRequest(Request.Method.POST, url, "", false, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {


                try {
                    newsList.remove(newsList.size() - 1);
                    mAdapter.notifyItemRemoved(newsList.size());

                } catch (Exception e) {

                }
                JSONObject object = null;
                JSONObject message = null;
                JSONObject news = null;
                JSONArray data = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {

                        message = object.getJSONObject("message");
                        news = message.getJSONObject("news");

                        urls = news.getString("next_page_url");
                        mAdapter.setUrl(news.getString("next_page_url"));

                        data = news.getJSONArray("data");


                        // newsList.addAll(AppHelper.get_news_response(data));

                        for (int i = 0; i < data.length(); i++) {

                            Gson gson = new Gson();
                            newsList.add(gson.fromJson(data.get(i).toString(), news.class));

                            mAdapter.notifyItemInserted(newsList.size());

                        }


                        mAdapter.setLoaded();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });

    }


    @Override
    public void onRefresh() {
        gitNews();
    }
}