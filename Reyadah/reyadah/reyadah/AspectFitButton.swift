//
//  AspectFitButton.swift
//  reyadah
//
//  Created by WASHM on 4/25/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class AspectFitButton: UIButton {


    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.imageView?.contentMode = .scaleAspectFit
    }
 

}
