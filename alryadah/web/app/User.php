<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name','nick_name','user_role_id','gender','reg_id', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'api_token','reg_id','created_at','updated_at','user_role_id'
    ];


    /**
     * One role for each user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role(){
        return $this->belongsTo('App\UserRole','user_role_id');
    }

    /**
     * many attachment to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachment(){
        return $this->hasMany('App\UserAttachment','user_id');
    }

    /**
     *  many news to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news(){
        return $this->hasMany('App\News','user_id');
    }


}
