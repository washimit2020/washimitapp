<?php

namespace App\Policies;

use App\Http\Helpers\Constants;
use App\User;
use App\News;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create news.
     *
     * @param  \App\User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can(Constants::WRITE_NEWS);
    }

    /**
     * Determine whether the user can update the news.
     *
     * @param  \App\User $user
     * @param  \App\News $news
     * @return mixed
     */
    public function update(User $user)
    {
        return ($user->can(Constants::ADMINISTRATOR));
    }

    /**
     * Determine whether the user can delete the news.
     *
     * @param  \App\User $user
     * @param  \App\News $news
     * @return mixed
     */
    public function delete(User $user)
    {
        return ($user->can(Constants::ADMINISTRATOR));
    }
}
