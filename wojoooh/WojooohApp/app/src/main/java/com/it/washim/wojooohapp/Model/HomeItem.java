package com.it.washim.wojooohapp.Model;

/**
 * Created by mohammad on 9/13/2017.
 */

public class HomeItem {
    public String name;
    public int type;
    public int action;

    public HomeItem(String name, int type, int action) {
        this.name = name;
        this.type = type;
        this.action = action;
    }
}
