package com.it.washim.wojooohapp.activitys;

import android.os.Bundle;
import android.os.CountDownTimer;

import com.it.washim.wojooohapp.R;

public class SplashScreenActivity extends BaseActivity {
    CountDownTimer timer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        timer = new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                openActivity(getApplicationContext(), HomeActivity.class);
                finish();
            }
        }.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();


    }

}
