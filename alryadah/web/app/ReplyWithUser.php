<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyWithUser extends Model
{
    //just to be extended

    public $incrementing = false;
    /**
     * @var array of primary arrays
     */
    protected $primaryKey = ["reply_id", "user_id"];

    /**
     * @var array of fillable arrays
     */
    protected $fillable = ["reply_id", "user_id"];

    /**
     * @var array of hidden arrays
     */
    protected $hidden = ["reply_id", "user_id"];


    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * belongs to one news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reply()
    {
        return $this->belongsTo('App\Reply', 'reply_id');
    }
}
