<?php

namespace App\Providers;

use App\Comment;
use App\Http\Helpers\Constants;
use App\News;
use App\Policies\CommentPolicy;
use App\Policies\NewsPolicy;
use App\Policies\RepliesPolicy;
use App\Reply;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Comment::class => CommentPolicy::class,
        Reply::class => RepliesPolicy::class,
        News::class => NewsPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define(Constants::ADMINISTRATOR, function ($user) {
            return $user->user_role_id == Constants::USER_ROLE_ADMIN;
        });


        Gate::define(Constants::NEWSMAN, function ($user) {
            return $user->user_role_id == Constants::USER_ROLE_NEWSMAN;
        });


        Gate::define(Constants::WRITE_NEWS, function ($user) {
            return ($user->can(Constants::NEWSMAN) || $user->can(Constants::ADMINISTRATOR));
        });
    }
}
