//
//  Utils.swift
//  al-haj
//
//  Created by WASHM on 7/23/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import SQLite
class DatabaseUtils {
    static let shared = DatabaseUtils()
    //tables
    var sections_t = Table("sections")
    var pages_t = Table("pages")
    var comments_t = Table("comments")
    var images_t = Table("images")
    var highlights_t = Table("highlights")
    //we shall add -- -- before any quran ayat
    
    
    //    private var r_db : Connection?{
    //        do {
    //            let path = Bundle.main.path(forResource: "al_haj", ofType: "sqlite3")!
    //            let db = try Connection(path)
    //            return db
    //        }catch(let exc){
    //            print(exc)
    //        }
    //        return nil
    //    }
    
    var db : Connection?{
        do {
            let connection = try Connection(getDBPath())
            return connection
        }catch(let exc){
            print(exc)
        }
        return nil
    }
    
    func getDBPath()->String
    {
        let fileManager = FileManager.default
        
        guard let doumentDirectoryPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first else{
            return ""
        }
        
        return doumentDirectoryPath.appendingPathComponent("al_haj.sqlite3").path;
    }
    
    func copyDb(){
        
        let fileManager = FileManager.default
        //          to stop the copying proccess you shall uncomment this when app in production
        //        let success = fileManger.fileExists(atPath: getDBPath())
        //        if success{
        //
        //            return
        //        }
        
        guard let doumentDirectoryPath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first  else{
            return
        }
        let destinationPath = doumentDirectoryPath.appendingPathComponent("al_haj.sqlite3")
        do{
            if !fileManager.fileExists(atPath: destinationPath.path) {
                print("DB does not exist in documents folder")
                if let dbFilePath = Bundle.main.path(forResource: "al_haj", ofType: "sqlite3") {
                    try fileManager.copyItem(atPath: dbFilePath, toPath: destinationPath.path)
                } else {
                    print("Uh oh - foo.db is not in the app bundle")
                }
            }
//            else {
//                try fileManager.removeItem(atPath: destinationPath.path)
//                copyDb()
//                print("Database file found at path: \(destinationPath.path) , delete the remove item line...")
//            }
            //            print("copyDb: delete the file remover")
            //
            //            try fileManger.removeItem(atPath: destinationPath)
            //            try fileManger.copyItem(atPath: sourcePath, toPath: destinationPath)
        }catch(let exp){
            print(exp)
        }
        
        
        
        //        do{
        //            try db!.run(pages_t.create(ifNotExists: true) { t in
        //                t.column(Expressions.id, primaryKey: .autoincrement) //     "id" INTEGER PRIMARY KEY NOT NULL,
        //                t.column(Expressions.title)  //
        //                t.column(Expressions.subTitle)
        //                t.column(Expressions.body)
        //                t.column(Expressions.references)
        //            })
        //            try db!.run(favorite_t.create(ifNotExists: true) { t in
        //                t.column(Expressions.page_id, primaryKey: true) //     "id" INTEGER PRIMARY KEY NOT NULL,
        //            })
        //
        //            try db!.run(comments_t.create(ifNotExists: true) { t in
        //                t.column(Expressions.page_id, primaryKey: true) //     "id" INTEGER PRIMARY KEY NOT NULL,
        //                t.column(Expressions.body) //     "id" INTEGER PRIMARY KEY NOT NULL,
        //            })
        //
        //            try db!.run(images_t.create(ifNotExists: true) { t in
        //                t.column(Expressions.id, primaryKey: .autoincrement) //     "id" INTEGER PRIMARY KEY NOT NULL,
        //                t.column(Expressions.page_id) //
        //                t.column(Expressions.title) //
        //            })
        //            try db!.run(highlights_t.create(ifNotExists: true) { t in
        //                t.column(Expressions.page_id ,  primaryKey : true) //
        //                t.column(Expressions.start) //
        //                t.column(Expressions.end) //
        //            })
        //
        //            let current_pages = try Array(r_db!.prepare(pages_t))
        //            let old_pages = try Array(db!.prepare(pages_t))
        //
        //            if current_pages.count  == old_pages.count{
        //                return
        //            }
        //
        //            try db!.run(pages_t.delete())
        //
        //            for page in current_pages{
        //                try db!.run(pages_t.insert(
        //                    Expressions.id <- page[Expressions.id],
        //                    Expressions.title <- page[Expressions.title],
        //                    Expressions.subTitle <- page[Expressions.subTitle],
        //                    Expressions.body <- page[Expressions.body],
        //                    Expressions.references <- page[Expressions.references]
        //                ))
        //            }
        //
        //        }catch(let exp){
        //            print(exp)
        //        }
        
    }
    
}

class Expressions {
    static let id = Expression<Int>("id")
    static let is_wishlisted = Expression<Int>("is_wishlisted")
    static let section_id = Expression<Int?>("section_id")
    static let page_id = Expression<Int>("page_id")
    static let title = Expression<String?>("title")
    static let imageTitle = Expression<String?>("image")
    static let subTitle = Expression<String?>("sub_title")
    static let body = Expression<String?>("body")
    static let comment = Expression<String?>("comment")
    static let references = Expression<String?>("references")
    static let start = Expression<Int>("start")
    static let end = Expression<Int>("end")
    static let color = Expression<Int>("color")
    static let is_full_line = Expression<Int>("is_full_line")
    static let is_read = Expression<Int>("is_read")
}


class Utils {
    static var shared = Utils()
    var light_mode_font = UIColor.black
    var light_mode_header = UIColor.tintColor
    var light_mode_sub = UIColor.tintColor
    var light_mode_background = UIColor.white
    
    var dark_mode_font = UIColor.white
    var dark_mode_header = UIColor.tintColor
    var dark_mode_sub = UIColor.tintColor
    var dark_mode_background = UIColor.black
    
    var pages:[Page]!
    var sections:[Section]!
    func setFont(newfontName:String , oldFont:UIFont?)  -> UIFont?{
        let fontNameToTest = oldFont?.fontName.lowercased() ?? "";
        var fontName = newfontName;
        if fontNameToTest.range(of: "bold") != nil {
            fontName += "-Bold";
        } else if fontNameToTest.range(of: "medium") != nil {
            fontName += "-Medium";
        } else if fontNameToTest.range(of: "light") != nil {
            fontName += "-Light";
        } else if fontNameToTest.range(of: "ultralight") != nil {
            fontName += "-UltraLight";
        }
        return UIFont(name: fontName, size: oldFont?.pointSize ?? 17 )
    }
    
    func go_to_index(tabBarController:MainTabbar_UIViewController, page:Int)  {
        
        tabBarController.vm.tabBar.selectedItem = tabBarController.vm.tabBar.items?[2]
        tabBarController.vm.selectedViewController = tabBarController.vm.viewControllers[1]
        
        if let book  = tabBarController.vm.viewControllers[1] as? Book_PageViewController{
            book.check_if_page_exist(page: page)
        }
        //        for (key,controller) in tabBarController.vm.viewControllers.enumerated(){
        //            if let book  = controller as? Book_PageViewController{
        //                book.check_if_page_exist(page: page)
        //                break
        //            }
        //        }
    }
    
    func go_to_section(tabBarController:MainTabbar_UIViewController, section:Int)  {
        
        tabBarController.vm.tabBar.selectedItem = tabBarController.vm.tabBar.items?[2]
        tabBarController.vm.selectedViewController = tabBarController.vm.viewControllers[1]
        
        guard let book  = tabBarController.vm.viewControllers[1] as? Book_PageViewController else{return}
        guard let page = Utils.shared.pages.first(where: { (page) -> Bool in
            return page.section_id == section
        }) else {return}
        book.check_if_page_exist(page: page.id ?? 1 )
        
        //        for (key,controller) in tabBarController.vm.viewControllers.enumerated(){
        //            if let book  = controller as? Book_PageViewController{
        //                book.check_if_page_exist(page: page)
        //                break
        //            }
        //        }
    }
    
    func make_no_data_view(container:UIView , condition:Bool)->UIView{
        let view  = UIView(frame: .init(origin: .zero, size: .init(width: container.frame.width, height: 60)))
        let label = UILabel(frame: .init(origin: .zero, size:  view.frame.size))
        label.textAlignment = .center
        label.text = "لا يوجد نتائج"
        label.isHidden = condition
        label.tag = 1
        view.addSubview(label)
        return view
    }
    
    func makeHeaderView(For controller:BasePageGetter_TableViewController, icon: UIImage , title:String){
        let header = UIView(frame: .init(origin: .zero, size: .init(width: controller.view.frame.width, height: 150)))
        let label = TitleLabel(frame: .init(x: controller.view.frame.width / 2 - 8, y: 32, width: controller.view.frame.width / 2 - 8, height: 45))
        label.textAlignment = .left
        let image = UIImageView(frame: .init(x: controller.view.frame.width / 2 - 56, y: 39.5, width: 30, height: 30))
        image.image = icon.withRenderingMode(.alwaysTemplate)
        image.tintColor = #colorLiteral(red: 0.8553363681, green: 0.8105258346, blue: 0.7671664357, alpha: 1)
        header.addSubview(label)
        header.addSubview(image)
        label.text = title
        controller.header = header
    }
    
    //user defaults
    
    func set_dark_mode(on:Bool){
        UserDefaults.standard.set(on, forKey: "dark_mode")
    }
    
    func is_dark_mode() -> Bool{
        return UserDefaults.standard.bool(forKey: "dark_mode")
    }
    
    func set_font_sizes(sizes:[Float])  {
        UserDefaults.standard.set(sizes, forKey: "font_size")
    }
    
    func get_font_sizes()->[Float]{
        return UserDefaults.standard.array(forKey: "font_size") as? [Float] ?? [17,15,14]
    }
    
    //    func set_font_name(font:String){
    //        UserDefaults.standard.set(font, forKey: "app_font")
    //    }
    
    func get_font_name () -> String{
        //        return UserDefaults.standard.string(forKey: "app_font") ?? "Lotus-Light"
        return  "Lotus-Light"
    }
    
    func save_time (seconds:Int){
        UserDefaults.standard.set(seconds, forKey: "last_logged_in")
        UserDefaults.standard.synchronize()
    }
    func get_time()->Int{
       return UserDefaults.standard.integer(forKey: "last_logged_in")
    }
    func get_last_page () -> Int{
        return UserDefaults.standard.integer(forKey: "last_page")
    }
    
    func save_page (page:Page?){
        guard let id = page?.id else {
            return
        }
        UserDefaults.standard.set(id, forKey: "last_page")
    }
}

enum UIStoryboardEnum : String{
    case main = "main"
    case settings = "settings"
    case search = "search"
    case book = "book"
    case cv = "cv"
}

extension UIStoryboard{
    func instantiateViewController(withEnum s_enum:UIStoryboardEnum)->UIViewController{
        return instantiateViewController(withIdentifier: s_enum.rawValue)
    }
}

