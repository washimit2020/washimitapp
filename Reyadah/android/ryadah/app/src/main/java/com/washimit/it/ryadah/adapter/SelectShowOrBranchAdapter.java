package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.washimit.it.ryadah.Interface.OnBranchCallback;
import com.washimit.it.ryadah.Interface.OnLoadMoreListener;
import com.washimit.it.ryadah.Interface.OnuserCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Branche;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectShowOrBranchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_PROG = 0;
    private final int USER = 1;
    public final int BRANCHE = 5;


    List<?> list;
    Activity context;
    private int lastVisibleItem, totalItemCount;
    String url;
    boolean isLoading, selectProduct;
    OnLoadMoreListener loadMoreListener;
    ApiRequest request;
    Gson gson;
    OnuserCallback callback;
    OnBranchCallback branchCallback;
    ArrayList<User> users;
    ArrayList<Branche> branches;
    User user1;
    Branche branche1;


    public SelectShowOrBranchAdapter(List<?> list, Activity context, RecyclerView recyclerView) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        users = new ArrayList<>();
        branches = new ArrayList<>();
        request = new ApiRequest(context);
        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            Log.d("ok", "ok");
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if (url != null) {

                        totalItemCount = linearLayoutManager.getItemCount();
                        lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();


                        if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {

                            if (loadMoreListener != null && !url.equalsIgnoreCase("null")) {

                                loadMoreListener.onLoadMore();

                            }
                            isLoading = true;

                        }


                    }

                }
            });


        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {

        if (viewHolder instanceof UserHolder) {

            final User user = (User) list.get(i);
            if (user.isSelected()) {
                ((UserHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_selecte));
            } else {
                ((UserHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));
            }

            ((UserHolder) viewHolder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!user.isSelected()) {
                        ((UserHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_selecte));
                    } else {
                        ((UserHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));

                    }
                    users.clear();
                    user.setSelected(!user.isSelected());
                    for (int i = 0; i < list.size(); i++) {
                        user1 = (User) list.get(i);
                        if (user1.isSelected()) {
                            users.add((User) list.get(i));
                        }
                    }
                    callback.users(users);
                }
            });
            ((UserHolder) viewHolder).item_user_select_show_or_branch_location.setText(user.getSector_name() + " - " + user.getStreet_name());
            ((UserHolder) viewHolder).item_user_select_show_or_branch_name.setText(user.getName());
            ((UserHolder) viewHolder).item_user_select_show_or_branch_rating.setRating((float) user.getRating());
            Picasso.with(context).load(Constant._AVATAR_URL + user.getId()).fit().centerCrop().placeholder(R.drawable.ic_profile_picture).into((((UserHolder) viewHolder).item_user_select_show_or_branch_img));

        } else if (viewHolder instanceof BrancheHolder) {

            final Branche branche = (Branche) list.get(i);

            ((BrancheHolder) viewHolder).item_user_select_show_or_branch_location.setText(branche.getSector_name() + " - " + branche.getStreet_name());

            ((BrancheHolder) viewHolder).item_user_select_show_or_branch_name.setText(branche.getName());

            Picasso.with(context).load(Constant._AVATAR_URL + branche.getUser_id()).fit().centerCrop().into(((BrancheHolder) viewHolder).item_user_select_show_or_branch_img);


            if (branche.isSelected()) {
                ((BrancheHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_selecte));
            } else {
                ((BrancheHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));
            }


            ((BrancheHolder) viewHolder).itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!branche.isSelected()) {
                        ((BrancheHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_selecte));
                    } else {
                        ((BrancheHolder) viewHolder).item_user_select_show_or_branch_switch.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_unselected));

                    }
                    branches.clear();

                    branche.setSelected(!branche.isSelected());

                    branches.clear();
                    for (int i = 0; i < list.size(); i++) {
                        branche1 = (Branche) list.get(i);
                        if (branche1.isSelected()) {
                            branches.add((Branche) list.get(i));
                        }
                    }
                    branchCallback.branch(branches);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == USER) {
            return new UserHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_select_show_or_branch, viewGroup, false));

        }
        if (i == VIEW_PROG) {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_load_more, viewGroup, false));

        }
        if (i == BRANCHE) {
            return new BrancheHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_select_show_or_branch, viewGroup, false));
        }


        return null;
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (list.get(position) instanceof User) {

            return USER;
        } else if (list.get(position) instanceof Branche) {
            return BRANCHE;
        } else if (list.get(position) == null) {

            return VIEW_PROG;
        }
        return position;
    }


    public class UserHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_user_select_show_or_branch_switch)
        ImageView item_user_select_show_or_branch_switch;
        @BindView(R.id.item_user_select_show_or_branch_img)
        ImageView item_user_select_show_or_branch_img;
        @BindView(R.id.item_user_select_show_or_branch_location)
        TextViewCast item_user_select_show_or_branch_location;
        @BindView(R.id.item_user_select_show_or_branch_name)
        TextView item_user_select_show_or_branch_name;
        @BindView(R.id.item_user_select_show_or_branch_rating)
        RatingBar item_user_select_show_or_branch_rating;


        public UserHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }

    public class BrancheHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.item_user_select_show_or_branch_switch)
        ImageView item_user_select_show_or_branch_switch;
        @BindView(R.id.item_user_select_show_or_branch_img)
        ImageView item_user_select_show_or_branch_img;
        @BindView(R.id.item_user_select_show_or_branch_location)
        TextViewCast item_user_select_show_or_branch_location;
        @BindView(R.id.item_user_select_show_or_branch_name)
        TextView item_user_select_show_or_branch_name;
        @BindView(R.id.item_user_select_show_or_branch_rating)
        RatingBar item_user_select_show_or_branch_rating;

        public BrancheHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            item_user_select_show_or_branch_rating.setVisibility(View.GONE);

        }
    }


    public void setUrl(@Nullable String url) {
        this.url = url;
    }

    public void setLoading() {
        isLoading = false;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }


    public void setOnUserCallback(OnuserCallback callback) {
        this.callback = callback;
    }

    public void setOnBranchCallback(OnBranchCallback branchCallback) {
        this.branchCallback = branchCallback;
    }
}