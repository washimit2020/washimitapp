package com.it.washim.wojooohapp.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.CustemView.MyEditText;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnBooksCollBack;
import com.it.washim.wojooohapp.Interfases.OnRequestPublishCollBack;
import com.it.washim.wojooohapp.Model.BookRequest;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.AddBookAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestBookFragment extends BaseFragment implements View.OnClickListener {


    public RequestBookFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.recycler_book)
    RecyclerView recyclerView;

    @BindView(R.id.add_book)
    ImageView add_book;

    @BindView(R.id.scroll)
    ScrollView scroll;

    @BindView(R.id.name)
    MyEditText name;

    @BindView(R.id.name_of_organization)
    MyEditText name_of_organization;

    @BindView(R.id.phone_number)
    MyEditText phone_number;

    @BindView(R.id.email_address)
    MyEditText email_address;

    @BindView(R.id.city)
    MyEditText city;


    @BindView(R.id.transfer_type)
    Spinner transfer_type;

    @BindView(R.id.country)
    Spinner country;

    @BindView(R.id.sent)
    MyButtonView sent;

    AddBookAdapter adapter;
    ArrayList<BookRequest> list;
    ArrayList<BookRequest> Booklist;
    LinearLayoutManager manager;
    HashMap<String, String> map;
    ArrayAdapter arrayAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_book, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        Booklist = new ArrayList<>();
        map = new HashMap<String, String>();
        adapter = new AddBookAdapter(getActivity(), list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(adapter);

        list.add(new BookRequest("", 0));
        adapter.notifyDataSetChanged();


        add_book.setOnClickListener(this);
        sent.setOnClickListener(this);
        arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, getCountries());
        country.setAdapter(arrayAdapter);

        adapter.setListOnBooklisenar(new OnBooksCollBack() {
            @Override
            public void onSend(ArrayList<BookRequest> l) {
                if (l.size() == 10) {
                    add_book.setVisibility(View.INVISIBLE);

                } else {
                    add_book.setVisibility(View.VISIBLE);

                }
                Booklist.clear();

                for (int i = 0; i < list.size(); i++) {

                    Booklist.add(l.get(i));

                }
            }
        });


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.add_book:

                list.add(new BookRequest("", 0));
                // adapter.notifyItemInserted(list.size()-1);
                adapter.notifyDataSetChanged();

                scroll.post(new Runnable() {
                    @Override
                    public void run() {
                        scroll.fullScroll(View.FOCUS_DOWN);
                        if (list.size() == 10) {
                            add_book.setVisibility(View.INVISIBLE);

                        } else {
                            add_book.setVisibility(View.VISIBLE);

                        }
                    }
                });

                break;


            case R.id.sent:
                if (validation()) {

                    showProgress("الرجاء الانتظار");
                    map.put("name", name.getText().toString());
                    map.put("email", email_address.getText().toString());
                    map.put("phone_number", phone_number.getText().toString());
                    map.put("city", city.getText().toString());
                    map.put("country", country.getSelectedItem().toString());
                    map.put("transfer_type", transfer_type.getSelectedItemPosition() + "");
                    map.put("company_name", name_of_organization.getText().toString());

                    for (int i = 0; i < list.size(); i++) {
                        map.put("order[" + i + "][title]", Booklist.get(i).bookTitle);
                        map.put("order[" + i + "][number]", Booklist.get(i).bookNumber + "");

                        Log.d("asfasfas", Booklist.get(i).bookTitle);
                    }

                    getRequest().requestString(Request.Method.POST, "post/" + Constant.PAGES_BUY_REQUEST, map, new OnRequestPublishCollBack() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("sadfas", response);
                            name.setText("");
                            email_address.setText("");
                            phone_number.setText("");
                            name_of_organization.setText("");
                            name.requestFocus();
                            hideProgress();
                            Toast.makeText(getActivity(), "تم ارسال الكتاب سيتم التواصل معك في اقرب وقت ", Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("sadfas", "error error");
                            Toast.makeText(getActivity(), "يوجد مشكلة بالاتصال بالانترنت اعد المحاولة!", Toast.LENGTH_LONG).show();

                            hideProgress();
                        }
                    });


                }

                break;
        }
    }

    private boolean validation() {

        if (name.getText().toString().equalsIgnoreCase("")) {
            name.setError("قم بأدخال اسم صحيح");
            name.requestFocus();
            return false;

        }

        if (name_of_organization.getText().toString().equalsIgnoreCase("")) {
            name_of_organization.setError("قم بأدخال اسم صحيح");
            name_of_organization.requestFocus();
            return false;


        }
        if (phone_number.getText().toString().equalsIgnoreCase("") && phone_number.getText().toString().length() < 10) {
            phone_number.setError("قم بأدخال رقم صحيح");
            phone_number.requestFocus();
            return false;


        }
        if (email_address.getText().toString().equalsIgnoreCase("") || !Patterns.EMAIL_ADDRESS.matcher(email_address.getText().toString()).matches()) {
            email_address.setError("قم بأدخال بريد الإلكتروني صحيح");
            email_address.requestFocus();
            return false;


        }
        if (city.getText().toString().equalsIgnoreCase("")) {
            city.setError("قم بأدخال المدينة");
            city.requestFocus();
            return false;
        }


        return true;
    }


}
