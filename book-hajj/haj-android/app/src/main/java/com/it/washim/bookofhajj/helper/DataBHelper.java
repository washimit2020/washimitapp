package com.it.washim.bookofhajj.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.it.washim.bookofhajj.model.comment;
import com.it.washim.bookofhajj.model.highlight;
import com.it.washim.bookofhajj.model.pages;
import com.it.washim.bookofhajj.model.searchResult;
import com.it.washim.bookofhajj.model.wishiListed;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NgocTri on 11/7/2015.
 */
public class DataBHelper extends SQLiteOpenHelper {
    public static final String DBNAME = "al_haj.sqlite3";
    public static final String DBLOCATION = "/data/data/com.it.washim.bookofhajj/databases/";
    private Context mContext;
    private SQLiteDatabase mDatabase;

    public DataBHelper(Context context) {
        super(context, DBNAME, null, 1);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void openDatabase() {
        String dbPath = mContext.getDatabasePath(DBNAME).getPath();
        if (mDatabase != null && mDatabase.isOpen()) {
            return;
        }
        mDatabase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    public void closeDatabase() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, "highlight");
        return numRows;
    }

    //get commaend
    public List<pages> getListpagestList() {
        pages pages = null;
        List<pages> pagestList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.id,pages.title,body,is_wishlisted,comment,`references`,is_full_line,is_read,images.title FROM pages LEFT JOIN images ON pages.id=images.page_id ORDER BY pages.id DESC", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getInt(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getString(8));
            pagestList.add(pages);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return pagestList;
    }

    public List<highlight> getListhighlightList(int id) {
        highlight highlight = null;
        List<highlight> highlightList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT highlights.start,highlights.end,highlights.color FROM highlights  WHERE highlights.page_id=" + id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            highlight = new highlight(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));
            highlightList.add(highlight);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return highlightList;
    }

    public List<wishiListed> getListWishilsted() {
        wishiListed listeds = null;
        List<wishiListed> wishiListeds = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.id,pages.title FROM pages WHERE is_wishlisted = 1", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listeds = new wishiListed(cursor.getInt(0), cursor.getString(1));
//            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3));
            wishiListeds.add(listeds);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return wishiListeds;
    }

    public List<comment> getListComment() {
        comment listeds = null;
        List<comment> commentList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.id,pages.comment,pages.title FROM pages WHERE pages.comment  NOT NULL", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listeds = new comment(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
//            pages = new pages(cursor.getInt(0), cursor.getString(1), cursor.getInt(2), cursor.getString(3));
            commentList.add(listeds);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return commentList;
    }

    public int getSectionId(int id) {
        int section;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT id FROM pages WHERE section_id = " + id + " LIMIT 1", null);
        cursor.moveToFirst();

        section = cursor.getInt(0);
        cursor.close();
        closeDatabase();
        return section;
    }


    public List<wishiListed> getListSection() {
        wishiListed listeds = null;
        List<wishiListed> sectiontList = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT sections.id,sections.title FROM sections", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            listeds = new wishiListed(cursor.getInt(0), cursor.getString(1));
            sectiontList.add(listeds);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return sectiontList;
    }


    public List<searchResult> search(String s) {
        searchResult pages = null;
        List<searchResult> searchResults = new ArrayList<>();
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.id,pages.title,pages.body FROM pages WHERE pages.body LIKE '%" + s + "%' ", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            pages = new searchResult(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
            searchResults.add(pages);
            cursor.moveToNext();
        }
        cursor.close();
        closeDatabase();
        return searchResults;
    }

    public int isWishilisted(int id) {
        int isWishilisted = 0;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT pages.is_wishlisted FROM pages WHERE pages.id = " + id + " LIMIT 1", null);
        cursor.moveToFirst();
        isWishilisted = cursor.getInt(0);
        cursor.close();
        closeDatabase();
        return isWishilisted;
    }

    public int numberRead() {
        int numberRead = 0;
        openDatabase();
        Cursor cursor = mDatabase.rawQuery("SELECT COUNT (*) FROM  pages WHERE pages.is_read = 1", null);
        cursor.moveToFirst();
        numberRead = cursor.getInt(0);
        cursor.close();
        closeDatabase();
        return numberRead;
    }

    public boolean insertRead(int id) {
        openDatabase();
        String strSQL = "UPDATE pages SET IS_READ = 1 where ID = " + id;
        mDatabase.execSQL(strSQL);
        closeDatabase();

        return true;
    }

    //insert commaned
    public boolean insertHighlight(int start, int end, int page_id, int color_id) {
        deleteHighlight(start, end, page_id);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("start", start);
        contentValues.put("end", end);
        contentValues.put("page_id", page_id);
        contentValues.put("color", color_id);
        db.insert("highlights", null, contentValues);
        return true;
    }

    public boolean insertWishilsted(int id) {
        openDatabase();
        String strSQL = "UPDATE pages SET is_wishlisted = ((select count(*) from pages where id = " + id + " AND is_wishlisted = 0 LIMIT 1)>0) where id = " + id;
        mDatabase.execSQL(strSQL);
        closeDatabase();
        return true;
    }

    public boolean insertComment(int id, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("comment", comment);
        db.update("pages", args, "id" + "=" + id, null);
        return true;
    }

    public void deleteHighlight(int start, int end, int page_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("highlights", "page_id=? and start>=? and end<=?", new String[]{String.valueOf(page_id), String.valueOf(start), String.valueOf(end)});
    }
}
