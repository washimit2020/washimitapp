package com.it.washim.wojooohapp.Model;

/**
 * Created by mohammad on 9/19/2017.
 */

public class Categorie {
    public int id;
    public String title;
    public String created_at;
    public String updated_at;

    @Override
    public String toString() {
        return title;
    }
}
