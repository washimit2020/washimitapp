//
//  Blocked_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 5/9/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Blocked_TableViewController: ItemsLoader_TableViewController {
    let reuseId = "cell"
    override func viewDidLoad() {
        super.viewDidLoad()
        link = RequestLinks.block.rawValue
        beginWithModel = "blocked"
        clearData()
        tableView.register(UINib(nibName: "Blocked_TableViewCell", bundle: nil), forCellReuseIdentifier: reuseId)
        makeDetailsTitle(self, title: "قائمة الحظر", image: nil)
        tableView.separatorColor = Constants.themeColor
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let user = User(items[indexPath.row])
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId) as! Blocked_TableViewCell
        cell.user = user
        cell.unblock.addTarget(nil, action: #selector(deleteRow(_:)), for: .touchUpInside)
        return cell
    }
    
    //    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //        return true
    //    }
    
    //    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    //        let remove = UITableViewRowAction(style: .destructive, title: "الغاء الحظر", handler: {
    //            (rowAction: UITableViewRowAction, indexPath: IndexPath) -> Void in
    //            self.items.remove(at: indexPath.row)
    //            tableView.beginUpdates()
    //            tableView.deleteRows(at: [indexPath], with: .automatic)
    //            tableView.endUpdates()
    //        })
    //
    //        return [remove]
    //    }
    
    
    func deleteRow(_ sender:UIButton){
        let point = sender.convert(CGPoint.zero, to: tableView)
        let indexPath = tableView.indexPathForRow(at: point)!
        let user:User = User (items[indexPath.row])
        self.apiRequest.make_request(parameters: ["user_id":"\(user.id!)"], apiImages: nil, link: RequestLinks.block.rawValue, method: .post, completion: nil)
        
        self.items.remove(at: indexPath.row)
        tableView.beginUpdates()
        tableView.deleteRows(at: [indexPath], with: .left)
        tableView.endUpdates()
        if self.items.count == 0 {
            emptyLabel?.isHidden = false
        }
        
    }
    
}


