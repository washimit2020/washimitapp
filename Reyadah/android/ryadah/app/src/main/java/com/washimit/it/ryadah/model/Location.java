package com.washimit.it.ryadah.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by mohammad on 5/6/2017.
 */

public class Location {

    LatLng latLng;
    int type;

    public Location(LatLng latLng, int type) {
        this.latLng = latLng;
        this.type = type;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
