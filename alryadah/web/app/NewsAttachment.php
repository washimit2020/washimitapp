<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsAttachment extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'news_id', 'path', 'mime_type', 'news_attachment_type_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * belongs to one news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function news()
    {
        return $this->belongsTo('App\News', 'news_id');
    }

    /**
     * belongs to one attachment type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function type()
    {
        return $this->belongsTo('App\NewsAttachmentType', 'news_attachment_type_id');
    }
}
