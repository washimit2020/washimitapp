package com.washimit.it.ryadah.model;

import java.io.File;

/**
 * Created by mohammad on 5/4/2017.
 */

public class CaverEdit {
    File file;
    int id;

    public CaverEdit(File file, int id) {
        this.file = file;
        this.id = id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
