package com.it.washim.bakkar.fcm;

/**
 * Created by mohammad on 8/22/2017.
 */


import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.it.washim.bakkar.halper.ApiRequest;
import com.it.washim.bakkar.interfase.OnRequestCollBack;

import org.json.JSONException;

import java.util.HashMap;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */

    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        HashMap map = new HashMap<String, String>();
        map.put("reg_id", token + "");
        ApiRequest.getInstance(this).stringRequest(Request.Method.POST, this, "reg_id/update", map, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) throws JSONException {

                Log.d(TAG, "update: " + response);
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "update: " + " Error");

            }
        });
    }
}