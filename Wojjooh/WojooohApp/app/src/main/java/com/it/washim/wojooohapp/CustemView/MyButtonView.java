package com.it.washim.wojooohapp.CustemView;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by mohammad on 9/11/2017.
 */

public class MyButtonView extends Button {
    public MyButtonView(Context context) {
        super(context);
    }

    public MyButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyButtonView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
