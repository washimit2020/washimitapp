<?php

namespace App\Http\Controllers;

use App\Comment;
use App\CommentLike;
use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\Comments\Like;
use App\Http\Requests\Comments\Store;
use App\News;
use App\Reply;
use App\ReplyLike;
use App\User;
use Illuminate\Http\Request;

class CommentsController extends BaseController
{
    private $counts = ['likes', 'replies'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = get_user_obj();

        $comments = Comment::with(["user" => $user])
            ->withCount($this->counts)
            ->where('news_id', $id)->latest()
            ->paginate(10);

        foreach ($comments as $comment) {

            $data ["comment_id"] = $comment->id;

            $comment->is_liked = check_status($data, new CommentLike());

            $comment->replies = Reply::where('comment_id', $comment->id)->with(['user' => get_user_obj()])->latest()->take(2)->get();
        }

        return response()->json(ResponseHelper::successResponse(["comments" => $comments]));

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     *
     **/
    public function store(Store $request)
    {
        $request["user_id"] = $request->user()->id;
        $comment = Comment::create($request->all());
        return response()->json(ResponseHelper::successResponse(["comment" => $comment]));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('delete', $comment);

        $comment->delete();
        return response()->json(ResponseHelper::successResponse(["deleted" => true]));
    }
}
