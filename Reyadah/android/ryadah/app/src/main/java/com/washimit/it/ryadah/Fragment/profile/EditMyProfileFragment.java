package com.washimit.it.ryadah.Fragment.profile;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.washimit.it.ryadah.Fragment.MangaFragment;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.activity.MapsActivity;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.CaverEdit;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.registrationActivity.ImageAdd;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.koushikdutta.async.http.body.FilePart;
import com.koushikdutta.async.http.body.Part;
import com.koushikdutta.async.http.body.StringPart;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class EditMyProfileFragment extends Fragment implements View.OnClickListener {

    @BindView(R.id.fragment_edit_my_profile_text_name)
    TextViewCast text_name;
    @BindView(R.id.fragment_edit_my_profile_text_des)
    TextViewCast text_des;
    @BindView(R.id.fragment_edit_my_profile_social_media_show)
    TextViewCast social_media_show;
    @BindView(R.id.fragment_edit_my_profile_place_des)
    LinearLayout place_des;
    @BindView(R.id.fragment_edit_my_profile_location_des)
    LinearLayout location_des;
    @BindView(R.id.password_linear)
    LinearLayout password_linear;
    @BindView(R.id.password_confirmation_linear)
    LinearLayout password_confirmation_linear;
    @BindView(R.id.fragment_edit_my_profile_social_media)
    LinearLayout social_media;
    @BindView(R.id.fragment_edit_my_profile_add_img)
    TextViewCast add_img;
    @BindView(R.id.fragment_edit_my_profile_name)
    EditTextView name;
    @BindView(R.id.fragment_edit_my_profile_des)
    EditTextView des;
    @BindView(R.id.fragment_edit_my_profile_street_name)
    EditTextView street_name;
    @BindView(R.id.fragment_edit_my_profile_sector)
    EditTextView sector;
    @BindView(R.id.fragment_edit_my_profile_password)
    EditTextView password;
    @BindView(R.id.fragment_edit_my_profile_password_confirmation)
    EditTextView password_confirmation;
    @BindView(R.id.fragment_edit_my_profile_edit)
    ImageView edit;
    @BindView(R.id.fragment_edit_my_profile_edit_location)
    ButtonView edit_location;

    @BindView(R.id.fragment_edit_my_profile_facebook)
    ImageView facebook;
    @BindView(R.id.fragment_edit_my_profile_instgram)
    ImageView instgram;
    @BindView(R.id.fragment_edit_my_profile_snapchat)
    ImageView snapchat;
    @BindView(R.id.fragment_edit_my_profile_twitter)
    ImageView twitter;
    @BindView(R.id.fragment_edit_my_profile_whatsapp)
    ImageView whatsapp;


    User user;
    SharedPref pref;

    ArrayList<Part> parts;
    ArrayList<CaverEdit> cavers;
    File avatar;
    LatLng lng;
    int userType;
    private Unbinder unbinder;
    ApiRequest request;
    String facebook_ = "", instagram_ = "", twitter_ = "", snapchat_ = "", whatsapp_ = "";


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant._TYPE_MAP) {

            if (data != null) {
                lng = (LatLng) data.getExtras().get("latLng");
                Log.d("LatLnge", lng.latitude + " " + lng.longitude);
                parts.add(new StringPart("lat", lng.latitude + ""));
                parts.add(new StringPart("lng", lng.longitude + ""));
            } else {

            }


        } else if (requestCode == Constant._TYPE_PICTURE) {
            cavers.clear();
            if (data != null) {
                if (data.getExtras().get("avatar") != null) {
                    avatar = new File(data.getExtras().getString("avatar"));
                    Log.d("avatare", data.getExtras().get("avatar") + "");
                }
                if (data.getExtras().get("caver1") != null) {
                    cavers.add(new CaverEdit(new File(data.getExtras().getString("caver1")), 0));
                    Log.d("caver1e", data.getExtras().get("caver1") + "");

                }
                if (data.getExtras().get("caver2") != null) {
                    cavers.add(new CaverEdit(new File(data.getExtras().getString("caver2")), 1));

                    Log.d("caver2e", data.getExtras().get("caver2") + "");

                }
                if (data.getExtras().get("caver3") != null) {
                    //    files.add(new File(data.getExtras().getString("caver3")));
                    cavers.add(new CaverEdit(new File(data.getExtras().getString("caver3")), 2));

                    Log.d("caver3e", data.getExtras().get("caver3") + "");


                }


            }


        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_my_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        pref = new SharedPref(getActivity());
        user = pref.getUser();
        request = new ApiRequest(getActivity());
        userType = user.getType().getId();
        updateUi(userType);
        edit.setOnClickListener(this);
        edit_location.setOnClickListener(this);

        parts = new ArrayList<>();
        cavers = new ArrayList<>();

        if (!pref.getLogInTocen().equalsIgnoreCase("")) {
            password_linear.setVisibility(View.GONE);
            password_confirmation_linear.setVisibility(View.GONE);
        }


        add_img.setOnClickListener(this);
        facebook.setOnClickListener(this);
        instgram.setOnClickListener(this);
        snapchat.setOnClickListener(this);
        twitter.setOnClickListener(this);
        whatsapp.setOnClickListener(this);
        return view;
    }

    public void updateUi(int userType) {

        name.setText(user.getName());
        des.setText(user.getInfo());

        if (userType == 3) {

            text_name.setText(R.string.name);
            text_des.setText(R.string.desc);
            social_media_show.setVisibility(View.GONE);
            social_media.setVisibility(View.GONE);
            location_des.setVisibility(View.GONE);
            add_img.setVisibility(View.GONE);
        } else {
            sector.setText(user.getSector_name());
            street_name.setText(user.getStreet_name());
            text_name.setText(R.string.store_name);
            text_des.setText(R.string.bio_store);
            social_media_show.setText(R.string.social_media_store);
        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_edit_my_profile_edit:
                if (validation()) {
                    parts.add(new StringPart("login_type", String.valueOf(Constant.NORMAL_REGISTER)));
                    parts.add(new StringPart("name", name.getText().toString()));
                    parts.add(new StringPart("reg_id", "lk.jkgfjhhoilgghjhjjk"));
                    parts.add(new StringPart("street_name", street_name.getText().toString()));
                    parts.add(new StringPart("sector_name", sector.getText().toString()));


                    if (pref.getLogInTocen().equalsIgnoreCase("")) {
                        parts.add(new StringPart("password", password.getText().toString()));
                        parts.add(new StringPart("password_confirmation", password_confirmation.getText().toString()));
                    } else {
                        parts.add(new StringPart("token", pref.getLogInTocen()));

                    }
                    parts.add(new StringPart("info", des.getText().toString()));

                    if (!facebook_.equalsIgnoreCase("")) {
                        parts.add(new StringPart("facebook", facebook_));
                    }
                    if (!twitter_.equalsIgnoreCase("")) {

                        parts.add(new StringPart("twitter", twitter_));
                    }
                    if (!instagram_.equalsIgnoreCase("")) {

                        parts.add(new StringPart("instagram", instagram_));
                    }
                    if (!snapchat_.equalsIgnoreCase("")) {

                        parts.add(new StringPart("snapchat", snapchat_));
                    }
                    if (!whatsapp_.equalsIgnoreCase("")) {

                        parts.add(new StringPart("whatsapp", whatsapp_));
                    }

                    if (avatar != null) {
                        parts.add(new FilePart("avatar", avatar));
                    }
                    for (int i = 0; i < cavers.size(); i++) {

                        parts.add(new FilePart("cover[" + cavers.get(i).getId() + "]", cavers.get(i).getFile()));
                    }
                    request.MultipartRequest("POST", "update/profile", getString(R.string.register_edit), parts, new MultipartCallback() {
                        @Override
                        public void onSuccess(String result_) {
                            JSONObject result = null;
                            JSONObject msg = null;

                            try {
                                result = new JSONObject(result_);
                                msg = result.getJSONObject("msg");
                                Gson gson = new Gson();
                                User user = gson.fromJson(msg.getString("user"), User.class);
                                user.setToken(msg.getString("token"));

                                pref.setUser(gson.toJson(user));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("fsdgsdfgsg", result_);
                     //       openActivity(MainActivity.class, userType, 3);
                            ((MainActivity)getActivity()).openFragment(MangaFragment.newInstants(Constant._PAGE_HOME));

                        }
                        @Override
                        public void onRequestError(String errorMessage) {
                            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                        }
                    });
                }
                break;
            case R.id.fragment_edit_my_profile_edit_location:


                openActivity(MapsActivity.class, 90, Constant._TYPE_MAP);


                break;

            case R.id.fragment_edit_my_profile_add_img:


                openActivity(ImageAdd.class, userType, Constant._TYPE_PICTURE);


                break;

            case R.id.fragment_edit_my_profile_facebook:


                openDialog(0);


                break;

            case R.id.fragment_edit_my_profile_twitter:


                openDialog(1);


                break;

            case R.id.fragment_edit_my_profile_instgram:


                openDialog(2);


                break;

            case R.id.fragment_edit_my_profile_snapchat:


                openDialog(3);


                break;

            case R.id.fragment_edit_my_profile_whatsapp:


                openDialog(4);


                break;

        }
    }


    public void openActivity(Class activity, int userType, int requstCode) {

        Intent intent = new Intent(getActivity(), activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (userType != 90) {
            intent.putExtra("userType", userType);
            intent.putExtra("requestType", Constant._TYPE_SIGN_UP);
        }
        startActivityForResult(intent, requstCode);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public boolean validation() {


        if (name.getText().toString().equalsIgnoreCase("")) {
            name.setError(getResources().getString(R.string.fill_name));
            name.requestFocus();
            return false;
        }
        if (des.getText().toString().equalsIgnoreCase("")) {
            des.setError(getResources().getString(R.string.fill_desc));
            des.requestFocus();

            return false;
        }

        if (sector.getText().toString().equalsIgnoreCase("")) {
            sector.setError(getResources().getString(R.string.fill_street));
            sector.requestFocus();

            return false;
        }
        if (street_name.getText().toString().equalsIgnoreCase("")) {
            street_name.setError(getResources().getString(R.string.fill_street));
            street_name.requestFocus();
            return false;
        }
        if (pref.getLogInTocen().equalsIgnoreCase("")) {


            if (!(password.getText().toString().length() >= 5)) {
                password.setError(getResources().getString(R.string.invalid_password));
                password.requestFocus();

                return false;
            }
        }
        return true;
    }


    private void openDialog(final int x) {
        AapHalper.CreateDialog(R.layout.dialog_social_media, getActivity(), new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                final EditTextView text = (EditTextView) dialog.findViewById(R.id.dialog_social_media_text);
                ButtonView ok = (ButtonView) dialog.findViewById(R.id.dialog_social_media_ok);
                ButtonView cancel = (ButtonView) dialog.findViewById(R.id.dialog_social_media_cancel);


                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        switch (x) {

                            case 0:
                                facebook_ = text.getText().toString();
                                break;
                            case 1:
                                twitter_ = text.getText().toString();
                                break;
                            case 2:
                                instagram_ = text.getText().toString();
                                break;
                            case 3:
                                snapchat_ = text.getText().toString();
                                break;

                            case 4:
                                whatsapp_ = text.getText().toString();
                                break;
                        }

                        dialog.dismiss();

                    }
                });
                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });

            }
        });


    }


}
