package com.washimit.it.ryadah.model;

/**
 * Created by mohammad on 5/21/2017.
 */

public class SocialMedia {
    String name;
    String email;
    String phone_number;
    String info;
    String sector;
    String street_name;
    String token;
    String type_id;

    public SocialMedia(String name, String email, String phone_number, String info, String sector, String street_name, String token, String type_id) {
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.info = info;
        this.sector = sector;
        this.street_name = street_name;
        this.token = token;
        this.type_id = type_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }
}
