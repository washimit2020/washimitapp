<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsSubType extends Type
{
    /**
     * @var array
     */
    protected $fillable = [
        'id' ,'name_ar','name_en','news_main_type_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function main(){
        return $this->belongsTo('App\NewsMainType','news_main_type_id');
    }
}
