//
//  HomeProductsCVC.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Social

class HomeProductsCVC: ItemsLoader_CollectionViewController {
   
    var documentInteractionController:UIDocumentInteractionController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.link = RequestLinks.product.rawValue
        self.beginWithModel = "products"
        always_hidden = true
    }
  
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let product = items[indexPath.row] as? Product
        var id = "product"
        
        if product!.is_sponsored{
          id = "sponserAd"
        }
        let product_cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath) as! HomeProducts_CollectionViewCell
        
        product_cell.product = product
        product_cell.apiRequest = self.apiRequest
        if !product!.is_sponsored{
            product_cell.comment.addTarget(self, action: #selector(go_to_comments(_:)), for: .touchUpInside)
            product_cell.share.addTarget(self, action: #selector(share_product(_:)), for: .touchUpInside)
            product_cell.location.addTarget(self, action: #selector(go_to_locations(_:)), for: .touchUpInside)
        }
        make_materialCell(product_cell)
        
        return product_cell
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        //2
        case UICollectionElementKindSectionHeader:
            //3
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "emptyProducts",
                                                                            for: indexPath)
            
            (headerView.viewWithTag(1) as! UIButton).isEnabled = Gate.is_authed()
            return headerView
        default:
            //4
            
            return super.collectionView(collectionView, viewForSupplementaryElementOfKind: kind, at: indexPath)
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let details = Details_CollectionViewController(nibName: "Details_CollectionViewController", bundle: nil)
        details.model = items[indexPath.row] as? Product
        details.apiRequest = self.apiRequest
        self.navigationController?.pushViewController(details, animated: true)
        
        super.collectionView(collectionView, didSelectItemAt: indexPath)
    }
    
    
    func go_to_comments(_ sender:AspectFitButton){
        get_collection_cell(sender)
        let comment = Comment_ViewController(nibName: "Comment_ViewController", bundle: nil)
        comment.model = items[selectedIndexPath!.row] as? Product
        comment.apiRequest = apiRequest
        self.navigationController?.pushViewController(comment, animated: true)
    }
    
    func share_product(_ sender:AspectFitButton){
        let cell  = get_collection_cell(sender)
        let img = cell.productImage.image
        let name = cell.product!.name!
        let activityViewController = shareProductToMedia(image: img!, name: name)
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func go_to_locations(_ sender:AspectFitButton){
       
        let cell = get_collection_cell(sender)
        
        perform_locations_segue(ProductOrUser: cell.product!, ViewController: self, apiRequest)

    }
    
    func get_collection_cell(_ sender:AspectFitButton)->HomeProducts_CollectionViewCell{
        let point = sender.convert(CGPoint.zero, to: collectionView)
        selectedIndexPath = collectionView?.indexPathForItem(at: point)
        let cell = collectionView?.cellForItem(at: selectedIndexPath!) as! HomeProducts_CollectionViewCell
        return cell
    }
}


extension HomeProductsCVC:ItemsLoaderDelegate{
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var height:CGFloat = 530
        let product = items[indexPath.row] as! Product
        if product.is_sponsored{
            height = 440
        }
        
        return CGSize(width: collectionView.bounds.size.width - 16, height: height)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,layout collectionViewLayout: UICollectionViewLayout,referenceSizeForHeaderInSection section: Int) -> CGSize{
        
        if !Gate.is_authed() || (collectionView.numberOfItems(inSection: section) == 0 && didCall){
            return self.view.bounds.size
        }
        return CGSize.zero
    }
    
    func dataFiller(data: [JSON]) {
        for productData in data{
            self.items.append(Product(productData))
        }
    }
}

