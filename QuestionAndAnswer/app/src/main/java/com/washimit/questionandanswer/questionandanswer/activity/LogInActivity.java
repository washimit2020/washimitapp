package com.washimit.questionandanswer.questionandanswer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.custumView.MyEditText;
import com.washimit.questionandanswer.questionandanswer.custumView.MyTextView;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LogInActivity extends BaseActivity implements View.OnClickListener {
    @BindView(R.id.email_log_in)
    MyEditText email_log_in;
    @BindView(R.id.log_in_password)
    MyEditText log_in_password;
    @BindView(R.id.log_in)
    MyButtonView log_in;
    @BindView(R.id.new_acount)
    MyTextView new_acount;
    ApiRequest request;
    PreferenceClass pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        request = new ApiRequest(this);
        pref = new PreferenceClass(this);
        log_in.setOnClickListener(this);
        new_acount.setOnClickListener(this);



        if (pref.getUser() != null) {
            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {


            case R.id.log_in:

                if (validation()) {

                    HashMap<String, String> map = new HashMap<>();

                    map.put("email", email_log_in.getText().toString());
                    map.put("password", log_in_password.getText().toString());

                    showDialog();

                    request.setRequst("login", map, new RequestCallback() {
                        @Override
                        public void onResponse(String response) throws JSONException {

                            Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            Log.d("response", response);


                            JSONObject object = null;
                            object = new JSONObject(response);
                            //JSONObject msg = object.getJSONObject("msg");
                            pref.setUser(object.getString("user"));
                            hideDialog();

                        }

                        @Override
                        public void ErrorListener() {
                            Log.d("ErrorListener", "ErrorListener");

                            hideDialog();
                            Toast.makeText(getApplicationContext(),"توجد مشكلة بالاتصال او ان الايميل او كلمة السر غير صحيح",Toast.LENGTH_SHORT).show();


                        }
                    });
                }
                break;


            case R.id.new_acount:

                Intent intent = new Intent(this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

                break;

        }
    }

    public boolean validation() {

        if (! email_log_in.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            email_log_in.setError("يجب ادخال ايميل صحيح");
            email_log_in.requestFocus();
            return false;
        }



        if (TextUtils.isEmpty(log_in_password.getText().toString())) {
            log_in_password.setError("يجب ادخال كلمة السر");
            log_in_password.requestFocus();
            return false;
        }

        return true;
    }

}
