//
//  Model.swift
//  al-haj
//
//  Created by WASHM on 7/24/17.
//  Copyright © 2017 WASHM. All rights reserved.
//
import SQLite

class Model {
    var id : Int?
    
    init(_ table_row:Row) {
        id = table_row[Expressions.id]
    }
}

class Page:Model {
    var is_wishlisted : Int!
    var title : String?
    var body : NSMutableAttributedString?
    var quran : NSMutableAttributedString?
    var references : String?
    var section_id : Int?
    var comment : String?
    var is_read : Int = 0
    var is_full_line : Int = 0
    var image:Image?
    private var text:String?
   
    override init(_ table_row: Row) {
        self.image = Image(table_row)
        super.init(table_row)
        title = table_row[DatabaseUtils.shared.pages_t[Expressions.title]]
        is_full_line = table_row[Expressions.is_full_line]
        is_read = table_row[Expressions.is_read]
        text = table_row[Expressions.body]
        inhanceBody()
        inhanceQuran()
        references = table_row[Expressions.references]
        is_wishlisted = table_row[Expressions.is_wishlisted]
        comment = table_row[Expressions.comment]
        section_id = table_row[Expressions.section_id]
    }
    func inhanceBody(){
        body = NSMutableAttributedString(string: text ?? "")
        let sizes = Utils.shared.get_font_sizes()
        let fontName = Utils.shared.get_font_name()
        let nightMode = Utils.shared.is_dark_mode()
        let fontColor = nightMode ? Utils.shared.dark_mode_font : Utils.shared.light_mode_font
        body!.addAttribute(NSAttributedStringKey.font, value: UIFont(name: fontName, size: CGFloat(sizes[2]))!, range: NSRange(location: 0, length: body!.length))
        body!.addAttributes([NSAttributedStringKey.foregroundColor : fontColor], range: NSRange(location: 0, length: body!.length))
        body!.changeToQuranFont()
        body!.changeSubTitle()
        body!.changeToRef()
        body!.changeToBold()
    }
    func inhanceQuran(){
        quran = NSMutableAttributedString(string: text ?? "")
        quran?.addAttributes([NSAttributedStringKey.font: UIFont(name: "Lotus-Light", size: 15)], range: .init(location: 0, length: quran?.length ?? 0))
        quran?.changeToQuranFont(size: 15)
        quran?.changeSubTitle(size:15)
        quran?.changeToRef(size:15)
        quran?.changeToBold(size:15)
    }
}

class Image:Model {
    var page_id: Int!
    var title : String!
      override init(_ table_row: Row) {
        super.init(table_row)
        title = table_row[DatabaseUtils.shared.images_t[Expressions.title]]
//        page_id = table_row[Expressions.page_id]
    }
    
    func get_uiimage() -> UIImage?{
        return UIImage(named:title)
    }
}



struct MenuItem{
    var image:UIImage?
    var title:String?
    init(image:UIImage? , title:String? ) {
        self.image  = image
        self.title = title
    }
}

class Section:Model {
    var title : String!
    override init(_ table_row: Row) {
        super.init(table_row)
        title = table_row[Expressions.title]
    }
}

