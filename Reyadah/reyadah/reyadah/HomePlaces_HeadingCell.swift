//
//  HomePlaces_CollectionReusableView.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import ImageSlideshow
import Kingfisher

import SlideMenuControllerSwift

class HomePlaces_HeadingCell: HomePlaces_CollectionViewCell {
    
    @IBOutlet weak var userCovers: ImageSlideshow!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        add_grey_border(layer: userCovers.layer)
        setup_cosmos(cosmos: ratingStar)
        
        setup_slider(slider: userCovers)
       
    }
    
   
    
   
    
   override func setupView(){
    super.setupView()
    
     let images = make_slider_images(id: user!.id,placeholder: "defaultImage")
        
     userCovers.setImageInputs(images as! [InputSource])
    }
    
    
}



