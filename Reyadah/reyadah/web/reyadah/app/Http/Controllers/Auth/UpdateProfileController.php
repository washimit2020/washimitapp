<?php

namespace App\Http\Controllers\Auth;

use App\Attachment;
use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profile\UpdateAvatar;
use App\Http\Requests\Profile\UpdateCover;
use App\Http\Requests\Profile\UpdateProfileRequest;
use App\Http\Requests\Profile\UpdateRegID;
use App\SocialMediaToken;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UpdateProfileController extends Controller
{

    /**
     * @var string array
     */
    private $attributes = [
        'name', 'street_name', 'sector_name', 'lat', 'lng', 'info',
        'facebook', 'twitter', 'snapchat', 'instagram', 'whatsapp'
    ];

    /**
     * to update the user profile
     *
     * @param UpdateProfileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile(UpdateProfileRequest $request)
    {

        $this->check_password($request);

        $user = auth()->user();


        $data = $request->only($this->attributes);


        if ($request->has('new_password')) {
            $user->password = bcrypt($request->new_password);
        }

        $user->update($data);

        $user->social_media->update($data);

        register_user_attachment($request, $user);

        make_api_token($user);

        return user_response($user);
    }

//    /**
//     * to update the user avatar
//     *
//     * @param  $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    private function avatar($request)
//    {
////        $this->check_password($request);
//
//        $attachment = Attachment::where([
//            ['related_id', auth()->user()->id],
//            ['type_id', Constants::_TYPE_AVATAR_IMAGE],
//        ])->first();
//
//        if (isset($attachment)) {
//            Storage::delete($attachment->path);
//        }
//
//
////        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['avatar' => 'updated']));
//    }



//    /**
//     * to update the user cover photo
//     *
//     * @param UpdateCover $request
//     * @return \Illuminate\Http\JsonResponse
//     */
//    public function cover($request)
//    {
//        $this->check_password($request);
//
//        $data ['related_id'] = auth()->user()->id;
//        $data ['type_id'] = Constants::_TYPE_COVER_IMAGE;
//
//        $attachment = Attachment::where($data)->skip($request->where)->first();
//
//        $dir = "users/{$data['related_id']}/";
//
//        if (isset($attachment)) {
//            Storage::delete($attachment->path);
//        }
//
//        $file = $request->file('cover');
//
//        $data['path'] = $file->store($dir . 'cover');
//        $data['mime_type'] = $file->getMimeType();
//        $data['extension'] = $file->extension();
//
//        if (isset($attachment)) {
//            $attachment->update($data);
//        } else {
//            Attachment::create($data);
//        }
//
//        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['cover' => 'updated']));
//
//    }

    /**
     * to update the reg id
     *
     * @param UpdateRegID $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update_reg_id(UpdateRegID $request)
    {
        $this->check_password($request);

        $user = auth()->user();

        $user->reg_id = $request->reg_id;

        $user->update();

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['reg_id' => 'updated']));
    }

    /**
     * Determine if the current user knows the password
     *
     * @param $request
     */
    private function check_password($request)
    {
        if ($request->has('token')){
           $social = SocialMediaToken::where('user_id',auth()->user()->id)->first();

           if (!Hash::check($request->token, $social->token)){
               abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
           }
        }else if (!Hash::check($request->password, auth()->user()->password))
            abort(Constants::RESPONSE_CODE_UNAUTHORIZED);
    }

}
