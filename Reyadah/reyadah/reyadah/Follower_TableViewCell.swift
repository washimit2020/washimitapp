//
//  Follower_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class Follower_TableViewCell: UITableViewCell {

    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    
    var user:User?{
        didSet{
            updateView()
        }
    }
    
    func updateView()  {
        guard let userData = user else {
            return
        }
        
        avatar.set_avatar_image(user_id: userData.id)
    
        name.text = userData.name
        location.text = make_location_text(userData)
    }
    
}
