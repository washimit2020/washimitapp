<?php

namespace App\Http\Requests\Ratings;

use App\Http\Constants;
use Illuminate\Foundation\Http\FormRequest;

class ProductRating extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id'=>'required|exists:products,id,status_id,'.Constants::_STATUS_ACTIVE,
            'quality'=>'required|numeric|min:1|max:5',
            'style'=>'required|numeric|min:1|max:5',
            'ingredients'=>'required|numeric|min:1|max:5',
            'price'=>'required|numeric|min:1|max:5',
        ];
    }
}
