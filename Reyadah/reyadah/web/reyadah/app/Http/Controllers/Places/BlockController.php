<?php

namespace App\Http\Controllers\Places;

use App\BlockedUser;
use App\Http\Constants;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlockController extends Controller
{
    /**
     * to block or unblock user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id,status_id,' . Constants::_STATUS_ACTIVE . ',id,!' . auth()->user()->id,
        ]);

        $data = [
            'user_id' => auth()->user()->id,
            'related_id' => $request->user_id];
        $blocked = BlockedUser::firstOrCreate($data);

        $msg = 'blocked';

        if (!$blocked->wasRecentlyCreated) {
            $blocked->delete();
            $msg = 'unblocked';
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['user' => $msg]));
    }

    /**
     * get all the blocked user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $users = User::whereHas('blocked_by',function ($query){
            $query->where('user_id',auth()->user()->id);
        })->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ['blocked' => $users]));
    }
}
