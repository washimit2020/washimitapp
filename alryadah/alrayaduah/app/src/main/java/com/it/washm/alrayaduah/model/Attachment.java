package com.it.washm.alrayaduah.model;

/**
 * Created by washm on 1/9/17.
 */

public class Attachment {
    String id;
    String path;
    String userId;
    String mime_type;
    AttachmentType type;


    public Attachment(String id, String path, String userId, String mime_type, AttachmentType type) {
        this.id = id;
        this.path = path;
        this.userId = userId;
        this.mime_type = mime_type;
        this.type = type;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public AttachmentType getType() {
        return type;
    }

    public void setType(AttachmentType type) {
        this.type = type;
    }
}