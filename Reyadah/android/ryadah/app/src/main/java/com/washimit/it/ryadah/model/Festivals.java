package com.washimit.it.ryadah.model;

import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.model.user.Rated;
import com.washimit.it.ryadah.model.user.SocialMedia;
import com.washimit.it.ryadah.model.user.User;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by mohammad on 5/13/2017.
 */

public class Festivals implements Serializable {
    int id;
    String name;
    String organization;
    String source;
    float lat;
    float lng;
    String street_name;
    String sector_name;
    String organizer_name;
    int organizer_phone_number;
    String start_in;
    String from;
    String to;
    String info;
    int user_id;
    float rating;
    int status_id;
    String created_at;
    String updated_at;
    ArrayList<Comment> comments;
    int followed;
    Rated rated;
    int total_followers;
    User user;
    SocialMedia social_media;

    public Festivals(){

    }
    public Festivals(int id, String name, String organization, String source, float lat, float lng, String street_name, String sector_name, String organizer_name, int organizer_phone_number, String start_in, String from, String to, String info, int user_id, float rating, int status_id, String created_at, String updated_at, ArrayList<Comment> comments, int followed, Rated rated, int total_followers, User user, SocialMedia social_media) {
        this.id = id;
        this.name = name;
        this.organization = organization;
        this.source = source;
        this.lat = lat;
        this.lng = lng;
        this.street_name = street_name;
        this.sector_name = sector_name;
        this.organizer_name = organizer_name;
        this.organizer_phone_number = organizer_phone_number;
        this.start_in = start_in;
        this.from = from;
        this.to = to;
        this.info = info;
        this.user_id = user_id;
        this.rating = rating;
        this.status_id = status_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.comments = comments;
        this.followed = followed;
        this.rated = rated;
        this.total_followers = total_followers;
        this.user = user;
        this.social_media = social_media;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getSector_name() {
        return sector_name;
    }

    public void setSector_name(String sector_name) {
        this.sector_name = sector_name;
    }

    public String getOrganizer_name() {
        return organizer_name;
    }

    public void setOrganizer_name(String organizer_name) {
        this.organizer_name = organizer_name;
    }

    public int getOrganizer_phone_number() {
        return organizer_phone_number;
    }

    public void setOrganizer_phone_number(int organizer_phone_number) {
        this.organizer_phone_number = organizer_phone_number;
    }

    public String getStart_in() {
        return start_in;
    }

    public void setStart_in(String start_in) {
        this.start_in = start_in;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getStatus_id() {
        return status_id;
    }

    public void setStatus_id(int status_id) {
        this.status_id = status_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<Comment> getComments() {
        return comments;
    }

    public void setComments(ArrayList<Comment> comments) {
        this.comments = comments;
    }

    public int getFollowed() {
        return followed;
    }

    public void setFollowed(int followed) {
        this.followed = followed;
    }

    public Rated getRated() {
        return rated;
    }

    public void setRated(Rated rated) {
        this.rated = rated;
    }

    public int getTotal_followers() {
        return total_followers;
    }

    public void setTotal_followers(int total_followers) {
        this.total_followers = total_followers;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public SocialMedia getSocial_media() {
        return social_media;
    }

    public void setSocial_media(SocialMedia social_media) {
        this.social_media = social_media;
    }

}
