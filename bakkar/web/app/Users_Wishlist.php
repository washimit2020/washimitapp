<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Users_Wishlist extends Model
{
    protected $fillable = ['post_id','user_id'];
    public $incrementing = false;
    protected $primaryKey = ['post_id','user_id'];

    protected $with = ['post'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function post(){
        return $this->belongsTo(Post::class);
    }
}