package com.it.washm.alrayaduah;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.adapter.wishlistAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.interfase.OnLoadMoreListener;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.model.wishlist;

public class WishlistActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    @BindView(R.id.fevert)
    TextView fevert;
    @BindView(R.id.wishlist_recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.wishlist_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.wishList_btn_back)
    ImageButton back;
    @BindView(R.id.wishList_noData)
    TextView noData;
    @BindView(R.id.activity_wishlist)
    LinearLayout linearLayout;
    Handler handler;
    Runnable runnable;
    ArrayList<wishlist> wishlists;
    Requests requests;
    LinearLayoutManager mLayoutManager;
    wishlistAdapter mAdapter;
    public String urls;
    Gson gson;
    CustamViewFont viewFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        ButterKnife.bind(this);
        viewFont = new CustamViewFont(this, "Cairo-Regular.ttf");
        setFont();

        wishlists = new ArrayList<>();

        handler = new Handler();
        requests = new Requests(getApplicationContext(), this);

        gson = new Gson();


        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);

        // use a linear layout manager
        mRecyclerView.setLayoutManager(mLayoutManager);
        // create an Object for Adapter
        mAdapter = new wishlistAdapter(this, wishlists, mRecyclerView,viewFont);

        // set the adapter object to the Recyclerview

        mRecyclerView.setAdapter(mAdapter);


        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                //add null , so the adapter will check view_type and show progress bar at bottom

                runnable = new Runnable() {
                    public void run() {
                        wishlists.add(null);
                        mAdapter.notifyItemInserted(wishlists.size() - 1);
                    }
                };
                handler.post(runnable);
                getwishlist(urls, false);


            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);


        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getwishlist("wishlist", true);

                                    }
                                }
        );


        back.setOnClickListener(this);


    }

    public void setFont() {
        viewFont.setfont(fevert);
        viewFont.setfont(noData);


    }

    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.setRefreshing(true);

        getwishlist("wishlist", true);
    }

    public void getwishlist(String url, final boolean isFirst) {

        if (AppHelper.isOnline(this)) {
            requests.StringRequest(Request.Method.GET, url, "", isFirst, new VolleyCallback() {
                @Override
                public void onSuccess(String result) {
                    if (isFirst) {
                        wishlists.clear();
                    } else {
                        try {
                            wishlists.remove(wishlists.size() - 1);
                            mAdapter.notifyItemRemoved(wishlists.size());

                        } catch (Exception e) {

                        }
                    }
                    JSONObject object = null;
                    JSONObject message = null;
                    JSONObject wishlist = null;
                    JSONArray data = null;

                    try {
                        object = new JSONObject(result);
                        if (object.getBoolean("success")) {

                            Log.d("dfmafafasf", result);

                            message = object.getJSONObject("message");
                            wishlist = message.getJSONObject("wishlist");

                            urls = wishlist.getString("next_page_url");
                            mAdapter.setUrl(wishlist.getString("next_page_url"));

                            data = wishlist.getJSONArray("data");

                            wishlists.addAll((Collection<? extends wishlist>) gson.fromJson(data.toString(), new TypeToken<ArrayList<wishlist>>() {
                            }.getType()));


                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (wishlists.size() > 0) {
                        mAdapter.setUrl(urls);
                        mAdapter.setLoaded();
                        mAdapter.notifyDataSetChanged();
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        noData.setVisibility(View.VISIBLE);
                    }

                    swipeRefreshLayout.setRefreshing(false);

                }

                @Override
                public void onRequestError(VolleyError errorMessage) {
                    swipeRefreshLayout.setRefreshing(false);
                    Snackbar.make(linearLayout, "حصلة مشكلة ما يرجي اعادة المحاولة لاحقا", BaseTransientBottomBar.LENGTH_LONG).show();

                }
            });
        } else {
            swipeRefreshLayout.setRefreshing(false);

            Snackbar.make(linearLayout, "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

        }
    }


    @Override
    public void onRefresh() {
        getwishlist("wishlist", true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wishList_btn_back:
                this.finish();
                break;

        }
    }
}
