//
//  Status.swift
//  reyadah
//
//  Created by WASHM on 4/18/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//


class Status: Model {
    var id:Int!
    var title:String!
    
    override func map() {
        id = json["id"].intValue
        title = json["title"].stringValue
    }
}

class ModelType: Model {
    var id:Int!
    var title:String!
    
    override func map() {
        id = json["id"].intValue
        title = json["title"].stringValue
    }
}
