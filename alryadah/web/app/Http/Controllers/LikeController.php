<?php

namespace App\Http\Controllers;

use App\CommentLike;
use App\Http\Helpers\ResponseHelper;
use App\Http\Requests\Comments\Like;
use App\Http\Requests\News\NewsLikeRequest;
use App\Http\Requests\Replies\RepliesLike;
use App\News;
use App\NewsLike;
use App\Notifications\NewsLiked;
use App\ReplyLike;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class LikeController extends BaseController
{
    /**
     * @var array
     */
    private $data;

    /**
     * @var User
     */
    private $user;

    /**
     * @param Like $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function comment(Like $request)
    {
        $this->user = $request->user();

        $this->data = [
            "comment_id" => $request->comment_id
        ];

        return $this->like(new CommentLike());

    }

    /**
     * @param RepliesLike $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reply(RepliesLike $request)
    {
        $this->user = $request->user();

        $this->data = [
            "reply_id" => $request->reply_id
        ];

        return $this->like(new ReplyLike());
    }

    /**
     * @param NewsLikeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function news(NewsLikeRequest $request)
    {
        $this->user = $request->user();
        $this->data = [
            "news_id" => $request->news_id
        ];
        return $this->like(new NewsLike());
    }


    /**
     * @param Model $model
     * @return \Illuminate\Http\JsonResponse
     */
    private function like(Model $model)
    {
        $this->data['user_id'] = $this->user->id;
        add_or_remove($this->data, $model);

        unset($this->data['user_id']);

        return response()->json(ResponseHelper::successResponse(["likes" => $model::where($this->data)->count()]));
    }

}
