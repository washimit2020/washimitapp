package com.it.washim.wojooohapp.Halper;

import android.content.Context;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.it.washim.wojooohapp.Model.Categorie;

import java.util.ArrayList;

/**
 * Created by mohammad on 10/31/2017.
 */

public class SharedPref {
    private static final SharedPref ourInstance = new SharedPref();

    private static String categories;


    public static SharedPref getInstance(Context context) {

        return ourInstance;

    }

    private SharedPref() {
    }

    public void setCategories(String categories) {
        this.categories=categories;

    }

    public ArrayList<Categorie> getCategories() {
        if(categories==null){
            return null;
        }
        return new Gson().fromJson(categories, new TypeToken<ArrayList<Categorie>>() {
        }.getType());

    }

}
