//
//  ResetPasswordTVC.swift
//  reyadah
//
//  Created by WASHM on 4/12/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator

class ResetPasswordTVC: FormsTVC {
    
    @IBOutlet weak var passwordConfirmationError: UILabel!
    @IBOutlet weak var passwordConfirmation: UITextField!
    @IBOutlet weak var passwordError: UILabel!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var emailError: UILabel!
    @IBOutlet weak var email: UITextField!
    
    let home = "backToHome"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fields = [(email,emailError),(password,passwordError),(passwordConfirmation,passwordConfirmationError)]
        
        validator.registerField(email, errorLabel: emailError, rules: [RequiredRule(message: Constants.requiredErrorMsg), EmailRule(message: Constants.emailErrorMsg)])
        
        validator.registerField(password, errorLabel: passwordError, rules: [RequiredRule(message: Constants.requiredErrorMsg), MinLengthRule(length: 6, message: Constants.minLengthErrorMsg)])
        
        validator.registerField(passwordConfirmation, errorLabel: passwordConfirmationError, rules: [RequiredRule(message: Constants.requiredErrorMsg), ConfirmationRule(confirmField: password,message:Constants.confirmedErrorMsg)])
        
        action = send(_:)
    }
    
    override func validationSuccessful() {
        performSegue(withIdentifier: home, sender: self)
    }
    
    @IBAction func send(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
}
