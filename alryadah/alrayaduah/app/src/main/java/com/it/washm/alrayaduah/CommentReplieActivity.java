package com.it.washm.alrayaduah;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.adapter.CommentRepliesAdapter;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Links;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.model.Attachment;
import com.it.washm.alrayaduah.model.AttachmentType;
import com.it.washm.alrayaduah.model.User;
import com.it.washm.alrayaduah.model.UserRole;
import com.it.washm.alrayaduah.model.replies;
import com.it.washm.alrayaduah.registration.LogInActivity;

public class CommentReplieActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.comment_Replies_Recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.comment_Replies_text)
    EditText Replies_text;
    @BindView(R.id.comment_Replies_but)
    ImageView Replies_but;
    @BindView(R.id.name_main_comment)
    TextView name_main_comment;
    @BindView(R.id.comment_main)
    TextView comment_main;
    @BindView(R.id.like_comment)
    TextView like_comment;
    @BindView(R.id.date_comment)
    TextView date_comment;
    @BindView(R.id.time_comment)
    TextView time_comment;
    @BindView(R.id.img_main_comment_item)
    ImageView img_main_comment_item;
    @BindView(R.id.no_replies)
    TextView no_replies;
    @BindView(R.id.progressBar)
    AVLoadingIndicatorView progressBar;


    //   @InjectView(R.id.Replies_comment_refresh_layout)SwipeRefreshLayout refreshLayout;

    Requests requests;
    LinearLayoutManager manager;
    CommentRepliesAdapter mAdapter;
    ArrayList<replies> repliesList;
    SharedPref pref;
    public static HashMap<String, String> Stringparams;

    //  Handler handler ;
    //  Runnable runnable;
    Gson gson;
    //    Bundle extras;
    String urls;
    String commentId, date, time, user_name, img_user, comment;
    int like_count;
    Bundle extras;
    boolean IsLike;
    Intent intent;
    CustamViewFont viewFont;

    @Override
    public void onBackPressed() {
        if (MainActivity.activity == true) {
            super.onBackPressed();
        } else {
            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_replie);
        ButterKnife.bind(this);
        viewFont = new CustamViewFont(this, "Cairo-Regular.ttf");
        setFont();
        gson = new Gson();
        pref = new SharedPref(getApplicationContext().getApplicationContext(), "MyPref");
        Stringparams = new HashMap<String, String>();

        extras = getIntent().getExtras();
        if (extras != null) {
            commentId = extras.getString("commentId");
            date = extras.getString("date");
            time = extras.getString("time");
            user_name = extras.getString("user_name");
            img_user = extras.getString("img_user");
            comment = extras.getString("comment");
            like_count = extras.getInt("like_count");
            IsLike = extras.getBoolean("IsLike");
        }


        if (IsLike) {

            like_comment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);
        } else {

            like_comment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);
        }
        Picasso.with(this).load(Links.UrlImg + img_user).fit().centerCrop()
                .placeholder(R.drawable.ic_defult_profile_pic).into(img_main_comment_item);
        name_main_comment.setText(user_name);
        comment_main.setText(comment);
        like_comment.setText(like_count + "");
        date_comment.setText(date);
        time_comment.setText(time);

        //  handler = new Handler();
        repliesList = new ArrayList<>();

        requests = new Requests(getApplicationContext(), this);

        recyclerView.setHasFixedSize(true);

        manager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(manager);

        mAdapter = new CommentRepliesAdapter(this, repliesList, recyclerView);

        recyclerView.setAdapter(mAdapter);

        getRepliesComments();


        like_comment.setOnClickListener(this);
        Replies_but.setOnClickListener(this);

        Replies_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (Replies_text.getText().length() > 0) {
                    Replies_but.setClickable(true);
                    Replies_but.setImageResource(R.drawable.ic_send_act);
                } else {
                    Replies_but.setClickable(false);
                    Replies_but.setImageResource(R.drawable.ic_send);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Replies_text.getText().length() > 0) {
                    Replies_but.setClickable(true);
                    Replies_but.setImageResource(R.drawable.ic_send_act);
                } else {
                    Replies_but.setClickable(false);
                    Replies_but.setImageResource(R.drawable.ic_send);
                }
            }
        });

//        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                //add null , so the adapter will check view_type and show progress bar at bottom
//
//                runnable = new Runnable() {
//                    public void run() {
//                        repliesList.add(null);
//                        mAdapter.notifyItemInserted(repliesList.size() - 1);
//                    }
//                };
//                handler.post(runnable);
//           //     getNextRepliesComment(urls);
//
//            }
//        });
//
//        refreshLayout.setOnRefreshListener(this);
//
//        refreshLayout.post(new Runnable() {
//                               @Override
//                               public void run() {
//                                   refreshLayout.setRefreshing(true);
//                                   getRepliesComments();
//
//                               }
//                           }
//        );
    }


    public void setFont() {
        viewFont.setfont(comment_main, "Cairo-Regular.ttf");
        viewFont.setfont(like_comment, "Cairo-Regular.ttf");
        viewFont.setfont(date_comment, "Cairo-Regular.ttf");
        viewFont.setfont(time_comment, "Cairo-Regular.ttf");
        viewFont.setfont(no_replies, "Cairo-Regular.ttf");
        viewFont.setfont(Replies_text, "Cairo-Regular.ttf");
        viewFont.setfont(name_main_comment, "Cairo-Bold.ttf");

    }


    public void getRepliesComments() {
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        requests.StringRequest(Request.Method.GET, "replies/" + commentId, "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                repliesList.clear();
                JSONObject object = null;
                JSONObject message = null;
                JSONObject replies = null;

                try {
                    object = new JSONObject(result);
                    if (object.getBoolean("success")) {
                        message = object.getJSONObject("message");
                        replies = message.getJSONObject("replies");

                        urls = replies.getString("next_page_url");
                        mAdapter.setUrl(replies.getString("next_page_url"));

                        repliesList.addAll(AppHelper.get_comment_replies(replies.getJSONArray("data")));

                        if (!urls.equalsIgnoreCase("null")) {
                            repliesList.add(0, null);
                            mAdapter.notifyItemInserted(0);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (repliesList.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    no_replies.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    mAdapter.setLoaded();
                    mAdapter.notifyDataSetChanged();
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            recyclerView.smoothScrollToPosition(repliesList.size());
                        }
                    }, 10);

                } else {
                    no_replies.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);

                }
                //    refreshLayout.setRefreshing(false);
            }


            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.like_comment:

                if (AppHelper.IsLogIn(this)) {
                    like_comment.setClickable(false);

                    if (!IsLike) {
                        like_comment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like_act, 0);
                        IsLike = true;
                        like_comment.setClickable(true);
                        like_comment.setText(String.valueOf(like_count + 1));
                        like_count = like_count + 1;

                    } else {


                        like_comment.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_like, 0);
                        IsLike = false;
                        like_comment.setClickable(true);
                        like_comment.setText(String.valueOf(like_count - 1));
                        like_count = like_count - 1;

                    }


                    Stringparams.put("comment_id", commentId);
                    requests.StringRequest(Request.Method.POST, "like/comment", Stringparams, "", new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {
                            like_comment.setClickable(true);

                        }

                        @Override
                        public void onRequestError(VolleyError errorMessage) {
                            like_comment.setClickable(true);

                        }
                    });
                } else {
                    Toast.makeText(getApplicationContext(), "يجب تسجيل الدخول للآعجاب!", Toast.LENGTH_LONG).show();

                }
                break;


            case R.id.comment_Replies_but:
                if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {
                    Replies_but.setClickable(false);
                    Stringparams.put("content", Replies_text.getText().toString());
                    Stringparams.put("comment_id", commentId);
                    requests.StringRequest(Request.Method.POST, "replies", Stringparams, "", new VolleyCallback() {
                        @Override
                        public void onSuccess(String result) {

                            //  getRepliesComments();


                            JSONObject object = null;
                            JSONObject message = null;
                            JSONObject replies = null;


                            try {
                                object = new JSONObject(result);
                                if (!object.getBoolean("success")) {
                                    return;
                                }
                                message = object.getJSONObject("message");
                                replies = message.getJSONObject("reply");
                                ArrayList<Attachment> attachments = new ArrayList<Attachment>();
                                attachments.add(new Attachment("", pref.gitShared("path"), pref.gitShared("id"), "", new AttachmentType("", "", "")));

                                repliesList.add(repliesList.size(), new replies(replies.getString("id"), replies.getString("comment_id"), replies.getString("content"),
                                        replies.getString("created_at"), replies.getString("updated_at"), 0, false
                                        , new User(pref.gitShared("id"), pref.gitShared("first_name"), pref.gitShared("last_name"),
                                        pref.gitSharedNek("nick_name"), "", "", "", new UserRole("", "", "", ""), attachments)));


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            if (repliesList.size() > 0) {
                                no_replies.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                mAdapter.notifyItemInserted(repliesList.size() + 1);
                                recyclerView.smoothScrollToPosition(repliesList.size());


                            } else {
                                no_replies.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                            }

                            Replies_but.setClickable(true);
                            Replies_text.setText("");


                        }

                        @Override
                        public void onRequestError(VolleyError errorMessage) {
                            Replies_but.setClickable(true);

                        }
                    });
                } else {
                    AppHelper.showDialog(this, "يجب تسجيل الدخول للتعليق","تسجيل الدخول","", true, new dialogCallback() {
                        @Override
                        public void ok(Dialog dialog) {
                            intent = new Intent(getApplicationContext(), LogInActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            dialog.cancel();
                        }

                        @Override
                        public void hidden(Dialog dialog) {
                            dialog.cancel();
                        }
                    });
                }
                break;
        }
    }

//    @Override
//    public void onRefresh() {
//
//    }
}
