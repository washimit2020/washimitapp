package com.it.washm.alrayaduah.registration;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.firebase.iid.FirebaseInstanceId;
//import com.twitter.sdk.android.Twitter;
//import com.twitter.sdk.android.core.Callback;
//import com.twitter.sdk.android.core.Result;
//import com.twitter.sdk.android.core.TwitterAuthConfig;
//import com.twitter.sdk.android.core.TwitterException;
//import com.twitter.sdk.android.core.TwitterSession;
//import com.twitter.sdk.android.core.identity.TwitterAuthClient;
//import com.twitter.sdk.android.core.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
//import io.fabric.sdk.android.Fabric;
import com.it.washm.alrayaduah.MainActivity;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.helper.SharedPref;
import com.it.washm.alrayaduah.interfase.VolleyCallback;

/**
 * Created by washm on 1/3/17.
 */

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.


    private static final String TWITTER_KEY = "dyRECw9gG0J02x9u7nK7AsPjG";
    private static final String TWITTER_SECRET = "D8TJC3O5c8Wzbp0gm0uKK2wVZqbeTG1noMP3taqjkJStTAttyT";



    @BindView(R.id.new_account_log_in)
    TextView new_account;
    @BindView(R.id.or_reg)
    TextView or_reg;
    @BindView(R.id.remmper_log_in)
    TextView forget_the_password;
    @BindView(R.id.btn_log_in_text)
    Button btn_log_in_text;
    @BindView(R.id.btn_log_in)
    RippleView btn_log_in;
    @BindView(R.id.facebook_log_in)
    RippleView facebook_log_in;
    @BindView(R.id.twitter_log_in)
    RippleView twitter_log_in;
    @BindView(R.id.google_log_in)
    RippleView google_log_in;
    @BindView(R.id.email_log_in)
    AutoCompleteTextView email_log_in;
    @BindView(R.id.password_log_in)
    AutoCompleteTextView password_log_in;
    @BindView(R.id.skip)
    TextView skip;
    @BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;
    @BindView(R.id.tilemail_log_in)
    TextInputLayout tilemail_log_in;
    @BindView(R.id.tilpassword_log_in)
    TextInputLayout tilpassword_log_in;
    Intent intent;
    Requests requests;
    HashMap<String, String> Stringparams;
    String email;
    String password;
    public static String TAG = "LOG_IN";
    private static final int RC_SIGN_IN = 9001;
    SharedPref pref;
    public static Activity activity;
    CustamViewFont viewFont;
    CallbackManager callbackManager;
//    TwitterSession session;
//    TwitterAuthClient client;
    GoogleApiClient mGoogleApiClient;
    GoogleSignInResult result;
    GoogleSignInOptions gso;

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
      //  client.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);

        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
    //    Fabric.with(this, new Twitter(authConfig));
    //    client = new TwitterAuthClient();
        callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(getApplicationContext());
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestScopes(new Scope(Scopes.PLUS_LOGIN)).requestEmail().build();
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(Plus.API)
                .build();
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.it.washm.alrayaduah",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        viewFont = new CustamViewFont(LogInActivity.this, "Cairo-SemiBold.ttf");

        setFont();


        activity = this;

        requests = new Requests(getApplicationContext(), this);
        Stringparams = new HashMap<String, String>();
        pref = new SharedPref(getApplicationContext(), "MyPref");

        new_account.setOnClickListener(this);
        forget_the_password.setOnClickListener(this);
        btn_log_in.setOnClickListener(this);
        facebook_log_in.setOnClickListener(this);
        twitter_log_in.setOnClickListener(this);
        google_log_in.setOnClickListener(this);
        email_log_in.setOnClickListener(this);
        password_log_in.setOnClickListener(this);
        skip.setOnClickListener(this);

        if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {
            intent = new Intent(LogInActivity.this, MainActivity.class);
            startActivity(intent);
            LogInActivity.this.finish();
        }

    }


    public void setFont() {
        viewFont.setfont(tilemail_log_in);
        viewFont.setfont(tilpassword_log_in);
        viewFont.setfont(btn_log_in_text, "Cairo-Regular.ttf");
        viewFont.setfont(forget_the_password, "Cairo-Regular.ttf");
        viewFont.setfont(new_account, "Cairo-Bold.ttf");
        viewFont.setfont(or_reg, "Cairo-Regular.ttf");
        viewFont.setfont(skip, "Cairo-Bold.ttf");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.new_account_log_in: {
                intent = new Intent(this, SignUpActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.remmper_log_in: {
                intent = new Intent(this, ForgetThePasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            }
            case R.id.btn_log_in: {

                if (validate()) {

                    if (AppHelper.isOnline(LogInActivity.this)) {
                        Stringparams.put("email", email);
                        Stringparams.put("password", password);
                        Log.d("getToken", FirebaseInstanceId.getInstance().getToken() + "");
                        Stringparams.put("reg_id", FirebaseInstanceId.getInstance().getToken() + "");
                        requests.StringRequest(Request.Method.POST, "login", Stringparams, "جاري تسجيل الدخول", new VolleyCallback() {
                            @Override
                            public void onSuccess(String result) {
                                Log.d(TAG, result);
                                JSONObject object = null;
                                JSONObject message = null;
                                JSONObject user = null;
                                JSONObject role = null;
                                JSONArray attachment = null;
                                try {
                                    object = new JSONObject(result);
                                    if (object.getBoolean("success")) {
                                        message = object.getJSONObject("message");
                                        user = message.getJSONObject("user");
                                        role = user.getJSONObject("role");
                                        attachment = user.getJSONArray("attachment");

                                        Log.d(TAG, result);
                                        pref.clearRef();

                                        //user id
                                        pref.setShared("id", user.getString("id"));

                                        //first_name
                                        pref.setShared("first_name", user.getString("first_name"));

                                        //last_name
                                        pref.setShared("last_name", user.getString("last_name"));


                                        //nick_name
                                        if (!user.getString("nick_name").equalsIgnoreCase("null")) {
                                            pref.setShared("nick_name", user.getString("nick_name"));

                                        }

                                        //user_role id
                                        pref.setShared("role_id", role.getString("id"));

                                        //user attachment
                                        if (attachment.length() > 0) {
                                            pref.setShared("path", attachment.getJSONObject(0).getString("path"));
                                        } else {
                                            pref.setShared("path", "");

                                        }
                                        pref.setShared("is_log_in", "1");
                                        //user token
                                        pref.setShared("token", message.getString("token"));

                                        //time_type
                                        pref.setShared("time", "24");


                                        //date_type
                                        pref.setShared("date", "m");


                                        //notification
                                        pref.setShared("notification", "1");


                                        intent = new Intent(LogInActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        finishAffinity();
                                    } else {
                                        Snackbar.make(relativeLayout, "" + object.getString("message"), BaseTransientBottomBar.LENGTH_LONG).show();

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void onRequestError(VolleyError errorMessage) {
                                Snackbar.make(relativeLayout, "توجد مشكلة ما اعد المحاولة لاحقا", BaseTransientBottomBar.LENGTH_LONG).show();

                            }
                        });

                    } else {
                        Snackbar.make(relativeLayout, "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }
                }
                break;
            }
            case R.id.facebook_log_in: {
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                // App code
                                GraphRequest request = GraphRequest.newMeRequest(
                                        loginResult.getAccessToken(),
                                        new GraphRequest.GraphJSONObjectCallback() {
                                            @Override
                                            public void onCompleted(JSONObject object, GraphResponse response) {
                                                Log.v("LoginActivity", response.toString());

                                                // Application code
                                                try {
                                                    String email = object.getString("email");
                                                    Log.v("LoginActivityemail", email);


                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });
                                Bundle parameters = new Bundle();
                                parameters.putString("fields", "id,name,email,first_name,last_name,picture,gender");
                                request.setParameters(parameters);
                                request.executeAsync();
                            }

                            @Override
                            public void onCancel() {
                                Log.d("loginButton", "cancel");

                            }

                            @Override
                            public void onError(FacebookException error) {
                                Toast.makeText(getApplicationContext(), "حدث خطأ ما يرجى اعادة المحاولة فيما بعد" + error, Toast.LENGTH_LONG).show();

                            }
                        });
                break;
            }
            case R.id.twitter_log_in: {

//
//                client.authorize(this, new Callback<TwitterSession>() {
//                    @Override
//                    public void success(Result<TwitterSession> twitterSessionResult) {
//                        Toast.makeText(getApplicationContext(), "success", Toast.LENGTH_SHORT).show();
//
//                        session = twitterSessionResult.data;
//
//                        String username = session.getUserName();
//
//                        Long userid = session.getUserId();
//                        session.getAuthToken();
//
//
//                        Twitter.getApiClient(session).getAccountService().verifyCredentials(true, true).enqueue(new Callback<User>() {
//                            @Override
//                            public void success(Result<User> userResult) {
//                                User user = userResult.data;
//                                String twitterImage = user.profileImageUrl;
//
//                                try {
//                                    Log.d("imageurl", user.profileImageUrl);
//                                    Log.d("name", user.name);
//                                    Log.d("des", user.description);
//                                    Log.d("followers ", String.valueOf(user.followersCount));
//                                    Log.d("createdAt", user.createdAt);
//
//                                    client.requestEmail(session, new Callback<String>() {
//                                        @Override
//                                        public void success(Result<String> result) {
//                                            // Do something with the result, which provides the email address
//                                            Log.d("email", new String(result.data));
//
//                                        }
//
//                                        @Override
//                                        public void failure(TwitterException exception) {
//                                            // Do something on failure
//                                            String exceptionMsg = exception.getMessage();
//                                            Toast.makeText(getApplicationContext(), "TwitterException = " + exceptionMsg, Toast.LENGTH_LONG).show();
//                                        }
//                                    });
//
//
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//
//
//                            }
//
//                            @Override
//                            public void failure(TwitterException e) {
//
//                            }
//
//                        });
//
//
//                    }
//
//                    @Override
//                    public void failure(TwitterException e) {
//                    }
//                });

                break;
            }
            case R.id.google_log_in: {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            }

            case R.id.skip:
                //time_type
                pref.setShared("time", "24");

                //date_type
                pref.setShared("date", "m");
                //notification
                pref.setShared("notification", "1");
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                LogInActivity.this.finish();

                break;

        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.

            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            Log.d("google", "Display Name: " + person.getDisplayName());
            Log.d("google", "Gender: " + person.getGender());

            GoogleSignInAccount acct = result.getSignInAccount();
            Log.d("google", acct.getEmail());
            Log.d("google", acct.getDisplayName());
            Log.d("google", acct.getGivenName());
            Log.d("google", acct.getFamilyName());
            Log.d("google", acct.getId());
            // Log.d("google",acct.getIdToken());
            Log.d("google", acct.getPhotoUrl() + "");

        } else {
            // Signed out, show unauthenticated UI.
        }
    }

    public boolean validate() {
        boolean valid = true;

        email = email_log_in.getText().toString();
        password = password_log_in.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            email_log_in.setError("آدخل ايميل صحيح");
            valid = false;
        } else {
            email_log_in.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            password_log_in.setError("آكبر من ٤ احرف او اقل من ١٠ احرف");
            valid = false;
        } else {
            password_log_in.setError(null);
        }

        return valid;
    }

}
