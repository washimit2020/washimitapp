<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 8/7/17
 * Time: 8:11 AM
 */

namespace App;



class Users_Roles extends Translatable
{
    public function users()
    {
        return $this->hasMany(User::class);
    }
}