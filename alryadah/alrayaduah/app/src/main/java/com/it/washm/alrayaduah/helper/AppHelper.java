package com.it.washm.alrayaduah.helper;

/**
 * Created by washm on 1/8/17.
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.interfase.VolleyCallback;
import com.it.washm.alrayaduah.interfase.dialogCallback;
import com.it.washm.alrayaduah.model.Comments;
import com.it.washm.alrayaduah.model.news;
import com.it.washm.alrayaduah.model.replies;

/**
 * Sketch Project Studio
 * Created by Angga on 12/04/2016 14.27.
 */
public class AppHelper {

    static int MAX_IMAGE_DIMENSION = 2000;


    /**
     * @param banners
     * @return
     * @throws JSONException
     */
    public static ArrayList<news> get_news_response(JSONArray banners) throws JSONException {
        ArrayList<news> response = new ArrayList();

        for (int i = 0; i < banners.length(); i++) {

            Gson gson = new Gson();


            response.add(gson.fromJson(banners.get(i).toString(), news.class));

        }
        return response;
    }

    /**
     * @param banners
     * @return
     * @throws JSONException
     */
    public static ArrayList<Comments> get_comment_response(JSONArray banners) throws JSONException {
        ArrayList<Comments> response = new ArrayList();

        for (int i = 0; i < banners.length(); i++) {

            Gson gson = new Gson();


            response.add(0, gson.fromJson(banners.get(i).toString(), Comments.class));

        }
        return response;
    }

    /**
     * @param banners
     * @return
     * @throws JSONException
     */
    public static ArrayList<replies> get_comment_replies(JSONArray banners) throws JSONException {
        ArrayList<replies> response = new ArrayList();

        for (int i = 0; i < banners.length(); i++) {

            Gson gson = new Gson();


            response.add(0, gson.fromJson(banners.get(i).toString(), replies.class));

        }
        return response;
    }

    /**
     * @param url
     * @param activity
     * @param hashMap
     * @param msg
     */
    public static void report(String url, final Activity activity, HashMap<String, String> hashMap, final String msg) {
        Requests requests = new Requests(activity.getApplicationContext(), activity);

        requests.StringRequest(Request.Method.POST, url, hashMap, "", new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }

    /**
     * @param url
     * @param activity
     * @param msg
     */
    public static void delete(String url, final Activity activity, final String msg) {
        Requests requests = new Requests(activity.getApplicationContext(), activity);

        requests.StringRequest(Request.Method.DELETE, url, "", true, new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }

    /**
     * @param url
     * @param activity
     * @param Stringparams
     * @param msg
     */
    public static void wishlist(String url, final Activity activity, HashMap<String, String> Stringparams, final String msg) {


        Requests requests = new Requests(activity.getApplicationContext(), activity);


        requests.StringRequest(Request.Method.POST, url, Stringparams, "", new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                Toast.makeText(activity, msg, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onRequestError(VolleyError errorMessage) {

            }
        });


    }

    /**
     * @return
     */
    public static Date GetUTCdatetimeAsDate() {
        //note: doesn't check for null
        return StringDateToDate(GetUTCdatetimeAsString());
    }

    /**
     * @return
     */
    public static String GetUTCdatetimeAsString() {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        return utcTime;
    }

    /**
     * @param StrDate
     * @return
     */
    public static Date StringDateToDate(String StrDate) {
        Date dateToReturn = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");

        try {
            dateToReturn = (Date) dateFormat.parse(StrDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dateToReturn;
    }

    /**
     * @param inputdate
     * @param context
     * @param date_
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static String getDateDifferenceForDisplay(Date inputdate, Context context, String date_) {
        SharedPref pref = new SharedPref(context, "MyPref");
        Calendar now = Calendar.getInstance();
        Calendar then = Calendar.getInstance();
        now.setTime(GetUTCdatetimeAsDate());
        then.setTime(inputdate);

        // Get the represented date in milliseconds
        long nowMs = now.getTimeInMillis();
        long thenMs = then.getTimeInMillis();

        // Calculate difference in milliseconds
        long diff = nowMs - thenMs;

        // Calculate difference in seconds
        long diffSucend = diff / (1000);
        long diffMinutes = diff / (60 * 1000);
        long diffHours = diff / (60 * 60 * 1000);
        long diffDays = diff / (24 * 60 * 60 * 1000);

        if (diffSucend < 59) {
            if (diffSucend == 1) {
                return "منذ ثانية ";

            } else if (diffSucend == 2) {
                return "منذ ثانيتان ";

            } else if (diffSucend > 2 && diffSucend < 11) {
                return "منذ " + diffSucend + " ثواني";

            } else {
                return "منذ " + diffSucend + " ثانية";

            }


        } else if (diffMinutes < 60) {
            if (diffMinutes == 1) {
                return "منذ دقيقة ";

            } else if (diffMinutes == 2) {
                return "منذ دقيقتان ";

            } else if (diffMinutes > 2 && diffDays < 11) {
                return "منذ " + diffMinutes + " دقائق";

            } else {
                return "منذ " + diffMinutes + " دقيقة";

            }
        } else if (diffHours < 24) {
            if (diffHours == 1) {
                return "منذ ساعة ";

            } else if (diffHours == 2) {
                return "منذ ساعتان ";

            } else if (diffHours > 2 && diffHours < 11) {
                return "منذ " + diffHours + " ساعات";

            } else {
                return "منذ " + diffHours + " ساعة";

            }

        } else if (diffDays < 7) {

            if (diffDays == 1) {
                return "منذ يوم ";

            } else if (diffDays == 2) {
                return "منذ يومان ";

            } else {
                return "منذ " + diffDays + " أيام";

            }


        } else if (diffDays == 7) {
            return "منذ أسبوع";
        } else {

            Locale ar = new Locale("ar");

            if (pref.gitShared("date").equalsIgnoreCase("h")) {
                Calendar uCal = new UmmalquraCalendar(ar);
                try {
                    uCal.setTime(new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.forLanguageTag("ar")).parse(date_));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return uCal.get(Calendar.DAY_OF_MONTH) + " " + uCal.getDisplayName(Calendar.MONTH, Calendar.LONG, ar);
            } else {
                return new SimpleDateFormat("dd MMM", ar).format(inputdate);
            }
        }
    }

    /**
     * @param context
     * @param photoUri
     * @return
     * @throws IOException
     */
    public static Bitmap getCorrectlyOrientedImage(Context context, Uri photoUri) throws IOException {
        InputStream is = context.getContentResolver().openInputStream(photoUri);
        BitmapFactory.Options dbo = new BitmapFactory.Options();
        dbo.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(is, null, dbo);
        is.close();

        int rotatedWidth, rotatedHeight;
        int orientation = getOrientation(context, photoUri);

        if (orientation == 90 || orientation == 270) {
            rotatedWidth = dbo.outHeight;
            rotatedHeight = dbo.outWidth;
        } else {
            rotatedWidth = dbo.outWidth;
            rotatedHeight = dbo.outHeight;
        }

        Bitmap srcBitmap;
        is = context.getContentResolver().openInputStream(photoUri);
        if (rotatedWidth > MAX_IMAGE_DIMENSION || rotatedHeight > MAX_IMAGE_DIMENSION) {
            float widthRatio = ((float) rotatedWidth) / ((float) MAX_IMAGE_DIMENSION);
            float heightRatio = ((float) rotatedHeight) / ((float) MAX_IMAGE_DIMENSION);
            float maxRatio = Math.max(widthRatio, heightRatio);

            // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = (int) maxRatio;
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
        } else {
            srcBitmap = BitmapFactory.decodeStream(is);
        }
        is.close();

    /*
     * if the orientation is not 0 (or -1, which means we don't know), we
     * have to do a rotation.
     */
        if (orientation > 0) {
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);

            srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0, srcBitmap.getWidth(),
                    srcBitmap.getHeight(), matrix, true);
        }

        return srcBitmap;
    }

    /**
     * @param context
     * @param photoUri
     * @return
     */
    public static int getOrientation(Context context, Uri photoUri) {
    /* it's on the external media. */
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);

        if (cursor.getCount() != 1) {
            return -1;
        }

        cursor.moveToFirst();
        return cursor.getInt(0);
    }

    /**
     * @param activity
     * @param uri
     * @return
     */
    public static String getPathfromURI(Activity activity, Uri uri) {
        try {
            Cursor cursor = activity.getContentResolver().query(uri, null, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param activity
     * @param resultCode
     */
    public static void showImgOrVideo(Activity activity, int resultCode) {


        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        } else {

            Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            pickIntent.setType("image/* video/*");
            Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

            String pickTitle = "Select or take a new Picture"; // Or get from strings.xml
            Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
            chooserIntent.putExtra
                    (
                            Intent.EXTRA_INITIAL_INTENTS,
                            new Intent[]{takePhotoIntent, takeVideoIntent}
                    );

            activity.startActivityForResult(chooserIntent, resultCode);
        }
    }


    /**
     * @param activity
     * @return
     */
    public static boolean IsLogIn(Activity activity) {
        SharedPref pref = new SharedPref(activity, "MyPref");

        if (pref.gitShared("is_log_in").equalsIgnoreCase("1")) {
            return true;
        } else return false;

    }


    /**
     * @param context
     * @param info
     * @param withHidden
     * @param dialogCallback
     */
    public static void showDialog(Activity context, String info, String okText, String hoddenText, boolean withHidden, final dialogCallback dialogCallback) {


        CustamViewFont viewFont = new CustamViewFont(context, "Cairo-SemiBold.ttf");
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dilog_news_details);
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        // wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FIRST_SYSTEM_WINDOW;
        window.setAttributes(wlp);

        TextView textInfo = (TextView) dialog.findViewById(R.id.textInfo);
        TextView hidden = (TextView) dialog.findViewById(R.id.hidden);
        TextView ok = (TextView) dialog.findViewById(R.id.ok);

        if (okText.length() > 0) {
            ok.setText(okText);
        }
        if (hoddenText.length() > 0) {
            hidden.setText(hoddenText);
        }
        viewFont.setfont(textInfo);
        viewFont.setfont(hidden);
        viewFont.setfont(ok);
        textInfo.setText(info);
        if (!withHidden) {
            hidden.setVisibility(View.GONE);
        }
        hidden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCallback.hidden(dialog);
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogCallback.ok(dialog);
            }
        });


        dialog.show();


    }

    /**
     * @param imageView
     * @return
     */
    public static Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            File file = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS), "share_image_" + System.currentTimeMillis() + ".png");
            file.getParentFile().mkdirs();
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


    /**
     * @param activity
     * @return
     */
    public static boolean isOnline(Activity activity) {
        if (activity != null) {
            ConnectivityManager cm =
                    (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            return netInfo != null && netInfo.isConnectedOrConnecting();
        }
        return true;
    }

    /**
     * @param date_
     * @param context
     * @param format_
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static String getDateType(final String date_, Context context, String format_) {
        SimpleDateFormat format = new SimpleDateFormat(format_, Locale.forLanguageTag("ar"));
        SharedPref pref = new SharedPref(context, "MyPref");
        String d[] = date_.split(" ");

        if (pref.gitShared("date").equalsIgnoreCase("h")) {
            Calendar uCal = new UmmalquraCalendar();
            try {
                uCal.setTime(format.parse(date_));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return uCal.get(Calendar.YEAR) + "-" + (uCal.get(Calendar.MONTH) + 1) + "-" + uCal.get(Calendar.DAY_OF_MONTH)
                    + " " + TimeType(uCal.get(Calendar.HOUR) + ":" + uCal.get(Calendar.MINUTE) + ":" + uCal.get(Calendar.SECOND), context);
        } else {
            return d[0] + " " + TimeType(d[1], context);
        }
    }

    /**
     * @param time
     * @param context
     * @return
     */
    public static String TimeType(String time, Context context) {
        SharedPref pref = new SharedPref(context, "MyPref");

        if (pref.gitShared("time").equalsIgnoreCase("12")) {

            try {

                String _24HourTime = time;
                SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat _12HourSDF = new SimpleDateFormat("HH:mm:ssa");
                Date _24HourDt = _24HourSDF.parse(_24HourTime);
                Log.d("12HourSDF", _12HourSDF.format(_24HourDt));
                return _12HourSDF.format(_24HourDt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return time;

    }

    /**
     * @param singleComments
     * @param i
     * @return
     */
    public static String getAtachmentComment(Comments singleComments, int i) {
        if (singleComments.getUser().getAttachment().size() != 0) {
            if (i <= singleComments.getUser().getAttachment().size()) {
                return singleComments.getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    /**
     * @param singleNew
     * @param i
     * @return
     */
    public static String getAtachmentUser(news singleNew, int i) {
        if (singleNew.getUser().getAttachment().size() != 0) {
            if (i <= singleNew.getUser().getAttachment().size()) {
                return singleNew.getUser().getAttachment().get(i).getPath();
            }
        }

        return "";

    }

    /**
     * @param size
     * @return
     */
    public static String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

}


