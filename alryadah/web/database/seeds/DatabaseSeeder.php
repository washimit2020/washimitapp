<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //user
        $this->call(UserRolesTableSeeder::class);
        $this->call(UserAttacmentTypesTableSeeder::class);

        //news
        $this->call(NewsMainTypesTableSeeder::class);
        $this->call(NewsSubTypesTableSeeder::class);
        $this->call(NewsStatusesTableSeeder::class);
        $this->call(NewsAttachmentTypesTableSeeder::class);

    }
}
