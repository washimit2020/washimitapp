<?php

use Illuminate\Database\Seeder;
use App\Http\Helpers\Constants;
class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            [
                "id"=>Constants::USER_ROLE_ADMIN,
                "name_en"=>"Admin",
                "name_ar"=>"مدير"
            ],
            [
                "id"=>Constants::USER_ROLE_NEWSMAN,
                "name_en"=>"Newsman",
                "name_ar"=>"صحفي"
            ],
            [
                "id"=>Constants::USER_ROLE_COSTUMER,
                "name_en"=>"Costumer",
                "name_ar"=>"مستخدم"
            ]
        ]);
    }
}
