package com.washimit.it.ryadah.registrationActivity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.ButtonView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectTheUserActivity extends AppCompatActivity implements View.OnClickListener {


    @BindView(R.id.buttonShopper)
    ButtonView buttonShopper;
    @BindView(R.id.buttonShow)
    ButtonView buttonShow;
    @BindView(R.id.buttonStore)
    ButtonView buttonStore;
    @BindView(R.id.Skip)
    TextViewCast Skip;
    SharedPref pref;
    User user;
    PackageInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_the_user);
        ButterKnife.bind(this);
        pref = new SharedPref(this);
        buttonShopper.setOnClickListener(this);
        buttonShow.setOnClickListener(this);
        buttonStore.setOnClickListener(this);
        Skip.setOnClickListener(this);
        user = pref.getUser();


        if (user != null) {

            if (null != pref.getLocal()) {
                Locale myLocale = new Locale(pref.getLocal());
                Resources res = getResources();
                DisplayMetrics dm = res.getDisplayMetrics();
                Configuration conf = res.getConfiguration();
                conf.locale = myLocale;
                res.updateConfiguration(conf, dm);
            }

            openActivity(MainActivity.class, user.getType().getId());
            finish();

        }


        try {
            info = getPackageManager().getPackageInfo("com.washimit.mohammad.ryadah", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                Log.e("hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.buttonShopper:
                openActivity(LogInActivity.class, Constant._TYPE_COSTUMER);
                break;
            case R.id.buttonShow:
                openActivity(LogInActivity.class, Constant._TYPE_SHOW);

                break;
            case R.id.buttonStore:
                openActivity(LogInActivity.class, Constant._TYPE_STORE);

                break;
            case R.id.Skip:
                openActivity(MainActivity.class, Constant._TYPE_VISITOR);
                break;

        }

    }


    public void openActivity(Class activity, int userType) {
        Intent intent = new Intent(SelectTheUserActivity.this, activity);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("userType", userType);
        startActivity(intent);

    }

    @Override
    protected void onStart() {
        super.onStart();

    }


}
