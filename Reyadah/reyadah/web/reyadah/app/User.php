<?php

namespace App;

use App\__ModelsFestival\Festival;
use App\__ModelsProducts\Product;
use App\__ModelsProducts\ShareProduct;
use App\__ModelsProducts\Wishlist;
use App\Http\Constants;
use App\__ModelsRating\PlaceRating;
use App\ManyToPlacesModels\ShowBranch;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'phone_number', 'email', 'info',
        'password', 'type_id', 'status_id', 'reg_id', 'social_media_id',
        'street_name', 'sector_name', 'lat', 'lng'
    ];

    protected $with = ["social_media","type"];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token', 'type_id', 'social_media_id', 'status_id', 'reg_id'
    ];

    /**
     * @var array
     */
    protected $appends = ['followed', 'rated','total_followers'];

    /**
     * belongs to one type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    /**
     * belongs to one status
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    /**
     * belongs to one social media
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function social_media()
    {
        return $this->belongsTo(SocialMedia::class);
    }

    /**
     * to get all the followers instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function following()
    {
        return $this->hasMany('App\ManyToPlacesModels\Follower', 'user_id');
    }

    /**
     * to get all the followers instance
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function followedBy()
    {
        return $this->hasMany('App\ManyToPlacesModels\Follower', 'place_id')->where('type_id',Constants::_TYPE_FOLLOW_USER);
    }

    /**
     * get all attachments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany(Attachment::class, 'related_id');
    }

    /**
     * has many branches
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function branch()
    {
        return $this->hasMany(ShowBranch::class, 'user_id');
    }

    /**
     * to get the ratings of the place
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(PlaceRating::class, 'related_id');
    }

    /**
     * has many blocked users
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blocked_users()
    {
        return $this->hasMany(BlockedUser::class, 'user_id');
    }

    /**
     * has many blocked by
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blocked_by()
    {
        return $this->hasMany(BlockedUser::class, 'related_id');
    }

    /**
     * to get the ratings that the user rated
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function place_rates()
    {
        return $this->hasMany(PlaceRating::class, 'user_id');
    }

    /**
     * to get the festivals that the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function festivals()
    {
        return $this->hasMany(Festival::class, 'user_id');
    }


    //products

    /**
     * has many products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'user_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wishlisted()
    {
        return $this->hasMany(Wishlist::class, 'user_id');
    }

    /**
     * has many shared products
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sharedProducts()
    {
        return $this->morphMany(ShareProduct::class, 'shareable');
    }




    /**
     * @return int
     */
    public function getFollowedAttribute()
    {
        $follow = 0;
        if (auth()->guard('api')->check()) {
            $followed = $this->followedBy()->where('user_id', auth()->guard('api')->user()->id)->where('status_id', Constants::_STATUS_ACTIVE)->first();
            if (!empty($followed)) {
                $follow = 1;
            }
        }
        return $follow;
    }

    /**
     * @return null
     */
    public function getRatedAttribute()
    {
        $rated = null;
        if (auth()->guard('api')->check()) {
            $rated = $this->ratings()->where('user_id', auth()->guard('api')->user()->id)->first();
        }
        return $rated;
    }


    /**
     * @return int
     */
    public function getTotalFollowersAttribute()
    {
        return $this->followedBy()->where('status_id',Constants::_STATUS_ACTIVE)->count();
    }


}
