<?php

namespace App\ManyToPlacesModels;

use App\__ModelsProducts\ShareProduct;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ShowBranch extends Model
{
    protected $fillable = ['street_name', 'sector_name', 'lat', 'lng', 'name', 'user_id'];

    /**
     * belongs to one show
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
     * has many shared products
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sharedProducts()
    {
        return $this->morphMany(ShareProduct::class, 'shareable');
    }
}
