//
//  ProductSearch_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/6/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class ProductSearch_CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userIcon: UIImageView!
    var product:Product?{
        didSet{
            updateView()
        }
    }
   
    func updateView(){
        guard let product = product else {
            return
        }
        productImage.set_product_image(product_id: product.id)
        price.text = set_price(product.price)
        userImage.set_avatar_image(user_id: product.user.id)
        name.text = product.name
        username.text = product.user.name
        userIcon.image = UIImage(named: Constants.icon_name[product.user.type.id  - 1])
    }
}

