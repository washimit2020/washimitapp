<?php

namespace App\Http\Requests;

use App\Http\Helpers\Constants;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|max:255',
            'last_name'=>'required|max:255',
            'nick_name'=>'max:255',
            'password'=>'required|max:60|min:6',
            'profile_image'=>'mimes:png,jpeg|max:512',
            'new_password'=>'max:60|min:6',
        ];
    }
}
