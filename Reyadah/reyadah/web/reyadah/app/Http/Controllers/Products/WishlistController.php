<?php

namespace App\Http\Controllers\Products;

use App\__ModelsProducts\Product;
use App\__ModelsProducts\Wishlist;
use App\Http\Constants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WishlistController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id,status_id,' . Constants::_STATUS_ACTIVE
        ]);


        abort_if_blocked(Product::find( $request->product_id)->user);

        $wishlist = Wishlist::where([
            ['product_id', $request->product_id],
            ['user_id', $request->user()->id],
        ])->first();
        $wishlisted = true;
        if (isset($wishlist)) {
            $status = $wishlist->status_id == Constants::_STATUS_ACTIVE ? Constants::_STATUS_DELETED : Constants::_STATUS_ACTIVE;
            $wishlist->status_id = $status;
            $wishlist->update();
            if ($status == Constants::_STATUS_DELETED) {
                $wishlisted = false;
            }
        } else {
            $wishlist = new Wishlist();
            $wishlist->product_id = $request->product_id;
            $wishlist->user_id = $request->user()->id;
            $wishlist->save();
        }

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["wishlisted" => $wishlisted]));
    }

    public function index(){
        $wishlisted = Product::where("status_id",Constants::_STATUS_ACTIVE)->whereHas('wishlisted',function ($query){
            $query->where([
                ['user_id',auth()->user()->id],
                ['status_id',Constants::_STATUS_ACTIVE],
            ]);
        })->whereHas('user',function ($query){
            check_if_blocked($query);
        })->paginate(Constants::PAGINATION);

        return response()->json(response_json(Constants::RESPONSE_SUCCESS, ["wishlisted" => $wishlisted]));
    }
}
