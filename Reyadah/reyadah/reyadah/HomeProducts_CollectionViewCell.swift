//
//  HomeProducts_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/16/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON

class HomeProducts_CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var sub_username: UILabel!
    @IBOutlet weak var userTypeIcon: UIImageView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var avaterImage: BorderedImageView!
    
    @IBOutlet weak var latestCommentsHolder: UIStackView!
    
    
    @IBOutlet weak var loadMore: AspectFitButton!
    
    @IBOutlet weak var firstComment: UILabel!
    @IBOutlet weak var firstUser: UILabel!
    @IBOutlet weak var secondUser: UILabel!
    @IBOutlet weak var secondComment: UILabel!
    
    @IBOutlet weak var numberOfLikes: UILabel!
    
    @IBOutlet weak var numberOfEatersText: UILabel!
    
    @IBOutlet weak var share: AspectFitButton!
    @IBOutlet weak var wishlist: AspectFitButton!
    @IBOutlet weak var likeButton: AspectFitButton!
    @IBOutlet weak var location: AspectFitButton!
    
    @IBOutlet weak var comment: AspectFitButton!
    
    var apiRequest:ApiRequest?
    
    var product:Product?{
        didSet{
            updateView()
        }
    }
    
    
    
    func clearComments(){
        firstComment.text = ""
        firstUser.text = ""
        secondUser.text = ""
        secondComment.text = ""
    }
    
    private func updateView(){
        guard let product  = self.product else{
            return
        }
        var user = product.user!
        
        if let shared_with = product.shared_with {
            if shared_with.id != 0{
                user = shared_with
                sub_username.isHidden = false
                sub_username.text = product.user!.name!
            }else{
                sub_username.isHidden = true
            }
        }
        
        productImage.set_product_image(product_id: product.id)
        
        avaterImage.set_avatar_image(user_id: user.id)
   
        username.text = user.name

        
        if !product.is_sponsored{
            
            if Gate.is_authed(){
                wishlist.tag = 1
                likeButton.tag = 2
                wishlist.addTarget(self, action: #selector(likeOrWishlist(_:)), for: .touchUpInside)
                likeButton.addTarget(self, action: #selector(likeOrWishlist(_:)), for: .touchUpInside)
            }
            clearComments()

            
            numberOfEatersText.text = product.numberOfEaters
            numberOfLikes.text = set_likes(product.total_likes)
            productName.text = product.name
            userTypeIcon.image = UIImage(named:Constants.icon_name[user.type.id - 1])
            change_like_wishlist()
            product.comments.reverse()
            if product.comments!.count > 0 {
                latestCommentsHolder.isHidden = false
                firstUser.text = product.comments[0].user?.name
                firstComment.text = product.comments[0].body
                
                if(product.comments.count > 1){
                    secondUser.text = product.comments[1].user?.name
                    secondComment.text = product.comments[1].body
                }
            }
        }
    }
    
    
    public func likeOrWishlist(_ sender:AspectFitButton){
        guard let product = self.product else {
            return
        }
        apiRequest?.likeOrWishlist(sender, product , numberOfLikes)
    }
    
    public func change_like_wishlist(){
        change_tint_of_button(button: likeButton, checked: product!.is_liked)
        change_tint_of_button(button: wishlist, checked: product!.is_wishlisted)
    }
    
    
}
