//
//  LocationsTabBar_ViewController.swift
//  reyadah
//
//  Created by WASHM on 5/22/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

class LocationsTabBar_ViewController: UITabBarController {
    
    var set_selectedViewController:Int?{
        didSet{
            if set_selectedViewController! > 1{
                set_selectedViewController = 1
            }else if set_selectedViewController! < 0 {
                set_selectedViewController = 0
            }
            selectedViewController = viewControllers![set_selectedViewController!]
            fillData()
        }
    }
    
    var apiRequest:ApiRequest?
    var product:Product?
    var shared = [Branch]()
    var get_branches = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = " "

        tabBar.isHidden = true
        
        
        guard  let apiRequest = apiRequest else {
            return
        }
        var link = ""
        if get_branches {
            link = RequestLinks.create_branch.rawValue
        }
        
        if let product = product  {
            var type = Constants.USER_SHARED
            if product.user.type.id == Constants.TYPE_SHOW{
                type = Constants.BRANCH_SHARED
            }
            link = RequestLinks.productShares.rawValue + "\(product.id!)/\(type)"
            shared.append(product.user)
        }
       
        apiRequest.make_request(link: link, method: .get, completion: {
            $0?.responseJSON(completionHandler: {
                let json = JSON($0.data)
                var beginWith = "shares"
                
                if self.get_branches {
                    beginWith = "branches"
                }
                
                guard let msg = json["msg"].dictionary , let shares = msg[beginWith]?.array , shares.count > 0  else{
                    return
                }
                
                if self.get_branches {
                    for share in shares{
                        let branch = Branch(share)
                        branch.rating = self.shared[0].rating
                        self.shared.append(branch)
                    }
                }else{
                    for share in shares{
                        let shared = Shared(share)
                        if shared.shareable_type == Constants.BRANCH_SHARED{
                            shared.shareable.rating = self.shared[0].rating
                        }
                        self.shared.append(shared.shareable)
                    }
                }
                
                self.fillData()
            })
        })
    }
    
    
    
    func fillData() {
        if let map = selectedViewController as? UsersLocationsMap_ViewController{
            map.items = self.shared
            map.updateView()
        }
        
        if let table = selectedViewController as? UsersLocations_TableViewController{
            table.items = self.shared
            table.tableView.reloadData()
        }
    }
}


