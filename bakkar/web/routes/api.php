<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->group(["middleware" => "app-key"], function () use ($app) {
    $app->post('register', 'UsersController@register');
    $app->post('login', 'UsersController@login');


	$app->get('posts/{type}[/{search}]', 'PostsController@index');
	$app->get('categories', 'CategoryController@index');

    $app->get('comment/{post_id}', 'CommentController@index');


    $app->group(['middleware' => 'auth'], function () use ($app) {
        $app->post('reg_id/update', 'UsersController@registration_id_updater');
        $app->post('notifications/read', 'NotificationsController@read');

        $app->get('wishlist', 'WishlistController@index');
        $app->post('wishlist', 'WishlistController@store');


        $app->post('comment', 'CommentController@store');
        $app->post('comment/delete', 'CommentController@delete');

        $app->post('rate', 'PostsController@rate');

        $app->group(['middleware' => 'admin'], function () use ($app) {
	        $app->post('posts','PostsController@store');
	        $app->put('posts', 'PostsController@update');

	        $app->post('categories','CategoryController@store');
        });

    });
});


$app->get('attachments/{id}', 'PostsController@attachment');
