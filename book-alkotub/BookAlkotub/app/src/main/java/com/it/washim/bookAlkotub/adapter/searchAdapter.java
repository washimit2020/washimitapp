package com.it.washim.bookAlkotub.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.it.washim.bookAlkotub.R;
import com.it.washim.bookAlkotub.helper.CustomTypefaceSpan;
import com.it.washim.bookAlkotub.interfac.pageIdCallback;
import com.it.washim.bookAlkotub.model.searchResult;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by washm on 4/4/17.
 */

public class searchAdapter extends RecyclerView.Adapter {

    pageIdCallback collback;
    List<searchResult> list;
    Activity context;

    public searchAdapter(Activity context, List<searchResult> list) {
        this.list = list;
        this.context = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new WishilistedHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final searchResult get = (searchResult) list.get(position);
        if (holder instanceof WishilistedHolder) {


            ((WishilistedHolder) holder).title.setText(replace(new String[]{"--(.+?)--", "__(.+?)__", "sm(.+?)sm", "=(.+?)="}, new String[]{"--", "__", "sm", "="}, get.getBady()));
            ((WishilistedHolder) holder).pageNumber.setText(String.valueOf(get.getPageId()));

            ((WishilistedHolder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    collback.pageId(get.getPageId());
                }
            });


        }


    }


    public void onpageIdCollback(pageIdCallback collback) {
        this.collback = collback;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class WishilistedHolder extends RecyclerView.ViewHolder {
        TextView title, pageNumber;

        public WishilistedHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            pageNumber = (TextView) itemView.findViewById(R.id.page_namber);


        }
    }


    public SpannableStringBuilder replace(String[] p, String[] r, String text) {

        SpannableStringBuilder SS = null;
        String body = text;

        final Pattern pattern1 = Pattern.compile(p[0]);
        final Matcher matcher1 = pattern1.matcher(body);

        final Pattern pattern2 = Pattern.compile(p[1]);
        final Matcher matcher2 = pattern2.matcher(body);


        final Pattern pattern3 = Pattern.compile(p[2]);
        final Matcher matcher3 = pattern3.matcher(body);

        final Pattern pattern4 = Pattern.compile(p[3]);
        final Matcher matcher4 = pattern4.matcher(body);

        int x = 0;

        body = body.replaceAll(r[0], "  ");
        body = body.replaceAll(r[1], "  ");
        body = body.replaceAll(r[2], "  ");
        body = body.replaceAll(r[3], " ");
        Matcher matcher = null;
        Typeface font = null;
        for (int i = 0; i < p.length; i++) {

            if (i == 0) {
                matcher = matcher1;

            } else if (i == 1) {
                matcher = matcher2;

            } else if (i == 2) {
                matcher = matcher3;

            } else if (i == 3) {
                matcher = matcher4;

            }
            try {

                if (SS == null) {
                    SS = new SpannableStringBuilder(body);
                }

                while (matcher.find()) {



                    if (i == 0) {
                        Typeface tf = Typeface.createFromAsset(context.getAssets(), "uthmani.otf");

                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    } else if (i == 1) {
                        Typeface tf = Typeface.createFromAsset(context.getAssets(), "LotusLinotype-Light.otf");
                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    } else if (i == 3) {
                        Typeface tf = Typeface.createFromAsset(context.getAssets(), "LotusLinotype-Bold.otf");
                        SS.setSpan(new CustomTypefaceSpan("", tf), matcher.start(), matcher.end(), 0);
                        SS.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.colorPrimary)), matcher.start(), matcher.end(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    }

//                    x=x+4;
                }

            } catch (Exception e) {

                break;
            }

        }

        return SS;


    }


}
