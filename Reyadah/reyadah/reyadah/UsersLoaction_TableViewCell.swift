//
//  UsersLoaction_TableViewCell.swift
//  reyadah
//
//  Created by WASHM on 5/24/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class UsersLoaction_TableViewCell: UITableViewCell {

    @IBOutlet weak var user_name: UILabel!
    @IBOutlet weak var avatar: BorderedImageView!
    @IBOutlet weak var locaitonText: UILabel!
    @IBOutlet weak var ratingStars: CosmosView!
    
    var branch:Branch?{
        didSet{
            guard let branch = branch else {
                return
            }
            user_name.text = branch.name
            if branch.user_id != 0{
                avatar.set_avatar_image(user_id: branch.user_id)
            }else{
                avatar.set_avatar_image(user_id: branch.id)

            }
            locaitonText.text = make_location_text(branch)
            ratingStars.rating = branch.rating
           
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup_cosmos(cosmos: ratingStars)
    }
   
    

}
