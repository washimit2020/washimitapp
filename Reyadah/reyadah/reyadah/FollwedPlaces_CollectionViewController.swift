//
//  FollwedPlaces_CollectionViewController.swift
//  reyadah
//
//  Created by WASHM on 5/20/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftyJSON

class FollwedPlaces_CollectionViewController: ItemsLoader_CollectionViewController {
    
    let reuseIdentifier = "profilePlace"
    
    var type:Int! = Constants.TYPE_STORE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        link = RequestLinks.following.rawValue + "\(type!)"
        beginWithModel = "users"
        
        self.collectionView!.register(UINib(nibName: "ProfileUsers_CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        clearData()
        self.emptyLabel?.frame.origin.y = collectionView!.frame.height/4
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using [segue destinationViewController].
     // Pass the selected object to the new view controller.
     }
     */
    
    // MARK: UICollectionViewDataSource
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProfileUsers_CollectionViewCell
        cell.user = items[indexPath.row] as? User
        cell.unfollow.addTarget(self, action: #selector(unfollow(_:)), for: .touchUpInside)
        cell.locationButton.addTarget(self, action: #selector(go_to_locations(_:)), for: .touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:self.collectionView!.frame.width, height: 100)
    }
}

extension FollwedPlaces_CollectionViewController:ItemsLoaderDelegate{
    func dataFiller(data: [JSON]) {
        for item in data{
            items.append(Branch(item))
        }
    }
    
    func unfollow(_ sender:AspectFitButton){
        sender.isEnabled = false
        
        let point = sender.convert(CGPoint.zero, to: collectionView)
        
        guard let index = collectionView!.indexPathForItem(at: point), let user = items[index.row] as? User else{
            return
        }
        apiRequest.call_follow_unfollow(user.id!)

        collectionView?.performBatchUpdates({
            self.items.remove(at: index.row)
            self.collectionView?.deleteItems(at: [index])
        }, completion: {
            if($0 && self.items.count == 0){
                self.showEmpty()
            }
        })
    }
    
    func go_to_locations(_ sender:UIButton) {
        let point = sender.convert(CGPoint.zero, to: collectionView)
        guard let index = collectionView?.indexPathForItem(at: point) else{
            return
        }
        selectedIndexPath = index
        perform_locations_segue(ProductOrUser: items[index.row], ViewController: self, apiRequest)
    }
}
