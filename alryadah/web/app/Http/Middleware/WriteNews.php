<?php

namespace App\Http\Middleware;

use App\Http\Helpers\Constants;
use Closure;

class WriteNews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->cannot(Constants::WRITE_NEWS)){
            abort(403);
        }
        return $next($request);
    }
}
