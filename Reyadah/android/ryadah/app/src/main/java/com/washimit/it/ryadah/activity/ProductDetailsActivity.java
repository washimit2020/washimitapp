package com.washimit.it.ryadah.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.DetailsPageAdapter;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.product.Product;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.activity_product_details_recycler_details)
    RecyclerView recycler_details;
    @BindView(R.id.activity_details_product_name)
    TextViewCast activity_details_product_name;
    @BindView(R.id.activity_details_user_name)
    TextViewCast activity_details_user_name;
    @BindView(R.id.activity_product_details_add_comment)
    ImageView activity_details_add_comment;
    @BindView(R.id.activity_details_user_icon)
    ImageView activity_details_user_icon;
    DetailsPageAdapter adapter;
    ArrayList<Object> detailsItem;
    int itemNumber = 1;
    View header;
    String data;
    Object headerData;
    Gson gson;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        data = bundle.getString("data");


        headerData = gson.fromJson(data, Product.class);


        header = getLayoutInflater().inflate(R.layout.item_details_header, null);

        detailsItem = new ArrayList<>();

        adapter = new DetailsPageAdapter(detailsItem, this, recycler_details);

        adapter.setHeader(header);

        recycler_details.setHasFixedSize(true);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, itemNumber);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return itemNumber;
                }
                return 1;

            }
        });

        recycler_details.setLayoutManager(gridLayoutManager);

        recycler_details.setAdapter(adapter);

        updateUi();
        activity_details_add_comment.setOnClickListener(this);


    }

    public void updateUi() {

        if (((Product) headerData).getName() != null) {
            activity_details_user_name.setText(((Product) headerData).getUser().getName());
            activity_details_product_name.setText(((Product) headerData).getName());

            detailsItem.add(headerData);


            if ((((Product) headerData).getComments().size() > 0)) {
                detailsItem.add(1,(((Product) headerData).getComments().get(0)));
            }
            if ((((Product) headerData).getComments().size() > 1)) {
                detailsItem.add(1,(((Product) headerData).getComments().get(1)));
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_product_details_add_comment:
                intent = new Intent(this, CommentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("Id", ((Product) headerData).getId());
                intent.putExtra("type", Constant._TYPE_PRODUCT_COMMENT);
                startActivity(intent);
                break;
        }
    }
}
