<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Category;
use App\Events\PostCreated;
use App\Http\Constants;
use App\Post;
use App\Posts_Rating;
use Illuminate\Container\Container;
use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Factory as FilesystemFactory;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CategoryController extends Controller
{
    public function index()
    {

        return response()->json(["categories" => Category::paginate(100)]);
    }

    public function store(Request $request)
    {
        $rules = [
            "name" => "required|string|max:255|unique:categories,name",
        ];

        $this->validate($request, $rules);

        $data = $request->only(array_keys($rules));


        return response()->json(['category' =>  Category::create($data)]);
    }

}
