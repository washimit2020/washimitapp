//
//  AddFestival_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 5/28/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import MapKit
import Alamofire
class AddFestival_TableViewController: FormsTVC {

    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var nameError: UILabel!
    @IBOutlet weak var info: UITextView!
    @IBOutlet weak var infoError: UILabel!
    @IBOutlet weak var organization: UITextField!
    @IBOutlet weak var organizationError: UILabel!
    @IBOutlet weak var source: UITextField!
    @IBOutlet weak var sourceError: UILabel!
    @IBOutlet weak var start_in: UITextField!
    @IBOutlet weak var start_inError: UILabel!
    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var fromError: UILabel!
    @IBOutlet weak var to: UITextField!
    @IBOutlet weak var toError: UILabel!
    @IBOutlet weak var streetName: UITextField!
    @IBOutlet weak var streetNameError: UILabel!
    @IBOutlet weak var sectorName: UITextField!
    @IBOutlet weak var sectorNameError: UILabel!
    @IBOutlet weak var organizer_name: UITextField!
    @IBOutlet weak var organizer_nameError: UILabel!
    @IBOutlet weak var organizer_phone_number: UITextField!
    @IBOutlet weak var organizer_phone_numberError: UILabel!
    var apiImages = [String:UIImage]()

    var currentTextField:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerFields()
        
        start_in.delegate = self
        
    }
    
    
    
    private func registerFields(){
        fields = [
            (name, nameError),(info,infoError),(organization,organizationError),
            (source, sourceError),(start_in,start_inError),(from,fromError),
            (to, toError),(streetName,streetNameError),(sectorName,sectorNameError),
            (organizer_name, organizer_nameError),(organizer_phone_number,organizer_phone_numberError)
        ]
        
        validator.registerField(name, errorLabel: nameError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 191)])
        validator.registerField(info, errorLabel: infoError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 400)])
        validator.registerField(organization, errorLabel: organizationError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 191)])
        validator.registerField(source, errorLabel: sourceError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 191)])
        validator.registerField(streetName, errorLabel: streetNameError, rules: [RequiredRule(message: Constants.requiredErrorMsg) ,MaxLengthRule(length: 191)])
        validator.registerField(sectorName, errorLabel: sectorNameError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 191)])
        
        validator.registerField(organizer_name, errorLabel: organizer_nameError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 191)])
        validator.registerField(organizer_phone_number, errorLabel: organizer_phone_numberError, rules: [RequiredRule(message: Constants.requiredErrorMsg), PhoneNumberRule(message: Constants.phoneErrorMsg)])
        
        validator.registerField(start_in, errorLabel: start_inError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        validator.registerField(from, errorLabel: fromError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        validator.registerField(to, errorLabel: toError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
   
        
        action = create(_:)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  let controller = segue.destination as? AddLocation_ViewController {
            if let lattiude = postValues["lat"],
                let long = postValues["lng"],
                let lat = CLLocationDegrees(lattiude) ,
                let lng  = CLLocationDegrees(long ){
                let coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lng)
                controller.current = coordinate
            }
            controller.userLocationCallBack = {
                annotaiton in
                if let annotation = annotaiton{
                    self.postValues["lat"] = "\(annotation.coordinate.latitude)"
                    self.postValues["lng"] = "\(annotation.coordinate.longitude)"
                }
            }
        }
    }
   

    @IBAction func showDialog(_ sender: AspectFitButton) {
        let alert = UIAlertController(title: "قم بادخال الرابط او اسم المستخدم", message: nil, preferredStyle: .alert)
        alert.addTextField(configurationHandler: {
            $0.text = self.postValues[sender.restorationIdentifier!]
        })
        alert.addAction(.init(title: "تم", style: .default, handler: {
            _ in
            self.postValues[sender.restorationIdentifier!] = alert.textFields![0].text!
        }))
        alert.addAction(.init(title: "الغاء", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: Constants.image_adder_id) as! ImageAdder_ViewController
        controller.hideAvatar = true
        for (_,value) in self.apiImages{
            controller.items.append(value)
        }
        
        controller.getImagesCallback = {
            (avatar , covers) in
            
            if let covers = covers {
                for i in 0...covers.count-1{
                    self.apiImages["images[\(i)]"] = covers[i]
                }
            }
        }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func create(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
    private func handler(_ response:DataRequest?)->Void{
        
        guard let response = response else {
            self.request.handle_error_dialog(nil)
            return
        }
        
        response.responseJSON{
            response in
            
            if response.response?.statusCode == 422{
                self.request.handle_error_dialog("تاكد من صحة البيانات ")
            }
                
            else if response.response?.statusCode == 200{
                self.request.costumeAlert.closeButton.isHidden = false
                self.request.costumeAlert.loader.stopAnimating()
                self.request.costumeAlert.dismiss(animated: true, completion: {
                    self.navigationController?.popViewController(animated: true)
                })
            }else{
                self.request.handle_error_dialog(nil)
            }
        }
        
    }
    
    override func validationSuccessful() {
        postValues["info"] = info.text
        postValues["name"] = name.text
        postValues["organization"] = organization.text
        postValues["source"] = source.text
        postValues["start_in"] = start_in.text
        postValues["from"] = from.text
        postValues["to"] = to.text
        postValues["street_name"] = streetName.text
        postValues["sector_name"] = sectorName.text
        postValues["organizer_name"] = organizer_name.text
        postValues["organizer_phone_number"] = organizer_phone_number.text
    
        request.make_request(parameters: postValues,
                             apiImages: apiImages, link: RequestLinks.festivals.rawValue,
                             method: .post,
                             dialogMsg: "جاري انشاء الطلب",
                             completion: handler)
        
    }
}

extension AddFestival_TableViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField != start_in && textField != to && textField != from{
            return
        }
        let datePicker = UIDatePicker()
        if textField == start_in{
            datePicker.datePickerMode = .date
            datePicker.minimumDate = Date()
        }else{
            datePicker.datePickerMode = .time           
        }
        
        currentTextField = textField
        
        datePicker.locale = Locale(identifier: "ar")
        
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: .valueChanged)
        textField.inputView = datePicker
        textField.inputView?.backgroundColor = UIColor.white
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        guard let  currentTextField = currentTextField else {
            return
        }
        
        let dateFormatter = DateFormatter()
        
       
        if currentTextField == start_in {
            dateFormatter.dateFormat = "yyyy-MM-dd"

        }else{
            dateFormatter.dateFormat = "HH:mm"
        }
        
        currentTextField.text = dateFormatter.string(from: sender.date)
    }
}

