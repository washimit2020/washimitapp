//
//  WishlistCell_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/26/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit

class WishlistCell_CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userIcon: UIImageView!
    @IBOutlet weak var productImage: BorderedImageView!
   
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var ratingStar: CosmosView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var wishlist: UIImageView!
    
    @IBOutlet weak var editButton: AspectFitButton!
    override func awakeFromNib() {
        setup_cosmos(cosmos: ratingStar)
    }
    var product:Product?{
        didSet{
            updateView()
        }
    }
    
    func updateView(){
        guard let product = product else {
            return
        }
        
        productImage.set_product_image(product_id: product.id)
        productName.text = product.name
        ratingStar.rating = product.rating

        if let userIcon = userIcon{
            userIcon.image = UIImage(named:Constants.icon_name[product.user.type.id - 1])
            username.text = product.user.name
        }
      
    }
}
