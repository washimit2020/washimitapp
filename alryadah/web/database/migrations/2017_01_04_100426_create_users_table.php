<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nick_name')->nullable();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('gender');
            $table->string('api_token')->nullable();
            $table->string('reg_id')->nullable();
            $table->string('preferred_language',3)->default('ar');
            $table->unsignedInteger('user_role_id')->index();
            $table->timestamps();

            $table->foreign('user_role_id')->references('id')->on('user_roles')->onDelete('restrict')->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
