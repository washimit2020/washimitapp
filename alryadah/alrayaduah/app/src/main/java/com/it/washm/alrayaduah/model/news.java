package com.it.washm.alrayaduah.model;

import java.util.ArrayList;

/**
 * Created by washm on 1/9/17.
 */

public class news {
    String id;
    String title;
    String content;
    String disapprove_description;
    String news_status_id;
    String updated_at;
    String views_count;
    String comments_count;
    String likes_count;
    NewsType type;
    Status status;
    User user;
    ArrayList<Attachment> attachment;

    public news(String id, String title, String content, String disapprove_description, String news_status_id, String updated_at, String views_count, String comments_count, String likes_count, NewsType type, Status status, User user, ArrayList<Attachment> attachment) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.disapprove_description = disapprove_description;
        this.news_status_id = news_status_id;
        this.updated_at = updated_at;
        this.views_count = views_count;
        this.comments_count = comments_count;
        this.likes_count = likes_count;
        this.type = type;
        this.status = status;
        this.user = user;
        this.attachment = attachment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDisapprove_description() {
        return disapprove_description;
    }

    public void setDisapprove_description(String disapprove_description) {
        this.disapprove_description = disapprove_description;
    }

    public String getNews_status_id() {
        return news_status_id;
    }

    public void setNews_status_id(String news_status_id) {
        this.news_status_id = news_status_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getViews_count() {
        return views_count;
    }

    public void setViews_count(String views_count) {
        this.views_count = views_count;
    }

    public String getComments_count() {
        return comments_count;
    }

    public void setComments_count(String comments_count) {
        this.comments_count = comments_count;
    }

    public String getLikes_count() {
        return likes_count;
    }

    public void setLikes_count(String likes_count) {
        this.likes_count = likes_count;
    }

    public NewsType getType() {
        return type;
    }

    public void setType(NewsType type) {
        this.type = type;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Attachment> getAttachment() {
        return attachment;
    }

    public void setAttachment(ArrayList<Attachment> attachment) {
        this.attachment = attachment;
    }



}