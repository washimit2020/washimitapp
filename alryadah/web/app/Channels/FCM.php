<?php


namespace App\Channels;

use Illuminate\Notifications\Notification;

class FCM
{
    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $message = $notification->toFCM($notifiable,$notification);

        $message->add_reg("ft49qo5AKGI:APA91bFTK1mWVO24sW6l74Rq_DeHW_zQDpenABloJJwsjNlLFRN_YelBeHHr5Ttq0fgwLhllrRuuJchSInuxUBhoJtKcdTCMQmwXDliiWDi_vvjtRN1zfxVzYWyKdi2k3f-AVnrS1KSf");
       $data = $message->msg_data();
       $data['title'] =  config('app.name','Al ra\'adyah');
        $fields = array(
            'to' =>  $notifiable->reg_id,
            'data' => $data,
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $message->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $message->headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields,JSON_PRETTY_PRINT));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }
}