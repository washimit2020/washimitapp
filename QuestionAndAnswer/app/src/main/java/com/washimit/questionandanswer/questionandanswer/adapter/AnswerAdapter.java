package com.washimit.questionandanswer.questionandanswer.adapter;

import android.app.Activity;
import android.os.Build;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.washimit.questionandanswer.questionandanswer.Halper.Conestant;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.custumView.MyTextView;
import com.washimit.questionandanswer.questionandanswer.interfac.AnswerCollbackLisenar;
import com.washimit.questionandanswer.questionandanswer.modle.Answers;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by mohammad on 7/9/2017.
 */

public class AnswerAdapter extends RecyclerView.Adapter {
    ArrayList<Answers> answerses;
    Activity activity;
    AnswerCollbackLisenar lisenar;
    Animation animation, answer;
    Vibrator vibrate;
    public boolean isClickable = true;

    public AnswerAdapter(Activity activity, ArrayList<Answers> answerses) {
        this.activity = activity;
        this.answerses = answerses;

        animation = AnimationUtils.loadAnimation(activity,
                R.anim.myanimation);

        answer = AnimationUtils.loadAnimation(activity,
                R.anim.answer);


        vibrate = (Vibrator) activity.getSystemService(activity.VIBRATOR_SERVICE);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new AnswerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer, parent, false));
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof AnswerViewHolder) {

            final Answers answers = answerses.get(position);


            ((AnswerViewHolder) holder).image_title.setVisibility(View.GONE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ((AnswerViewHolder) holder).answer.setBackground(activity.getResources().getDrawable(R.drawable.answer_bac));
            }
            ((AnswerViewHolder) holder).answer_number.setTextColor(activity.getResources().getColor(R.color.black));
            ((AnswerViewHolder) holder).answer_text.setTextColor(activity.getResources().getColor(R.color.black));

            for (int i = 0; i < answers.attachments.size(); i++) {
                if (answers.attachments.get(i).attachments__type_id == Conestant.ANSWER_TITLE) {
                    //      ((AnswerViewHolder) holder).image_title.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_menu_camera));
                    Picasso.with(activity).load(ApiRequest.URL_ANSWER + answers.id + "/" + answers.attachments.get(i).attachments__type_id).into(((AnswerViewHolder) holder).image_title);

                    ((AnswerViewHolder) holder).image_title.setVisibility(View.VISIBLE);

                }


            }
            isClickable = true;

            if (answers.select) {
                Log.d("dssgdg", answers.select + "");
                isClickable = false;
                lisenar.onClack(position);
                ((AnswerViewHolder) holder).answer_number.setTextColor(activity.getResources().getColor(R.color.white));
                ((AnswerViewHolder) holder).answer_text.setTextColor(activity.getResources().getColor(R.color.white));

                ((AnswerViewHolder) holder).answer.startAnimation(animation);
                ((AnswerViewHolder) holder).answer.setBackground(activity.getResources().getDrawable(R.drawable.answer_green_bac));
            }


            ((AnswerViewHolder) holder).answer_number.setText((position + 1) + "");
            ((AnswerViewHolder) holder).answer_text.setText(answers.title);

            ((AnswerViewHolder) holder).answer.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                @Override
                public void onClick(View v) {

                    if (isClickable) {
                        ((AnswerViewHolder) holder).answer_number.setTextColor(activity.getResources().getColor(R.color.white));
                        ((AnswerViewHolder) holder).answer_text.setTextColor(activity.getResources().getColor(R.color.white));


                        if (answers.is_correct == 1) {
                            isClickable = false;
                            lisenar.onClack(position);
                            ((AnswerViewHolder) holder).answer.startAnimation(animation);
                            ((AnswerViewHolder) holder).answer.setBackground(activity.getResources().getDrawable(R.drawable.answer_green_bac));

                        } else {
                            ((AnswerViewHolder) holder).answer.startAnimation(answer);
                            //   Toast.makeText(activity, "خطأ", Toast.LENGTH_SHORT).show();
                            ((AnswerViewHolder) holder).answer.setBackground(activity.getResources().getDrawable(R.drawable.answer_red_bac));
                            vibrate.vibrate(280);

                            for (int i = 0; i < answerses.size(); i++) {
                                if (answerses.get(i).is_correct == 1) {
                                    answerses.get(i).select = true;
                                    Log.d("dssgdg", answerses.get(i).select + " " + i);
                                    notifyItemChanged(i);
                                }
                            }

                        }
                    }
                }
            });


        }


    }


    public class AnswerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.answer)
        LinearLayout answer;
        @BindView(R.id.image_title)
        ImageView image_title;
        @BindView(R.id.answer_text)
        MyTextView answer_text;
        @BindView(R.id.answer_number)
        MyTextView answer_number;

        public AnswerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @Override
    public int getItemCount() {
        return answerses.size();
    }


    public void onAnswerClickLisenar(AnswerCollbackLisenar lisenar) {
        this.lisenar = lisenar;
    }
}
