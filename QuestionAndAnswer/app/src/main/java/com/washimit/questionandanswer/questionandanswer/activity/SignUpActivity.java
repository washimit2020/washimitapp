package com.washimit.questionandanswer.questionandanswer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.washimit.questionandanswer.questionandanswer.Halper.PreferenceClass;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.MyButtonView;
import com.washimit.questionandanswer.questionandanswer.custumView.MyEditText;
import com.washimit.questionandanswer.questionandanswer.requstes.ApiRequest;
import com.washimit.questionandanswer.questionandanswer.requstes.RequestCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SignUpActivity extends BaseActivity implements View.OnClickListener {
    ApiRequest request;
    @BindView(R.id.name_sign_up)
    MyEditText name;
    @BindView(R.id.email_sign_up)
    MyEditText email;
    @BindView(R.id.password_sign_up)
    MyEditText password;
    @BindView(R.id.sign_up)
    MyButtonView sign_up;
    PreferenceClass pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        request = new ApiRequest(this);
        pref = new PreferenceClass(this);
        sign_up.setOnClickListener(this);

    }

    // android studio hi my brother
    //im fine thank you
    //wow  good
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_up:

                signUp();


                break;
        }
    }


    public void signUp() {


        if (validation()) {
            HashMap<String, String> map = new HashMap<>();
            map.put("name", name.getText().toString());
            map.put("email", email.getText().toString());
            map.put("password", password.getText().toString());
            showDialog();

            request.setRequst("register", map, new RequestCallback() {
                @Override
                public void onResponse(String response) throws JSONException {
                    Log.d("response", response);
                    Intent intent = new Intent(getApplicationContext(), StartActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    JSONObject object = null;
                    object = new JSONObject(response);
                    //JSONObject msg = object.getJSONObject("msg");
                    pref.setUser(object.getString("user"));
                    hideDialog();


                }

                @Override
                public void ErrorListener() {
                    Log.d("ErrorListener", "ErrorListener");
                    hideDialog();
                    Toast.makeText(getApplicationContext(), "الايمل مسجل من قبل او ان هناك مشكلة بالاتصال", Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


    public boolean validation() {
        if (TextUtils.isEmpty(name.getText().toString())) {
            name.setError("يجب ادخال الاسم");
            name.requestFocus();
            return false;

        }


        if (!email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            email.setError("يجب ادخال ايميل صحيح");
            email.requestFocus();
            return false;

        }


        if (TextUtils.isEmpty(password.getText().toString())) {
            password.setError("يجب ادخال كلمة السر");
            password.requestFocus();
            return false;

        }

        return true;

    }
}
