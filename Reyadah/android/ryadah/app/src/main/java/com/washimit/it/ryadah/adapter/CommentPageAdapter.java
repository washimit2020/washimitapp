package com.washimit.it.ryadah.adapter;

/**
 * Created by mohammad on 4/16/2017.
 */

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.Interface.MultipartCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.details.Comment;
import com.washimit.it.ryadah.viewCasting.EditTextView;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentPageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_PROG = 0;
    public final int COMMENT = 1;


    List<Comment> list;
    Activity context;
    ApiRequest request;
    Gson gson;
    String url;
    SharedPref pref;
    HashMap<String, String> prams;

    public CommentPageAdapter(List<Comment> list, Activity context) {
        this.list = list;
        this.context = context;
        gson = new Gson();
        request = new ApiRequest(context);
        pref = new SharedPref(context);
        prams = new HashMap<>();


    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int i) {


        if (viewHolder instanceof CommentHolder) {


            Comment comment = (Comment) list.get(i);

            ((CommentHolder) viewHolder).item_comment_name.setText(comment.getUser().getName());
            ((CommentHolder) viewHolder).item_comment_text.setText(comment.getBody());

            Picasso.with(context)
                    .load(Constant._AVATAR_URL + comment.getUser().getId())
                    .placeholder(R.drawable.ic_profile_picture)
                    .fit()
                    .centerCrop()
                    .into(((CommentHolder) viewHolder).item_comment_user_img);


        } else {
            ((ProgressViewHolder) viewHolder).cardView.setVisibility(View.VISIBLE);
            ((ProgressViewHolder) viewHolder).avi.setVisibility(View.GONE);
            ((ProgressViewHolder) viewHolder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    getNextComment(url);

                    ((ProgressViewHolder) viewHolder).cardView.setVisibility(View.GONE);
                    ((ProgressViewHolder) viewHolder).avi.setVisibility(View.VISIBLE);
                    ((ProgressViewHolder) viewHolder).avi.show();

                }
            });

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {

        if (i == COMMENT) {
            return new CommentHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_comment, viewGroup, false));
        } else {

            return new ProgressViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(
                    R.layout.item_see_more, viewGroup, false));

        }
    }


    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (list.get(position) == null) {
            return VIEW_PROG;
        } else if (list.get(position) instanceof Comment) {
            return COMMENT;
        }
        return 0;

    }


    public void getNextComment(String urls) {

        request.MultipartRequest(Request.Method.GET, true, urls, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                try {
                    list.remove(0);
                    notifyItemRemoved(0);

                } catch (Exception e) {

                }
                JSONObject object;
                JSONObject msg;
                JSONObject comments;
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                comments = msg.getJSONObject("comments");
                url = comments.getString("next_page_url");


                for (int i = 0; i < comments.getJSONArray("data").length(); i++) {

                    Gson gson = new Gson();

                    list.add(0, gson.fromJson(comments.getJSONArray("data").get(i).toString(), Comment.class));

                    notifyItemInserted(0);
                    Log.d("mmmm"+" "+i,comments.getJSONArray("data").get(i).toString());
                }
                if (!url.equalsIgnoreCase("null")) {
                    list.add(0, null);
                    notifyItemInserted(0);
                }

            }

            @Override
            public void onRequestError(String errorMessage) {

            }
        });


    }

    public void deleteComment(int Commentid) {
        request.MultipartRequest(Request.Method.POST, false, "comment/" + Commentid + "/delete", null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("deleteComment", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("deleteComment", errorMessage);

            }
        });

    }

    public void updateComment(int Commentid, String Newbody, final int position) {

        prams.put("body", Newbody);

        request.MultipartRequest(Request.Method.POST, false, "comment/" + Commentid + "/update", prams, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                JSONObject object;
                JSONObject msg;
                JSONObject comment;
                object = new JSONObject(result);
                msg = object.getJSONObject("msg");
                list.set(position, gson.fromJson(msg.getString("comment"), Comment.class));
                notifyItemChanged(position);
                Log.d("deleteComment", result);
            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("deleteComment", errorMessage);

            }
        });

    }

    public void setNextUrl(String url) {
        this.url = url;
    }


    public class CommentHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_comment_name)
        TextViewCast item_comment_name;
        @BindView(R.id.item_comment_text)
        TextViewCast item_comment_text;
        @BindView(R.id.item_comment_user_img)
        ImageView item_comment_user_img;

        public CommentHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnLongClickListener(new View.OnLongClickListener() {


                @Override
                public boolean onLongClick(View v) {
                    final Comment comment = list.get(getAdapterPosition());

                    AapHalper.CreateDialog(R.layout.dialog_comment_option, context, new DaelogCallback() {
                        @Override
                        public void dialog(final Dialog dialog) {
                            final LinearLayout edit, delete, editOption, block;
                            Button edit_but, cancel_but;
                            final EditTextView new_comment;
                            edit = (LinearLayout) dialog.findViewById(R.id.dialog_comment_option_edit);
                            delete = (LinearLayout) dialog.findViewById(R.id.dialog_comment_option_delete);
                            block = (LinearLayout) dialog.findViewById(R.id.dialog_comment_option_block);
                            editOption = (LinearLayout) dialog.findViewById(R.id.dialog_comment_option_edit_option);
                            edit_but = (Button) dialog.findViewById(R.id.dialog_comment_option_edit_but);
                            cancel_but = (Button) dialog.findViewById(R.id.dialog_comment_option_cancel_but);
                            new_comment = (EditTextView) dialog.findViewById(R.id.dialog_comment_option_new_comment);


                            if (comment.getUser().getId() != pref.getUser().getId()) {

                                edit.setVisibility(View.GONE);
                                delete.setVisibility(View.GONE);
                                block.setVisibility(View.VISIBLE);

                            }

                            block.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    blockUser(comment.getUser_id());
                                    dialog.dismiss();
                                }
                            });
                            edit.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    new_comment.setText(comment.getBody());
                                    editOption.setVisibility(View.VISIBLE);
                                }
                            });
                            edit_but.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!new_comment.getText().toString().equalsIgnoreCase("")) {
                                        updateComment(comment.getId(), new_comment.getText().toString(), getAdapterPosition());
                                        dialog.dismiss();

                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            });
                            cancel_but.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    editOption.setVisibility(View.GONE);
                                }
                            });
                            delete.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    list.remove(getAdapterPosition());
                                    notifyItemRemoved(getAdapterPosition());
                                    // notifyItemRangeChanged(getAdapterPosition(),list.size());

                                    deleteComment(comment.getId());
                                    dialog.dismiss();
                                }
                            });
                        }
                    });


                    return false;
                }
            });

        }
    }


    public class ProgressViewHolder extends RecyclerView.ViewHolder {
        public AVLoadingIndicatorView avi;
        CardView cardView;
        TextView reed_more_comment;


        public ProgressViewHolder(View v) {
            super(v);
            avi = (AVLoadingIndicatorView) v.findViewById(R.id.avi);
            cardView = (CardView) v.findViewById(R.id.reed_more_comment);
            reed_more_comment = (TextView) v.findViewById(R.id.reed_more_comment_text);

        }

    }

    public void blockUser(int userId) {
        prams.put("user_id", userId + "");
        request.MultipartRequest(Request.Method.POST, false, "block", prams, null, new MultipartCallback() {
            @Override
            public void onSuccess(String result) throws JSONException {
                Log.d("block", result);

            }

            @Override
            public void onRequestError(String errorMessage) {
                Log.d("block", errorMessage);

            }
        });

    }


}