package com.it.washim.bakkar.halper;

/**
 * Created by mohammad on 7/27/2017.
 */

public class Constant {


    // public static final String URL = "http://192.168.0.102/bakkar/";

    public static final String URL = "http://bakkar.wojoooh.com/";

    public static final int EXIT = 0;
    public static final int BIOGRAPHY_AND_CAREER = 1;

    public static final int ARTICLES = 2;
    public static final int BOOKS = 3;

    public static final int SOUNDS = 4;
    public static final int COMMUNICATION = 5;

    public static final int MAIN = 6;
    public static final int WISH_LISTED = 7;
    public static final int DEVELOPER_PAGE = 8;
    public static final int AGENTS_AND_OPPOSED = 9;
    public static final int VIDEO = 10;
    public static final int SEARCH = 11;
    public static final int NOTIFICATION = 12;


    public static final int PAGES_ARTICLES = 1;
    public static final int PAGES_BOOKS = 2;
    public static final int PAGES_AUDIO = 3;
    public static final int PAGES_SHOW = 4;
    public static final int PAGES_VIDEO = 5;

    public static final int ATTACHMENT_BOOK = 1;
    public static final int ATTACHMENT_COVER = 2;
    public static final int Attachment_AUDIO = 3;

}
