package com.washimit.it.ryadah.Fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.washimit.it.ryadah.Fragment.profile.AddProduactFragment;
import com.washimit.it.ryadah.Fragment.profile.MyProfileItemFragment;
import com.washimit.it.ryadah.Interface.DaelogCallback;
import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.activity.MainActivity;
import com.washimit.it.ryadah.adapter.fragmentMangerAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.halper.SharedPref;
import com.washimit.it.ryadah.model.user.User;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MangaFragment extends Fragment implements View.OnClickListener {
    @BindView(R.id.view_pager)
    ViewPager view_pager;
    @BindView(R.id.TabLayout)
    TabLayout tabLayout;
    @BindView(R.id.add_product)
    ImageView add_product;

    fragmentMangerAdapter adapter;
    SharedPref pref;
    User user;
    int pageMangeId;
    MyProfileItemFragment fragment;
    Typeface tf;

    public static Fragment newInstants(int mangPageId) {

        MangaFragment fragment = new MangaFragment();

        Bundle bundle = new Bundle();
        bundle.putInt("mangPageId", mangPageId);
        fragment.setArguments(bundle);
        return fragment;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            pageMangeId = getArguments().getInt("mangPageId");

        }


    }

    public MangaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mang, container, false);
        ButterKnife.bind(this, view);
        tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/TheSansArab-Light_2.ttf");

        adapter = new fragmentMangerAdapter(getFragmentManager());
        pref = new SharedPref(getContext());


        updateUi(pageMangeId);

        if (pref.getfirstTimeHome() && pageMangeId == Constant._PAGE_HOME && pref.getUser() != null) {
            final SpannableString spannedDesc = new SpannableString(getString(R.string.developer_name));
            TapTargetView.showFor(getActivity(), TapTarget.forView(view.findViewById(R.id.add_product), getString(R.string.hi), spannedDesc)
                    .cancelable(false)
                    .drawShadow(true)
                    .tintTarget(false), new TapTargetView.Listener() {
                @Override
                public void onTargetClick(TapTargetView view) {
                    super.onTargetClick(view);
                }

                @Override
                public void onOuterCircleClick(TapTargetView view) {
                    super.onOuterCircleClick(view);
                    super.onTargetClick(view);
                }

                @Override
                public void onTargetDismissed(TapTargetView view, boolean userInitiated) {
                    Log.d("TapTargetViewSample", "You dismissed me :(");
                }
            });


        }

        add_product.setOnClickListener(this);
        changeTabsFont();

        return view;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.add_product:

                user = pref.getUser();

                if (user != null) {

                    if (user.getType().getId() == Constant._TYPE_STORE || user.getType().getId() == Constant._TYPE_SHOW) {
                        ((MainActivity) getActivity()).openFragment(new AddProduactFragment());
                    } else {
                        AapHalper.share("مشاركة تطبيق ريادة", getActivity());

                    }

                } else {
                    AapHalper.share("مشاركة تطبيق ريادة", getActivity());

                }

                break;


        }


    }

    public void updateUi(int pageMangeId) {


        switch (pageMangeId) {
            case Constant._PAGE_HOME:
                adapter.addFragment(new FestivalsFragment(), getString(R.string.fastviles));
                adapter.addFragment(HomeFragment.newInstance(Constant.gridLayoutManager, true, Constant._PAGE_SHOW), getString(R.string.show));
                adapter.addFragment(HomeFragment.newInstance(Constant.gridLayoutManager, true, Constant._PAGE_STORE), getString(R.string.store));
                adapter.addFragment(HomeFragment.newInstance(Constant.linearLayoutManager, false, Constant._PAGE_PRODUCT), getString(R.string.home));
                tabLayout.setupWithViewPager(view_pager);
                view_pager.setAdapter(adapter);
                view_pager.setCurrentItem(4);
                view_pager.setOffscreenPageLimit(getLimit());
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_festival);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_showroom);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_store);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_home);
                tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);

                getIcon(3);


                break;

            case Constant._PAGE_CUSTOMER_DETAILS:
                add_product.setVisibility(View.GONE);
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_FESTIVAL), getString(R.string.festival));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_SHOW), getString(R.string.show));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_STORE), getString(R.string.store));
                adapter.addFragment(new WishlistedFragment(), getString(R.string.favorite));
                tabLayout.setupWithViewPager(view_pager);
                view_pager.setAdapter(adapter);
                view_pager.setCurrentItem(3);
                view_pager.setOffscreenPageLimit(getLimit());
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_festival);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_showroom);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_store);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_favorite_1);
                tabLayout.setTabMode(TabLayout.MODE_FIXED);
                tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);
                getIcon(3);

                break;
            case Constant._PAGE_SHOW_DETAILS:
                add_product.setVisibility(View.GONE);

                fragment = MyProfileItemFragment.newInstance(Constant.gridLayoutManager, Constant._TYPE_MY_PRODUCT_DETAILS);

                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_FESTIVAL), getString(R.string.festival));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_REQUEST_SHOW_PRODUCT_STORE_DETAILS), getString(R.string.requests));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_SHOW), getString(R.string.show));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_STORE), getString(R.string.store));
                adapter.addFragment(fragment, getString(R.string.my_products));
                tabLayout.setupWithViewPager(view_pager);
                view_pager.setAdapter(adapter);
                view_pager.setCurrentItem(4);
                view_pager.setOffscreenPageLimit(getLimit());
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_festival);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_requests_sidebar);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_showroom);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_store);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_home);
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);
                getIcon(4);
                break;
            case Constant._PAGE_STORE_DETAILS:
                add_product.setVisibility(View.GONE);
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_FESTIVAL), getString(R.string.festival));
                fragment = MyProfileItemFragment.newInstance(Constant.gridLayoutManager, Constant._TYPE_MY_PRODUCT_DETAILS);
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_REQUEST_STORE_PRODUCT_SHOW_DETAILS), getString(R.string.requests));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_SHOW), getString(R.string.show));
                adapter.addFragment(MyProfileItemFragment.newInstance(Constant.linearLayoutManager, Constant._TYPE_STORE), getString(R.string.store));
                adapter.addFragment(fragment, getString(R.string.my_products));
                tabLayout.setupWithViewPager(view_pager);
                view_pager.setAdapter(adapter);
                view_pager.setCurrentItem(4);
                view_pager.setOffscreenPageLimit(getLimit());
                tabLayout.getTabAt(0).setIcon(R.drawable.ic_festival);
                tabLayout.getTabAt(1).setIcon(R.drawable.ic_requests_sidebar);
                tabLayout.getTabAt(2).setIcon(R.drawable.ic_showroom);
                tabLayout.getTabAt(3).setIcon(R.drawable.ic_store);
                tabLayout.getTabAt(4).setIcon(R.drawable.ic_home);
                tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                tabLayout.setTabMode(TabLayout.GRAVITY_CENTER);

                getIcon(4);
                break;


        }


    }


    private void getIcon(int hom) {
        tabLayout.getTabAt(hom).getIcon().setColorFilter(Color.parseColor("#813331"), PorterDuff.Mode.SRC_IN);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#813331"), PorterDuff.Mode.SRC_IN);
                if (tab.getPosition() != adapter.getCount() - 1 && pageMangeId == Constant._PAGE_HOME) {

                    switch (tab.getPosition()) {

                        case 0:
                            if (pref.getfirstTimeFestivel()) {
                                if (pref.getUser() != null) {
                                    tutorial(0);
                                    pref.SetfirstTimeFestivel();
                                }
                            }


                            break;
                        case 1:
                            if (pref.getfirstTimeShow()) {
                                if (pref.getUser() != null) {

                                    tutorial(1);
                                    pref.SetfirstTimeShow();
                                }

                            }

                            break;

                        case 2:
                            if (pref.getfirstTimeStore()) {
                                if (pref.getUser() != null) {
                                    tutorial(2);
                                    pref.SetfirstTimeStore();
                                }
                            }
                            break;


                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#a8a8a8"), PorterDuff.Mode.SRC_IN);

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    private int getLimit() {
        return (adapter.getCount() > 1 ? adapter.getCount() - 1 : 1);

    }

    public void setSelectProduct(boolean selectProduct) {
        fragment.setSelectProduct(selectProduct);
    }

    public void shaerSelectProduct(String type) {

        fragment.shaerSelectProduct(type);

    }

    private void tutorial(final int userType) {

        AapHalper.CreateDialog(R.layout.dialog_tutorial_home_page, getActivity(), new DaelogCallback() {
            @Override
            public void dialog(final Dialog dialog) {
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

                ImageView dialog_tutorial_img = (ImageView) dialog.findViewById(R.id.dialog_tutorial_img);
                ImageView dialog_tutorial_close = (ImageView) dialog.findViewById(R.id.dialog_tutorial_close);
                TextViewCast text = (TextViewCast) dialog.findViewById(R.id.text);


                switch (userType) {
                    case 0:
                        dialog_tutorial_img.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_dialogue_festival));
                        text.setText(getString(R.string.hi_festivals));

                        break;
                    case 1:

                        dialog_tutorial_img.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_dialogue_showroom));
                        text.setText(getString(R.string.hi_shows));

                        break;

                    case 2:
                        dialog_tutorial_img.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_dialogue_store));
                        text.setText(getString(R.string.hi_stores));

                        break;


                }


                dialog_tutorial_close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });


    }

    private void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tf);
                }
            }
        }
    }
}
