package com.it.washm.alrayaduah.registration;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.it.washm.alrayaduah.R;
import com.it.washm.alrayaduah.customview.CustamViewFont;
import com.it.washm.alrayaduah.helper.AppHelper;
import com.it.washm.alrayaduah.helper.Requests;
import com.it.washm.alrayaduah.interfase.VolleyCallback;

/**
 * Created by washm on 1/3/17.
 */

public class ForgetThePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    //    @InjectView(R.id.txv_back_forget_the_password)TextView back;
    @BindView(R.id.email_forget_the_password)
    AutoCompleteTextView email_forget_the_password;
    @BindView(R.id.btn_forget_the_password_text)
    Button btn_forget_the_password_text;
    @BindView(R.id.btn_forget_the_password)
    RippleView btn_forget_the_password;
    @BindView(R.id.forget_password)
    TextView forget_password;


    Requests requests;
    @BindView(R.id.linerLayout)
    LinearLayout LinearLayout;

    HashMap<String, String> hashMap;
    String email;
    CustamViewFont viewFont;


    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_the_password);
        ButterKnife.bind(this);

        viewFont = new CustamViewFont(ForgetThePasswordActivity.this, "Cairo-SemiBold.ttf");
      hashMap=new HashMap<String, String>();
        setFont();

        requests = new Requests(getApplicationContext(), this);

        btn_forget_the_password.setOnClickListener(this);
        //back.setOnClickListener(this);
    }


    public void setFont() {
        viewFont.setfont(email_forget_the_password);
        viewFont.setfont(forget_password,"Cairo-Bold.ttf");
        viewFont.setfont(btn_forget_the_password_text,"Cairo-Regular.ttf");

    }



    /**
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.txv_back_forget_the_password:{
//               this.finish();
//                break;
//            }
            case R.id.btn_forget_the_password: {
                if (validate()) {
                    if (AppHelper.isOnline(ForgetThePasswordActivity.this)) {


                        hashMap.put("email", email);
                        requests.StringRequest(Request.Method.POST, "reset-password", hashMap, "جاري الارسال...", new VolleyCallback() {
                            @Override
                            public void onSuccess(String result) {
                                Snackbar.make(LinearLayout,result+ "تم ارسال طلب اعادة الكلمة المرور سيتم ارسال ايميل الى بريدك لاكمال عملية اعادة كلمة المرور", BaseTransientBottomBar.LENGTH_LONG).show();

                            }

                            @Override
                            public void onRequestError(VolleyError errorMessage) {
                                Snackbar.make(LinearLayout, "حدثت مشكلة ما يرجى اعادة المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                            }
                        });
                    } else {
                        Snackbar.make(LinearLayout, "لا يوجد اتصال بالشبكة تأكد من الاتصال ثم اعد المحاولة", BaseTransientBottomBar.LENGTH_LONG).show();

                    }
         }
                break;


            }
        }

    }

    /**
     * @return
     */
    public boolean validate() {
        boolean valid = true;

        email = email_forget_the_password.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            email_forget_the_password.setError("آدخل ايميل صحيح");
            valid = false;
        } else {
            email_forget_the_password.setError(null);
        }

        return valid;
    }
}
