<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->double('rating',3,2)->default(0.00);
            $table->text('link')->nullable();
            $table->unsignedSmallInteger('price')->nullable();
            $table->string('published_at',6)->nullable();
            $table->unsignedInteger('page_id');
            $table->unsignedInteger('parent_id')->nullable();
            $table->timestamps();
            $table->foreign('page_id')
                ->references('id')->on('pages');

            $table->foreign('parent_id')
                ->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
