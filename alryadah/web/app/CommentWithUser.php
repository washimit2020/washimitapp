<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentWithUser extends Model
{
    //just to be extended

    public $incrementing = false;
    /**
     * @var array of primary arrays
     */
    protected $primaryKey = ["comment_id", "user_id"];

    /**
     * @var array of fillable arrays
     */
    protected $fillable = ["comment_id", "user_id"];

    /**
     * @var array of hidden arrays
     */
    protected $hidden = ["comment_id", "user_id"];


    /**
     * belongs to one user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * belongs to one news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comment()
    {
        return $this->belongsTo('App\Comment', 'comment_id');
    }
}
