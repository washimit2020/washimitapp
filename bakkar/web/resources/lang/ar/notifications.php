<?php
/**
 * Created by PhpStorm.
 * User: washm
 * Date: 9/6/17
 * Time: 10:45 AM
 */


return
    [
   "added_a_book"=>"لقد تم اضافة كتاب جديد",
   "added_an_article"=>"لقد تم اضافة مقالة جديدة",
   "added_an_agent"=>"لقد تم اضافة وكيل جديد",
   "added_an_audio"=>"لقد تم اضافة صوتية جديدة",
];