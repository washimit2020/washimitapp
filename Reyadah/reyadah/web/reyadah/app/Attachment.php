<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{

    protected $fillable = ['related_id','path','mime_type','extension','type_id'];
    protected $hidden = ['related_id','path','mime_type','extension'];

    public function user(){
        return $this->belongsTo(User::class,'related_id');
    }

    public function type(){
        return $this->belongsTo(Type::class,'type_id');
    }
}
