//
//  AddBranch_TableViewController.swift
//  reyadah
//
//  Created by WASHM on 4/29/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import SwiftValidator
import Alamofire
import SwiftyJSON

class AddBranch_TableViewController: FormsTVC {
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var nameError: UILabel!
    @IBOutlet weak var streetName: UITextField!
    @IBOutlet weak var sectorName: UITextField!
    @IBOutlet weak var streetError: UILabel!
    @IBOutlet weak var sectorError: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerFields()
    }
    
    
    
    private func registerFields(){
        fields = [
            (name, nameError),(streetName,streetError),(sectorName,sectorError)
        ]
        
        validator.registerField(name, errorLabel: nameError, rules: [RequiredRule(message: Constants.requiredErrorMsg),MaxLengthRule(length: 500)])
        
        validator.registerField(streetName, errorLabel: streetError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        validator.registerField(sectorName, errorLabel: sectorError, rules: [RequiredRule(message: Constants.requiredErrorMsg)])
        
        
        action = createbranch(_:)
    }
    
    
    @IBAction func createbranch(_ sender: Any) {
        change_to_old_status()
        validator.validate(self)
    }
    
    
    override func validationSuccessful() {
        
        postValues["name"] = name.text
        
        postValues["street_name"] = streetName.text
        postValues["sector_name"] = sectorName.text
        
        
        
        request.make_request(parameters: postValues,
                             apiImages: nil, link: RequestLinks.create_branch.rawValue,
                             method: .post,
                             dialogMsg: "جاري انشاء فرع",
                             completion: handler)
        
        
    }
    
    
    private func handler(_ response:DataRequest?)->Void{
        
        guard let response = response else {
            self.request.handle_error_dialog(nil)
            return
        }
        response.responseJSON{
            response in
            if response.response?.statusCode == 200{
                self.request.handle_error_dialog("تم انشاء الفرع بنجاح")
                self.request.costumeAlert.dismiss(animated: true, completion:{
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else{
                self.request.handle_error_dialog(JSON(response.data!)["msg"].stringValue)
            }
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  let controller = segue.destination as? AddLocation_ViewController {
            
            controller.userLocationCallBack = {
                annotaiton in
                if let annotation = annotaiton{
                    self.postValues["lat"] = "\(annotation.coordinate.latitude)"
                    self.postValues["lng"] = "\(annotation.coordinate.longitude)"
                }
            }
            
        }
    }
    
    
}
