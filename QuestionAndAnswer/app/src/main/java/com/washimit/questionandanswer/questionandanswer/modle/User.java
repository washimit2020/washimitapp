package com.washimit.questionandanswer.questionandanswer.modle;

/**
 * Created by mohammad on 5/31/2017.
 */

public class User {

    String name;
    String email;
    String api_token;
    String updated_at;
    String created_at;

    public User(String name, String email, String api_token, String updated_at, String created_at) {
        this.name = name;
        this.email = email;
        this.api_token = api_token;
        this.updated_at = updated_at;
        this.created_at = created_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
