package com.washimit.it.ryadah.model.details;

import com.washimit.it.ryadah.model.user.User;

/**
 * Created by mohammad on 5/2/2017.
 package com.washimit.mohammad.ryadah.model.details;

 import com.washimit.mohammad.ryadah.model.user.User;

 /**
 * Created by mohammad on 4/23/2017.
 */

public class Comment {

    String body;
    int user_id;
    int commentable_id;
    String commentable_type;
    String updated_at;
    String created_at;
    int id;
    User user;

    public Comment(String body, int user_id, int commentable_id, String commentable_type, String updated_at, String created_at, int id, User user) {
        this.body = body;
        this.user_id = user_id;
        this.commentable_id = commentable_id;
        this.commentable_type = commentable_type;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.id = id;
        this.user = user;
    }

    public Comment() {

    }
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCommentable_id() {
        return commentable_id;
    }

    public void setCommentable_id(int commentable_id) {
        this.commentable_id = commentable_id;
    }

    public String getCommentable_type() {
        return commentable_type;
    }

    public void setCommentable_type(String commentable_type) {
        this.commentable_type = commentable_type;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}

