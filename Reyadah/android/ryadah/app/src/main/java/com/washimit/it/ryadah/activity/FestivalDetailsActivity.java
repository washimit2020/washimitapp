package com.washimit.it.ryadah.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.washimit.it.ryadah.R;
import com.washimit.it.ryadah.adapter.DetailsPageAdapter;
import com.washimit.it.ryadah.halper.AapHalper;
import com.washimit.it.ryadah.halper.ApiRequest;
import com.washimit.it.ryadah.halper.Constant;
import com.washimit.it.ryadah.model.Festivals;
import com.washimit.it.ryadah.viewCasting.TextViewCast;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FestivalDetailsActivity extends AppCompatActivity  implements View.OnClickListener{
    @BindView(R.id.nodata)
    TextViewCast nodata;
    @BindView(R.id.activity_festival_details_recycler_details)
    RecyclerView activity_festival_details_recycler_details;
    @BindView(R.id.activity_festival_details_user_name)
    TextViewCast activity_details_user_name;
    @BindView(R.id.activity_festival_details_user_icon)
    ImageView activity_details_user_icon;
    @BindView(R.id.activity_festival_details_add_comment)
    ImageView activity_festival_details_add_comment;


    DetailsPageAdapter adapter;
    ArrayList<Object> detailsItem;


    View header;
    String data;
    Object headerData;
    Gson gson;
    Intent intent;
    ApiRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_festival_details);
        ButterKnife.bind(this);
        gson = new Gson();
        Bundle bundle = getIntent().getExtras();
        data = bundle.getString("data");
        request = new ApiRequest(this);

        headerData = gson.fromJson(data, Festivals.class);

        activity_details_user_icon.setImageDrawable(getResources().getDrawable(AapHalper.userIconType(Constant._PAGE_FESTIVAL)));


        header = getLayoutInflater().inflate(R.layout.item_details_header, null);

        detailsItem = new ArrayList<>();


        activity_festival_details_recycler_details.setHasFixedSize(true);

        activity_festival_details_recycler_details.setLayoutManager(new LinearLayoutManager(this));


        adapter = new DetailsPageAdapter(detailsItem, this, activity_festival_details_recycler_details);

        adapter.setHeader(header);

        activity_festival_details_recycler_details.setAdapter(adapter);

        updateUi();

        activity_festival_details_add_comment.setOnClickListener(this);
    }


    public void updateUi() {

        activity_details_user_name.setText(((Festivals) headerData).getName());
        detailsItem.add(headerData);

        if ((((Festivals) headerData).getComments().size() > 0)) {
            detailsItem.add(1,(((Festivals) headerData).getComments().get(0)));
        }
        if ((((Festivals) headerData).getComments().size() > 1)) {
            detailsItem.add(1,(((Festivals) headerData).getComments().get(1)));
        }

        adapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_festival_details_add_comment:
                intent = new Intent(this, CommentActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("Id", ((Festivals) headerData).getId());
                intent.putExtra("type", Constant._TYPE_FESTIVAL_COMMENT);
                startActivity(intent);
                break;
        }
    }
}
