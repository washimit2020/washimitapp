//
//  SocialMedia.swift
//  reyadah
//
//  Created by WASHM on 4/18/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//



class SocialMedia: Model {
    var id:Int!
    var facebook:String!
    var twitter:String!
    var instagram:String!
    var snapchat:String!
    var whatsapp:String!
    
    override func map() {
        
        id = json["id"].intValue
        facebook = json["facebook"].stringValue
        twitter = json["twitter"].stringValue
        instagram = json["instagram"].stringValue
        snapchat = json["snapchat"].stringValue
        whatsapp = json["whatsapp"].stringValue
    }
    
    subscript(_ key: String)->String{
        print(key)
        switch key {
        case "facebook":
            return facebook
        case "twitter":
            return twitter
        case "instagram":
            return instagram
        case "snapchat":
            return snapchat
        default :
            return whatsapp
        }
    }
}
