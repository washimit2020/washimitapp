//
//  Extensions.swift
//  al-haj
//
//  Created by WASHM on 7/27/17.
//  Copyright © 2017 WASHM. All rights reserved.
//

import UIKit

extension UILabel {
    public var substituteFontName : String {
        get {
            return self.font.fontName;
        }
        set {
            let fontNameToTest = self.font.fontName.lowercased();
            var fontName = newValue;
            if fontNameToTest.range(of: "bold") != nil {
                fontName += "-Bold";
            } else if fontNameToTest.range(of: "medium") != nil {
                fontName += "-Medium";
            } else if fontNameToTest.range(of: "light") != nil {
                fontName += "-Light";
            } else if fontNameToTest.range(of: "ultralight") != nil {
                fontName += "-UltraLight";
            }
            self.font = UIFont(name: fontName, size: self.font.pointSize)
        }
    }
}



extension UITextField {
    
    public var substituteFontName : String {
        get {
            return self.font?.fontName ?? "";
        }
        set {
            
            self.font = Utils.shared.setFont(newfontName: newValue, oldFont: self.font)
        }
    }
}


extension String{
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func isClear()->Bool{
        return trimmingCharacters(in: .whitespaces).isEmpty
    }
}

extension NSMutableAttributedString{
    
    func changeSubTitle(size:CGFloat? = nil){
        let font = Utils.shared.get_font_name()
        let color = Utils.shared.is_dark_mode() ? Utils.shared.dark_mode_sub : Utils.shared.light_mode_sub
        let attr = [
            NSAttributedStringKey.font: UIFont(name: font, size: size ?? CGFloat(Utils.shared.get_font_sizes()[1]))!,
            NSAttributedStringKey.foregroundColor:color,
            ]
        
        modify_font(expression: "__", attributes: attr)
    }
    func changeToQuranFont(size:CGFloat? = nil)   {
        let color = Utils.shared.is_dark_mode() ? Utils.shared.dark_mode_sub : Utils.shared.light_mode_sub
        let attr = [
            NSAttributedStringKey.font:UIFont(name: "KFGQPCUthmanicScriptHAFS", size: size ?? CGFloat(Utils.shared.get_font_sizes()[2]))!,
            NSAttributedStringKey.foregroundColor:color
        ]
        
        modify_font(expression: "--", attributes: attr)
//        mutableString.replaceOccurrences(of: "ا۟", with: "ا", options: .caseInsensitive, range: NSRange(location: 0, length: self.length))
//        mutableString.replaceOccurrences(of: "۟", with: "", options: .caseInsensitive, range: NSRange(location: 0, length: self.length))
       
    }
    
    func changeToRef(size:CGFloat = CGFloat(Utils.shared.get_font_sizes()[2] ))   {
        let font = Utils.shared.get_font_name()
        let attr:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font:UIFont(name: font, size: size/2) ,
            NSAttributedStringKey.baselineOffset : CGFloat(7)
        ]
        
        modify_font(expression: "sm", attributes: attr)
    }
    
    func changeToBold(size:CGFloat = CGFloat(Utils.shared.get_font_sizes()[2] ))    {
        let attr:[NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font:UIFont(name: "Lotus-Bold", size: CGFloat(size + 1) ),
        ]
        
        modify_font(expression: "=", attributes: attr)
    }
    
    private func modify_font( expression:String , attributes:[NSAttributedStringKey:Any]){
        
        do {
            
            let exp = "\(expression).+?\(expression)"
            let regex = try NSRegularExpression(pattern:exp, options: [])
            let nsString = mutableString as NSString
            let results = regex.matches(in: mutableString as String,
                                        options: [], range: NSMakeRange(0, nsString.length))
            let ranges =  results.map { (result) -> NSRange in
                //                let location = max(0, result.range.location - 2 )
                //                let length = result.range.length - 2
                
                return result.range
            }
            for range in ranges{
                self.addAttributes(attributes, range: range)
            }
            self.mutableString.replaceOccurrences(of: expression, with: "", options: .caseInsensitive, range: NSRange(location: 0, length: nsString.length))
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        
    }
    private func modify_font2( expression:String , attributes:[NSAttributedStringKey:Any] ){
        do {
            let exp = "\(expression).+?\(expression)"
            let regex = try NSRegularExpression(pattern:exp, options: [])
            let nsString = mutableString as NSString
            let results = regex.matches(in: mutableString as String,
                                        options: [], range: NSMakeRange(0, nsString.length))
            
            let ranges =  results.map { (result) -> NSRange in
                let location = max(0, result.range.location - 2 )
                let length = result.range.length - 2
            
                return NSRange(location: location, length: length)
            }
            
            mutableString.setString(string.replacingOccurrences(of: expression, with: ""))
            for range in ranges{
                addAttributes(attributes, range: range)
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
        }
        
    }
    
}

extension UIColor{
    class var tintColor:UIColor {
        return  #colorLiteral(red: 0.001782631967, green: 0.4196481407, blue: 0.4835928082, alpha: 1)
    }
}
extension UIFont {
    class var titleFont:UIFont? {
        return  UIFont(name: "TheSansArab-Plain", size: 17)
    }
    class var bodyFont:UIFont? {
        return  UIFont(name: "Lotus-Light", size: 17)
    }
}
extension UIView {
    public func createImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(
            CGSize(width: self.frame.width, height: self.frame.height), true, 1)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
extension UINavigationItem{
    public func addTitleImage(){
        let image = UIImageView(image: #imageLiteral(resourceName: "icon-alhaj"))
        self.titleView = image
    }
}
extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func resizeImage(targetSize: CGSize) -> UIImage {
        let size = self.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
}
