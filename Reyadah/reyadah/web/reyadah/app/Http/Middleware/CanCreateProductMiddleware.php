<?php

namespace App\Http\Middleware;

use App\Http\Constants;
use Closure;
use Illuminate\Support\Facades\Gate;

class CanCreateProductMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Gate::denies(Constants::ABILITY_ADD_PRODUCT)){
            abort(Constants::RESPONSE_CODE_FORBIDDEN);
        }
        return $next($request);
    }
}
