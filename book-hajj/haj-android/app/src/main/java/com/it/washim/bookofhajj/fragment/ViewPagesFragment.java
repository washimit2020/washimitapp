package com.it.washim.bookofhajj.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.washim.bookofhajj.activity.MainActivity;
import com.it.washim.bookofhajj.R;
import com.it.washim.bookofhajj.adapter.ViewPagerAdapter;
import com.it.washim.bookofhajj.coustumView.TextViewCustem;
import com.it.washim.bookofhajj.helper.DataBHelper;
import com.it.washim.bookofhajj.interfac.goToPagecallback;
import com.it.washim.bookofhajj.interfac.wishilistedCallback;
import com.it.washim.bookofhajj.model.pages;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPagesFragment extends Fragment {


    DataBHelper helper;
    ViewPagerAdapter pagerAdapter;
    List<pages> pagesList;
    ViewPager viewPager;
    int pageNumber = 0;
    wishilistedCallback collback;
    TextViewCustem pageNumberT;

    public ViewPagesFragment() {
        // Required empty public constructor
    }

    public ViewPagesFragment(int x) {
        pageNumber = x;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_pages, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.view_page);
        pageNumberT = (TextViewCustem) view.findViewById(R.id.page_number);
        helper = new DataBHelper(getActivity());

        pagerAdapter = new ViewPagerAdapter(getFragmentManager());


        pagesList = helper.getListpagestList();


        // Log.nav_header_main("akjshfakjfh", pagesList.get(i).getTitel() + "");
        if (pagesList != null) {
            for (int i = 0; i < pagesList.size(); i++) {
                pagerAdapter.addFragment(PageFragment.newInstance(pagesList.get(i).getTitel(), pagesList.get(i).getBady(), pagesList.get(i).getId(), pagesList.get(i).getImages(), pagesList.get(i).getIsWishilisted(), pagesList.get(i).getComment(), pagesList.get(i).getReferences(), pagesList.get(i).getIsFullLine()), pagesList.get(i).getTitel());

            }
            viewPager.setAdapter(pagerAdapter);


            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    collback.wishilistedCollback(pagesList.get(position).getId());
                    helper.insertRead(pagesList.get(position).getId());
                    pageNumberT.setText(pagesList.get(position).getId() + "");
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            if (pageNumber == 0) {
                viewPager.setCurrentItem(pagesList.size());
                collback.wishilistedCollback(pagesList.get(pagesList.size() - 1).getId());

            } else {
                viewPager.setCurrentItem(pagesList.size() - pageNumber);
                collback.wishilistedCollback(pagesList.get(pagesList.size() - pageNumber).getId());
            }
            helper.close();
        }


        ((MainActivity) getActivity()).OnGoToPage(new goToPagecallback() {
            @Override
            public void goToPage(int i) {
                Log.d("pagesListSize", i + "  " + pagesList.size());
                if (i == 0) {
                    viewPager.setCurrentItem(pagesList.size());
                    collback.wishilistedCollback(pagesList.get(pagesList.size() - 1).getId());

                } else if (i <= pagesList.size()) {
                    viewPager.setCurrentItem(pagesList.size() - i);
                    collback.wishilistedCollback(pagesList.get(pagesList.size() - i).getId());
                    helper.insertRead(pagesList.get(pagesList.size() - i).getId());

                } else if (i > pagesList.size() - 1) {
                    viewPager.setCurrentItem(0);
                    collback.wishilistedCollback(pagesList.get(0).getId());
                    helper.insertRead(pagesList.get(0).getId());

                }


            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        //unbinder.unbind();
        helper.close();
    }

    public void newwishilistedCollback(wishilistedCallback collback) {
        this.collback = collback;
    }

    public void update() {
        pagerAdapter.notifyDataSetChanged();
    }

}


