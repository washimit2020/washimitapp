<?php

namespace App\Http\Controllers\Auth;

use App\Attachment;
use App\Http\Constants;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\SocialMedia;
use App\SocialMediaToken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RegisterController extends Controller
{
    /**
     * to register a new user instance
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $this->validate($request,[
            'login_type' => 'required|in:' . make_validation_array_in([Constants::NORMAL_REGISTER, Constants::SOCIAL_REGISTER]),
            'name' => 'required_if:login_type,' . Constants::NORMAL_REGISTER . '|max:'.Constants::MAX_STRING_LENGTH,
            'email' => 'required|email|max:'.Constants::MAX_STRING_LENGTH,
            'reg_id' => 'required|max:1000',
            'type_id' => 'required|in:' . make_validation_array_in(get_app_user_types()),
            'street_name' => 'required_if:login_type,' . Constants::NORMAL_REGISTER . '|max:'.Constants::MAX_STRING_LENGTH,
            'sector_name' => 'required_if:login_type,' . Constants::NORMAL_REGISTER . '|max:'.Constants::MAX_STRING_LENGTH,
            'phone_number' => 'required_if:login_type,' . Constants::NORMAL_REGISTER . '|digits:10',
            'info' => 'required_if:login_type,' . Constants::NORMAL_REGISTER . '|max:5000',
            'token' => 'required_if:login_type,' . Constants::SOCIAL_REGISTER,
            'lat' => 'required_with:lng|max:'.Constants::MAX_STRING_LENGTH,
            'lng' => 'required_with:lat|max:'.Constants::MAX_STRING_LENGTH,
            'facebook' => 'max:'.Constants::MAX_STRING_LENGTH,
            'twitter' => 'max:'.Constants::MAX_STRING_LENGTH,
            'snapchat' => 'max:'.Constants::MAX_STRING_LENGTH,
            'instagram' => 'max:'.Constants::MAX_STRING_LENGTH,
            'whatsapp' => 'max:'.Constants::MAX_STRING_LENGTH,
            'avatar' => Constants::MIME_TYPE,
            'cover' => 'array|max:' . Constants::MAXIMUM_ALLOWED_IMAGE,
            'cover.*' => Constants::MIME_TYPE
        ]);

        if ($request->login_type == Constants::NORMAL_REGISTER){
            $this->validate($request,[
                'password' => 'required|min:6|confirmed',

            ]);
        }
        $user = $this->check_user_if_exist($request);

        if ($request->login_type == Constants::SOCIAL_REGISTER) {
            $user = $this->social_login($request,$user);
        } else {
            if (empty($user)) {
                $user = $this->create_user($request->all());
            } else {
                return response()->json(response_json(Constants::RESPONSE_FAILED, trans('auth.exist')),Constants::RESPONSE_CODE_UNPROCESSABLE);
            }
        }

        make_api_token($user);

        $this->create_attachments($request, $user);

        return user_response($user);
    }

    /**
     * to handle the token given from the api
     *
     * @param $request
     * @param $user
     * @return mixed
     */
    protected function social_login($request , $user = null)
    {

        if (empty($user)) {
            $this->validate(
                $request,[
                    'name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
                    'email' => 'required|email|max:'.Constants::MAX_STRING_LENGTH,
                    'reg_id' => 'required|max:1000',
                    'type_id' => 'required|in:' . make_validation_array_in(get_app_user_types()),
                    'street_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
                    'sector_name' => 'required|max:'.Constants::MAX_STRING_LENGTH,
                    'phone_number' => 'required|digits:10',
                    'info' => 'required|max:5000',
                    'token' => 'required|max:500000',
                    ]
            );
            $user = $this->create_user($request->all());
        }

        $token = SocialMediaToken::where([
            ['user_id', $user->id],
            [ 'token' ,md5( $request->token)]
        ])->first();

        if (empty($token)) {
           SocialMediaToken::create(['user_id' => $user->id, 'token' => md5( $request->token)]);
        }

        return $user;

    }

    /**
     * to check if the instance already exists in the database
     *
     * @param $request
     * @return mixed
     */
    protected function check_user_if_exist($request)
    {
        $user = User::where([
            ['type_id', $request->type_id],
            ['phone_number', $request->phone_number]
        ])->orWhere([
            ['type_id', $request->type_id],
            ['email', $request->email]
        ])->first();

        return $user;
    }

    /**
     * to create the new instance
     *
     * @param array $data
     * @return mixed
     */
    private function create_user(array $data)
    {
        $social_media = SocialMedia::create($data)->id;

        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => isset($data['password']) ? bcrypt($data['password']) : null,
            'phone_number' => $data['phone_number'],
            'info' => $data['info'],
            'reg_id' => $data['reg_id'],
            'type_id' => $data['type_id'],
            'status_id' => Constants::_STATUS_ACTIVE,
            'street_name' => isset($data['street_name']) ? $data['street_name'] : 'none',
            'sector_name' => isset($data['sector_name']) ? $data['sector_name'] : 'none',
            'lat' => isset($data['lat']) ? $data['lat'] : null,
            'lng' => isset($data['lng']) ? $data['lng'] : null,
            'social_media_id' => $social_media
        ]);
    }

    /**
     * @param $request
     * @param User $user
     */
    protected function create_attachments(Request $request, User $user)
    {
        $dir = "users/{$user->id}/";

        Storage::deleteDirectory($dir);
        register_user_attachment($request, $user);
    }
}
