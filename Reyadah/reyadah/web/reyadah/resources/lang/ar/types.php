<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Types Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during the systems.
    |
    */

    'store'=>'متجر',
    'show'=>'معرض',
    'costumer'=>'مستخدم',
    'festival'=>'مهرجان',
    'product'=>'منتج',
    'avatar-image'=>'صورة شخصية',
    'cover-image'=>'صورة خلقية',
    'product-attachment'=>'مرفق منتج',
    'product-like'=>'اعجاب بمنتج',
    'product-wishlist'=>'منتج مفضل',
];