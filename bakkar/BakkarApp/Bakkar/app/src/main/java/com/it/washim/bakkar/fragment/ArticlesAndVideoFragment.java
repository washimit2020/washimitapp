package com.it.washim.bakkar.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.it.washim.bakkar.R;
import com.it.washim.bakkar.activity.ArticleDetailsActivity;
import com.it.washim.bakkar.adapter.ArticleAndVideoAdapter;
import com.it.washim.bakkar.custemView.MyButtonView;
import com.it.washim.bakkar.custemView.MyTextView;
import com.it.washim.bakkar.halper.Constant;
import com.it.washim.bakkar.interfase.OnRequestCollBack;
import com.it.washim.bakkar.model.Articles;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArticlesAndVideoFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    ArticleAndVideoAdapter adapter;
    @BindView(R.id.articles_list)
    ExpandableListView listView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;


    @BindView(R.id.ratray)
    MyButtonView ratray;

    @BindView(R.id.na_data)
    MyTextView na_data;
    @BindView(R.id.articles)
    MyTextView article;

    ArrayList<Articles> articles;
    private int groupP, childP;

    private int id;

    public ArticlesAndVideoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_articles_and_fragment, container, false);
        ButterKnife.bind(this, view);
        articles = new ArrayList<>();

        id = getArguments().getInt("id");

        if (id == Constant.PAGES_ARTICLES) {
            article.setText("المقالات");
        } else if (id == Constant.PAGES_VIDEO) {
            article.setText("فيديو");

        }


        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);

                getData();
            }
        });


//        adapter = new ArticleAdapter(getContext(), header, child);
        adapter = new ArticleAndVideoAdapter(getContext(), articles);

        listView.setAdapter(adapter);

        listView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                for (int i = 0; i < articles.size(); i++) {
                    if (groupPosition != i) {
                        listView.collapseGroup(i);
                    }
                }
            }
        });

        // Listview on child click listener
        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {

                groupP = groupPosition;
                childP = childPosition;
                startActivityForResult(new Intent(getActivity(), ArticleDetailsActivity.class)
                                .putExtra("id", articles.get(groupPosition).children.get(childPosition).id)
                                .putExtra("title", articles.get(groupPosition).children.get(childPosition).title)
                                .putExtra("description", articles.get(groupPosition).children.get(childPosition).description)
                                .putExtra("is_wishlisted", articles.get(groupPosition).children.get(childPosition).is_wishlisted)
                                .putExtra("img", articles.get(groupPosition).children.get(childPosition).attachments.size() > 0 ? articles.get(groupPosition).children.get(childPosition).attachments.get(0).id : null)
                        , 1);


                return false;
            }
        });

        swipe_refresh.setOnRefreshListener(this);

        ratray.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipe_refresh.setRefreshing(true);
                getData();

            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (data != null) {
                articles.get(groupP).children.get(childP).is_wishlisted = data.getExtras().getBoolean("is_wishlisted");
            }
            adapter.notifyDataSetChanged();
        }

    }

    public void getData() {
        ratray.setVisibility(View.GONE);
        na_data.setVisibility(View.GONE);
        request().stringRequest(Request.Method.GET, getActivity(), "posts/" + id, null, new OnRequestCollBack() {
            @Override
            public void onResponse(String response) {
                articles.clear();
                JSONObject object;
                JSONObject posts;
                try {
                    object = new JSONObject(response);
                    posts = object.getJSONObject("posts");
                    articles.addAll((Collection<? extends Articles>) gson().fromJson(posts.getString("data"), new TypeToken<ArrayList<Articles>>() {
                    }.getType()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (articles.size() > 0) {
                    swipe_refresh.setVisibility(View.VISIBLE);
                    na_data.setVisibility(View.GONE);
                } else {
                    na_data.setText("لايوجد كتب");
                    swipe_refresh.setVisibility(View.GONE);
                    na_data.setVisibility(View.VISIBLE);

                }
                //Log.d("asfsfaf", articles.size() + "");
                // Log.d("asfsfaf", articles.get(0).title + "");
                swipe_refresh.setRefreshing(false);

                adapter.notifyDataSetChanged();

            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (getActivity() != null) {
                    Toast.makeText(getActivity(), "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                }
                swipe_refresh.setRefreshing(false);
                na_data.setText("لايوجد اتصال بالانترنت");
                swipe_refresh.setVisibility(View.GONE);
                na_data.setVisibility(View.VISIBLE);
                ratray.setVisibility(View.VISIBLE);


            }
        });


    }


    @Override
    public void onRefresh() {

        getData();

    }
}
