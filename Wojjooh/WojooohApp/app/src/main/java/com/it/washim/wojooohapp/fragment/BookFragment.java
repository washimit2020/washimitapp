package com.it.washim.wojooohapp.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.it.washim.wojooohapp.CustemView.MyButtonView;
import com.it.washim.wojooohapp.Halper.Constant;
import com.it.washim.wojooohapp.Interfases.OnLoadMoreCollBack;
import com.it.washim.wojooohapp.Interfases.OnRequestCollBack;
import com.it.washim.wojooohapp.Interfases.OnRequestPublishCollBack;
import com.it.washim.wojooohapp.Model.Categorie;
import com.it.washim.wojooohapp.Model.Data;
import com.it.washim.wojooohapp.Model.Post;
import com.it.washim.wojooohapp.R;
import com.it.washim.wojooohapp.adapter.BookAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BookFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    @BindView(R.id.recycler_book)
    RecyclerView recyclerView;
    @BindView(R.id.book_type)
    Spinner bookType;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipe_refresh;
    @BindView(R.id.no_data)
    TextView no_data;
    @BindView(R.id.retray_book)
    MyButtonView retray;
    BookAdapter adapter;
    ArrayList<Post> list;
    ArrayList<Categorie> categories;
    ArrayAdapter<Categorie> arrayAdapter;
    Runnable runnable;
    Handler handler;
    String url;
    int categorieId = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_book, container, false);
        ButterKnife.bind(this, view);
        list = new ArrayList<>();
        categories = new ArrayList<>();
        handler = new Handler();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new BookAdapter(getActivity(), list, recyclerView);
        recyclerView.setAdapter(adapter);
        retray.setOnClickListener(this);
        swipe_refresh.setOnRefreshListener(this);

        swipe_refresh.post(new Runnable() {
            @Override
            public void run() {
                swipe_refresh.setRefreshing(true);
                getBook();
                getCategories();

            }
        });

        adapter.setOnLoadMoreLesnar(new OnLoadMoreCollBack() {
            @Override
            public void onLoad() {

                runnable = new Runnable() {
                    public void run() {
                        list.add(null);
                        adapter.notifyItemInserted(list.size() - 1);
                        getNextBook(url);

                    }
                };
                handler.post(runnable);

            }
        });

        bookType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categorieId = categories.get(position).id;
                swipe_refresh.setRefreshing(true);
                getBook();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    @Override
    public void onRefresh() {
        getBook();
    }

    public void getBook() {
        recyclerView.setVisibility(View.VISIBLE);
        retray.setVisibility(View.GONE);
        no_data.setVisibility(View.GONE);

        getRequest().request(Request.Method.GET, "posts/" + Constant.PAGES_BOOK + "/" + categorieId, null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                list.clear();
                list.addAll(response.data);
                url = response.links.next;
                adapter.setUrl(url);
                Log.d("safasfasf", url + "");
                swipe_refresh.setRefreshing(false);

                adapter.notifyDataSetChanged();

                adapter.isLoading();

                if (list.size() > 0) {

                    recyclerView.setVisibility(View.VISIBLE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setText("لا يوجد");
                    recyclerView.setVisibility(View.GONE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipe_refresh.setRefreshing(false);
                no_data.setText("لا يوجد اتصال بالانترنت");
                recyclerView.setVisibility(View.GONE);
                retray.setVisibility(View.VISIBLE);
                no_data.setVisibility(View.VISIBLE);
                adapter.isLoading();

            }
        });
    }

    public void getNextBook(String nextUrl) {


        getRequest().request(Request.Method.GET, nextUrl.replace("http://192.168.0.101/wojoooh/api/", ""), null, new OnRequestCollBack() {
            @Override
            public void onResponse(Data response) {
                list.remove(list.size() - 1);
                adapter.notifyItemRemoved(list.size());
                url = response.links.next;
                adapter.setUrl(url);
                Log.d("asfasf", url + "");


                for (int i = 0; i < response.data.size(); i++) {
                    list.add(response.data.get(i));
                    adapter.notifyItemInserted(list.size() - 1);

                }

                adapter.isLoading();


                if (list.size() > 0) {

                    recyclerView.setVisibility(View.VISIBLE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.GONE);
                } else {
                    no_data.setText("لا يوجد");
                    recyclerView.setVisibility(View.GONE);
                    retray.setVisibility(View.GONE);
                    no_data.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                swipe_refresh.setRefreshing(false);
                no_data.setText("لا يوجد اتصال بالانترنت");
                recyclerView.setVisibility(View.GONE);
                retray.setVisibility(View.VISIBLE);
                adapter.isLoading();
                no_data.setVisibility(View.VISIBLE);
            }
        });
    }

    public void getCategories() {

        getRequest().requestString(Request.Method.GET, "categories", null, new OnRequestPublishCollBack() {
            @Override
            public void onResponse(String response) {
                JSONObject object;
                try {
                    object = new JSONObject(response);
                    categories.clear();
                    categories.addAll((Collection<? extends Categorie>) getGson().fromJson(object.getString("data"), new TypeToken<ArrayList<Categorie>>() {
                    }.getType()));
                    arrayAdapter = new ArrayAdapter<Categorie>(getActivity(), android.R.layout.simple_spinner_item, categories);
                    bookType.setAdapter(arrayAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retray_book:
                swipe_refresh.setRefreshing(true);
                getBook();
                getCategories();
                break;


        }
    }


}