package com.washimit.questionandanswer.questionandanswer.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.washimit.questionandanswer.questionandanswer.R;
import com.washimit.questionandanswer.questionandanswer.custumView.TouchImageView;

import static android.R.attr.id;

/**
 * A simple {@link Fragment} subclass.
 */
public class CvFragment extends Fragment {

    public CvFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cv, container, false);

        return view;
    }

}
