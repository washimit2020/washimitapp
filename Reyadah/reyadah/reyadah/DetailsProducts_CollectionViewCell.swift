//
//  DetailsProducts_CollectionViewCell.swift
//  reyadah
//
//  Created by WASHM on 4/23/17.
//  Copyright © 2017 Wojoooh. All rights reserved.
//

import UIKit
import Kingfisher

class DetailsProducts_CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var ratingStars: CosmosView!
    @IBOutlet weak var totalLikes: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var numberOfEaters: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    var product:Product?{
        didSet{
            updateVew()

        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setup_cosmos(cosmos: ratingStars)
    }
    
    func updateVew(){
        guard let product = product else {
            return
        }
        
        ratingStars.rating = product.rating
        totalLikes.text = set_likes(product.total_likes)
        price.text = set_price(product.price)
        numberOfEaters.text = product.numberOfEaters
        name.text = product.name
        
        productImage.set_product_image(product_id: product.id)

//
    }
}
